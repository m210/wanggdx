package ru.m210projects.Wang;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.Sect_User;
import ru.m210projects.Wang.Type.Vector3i;

import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Names.STAT_COPY_DEST;
import static ru.m210projects.Wang.Names.STAT_COPY_SOURCE;
import static ru.m210projects.Wang.Sector.SectorMidPoint;
import static ru.m210projects.Wang.Sector.getSectUser;
import static ru.m210projects.Wang.Sprites.KillSprite;
import static ru.m210projects.Wang.Sprites.SetupSectUser;
import static ru.m210projects.Wang.Stag.SECT_COPY_SOURCE;
import static ru.m210projects.Wang.Track.*;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Weapon.AddSpriteToSectorObject;
import static ru.m210projects.Wang.Weapon.SpriteQueueDelete;

public class CopySect {

    public static void CopySectorWalls(int dest_sectnum, int src_sectnum) {
        Sector destSec = boardService.getSector(dest_sectnum);
        Sector srcSec = boardService.getSector(src_sectnum);

        if (destSec == null || srcSec == null) {
            return;
        }

        int dest_wall_num = destSec.getWallptr();
        int src_wall_num = srcSec.getWallptr();
        int start_wall = dest_wall_num;

        do {
            Wall destWal = boardService.getWall(dest_wall_num);
            Wall srcWal = boardService.getWall(src_wall_num);

            if (destWal == null || srcWal == null) {
                return;
            }

            destWal.setPicnum(srcWal.getPicnum());
            destWal.setXrepeat(srcWal.getXrepeat());
            destWal.setYrepeat(srcWal.getYrepeat());
            destWal.setOverpicnum(srcWal.getOverpicnum());
            destWal.setPal(srcWal.getPal());
            destWal.setCstat(srcWal.getCstat());
            destWal.setShade(srcWal.getShade());
            destWal.setXpanning(srcWal.getXpanning());
            destWal.setYpanning(srcWal.getYpanning());
            destWal.setHitag(srcWal.getHitag());
            destWal.setLotag(srcWal.getLotag());
            destWal.setExtra(srcWal.getExtra());

            if (destWal.getNextwall() >= 0 && srcWal.getNextwall() >= 0) {
                Wall nDestWal = boardService.getWall(destWal.getNextwall());
                Wall nSrcWal = boardService.getWall(srcWal.getNextwall());

                if (nDestWal != null && nSrcWal != null) {
                    nDestWal.setPicnum(nSrcWal.getPicnum());
                    nDestWal.setXrepeat(nSrcWal.getXrepeat());
                    nDestWal.setYrepeat(nSrcWal.getYrepeat());
                    nDestWal.setOverpicnum(nSrcWal.getOverpicnum());
                    nDestWal.setPal(nSrcWal.getPal());
                    nDestWal.setCstat(nSrcWal.getCstat());
                    nDestWal.setShade(nSrcWal.getShade());
                    nDestWal.setXpanning(nSrcWal.getXpanning());
                    nDestWal.setYpanning(nSrcWal.getYpanning());
                    nDestWal.setHitag(nSrcWal.getHitag());
                    nDestWal.setLotag(nSrcWal.getLotag());
                    nDestWal.setExtra(nSrcWal.getExtra());
                }
            }

            dest_wall_num = destWal.getPoint2();
            src_wall_num = srcWal.getPoint2();
        } while (dest_wall_num != start_wall);
    }

    public static void CopySectorMatch(int match) {
        ListNode<Sprite> nexted, nextss;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_COPY_DEST); node != null; node = nexted) {
            int ed = node.getIndex();
            nexted = node.getNext();
            Sprite dest_sp = node.get();
            Sector dsectp = boardService.getSector(dest_sp.getSectnum());
            if (dsectp == null) {
                continue;
            }

            if (match != dest_sp.getLotag()) {
                continue;
            }

            for (ListNode<Sprite> ssnode = boardService.getStatNode(STAT_COPY_SOURCE); ssnode != null; ssnode = nextss) {
                nextss = ssnode.getNext();
                Sprite src_sp = ssnode.get();

                if (SP_TAG2(src_sp) == SPRITE_TAG2(ed) && SP_TAG3(src_sp) == SPRITE_TAG3(ed)) {
                    Sector ssectp = boardService.getSector(src_sp.getSectnum());
                    if (ssectp == null) {
                        continue;
                    }

                    // !!!!!AAAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHHHH
                    // Don't kill anything you don't have to
                    // this wall killing things on a Queue causing
                    // invalid situations

                    ListNode<Sprite> nextkill, nextsrc_move;
                    for (ListNode<Sprite> killNode = boardService.getSectNode(dest_sp.getSectnum()); killNode != null; killNode = nextkill) {
                        int kill = killNode.getIndex();
                        nextkill = killNode.getNext();
                        Sprite k = killNode.get();

                        // kill anything not invisible
                        if (!TEST(k.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                            if (getUser(kill) == null) {
                                SpriteQueueDelete(kill); // new function to allow killing - hopefully
                                KillSprite(kill);
                            }
                        }
                    }

                    CopySectorWalls(dest_sp.getSectnum(), src_sp.getSectnum());

                    for (ListNode<Sprite> srcNode = boardService.getSectNode(src_sp.getSectnum()); srcNode != null; srcNode = nextsrc_move) {
                        final int src_move = srcNode.getIndex();
                        Sprite srcSpr = srcNode.get();
                        nextsrc_move = srcNode.getNext();

                        // don't move ST1 Copy Tags
                        if (SPRITE_TAG1(src_move) != SECT_COPY_SOURCE) {

                            // move sprites from source to dest - use center offset

                            // get center of src and dest sect
                            Vector3i s = SectorMidPoint(src_sp.getSectnum());
                            int sx = s.x;
                            int sy = s.y;
                            Vector3i d = SectorMidPoint(dest_sp.getSectnum());
                            int dx = d.x;
                            int dy = d.y;

                            // get offset
                            int src_xoff = sx - srcSpr.getX();
                            int src_yoff = sy - srcSpr.getY();

                            // move sprite to dest sector
                            srcSpr.setX(dx - src_xoff);
                            srcSpr.setY(dy - src_yoff);

                            // change sector
                            engine.changespritesect(src_move, dest_sp.getSectnum());

                            Sector sec = boardService.getSector(dest_sp.getSectnum());
                            // check to see if it moved on to a sector object
                            if (sec != null && TEST(sec.getExtra(), SECTFX_SECTOR_OBJECT)) {
                                // find and add sprite to SO
                                int sopi = DetectSectorObject(boardService.getSector(srcSpr.getSectnum()));
                                AddSpriteToSectorObject(src_move, sopi);

                                // update sprites postions so they aren't in the
                                // wrong place for one frame
                                GlobSpeedSO = 0;
                                RefreshPoints(sopi, 0, 0, true, 1);
                            }
                        }
                    }

                    // copy sector user if there is one
                    if (getSectUser(src_sp.getSectnum()) != null || getSectUser(dest_sp.getSectnum()) != null) {
                        Sect_User ssectu = SetupSectUser(src_sp.getSectnum());
                        Sect_User dsectu = SetupSectUser(dest_sp.getSectnum());

                        dsectu.set(ssectu);
                    }

                    dsectp.setHitag(ssectp.getHitag());
                    dsectp.setLotag(ssectp.getLotag());

                    dsectp.setFloorz(ssectp.getFloorz());
                    dsectp.setCeilingz(ssectp.getCeilingz());

                    dsectp.setFloorshade(ssectp.getFloorshade());
                    dsectp.setCeilingshade(ssectp.getCeilingshade());

                    dsectp.setFloorpicnum(ssectp.getFloorpicnum());
                    dsectp.setCeilingpicnum(ssectp.getCeilingpicnum());

                    dsectp.setFloorheinum(ssectp.getFloorheinum());
                    dsectp.setCeilingheinum(ssectp.getCeilingheinum());

                    dsectp.setFloorpal(ssectp.getFloorpal());
                    dsectp.setCeilingpal(ssectp.getCeilingpal());

                    dsectp.setFloorxpanning(ssectp.getFloorxpanning());
                    dsectp.setCeilingxpanning(ssectp.getCeilingxpanning());

                    dsectp.setFloorypanning(ssectp.getFloorypanning());
                    dsectp.setCeilingypanning(ssectp.getCeilingypanning());

                    dsectp.setFloorstat(ssectp.getFloorstat());
                    dsectp.setCeilingstat(ssectp.getCeilingstat());

                    dsectp.setExtra(ssectp.getExtra());
                    dsectp.setVisibility(ssectp.getVisibility());
                }
            }
        }

        // do this outside of processing loop for safety

        // kill all matching dest
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_COPY_DEST); node != null; node = nexted) {
            nexted = node.getNext();
            if (match == node.get().getLotag()) {
                KillSprite(node.getIndex());
            }
        }

        // kill all matching sources
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_COPY_SOURCE); node != null; node = nextss) {
            nextss = node.getNext();
            if (match == node.get().getLotag()) {
                KillSprite(node.getIndex());
            }
        }
    }
}
