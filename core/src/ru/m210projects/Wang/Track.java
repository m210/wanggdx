package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Point;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Wang.Sound.SoundType;
import ru.m210projects.Wang.Type.*;
import ru.m210projects.Wang.Type.Anim.AnimCallback;
import ru.m210projects.Wang.Type.Anim.AnimType;
import ru.m210projects.Wang.Type.Sector_Object.SOAnimator;

import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Wang.Actor.DoActorBeginJump;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Enemies.Ninja.NinjaJumpActionFunc;
import static ru.m210projects.Wang.Enemies.Ripper.PickJumpSpeed;
import static ru.m210projects.Wang.Factory.WangNetwork.Prediction;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Morth.ScaleRandomPoint;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Player.*;
import static ru.m210projects.Wang.Rooms.*;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Stag.*;
import static ru.m210projects.Wang.Tags.*;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Weapon.*;

public class Track {

    public static final int ACTOR_STD_JUMP = (-384);
    public static final int MAX_TRACKS = 100;
    /*
     *
     * !AIC - Looks at endpoints to figure direction of the track and the closest
     * point to the sprite.
     *
     */
    public static final int TOWARD_PLAYER = 1;
    public static final int AWAY_FROM_PLAYER = -1;
//    public static final int TRACK_POINT_SIZE = 200;
    private static final short[] StatList = {STAT_DEFAULT, STAT_MISC, STAT_ITEM, STAT_TRAP, STAT_SPAWN_SPOT,
            STAT_SOUND_SPOT, STAT_WALL_MOVE, STAT_WALLBLOOD_QUEUE, STAT_SPRITE_HIT_MATCH, STAT_AMBIENT,
            STAT_DELETE_SPRITE, STAT_SPAWN_TRIGGER, // spawing monster trigger - for Randy's bullet train.
    };
    public static int GlobSpeedSO;
    public static final TRACK[] Track = new TRACK[MAX_TRACKS];
    public static final int[] end_point = {0, 0};
    private static final int[] z = new int[2];

    public static boolean TrackTowardPlayer(int sp, TRACK t, int start_point) {
        Sprite s = boardService.getSprite(sp);
        TRACK_POINT end_point = t.TrackPoint[0];
        // determine which end of the Track we are starting from
        if (start_point == 0) {
            end_point = t.TrackPoint[t.NumPoints - 1];
        }

        if (s == null || end_point == null) {
            return false;
        }

        int end_dist = Distance(end_point.x, end_point.y, s.getX(), s.getY());
        int start_dist = Distance(t.TrackPoint[start_point].x, t.TrackPoint[start_point].y, s.getX(), s.getY());
        return end_dist < start_dist;

    }

    public static boolean TrackStartCloserThanEnd(int SpriteNum, TRACK t, int start_point) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return false;
        }

        TRACK_POINT end_point = t.TrackPoint[0];
        // determine which end of the Track we are starting from
        if (start_point == 0) {
            end_point = t.TrackPoint[t.NumPoints - 1];
        }

        int end_dist = Distance(end_point.x, end_point.y, sp.getX(), sp.getY());
        int start_dist = Distance(t.TrackPoint[start_point].x, t.TrackPoint[start_point].y, sp.getX(), sp.getY());

        return start_dist < end_dist;
    }

    public static int ActorFindTrack(int SpriteNum, int player_dir, int track_type, LONGp track_point_num, LONGp track_dir) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return 0;
        }

        int near_dist = 999999;
        int near_track = -1;
        TRACK_POINT near_tp = null;

        // look at all tracks finding the closest endpoint
        for (int ti = 0; ti < MAX_TRACKS; ti++) {
            TRACK t = Track[ti];

            // Skip if high tag is not ONE of the track type we are looking for
            if (!TEST(t.ttflags, track_type)) {
                continue;
            }

            // Skip if already someone on this track
            if (TEST(t.flags, TF_TRACK_OCCUPIED)) {
                continue;
            }

            switch (track_type) {
                case 1 << (TT_DUCK_N_SHOOT): {
                    if (u.ActorActionSet.Duck == null) {
                        return (-1);
                    }

                    end_point[1] = 0;
                    break;
                }

                // for ladders only look at first track point
                case 1 << (TT_LADDER): {
                    if (u.ActorActionSet.Climb == null) {
                        return (-1);
                    }

                    end_point[1] = 0;
                    break;
                }

                case 1 << (TT_JUMP_UP):
                case 1 << (TT_JUMP_DOWN): {
                    if (u.ActorActionSet.Jump == null) {
                        return (-1);
                    }

                    end_point[1] = 0;
                    break;
                }

                case 1 << (TT_TRAVERSE): {
                    if (u.ActorActionSet.Crawl == null || u.ActorActionSet.Jump == null) {
                        return (-1);
                    }

                    break;
                }

                // look at end point also
                default:
                    end_point[1] =  (t.NumPoints - 1);
                    break;
            }

            int zdiff = Z(16);

            // Look at both track end points to see wich is closer
            for (int i = 0; i < 2; i++) {
                int tp = end_point[i];

                int dist = Distance(t.TrackPoint[tp].x, t.TrackPoint[tp].y, sp.getX(), sp.getY());

                if (dist < 15000 && dist < near_dist) {
                    // make sure track start is on approximate z level - skip if
                    // not
                    if (klabs(sp.getZ() - t.TrackPoint[tp].z) > zdiff) {
                        continue;
                    }

                    // determine if the track leads in the direction we want it
                    // to
                    if (player_dir == TOWARD_PLAYER) {
                        if (!TrackTowardPlayer(u.tgt_sp, t, tp)) {
                            continue;
                        }
                    } else if (player_dir == AWAY_FROM_PLAYER) {
                        if (TrackTowardPlayer(u.tgt_sp, t, tp)) {
                            continue;
                        }
                    }

                    // make sure the start distance is closer than the end
                    // distance
                    if (!TrackStartCloserThanEnd(SpriteNum, t, tp)) {
                        continue;
                    }

                    near_dist = dist;
                    near_track = ti;
                    near_tp = t.TrackPoint[tp];

                    track_point_num.value = end_point[i];
                    track_dir.value = i != 0 ? -1 : 1;
                }
            }
        }

        if (near_dist < 15000 && near_tp != null) {
            // get the sector number of the point

            int track_sect = COVERupdatesector(near_tp.x, near_tp.y, 0);
            Sector sec = boardService.getSector(track_sect);
            if (sec == null) {
                return -1;
            }

            // if can see the point, return the track number
            if (FAFcansee(sp.getX(), sp.getY(), sp.getZ() - Z(16), sp.getSectnum(), near_tp.x, near_tp.y,
                    sec.getFloorz() - Z(32), track_sect)) {
                return near_track;
            }
        }

        return (-1);
    }

    public static void NextTrackPoint(Sector_Object sop) {
        sop.point += sop.dir;

        if (sop.point > Track[sop.track].NumPoints - 1) {
            sop.point = 0;
        }

        if (sop.point < 0) {
            sop.point =  (Track[sop.track].NumPoints - 1);
        }
    }

    public static void NextActorTrackPoint(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u != null) {
            u.point += u.track_dir;

            if (u.point > Track[u.track].NumPoints - 1) {
                u.point = 0;
            }

            if (u.point < 0) {
                u.point = (Track[u.track].NumPoints - 1);
            }
        }
    }

    public static void TrackAddPoint(TRACK t, TRACK_POINT[] tp, int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        if (tp[t.NumPoints] == null) {
            tp[t.NumPoints] = new TRACK_POINT();
        }
        TRACK_POINT tpoint = tp[t.NumPoints];

        tpoint.x = sp.getX();
        tpoint.y = sp.getY();
        tpoint.z = sp.getZ();
        tpoint.ang = sp.getAng();
        tpoint.tag_low = sp.getLotag();
        tpoint.tag_high = sp.getHitag();

        t.NumPoints++;

        KillSprite(SpriteNum);
    }

    public static int TrackClonePoint(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return -1;
        }

        int newsp = COVERinsertsprite(sp.getSectnum(), sp.getStatnum());
        Sprite np = boardService.getSprite(newsp);
        if (np == null) {
            return -1;
        }

        np.setCstat(0);
        np.setExtra(0);
        np.setX(sp.getX());
        np.setY(sp.getY());
        np.setZ(sp.getZ());
        np.setAng(sp.getAng());
        np.setLotag(sp.getLotag());
        np.setHitag(sp.getHitag());

        return (newsp);
    }

    public static void QuickJumpSetup(int stat, int lotag, int type) {
        // make short quick jump tracks
        ListNode<Sprite> NextSprite;
        for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = NextSprite) {
            final int SpriteNum = node.getIndex();
            NextSprite = node.getNext();

            // find an open track
            int ndx = 0;
            for (; ndx < MAX_TRACKS; ndx++) {
                if (Track[ndx].NumPoints == 0) {
                    break;
                }
            }

            Track[ndx].TrackPoint = new TRACK_POINT[4];

            TRACK_POINT[] tp = Track[ndx].TrackPoint;
            TRACK t = Track[ndx];

            // set track type
            t.ttflags |= (BIT(type));
            t.flags = 0;

            // clone point
            int end_sprite = TrackClonePoint(SpriteNum);
            int start_sprite = TrackClonePoint(SpriteNum);

            // add start point
            Sprite nsp = boardService.getSprite(start_sprite);
            if (nsp != null) {
                nsp.setLotag(TRACK_START);
                nsp.setHitag(0);
                TrackAddPoint(t, tp, start_sprite);
            }

            // add jump point
            nsp = node.get();
            nsp.setX(nsp.getX() + (64 * EngineUtils.cos(nsp.getAng()) >> 14));
            nsp.setY(nsp.getY() + (64 * EngineUtils.sin(nsp.getAng()) >> 14));
            nsp.setLotag( lotag);
            TrackAddPoint(t, tp, SpriteNum);

            // add end point
            nsp = boardService.getSprite(end_sprite);
            if (nsp != null) {
                nsp.setX(nsp.getX() + (2048 * EngineUtils.cos(nsp.getAng()) >> 14));
                nsp.setY(nsp.getY() + (2048 * EngineUtils.sin(nsp.getAng()) >> 14));
                nsp.setLotag(TRACK_END);
                nsp.setHitag(0);
                TrackAddPoint(t, tp, end_sprite);
            }
        }
    }

    public static void QuickScanSetup(int stat, int lotag, int type) {
        // make short quick jump tracks
        ListNode<Sprite> NextSprite;
        for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = NextSprite) {
            int SpriteNum = node.getIndex();
            NextSprite = node.getNext();

            // find an open track
            int ndx = 0;
            for (; ndx < MAX_TRACKS; ndx++) {
                if (Track[ndx].NumPoints == 0) {
                    break;
                }
            }

            // save space for 3 points
            Track[ndx].TrackPoint = new TRACK_POINT[4];

            TRACK_POINT[] tp = Track[ndx].TrackPoint;
            TRACK t = Track[ndx];

            // set track type
            t.ttflags |= (BIT(type));
            t.flags = 0;

            // clone point
            int end_sprite =  TrackClonePoint(SpriteNum);
            int start_sprite =  TrackClonePoint(SpriteNum);

            // add start point
            Sprite nsp = boardService.getSprite(start_sprite);
            if (nsp != null) {
                nsp.setLotag(TRACK_START);
                nsp.setHitag(0);
                nsp.setX(nsp.getX() + (64 * EngineUtils.sin(NORM_ANGLE(nsp.getAng() + 1024 + 512)) >> 14));
                nsp.setY(nsp.getY() + (64 * EngineUtils.sin(NORM_ANGLE(nsp.getAng() + 1024)) >> 14));
                TrackAddPoint(t, tp, start_sprite);
            }

            // add jump point
            nsp = node.get();
            nsp.setLotag( lotag);
            TrackAddPoint(t, tp, SpriteNum);

            // add end point
            nsp = boardService.getSprite(end_sprite);
            if (nsp != null) {
                nsp.setX(nsp.getX() + (64 * EngineUtils.sin(NORM_ANGLE(nsp.getAng() + 512)) >> 14));
                nsp.setY(nsp.getY() + (64 * EngineUtils.sin(nsp.getAng()) >> 14));
                nsp.setLotag(TRACK_END);
                nsp.setHitag(0);
                TrackAddPoint(t, tp, end_sprite);
            }
        }
    }

    public static void QuickExitSetup(int stat, int type) {
        ListNode<Sprite> NextSprite;
        for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = NextSprite) {
            int SpriteNum = node.getIndex();
            NextSprite = node.getNext();

            int ndx = 0;
            for (; ndx < MAX_TRACKS; ndx++) {
                if (Track[ndx].NumPoints == 0) {
                    break;
                }
            }

            // save space for 3 points
            Track[ndx].TrackPoint = new TRACK_POINT[4];

            TRACK_POINT[] tp = Track[ndx].TrackPoint;
            TRACK t = Track[ndx];

            // set track type
            t.ttflags |= (BIT(type));
            t.flags = 0;

            // clone point
            int end_sprite =  TrackClonePoint(SpriteNum);
            int start_sprite =  TrackClonePoint(SpriteNum);

            // add start point
            Sprite nsp = boardService.getSprite(start_sprite);
            if (nsp != null) {
                nsp.setLotag(TRACK_START);
                nsp.setHitag(0);
                TrackAddPoint(t, tp, start_sprite);
            }

            KillSprite(SpriteNum);

            // add end point
            nsp = boardService.getSprite(end_sprite);
            if (nsp != null) {
                nsp.setX(nsp.getX() + (1024 * EngineUtils.cos(nsp.getAng()) >> 14));
                nsp.setY(nsp.getY() + (1024 * EngineUtils.sin(nsp.getAng()) >> 14));
                nsp.setLotag(TRACK_END);
                nsp.setHitag(0);
                TrackAddPoint(t, tp, end_sprite);
            }
        }
    }

    public static void QuickLadderSetup(int stat, int lotag, int type) {
        ListNode<Sprite> NextSprite;
        for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = NextSprite) {
            int SpriteNum = node.getIndex();
            NextSprite = node.getNext();
            // find an open track
            int ndx = 0;
            for (; ndx < MAX_TRACKS; ndx++) {
                if (Track[ndx].NumPoints == 0) {
                    break;
                }
            }

            if (ndx == MAX_TRACKS) {
                continue;
            }

            // save space for 3 points
            Track[ndx].TrackPoint = new TRACK_POINT[4];

            TRACK_POINT[] tp = Track[ndx].TrackPoint;
            TRACK t = Track[ndx];

            // set track type
            t.ttflags |= (BIT(type));
            t.flags = 0;

            // clone point
            int end_sprite =  TrackClonePoint(SpriteNum);
            int start_sprite =  TrackClonePoint(SpriteNum);

            // add start point
            Sprite nsp = boardService.getSprite(start_sprite);
            if (nsp != null) {
                nsp.setLotag(TRACK_START);
                nsp.setHitag(0);
                nsp.setX(nsp.getX() + MOVEx(256, nsp.getAng() + 1024));
                nsp.setY(nsp.getY() + MOVEy(256, nsp.getAng() + 1024));
                TrackAddPoint(t, tp, start_sprite);
            }

            // add climb point
            nsp = node.get();
            nsp.setLotag( lotag);
            TrackAddPoint(t, tp, SpriteNum);

            // add end point
            nsp = boardService.getSprite(end_sprite);
            if (nsp != null) {
                nsp.setX(nsp.getX() + MOVEx(512, nsp.getAng()));
                nsp.setY(nsp.getY() + MOVEy(512, nsp.getAng()));
                nsp.setLotag(TRACK_END);
                nsp.setHitag(0);
                TrackAddPoint(t, tp, end_sprite);
            }
        }
    }

    public static void TrackSetup() {
        // put points on track
        for (int ndx = 0; ndx < MAX_TRACKS; ndx++) {
            if (boardService.getStatNode(STAT_TRACK + ndx) == null) {
                // for some reason I need at least one record allocated
                // can't remember why at this point
                if (Track[ndx] == null) {
                    Track[ndx] = new TRACK();
                } else {
                    Track[ndx].reset();
                }
                Track[ndx].TrackPoint = new TRACK_POINT[1];
                continue;
            }

            // make the track array rather large. I'll resize it to correct size
            // later.
            if (Track[ndx] == null) {
                Track[ndx] = new TRACK();
            } else {
                Track[ndx].reset();
            }
            Track[ndx].TrackPoint = new TRACK_POINT[500];

            TRACK_POINT[] tp = Track[ndx].TrackPoint;
            TRACK t = Track[ndx];

            ListNode<Sprite> NextSprite;
            // find the first point and save it
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_TRACK + ndx); node != null; node = NextSprite) {
                int SpriteNum = node.getIndex();
                NextSprite = node.getNext();
                if (LOW_TAG_SPRITE(SpriteNum) == TRACK_START) {
                    TrackAddPoint(t, tp, SpriteNum);
                    break;
                }
            }

            if (t.NumPoints <= 0) {
                continue;
            }

            // set up flags for track types
            if (tp[0].tag_low == TRACK_START && tp[0].tag_high != 0) {
                t.ttflags |= (BIT(tp[0].tag_high));
            }

            // while there are still sprites on this status list
            while (boardService.getStatNode(STAT_TRACK + ndx) != null) {
                int next_sprite = -1;
                int dist, low_dist = 999999;

                // find the closest point to the last point
                for (ListNode<Sprite> node = boardService.getStatNode(STAT_TRACK + ndx); node != null; node = NextSprite) {
                    int SpriteNum = node.getIndex();
                    NextSprite = node.getNext();
                    Sprite sp = node.get();
                    dist = Distance((tp[t.NumPoints - 1]).x, (tp[t.NumPoints - 1]).y, sp.getX(),
                            sp.getY());

                    if (dist < low_dist) {
                        next_sprite = SpriteNum;
                        low_dist = dist;
                    }

                }

                // save the closest one off and kill it
                if (next_sprite != -1) {
                    TrackAddPoint(t, tp, next_sprite);
                }

            }

            int size = (Track[ndx].NumPoints + 1);
            TRACK_POINT[] newp = new TRACK_POINT[size]; // CallocMem(size, 1);
            if (Track[ndx].NumPoints >= 0) System.arraycopy(Track[ndx].TrackPoint, 0, newp, 0, Track[ndx].NumPoints);

            Track[ndx].TrackPoint = newp;
        }

        QuickJumpSetup(STAT_QUICK_JUMP, TRACK_ACTOR_QUICK_JUMP, TT_JUMP_UP);
        QuickJumpSetup(STAT_QUICK_JUMP_DOWN, TRACK_ACTOR_QUICK_JUMP_DOWN, TT_JUMP_DOWN);
        QuickJumpSetup(STAT_QUICK_SUPER_JUMP, TRACK_ACTOR_QUICK_SUPER_JUMP, TT_SUPER_JUMP_UP);
        QuickScanSetup(STAT_QUICK_SCAN, TRACK_ACTOR_QUICK_SCAN, TT_SCAN);
        QuickLadderSetup(STAT_QUICK_LADDER, TRACK_ACTOR_CLIMB_LADDER, TT_LADDER);
        QuickExitSetup(STAT_QUICK_EXIT, TT_EXIT);
        QuickJumpSetup(STAT_QUICK_OPERATE, TRACK_ACTOR_QUICK_OPERATE, TT_OPERATE);
        QuickJumpSetup(STAT_QUICK_DUCK, TRACK_ACTOR_QUICK_DUCK, TT_DUCK_N_SHOOT);
        QuickJumpSetup(STAT_QUICK_DEFEND, TRACK_ACTOR_QUICK_DEFEND, TT_HIDE_N_SHOOT);
    }

    public static int FindBoundSprite(int tag) {
        ListNode<Sprite> next_sn;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ST1); node != null; node = next_sn) {
            int sn = node.getIndex();
            next_sn = node.getNext();
            if (node.get().getHitag() == tag) {
                return sn;
            }
        }

        return -1;
    }

    private static boolean SectorObjectSetupBounds(int sopi) {
        Sector_Object sop = SectorObject[sopi];
        USER u = getUser(sop.sp_child);

        // search for 2 sprite bounding tags
        int BoundSpritei = FindBoundSprite(500 + (sopi * 5));
        Sprite BoundSprite = boardService.getSprite(BoundSpritei);
        if (BoundSprite == null) {
            return false;
        }

        int xlow = BoundSprite.getX();
        int ylow = BoundSprite.getY();

        KillSprite(BoundSpritei);

        BoundSpritei = FindBoundSprite(501 + (sopi * 5));
        BoundSprite = boardService.getSprite(BoundSpritei);
        if (BoundSprite == null) {
            return false;
        }

        int xhigh = BoundSprite.getX();
        int yhigh = BoundSprite.getY();

        KillSprite(BoundSpritei);

        if (u != null) {
            // set radius for explosion checking - based on bounding box
            u.Radius = DIV4((xhigh - xlow) + (yhigh - ylow));
            u.Radius -= DIV4(u.Radius); // trying to get it a good size
        }

        // search for center sprite if it exists

        BoundSpritei = FindBoundSprite(SECT_SO_CENTER);
        BoundSprite = boardService.getSprite(BoundSpritei);
        if (BoundSprite != null) {
            sop.xmid = BoundSprite.getX();
            sop.ymid = BoundSprite.getY();
            sop.zmid = BoundSprite.getZ();
            KillSprite(BoundSpritei);
        }

        // look through all sectors for whole sectors that are IN bounds
        for (int k = 0; k < boardService.getSectorCount(); k++) {
            Sector sec = boardService.getSector(k);
            if (sec == null) {
                continue;
            }

            boolean SectorInBounds = true;
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wall = wn.get();
                // all walls have to be in bounds to be in sector object
                if (!(wall.getX() > xlow && wall.getX() < xhigh && wall.getY() > ylow && wall.getY() < yhigh)) {
                    SectorInBounds = false;
                    break;
                }
            }

            if (SectorInBounds) {
                sop.sector[sop.num_sectors] = k;

                // all sectors in sector object have this flag set - for colision
                // detection and recognition
                sec.setExtra(sec.getExtra() | (SECTFX_SECTOR_OBJECT));

                sop.zorig_floor[sop.num_sectors] = sec.getFloorz();
                sop.zorig_ceiling[sop.num_sectors] = sec.getCeilingz();

                if (TEST(sec.getExtra(), SECTFX_SINK)) {
                    Sect_User su = getSectUser(k);
                    if (su != null) {
                        sop.zorig_floor[sop.num_sectors] += Z(su.depth);
                    }
                }

                // lowest and highest floorz's
                if (sec.getFloorz() > sop.floor_loz) {
                    sop.floor_loz = sec.getFloorz();
                }

                if (sec.getFloorz() < sop.floor_hiz) {
                    sop.floor_hiz = sec.getFloorz();
                }

                sop.num_sectors++;
            }
        }

        //
        // Make sure every sector object has an outer loop tagged - important
        //

        boolean FoundOutsideLoop = false;

        for (int j = 0; sop.sector[j] != -1; j++) {
            Sector sectp = boardService.getSector(sop.sector[j]);
            if (sectp == null) {
                continue;
            }

            // move all walls in sectors
            for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wall = wn.get();
                // for morph point - tornado style
                if (wall.getLotag() == TAG_WALL_ALIGN_SLOPE_TO_POINT) {
                    sop.morph_wall_point = wn.getIndex();
                }

                if (wall.getExtra() != 0 && TEST(wall.getExtra(), WALLFX_LOOP_OUTER)) {
                    FoundOutsideLoop = true;
                }

                // each wall has this set - for collision detection
                wall.setExtra(wall.getExtra() | (WALLFX_SECTOR_OBJECT | WALLFX_DONT_STICK));
                Wall nwall = boardService.getWall(wall.getNextwall());
                if (nwall != null) {
                    nwall.setExtra(nwall.getExtra() | (WALLFX_SECTOR_OBJECT | WALLFX_DONT_STICK));
                }
            }
        }

        if (!FoundOutsideLoop) {
            throw new WarningException("Forgot to tag outer loop for Sector Object #" + sopi);
        }

        ListNode<Sprite> next_sp_num;
        for (short value : StatList) {
            for (ListNode<Sprite> node = boardService.getStatNode(value); node != null; node = next_sp_num) {
                int sp_num = node.getIndex();
                next_sp_num = node.getNext();
                Sprite sp = node.get();

                if (sp.getX() > xlow && sp.getX() < xhigh && sp.getY() > ylow && sp.getY() < yhigh) {
                    // some delete sprites ride others don't
                    if (sp.getStatnum() == STAT_DELETE_SPRITE) {
                        if (!TEST_BOOL2(sp)) {
                            continue;
                        }
                    }

                    if (getUser(sp_num) == null) {
                        u = SpawnUser(sp_num, 0, null);
                    } else {
                        u = getUser(sp_num);
                    }

                    if (u == null) {
                        continue;
                    }

                    u.RotNum = 0;

                    u.ox = sp.getX();
                    u.oy = sp.getY();
                    u.oz = sp.getZ();

                    switch (sp.getStatnum()) {
                        case STAT_WALL_MOVE:

                            break;
                        case STAT_DEFAULT:
                            switch (sp.getHitag()) {
                                case SO_CLIP_BOX: {
                                    sop.clipdist = 0;
                                    sop.clipbox_dist[sop.clipbox_num] = sp.getLotag();
                                    sop.clipbox_xoff[sop.clipbox_num] = (sop.xmid - sp.getX());
                                    sop.clipbox_yoff[sop.clipbox_num] = (sop.ymid - sp.getY());

                                    sop.clipbox_vdist[sop.clipbox_num] = EngineUtils.sqrt(SQ(sop.xmid - sp.getX()) + SQ(sop.ymid - sp.getY()));

                                    int ang2 = EngineUtils.getAngle(sp.getX() - sop.xmid, sp.getY() - sop.ymid);
                                    sop.clipbox_ang[sop.clipbox_num] = GetDeltaAngle(sop.ang, ang2);

                                    sop.clipbox_num++;
                                    KillSprite(sp_num);

                                    continue;
                                }
                                case SO_SHOOT_POINT:
                                    sp.setOwner(-1);
                                    change_sprite_stat(sp_num, STAT_SO_SHOOT_POINT);
                                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                                    break;
                                default:
                                    break;
                            }
                            break;
                    }

                    u.sx = sop.xmid - sp.getX();
                    u.sy = sop.ymid - sp.getY();
                    Sector sec = boardService.getSector(sop.mid_sector);
                    if (sec != null) {
                        u.sz = sec.getFloorz() - sp.getZ();
                    }

                    u.Flags |= (SPR_SO_ATTACHED);

                    u.sang = sp.getAng();
                    u.spal = (byte) sp.getPal();

                    // search SO's sectors to make sure that it is not on a
                    // sector

                    // place all sprites on list
                    int sn = 0;
                    for (; sn < sop.sp_num.length; sn++) {
                        if (sop.sp_num[sn] == -1) {
                            break;
                        }
                    }

                    sop.sp_num[sn] = sp_num;

                    if (!TEST(sop.flags, SOBJ_SPRITE_OBJ)) {
                        // determine if sprite is on a SO sector - set flag if
                        // true
                        for (int j = 0; j < sop.num_sectors; j++) {
                            if (sop.sector[j] == sp.getSectnum()) {
                                u.Flags |= (SPR_ON_SO_SECTOR);
                                Sector spsec = boardService.getSector(sp.getSectnum());
                                if (spsec != null) {
                                    u.sz = spsec.getFloorz() - sp.getZ();
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        // for SPRITE OBJECT sprites, set the u.sz value to the difference
        // between the zmid and the sp.z
        if (TEST(sop.flags, SOBJ_SPRITE_OBJ)) {
            int zmid = -9999999;

            // choose the lowest sprite for the zmid
            for (int i = 0; sop.sp_num[i] != -1; i++) {
                Sprite sp = boardService.getSprite(sop.sp_num[i]);
                if (sp != null && sp.getZ() > zmid) {
                    zmid = sp.getZ();
                }
            }
            sop.zmid = zmid;

            for (int i = 0; sop.sp_num[i] != -1; i++) {
                Sprite sp = boardService.getSprite(sop.sp_num[i]);
                USER uo = getUser(sop.sp_num[i]);
                if (sp != null && uo != null) {
                    uo.sz = sop.zmid - sp.getZ();
                }
            }
        }

        return true;
    }

    public static boolean SetupSectorObject(final int sectnum, int tag) {
        tag -= (TAG_OBJECT_CENTER - 1);

        int object_num = (tag / 5);
        Sector_Object sop = SectorObject[object_num];

        // initialize stuff first time through
        if (sop.num_sectors == -1) {
            Arrays.fill(sop.sector,  -1);

            sop.crush_z = 0;
            sop.drive_angspeed = 0;
            sop.drive_angslide = 0;
            sop.drive_slide = 0;
            sop.drive_speed = 0;
            sop.num_sectors = 0;
            sop.update = 15000;
            sop.flags = 0;
            sop.clipbox_num = 0;
            sop.bob_amt = 0;
            sop.vel_rate = 6;
            sop.z_rate = 256;
            sop.zdelta = sop.z_tgt = 0;
            sop.wait_tics = 0;
            sop.spin_speed = 0;
            sop.spin_ang = 0;
            sop.ang_orig = 0;
            sop.clipdist = 1024;
            sop.target_dist = 0;
            sop.turn_speed = 4;
            sop.floor_loz = -9999999;
            sop.floor_hiz = 9999999;
            sop.player_xoff = sop.player_yoff = 0;
            sop.ang_tgt = sop.ang = sop.ang_moving = 0;
            sop.op_main_sector = -1;
            sop.ram_damage = 0;
            sop.max_damage = -9999;

            sop.scale_type = SO_SCALE_NONE;
            sop.scale_dist = 0;
            sop.scale_speed = 20;
            sop.scale_dist_min = -1024;
            sop.scale_dist_max = 1024;
            sop.scale_rand_freq = 64 >> 3;

            sop.scale_x_mult = 256;
            sop.scale_y_mult = 256;

            sop.morph_ang =  RANDOM_P2(2048);
            sop.morph_z_speed = 20;
            sop.morph_speed = 32;
            sop.morph_dist_max = 1024;
            sop.morph_rand_freq = 64;
            sop.morph_dist = 0;
            sop.morph_xoff = 0;
            sop.morph_yoff = 0;

            sop.PreMoveAnimator = null;
            sop.PostMoveAnimator = null;
            sop.Animator = null;
        }

        if (tag % 5 == TAG_OBJECT_CENTER - 500) {
            sop.mid_sector =  sectnum;
            Vector3i p = SectorMidPoint(sectnum); // , &sop.xmid, &sop.ymid, &sop.zmid);
            sop.xmid = p.x;
            sop.ymid = p.y;
            sop.zmid = p.z;

            sop.dir = 1;
            sop.track =  HIGH_TAG(sectnum);

            // spawn a sprite to make it easier to integrate with sprite routines
            int new1 = SpawnSprite(STAT_SO_SP_CHILD, 0, null, sectnum, sop.xmid, sop.ymid, sop.zmid, 0, 0);
            sop.sp_child = new1;
            USER u = getUser(new1);
            if (u == null) {
                return false;
            }

            u.sop_parent = object_num;
            u.Flags2 |= (SPR2_SPRITE_FAKE_BLOCK); // for damage test

            // check for any ST1 sprites laying on the center sector
            ListNode<Sprite> NextSprite;
            for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = NextSprite) {
                int SpriteNum = node.getIndex();
                NextSprite = node.getNext();
                Sprite sp = node.get();

                if (sp.getStatnum() == STAT_ST1) {
                    switch (sp.getHitag()) {
                        case SO_SCALE_XY_MULT:
                            if (SP_TAG5(sp) != 0) {
                                sop.scale_x_mult =  SP_TAG5(sp);
                            }
                            if (SP_TAG6(sp) != 0) {
                                sop.scale_y_mult =  SP_TAG6(sp);
                            }
                            KillSprite(SpriteNum);
                            break;

                        case SO_SCALE_POINT_INFO:
                            Arrays.fill(sop.scale_point_dist,  0);

                            sop.scale_point_base_speed =  SP_TAG2(sp);
                            for (int j = 0; j < sop.scale_point_speed.length; j++) {
                                sop.scale_point_speed[j] =  SP_TAG2(sp);
                            }

                            if (SP_TAG4(sp) != 0) {
                                sop.scale_point_rand_freq =  SP_TAG4(sp);
                            } else {
                                sop.scale_point_rand_freq = 64;
                            }

                            sop.scale_point_dist_min =  -SP_TAG5(sp);
                            sop.scale_point_dist_max =  SP_TAG6(sp);
                            KillSprite(SpriteNum);
                            break;

                        case SO_SCALE_INFO:
                            sop.flags |= (SOBJ_DYNAMIC);
                            sop.scale_speed =  SP_TAG2(sp);
                            sop.scale_dist_min =  -SP_TAG5(sp);
                            sop.scale_dist_max =  SP_TAG6(sp);

                            sop.scale_type =  SP_TAG4(sp);
                            sop.scale_active_type =  SP_TAG7(sp);

                            if (SP_TAG8(sp) != 0) {
                                sop.scale_rand_freq =  SP_TAG8(sp);
                            } else {
                                sop.scale_rand_freq = 64 >> 3;
                            }

                            if (SP_TAG3(sp) == 0) {
                                sop.scale_dist = sop.scale_dist_min;
                            } else if (SP_TAG3(sp) == 1) {
                                sop.scale_dist = sop.scale_dist_max;
                            }

                            KillSprite(SpriteNum);
                            break;

                        case SPAWN_SPOT:
                            if (sp.getClipdist() == 3) {
                                change_sprite_stat(SpriteNum, STAT_NO_STATE);
                                u = SpawnUser(SpriteNum, 0, null);
                                u.ActorActionFunc = null;
                            }
                            break;

                        case SO_AUTO_TURRET:
                            sop.Animator = SOAnimator.DoAutoTurretObject;
                            KillSprite(SpriteNum);
                            break;

                        case SO_TORNADO:
                            sop.vel = 120;
                            sop.flags |= (SOBJ_DYNAMIC);
                            sop.scale_type = SO_SCALE_CYCLE;
                            // spin stuff
                            sop.spin_speed = 16;
                            sop.last_ang = sop.ang;
                            // animators
                            sop.Animator = SOAnimator.DoTornadoObject;
                            sop.PreMoveAnimator = SOAnimator.ScaleSectorObject;
                            sop.PostMoveAnimator = SOAnimator.MorphTornado;
                            // clip
                            sop.clipdist = 2500;
                            // morph point
                            sop.morph_speed = 16;
                            sop.morph_z_speed = 6;
                            sop.morph_dist_max = 1024;
                            sop.morph_rand_freq = 8;
                            sop.scale_dist_min = -768;
                            KillSprite(SpriteNum);
                            break;
                        case SO_FLOOR_MORPH:
                            sop.flags |= (SOBJ_DYNAMIC);
                            sop.scale_type = SO_SCALE_NONE;
                            sop.morph_speed = 120;
                            sop.morph_z_speed = 7;
                            sop.PostMoveAnimator = SOAnimator.MorphFloor;
                            sop.morph_dist_max = 4000;
                            sop.morph_rand_freq = 8;
                            KillSprite(SpriteNum);
                            break;

                        case SO_AMOEBA:
                            sop.flags |= (SOBJ_DYNAMIC);
                            // sop.scale_type = SO_SCALE_CYCLE;
                            sop.scale_type = SO_SCALE_RANDOM_POINT;
                            sop.PreMoveAnimator = SOAnimator.ScaleSectorObject;
                            Arrays.fill(sop.scale_point_dist,  0);
                            sop.scale_point_base_speed =  SCALE_POINT_SPEED;
                            Arrays.fill(sop.scale_point_speed, SCALE_POINT_SPEED);

                            sop.scale_point_dist_min = -256;
                            sop.scale_point_dist_max = 256;
                            sop.scale_point_rand_freq = 32;
                            KillSprite(SpriteNum);
                            break;
                        case SO_MAX_DAMAGE:
                            u.MaxHealth =  SP_TAG2(sp);
                            if (SP_TAG5(sp) != 0) {
                                sop.max_damage = (short) SP_TAG5(sp);
                            } else {
                                sop.max_damage = (short) u.MaxHealth;
                            }

                            switch (sp.getClipdist()) {
                                case 0:
                                    break;
                                case 1:
                                    sop.flags |= (SOBJ_DIE_HARD);
                                    break;
                            }
                            KillSprite(SpriteNum);
                            break;

                        case SO_DRIVABLE_ATTRIB:

                            sop.drive_angspeed = SP_TAG2(sp);
                            sop.drive_angspeed <<= 5;
                            sop.drive_angslide = SP_TAG3(sp);
                            if (sop.drive_angslide <= 0 || sop.drive_angslide == 32) {
                                sop.drive_angslide = 1;
                            }

                            sop.drive_speed = SP_TAG6(sp);
                            sop.drive_speed <<= 5;
                            sop.drive_slide = SP_TAG7(sp);
                            if (sop.drive_slide <= 0) {
                                sop.drive_slide = 1;
                            }

                            if (TEST_BOOL1(sp)) {
                                sop.flags |= (SOBJ_NO_QUAKE);
                            }

                            if (TEST_BOOL3(sp)) {
                                sop.flags |= (SOBJ_REMOTE_ONLY);
                            }

                            if (TEST_BOOL4(sp)) {
                                sop.crush_z = sp.getZ();
                                sop.flags |= (SOBJ_RECT_CLIP);
                            }
                            break;

                        case SO_RAM_DAMAGE:
                            sop.ram_damage = sp.getLotag();
                            KillSprite(SpriteNum);
                            break;
                        case SECT_SO_CLIP_DIST:
                            sop.clipdist = sp.getLotag();
                            KillSprite(SpriteNum);
                            break;
                        case SECT_SO_SPRITE_OBJ:
                            sop.flags |= (SOBJ_SPRITE_OBJ);
                            KillSprite(SpriteNum);
                            break;
                        case SECT_SO_DONT_ROTATE:
                            sop.flags |= (SOBJ_DONT_ROTATE);
                            KillSprite(SpriteNum);
                            break;
                        case SO_LIMIT_TURN:
                            sop.limit_ang_center = sp.getAng();
                            sop.limit_ang_delta = sp.getLotag();
                            KillSprite(SpriteNum);
                            break;
                        case SO_MATCH_EVENT:
                            sop.match_event = sp.getLotag();
                            sop.match_event_sprite = SpriteNum;
                            break;
                        case SO_SET_SPEED:
                            sop.vel = sp.getLotag() * 256;
                            sop.vel_tgt = sop.vel;
                            KillSprite(SpriteNum);
                            break;
                        case SO_SPIN:
                            if (sop.spin_speed != 0) {
                                break;
                            }
                            sop.spin_speed = sp.getLotag();
                            sop.last_ang = sop.ang;
                            KillSprite(SpriteNum);
                            break;
                        case SO_ANGLE:
                            sop.ang = sop.ang_moving = sp.getAng();
                            sop.last_ang = sop.ang_orig = sop.ang;
                            sop.spin_ang = 0;
                            KillSprite(SpriteNum);
                            break;
                        case SO_SPIN_REVERSE:

                            sop.spin_speed = sp.getLotag();
                            sop.last_ang = sop.ang;

                            if (sop.spin_speed >= 0) {
                                sop.spin_speed =  -sop.spin_speed;
                            }

                            KillSprite(SpriteNum);
                            break;
                        case SO_BOB_START:
                            sop.bob_amt = Z(sp.getLotag());
                            sop.bob_sine_ndx = 0;
                            sop.bob_speed = 4;
                            KillSprite(SpriteNum);
                            break;
                        case SO_TURN_SPEED:
                            sop.turn_speed = sp.getLotag();
                            KillSprite(SpriteNum);
                            break;
                        case SO_SYNC1:
                            sop.flags |= (SOBJ_SYNC1);
                            KillSprite(SpriteNum);
                            break;
                        case SO_SYNC2:
                            sop.flags |= (SOBJ_SYNC2);
                            KillSprite(SpriteNum);
                            break;
                        case SO_KILLABLE:
                            sop.flags |= (SOBJ_KILLABLE);
                            KillSprite(SpriteNum);
                            break;
                    }
                }
            }

            if (sop.vel == -1) {
                sop.vel = sop.vel_tgt = 8 * 256;
            }

            if (!SectorObjectSetupBounds(object_num)) {
                return false;
            }

            if (sop.track >= SO_OPERATE_TRACK_START) {
                switch (sop.track) {
                    case SO_TURRET_MGUN:
                    case SO_TURRET:
                    case SO_TANK:
                        sop.vel = 0;
                        sop.flags |= (SOBJ_OPERATIONAL);
                        break;
                    case SO_SPEED_BOAT:
                        sop.vel = 0;
                        sop.bob_amt = Z(2);
                        sop.bob_speed = 4;
                        sop.flags |= (SOBJ_OPERATIONAL);
                        break;
                    default:
                        sop.flags |= (SOBJ_OPERATIONAL);
                        break;
                }
            }

            Sector sec = boardService.getSector(sectnum);
            if (sec != null) {
                sec.setLotag(0);
                sec.setHitag(0);
            }

            if (sop.max_damage <= 0) {
                VehicleSetSmoke(sop, SpawnVehicleSmoke);
            }
        }

        return true;
    }

    public static void PostSetupSectorObject() {
        for (int s = 0; s < MAX_SECTOR_OBJECTS; s++) {
            Sector_Object sop = SectorObject[s];
            if (sop.xmid == MAXLONG) {
                continue;
            }
            FindMainSector(s);
        }
    }

    public static int PlayerOnObject(int sectnum_match) {
        // place each sector object on the track
        for (int i = 0; (i < MAX_SECTOR_OBJECTS); i++) {
            Sector_Object sop = SectorObject[i];

            if (sop.track < SO_OPERATE_TRACK_START) {
                continue;
            }

            for (int j = 0; j < sop.num_sectors; j++) {
                Sector sec = boardService.getSector(sectnum_match);
                if (sop.sector[j] == sectnum_match && sec != null && TEST(sec.getExtra(), SECTFX_OPERATIONAL)) {
                    return i;
                }
            }
        }

        return (-1);
    }

    public static void PlaceSectorObjectsOnTracks() {
        // place each sector object on the track
        for (int i = 0; i < MAX_SECTOR_OBJECTS; i++) {
            int low_dist = 999999;
            Sector_Object sop = SectorObject[i];

            if (sop.xmid == MAXLONG) {
                continue;
            }

            // save off the original x and y locations of the walls AND sprites
            sop.num_walls = 0;
            for (int j = 0; sop.sector[j] != -1; j++) {
                Sector sec = boardService.getSector(sop.sector[j]);
                if (sec != null) {
                    // move all walls in sectors
                    for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wall = wn.get();
                        sop.xorig[sop.num_walls] = (sop.xmid - wall.getX());
                        sop.yorig[sop.num_walls] = (sop.ymid - wall.getY());
                        sop.num_walls++;
                    }
                }
            }

            if (sop.track <= -1) {
                continue;
            }

            if (sop.track >= SO_OPERATE_TRACK_START) {
                continue;
            }

            boolean found = false;
            // find the closest point on the track and put SOBJ on it
            TRACK_POINT[] tpoint = null;
            for (int j = 0; j < Track[sop.track].NumPoints; j++) {
                tpoint = Track[sop.track].TrackPoint;

                int dist = Distance((tpoint[j]).x, (tpoint[j]).y, sop.xmid, sop.ymid);
                if (dist < low_dist) {
                    low_dist = dist;
                    sop.point = j;
                    found = true;
                }
            }

            if (!found) {
                sop.track = -1;
                continue;
            }

            NextTrackPoint(sop);

            sop.ang = EngineUtils.getAngle((tpoint[sop.point]).x - sop.xmid, (tpoint[sop.point]).y - sop.ymid);

            sop.ang_moving = sop.ang_tgt = sop.ang;
        }
    }

    public static void PlaceActorsOnTracks() {

        // place each actor on the track
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ENEMY); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            int low_dist = 999999;

            Sprite sp = node.get();
            USER u = getUser(i);
            if (u == null) {
                continue;
            }

            int tag =  LOW_TAG_SPRITE(i);
            if (tag < TAG_ACTOR_TRACK_BEGIN || tag > TAG_ACTOR_TRACK_END) {
                continue;
            }

            // setup sprite track defaults
            u.track =  (tag - TAG_ACTOR_TRACK_BEGIN);

            // if facing left go backward
            if (BETWEEN(sp.getAng(), 513, 1535)) {
                u.track_dir = -1;
            } else {
                u.track_dir = 1;
            }

            u.track_vel = sp.getXvel() * 256;
            u.vel_tgt = u.track_vel;
            u.vel_rate = 6;

            // find the closest point on the track and put SOBJ on it
            TRACK_POINT[] tpoint = null;
            for (int j = 0; j < Track[u.track].NumPoints; j++) {
                tpoint = Track[u.track].TrackPoint;

                int dist = Distance((tpoint[j]).x, (tpoint[j]).y, sp.getX(), sp.getY());
                if (dist < low_dist) {
                    low_dist = dist;
                    u.point = j;
                }
            }

            NextActorTrackPoint(i);

            // check angle in the "forward" direction
            if (tpoint != null) {
                sp.setAng(EngineUtils.getAngle((tpoint[u.point]).x - sp.getX(), (tpoint[u.point]).y - sp.getY()));
            }
        }
    }

    public static void MovePlayer(PlayerStr pp, int sopi, int nx, int ny) {
        // make sure your standing on the so
        if (TEST(pp.Flags, PF_JUMPING | PF_FALLING | PF_FLYING)) {
            return;
        }

        Sector_Object sop = SectorObject[sopi];

        pp.sop_riding = sopi;

        // if player has NOT moved and player is NOT riding
        // set up the player for riding
        if (!TEST(pp.Flags, PF_PLAYER_MOVED) && !TEST(pp.Flags, PF_PLAYER_RIDING)) {
            pp.Flags |= (PF_PLAYER_RIDING);

            pp.RevolveAng = pp.getAnglei();
            pp.RevolveX = pp.posx;
            pp.RevolveY = pp.posy;

            // set the delta angle to 0 when moving
            pp.RevolveDeltaAng = 0;
        }

        pp.posx += (nx);
        pp.posy += (ny);

        if (TEST(sop.flags, SOBJ_DONT_ROTATE)) {
            UpdatePlayerSprite(pp);
            return;
        }

        if (TEST(pp.Flags, PF_PLAYER_MOVED)) {
            // Player is moving

            // save the current information so when Player stops
            // moving then you
            // know where he was last
            pp.RevolveAng = pp.getAnglei();
            pp.RevolveX = pp.posx;
            pp.RevolveY = pp.posy;

            // set the delta angle to 0 when moving
            pp.RevolveDeltaAng = 0;
        } else {
            // Player is NOT moving

            // Move saved x&y variables
            pp.RevolveX += (nx);
            pp.RevolveY += (ny);

            // Last known angle is now adjusted by the delta angle
            pp.RevolveAng = NORM_ANGLE(pp.getAnglei() - pp.RevolveDeltaAng);
        }

        // increment Players delta angle
        pp.RevolveDeltaAng = NORM_ANGLE(pp.RevolveDeltaAng + GlobSpeedSO);

        Point p = EngineUtils.rotatepoint(sop.xmid, sop.ymid, pp.RevolveX, pp.RevolveY, pp.RevolveDeltaAng);
        pp.posx = p.getX();
        pp.posy = p.getY();
        // THIS WAS CAUSING PROLEMS!!!!
        // Sectors are still being manipulated so you can end up in a void (-1) sector
        // COVERupdatesector(pp.posx, pp.posy, &pp.cursectnum);

        // New angle is formed by taking last known angle and
        // adjusting by the delta angle
        pp.pang = NORM_ANGLE(pp.RevolveAng + pp.RevolveDeltaAng);

        UpdatePlayerSprite(pp);
    }

    public static void MovePoints(int sopi, int delta_ang, int nx, int ny, int skip) {
        boolean PlayerMove = true;

        Sector_Object sop = SectorObject[sopi];

        if (sop.xmid == MAXSO) {
            PlayerMove = false;
        }

        // move along little midpoint
        sop.xmid += (nx);
        sop.ymid += (ny);

        if (sop.xmid == MAXSO) {
            PlayerMove = false;
        }

        Sprite childSp = boardService.getSprite(sop.sp_child);
        if (childSp == null) {
            return;
        }

        // move child sprite along also
        childSp.setX(sop.xmid);
        childSp.setY(sop.ymid);

        // setting floorz if need be
        Sector midSec = boardService.getSector(sop.mid_sector);
        if (midSec != null && TEST(sop.flags, SOBJ_ZMID_FLOOR)) {
            sop.zmid = midSec.getFloorz();
        }

        for (int j = 0; sop.sector[j] != -1; j++) {
            Sector sectp = boardService.getSector(sop.sector[j]);

            if (sectp != null && !TEST(sop.flags, SOBJ_SPRITE_OBJ | SOBJ_DONT_ROTATE)) {
                // move all walls in sectors
                for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wp = wn.get();
                    if (TEST(wp.getExtra(), WALLFX_LOOP_DONT_SPIN | WALLFX_DONT_MOVE)) {
                        continue;
                    }

                    if (wp.getExtra() != 0 && TEST(wp.getExtra(), WALLFX_LOOP_OUTER)) {
                        int x = wp.getX() + nx;
                        int y = wp.getY() + ny;
                        wp.setX(x);
                        wp.setY(y);
                        engine.dragpoint(skip, wn.getIndex(), x, y);
                    } else {
                        engine.getInterpolation(skip).setwallinterpolate(wn.getIndex(), wp);
                        wp.setX(wp.getX() + nx);
                        wp.setY(wp.getY() + ny);
                    }

                    int rot_ang = delta_ang;
                    if (TEST(wp.getExtra(), WALLFX_LOOP_REVERSE_SPIN)) {
                        rot_ang =  -delta_ang;
                    }

                    if (TEST(wp.getExtra(), WALLFX_LOOP_SPIN_2X)) {
                        rot_ang = NORM_ANGLE(rot_ang * 2);
                    }

                    if (TEST(wp.getExtra(), WALLFX_LOOP_SPIN_4X)) {
                        rot_ang = NORM_ANGLE(rot_ang * 4);
                    }

                    Point p = EngineUtils.rotatepoint(sop.xmid, sop.ymid, wp.getX(), wp.getY(), rot_ang);
                    int rx = p.getX();
                    int ry = p.getY();

                    if (wp.getExtra() != 0 && TEST(wp.getExtra(), WALLFX_LOOP_OUTER)) {
                        engine.dragpoint(skip, wn.getIndex(), rx, ry);
                    } else {
                        engine.getInterpolation(skip).setwallinterpolate(wn.getIndex(), wp);
                        wp.setX(rx);
                        wp.setY(ry);
                    }
                }
            }

            for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                PlayerStr pp = Player[pnum];

                // if controlling a sector object
                if (pp.sop != -1) {
                    continue;
                }

                Sector loSect = boardService.getSector(pp.lo_sectp);
                if (loSect == null) {
                    continue;
                }

                if (TEST(loSect.getExtra(), SECTFX_NO_RIDE)) {
                    continue;
                }

                // move the player
                if (pp.lo_sectp == sop.sector[j]) {
                    if (PlayerMove) {
                        MovePlayer(pp, sopi, nx, ny);
                    }
                }
            }
        }

        for (int i = 0; sop.sp_num[i] != -1; i++) {
            Sprite sp = boardService.getSprite(sop.sp_num[i]);
            USER u = getUser(sop.sp_num[i]);
            if (sp == null || u == null) {
                continue;
            }

            engine.getInterpolation(skip).setsprinterpolate(sop.sp_num[i], sp);

            // if its a player sprite || NOT attached
            if (u.PlayerP != -1 || !TEST(u.Flags, SPR_SO_ATTACHED)) {
                continue;
            }

            // move the player
            for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                PlayerStr pp = Player[pnum];

                if (pp.lo_sp != -1 && pp.lo_sp == sop.sp_num[i]) {
                    if (PlayerMove) {
                        MovePlayer(pp, sopi, nx, ny);
                    }
                }
            }

            sp.setX(sop.xmid - u.sx);
            sp.setY(sop.ymid - u.sy);

            // sprites z update
            if (TEST(sop.flags, SOBJ_SPRITE_OBJ)) {
                // Sprite Objects follow zmid
                sp.setZ(sop.zmid - u.sz);
            } else {
                // Sector Objects can either have sprites ON or OFF of the sector
                Sector sec;
                if (TEST(u.Flags, SPR_ON_SO_SECTOR)) {
                    // move with sector its on
                    sec = boardService.getSector(sp.getSectnum());
                } else {
                    // move with the mid sector
                    sec = boardService.getSector(sop.mid_sector);
                }

                if (sec != null) {
                    sp.setZ(sec.getFloorz() - u.sz);
                }
            }

            sp.setAng(u.sang);

            if (TEST(u.Flags, SPR_ON_SO_SECTOR)) {
                if (TEST(sop.flags, SOBJ_DONT_ROTATE)) {
                    continue;
                }

                Sector sec = boardService.getSector(sp.getSectnum());
                if (sec == null || sec.getWallNode() == null) {
                    continue;
                }

                // IS part of a sector - sprite can do things based on the
                // current sector it is in
                if (TEST(sec.getWallNode().get().getExtra(), WALLFX_LOOP_DONT_SPIN)) {
                    continue;
                }

                if (TEST(sec.getWallNode().get().getExtra(), WALLFX_LOOP_REVERSE_SPIN)) {
                    Point p = EngineUtils.rotatepoint(sop.xmid, sop.ymid, sp.getX(), sp.getY(),  -delta_ang);
                    sp.setX(p.getX());
                    sp.setY(p.getY());
                    sp.setAng(NORM_ANGLE(sp.getAng() - delta_ang));
                } else {
                    Point p = EngineUtils.rotatepoint(sop.xmid, sop.ymid, sp.getX(), sp.getY(), delta_ang);
                    sp.setX(p.getX());
                    sp.setY(p.getY());
                    sp.setAng(NORM_ANGLE(sp.getAng() + delta_ang));
                }

            } else {
                if (!TEST(sop.flags, SOBJ_DONT_ROTATE)) {
                    // NOT part of a sector - independant of any sector
                    Point p = EngineUtils.rotatepoint(sop.xmid, sop.ymid, sp.getX(), sp.getY(), delta_ang);
                    sp.setX(p.getX());
                    sp.setY(p.getY());
                    sp.setAng(NORM_ANGLE(sp.getAng() + delta_ang));
                }

                // Does not necessarily move with the sector so must accout for
                // moving across sectors
                if (sop.xmid < (long) MAXSO) // special case for operating SO's
                {
                    engine.setspritez(sop.sp_num[i], sp.getX(), sp.getY(), sp.getZ());
                }
            }

            if (TEST(sp.getExtra(), SPRX_BLADE)) {
                DoBladeDamage(sop.sp_num[i]);
            }
        }

        for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            PlayerStr pp = Player[pnum];

            // if player was on a sector object
            if (pp.sop_riding != -1) {
                // update here AFTER sectors/player has been manipulated
                // prevents you from falling into map HOLEs created by moving
                // Sectors and sprites around.
                // if (sop.xmid < (long)MAXSO)
                pp.cursectnum = COVERupdatesector(pp.posx, pp.posy, pp.cursectnum);
                Sprite psp = pp.getSprite();
                if (psp != null) {
                    // in case you are in a whirlpool
                    // move perfectly with the ride in the z direction
                    if (TEST(pp.Flags, PF_CRAWLING)) {
                        // move up some for really fast moving plats
                        // pp.posz -= PLAYER_HEIGHT + Z(12);
                        DoPlayerZrange(pp);
                        pp.posz = pp.loz - PLAYER_CRAWL_HEIGHT;
                        psp.setZ(pp.loz);
                    } else {
                        // move up some for really fast moving plats
                        // pp.posz -= Z(24);
                        DoPlayerZrange(pp);

                        if (!TEST(pp.Flags, PF_JUMPING | PF_FALLING | PF_FLYING)) {
                            pp.posz = pp.loz - PLAYER_HEIGHT;
                            psp.setZ(pp.loz);
                        }
                    }
                }
            } else {
                // if player was not on any sector object set Riding flag to false
                pp.Flags &= ~(PF_PLAYER_RIDING);
            }
        }
    }

    public static void RefreshPoints(int sopi, int nx, int ny, boolean dynamic, int skip) {
        int wallcount = 0;

        Sector_Object sop = SectorObject[sopi];
        // do scaling
        if (dynamic && sop.PreMoveAnimator != null) {
            sop.PreMoveAnimator.invoke(sopi);
        }

        for (int j = 0; sop.sector[j] != -1; j++) {
            Sector sectp = boardService.getSector(sop.sector[j]);

            if (sectp != null && !TEST(sop.flags, SOBJ_SPRITE_OBJ)) {
                // move all walls in sectors back to the original position
                for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wp = wn.get();

                    if (!(wp.getExtra() != 0 && TEST(wp.getExtra(), WALLFX_DONT_MOVE))) {
                        int x = sop.xmid - sop.xorig[wallcount];
                        int y = sop.ymid - sop.yorig[wallcount];
                        int dx = x;
                        int dy = y;

                        if (dynamic && sop.scale_type != 0) {
                            if (!TEST(wp.getExtra(), WALLFX_DONT_SCALE)) {
                                int ang = NORM_ANGLE(EngineUtils.getAngle(x - sop.xmid, y - sop.ymid));

                                if (sop.scale_type == SO_SCALE_RANDOM_POINT) {
                                    // was causing memory overwrites
                                    Vector2i d = ScaleRandomPoint(sop, wallcount, ang, x, y);
                                    dx = d.x;
                                    dy = d.y;
                                } else {
                                    int xmul = (sop.scale_dist * sop.scale_x_mult) >> 8;
                                    int ymul = (sop.scale_dist * sop.scale_y_mult) >> 8;

                                    dx = x + ((xmul * EngineUtils.sin(NORM_ANGLE(ang + 512))) >> 14);
                                    dy = y + ((ymul * EngineUtils.sin(ang)) >> 14);
                                }
                            }
                        }

                        if (wp.getExtra() != 0 && TEST(wp.getExtra(), WALLFX_LOOP_OUTER)) {
                            engine.dragpoint(skip, wn.getIndex(), dx, dy);
                        } else {
                            engine.getInterpolation(skip).setwallinterpolate(wn.getIndex(), wp);
                            wp.setX(dx);
                            wp.setY(dy);
                        }
                    }

                    wallcount++;
                }
            }
        }

        int delta_ang_from_orig;
        if (sop.spin_speed != 0) {
            // same as below - ignore the objects angle
            // last_ang is the last true angle before SO started spinning
            delta_ang_from_orig = NORM_ANGLE(sop.last_ang + sop.spin_ang - sop.ang_orig);
        } else {
            // angle traveling + the new spin angle all offset from the original
            // angle
            delta_ang_from_orig = NORM_ANGLE(sop.ang + sop.spin_ang - sop.ang_orig);
        }

        // Note that this delta angle is from the original angle
        // nx,ny are 0 so the points are not moved, just rotated
        MovePoints(sopi, delta_ang_from_orig, nx, ny, skip);

        // do morphing - angle independent
        if (dynamic && sop.PostMoveAnimator != null) {
            sop.PostMoveAnimator.invoke(sopi);
        }
    }

    public static void KillSectorObjectSprites(Sector_Object sop) {
        for (int i = 0; sop.sp_num[i] != -1; i++) {
            Sprite sp = boardService.getSprite(sop.sp_num[i]);
            USER u = getUser(sop.sp_num[i]);
            if (sp == null || u == null) {
                continue;
            }

            // not a part of the so anymore
            u.Flags &= ~(SPR_SO_ATTACHED);
            if (sp.getPicnum() == ST1 && sp.getHitag() == SPAWN_SPOT) {
                continue;
            }

            KillSprite(sop.sp_num[i]);
        }

        // clear the list
        sop.sp_num[0] = -1;
    }

    public static int DetectSectorObject(Sector sectph) {
        // collapse the SO to a single point
        // move all points to nx,ny
        for (int s = 0; s < MAX_SECTOR_OBJECTS; s++) {
            Sector_Object sop = SectorObject[s];
            if (sop.xmid == MAXSO) {
                continue;
            }

            for (int j = 0; sop.sector[j] != -1; j++) {
                Sector sectp = boardService.getSector(sop.sector[j]);
                if (sectph == sectp) {
                    return s;
                }
            }
        }

        return -1;
    }

    public static Sector_Object DetectSectorObjectByWall(Wall wph) {
        // collapse the SO to a single point
        // move all points to nx,ny
        for (int s = 0; s < MAX_SECTOR_OBJECTS; s++) {
            Sector_Object sop = SectorObject[s];
            if (sop.xmid == MAXSO) {
                continue;
            }

            for (int j = 0; sop.sector[j] != -1; j++) {
                Sector sectp = boardService.getSector(sop.sector[j]);
                if (sectp != null) {
                    for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wp = wn.get();
                        // if outer wall check the NEXTWALL also
                        if (TEST(wp.getExtra(), WALLFX_LOOP_OUTER)) {
                            if (wph == boardService.getWall(wp.getNextwall())) {
                                return (sop);
                            }
                        }

                        if (wph == wp) {
                            return (sop);
                        }
                    }
                }
            }
        }

        return null;
    }

    public static void CollapseSectorObject(Sector_Object sop, int nx, int ny) {
        // collapse the SO to a single point
        // move all points to nx,ny
        for (int j = 0; sop.sector[j] != -1; j++) {
            Sector sectp = boardService.getSector(sop.sector[j]);

            if (sectp != null && !TEST(sop.flags, SOBJ_SPRITE_OBJ)) {
                // move all walls in sectors back to the original position
                for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wp = wn.get();
                    if (TEST(wp.getExtra(), WALLFX_DONT_MOVE)) {
                        continue;
                    }

                    if (wp.getExtra() != 0 && TEST(wp.getExtra(), WALLFX_LOOP_OUTER)) {
                        engine.dragpoint(wn.getIndex(), nx, ny);
                    } else {
                        wp.setX(nx);
                        wp.setY(ny);
                    }
                }
            }
        }
    }

    public static void MoveZ(Sector_Object sop) {
        if (sop.bob_amt != 0) {
            sop.bob_sine_ndx =  ((totalsynctics << sop.bob_speed) & 2047);
            sop.bob_diff = ((sop.bob_amt * EngineUtils.sin(sop.bob_sine_ndx)) >> 14);

            // for all sectors
            for (int i = 0; sop.sector[i] != -1; i++) {
                Sector sectp = boardService.getSector(sop.sector[i]);
                if (sectp == null) {
                    continue;
                }

                Sect_User su = getSectUser(sop.sector[i]);
                if (su != null && TEST(su.flags, SECTFU_SO_DONT_BOB)) {
                    continue;
                }

                sectp.setFloorz(sop.zorig_floor[i] + sop.bob_diff);
            }
        }

        if (TEST(sop.flags, SOBJ_MOVE_VERTICAL)) {
            int i =  AnimGetGoal(sop, AnimType.SectorObjectZ);
            if (i < 0) {
                sop.flags &= ~(SOBJ_MOVE_VERTICAL);
            }
        }

        if (TEST(sop.flags, SOBJ_ZDIFF_MODE)) {
            return;
        }

        // move all floors
        if (TEST(sop.flags, SOBJ_ZDOWN)) {
            for (int i = 0; sop.sector[i] != -1; i++) {
                AnimSet(sop.sector[i], sop.zorig_floor[i] + sop.z_tgt, sop.z_rate, AnimType.FloorZ);
            }

            sop.flags &= ~(SOBJ_ZDOWN);
        } else if (TEST(sop.flags, SOBJ_ZUP)) {
            for (int i = 0; sop.sector[i] != -1; i++) {
                AnimSet(sop.sector[i], sop.zorig_floor[i] + sop.z_tgt, sop.z_rate, AnimType.FloorZ);
            }

            sop.flags &= ~(SOBJ_ZUP);
        }
    }

    public static void CallbackSOsink(Anim ap, Sector_Object data) {
        int dest_sector = -1;
        int src_sector = -1;

        for (int i = 0; data.sector[i] != -1; i++) {
            Sect_User su = getSectUser(data.sector[i]);
            if (su != null && TEST(su.flags, SECTFU_SO_SINK_DEST)) {
                src_sector = data.sector[i];
                break;
            }
        }

        for (int i = 0; data.sector[i] != -1; i++) {
            if (ap.ptr == boardService.getSector(data.sector[i]) && ap.index == data.sector[i] && ap.type == AnimType.FloorZ) {
                dest_sector = data.sector[i];
                break;
            }
        }

        Sector dest = boardService.getSector(dest_sector);
        Sector src = boardService.getSector(src_sector);
        if (dest == null || src == null) {
            return;
        }

        dest.setFloorpicnum(src.getFloorpicnum());
        dest.setFloorshade(src.getFloorshade());

        dest.setFloorstat(dest.getFloorstat() & ~(FLOOR_STAT_RELATIVE));

        int tgt_depth = (SetupSectUser(src_sector)).depth;
        for (int sectnum = 0; sectnum < boardService.getSectorCount(); sectnum++) {
            if (sectnum == dest_sector) {
                // This is interesting
                // Added a depth_fract to the struct so I could do a
                // 16.16 Fixed point representation to change the depth
                // in a more precise way
                int ndx =  AnimSet(sectnum, tgt_depth << 16, (ap.vel << 8) >> 8, AnimType.SectUserDepth);
                AnimSetVelAdj(ndx, ap.vel_adj);
                break;
            }
        }

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getSectNode(dest_sector); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();
            USER u = getUser(i);

            if (u == null || u.PlayerP != -1 || !TEST(u.Flags, SPR_SO_ATTACHED)) {
                continue;
            }

            // move sprite WAY down in water
            int ndx =  AnimSet(i, -u.sz - SPRITEp_SIZE_Z(sp) - Z(100), ap.vel >> 8, AnimType.UserZ);
            AnimSetVelAdj(ndx, ap.vel_adj);
        }

        // Take out any blocking walls
        for (ListNode<Wall> wn = dest.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall wall = wn.get();
            wall.setCstat(wall.getCstat() & ~(CSTAT_WALL_BLOCK));
        }
    }

    public static void MoveSectorObjects(int sopi, int locktics, int skip) {
        Sector_Object sop = SectorObject[sopi];
        if (sop.track >= SO_OPERATE_TRACK_START) {
            if (TEST(sop.flags, SOBJ_UPDATE_ONCE)) {
                sop.flags &= ~(SOBJ_UPDATE_ONCE);
                RefreshPoints(sopi, 0, 0, false, skip);
            }
            return;
        }

        int nx = 0;
        int ny = 0;

        // if pausing the return
        if (sop.wait_tics != 0) {
            sop.wait_tics -= locktics;
            if (sop.wait_tics <= 0) {
                sop.wait_tics = 0;
            }

            return;
        }

        if (sop.track > -1) {
            DoTrack(sopi, locktics, tmp_ptr[0].set(0), tmp_ptr[1].set(0));
            nx = tmp_ptr[0].value;
            ny = tmp_ptr[1].value;
        }

        // get delta to target angle
        int delta_ang = GetDeltaAngle(sop.ang_tgt, sop.ang);
        sop.ang = NORM_ANGLE(sop.ang + (delta_ang >> sop.turn_speed));
        delta_ang =  (delta_ang >> sop.turn_speed);

        // move z values
        MoveZ(sop);

        // calculate the spin speed
        int speed =  (sop.spin_speed * locktics);
        // spin_ang is incremented by the spin_speed
        sop.spin_ang = NORM_ANGLE(sop.spin_ang + speed);

        if (sop.spin_speed != 0) {
            // ignore delta angle if spinning
            GlobSpeedSO = speed;
        } else {
            // The actual delta from the last frame
            GlobSpeedSO = speed;
            GlobSpeedSO += delta_ang;
        }

        if (TEST(sop.flags, SOBJ_DYNAMIC)) {
            // trick tricks
            RefreshPoints(sopi, nx, ny, true, skip);
        } else {
            // Update the points so there will be no warping
            if (TEST(sop.flags, SOBJ_UPDATE | SOBJ_UPDATE_ONCE) || sop.vel != 0 || (sop.ang != sop.ang_tgt)
                    || GlobSpeedSO != 0) {
                sop.flags &= ~(SOBJ_UPDATE_ONCE);
                RefreshPoints(sopi, nx, ny, false, skip);
            }
        }
    }

    public static void DoTrack(int sopi, int locktics, LONGp nx, LONGp ny) {
        Sector_Object sop = SectorObject[sopi];
        TRACK_POINT tpoint = Track[sop.track].TrackPoint[sop.point];

        // calculate an angle to the target

        if (sop.vel != 0) {
            sop.ang_moving = sop.ang_tgt = EngineUtils.getAngle(tpoint.x - sop.xmid, tpoint.y - sop.ymid);
        }

        // NOTE: Jittery ride - try new value out here
        // NOTE: Put a loop around this (locktics) to make it more acuruate

        if (sop.target_dist < 100) {
            switch (tpoint.tag_low) {
                case TRACK_MATCH_EVERYTHING:
                    DoMatchEverything(null, tpoint.tag_high, -1);
                    break;

                case TRACK_MATCH_EVERYTHING_ONCE:
                    DoMatchEverything(null, tpoint.tag_high, -1);
                    tpoint.tag_low = 0;
                    tpoint.tag_high = 0;
                    break;

                case TRACK_SPIN:
                    if (sop.spin_speed != 0) {
                        break;
                    }

                    sop.spin_speed = tpoint.tag_high;
                    sop.last_ang = sop.ang;
                    break;

                case TRACK_SPIN_REVERSE: {
                    if (sop.spin_speed == 0) {
                        break;
                    }

                    if (sop.spin_speed >= 0) {
                        sop.spin_speed =  -sop.spin_speed;
                    }
                }
                break;

                case TRACK_SPIN_STOP:
                    if (sop.spin_speed == 0) {
                        break;
                    }

                    sop.spin_speed = 0;
                    break;

                case TRACK_BOB_START:
                    sop.flags |= (SOBJ_ZMID_FLOOR);
                    sop.bob_amt = Z(tpoint.tag_high);
                    sop.bob_sine_ndx = 0;
                    sop.bob_speed = 4;
                    break;

                case TRACK_BOB_STOP:
                    sop.bob_speed = 0;
                    sop.bob_sine_ndx = 0;
                    sop.bob_amt = 0;
                    break;

                case TRACK_BOB_SPEED:
                    sop.bob_speed = tpoint.tag_high;
                    break;

                case TRACK_REVERSE:
                    sop.dir *= -1;
                    break;
                case TRACK_STOP:
                    sop.vel = 0;
                    sop.wait_tics = (short) (tpoint.tag_high * 128);
                    break;
                case TRACK_SET_SPEED:
                    sop.vel = tpoint.tag_high * 256;
                    sop.vel_tgt = sop.vel;
                    break;

                //
                // Controls the velocity
                //

                case TRACK_VEL_RATE:
                    sop.vel_rate = tpoint.tag_high;
                    break;
                case TRACK_SPEED_UP:
                    sop.flags &= ~(SOBJ_SLOW_DOWN | SOBJ_SPEED_UP);
                    if (sop.dir < 0) {
                        // set target to new slower target
                        sop.vel_tgt = sop.vel_tgt - (tpoint.tag_high * 256);
                        sop.flags |= (SOBJ_SLOW_DOWN);
                    } else {
                        sop.vel_tgt = sop.vel_tgt + (tpoint.tag_high * 256);
                        sop.flags |= (SOBJ_SPEED_UP);
                    }

                    break;

                case TRACK_SLOW_DOWN:
                    sop.flags &= ~(SOBJ_SLOW_DOWN | SOBJ_SPEED_UP);
                    if (sop.dir > 0) {
                        sop.vel_tgt = sop.vel_tgt - (tpoint.tag_high * 256);
                        sop.flags |= (SOBJ_SLOW_DOWN);
                    } else {
                        sop.vel_tgt = sop.vel_tgt + (tpoint.tag_high * 256);
                        sop.flags |= (SOBJ_SPEED_UP);
                    }
                    break;

                //
                // Controls z
                //

                case TRACK_SO_SINK: {
                    int dest_sector = -1;
                    for (int i = 0; sop.sector[i] != -1; i++) {
                        Sect_User su = getSectUser(sop.sector[i]);
                        if (su != null && TEST(su.flags, SECTFU_SO_SINK_DEST)) {
                            dest_sector = sop.sector[i];
                            break;
                        }
                    }

                    Sector dest = boardService.getSector(dest_sector);
                    if (dest == null) {
                        break;
                    }

                    sop.bob_speed = 0;
                    sop.bob_sine_ndx = 0;
                    sop.bob_amt = 0;

                    for (int i = 0; sop.sector[i] != -1; i++) {
                        Sect_User su = getSectUser(sop.sector[i]);
                        if (su != null && TEST(su.flags, SECTFU_SO_DONT_SINK)) {
                            continue;
                        }

                        int ndx = AnimSet(sop.sector[i], dest.getFloorz(), tpoint.tag_high, AnimType.FloorZ);
                        AnimSetCallback(ndx, AnimCallback.SOsink, sopi);
                        AnimSetVelAdj(ndx, 6);
                    }

                    break;
                }

                case TRACK_SO_FORM_WHIRLPOOL: {
                    // for lowering the whirlpool in level 1
                    for (int i = 0; sop.sector[i] != -1; i++) {
                        Sector sectp = boardService.getSector(sop.sector[i]);
                        Sect_User sectu = getSectUser(sop.sector[i]);

                        if (sectp != null && sectu != null && sectu.stag == SECT_SO_FORM_WHIRLPOOL) {
                            AnimSet(sop.sector[i], sectp.getFloorz() + Z(sectu.height), 128, AnimType.FloorZ);
                            sectp.setFloorshade(sectp.getFloorshade() + sectu.height / 6);
                            sectp.setExtra((sectp).getExtra() & ~(SECTFX_NO_RIDE));
                        }
                    }
                    break;
                }

                case TRACK_MOVE_VERTICAL: {
                    sop.flags |= (SOBJ_MOVE_VERTICAL);

                    int zr = 256;
                    if (tpoint.tag_high > 0) {
                        zr = tpoint.tag_high;
                    }

                    // look at the next point
                    NextTrackPoint(sop);
                    tpoint = Track[sop.track].TrackPoint[sop.point];

                    // set anim
                    AnimSet(sopi, tpoint.z, zr, AnimType.SectorObjectZ);

                    // move back to current point by reversing direction
                    sop.dir *= -1;
                    NextTrackPoint(sop);
                    sop.dir *= -1;

                    break;
                }

                case TRACK_WAIT_FOR_EVENT: {
                    if (tpoint.tag_high == -1) {
                        break;
                    }

                    sop.flags |= (SOBJ_WAIT_FOR_EVENT);
                    sop.save_vel =  sop.vel;
                    sop.save_spin_speed = sop.spin_speed;

                    sop.vel = sop.spin_speed = 0;
                    // only set event if non-zero
                    if (tpoint.tag_high != 0) {
                        sop.match_event = tpoint.tag_high;
                    }
                    tpoint.tag_high = -1;
                    break;
                }

                case TRACK_ZDIFF_MODE:
                    sop.flags |= (SOBJ_ZDIFF_MODE);
                    sop.zdelta = Z(tpoint.tag_high);
                    break;
                case TRACK_ZRATE:
                    sop.z_rate = Z(tpoint.tag_high);
                    break;
                case TRACK_ZUP:
                    sop.flags &= ~(SOBJ_ZDOWN | SOBJ_ZUP);
                    if (sop.dir < 0) {
                        sop.z_tgt = sop.z_tgt + Z(tpoint.tag_high);
                        sop.flags |= (SOBJ_ZDOWN);
                    } else {
                        sop.z_tgt = sop.z_tgt - Z(tpoint.tag_high);
                        sop.flags |= (SOBJ_ZUP);
                    }
                    break;
                case TRACK_ZDOWN:
                    sop.flags &= ~(SOBJ_ZDOWN | SOBJ_ZUP);
                    if (sop.dir > 0) {
                        sop.z_tgt = sop.z_tgt + Z(tpoint.tag_high);
                        sop.flags |= (SOBJ_ZDOWN);
                    } else {
                        sop.z_tgt = sop.z_tgt - Z(tpoint.tag_high);
                        sop.flags |= (SOBJ_ZUP);
                    }
                    break;
            }

            // get the next point
            NextTrackPoint(sop);
            tpoint = Track[sop.track].TrackPoint[sop.point];

            // calculate distance to target poing
            sop.target_dist = Distance(sop.xmid, sop.ymid, tpoint.x, tpoint.y);

            // calculate a new angle to the target
            sop.ang_moving = sop.ang_tgt = EngineUtils.getAngle(tpoint.x - sop.xmid, tpoint.y - sop.ymid);

            if (TEST(sop.flags, SOBJ_ZDIFF_MODE)) {
                // set dx,dy,dz up for finding the z magnitude
                int dx = tpoint.x;
                int dy = tpoint.y;
                int dz = tpoint.z - sop.zdelta;

                // find the distance to the target (player)
                int dist = DIST(dx, dy, sop.xmid, sop.ymid);

                // (velocity * difference between the target and the object)
                // / distance
                sop.z_rate = (sop.vel * (sop.zmid - dz)) / dist;

                // take absolute value and convert to pixels (divide by 256)
                sop.z_rate = PIXZ(klabs(sop.z_rate));

                if (TEST(sop.flags, SOBJ_SPRITE_OBJ)) {
                    // only modify zmid for sprite_objects
                    AnimSet(sopi, dz, sop.z_rate, AnimType.SectorObjectZ);
                } else {
                    // churn through sectors setting their new z values
                    for (int i = 0; sop.sector[i] != -1; i++) {
                        Sector sec = boardService.getSector(sop.mid_sector);
                        Sector sec2 = boardService.getSector(sop.sector[i]);
                        if (sec != null && sec2 != null) {
                            AnimSet(sop.sector[i], dz - (sec.getFloorz() - sec2.getFloorz()), sop.z_rate, AnimType.FloorZ);
                        }
                    }
                }
            }
        } else {

            // make velocity approach the target velocity
            if (TEST(sop.flags, SOBJ_SPEED_UP)) {
                if ((sop.vel += (locktics << sop.vel_rate)) >= sop.vel_tgt) {
                    sop.vel = sop.vel_tgt;
                    sop.flags &= ~(SOBJ_SPEED_UP);
                }
            } else if (TEST(sop.flags, SOBJ_SLOW_DOWN)) {
                if ((sop.vel -= (locktics << sop.vel_rate)) <= sop.vel_tgt) {
                    sop.vel = sop.vel_tgt;
                    sop.flags &= ~(SOBJ_SLOW_DOWN);
                }
            }
        }

        // calculate a new x and y
        if (sop.vel != 0 && !TEST(sop.flags, SOBJ_MOVE_VERTICAL)) {
            nx.value = (DIV256(sop.vel)) * locktics * EngineUtils.sin(NORM_ANGLE(sop.ang_moving + 512)) >> 14;
            ny.value = (DIV256(sop.vel)) * locktics * EngineUtils.sin(sop.ang_moving) >> 14;

            int dist = Distance(sop.xmid, sop.ymid, sop.xmid + nx.value, sop.ymid + ny.value);
            sop.target_dist -= dist;
        }
    }

    public static void OperateSectorObject(int sopi, float newang, int newx, int newy, int skip) {
        if (Prediction) {
            return;
        }

        Sector_Object sop = SectorObject[sopi];
        if (sop.track < SO_OPERATE_TRACK_START) {
            return;
        }

        if (sop.bob_amt != 0) {
            sop.bob_sine_ndx =  ((totalsynctics << sop.bob_speed) & 2047);
            sop.bob_diff = ((sop.bob_amt * EngineUtils.sin(sop.bob_sine_ndx)) >> 14);

            // for all sectors
            for (int i = 0; sop.sector[i] != -1; i++) {
                Sector sectp = boardService.getSector(sop.sector[i]);
                if (sectp == null) {
                    continue;
                }

                Sect_User su = getSectUser(sop.sector[i]);
                if (su != null && TEST(su.flags, SECTFU_SO_DONT_BOB)) {
                    continue;
                }

                sectp.setFloorz(sop.zorig_floor[i] + sop.bob_diff);
            }
        }

        GlobSpeedSO = 0;

        // sop.ang_tgt = newang;
        sop.ang_moving = (int) newang;

        sop.spin_ang = 0;
        sop.ang = (int) newang;

        RefreshPoints(sopi, newx - sop.xmid, newy - sop.ymid, false, skip);
    }

    public static void PlaceSectorObject(int sopi, int ignored, int newx, int newy) {
        Sector_Object sop = SectorObject[sopi];
        RefreshPoints(sopi, newx - sop.xmid, newy - sop.ymid, false, 1);
    }

    public static void VehicleSetSmoke(Sector_Object sop, Animator animator) {
        ListNode<Sprite> NextSprite;
        for (int j = 0; sop.sector[j] != -1; j++) {
            for (ListNode<Sprite> node = boardService.getSectNode(sop.sector[j]); node != null; node = NextSprite) {
                int SpriteNum = node.getIndex();
                NextSprite = node.getNext();
                Sprite sp = node.get();
                USER u = getUser(SpriteNum);

                if (u != null && sp.getHitag() == SPAWN_SPOT) {
                    if (sp.getClipdist() == 3) {
                        if (animator != null) {
                            if (sp.getStatnum() == STAT_NO_STATE) {
                                continue;
                            }

                            change_sprite_stat(SpriteNum, STAT_NO_STATE);
                            DoSoundSpotMatch(sp.getLotag(), 1, SoundType.SOUND_OBJECT_TYPE);
                            DoSpawnSpotsForDamage(sp.getLotag());
                        } else {
                            change_sprite_stat(SpriteNum, STAT_SPAWN_SPOT);
                            DoSoundSpotStopSound(sp.getLotag());
                        }

                        u.ActorActionFunc = animator;
                    }
                }
            }
        }
    }

    public static void TornadoSpin(Sector_Object sop) {
        // get delta to target angle
        int delta_ang = GetDeltaAngle(sop.ang_tgt, sop.ang);

        sop.ang = NORM_ANGLE(sop.ang + (delta_ang >> sop.turn_speed));
        delta_ang =  (delta_ang >> sop.turn_speed);

        // move z values
        MoveZ(sop);

        // calculate the spin speed
        int speed =  (sop.spin_speed * synctics);
        // spin_ang is incremented by the spin_speed
        sop.spin_ang = NORM_ANGLE(sop.spin_ang + speed);

        if (sop.spin_speed != 0) {
            // ignore delta angle if spinning
            GlobSpeedSO = speed;
        } else {
            // The actual delta from the last frame
            GlobSpeedSO = speed;
            GlobSpeedSO += delta_ang;
        }
    }

    public static void DoTornadoObject(int sopi) {
        Sector_Object sop = SectorObject[sopi];

        int xvect = (sop.vel * EngineUtils.sin(NORM_ANGLE(sop.ang_moving + 512)));
        int yvect = (sop.vel * EngineUtils.sin(NORM_ANGLE(sop.ang_moving)));

        int cursect = sop.op_main_sector; // for sop.vel
        Sector sec = boardService.getSector(cursect);
        if (sec == null) {
            return;
        }

        int floor_dist = DIV4(klabs(sec.getCeilingz() - sec.getFloorz()));
        int x = sop.xmid;
        int y = sop.ymid;

        PlaceSectorObject(sopi, sop.ang_moving, MAXSO, MAXSO);
        int ret = engine.clipmove(x, y, floor_dist, cursect, xvect, yvect, sop.clipdist, Z(0), floor_dist, CLIPMASK_ACTOR);

        x = clipmove_x;
        y = clipmove_y;

        if (ret != 0) {
            sop.ang_moving = NORM_ANGLE(sop.ang_moving + 1024 + RANDOM_P2(512) - 256);
        }

        TornadoSpin(sop);
        RefreshPoints(sopi, x - sop.xmid, y - sop.ymid, true, 1);
    }

    public static void DoAutoTurretObject(int sopi) {
        Sector_Object sop = SectorObject[sopi];
        final int SpriteNum = sop.sp_child;
        USER u = getUser(SpriteNum);
        if (sop.max_damage != -9999 && sop.max_damage <= 0) {
            return;
        }

        if (u == null) {
            return;
        }

        u.WaitTics -= synctics;

        // check for new player if doesn't have a target or time limit expired
        if (u.tgt_sp == -1 || u.WaitTics < 0) {
            // 4 seconds
            u.WaitTics = 4 * 120;
            DoActorPickClosePlayer(SpriteNum);
        }

        if (!MoveSkip2) {
            Sprite tspr = boardService.getSprite(u.tgt_sp);

            for (int i = 0; sop.sp_num[i] != -1; i++) {
                Sprite shootp = boardService.getSprite(sop.sp_num[i]);
                if (shootp != null && shootp.getStatnum() == STAT_SO_SHOOT_POINT) {
                    if (tspr != null && !FAFcansee(shootp.getX(), shootp.getY(), shootp.getZ() - Z(4), shootp.getSectnum(), tspr.getX(),
                            tspr.getY(), SPRITEp_UPPER(tspr), tspr.getSectnum())) {
                        return;
                    }
                }
            }

            // FirePausing
            if (u.Counter > 0) {
                u.Counter -= synctics * 2;
                if (u.Counter <= 0) {
                    u.Counter = 0;
                }
            }

            if (u.Counter == 0) {
                for (int i = 0; sop.sp_num[i] != -1; i++) {
                    Sprite shootp = boardService.getSprite(sop.sp_num[i]);
                    if (shootp != null && shootp.getStatnum() == STAT_SO_SHOOT_POINT) {
                        if (SP_TAG5(shootp) != 0) {
                            u.Counter =  SP_TAG5(shootp);
                        } else {
                            u.Counter = 12;
                        }
                        InitTurretMgun(sop);
                    }
                }
            }

            // sop.ang_tgt = getangle(sop.xmid - u.tgt_sp.x, sop.ymid - u.tgt_sp.y);
            if (tspr != null) {
                sop.ang_tgt = EngineUtils.getAngle(tspr.getX() - sop.xmid, tspr.getY() - sop.ymid);
            }

            // get delta to target angle
            int delta_ang = GetDeltaAngle(sop.ang_tgt, sop.ang);
            // sop.ang += delta_ang >> 4;
            sop.ang = NORM_ANGLE(sop.ang + (delta_ang >> 3));
            // sop.ang += delta_ang >> 2;

            if (sop.limit_ang_center >= 0) {
                int diff = GetDeltaAngle(sop.ang, sop.limit_ang_center);

                if (klabs(diff) >= sop.limit_ang_delta) {
                    if (diff < 0) {
                        sop.ang =  (sop.limit_ang_center - sop.limit_ang_delta);
                    } else {
                        sop.ang =  (sop.limit_ang_center + sop.limit_ang_delta);
                    }

                }
            }

            OperateSectorObject(sopi, sop.ang, sop.xmid, sop.ymid, 2);
        }
    }

    public static void DoActorHitTrackEndPoint(final int SpriteNum) {
        final Sprite sp = boardService.getSprite(SpriteNum);
        final USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Track[u.track].flags &= ~(TF_TRACK_OCCUPIED);

        // jump the current track & determine if you should go to another
        if (TEST(u.Flags, SPR_RUN_AWAY)) {

            // look for another track leading away from the player
            u.track = FindTrackAwayFromPlayer(SpriteNum);

            if (u.track >= 0) {
                sp.setAng(NORM_ANGLE(EngineUtils.getAngle((Track[u.track].TrackPoint[u.point]).x - sp.getX(),
                        (Track[u.track].TrackPoint[u.point]).y - sp.getY())));
            } else {
                u.Flags &= ~(SPR_RUN_AWAY);
                DoActorSetSpeed(SpriteNum, NORM_SPEED);
                u.track = -1;
            }
        } else if (TEST(u.Flags, SPR_FIND_PLAYER)) {
            // look for another track leading away from the player
            u.track =  FindTrackToPlayer(SpriteNum);

            if (u.track >= 0) {
                sp.setAng(NORM_ANGLE(EngineUtils.getAngle((Track[u.track].TrackPoint[u.point]).x - sp.getX(),
                        (Track[u.track].TrackPoint[u.point]).y - sp.getY())));
            } else {
                u.Flags &= ~(SPR_FIND_PLAYER);
                DoActorSetSpeed(SpriteNum, NORM_SPEED);
                u.track = -1;
            }
        } else {
            u.track = -1;
        }
    }

    public static void ActorLeaveTrack(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null || u.track == -1) {
            return;
        }

        u.Flags &= ~(SPR_FIND_PLAYER | SPR_RUN_AWAY | SPR_CLIMBING);
        Track[u.track].flags &= ~(TF_TRACK_OCCUPIED);
        u.track = -1;
    }

    public static boolean ActorTrackDecide(TRACK_POINT tpoint, int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        switch (tpoint.tag_low) {
            case TRACK_START:

                // if track has a type and actor is going the right direction jump
                // the track
                if (Track[u.track].ttflags != 0) {
                    if (u.track_dir == -1) {
                        DoActorHitTrackEndPoint(SpriteNum);
                        return (false);
                    }
                }
                break;
            case TRACK_END:
                // if track has a type and actor is going to right direction jump the
                // track
                if (Track[u.track].ttflags != 0) {
                    if (u.track_dir == 1) {
                        DoActorHitTrackEndPoint(SpriteNum);
                        return (false);
                    }
                }
                break;

            case TRACK_ACTOR_WAIT_FOR_PLAYER: {
                u.Flags |= (SPR_WAIT_FOR_PLAYER);
                u.Dist = tpoint.tag_high;
                break;
            }
            case TRACK_ACTOR_WAIT_FOR_TRIGGER: {
                u.Flags |= (SPR_WAIT_FOR_TRIGGER);
                u.Dist = tpoint.tag_high;
                break;
            }

            //
            // Controls the velocity
            //

            case TRACK_ACTOR_VEL_RATE:
                u.vel_rate = tpoint.tag_high;
                break;
            case TRACK_ACTOR_SPEED_UP:
                u.Flags &= ~(SPR_SLOW_DOWN | SPR_SPEED_UP);
                if (u.track_dir < 0) {
                    // set target to new slower target
                    u.vel_tgt = u.vel_tgt - (tpoint.tag_high * 256);
                    u.Flags |= (SPR_SLOW_DOWN);
                } else {
                    u.vel_tgt = u.vel_tgt + (tpoint.tag_high * 256);
                    u.Flags |= (SPR_SPEED_UP);
                }
                break;
            case TRACK_ACTOR_SLOW_DOWN:
                u.Flags &= ~(SPR_SLOW_DOWN | SPR_SPEED_UP);
                if (u.track_dir > 0) {
                    u.vel_tgt = u.vel_tgt - (tpoint.tag_high * 256);
                    u.Flags |= (SPR_SLOW_DOWN);
                } else {
                    u.vel_tgt = u.vel_tgt + (tpoint.tag_high * 256);
                    u.Flags |= (SPR_SPEED_UP);
                }
                break;
            // Reverse it
            case TRACK_ACTOR_REVERSE:
                u.track_dir *= -1;
                break;
            case TRACK_ACTOR_STAND:
                NewStateGroup(SpriteNum, u.ActorActionSet.Stand);
                break;
            case TRACK_ACTOR_JUMP:
                if (u.ActorActionSet.Jump != null) {
                    sp.setAng(tpoint.ang);
                    if (tpoint.tag_high == 0) {
                        u.jump_speed = ACTOR_STD_JUMP;
                    } else {
                        u.jump_speed =  -tpoint.tag_high;
                    }

                    DoActorBeginJump(SpriteNum);
                    u.ActorActionFunc = DoActorMoveJump;
                }
                break;
            case TRACK_ACTOR_QUICK_JUMP:
            case TRACK_ACTOR_QUICK_SUPER_JUMP:
                if (u.ActorActionSet.Jump != null) {
                    sp.setAng(tpoint.ang);

                    ActorLeaveTrack(SpriteNum);

                    if (tpoint.tag_high != 0) {
                        u.jump_speed =  -tpoint.tag_high;
                    } else {
                        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK));

                        FAFhitscan(sp.getX(), sp.getY(), sp.getZ() - Z(24), sp.getSectnum(), // Start position
                                EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)), // X vector of 3D ang
                                EngineUtils.sin(sp.getAng()), // Y vector of 3D ang
                                0, // Z vector of 3D ang
                                pHitInfo, CLIPMASK_MISSILE);
                        int hitwall = pHitInfo.hitwall;
                        int hitsprite = pHitInfo.hitsprite;

                        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK));

                        if (hitsprite != -1) {
                            return (false);
                        }

                        Wall hwall = boardService.getWall(hitwall);
                        if (hwall == null || hwall.getNextsector() < 0) {
                            return (false);
                        }

                        Sector nsec = boardService.getSector(hwall.getNextsector());
                        if (nsec != null) {
                            int zdiff = klabs(sp.getZ() - nsec.getFloorz()) >> 8;
                            u.jump_speed = PickJumpSpeed(SpriteNum, zdiff);
                        }
                    }

                    DoActorBeginJump(SpriteNum);
                    u.ActorActionFunc = DoActorMoveJump;

                    return (false);
                }
                break;
            case TRACK_ACTOR_QUICK_JUMP_DOWN:
                if (u.ActorActionSet.Jump != null) {
                    sp.setAng(tpoint.ang);
                    ActorLeaveTrack(SpriteNum);
                    if (tpoint.tag_high != 0) {
                        u.jump_speed =  -tpoint.tag_high;
                    } else {
                        u.jump_speed = -350;
                    }
                    DoActorBeginJump(SpriteNum);
                    u.ActorActionFunc = DoActorMoveJump;
                    return (false);
                }
                break;
            case TRACK_ACTOR_QUICK_SCAN:
                if (u.ActorActionSet.Jump != null) {
                    ActorLeaveTrack(SpriteNum);
                    return (false);
                }
                break;
            case TRACK_ACTOR_QUICK_DUCK:
                if (u.getRot() != u.ActorActionSet.Duck) {
                    sp.setAng(tpoint.ang);
                    ActorLeaveTrack(SpriteNum);
                    if (tpoint.tag_high == 0) {
                        u.WaitTics = 4 * 120;
                    } else {
                        u.WaitTics =  (tpoint.tag_high * 128);
                    }
                    InitActorDuck.animatorInvoke(SpriteNum);
                    u.ActorActionFunc = DoActorDuck;
                    return (false);
                }
                break;
            case TRACK_ACTOR_OPERATE:
            case TRACK_ACTOR_QUICK_OPERATE: {
                int nearsector = -1, nearsprite;
                int nearhitdist = -1;
                if (u.getRot() == u.ActorActionSet.Sit || u.getRot() == u.ActorActionSet.Stand) {
                    return (false);
                }

                sp.setAng(tpoint.ang);

                z[0] = sp.getZ() - SPRITEp_SIZE_Z(sp) + Z(5);
                z[1] = sp.getZ() - DIV2(SPRITEp_SIZE_Z(sp));

                for (int j : z) {
                    engine.neartag(sp.getX(), sp.getY(), j, sp.getSectnum(), sp.getAng(), neartag, 1024, NTAG_SEARCH_LO_HI);
                    nearsector = neartag.tagsector;
                    nearsprite = neartag.tagsprite;
                    nearhitdist = neartag.taghitdist;

                    if (nearsprite >= 0 && nearhitdist < 1024) {
                        if (OperateSprite(nearsprite, false)) {
                            if (tpoint.tag_high == 0) {
                                u.WaitTics = 2 * 120;
                            } else {
                                u.WaitTics = (tpoint.tag_high * 128);
                            }

                            NewStateGroup(SpriteNum, u.ActorActionSet.Stand);
                        }
                    }
                }

                if (nearsector >= 0 && nearhitdist < 1024) {
                    if (OperateSector(nearsector, false)) {
                        if (tpoint.tag_high == 0) {
                            u.WaitTics = 2 * 120;
                        } else {
                            u.WaitTics =  (tpoint.tag_high * 128);
                        }

                        NewStateGroup(SpriteNum, u.ActorActionSet.Sit);
                    }
                }

                break;
            }

            case TRACK_ACTOR_JUMP_IF_FORWARD:
                if (u.ActorActionSet.Jump != null && u.track_dir == 1) {
                    if (tpoint.tag_high == 0) {
                        u.jump_speed = ACTOR_STD_JUMP;
                    } else {
                        u.jump_speed =  -tpoint.tag_high;
                    }

                    DoActorBeginJump(SpriteNum);
                }
                break;
            case TRACK_ACTOR_JUMP_IF_REVERSE:
                if (u.ActorActionSet.Jump != null && u.track_dir == -1) {
                    if (tpoint.tag_high == 0) {
                        u.jump_speed = ACTOR_STD_JUMP;
                    } else {
                        u.jump_speed =  -tpoint.tag_high;
                    }

                    DoActorBeginJump(SpriteNum);
                }
                break;
            case TRACK_ACTOR_CRAWL:
                if (u.getRot() != u.ActorActionSet.Crawl) {
                    NewStateGroup(SpriteNum, u.ActorActionSet.Crawl);
                } else {
                    NewStateGroup(SpriteNum, u.ActorActionSet.Rise);
                }
                break;
            case TRACK_ACTOR_SWIM:
                if (u.getRot() != u.ActorActionSet.Swim) {
                    NewStateGroup(SpriteNum, u.ActorActionSet.Swim);
                } else {
                    NewStateGroup(SpriteNum, u.ActorActionSet.Rise);
                }
                break;
            case TRACK_ACTOR_FLY:
                NewStateGroup(SpriteNum, u.ActorActionSet.Fly);
                break;
            case TRACK_ACTOR_SIT:
                if (u.ActorActionSet.Sit != null) {
                    if (tpoint.tag_high == 0) {
                        u.WaitTics = 3 * 120;
                    } else {
                        u.WaitTics =  (tpoint.tag_high * 128);
                    }

                    NewStateGroup(SpriteNum, u.ActorActionSet.Sit);
                }
                break;
            case TRACK_ACTOR_DEATH1:
                if (u.ActorActionSet.Death2 != null) {
                    u.WaitTics = 4 * 120;
                    NewStateGroup(SpriteNum, u.ActorActionSet.Death1);
                }
                break;
            case TRACK_ACTOR_DEATH2:
                if (u.ActorActionSet.Death2 != null) {
                    u.WaitTics = 4 * 120;
                    NewStateGroup(SpriteNum, u.ActorActionSet.Death2);
                }
                break;
            case TRACK_ACTOR_DEATH_JUMP:
                if (u.ActorActionSet.DeathJump != null) {
                    u.Flags |= (SPR_DEAD);
                    sp.setXvel(sp.getXvel() << 1);
                    u.jump_speed = -495;
                    DoActorBeginJump(SpriteNum);
                    NewStateGroup(SpriteNum, u.ActorActionSet.DeathJump);
                }
                break;
            case TRACK_ACTOR_CLOSE_ATTACK1:
                if (u.ActorActionSet.CloseAttack[0] != null) {
                    if (tpoint.tag_high == 0) {
                        u.WaitTics = 2 * 120;
                    } else {
                        u.WaitTics =  (tpoint.tag_high * 128);
                    }

                    NewStateGroup(SpriteNum, u.ActorActionSet.CloseAttack[0]);
                }
                break;
            case TRACK_ACTOR_CLOSE_ATTACK2:
                if (u.ActorActionSet.CloseAttack[1] != null) {
                    if (tpoint.tag_high == 0) {
                        u.WaitTics = 4 * 120;
                    } else {
                        u.WaitTics =  (tpoint.tag_high * 128);
                    }
                    NewStateGroup(SpriteNum, u.ActorActionSet.CloseAttack[1]);
                }
                break;
            case TRACK_ACTOR_ATTACK1:
            case TRACK_ACTOR_ATTACK2:
            case TRACK_ACTOR_ATTACK3:
            case TRACK_ACTOR_ATTACK4:
            case TRACK_ACTOR_ATTACK5:
            case TRACK_ACTOR_ATTACK6: {
                StateGroup ap = u.ActorActionSet.Attack[tpoint.tag_low - TRACK_ACTOR_ATTACK1];
                if (ap != null) {
                    if (tpoint.tag_high == 0) {
                        u.WaitTics = 4 * 120;
                    } else {
                        u.WaitTics =  (tpoint.tag_high * 128);
                    }

                    NewStateGroup(SpriteNum, ap);
                }
                break;
            }
            case TRACK_ACTOR_ZDIFF_MODE:
                if (TEST(u.Flags, SPR_ZDIFF_MODE)) {
                    u.Flags &= ~(SPR_ZDIFF_MODE);
                    Sector sec = boardService.getSector(sp.getSectnum());
                    if (sec != null) {
                        sp.setZ(sec.getFloorz());
                        sp.setZvel(0);
                    }
                } else {
                    u.Flags |= (SPR_ZDIFF_MODE);
                }
                break;
            case TRACK_ACTOR_CLIMB_LADDER:
                if (u.ActorActionSet.Jump != null) {
                    //
                    // Get angle and x,y pos from CLIMB_MARKER
                    //
                    int lspi = FindNearSprite(sp, STAT_CLIMB_MARKER);

                    Sprite lsp = boardService.getSprite(lspi);
                    if (lsp == null) {
                        ActorLeaveTrack(SpriteNum);
                        return (false);
                    }

                    // determine where the player is supposed to be in relation to the ladder
                    // move out in front of the ladder
                    int nx = MOVEx(100, lsp.getAng());
                    int ny = MOVEy(100, lsp.getAng());

                    sp.setX(lsp.getX() + nx);
                    sp.setY(lsp.getY() + ny);

                    sp.setAng(NORM_ANGLE(lsp.getAng() + 1024));

                    //
                    // Get the z height to climb
                    //

                    engine.neartag(sp.getX(), sp.getY(), SPRITEp_TOS(sp) - DIV2(SPRITEp_SIZE_Z(sp)), sp.getSectnum(), sp.getAng(), neartag, 600,
                            NTAG_SEARCH_LO_HI);

                    int hitwall = neartag.tagwall;
                    Wall hwall = boardService.getWall(hitwall);
                    if (hwall == null) {
                        ActorLeaveTrack(SpriteNum);
                        return (false);
                    }

                    // destination z for climbing
                    Sector nsec = boardService.getSector(hwall.getNextsector());
                    if (nsec != null) {
                        u.sz = nsec.getFloorz();
                    }

                    DoActorZrange(SpriteNum);

                    //
                    // Adjust for YCENTERING
                    //

                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YCENTER));
                    int bos_z = SPRITEp_BOS(sp);
                    if (bos_z > u.loz) {
                        u.sy = (bos_z - sp.getZ());
                        sp.setZ(sp.getZ() - u.sy);
                    }

                    //
                    // Misc climb setup
                    //

                    u.Flags |= (SPR_CLIMBING);
                    NewStateGroup(SpriteNum, u.ActorActionSet.Climb);

                    sp.setZvel( -Z(1));
                }
                break;
            case TRACK_ACTOR_SET_JUMP:
                u.jump_speed =  -tpoint.tag_high;
                break;
        }
        return (true);
    }

    /*
     *
     * !AIC - This is where actors follow tracks. Its massy, hard to read, and more
     * complex than it needs to be. It was taken from sector object track movement
     * code. The routine above ActorTrackDecide() is where a track tag is recognized
     * and acted upon. There are quite a few of these that are not useful to us at
     * present time.
     *
     */

    public static void ActorFollowTrack(int SpriteNum, int locktics) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // if not on a track then better not go here
        if (u.track == -1) {
            return;
        }

        // if lying in wait for player
        if (TEST(u.Flags, SPR_WAIT_FOR_PLAYER | SPR_WAIT_FOR_TRIGGER)) {
            if (TEST(u.Flags, SPR_WAIT_FOR_PLAYER)) {
                for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                    PlayerStr pp = Player[pnum];

                    if (Distance(sp.getX(), sp.getY(), pp.posx, pp.posy) < u.Dist) {
                        u.tgt_sp = pp.PlayerSprite;
                        u.Flags &= ~(SPR_WAIT_FOR_PLAYER);
                        return;
                    }
                }
            }

            u.Tics = 0;
            return;
        }

        // if pausing the return
        if (u.WaitTics != 0) {
            u.WaitTics -= locktics;
            if (u.WaitTics <= 0) {
                u.Flags &= ~(SPR_DONT_UPDATE_ANG);
                NewStateGroup(SpriteNum, u.ActorActionSet.Run);
                u.WaitTics = 0;
            }

            return;
        }

        TRACK_POINT tpoint = Track[u.track].TrackPoint[u.point];
        if (tpoint == null) {
            return;
        }

        if (!(TEST(u.Flags, SPR_CLIMBING | SPR_DONT_UPDATE_ANG))) {
            sp.setAng(EngineUtils.getAngle(tpoint.x - sp.getX(), tpoint.y - sp.getY()));
        }

        int nx = 0, ny = 0, nz = 0;
        if ((Distance(sp.getX(), sp.getY(), tpoint.x, tpoint.y)) < 200) // 64
        {
            if (!ActorTrackDecide(tpoint, SpriteNum)) {
                return;
            }

            // get the next point
            NextActorTrackPoint(SpriteNum);
            tpoint = Track[u.track].TrackPoint[u.point];

            if (!(TEST(u.Flags, SPR_CLIMBING | SPR_DONT_UPDATE_ANG))) {
                // calculate a new angle to the target
                sp.setAng(EngineUtils.getAngle(tpoint.x - sp.getX(), tpoint.y - sp.getY()));
            }

            if (TEST(u.Flags, SPR_ZDIFF_MODE)) {

                // set dx,dy,dz up for finding the z magnitude
                int dx = tpoint.x;
                int dy = tpoint.y;
                int dz = tpoint.z;

                // find the distance to the target (player)
                int dist = DIST(dx, dy, sp.getX(), sp.getY());

                // (velocity * difference between the target and the object) /
                // distance
                sp.setZvel( -((sp.getXvel() * (sp.getZ() - dz)) / dist));
            }
        } else {
            // make velocity approach the target velocity
            if (TEST(u.Flags, SPR_SPEED_UP)) {
                if ((u.track_vel += (locktics << u.vel_rate)) >= u.vel_tgt) {
                    u.track_vel = u.vel_tgt;
                    u.Flags &= ~(SPR_SPEED_UP);
                }

                // update the real velocity
                sp.setXvel( DIV256(u.track_vel));
            } else if (TEST(u.Flags, SPR_SLOW_DOWN)) {
                if ((u.track_vel -= (locktics << u.vel_rate)) <= u.vel_tgt) {
                    u.track_vel = u.vel_tgt;
                    u.Flags &= ~(SOBJ_SLOW_DOWN);
                }

                sp.setXvel( DIV256(u.track_vel));
            }

            if (TEST(u.Flags, SPR_CLIMBING)) {
                if (SPRITEp_TOS(sp) + DIV4(SPRITEp_SIZE_Z(sp)) < u.sz) {
                    u.Flags &= ~(SPR_CLIMBING);

                    sp.setZvel(0);

                    sp.setAng(EngineUtils.getAngle(tpoint.x - sp.getX(), tpoint.y - sp.getY()));

                    ActorLeaveTrack(SpriteNum);
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
                    sp.setZ(sp.getZ() + u.sy);

                    DoActorSetSpeed(SpriteNum, SLOW_SPEED);
                    u.ActorActionFunc = NinjaJumpActionFunc;
                    u.jump_speed = -650;
                    DoActorBeginJump(SpriteNum);

                    return;
                }
            } else {
                // calculate a new x and y
                nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
                ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;
            }

            if (sp.getZvel() != 0) {
                nz = sp.getZvel() * locktics;
            }
        }

        u.moveSpriteReturn = move_sprite(SpriteNum, nx, ny, nz, u.ceiling_dist, u.floor_dist, 0, locktics);
        if (u.moveSpriteReturn != 0) {
            if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
                ActorLeaveTrack(SpriteNum);
            }
        }

    }
}
