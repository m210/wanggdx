package ru.m210projects.Wang;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Sound.SoundType;
import ru.m210projects.Wang.Type.Animator;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Morth.SOBJ_AlignCeilingToPoint;
import static ru.m210projects.Wang.Morth.SOBJ_AlignFloorToPoint;
import static ru.m210projects.Wang.Names.STAT_SPIKE;
import static ru.m210projects.Wang.Rooms.cz;
import static ru.m210projects.Wang.Rooms.fz;
import static ru.m210projects.Wang.Sector.DoMatchEverything;
import static ru.m210projects.Wang.Sector.DoSoundSpotMatch;
import static ru.m210projects.Wang.Sprites.KillSprite;
import static ru.m210projects.Wang.Sprites.SectorObject;
import static ru.m210projects.Wang.Stag.SECT_SPIKE;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Vator.InterpSectorSprites;
import static ru.m210projects.Wang.Vator.VatorSwitch;

public class Spike {

    public static final Animator DoSpike = new Animator((Animator.Runnable) Spike::DoSpike);
    public static final Animator DoSpikeAuto = new Animator((Animator.Runnable) Spike::DoSpikeAuto);

    public static void ReverseSpike(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // if paused go ahead and start it up again
        if (u.Tics != 0) {
            u.Tics = 0;
            SetSpikeActive(SpriteNum);
            return;
        }

        // moving toward to OFF pos
        if (u.z_tgt == u.oz) {
            if (sp.getZ() == u.oz) {
                u.z_tgt = u.sz;
            } else if (u.sz == u.oz) {
                u.z_tgt = sp.getZ();
            }
        } else if (u.z_tgt == u.sz) {
            if (sp.getZ() == u.oz) {
                u.z_tgt = sp.getZ();
            } else if (u.sz == u.oz) {
                u.z_tgt = u.sz;
            }
        }

        u.vel_rate =  -u.vel_rate;
    }

    public static void SetSpikeActive(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        InterpSectorSprites(sp.getSectnum(), true);

        // play activate sound
        DoSoundSpotMatch(SP_TAG2(sp), 1, SoundType.SOUND_OBJECT_TYPE);

        u.Flags |= (SPR_ACTIVE);
        u.Tics = 0;

        // moving to the ON position
        if (u.z_tgt == sp.getZ()) {
            VatorSwitch(SP_TAG2(sp), ON);
        } else
            // moving to the OFF position
            if (u.z_tgt == u.sz) {
                VatorSwitch(SP_TAG2(sp), OFF);
            }
    }

    public static void SetSpikeInactive(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        InterpSectorSprites(sp.getSectnum(), false);

        // play activate sound
        DoSoundSpotMatch(SP_TAG2(sp), 2, SoundType.SOUND_OBJECT_TYPE);

        u.Flags &= ~(SPR_ACTIVE);
    }

    // called for operation from the space bar
    public static void DoSpikeOperate(PlayerStr pp, int sectnum) {
        int match;
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite fsp = node.get();

            if (fsp.getStatnum() == STAT_SPIKE && SP_TAG1(fsp) == SECT_SPIKE && SP_TAG3(fsp) == 0) {
                match =  SP_TAG2(fsp);
                if (match > 0) {
                    if (!TestSpikeMatchActive(match)) {
                        DoSpikeMatch(pp, match);
                    }
                    return;
                }

                SetSpikeActive(i);
                break;
            }
        }
    }

    // called from switches and triggers
    // returns first spike found
    public static void DoSpikeMatch(PlayerStr ignored, int match) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPIKE); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite fsp = node.get();

            if (SP_TAG1(fsp) == SECT_SPIKE && SP_TAG2(fsp) == match) {
                USER fu = getUser(i);
                if (fu != null && TEST(fu.Flags, SPR_ACTIVE)) {
                    ReverseSpike(i);
                    continue;
                }

                SetSpikeActive(i);
            }
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean TestSpikeMatchActive(int match) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPIKE); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite fsp = node.get();

            if (SP_TAG1(fsp) == SECT_SPIKE && SP_TAG2(fsp) == match) {
                USER fu = getUser(i);

                // door war
                if (TEST_BOOL6(fsp)) {
                    continue;
                }

                if (fu != null && (TEST(fu.Flags, SPR_ACTIVE) || fu.Tics != 0)) {
                    return (true);
                }
            }
        }

        return (false);
    }

    public static int DoSpikeMove(int SpriteNum, int lptr) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return 0;
        }

        int zval = lptr;
        // if LESS THAN goal
        if (zval < u.z_tgt) {
            // move it DOWN
            zval += (synctics * u.jump_speed);

            u.jump_speed += u.vel_rate * synctics;

            // if the other way make it equal
            if (zval > u.z_tgt) {
                zval = u.z_tgt;
            }
        }

        // if GREATER THAN goal
        if (zval > u.z_tgt) {
            // move it UP
            zval -= (synctics * u.jump_speed);

            u.jump_speed += u.vel_rate * synctics;

            if (zval < u.z_tgt) {
                zval = u.z_tgt;
            }
        }

        return zval;
    }

    public static void SpikeAlign(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // either work on single sector or all tagged in SOBJ
        if ((byte) SP_TAG7(sp) < 0) {
            if (TEST(sp.getCstat(), CSTAT_SPRITE_YFLIP)) {
                game.pInt.setcheinuminterpolate(sp.getSectnum(), boardService.getSector(sp.getSectnum()));
                engine.alignceilslope(sp.getSectnum(), sp.getX(), sp.getY(), u.zclip);
            } else {
                game.pInt.setfheinuminterpolate(sp.getSectnum(), boardService.getSector(sp.getSectnum()));
                engine.alignflorslope(sp.getSectnum(), sp.getX(), sp.getY(), u.zclip);
            }
        } else {
            if (TEST(sp.getCstat(), CSTAT_SPRITE_YFLIP)) {
                SOBJ_AlignCeilingToPoint(SectorObject[SP_TAG7(sp)], sp.getX(), sp.getY(), u.zclip);
            } else {
                SOBJ_AlignFloorToPoint(SectorObject[SP_TAG7(sp)], sp.getX(), sp.getY(), u.zclip);
            }
        }
    }

    public static void MoveSpritesWithSpike(int sectnum) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();

            if (getUser(i) != null) {
                continue;
            }

            if (TEST(sp.getExtra(), SPRX_STAY_PUT_VATOR)) {
                continue;
            }

            engine.getzsofslope(sectnum, sp.getX(), sp.getY(), fz, cz);
            game.pInt.setsprinterpolate(i, sp);
            sp.setZ(fz.get());
        }
    }

    public static void DoSpike(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // zclip = floor or ceiling z
        // oz = original z
        // z_tgt = target z - on pos
        // sz = starting z - off pos

        int lptr = u.zclip = DoSpikeMove(SpriteNum, u.zclip);
        MoveSpritesWithSpike(sp.getSectnum());
        SpikeAlign(SpriteNum);

        // EQUAL this entry has finished
        if (lptr == u.z_tgt) {
            // in the ON position
            if (u.z_tgt == sp.getZ()) {
                // change target
                u.z_tgt = u.sz;
                u.vel_rate =  -u.vel_rate;

                SetSpikeInactive(SpriteNum);

                if (SP_TAG6(sp) != 0) {
                    DoMatchEverything(null, SP_TAG6(sp), -1);
                }
            } else
                // in the OFF position
                if (u.z_tgt == u.sz) {
                    // change target
                    u.jump_speed =  u.vel_tgt;
                    u.vel_rate =  klabs(u.vel_rate);
                    u.z_tgt = sp.getZ();

                    SetSpikeInactive(SpriteNum);
                    if (SP_TAG6(sp) != 0 && TEST_BOOL5(sp)) {
                        DoMatchEverything(null, SP_TAG6(sp), -1);
                    }
                }

            // operate only once
            if (TEST_BOOL2(sp)) {
                SetSpikeInactive(SpriteNum);
                KillSprite(SpriteNum);
                return;
            }

            // setup to go back to the original z
            if (lptr != u.oz) {
                if (u.WaitTics != 0) {
                    u.Tics = u.WaitTics;
                }
            }
        } else {
            // if heading for the OFF (original) position and should NOT CRUSH
            if (TEST_BOOL3(sp) && u.z_tgt == u.oz) {
                Sprite bsp;
                USER bu;
                boolean found = false;

                ListNode<Sprite> nexti;
                for (ListNode<Sprite> node = boardService.getSectNode(sp.getSectnum()); node != null; node = nexti) {
                    int i = node.getIndex();
                    nexti = node.getNext();
                    bsp = node.get();
                    bu = getUser(i);

                    if (bu != null && TEST(bsp.getCstat(), CSTAT_SPRITE_BLOCK) && TEST(bsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                        ReverseSpike(SpriteNum);
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    int pnum;
                    PlayerStr pp;
                    // go ahead and look for players clip box bounds
                    for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                        pp = Player[pnum];

                        if ((boardService.getSector(pp.lo_sectp) != null && pp.lo_sectp == sp.getSectnum()) || (pp.hi_sectp != -1 && pp.hi_sectp == sp.getSectnum())) {
                            ReverseSpike(SpriteNum);
                        }
                    }
                }
            }
        }
    }

    public static void DoSpikeAuto(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int lptr = u.zclip = DoSpikeMove(SpriteNum, u.zclip);
        MoveSpritesWithSpike(sp.getSectnum());
        SpikeAlign(SpriteNum);

        // EQUAL this entry has finished
        if (lptr == u.z_tgt) {
            // in the UP position
            if (u.z_tgt == sp.getZ()) {
                // change target
                u.z_tgt = u.sz;
                u.vel_rate =  -u.vel_rate;
                u.Tics = u.WaitTics;

                if (SP_TAG6(sp) != 0) {
                    DoMatchEverything(null, SP_TAG6(sp), -1);
                }
            } else
                // in the DOWN position
                if (u.z_tgt == u.sz) {
                    // change target
                    u.jump_speed =  u.vel_tgt;
                    u.vel_rate =  klabs(u.vel_rate);
                    u.z_tgt = sp.getZ();
                    u.Tics = u.WaitTics;

                    if (SP_TAG6(sp) != 0 && TEST_BOOL5(sp)) {
                        DoMatchEverything(null, SP_TAG6(sp), -1);
                    }
                }
        }
    }

}
