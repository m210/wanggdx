package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Wang.Actor.DoBeginJump;
import static ru.m210projects.Wang.Break.HitBreakWall;
import static ru.m210projects.Wang.Break.SetupSpriteForBreak;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.LUMINOUS;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.*;
import static ru.m210projects.Wang.Rooms.*;
import static ru.m210projects.Wang.Sector.DoMatchEverything;
import static ru.m210projects.Wang.Sector.getSectUser;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Tags.TAG_WALL_BREAK;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Weapon.*;

public class JWeapon {

    private static final Animator BloodSprayFall = new Animator((Animator.Runnable) JWeapon::BloodSprayFall);
    public static final Animator DoRadiationCloud = new Animator((Animator.Runnable) JWeapon::DoRadiationCloud);
    public static final Animator DoChemBomb = new Animator((Animator.Runnable) JWeapon::DoChemBomb);
    public static final Animator DoCaltrops = new Animator((Animator.Runnable) JWeapon::DoCaltrops);
    public static final Animator DoCaltropsStick = new Animator((Animator.Runnable) JWeapon::DoCaltropsStick);
    public static final Animator DoFlag = new Animator((Animator.Runnable) JWeapon::DoFlag);
    public static final Animator DoCarryFlag = new Animator((Animator.Runnable) JWeapon::DoCarryFlag);
    public static final Animator DoCarryFlagNoDet = new Animator((Animator.Runnable) JWeapon::DoCarryFlagNoDet);
    public static final Animator DoPhosphorus = new Animator((Animator.Runnable) JWeapon::DoPhosphorus);
    public static final Animator DoBloodSpray = new Animator((Animator.Runnable) JWeapon::DoBloodSpray);
    public static final Animator DoWallBloodDrip = new Animator((Animator.Runnable) JWeapon::DoWallBloodDrip);

    public static final int CHEMTICS = SEC(40);
    public static final int FLAG_DETONATE_STATE = 99;

    public static final int GOREDrip = 1562; // 2430
    public static final int BLOODSPRAY_RATE = 20;

    public static final State[] s_BloodSpray = { new State(GOREDrip, BLOODSPRAY_RATE, BloodSprayFall),
            new State(GOREDrip + 1, BLOODSPRAY_RATE, BloodSprayFall),
            new State(GOREDrip + 2, BLOODSPRAY_RATE, BloodSprayFall),
            new State(GOREDrip + 3, BLOODSPRAY_RATE, BloodSprayFall), new State(GOREDrip + 3, 100, DoSuicide), };

    public static final int EXP_RATE = 2;
    public static final State[] s_PhosphorExp = { new State(EXP, EXP_RATE, null),
            new State(EXP + 1, EXP_RATE, null), new State(EXP + 2, EXP_RATE, null), new State(EXP + 3, EXP_RATE, null),
            new State(EXP + 4, EXP_RATE, null), new State(EXP + 5, EXP_RATE, null), new State(EXP + 6, EXP_RATE, null),
            new State(EXP + 7, EXP_RATE, null), new State(EXP + 8, EXP_RATE, null), new State(EXP + 9, EXP_RATE, null),
            new State(EXP + 10, EXP_RATE, null), new State(EXP + 11, EXP_RATE, null),
            new State(EXP + 12, EXP_RATE, null), new State(EXP + 13, EXP_RATE, null),
            new State(EXP + 14, EXP_RATE, null), new State(EXP + 15, EXP_RATE, null),
            new State(EXP + 16, EXP_RATE, null), new State(EXP + 17, EXP_RATE, null),
            new State(EXP + 18, EXP_RATE, null), new State(EXP + 19, EXP_RATE, null),
            new State(EXP + 20, 100, DoSuicide), };

    public static final int MUSHROOM_RATE = 25;

    public static final State[] s_NukeMushroom = { new State(MUSHROOM_CLOUD, MUSHROOM_RATE, null),
            new State(MUSHROOM_CLOUD + 1, MUSHROOM_RATE, null), new State(MUSHROOM_CLOUD + 2, MUSHROOM_RATE, null),
            new State(MUSHROOM_CLOUD + 3, MUSHROOM_RATE, null), new State(MUSHROOM_CLOUD + 4, MUSHROOM_RATE, null),
            new State(MUSHROOM_CLOUD + 5, MUSHROOM_RATE, null), new State(MUSHROOM_CLOUD + 6, MUSHROOM_RATE, null),
            new State(MUSHROOM_CLOUD + 7, MUSHROOM_RATE, null), new State(MUSHROOM_CLOUD + 8, MUSHROOM_RATE, null),
            new State(MUSHROOM_CLOUD + 9, MUSHROOM_RATE, null), new State(MUSHROOM_CLOUD + 10, MUSHROOM_RATE, null),
            new State(MUSHROOM_CLOUD + 11, MUSHROOM_RATE, null), new State(MUSHROOM_CLOUD + 12, MUSHROOM_RATE, null),
            new State(MUSHROOM_CLOUD + 13, MUSHROOM_RATE, null), new State(MUSHROOM_CLOUD + 14, MUSHROOM_RATE, null),
            new State(MUSHROOM_CLOUD + 15, MUSHROOM_RATE, null), new State(MUSHROOM_CLOUD + 16, MUSHROOM_RATE, null),
            new State(MUSHROOM_CLOUD + 17, MUSHROOM_RATE, null), new State(MUSHROOM_CLOUD + 18, MUSHROOM_RATE, null),
            new State(MUSHROOM_CLOUD + 19, 100, DoSuicide), };


    public static final int RADIATION_RATE = 16;

    public static final State[] s_RadiationCloud = { new State(RADIATION_CLOUD, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 1, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 2, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 3, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 4, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 5, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 6, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 7, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 8, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 9, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 10, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 11, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 12, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 13, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 14, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 15, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 16, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 17, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 18, RADIATION_RATE, DoRadiationCloud),
            new State(RADIATION_CLOUD + 19, 100, DoSuicide), };

    public static final int CHEMBOMB_FRAMES = 1;
    public static final int CHEMBOMB_R0 = 3038;
    public static final int CHEMBOMB_R1 = CHEMBOMB_R0 + (CHEMBOMB_FRAMES);
    public static final int CHEMBOMB_R2 = CHEMBOMB_R0 + (CHEMBOMB_FRAMES * 2);
    public static final int CHEMBOMB_R3 = CHEMBOMB_R0 + (CHEMBOMB_FRAMES * 3);
    public static final int CHEMBOMB_R4 = CHEMBOMB_R0 + (CHEMBOMB_FRAMES * 4);

    public static final int CHEMBOMB = CHEMBOMB_R0;
    public static final int CHEMBOMB_RATE = 8;

    public static final State[] s_ChemBomb = { new State(CHEMBOMB_R0, CHEMBOMB_RATE, DoChemBomb),
            new State(CHEMBOMB_R1, CHEMBOMB_RATE, DoChemBomb),
            new State(CHEMBOMB_R2, CHEMBOMB_RATE, DoChemBomb),
            new State(CHEMBOMB_R3, CHEMBOMB_RATE, DoChemBomb),
            new State(CHEMBOMB_R4, CHEMBOMB_RATE, DoChemBomb), };

//    public static final int CALTROPS_FRAMES = 1;
    public static final int CALTROPS_R0 = CALTROPS - 1;

    public static final int CALTROPS_RATE = 8;


    public static final State[] s_Caltrops = { new State(CALTROPS_R0, CALTROPS_RATE, DoCaltrops),
            new State(CALTROPS_R0 + 1, CALTROPS_RATE, DoCaltrops),
            new State(CALTROPS_R0 + 2, CALTROPS_RATE, DoCaltrops), };

    public static final State[] s_CaltropsStick = {
            new State(CALTROPS_R0 + 2, CALTROPS_RATE, DoCaltropsStick).setNext() };

    //////////////////////
    //
    // CAPTURE FLAG
    //
    //////////////////////



    public static final int FLAG = 2520;
    public static final int FLAG_RATE = 16;

    public static final State[] s_CarryFlag = { new State(FLAG, FLAG_RATE, DoCarryFlag),
            new State(FLAG + 1, FLAG_RATE, DoCarryFlag), new State(FLAG + 2, FLAG_RATE, DoCarryFlag), };

    public static final State[] s_CarryFlagNoDet = { new State(FLAG, FLAG_RATE, DoCarryFlagNoDet),
            new State(FLAG + 1, FLAG_RATE, DoCarryFlagNoDet), new State(FLAG + 2, FLAG_RATE, DoCarryFlagNoDet), };

    public static final State[] s_Flag = { new State(FLAG, FLAG_RATE, DoFlag),
            new State(FLAG + 1, FLAG_RATE, DoFlag), new State(FLAG + 2, FLAG_RATE, DoFlag), };

    public static final int PHOSPHORUS_RATE = 8;

    public static final State[] s_Phosphorus = { new State(PHOSPHORUS, PHOSPHORUS_RATE, DoPhosphorus),
            new State(PHOSPHORUS + 1, PHOSPHORUS_RATE, DoPhosphorus), };



    public static final int CHUNK1 = 1685;
    public static final State[] s_BloodSprayChunk = { new State(CHUNK1, 8, DoBloodSpray),
            new State(CHUNK1 + 1, 8, DoBloodSpray), new State(CHUNK1 + 2, 8, DoBloodSpray),
            new State(CHUNK1 + 3, 8, DoBloodSpray), new State(CHUNK1 + 4, 8, DoBloodSpray),
            new State(CHUNK1 + 5, 8, DoBloodSpray), };


    public static final int DRIP = 1566;
    public static final State[] s_BloodSprayDrip = { new State(DRIP, PHOSPHORUS_RATE, DoWallBloodDrip),
            new State(DRIP + 1, PHOSPHORUS_RATE, DoWallBloodDrip),
            new State(DRIP + 2, PHOSPHORUS_RATE, DoWallBloodDrip), };

    public static void InitJWeaponStates() {
        State.InitState(s_BloodSpray);
        State.InitState(s_PhosphorExp);
        State.InitState(s_NukeMushroom);
        State.InitState(s_RadiationCloud);
        State.InitState(s_ChemBomb);
        State.InitState(s_Caltrops);
        State.InitState(s_CaltropsStick);
        State.InitState(s_CarryFlag);
        State.InitState(s_CarryFlagNoDet);
        State.InitState(s_Flag);
        State.InitState(s_Phosphorus);
        State.InitState(s_BloodSprayChunk);
        State.InitState(s_BloodSprayDrip);
    }

    public static void DoWallBloodDrip(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // sy & sz are the ceiling and floor of the sector you are sliding down
        if (u.sz != u.sy) {
            // if you are between the ceiling and floor fall fast
            if (sp.getZ() > u.sy && sp.getZ() < u.sz) {
                sp.setZvel(sp.getZvel() + 300);
                sp.setZ(sp.getZ() + sp.getZvel());
            } else {
                sp.setZvel( ((300 + RANDOM_RANGE(2300)) >> 1));
                sp.setZ(sp.getZ() + sp.getZvel());
            }
        } else {
            sp.setZvel( ((300 + RANDOM_RANGE(2300)) >> 1));
            sp.setZ(sp.getZ() + sp.getZvel());
        }

        if (sp.getZ() >= u.loz) {
            sp.setZ(u.loz);
            SpawnFloorSplash(SpriteNum);
            KillSprite(SpriteNum);
        }
    }

    public static void SpawnMidSplash(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int newsp = SpawnSprite(STAT_MISSILE, GOREDrip, s_GoreSplash[0], sp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_MID(sp),
                sp.getAng(), 0);

        Sprite np = boardService.getSprite(newsp);
        USER nu = getUser(newsp);
        if (np == null || nu == null) {
            return;
        }

        np.setShade(-12);
        np.setXrepeat( (70 - RANDOM_RANGE(20)));
        np.setYrepeat( (70 - RANDOM_RANGE(20)));
        nu.ox = u.ox;
        nu.oy = u.oy;
        nu.oz = u.oz;
        np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
        np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        if (RANDOM_P2(1024) < 512) {
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_XFLIP));
        }

        nu.xchange = 0;
        nu.ychange = 0;
        nu.zchange = 0;

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            nu.Flags |= (SPR_UNDERWATER);
        }
    }

    public static void SpawnFloorSplash(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int newsp = SpawnSprite(STAT_MISSILE, GOREDrip, s_GoreFloorSplash[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);

        Sprite np = boardService.getSprite(newsp);
        USER nu = getUser(newsp);
        if (np == null || nu == null) {
            return;
        }

        np.setShade(-12);
        np.setXrepeat( (70 - RANDOM_RANGE(20)));
        np.setYrepeat( (70 - RANDOM_RANGE(20)));
        nu.ox = u.ox;
        nu.oy = u.oy;
        nu.oz = u.oz;
        np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
        np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        if (RANDOM_P2(1024) < 512) {
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_XFLIP));
        }

        nu.xchange = 0;
        nu.ychange = 0;
        nu.zchange = 0;

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            nu.Flags |= (SPR_UNDERWATER);
        }
    }

    public static void DoBloodSpray(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            ScaleSpriteVector(Weapon, 50000);
        }
        u.Counter += 20; // These are STAT_SKIIP4 now, so * 2
        u.zchange += u.Counter;

        if (sp.getXvel() <= 2) {
            // special stuff for blood worm
            sp.setZ(sp.getZ() + (u.zchange >> 1));

            engine.getzsofslope(sp.getSectnum(), sp.getX(), sp.getY(), fz, cz);
            // pretend like we hit a sector
            if (sp.getZ() >= fz.get()) {
                sp.setZ(fz.get());
                SpawnFloorSplash(Weapon);
                KillSprite( Weapon);
                return;
            }
        } else {
            u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist,
                    CLIPMASK_MISSILE, MISSILEMOVETICS);
        }

        MissileHitDiveArea(Weapon);

        if (u.moveSpriteReturn != 0) {
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_PLAX_WALL:
                    KillSprite(Weapon);
                    return;
                case HIT_SPRITE: {
                    int wall_ang;
                    int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Sprite hsp = boardService.getSprite(hitsprite);
                    if (hsp == null) {
                        break;
                    }

                    if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                        wall_ang = NORM_ANGLE(hsp.getAng());
                        SpawnMidSplash(Weapon);
                        QueueWallBlood(Weapon, hsp.getAng());
                        WallBounce(Weapon, wall_ang);
                        ScaleSpriteVector(Weapon, 32000);
                    } else {
                        u.xchange = u.ychange = 0;
                        SpawnMidSplash(Weapon);
                        QueueWallBlood(Weapon, hsp.getAng());
                        KillSprite( Weapon);
                        return;
                    }

                    break;
                }

                case HIT_WALL: {
                    int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Wall wph = boardService.getWall(hitwall);
                    if (wph == null) {
                        break;
                    }

                    if (wph.getLotag() == TAG_WALL_BREAK) {
                        HitBreakWall(hitwall, sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), u.ID);
                        u.moveSpriteReturn = 0;
                        break;
                    }

                    Wall nw = wph.getWall2();
                    int wall_ang = NORM_ANGLE(EngineUtils.getAngle(nw.getX() - wph.getX(), nw.getY() - wph.getY()) + 512);

                    SpawnMidSplash(Weapon);
                    int wb =  QueueWallBlood(Weapon, NORM_ANGLE(wall_ang + 1024));
                    if (wb < 0) {
                        KillSprite(Weapon);
                        return;
                    } else {
                        Sprite wbSpr = boardService.getSprite(wb);
                        if (wbSpr == null || FAF_Sector(wbSpr.getSectnum()) || FAF_ConnectArea(wbSpr.getSectnum())) {
                            KillSprite(Weapon);
                            return;
                        }

                        u.xchange = u.ychange = 0;
                        sp.setXvel(0);
                        sp.setYvel(0);
                        int siz = (70 - RANDOM_RANGE(25));
                        sp.setXrepeat(siz);
                        sp.setYrepeat(siz);
                        sp.setX(wbSpr.getX());
                        sp.setY(wbSpr.getY());

                        // !FRANK! bit of a hack
                        // yvel is the hitwall
                        if (wbSpr.getYvel() >= 0) {
                            int wallnum = wbSpr.getYvel();

                            // sy & sz are the ceiling and floor of the sector you are sliding down
                            Wall wal = boardService.getWall(wallnum);
                            if (wal != null && wal.getNextsector() >= 0) {
                                engine.getzsofslope(wal.getNextsector(), sp.getX(), sp.getY(), fz, cz);
                                u.sy = cz.get();
                                u.sz = fz.get();
                            } else {
                                u.sy = u.sz; // ceiling and floor are equal - white wall
                            }
                        }

                        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE));
                        ChangeState(Weapon, s_BloodSprayDrip[0]);
                    }
                    break;
                }

                case HIT_SECTOR: {
                    // hit floor
                    if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                        if (TEST(u.Flags, SPR_UNDERWATER)) {
                            u.Flags |= (SPR_BOUNCE); // no bouncing
                        }
                        // underwater

                        Sect_User su = getSectUser(sp.getSectnum());
                        if (u.lo_sectp != -1 && su != null && su.depth != 0) {
                            u.Flags |= (SPR_BOUNCE); // no bouncing on
                        }
                        // shallow water

                        u.xchange = u.ychange = 0;
                        SpawnFloorSplash(Weapon);
                        KillSprite( Weapon);
                        return;

                    } else
                    // hit something above
                    {
                        u.zchange = -u.zchange;
                        ScaleSpriteVector(Weapon, 32000); // was 22000
                    }
                    break;
                }
            }
        }

        // if you haven't bounced or your going slow do some puffs
        if (!TEST(u.Flags, SPR_BOUNCE | SPR_UNDERWATER)) {
            int newsp = SpawnSprite(STAT_MISSILE, GOREDrip, s_BloodSpray[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 100);
            Sprite np = boardService.getSprite(newsp);
            USER nu = getUser(newsp);
            if (np == null || nu == null) {
                return;
            }

            SetOwner(Weapon, newsp);
            np.setShade(-12);
            np.setXrepeat( (40 - RANDOM_RANGE(30)));
            np.setYrepeat( (40 - RANDOM_RANGE(30)));
            nu.ox = u.ox;
            nu.oy = u.oy;
            nu.oz = u.oz;
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
            np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

            if (RANDOM_P2(1024) < 512) {
                np.setCstat(np.getCstat() | (CSTAT_SPRITE_XFLIP));
            }
            if (RANDOM_P2(1024) < 512) {
                np.setCstat(np.getCstat() | (CSTAT_SPRITE_YFLIP));
            }

            nu.xchange = u.xchange;
            nu.ychange = u.ychange;
            nu.zchange = u.zchange;

            ScaleSpriteVector(newsp, 20000);

            if (TEST(u.Flags, SPR_UNDERWATER)) {
                nu.Flags |= (SPR_UNDERWATER);
            }
        }

    }

    public static void DoPhosphorus(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            ScaleSpriteVector(Weapon, 50000);
        }
        u.Counter += 20 * 2;
        u.zchange += u.Counter;

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE,
                MISSILEMOVETICS * 2);

        MissileHitDiveArea(Weapon);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (u.moveSpriteReturn != 0) {
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_PLAX_WALL:
                    KillSprite(Weapon);
                    return;
                case HIT_SPRITE: {
                    int wall_ang;
                    int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Sprite hsp = boardService.getSprite(hitsprite);

                    if (hsp != null && TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                        wall_ang = NORM_ANGLE(hsp.getAng());
                        WallBounce(Weapon, wall_ang);
                        ScaleSpriteVector(Weapon, 32000);
                    } else {
                        if (hsp != null && TEST(hsp.getExtra(), SPRX_BURNABLE)) {
                            USER hu = getUser(hitsprite);
                            if (hu == null) {
                                SpawnUser(hitsprite, hsp.getPicnum(), null);
                            }
                            SpawnFireballExp(Weapon);
                            SpawnFireballFlames(Weapon, hitsprite);
                            DoFlamesDamageTest(Weapon);
                        }
                        u.xchange = u.ychange = 0;
                        KillSprite( Weapon);
                        return;
                    }

                    break;
                }

                case HIT_WALL: {
                    int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Wall wph = boardService.getWall(hitwall);
                    if (wph == null) {
                        break;
                    }

                    if (wph.getLotag() == TAG_WALL_BREAK) {
                        HitBreakWall(hitwall, sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), u.ID);
                        u.moveSpriteReturn = 0;
                        break;
                    }

                    Wall nw = wph.getWall2();
                    int wall_ang = NORM_ANGLE(EngineUtils.getAngle(nw.getX() - wph.getX(), nw.getY() - wph.getY()) + 512);

                    WallBounce(Weapon, wall_ang);
                    ScaleSpriteVector(Weapon, 32000);
                    break;
                }

                case HIT_SECTOR: {
                    boolean hitwall;

                    if (SlopeBounce(Weapon, tmp_ptr[0])) {
                        hitwall = tmp_ptr[0].value != 0;
                        if (hitwall) {
                            // hit a wall
                            ScaleSpriteVector(Weapon, 28000);
                            u.moveSpriteReturn = 0;
                            u.Counter = 0;
                        } else {
                            // hit a sector
                            if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                                // hit a floor
                                if (!TEST(u.Flags, SPR_BOUNCE)) {
                                    u.Flags |= (SPR_BOUNCE);
                                    ScaleSpriteVector(Weapon, 32000); // was 18000
                                    u.zchange /= 6;
                                    u.moveSpriteReturn = 0;
                                    u.Counter = 0;
                                } else {
                                    u.xchange = u.ychange = 0;
                                    SpawnFireballExp(Weapon);
                                    KillSprite( Weapon);
                                    return;
                                }
                            } else {
                                // hit a ceiling
                                ScaleSpriteVector(Weapon, 32000); // was 22000
                            }
                        }
                    } else {
                        // hit floor
                        if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                            if (TEST(u.Flags, SPR_UNDERWATER)) {
                                u.Flags |= (SPR_BOUNCE); // no bouncing
                            }
                            // underwater
                            Sect_User su = getSectUser(sp.getSectnum());
                            if (u.lo_sectp != -1 && su != null && su.depth != 0) {
                                u.Flags |= (SPR_BOUNCE); // no bouncing on
                            }
                            // shallow water

                            if (!TEST(u.Flags, SPR_BOUNCE)) {
                                u.Flags |= (SPR_BOUNCE);
                                u.moveSpriteReturn = 0;
                                u.Counter = 0;
                                u.zchange = -u.zchange;
                                ScaleSpriteVector(Weapon, 32000); // Was 18000
                                u.zchange /= 6;
                            } else {
                                u.xchange = u.ychange = 0;
                                SpawnFireballExp(Weapon);
                                KillSprite( Weapon);
                                return;
                            }
                        } else
                        // hit something above
                        {
                            u.zchange = -u.zchange;
                            ScaleSpriteVector(Weapon, 32000); // was 22000
                        }
                    }
                    break;
                }
            }
        }

        // if you haven't bounced or your going slow do some puffs
        if (!TEST(u.Flags, SPR_BOUNCE | SPR_UNDERWATER) && !TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
            int newsp = SpawnSprite(STAT_SKIP4, PUFF, s_PhosphorExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 100);

            Sprite np = boardService.getSprite(newsp);
            USER nu = getUser(newsp);
            if (np == null || nu == null) {
                return;
            }

            np.setHitag(LUMINOUS); // Always full brightness
            SetOwner(Weapon, newsp);
            np.setShade(-40);
            np.setXrepeat( (12 + RANDOM_RANGE(10)));
            np.setYrepeat( (12 + RANDOM_RANGE(10)));
            nu.ox = u.ox;
            nu.oy = u.oy;
            nu.oz = u.oz;
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
            np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

            if (RANDOM_P2(1024) < 512) {
                np.setCstat(np.getCstat() | (CSTAT_SPRITE_XFLIP));
            }
            if (RANDOM_P2(1024) < 512) {
                np.setCstat(np.getCstat() | (CSTAT_SPRITE_YFLIP));
            }

            nu.xchange = u.xchange;
            nu.ychange = u.ychange;
            nu.zchange = u.zchange;

            nu.spal = (byte) PALETTE_PLAYER3;
            np.setPal(nu.spal); // RED

            ScaleSpriteVector(newsp, 20000);

            if (TEST(u.Flags, SPR_UNDERWATER)) {
                nu.Flags |= (SPR_UNDERWATER);
            }
        }

    }

    public static void DoChemBomb(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            ScaleSpriteVector(Weapon, 50000);
        }
        u.Counter += 20;
        u.zchange += u.Counter;

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE,
                MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (u.moveSpriteReturn != 0) {
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_PLAX_WALL:
                    KillSprite(Weapon);
                    return;
                case HIT_SPRITE: {
                    int wall_ang;

                    if (!TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                        PlaySound(DIGI_CHEMBOUNCE, sp, v3df_dontpan);
                    }

                    int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Sprite hsp = boardService.getSprite(hitsprite);
                    if (hsp == null) {
                        break;
                    }

                    if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                        wall_ang = NORM_ANGLE(hsp.getAng());
                        WallBounce(Weapon, wall_ang);
                        ScaleSpriteVector(Weapon, 32000);
                    } else {
                        // Canister pops when first smoke starts out
                        if (u.WaitTics == CHEMTICS && !TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                            PlaySound(DIGI_GASPOP, sp, v3df_dontpan | v3df_doppler);
                            VOC3D voc = PlaySound(DIGI_CHEMGAS, sp, v3df_dontpan | v3df_doppler);
                            Set3DSoundOwner(Weapon, voc);
                        }
                        u.xchange = u.ychange = 0;
                        u.WaitTics -= (MISSILEMOVETICS * 2);
                        if (u.WaitTics <= 0) {
                            KillSprite( Weapon);
                        }
                        return;
                    }

                    break;
                }
                case HIT_WALL: {
                    int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Wall wph = boardService.getWall(hitwall);
                    if (wph == null) {
                        break;
                    }

                    if (wph.getLotag() == TAG_WALL_BREAK) {
                        HitBreakWall(hitwall, sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), u.ID);
                        u.moveSpriteReturn = 0;
                        break;
                    }

                    if (!TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                        PlaySound(DIGI_CHEMBOUNCE, sp, v3df_dontpan);
                    }

                    Wall nw = wph.getWall2();
                    int wall_ang = NORM_ANGLE(EngineUtils.getAngle(nw.getX() - wph.getX(), nw.getY() - wph.getY()) + 512);

                    WallBounce(Weapon, wall_ang);
                    ScaleSpriteVector(Weapon, 32000);
                    break;
                }

                case HIT_SECTOR: {
                    boolean hitwall;

                    if (SlopeBounce(Weapon, tmp_ptr[0])) {
                        hitwall = tmp_ptr[0].value != 0;
                        if (hitwall) {
                            // hit a wall
                            ScaleSpriteVector(Weapon, 28000);
                            u.moveSpriteReturn = 0;
                            u.Counter = 0;
                        } else {
                            // hit a sector
                            if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                                // hit a floor
                                if (!TEST(u.Flags, SPR_BOUNCE)) {
                                    if (!TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                                        PlaySound(DIGI_CHEMBOUNCE, sp, v3df_dontpan);
                                    }
                                    u.Flags |= (SPR_BOUNCE);
                                    ScaleSpriteVector(Weapon, 32000); // was 18000
                                    u.zchange /= 6;
                                    u.moveSpriteReturn = 0;
                                    u.Counter = 0;
                                } else {
                                    // Canister pops when first smoke starts out
                                    if (u.WaitTics == CHEMTICS && !TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                                        PlaySound(DIGI_GASPOP, sp, v3df_dontpan | v3df_doppler);
                                        VOC3D voc = PlaySound(DIGI_CHEMGAS, sp, v3df_dontpan | v3df_doppler);
                                        Set3DSoundOwner(Weapon, voc);
                                    }
                                    SpawnRadiationCloud(Weapon);
                                    u.xchange = u.ychange = 0;
                                    u.WaitTics -= (MISSILEMOVETICS * 2);
                                    if (u.WaitTics <= 0) {
                                        KillSprite( Weapon);
                                    }
                                    return;
                                }
                            } else {
                                // hit a ceiling
                                ScaleSpriteVector(Weapon, 32000); // was 22000
                            }
                        }
                    } else {
                        // hit floor
                        if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                            if (TEST(u.Flags, SPR_UNDERWATER)) {
                                u.Flags |= (SPR_BOUNCE); // no bouncing
                            }
                            // underwater
                            Sect_User su = getSectUser(sp.getSectnum());
                            if (u.lo_sectp != -1 && su != null && su.depth != 0) {
                                u.Flags |= (SPR_BOUNCE); // no bouncing on
                            }
                            // shallow water

                            if (!TEST(u.Flags, SPR_BOUNCE)) {
                                if (!TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                                    PlaySound(DIGI_CHEMBOUNCE, sp, v3df_dontpan);
                                }
                                u.Flags |= (SPR_BOUNCE);
                                u.moveSpriteReturn = 0;
                                u.Counter = 0;
                                u.zchange = -u.zchange;
                                ScaleSpriteVector(Weapon, 32000); // Was 18000
                                u.zchange /= 6;
                            } else {
                                // Canister pops when first smoke starts out
                                if (u.WaitTics == CHEMTICS && !TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                                    PlaySound(DIGI_GASPOP, sp, v3df_dontpan | v3df_doppler);
                                    VOC3D voc = PlaySound(DIGI_CHEMGAS, sp, v3df_dontpan | v3df_doppler);
                                    Set3DSoundOwner(Weapon, voc);
                                }
                                // WeaponMoveHit(Weapon);
                                SpawnRadiationCloud(Weapon);
                                u.xchange = u.ychange = 0;
                                u.WaitTics -= (MISSILEMOVETICS * 2);
                                if (u.WaitTics <= 0) {
                                    KillSprite( Weapon);
                                }
                                return;
                            }
                        } else
                        // hit something above
                        {
                            u.zchange = -u.zchange;
                            ScaleSpriteVector(Weapon, 32000); // was 22000
                        }
                    }
                    break;
                }
            }
        }

        // if you haven't bounced or your going slow do some puffs
        if (!TEST(u.Flags, SPR_BOUNCE | SPR_UNDERWATER) && !TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
            int newsp = SpawnSprite(STAT_MISSILE, PUFF, s_Puff[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 100);

            Sprite np = boardService.getSprite(newsp);
            USER nu = getUser(newsp);
            if (np == null || nu == null) {
                return;
            }

            SetOwner(Weapon, newsp);
            np.setShade(-40);
            np.setXrepeat(40);
            np.setYrepeat(40);
            nu.ox = u.ox;
            nu.oy = u.oy;
            nu.oz = u.oz;
            // !Frank - dont do translucent
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
            np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

            nu.xchange = u.xchange;
            nu.ychange = u.ychange;
            nu.zchange = u.zchange;

            nu.spal = (byte) PALETTE_PLAYER6;
            np.setPal(nu.spal);

            ScaleSpriteVector(newsp, 20000);

            if (TEST(u.Flags, SPR_UNDERWATER)) {
                nu.Flags |= (SPR_UNDERWATER);
            }
        }
    }

    public static void DoCaltropsStick(int Weapon) {
        USER u = getUser(Weapon);
        if (u != null) {
            u.Counter = (u.Counter == 0) ? 1 : 0;
            if (u.Counter != 0) {
                DoFlamesDamageTest(Weapon);
            }
        }
    }

    public static void DoCaltrops(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            ScaleSpriteVector(Weapon, 50000);
            u.Counter += 20;
        } else {
            u.Counter += 70;
        }
        u.zchange += u.Counter;

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE,
                MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (u.moveSpriteReturn != 0) {
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_PLAX_WALL:
                    KillSprite(Weapon);
                    return;
                case HIT_SPRITE: {
                    int wall_ang;
                    PlaySound(DIGI_CALTROPS, sp, v3df_dontpan);

                    int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Sprite hsp = boardService.getSprite(hitsprite);

                    if (hsp != null && TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                        wall_ang = NORM_ANGLE(hsp.getAng());
                        WallBounce(Weapon, wall_ang);
                        ScaleSpriteVector(Weapon, 10000);
                    } else {
                        // fall to the ground
                        u.xchange = u.ychange = 0;
                    }

                    break;
                }

                case HIT_WALL: {
                    int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Wall wph = boardService.getWall(hitwall);
                    if (wph == null) {
                        return;
                    }

                    if (wph.getLotag() == TAG_WALL_BREAK) {
                        HitBreakWall(hitwall, sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), u.ID);
                        u.moveSpriteReturn = 0;
                        break;
                    }

                    PlaySound(DIGI_CALTROPS, sp, v3df_dontpan);

                    Wall nw = wph.getWall2();
                    int wall_ang = NORM_ANGLE(EngineUtils.getAngle(nw.getX() - wph.getX(), nw.getY() - wph.getY()) + 512);

                    WallBounce(Weapon, wall_ang);
                    ScaleSpriteVector(Weapon, 1000);
                    break;
                }

                case HIT_SECTOR: {
                    boolean hitwall;

                    if (SlopeBounce(Weapon, tmp_ptr[0])) {
                        hitwall = tmp_ptr[0].value != 0;
                        if (hitwall) {
                            // hit a wall
                            ScaleSpriteVector(Weapon, 1000);
                            u.moveSpriteReturn = 0;
                            u.Counter = 0;
                        } else {
                            // hit a sector
                            if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                                // hit a floor
                                if (!TEST(u.Flags, SPR_BOUNCE)) {
                                    PlaySound(DIGI_CALTROPS, sp, v3df_dontpan);
                                    u.Flags |= (SPR_BOUNCE);
                                    ScaleSpriteVector(Weapon, 1000); // was 18000
                                    u.moveSpriteReturn = 0;
                                    u.Counter = 0;
                                } else {
                                    u.xchange = u.ychange = 0;
                                    sp.setExtra(sp.getExtra() | (SPRX_BREAKABLE));
                                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BREAKABLE));
                                    ChangeState(Weapon, s_CaltropsStick[0]);
                                    return;
                                }
                            } else {
                                // hit a ceiling
                                ScaleSpriteVector(Weapon, 1000); // was 22000
                            }
                        }
                    } else {
                        // hit floor
                        if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                            if (TEST(u.Flags, SPR_UNDERWATER)) {
                                u.Flags |= (SPR_BOUNCE); // no bouncing
                            }
                            // underwater
                            Sect_User su = getSectUser(sp.getSectnum());
                            if (u.lo_sectp != -1 && su != null && su.depth != 0) {
                                u.Flags |= (SPR_BOUNCE); // no bouncing on
                            }
                            // shallow water

                            if (!TEST(u.Flags, SPR_BOUNCE)) {
                                PlaySound(DIGI_CALTROPS, sp, v3df_dontpan);
                                u.Flags |= (SPR_BOUNCE);
                                u.moveSpriteReturn = 0;
                                u.Counter = 0;
                                u.zchange = -u.zchange;
                                ScaleSpriteVector(Weapon, 1000); // Was 18000
                            } else {
                                u.xchange = u.ychange = 0;
                                sp.setExtra(sp.getExtra() | (SPRX_BREAKABLE));
                                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BREAKABLE));
                                ChangeState(Weapon, s_CaltropsStick[0]);
                                return;
                            }
                        } else
                        // hit something above
                        {
                            u.zchange = -u.zchange;
                            ScaleSpriteVector(Weapon, 1000); // was 22000
                        }
                    }
                    break;
                }
            }
        }

    }

    /////////////////////////////
    //
    // Deadly green gas clouds
    //
    /////////////////////////////
    public static void SpawnRadiationCloud(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (MoveSkip4 == 0) {
            return;
        }

        // This basically works like a MoveSkip8, if one existed
        if (u.ID == MUSHROOM_CLOUD || u.ID == 3121) {
            if ((u.Counter2++) > 16) {
                u.Counter2 = 0;
            }
        } else {
            if ((u.Counter2++) > 2) {
                u.Counter2 = 0;
            }
        }
        if (u.Counter2 != 0) {
            return;
        }

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            return;
        }

        int newsp = SpawnSprite(STAT_MISSILE, RADIATION_CLOUD, s_RadiationCloud[0], sp.getSectnum(), sp.getX(), sp.getY(),
                sp.getZ() - RANDOM_P2(Z(8)), sp.getAng(), 0);

        Sprite np = boardService.getSprite(newsp);
        USER nu = getUser(newsp);
        if (np == null || nu == null) {
            return;
        }

        SetOwner(sp.getOwner(), newsp);
        nu.WaitTics = 120;
        np.setShade(-40);
        np.setXrepeat(32);
        np.setYrepeat(32);
        np.setClipdist(sp.getClipdist());
        np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
        np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        nu.spal = (byte) PALETTE_PLAYER6;
        np.setPal(nu.spal);
        // Won't take floor palettes
        np.setHitag( SECTFU_DONT_COPY_PALETTE);

        if (RANDOM_P2(1024) < 512) {
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_XFLIP));
        }

        np.setAng( RANDOM_P2(2048));
        np.setXvel( RANDOM_P2(32));

        nu.Counter = 0;
        nu.Counter2 = 0;

        if (u.ID == MUSHROOM_CLOUD || u.ID == 3121) {
            nu.Radius = 2000;
            nu.xchange = (MOVEx(np.getXvel() >> 2, np.getAng()));
            nu.ychange = (MOVEy(np.getXvel() >> 2, np.getAng()));
            np.setZvel( (Z(1) + RANDOM_P2(Z(2))));
        } else {
            nu.xchange = MOVEx(np.getXvel(), np.getAng());
            nu.ychange = MOVEy(np.getXvel(), np.getAng());
            np.setZvel( (Z(4) + RANDOM_P2(Z(4))));
            nu.Radius = 4000;
        }

    }

    public static void DoRadiationCloud(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setX(sp.getX() + u.xchange);
        sp.setY(sp.getY() + u.ychange);
        sp.setZ(sp.getZ() - sp.getZvel());

        if (u.ID != 0) {
            DoFlamesDamageTest(SpriteNum);
        }
    }

    //////////////////////////////////////////////
    //
    // Inventory Chemical Bombs
    //
    //////////////////////////////////////////////
    public static void PlayerInitChemBomb(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        PlaySound(DIGI_THROW, pp, v3df_dontpan | v3df_doppler);

        int nx = pp.posx;
        int ny = pp.posy;
        int nz = pp.posz + pp.bob_z + Z(8);

        // Spawn a shot
        // Inserting and setting up variables
        int w = SpawnSprite(STAT_MISSILE, CHEMBOMB, s_ChemBomb[0], pp.cursectnum, nx, ny, nz, pp.getAnglei(),
                CHEMBOMB_VELOCITY);

        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        // don't throw it as far if crawling
        if (TEST(pp.Flags, PF_CRAWLING)) {
            wp.setXvel(wp.getXvel() - DIV4(wp.getXvel()));
        }

        wu.Flags |= (SPR_XFLIP_TOGGLE);

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(32);
        wp.setXrepeat(32);
        wp.setShade(-15);
        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 200;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wu.Counter = 0;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK));

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        wp.setZvel( ((100 - pp.getHorizi()) * HORIZ_MULT));

        Sprite psp = pp.getSprite();
        if (psp == null) {
            return;
        }

        int oclipdist = psp.getClipdist();
        psp.setClipdist(0);
        wp.setClipdist(0);

        MissileSetPos(w, DoChemBomb, 1000);

        psp.setClipdist(oclipdist);
        wp.setClipdist(80 >> 2);

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel() >> 1;

        // adjust xvel according to player velocity
        wu.xchange += pp.xvect >> 14;
        wu.ychange += pp.yvect >> 14;

        // Smoke will come out for this many seconds
        wu.WaitTics =  CHEMTICS;
    }

    public static void InitSpriteChemBomb(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        PlaySound(DIGI_THROW, sp, v3df_dontpan | v3df_doppler);

        int nx = sp.getX();
        int ny = sp.getY();
        int nz = sp.getZ();

        // Spawn a shot
        // Inserting and setting up variables
        int w = SpawnSprite(STAT_MISSILE, CHEMBOMB, s_ChemBomb[0], sp.getSectnum(), nx, ny, nz, sp.getAng(), CHEMBOMB_VELOCITY);

        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        wu.Flags |= (SPR_XFLIP_TOGGLE);

        SetOwner(SpriteNum, w);
        wp.setYrepeat(32);
        wp.setXrepeat(32);
        wp.setShade(-15);
        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 200;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wu.Counter = 0;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK));

        wp.setZvel( ((-100 - RANDOM_RANGE(100)) * HORIZ_MULT));

        wp.setClipdist(80 >> 2);

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel() >> 1;

        // Smoke will come out for this many seconds
        wu.WaitTics =  CHEMTICS;

    }

    public static void InitChemBomb(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int nx = sp.getX();
        int ny = sp.getY();
        int nz = sp.getZ();

        // Spawn a shot
        // Inserting and setting up variables
        int w = SpawnSprite(STAT_MISSILE, MUSHROOM_CLOUD, s_ChemBomb[0], sp.getSectnum(), nx, ny, nz, sp.getAng(),
                CHEMBOMB_VELOCITY);

        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        wu.Flags |= (SPR_XFLIP_TOGGLE);

        SetOwner(sp.getOwner(), w); // !FRANK
        wp.setYrepeat(32);
        wp.setXrepeat(32);
        wp.setShade(-15);
        wu.Radius = 200;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wu.Counter = 0;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER | CSTAT_SPRITE_INVISIBLE)); // Make nuke radiation
        // invis.
        wp.setCstat(wp.getCstat() & ~(CSTAT_SPRITE_BLOCK));

        if (SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        wp.setZvel( ((-100 - RANDOM_RANGE(100)) * HORIZ_MULT));
        wp.setClipdist(0);

        if (u.ID == MUSHROOM_CLOUD || u.ID == 3121 || u.ID == SUMO_RUN_R0) // 3121 == GRENADE_EXP
        {
            wu.xchange = 0;
            wu.ychange = 0;
            wu.zchange = 0;
            wp.setXvel(0);
            wp.setYvel(0);
            wp.setZvel(0);
            // Smoke will come out for this many seconds
            wu.WaitTics = 40 * 120;
        } else {
            wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
            wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
            wu.zchange = wp.getZvel() >> 1;
            // Smoke will come out for this many seconds
            wu.WaitTics = 3 * 120;
        }
    }

    //////////////////////////////////////////////
    //
    // Inventory Flash Bombs
    //
    //////////////////////////////////////////////
    public static void PlayerInitFlashBomb(PlayerStr pp) {
        PlaySound(DIGI_GASPOP, pp, v3df_dontpan | v3df_doppler);

        // Set it just a little to let player know what he just did
        SetFadeAmt(pp, -30, 1); // White flash

        Sprite sp = pp.getSprite();
        if (sp == null) {
            return;
        }

        ListNode<Sprite> nexti;
        for (int j : StatDamageList) {
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite hp = node.get();
                USER hu = getUser(i);
                if (hu == null) {
                    continue;
                }

                if (i == pp.PlayerSprite) {
                    break;
                }

                int dist = DISTANCE(hp.getX(), hp.getY(), sp.getX(), sp.getY());
                if (dist > 16384) // Flash radius
                {
                    continue;
                }

                if (!TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK)) {
                    continue;
                }

                if (!FAFcansee(hp.getX(), hp.getY(), hp.getZ(), hp.getSectnum(), sp.getX(), sp.getY(), sp.getZ() - SPRITEp_SIZE_Z(sp), sp.getSectnum())) {
                    continue;
                }

                int damage = GetDamage(i, pp.PlayerSprite, DMG_FLASHBOMB);

                if (hu.sop_parent != -1) {
                    break;
                } else if (hu.PlayerP != -1) {

                    if (damage < -70) {
                        int choosesnd = RANDOM_RANGE(MAX_PAIN);

                        PlayerSound(PlayerLowHealthPainVocs[choosesnd], v3df_dontpan | v3df_doppler | v3df_follow, pp);
                    }
                    SetFadeAmt(Player[hu.PlayerP], damage, 1); // White flash
                } else {
                    ActorPain(i);
                    SpawnFlashBombOnActor(i);
                }
            }
        }

    }

    public static void InitFlashBomb(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        PlayerStr pp = Player[screenpeek];
        if (sp == null) {
            return;
        }

        PlaySound(DIGI_GASPOP, sp, v3df_dontpan | v3df_doppler);

        ListNode<Sprite> nexti;
        for (int j : StatDamageList) {
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();

                Sprite hp = node.get();
                USER hu = getUser(i);
                if (hu == null) {
                    continue;
                }

                int dist = DISTANCE(hp.getX(), hp.getY(), sp.getX(), sp.getY());
                if (dist > 16384) // Flash radius
                {
                    continue;
                }

                if (!TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK)) {
                    continue;
                }

                if (!FAFcansee(hp.getX(), hp.getY(), hp.getZ(), hp.getSectnum(), sp.getX(), sp.getY(), sp.getZ() - SPRITEp_SIZE_Z(sp), sp.getSectnum())) {
                    continue;
                }

                int damage = GetDamage(i, SpriteNum, DMG_FLASHBOMB);

                if (hu.sop_parent != -1) {
                    break;
                } else if (hu.PlayerP != -1) {
                    if (damage < -70) {
                        int choosesnd = RANDOM_RANGE(MAX_PAIN);
                        PlayerSound(PlayerLowHealthPainVocs[choosesnd], v3df_dontpan | v3df_doppler | v3df_follow, pp);
                    }
                    SetFadeAmt(Player[hu.PlayerP], damage, 1); // White flash
                } else {
                    if (i != SpriteNum) {
                        ActorPain(i);
                        SpawnFlashBombOnActor(i);
                    }
                }
            }
        }
    }

    // This is a sneaky function to make actors look blinded by flashbomb while
    // using flaming code
    public static void SpawnFlashBombOnActor(int enemy) {
        Sprite ep = boardService.getSprite(enemy);
        USER eu = getUser(enemy);

        // Forget about burnable sprites
        if (ep == null || TEST(ep.getExtra(), SPRX_BURNABLE)) {
            return;
        }

        if (eu != null) {
            if (eu.flame >= 0) {
                int sizez = SPRITEp_SIZE_Z(ep) + DIV4(SPRITEp_SIZE_Z(ep));

                Sprite np = boardService.getSprite(eu.flame);
                USER nu = getUser(eu.flame);
                if (np == null || nu == null) {
                    return;
                }

                if (nu.Counter >= SPRITEp_SIZE_Z_2_YREPEAT(np, sizez)) {
                    // keep flame only slightly bigger than the enemy itself
                    nu.Counter =  (SPRITEp_SIZE_Z_2_YREPEAT(np, sizez) * 2);
                } else {
                    // increase max size
                    nu.Counter += SPRITEp_SIZE_Z_2_YREPEAT(np, 8 << 8) * 2;
                }

                // Counter is max size
                if (nu.Counter >= 230) {
                    // this is far too big
                    nu.Counter = 230;
                }

                if (nu.WaitTics < 2 * 120) {
                    nu.WaitTics = 2 * 120; // allow it to grow again
                }

                return;
            }
        }

        int newsp = SpawnSprite(STAT_MISSILE, FIREBALL_FLAMES, s_FireballFlames[0], ep.getSectnum(), ep.getX(), ep.getY(), ep.getZ(), ep.getAng(),
                0);
        Sprite np = boardService.getSprite(newsp);
        USER nu = getUser(newsp);
        if (np == null || nu == null) {
            return;
        }

        if (eu != null) {
            eu.flame =  newsp;
        }

        np.setXrepeat(16);
        np.setYrepeat(16);

        if (enemy >= 0) {
            nu.Counter =  (SPRITEp_SIZE_Z_2_YREPEAT(np, SPRITEp_SIZE_Z(ep) >> 1) * 4);
        } else {
            nu.Counter = 0; // max flame size
        }

        np.setShade(-40);
        np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER | CSTAT_SPRITE_INVISIBLE));
        np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        nu.Radius = 200;

        if (enemy >= 0) {
            SetAttach(enemy, newsp);
        }
    }

    //////////////////////////////////////////////
    //
    // Inventory Caltrops
    //
    //////////////////////////////////////////////
    public static void PlayerInitCaltrops(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        PlaySound(DIGI_THROW, pp, v3df_dontpan | v3df_doppler);

        int nx = pp.posx;
        int ny = pp.posy;
        int nz = pp.posz + pp.bob_z + Z(8);

        // Throw out several caltrops
        // for(i=0;i<3;i++)
        // {
        // Spawn a shot
        // Inserting and setting up variables
        int w = SpawnSprite(STAT_DEAD_ACTOR, CALTROPS, s_Caltrops[0], pp.cursectnum, nx, ny, nz, pp.getAnglei(),
                (CHEMBOMB_VELOCITY + RANDOM_RANGE(CHEMBOMB_VELOCITY)) / 2);

        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        // don't throw it as far if crawling
        if (TEST(pp.Flags, PF_CRAWLING)) {
            wp.setXvel(wp.getXvel() - DIV4(wp.getXvel()));
        }

        wu.Flags |= (SPR_XFLIP_TOGGLE);

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(64);
        wp.setXrepeat(64);
        wp.setShade(-15);
        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 200;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wu.Counter = 0;

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        wp.setZvel( ((100 - pp.getHorizi()) * HORIZ_MULT));

        Sprite psp = pp.getSprite();
        if (psp == null) {
            return;
        }

        int oclipdist =  psp.getClipdist();
        psp.setClipdist(0);
        wp.setClipdist(0);

        MissileSetPos(w, DoCaltrops, 1000);

        psp.setClipdist(oclipdist);
        wp.setClipdist(80 >> 2);

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel() >> 1;

        // adjust xvel according to player velocity
        wu.xchange += pp.xvect >> 14;
        wu.ychange += pp.yvect >> 14;

        SetupSpriteForBreak(wp); // Put Caltrops in the break queue
    }

    public static void InitCaltrops(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        PlaySound(DIGI_THROW, sp, v3df_dontpan | v3df_doppler);

        int nx = sp.getX();
        int ny = sp.getY();
        int nz = sp.getZ();

        // Spawn a shot
        // Inserting and setting up variables
        int w = SpawnSprite(STAT_DEAD_ACTOR, CALTROPS, s_Caltrops[0], sp.getSectnum(), nx, ny, nz, sp.getAng(),
                CHEMBOMB_VELOCITY / 2);

        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        wu.Flags |= (SPR_XFLIP_TOGGLE);

        SetOwner(SpriteNum, w);
        wp.setYrepeat(64);
        wp.setXrepeat(64);
        wp.setShade(-15);
        // !FRANK - clipbox must be <= weapon otherwise can clip thru walls
        wp.setClipdist(sp.getClipdist());
        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 200;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wu.Counter = 0;

        wp.setZvel( ((-100 - RANDOM_RANGE(100)) * HORIZ_MULT));

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel() >> 1;

        SetupSpriteForBreak(wp); // Put Caltrops in the break queue
    }

    ////////////////// DEATHFLAG!
    ////////////////// ////////////////////////////////////////////////////////////////
    // Rules: Run to an enemy flag, run over it an it will stick to you.
    // The goal is to run the enemy's flag back to your startpoint.
    // If an enemy flag touches a friendly start sector, then the opposing team
    ////////////////// explodes and
    // your team wins and the level restarts.
    // Once you pick up a flag, you have 30 seconds to score, otherwise, the flag
    ////////////////// detonates
    // an explosion, killing you and anyone in the vicinity, and you don't score.
    //////////////////////////////////////////////////////////////////////////////////////////////

    public static void InitPhosphorus(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        PlaySound(DIGI_FIREBALL1, sp, v3df_follow);

        int nx = sp.getX();
        int ny = sp.getY();
        int nz = sp.getZ();
        int daang = NORM_ANGLE(RANDOM_RANGE(2048));

        // Spawn a shot
        // Inserting and setting up variables
        int w = SpawnSprite(STAT_SKIP4, FIREBALL1, s_Phosphorus[0], sp.getSectnum(), nx, ny, nz, daang,
                CHEMBOMB_VELOCITY / 3);

        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        wp.setHitag(LUMINOUS); // Always full brightness
        wu.Flags |= (SPR_XFLIP_TOGGLE);
        // !Frank - don't do translucent
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setShade(-128);

        wp.setYrepeat(64);
        wp.setXrepeat(64);
        wp.setShade(-15);
        // !FRANK - clipbox must be <= weapon otherwise can clip thru walls
        if (sp.getClipdist() > 0) {
            wp.setClipdist(sp.getClipdist() - 1);
        } else {
            wp.setClipdist(sp.getClipdist());
        }
        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 600;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wu.Counter = 0;

        wp.setZvel( ((-100 - RANDOM_RANGE(100)) * HORIZ_MULT));

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = (wp.getZvel() >> 1);
    }

    public static void InitBloodSpray(int SpriteNum, boolean dogib, int velocity) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int cnt = 1;
        if (dogib) {
            cnt = RANDOM_RANGE(3) + 1;
        }

        int rnd = RANDOM_RANGE(1000);
        if (rnd > 650) {
            PlaySound(DIGI_GIBS1, sp, v3df_none);
        } else if (rnd > 350) {
            PlaySound(DIGI_GIBS2, sp, v3df_none);
        } else {
            PlaySound(DIGI_GIBS3, sp, v3df_none);
        }

        int ang = sp.getAng();
        int vel = velocity;

        for (int i = 0; i < cnt; i++) {

            if (velocity == -1) {
                vel = 105 + RANDOM_RANGE(320);
            } else if (velocity == -2) {
                vel = 105 + RANDOM_RANGE(100);
            }

            if (dogib) {
                ang = NORM_ANGLE(ang + 512 + RANDOM_RANGE(200));
            } else {
                ang = NORM_ANGLE(ang + 1024 + 256 - RANDOM_RANGE(256));
            }

            int nx = sp.getX();
            int ny = sp.getY();
            int nz = SPRITEp_TOS(sp) - 20;

            // Spawn a shot
            // Inserting and setting up variables
            int w = SpawnSprite(STAT_MISSILE, GOREDrip, s_BloodSprayChunk[0], sp.getSectnum(), nx, ny, nz, ang, vel * 2);

            Sprite wp = boardService.getSprite(w);
            USER wu = getUser(w);
            if (wp == null || wu == null) {
                return;
            }

            wu.Flags |= (SPR_XFLIP_TOGGLE);
            if (dogib) {
                wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
            } else {
                wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER | CSTAT_SPRITE_INVISIBLE));
            }
            wp.setShade(-12);

            SetOwner(SpriteNum, w);
            wp.setYrepeat( (64 - RANDOM_RANGE(35)));
            wp.setXrepeat( (64 - RANDOM_RANGE(35)));
            wp.setShade(-15);
            wp.setClipdist(sp.getClipdist());
            wu.WeaponNum = u.WeaponNum;
            wu.Radius = 600;
            wu.ceiling_dist =  Z(3);
            wu.floor_dist =  Z(3);
            wu.Counter = 0;

            wp.setZvel( ((-10 - RANDOM_RANGE(50)) * HORIZ_MULT));

            wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
            wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
            wu.zchange = wp.getZvel() >> 1;

            if (!GlobalSkipZrange) {
                DoActorZrange(w);
            }
        }
    }

    public static void BloodSprayFall(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null) {
            sp.setZ(sp.getZ() + 1500);
        }
    }

    // Update the scoreboard for team color that just scored.
    public static void DoFlagScore(int pal) {
        for (ListNode<Sprite> node = boardService.getStatNode(0); node != null; node = node.getNext()) {
            Sprite sp = node.get();

            if (sp.getPicnum() < 1900 || sp.getPicnum() > 1999) {
                continue;
            }

            if (sp.getPal() == pal) {
                sp.setPicnum(sp.getPicnum() + 1); // Increment the counter
            }

            if (sp.getPicnum() > 1999) {
                sp.setPicnum(1900); // Roll it over if you must
            }
        }
    }

    public static int DoFlagRangeTest(int Weapon, int range) {
        Sprite wp = boardService.getSprite(Weapon);
        if (wp == null) {
            return 0;
        }

        ListNode<Sprite> nexti;
        for (int i : StatDamageList) {
            for (ListNode<Sprite> node = boardService.getStatNode(i); node != null; node = nexti) {
                nexti = node.getNext();
                Sprite sp = node.get();
                int dist = DISTANCE(sp.getX(), sp.getY(), wp.getX(), wp.getY());

                if (dist > range) {
                    continue;
                }

                if (sp == wp) {
                    continue;
                }

                if (!TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK)) {
                    continue;
                }

                if (!TEST(sp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    continue;
                }

                if (!FAFcansee(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), wp.getX(), wp.getY(), wp.getZ(), wp.getSectnum())) {
                    continue;
                }

                dist = FindDistance3D(wp.getX() - sp.getX(), wp.getY() - sp.getY(), (wp.getZ() - sp.getZ()) >> 4);
                if (dist > range) {
                    continue;
                }

                return (node.getIndex()); // Return the spritenum
            }
        }

        return (-1); // -1 for no sprite index. Not
        // found.
    }

    public static void DoCarryFlag(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        Sprite fp = boardService.getSprite(u.FlagOwner);

        // if no owner then die
        Sprite ap = boardService.getSprite(u.Attach);
        if (ap != null) {
            engine.setspritez( Weapon, ap.getX(), ap.getY(), SPRITEp_MID(ap));
            sp.setAng(NORM_ANGLE(ap.getAng() + 1536));
        }

        // not activated yet
        if (!TEST(u.Flags, SPR_ACTIVE)) {
            if ((u.WaitTics -= (MISSILEMOVETICS * 2)) > 0) {
                return;
            }

            // activate it
            u.WaitTics =  SEC(30); // You have 30 seconds to get it to
            // scorebox
            u.Counter2 = 0;
            u.Flags |= (SPR_ACTIVE);
        }

        // limit the number of times DoFlagRangeTest is called
        u.Counter++;
        if (u.Counter > 1) {
            u.Counter = 0;
        }

        if (u.Counter == 0) {
            // not already in detonate state
            if (u.Counter2 < FLAG_DETONATE_STATE) {
                ap = boardService.getSprite(u.Attach);
                USER au = getUser(u.Attach);

                if (au == null || au.Health <= 0) {
                    u.Counter2 = FLAG_DETONATE_STATE;
                    u.WaitTics =  (SEC(1) / 2);
                }
                // if in score box, score.
                if (ap != null) {
                    ru.m210projects.Build.Types.Sector sec = boardService.getSector(ap.getSectnum());
                    if (fp != null && sec != null && sec.getHitag() == 9000 && sec.getLotag() == ap.getPal() && ap.getPal() != sp.getPal()) {
                        if (u.FlagOwner >= 0) {
                            if (fp.getLotag() != 0) // Trigger everything if there is a
                            // lotag
                            {
                                DoMatchEverything(null, fp.getLotag(), ON);
                            }
                        }

                        if (!TEST_BOOL1(fp)) {
                            PlaySound(DIGI_BIGITEM, ap, v3df_none);
                            DoFlagScore(ap.getPal());
                            if (SP_TAG5(fp) > 0) {
                                fp.setDetail(fp.getDetail() + 1);
                                if (fp.getDetail() >= SP_TAG5(fp)) {
                                    fp.setDetail(0);
                                    DoMatchEverything(null, SP_TAG6(fp), ON);
                                }
                            }
                        }
                        SetSuicide(Weapon); // Kill the flag, you scored!
                    }
                }
            } else {
                // Time's up! Move directly to detonate state
                u.Counter2 = FLAG_DETONATE_STATE;
                u.WaitTics =  (SEC(1) / 2);
            }

        }

        u.WaitTics -= (MISSILEMOVETICS * 2);

        switch (u.Counter2) {
            case 0:
                if (u.WaitTics < SEC(30)) {
                    PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                    u.Counter2++;
                }
                break;
            case 1:
                if (u.WaitTics < SEC(20)) {
                    PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                    u.Counter2++;
                }
                break;
            case 2:
                if (u.WaitTics < SEC(10)) {
                    PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                    u.Counter2++;
                }
                break;
            case 3:
                if (u.WaitTics < SEC(5)) {
                    PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                    u.Counter2++;
                }
                break;
            case 4:
                if (u.WaitTics < SEC(4)) {
                    PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                    u.Counter2++;
                }
                break;
            case 5:
                if (u.WaitTics < SEC(3)) {
                    PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                    u.Counter2++;
                }
                break;
            case 6:
                if (u.WaitTics < SEC(2)) {
                    PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                    u.Counter2 = FLAG_DETONATE_STATE;
                }
                break;
            case FLAG_DETONATE_STATE:
                // start frantic beeping
                PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                u.Counter2++;
                break;
            case FLAG_DETONATE_STATE + 1:
                SpawnGrenadeExp(Weapon);
                SetSuicide(Weapon);
                break;
        }
    }

    public static void DoCarryFlagNoDet(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        Sprite fp = boardService.getSprite(u.FlagOwner);
        USER fu = getUser(u.FlagOwner);

        if (fu != null && u.FlagOwner >= 0) {
            fu.WaitTics = 30 * 120; // Keep setting respawn tics so it
        }

        // won't respawn
        USER au = null;
        // if no owner then die
        Sprite ap = boardService.getSprite(u.Attach);
        if (ap != null) {
            au = getUser(u.Attach);
            engine.setspritez( Weapon, ap.getX(), ap.getY(), SPRITEp_MID(ap));
            sp.setAng(NORM_ANGLE(ap.getAng() + 1536));
            sp.setZ(ap.getZ() - DIV2(SPRITEp_SIZE_Z(ap)));
        }

        if (au == null || au.Health <= 0) {
            if (fu != null && u.FlagOwner >= 0) {
                fu.WaitTics = 0; // Tell it to respawn
            }
            SetSuicide(Weapon);
            return;
        }

        ru.m210projects.Build.Types.Sector sec = boardService.getSector(ap.getSectnum());

        // if in score box, score.
        if (sec != null && fp != null && sec.getHitag() == 9000 && sec.getLotag() == ap.getPal() && ap.getPal() != sp.getPal()) {
            if (fu != null && u.FlagOwner >= 0) {
                if (fp.getLotag() != 0) {// Trigger everything if there is a lotag
                    DoMatchEverything(null, fp.getLotag(), ON);
                }
                fu.WaitTics = 0; // Tell it to respawn
            }

            if (!TEST_BOOL1(fp)) {
                PlaySound(DIGI_BIGITEM, ap, v3df_none);
                DoFlagScore(ap.getPal());
                if (SP_TAG5(fp) > 0) {
                    fp.setDetail(fp.getDetail() + 1);
                    if (fp.getDetail() >= SP_TAG5(fp)) {
                        fp.setDetail(0);
                        DoMatchEverything(null, SP_TAG6(fp), ON);
                    }
                }
            }
            SetSuicide(Weapon); // Kill the flag, you scored!
        }
    }

    public static void SetCarryFlag(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        // stuck
        u.Flags |= (SPR_BOUNCE);
        // not yet active for 1 sec
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        u.Counter = 0;
        change_sprite_stat(Weapon, STAT_ITEM);
        if (sp.getHitag() == 1) {
            ChangeState(Weapon, s_CarryFlagNoDet[0]);
        } else {
            ChangeState(Weapon, s_CarryFlag[0]);
        }
    }

    public static void DoFlag(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        int hitsprite = DoFlagRangeTest(Weapon, 1000);
        Sprite hsp = boardService.getSprite(hitsprite);
        if (hsp != null) {
            SetCarryFlag(Weapon);

            // check to see if sprite is player or enemy
            if (TEST(hsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                // attach weapon to sprite
                sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                SetAttach(hitsprite, Weapon);
                u.sz = hsp.getZ() - DIV2(SPRITEp_SIZE_Z(hsp));
            }
        }
    }

    public static void InitShell(int SpriteNum, int ShellNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int id = 0, velocity = 0;
        State p = null;
        int nx = sp.getX();
        int ny = sp.getY();
        int nz = DIV2(SPRITEp_TOS(sp) + SPRITEp_BOS(sp));

        switch (ShellNum) {
            case -2:
            case -3:
                id = UZI_SHELL;
                p = s_UziShellShrap[0];
                velocity =  (1500 + RANDOM_RANGE(1000));
                break;
            case -4:
                id = SHOT_SHELL;
                p = s_ShotgunShellShrap[0];
                velocity =  (2000 + RANDOM_RANGE(1000));
                break;
        }

        int w = SpawnSprite(STAT_SKIP4, id, p, sp.getSectnum(), nx, ny, nz, sp.getAng(), 64);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        wp.setZvel( -(velocity));
        if (u.PlayerP != -1) {
            wp.setZ(wp.getZ() + ((100 - Player[u.PlayerP].getHorizi()) * (HORIZ_MULT / 3)));
        }

        switch (wu.ID) {
            case UZI_SHELL:
                wp.setZ(wp.getZ() - Z(13));

                if (ShellNum == -3) {
                    wp.setAng(sp.getAng());
                    HelpMissileLateral(w, 2500);
                    wp.setAng(NORM_ANGLE(wp.getAng() - 512));
                    HelpMissileLateral(w, 1000); // Was 1500
                    wp.setAng(NORM_ANGLE(wp.getAng() + 712));
                } else {
                    wp.setAng(sp.getAng());
                    HelpMissileLateral(w, 2500);
                    wp.setAng(NORM_ANGLE(wp.getAng() + 512));
                    HelpMissileLateral(w, 1500);
                    wp.setAng(NORM_ANGLE(wp.getAng() - 128));
                }
                wp.setAng(wp.getAng() + (RANDOM_P2(128 << 5) >> 5) - DIV2(128));
                wp.setAng(NORM_ANGLE(wp.getAng()));

                // Set the shell number
                wu.ShellNum = ShellCount;
                wp.setXrepeat(13);
                wp.setYrepeat(13);
                break;
            case SHOT_SHELL:
                wp.setZ(wp.getZ() - Z(13));
                wp.setAng(sp.getAng());
                HelpMissileLateral(w, 2500);
                wp.setAng(NORM_ANGLE(wp.getAng() + 512));
                HelpMissileLateral(w, 1300);
                wp.setAng(NORM_ANGLE(wp.getAng() - 128 - 64));
                wp.setAng(wp.getAng() + (RANDOM_P2(128 << 5) >> 5) - DIV2(128));
                wp.setAng(NORM_ANGLE(wp.getAng()));

                // Set the shell number
                wu.ShellNum = ShellCount;
                wp.setXrepeat(18);
                wp.setYrepeat(18);
                break;
        }

        SetOwner(SpriteNum, w);
        wp.setShade(-15);
        wu.ceiling_dist =  Z(1);
        wu.floor_dist =  Z(1);
        wu.Counter = 0;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        wu.Flags &= ~(SPR_BOUNCE | SPR_UNDERWATER); // Make em' bounce

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

        wu.jump_speed = 200;
        wu.jump_speed += RANDOM_RANGE(400);
        wu.jump_speed =  -wu.jump_speed;

        DoBeginJump(w);
        wu.jump_grav = ACTOR_GRAVITY;
    }

    public static void JWeaponSaveable() {
        SaveData(BloodSprayFall);
        SaveData(DoRadiationCloud);
        SaveData(DoChemBomb);
        SaveData(DoCaltrops);
        SaveData(DoCaltropsStick);
        SaveData(DoFlag);
        SaveData(DoCarryFlag);
        SaveData(DoCarryFlagNoDet);
        SaveData(DoPhosphorus);
        SaveData(DoBloodSpray);
        SaveData(DoWallBloodDrip);
        SaveData(s_BloodSpray);
        SaveData(s_PhosphorExp);
        SaveData(s_NukeMushroom);
        SaveData(s_RadiationCloud);
        SaveData(s_ChemBomb);
        SaveData(s_Caltrops);
        SaveData(s_CaltropsStick);
        SaveData(s_CarryFlag);
        SaveData(s_CarryFlagNoDet);
        SaveData(s_Flag);
        SaveData(s_Phosphorus);
        SaveData(s_BloodSprayChunk);
        SaveData(s_BloodSprayDrip);
    }
}
