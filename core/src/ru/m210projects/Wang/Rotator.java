package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Point;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Sound.SoundType;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Wang.Game.Distance;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.STAT_ROTATOR;
import static ru.m210projects.Wang.Names.STAT_ROTATOR_PIVOT;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Sprites.KillSprite;
import static ru.m210projects.Wang.Stag.SECT_LOCK_DOOR;
import static ru.m210projects.Wang.Stag.SECT_ROTATOR;
import static ru.m210projects.Wang.Text.KeyDoorMessage;
import static ru.m210projects.Wang.Text.PutStringInfo;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Vator.VatorSwitch;

public class Rotator {

    public static final Animator DoRotator = new Animator((Animator.Runnable) Rotator::DoRotator);

    public static void ReverseRotator(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        RotatorStr r = u.getRotator();
        // if paused go ahead and start it up again
        if (u.Tics != 0) {
            u.Tics = 0;
            SetRotatorActive(SpriteNum);
            return;
        }

        // moving toward to OFF pos
        if (r.tgt == 0) {
            r.tgt = r.open_dest;
        } else if (r.tgt == r.open_dest) {
            r.tgt = 0;
        }

        r.vel = -r.vel;
    }

    public static void SetRotatorActive(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        RotatorStr r = u.getRotator();

        // play activate sound
        DoSoundSpotMatch(SP_TAG2(sp), 1, SoundType.SOUND_OBJECT_TYPE);

        u.Flags |= (SPR_ACTIVE);
        u.Tics = 0;

        // moving to the OFF position
        if (r.tgt == 0) {
            VatorSwitch(SP_TAG2(sp), OFF);
        } else {
            VatorSwitch(SP_TAG2(sp), ON);
        }
    }

    public static void SetRotatorInactive(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // play inactivate sound
        DoSoundSpotMatch(SP_TAG2(sp), 2, SoundType.SOUND_OBJECT_TYPE);

        u.Flags &= ~(SPR_ACTIVE);
    }

    // called for operation from the space bar
    public static void DoRotatorOperate(PlayerStr pp, int sectnum) {
        ru.m210projects.Build.Types.Sector sec = boardService.getSector(sectnum);
        if (sec == null) {
            return;
        }

        int match = sec.getHitag();
        if (match > 0) {
            if (TestRotatorMatchActive(match)) {
                DoRotatorMatch(pp, match, true);
            }
        }
    }

    // called from switches and triggers
    // returns first vator found
    public static void DoRotatorMatch(PlayerStr pp, int match, boolean manual) {
        USER fu;
        Sprite fsp;
        int sectnum;
        int first_vator = -1;

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ROTATOR); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            fsp = node.get();

            if (SP_TAG1(fsp) == SECT_ROTATOR && SP_TAG2(fsp) == match) {
                fu = getUser(i);

                // single play only vator
                // boolean 8 must be set for message to display
                if (TEST_BOOL4(fsp) && (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT
                        || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_AI_BOTS)) {
                    if (pp != null && TEST_BOOL11(fsp)) {
                        PutStringInfo(pp, "This only opens in single play.");
                    }
                    continue;
                }

                // switch trigger only
                if (SP_TAG3(fsp) == 1) {
                    // tried to manually operat a switch/trigger only
                    if (manual) {
                        continue;
                    }
                }

                if (first_vator == -1) {
                    first_vator = i;
                }

                sectnum = fsp.getSectnum();
                Sect_User su = getSectUser(sectnum);

                if (pp != null && su != null && su.stag == SECT_LOCK_DOOR
                        && su.number != 0) {
                    int key_num = su.number;

                    PutStringInfo(pp, KeyDoorMessage[key_num - 1]);
                    return;
                }

                if (fu != null && TEST(fu.Flags, SPR_ACTIVE)) {
                    ReverseRotator(i);
                    continue;
                }

                SetRotatorActive(i);
            }
        }

    }

    public static boolean TestRotatorMatchActive(int match) {
        USER fu;
        Sprite fsp;

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ROTATOR); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            fsp = node.get();

            if (SP_TAG1(fsp) == SECT_ROTATOR && SP_TAG2(fsp) == match) {
                fu = getUser(i);

                // Does not have to be inactive to be operated
                if (fu == null || TEST_BOOL6(fsp)) {
                    continue;
                }

                if (TEST(fu.Flags, SPR_ACTIVE) || fu.Tics != 0) {
                    return (false);
                }
            }
        }

        return (true);
    }

    private static void DoRotatorMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        RotatorStr r;
        int ndx;
        Sprite pivot = null;
        int dist, closest;
        boolean kill = false;

        r = u.getRotator();

        // Example - ang pos moves from 0 to 512 <<OR>> from 0 to -512

        // control SPEED of swinging
        if (r.pos < r.tgt) {
            // Increment swing angle
            r.pos += r.speed;
            r.speed += r.vel;

            // if the other way make it equal
            if (r.pos > r.tgt) {
                r.pos = r.tgt;
            }
        }

        if (r.pos > r.tgt) {
            // Increment swing angle
            r.pos -= r.speed;
            r.speed += r.vel;

            // if the other way make it equal
            if (r.pos < r.tgt) {
                r.pos = r.tgt;
            }
        }

        if (r.pos == r.tgt) {
            // If ang is OPEN
            if (r.pos == r.open_dest) {
                // new tgt is CLOSED (0)
                r.tgt = 0;
                r.vel = -r.vel;
                SetRotatorInactive(SpriteNum);

                if (SP_TAG6(sp) != 0) {
                    DoMatchEverything(null, SP_TAG6(sp), -1);
                }

                // wait a bit and close it
                if (u.WaitTics != 0) {
                    u.Tics = u.WaitTics;
                }
            } else
                // If ang is CLOSED then
                if (r.pos == 0) {
                    // new tgt is OPEN (open)
                    r.tgt = r.open_dest;
                    r.speed = r.orig_speed;
                    r.vel = klabs(r.vel);

                    SetRotatorInactive(SpriteNum);
                    if (SP_TAG6(sp) != 0 && TEST_BOOL5(sp)) {
                        DoMatchEverything(null, SP_TAG6(sp), -1);
                    }
                }

            if (TEST_BOOL2(sp)) {
                kill = true;
            }
        }

        closest = 99999;

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ROTATOR_PIVOT); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite spr = node.get();

            if (spr.getLotag() == sp.getLotag()) {
                dist = Distance(sp.getX(), sp.getY(), spr.getX(), spr.getY());
                if (dist < closest) {
                    closest = dist;
                    pivot = spr;
                }
            }
        }

        if (pivot == null) {
            return;
        }

        ru.m210projects.Build.Types.Sector sec = boardService.getSector(sp.getSectnum());

        if (sec != null) {
            ndx = 0;
            // move points
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Point p = EngineUtils.rotatepoint(pivot.getX(), pivot.getY(), r.orig[ndx].x, r.orig[ndx].y, r.pos);
                engine.dragpoint(wn.getIndex(), p.getX(), p.getY());
                ndx++;
            }
        }

        if (kill) {
            SetRotatorInactive(SpriteNum);
            KillSprite(SpriteNum);
        }
    }

    public static void DoRotator(int SpriteNum) {
        // could move this inside sprite control
        DoRotatorMove(SpriteNum);
    }

}
