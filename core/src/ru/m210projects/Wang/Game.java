package ru.m210projects.Wang;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.osd.CommandResponse;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.commands.OsdCallback;
import ru.m210projects.Build.osd.commands.OsdValueRange;
import ru.m210projects.Wang.Factory.DemoRecorder;
import ru.m210projects.Wang.Factory.rts.RTSFile;
import ru.m210projects.Wang.Menus.Network.NetInfo;
import ru.m210projects.Wang.Type.*;

import java.io.IOException;
import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Border.BorderSetView;
import static ru.m210projects.Wang.Break.SortBreakInfo;
import static ru.m210projects.Wang.Draw.dimensionmode;
import static ru.m210projects.Wang.Draw.zoom;
import static ru.m210projects.Wang.Enemies.Bunny.Bunny_Count;
import static ru.m210projects.Wang.Enemies.Ninja.InitAllPlayerSprites;
import static ru.m210projects.Wang.Enemies.Ninja.PlayerPanelSetup;
import static ru.m210projects.Wang.Enemies.Sumo.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JSector.*;
import static ru.m210projects.Wang.Light.InitLighting;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.STAT_PLAYER0;
import static ru.m210projects.Wang.Panel.pClearSpriteList;
import static ru.m210projects.Wang.Player.*;
import static ru.m210projects.Wang.Rooms.SetupMirrorTiles;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.Track;
import static ru.m210projects.Wang.Track.*;
import static ru.m210projects.Wang.Type.MyTypes.DIV2;
import static ru.m210projects.Wang.Type.ResourceHandler.episodeManager;
import static ru.m210projects.Wang.Weapon.QueueReset;
import static ru.m210projects.Wang.Weapon.left_foot;

public class Game {

    public static final int BACKBUTTON = 9216;
    public static final int MOUSECURSOR = 9217;
    public static final int ALTHUDLEFT = 9218;
    public static final int ALTHUDRIGHT = 9219;
    public static final int ANIM_INTRO = 0;
    public static final int ANIM_SERP = 1;
    public static final int ANIM_SUMO = 2;
    public static final int ANIM_ZILLA = 3;
    public static final int RECSYNCBUFSIZ = 2520;
    public static final EpisodeInfo pSharewareEp = new EpisodeInfo("Enter the Wang", "Four levels (Shareware Version)",
            new LevelInfo[]{new LevelInfo("$bullet.map", "e1l01.mid", "Seppuku Station", "0 : 55", "5 : 00"),
                    new LevelInfo("$dozer.map", "e1l03.mid", "Zilla Construction", "4 : 59", "8 : 00"),
                    new LevelInfo("$shrine.map", "e1l02.mid", "Master Leep's Temple", "3 : 16", "10 : 00"),
                    new LevelInfo("$woods.map", "e1l04.mid", "Dark Woods of the Serpent", "7 : 06", "16 : 00"),});
    public static final EpisodeInfo pOriginalEp = new EpisodeInfo("Code of Honor",
            "Eighteen levels (Full Version only)",
            new LevelInfo[]{new LevelInfo("$whirl.map", "yokoha03.mid", "Rising Son", "5 : 30", "10 : 00"),
                    new LevelInfo("$tank.map", "nippon34.mid", "Killing Fields", "1 : 46", "4 : 00"),
                    new LevelInfo("$boat.map", "execut11.mid", "Hara-Kiri Harbor", "1 : 56", "4 : 00"),
                    new LevelInfo("$garden.map", "execut11.mid", "Zilla's Villa", "1 : 06", "2 : 00"),
                    new LevelInfo("$outpost.map", "sanai.mid", "Monastery", "1 : 23", "3 : 00"),
                    new LevelInfo("$hidtemp.map", "kotec2.mid", "Raider of the Lost Wang", "2 : 05", "4 : 10"),
                    new LevelInfo("$plax1.map", "kotec2.mid", "Sumo Sky Palace", "6 : 32", "12 : 00"),
                    new LevelInfo("$bath.map", "yokoha03.mid", "Bath House", "10 : 00", "10 : 00"),
                    new LevelInfo("$airport.map", "nippon34.mid", "Unfriendly Skies", "2 : 59", "6 : 00"),
                    new LevelInfo("$refiner.map", "kotoki12.mid", "Crude Oil", "2 : 40", "5 : 00"),
                    new LevelInfo("$newmine.map", "hoshia02.mid", "Coolie Mines", "2 : 48", "6 : 00"),
                    new LevelInfo("$subbase.map", "hoshia02.mid", "Subpen 7", "2 : 02", "4 : 00"),
                    new LevelInfo("$rock.map", "kotoki12.mid", "The Great Escape", "3 : 18", "6 : 00"),
                    new LevelInfo("$yamato.map", "sanai.mid", "Floating Fortress", "11 : 38", "20 : 00"),
                    new LevelInfo("$seabase.map", "kotec2.mid", "Water Torture", "5 : 07", "10 : 00"),
                    new LevelInfo("$volcano.map", "kotec2.mid", "Stone Rain", "9 : 15", "20 : 00"),
                    // Secret levels:
                    new LevelInfo("$shore.map", "kotec2.mid", "Shanghai Shipwreck", "3 : 58", "8 : 00"),
                    new LevelInfo("$auto.map", "kotec2.mid", "Auto Maul", "4 : 07", "8 : 00"),});
    public static final EpisodeInfo pWangBangEp = new EpisodeInfo("Wang Bang", "Multiplayer maps",
            new LevelInfo[]{
                    new LevelInfo("tank.map", "kotec2.mid", "Heavy Metal (DM only)", "10 : 00", "10 : 00"),
                    new LevelInfo("$dmwoods.map", "kotec2.mid", "Ripper Valley (DM only)", "10 : 00", "10 : 00"),
                    new LevelInfo("$dmshrin.map", "kotec2.mid", "House of Wang (DM only)", "10 : 00", "10 : 00"),
                    new LevelInfo("$rush.map", "kotec2.mid", "Lo Wang Rally (DM only)", "10 : 00", "10 : 00"),
                    new LevelInfo("shotgun.map", "kotec2.mid", "Ruins of the Ronin (CTF)", "10 : 00", "10 : 00"),
                    new LevelInfo("$dmdrop.map", "kotec2.mid", "Killing Fields (CTF)", "10 : 00", "10 : 00"),
            });
    public static final EpisodeInfo pWantonEp = new EpisodeInfo("Wanton Destr",
            "Twelve levels (Full Version only)",
            new LevelInfo[]{
                    new LevelInfo("$whirl.map", "yokoha03.mid", "Chinatown", "5 : 30", "10 : 00"),
                    new LevelInfo("$tank.map", "nippon34.mid", "Monastery", "1 : 46", "4 : 00"),
                    new LevelInfo("$boat.map", "execut11.mid", "Trolley Yard", "1 : 56", "4 : 00"),
                    new LevelInfo("$garden.map", "execut11.mid", "Restaurant", "1 : 06", "2 : 00"),
                    new LevelInfo("$outpost.map", "sanai.mid", "Skyscraper", "1 : 23", "3 : 00"),
                    new LevelInfo("$hidtemp.map", "kotec2.mid", "Airplane", "2 : 05", "4 : 10"),
                    new LevelInfo("$plax1.map", "kotec2.mid", "Military Base", "6 : 32", "12 : 00"),
                    new LevelInfo("$bath.map", "yokoha03.mid", "Train", "10 : 00", "10 : 00"),
                    new LevelInfo("$airport.map", "nippon34.mid", "Auto Factory", "2 : 59", "6 : 00"),

                    null, //"$refiner.map"
                    null, //"$newmine.map"
                    null, //"$subbase.map"
                    null, //"$rock.map"
                    null, //"$yamato.map"
                    null, //"$seabase.map"

                    new LevelInfo("$volcano.map", "kotec2.mid", "Skyline", "9 : 15", "20 : 00"),
                    // Secret levels:
                    new LevelInfo("$shore.map", "kotec2.mid", "Redwood Forest", "3 : 58", "8 : 00"),
                    new LevelInfo("$auto.map", "kotec2.mid", "The Docks", "4 : 07", "8 : 00"),});
    public static final EpisodeInfo pTwinDragonEp = new EpisodeInfo("Twin Dragon",
            "Thirteen levels (Full Version only)",
            new LevelInfo[]{
                    new LevelInfo("$whirl.map", "yokoha03.mid", "Wang's Home", "5 : 30", "10 : 00"),
                    new LevelInfo("$tank.map", "nippon34.mid", "City of Despair", "1 : 46", "4 : 00"),
                    new LevelInfo("$boat.map", "execut11.mid", "Emergency Room", "1 : 56", "4 : 00"),
                    new LevelInfo("$garden.map", "execut11.mid", "Hide and Seek", "1 : 06", "2 : 00"),
                    new LevelInfo("$outpost.map", "sanai.mid", "Warehouse", "1 : 23", "3 : 00"),
                    new LevelInfo("$hidtemp.map", "kotec2.mid", "Military Research Base", "2 : 05", "4 : 10"),
                    new LevelInfo("$plax1.map", "kotec2.mid", "Toxic Waste", "6 : 32", "12 : 00"),
                    new LevelInfo("$bath.map", "yokoha03.mid", "Crazy Train", "10 : 00", "10 : 00"),
                    new LevelInfo("$airport.map", "nippon34.mid", "Fishing Village", "2 : 59", "6 : 00"),
                    new LevelInfo("$refiner.map", "kotoki12.mid", "The Garden", "2 : 40", "5 : 00"),
                    new LevelInfo("$newmine.map", "hoshia02.mid", "The Fortress", "2 : 48", "6 : 00"),

                    null, //"$subbase.map"
                    null, //"$rock.map"
                    null, //"$yamato.map"
                    null, //"$seabase.map"

                    new LevelInfo("$volcano.map", "kotec2.mid", "The Palace", "9 : 15", "20 : 00"),
                    // Secret levels:
                    new LevelInfo("$shore.map", "kotec2.mid", "Prison Camp", "3 : 58", "8 : 00"),});
    private static final char[] buffer = new char[256];
    public static int nInterpolation = 2;
    public static final boolean SW_SHAREWARE = false;
    public static Entry boardfilename;
    public static final LastLevelUser[] puser = new LastLevelUser[MAX_SW_PLAYERS];
    public static final short PlaxBits = 0;
    public static final PlayerStr[] Player = new PlayerStr[MAX_SW_PLAYERS];
    public static int screenpeek;
    public static final boolean Global_PLock = false;
    public static int PlayingLevel, Level, Skill;
    public static boolean QuitFlag, ExitLevel, FinishedLevel;
    public static int FinishAnim = 0;
    public static boolean GodMode = false;
//    public static boolean ConPanel = false;
    public static final boolean DebugActorFreeze = false;
    public static final boolean DebugActor = false;
    public static final boolean DebugAnim = false;
    public static final boolean DebugPanel = false;
    public static final boolean DebugSector = false;
//    public static boolean ResCheat = false;
    public static final boolean PlayerTrackingMode = false;
    public static final NetInfo pNetInfo = new NetInfo();
    public static boolean MessageInputMode = false;
    public static int PlayClock;
    public static int Kills;
    public static int TotalKillable;
    public static int totalsynctics;
    public static final int RAND_MAX = 32767;
    public static final short HelpPage = 0;
    public static final short[] HelpPagePic = {5115, 5116, 5117};
    public static int ChopTics;
    public static int PlaxCeilGlobZadjust, PlaxFloorGlobZadjust;
    public static DemoRecorder rec;
    public static boolean DemoRecording = false;
    public static boolean isDemoRecording = false;
    public static final GameInfo defGame = new GameInfo("Default", new EpisodeEntry.Addon(game.cache.getGameDirectory(), game.cache.getGameDirectory().getEntry(game.mainGrp)), pSharewareEp, pOriginalEp, pWangBangEp) {
        @Override
        public LevelInfo getLevel(int num) {
            if (num < 1) {
                return null;
            }
            if (num < 5) {
                return episode[0] != null ? episode[0].gMapInfo[num - 1] : null;
            }
            if (num < 23) {
                return episode[1] != null ? episode[1].gMapInfo[num - 5] : null;
            }
            return episode[2] != null ? episode[2].gMapInfo[num - 23] : null;
        }

        @Override
        public int getNumEpisode(int level) {
            if (level < 5) {
                return 0;
            }
            if (level < 23) {
                return 1;
            }
            return 2;
        }

        @Override
        public int getNumLevel(int level) {
            if (level < 5) {
                return level - 1;
            }
            if (level < 23) {
                return level - 5;
            }
            return level - 23;
        }
    };
    public static GameInfo currentGame = defGame;
    public static final LONGp[] tmp_ptr = {new LONGp(), new LONGp(), new LONGp(), new LONGp(), new LONGp(), new LONGp(),
            new LONGp(), new LONGp()};

    public static void MapSetAll2D(byte fill) {
        Arrays.fill(show2dwall.getArray(), fill);
        Arrays.fill(show2dsprite.getArray(), fill);
        ru.m210projects.Build.Types.Sector[] sectors = boardService.getBoard().getSectors();
        for (int i = 0; i < sectors.length; i++) {
            ru.m210projects.Build.Types.Sector sec = sectors[i];
            if (sec.getCeilingpicnum() != 342 && sec.getFloorpicnum() != 342) {
                show2dsector.getArray()[i >> 3] = fill;
            }
        }
    }

    public static void LoadLevel(Entry entry) {
        if (!entry.exists()) {
            throw new WarningException("Error opening map file \"" + entry.getName() + "\"");
        }

        Board board = engine.loadboard(entry);
        BuildPos out = board.getPos();
        Player[0].posx = out.x;
        Player[0].posy = out.y;
        Player[0].posz = out.z;
        Player[0].pang = out.ang;
        Player[0].cursectnum = out.sectnum;
    }

    public static void InitGame() throws IOException {
        Entry rtsEntry = game.getCache().getEntry("sw.rts", true);
        if (rtsEntry.exists()) {
            RTSFile rts = new RTSFile(rtsEntry.getName(), rtsEntry::getInputStream);
            if (rts.getSize() != 0) {
                Console.out.println("Using .RTS file:" + rtsEntry.getName());
                Main.RTS_File = rts;
            }
        }

        episodeManager.putEpisode(defGame);

        InitAnim();

        ConsoleInit(); // Init console command list

        SoundStartup();

        SortBreakInfo();
        parallaxtype = 1;
        pskyoff[0] = 0;
        pskybits = PlaxBits;
        // Default scale value for parallax skies
        Renderer renderer = game.getRenderer();
        renderer.setParallaxScale(8192);

        Arrays.fill(Track, null);

        for (int i = 0; i < MAX_SW_PLAYERS; i++) {
            Player[i] = new PlayerStr();
            puser[i] = new LastLevelUser();
        }

        BorderSetView(cfg.BorderNum);

//		LoadPLockFromScript( "swplock.txt" ); // Get Parental Lock setup info XXX
    }

    public static void ConsoleInit() {
        Console.out.println("Initializing on-screen display system");
        Console.out.getPrompt().setVersion(game.getTitle(), OsdColor.YELLOW, 10);

        Console.out.registerCommand(new OsdCallback("levelinfo", "", argv -> {
            String name;
            if (mUserFlag != UserFlag.UserMap || boardfilename == null) {
                name = currentGame.getMapTitle(Level);
            } else {
                name = boardfilename.getName();
            }

            Console.out.println("LevelInfo:", OsdColor.GREEN);
            Console.out.println("   Mapname: " + name);
            Console.out.println("   Levelnum: " + Level);

            return CommandResponse.SILENT_RESPONSE;
        }));

        Console.out.registerCommand(new OsdCallback("hitscan", "", argv -> {
            PlayerStr pp = Player[screenpeek];
            int daang = pp.getAnglei();
            int daz = ((100 - pp.getHorizi()) * 2000);

            engine.hitscan(pp.posx, pp.posy, pp.posz, pp.cursectnum, // Start position
                    EngineUtils.sin(NORM_ANGLE(daang + 512)), // X vector of 3D ang
                    EngineUtils.sin(NORM_ANGLE(daang)), // Y vector of 3D ang
                    daz, // Z vector of 3D ang
                    pHitInfo, 0xFFFFFFFF);

            Console.out.println("Sprite : " + pHitInfo.hitsprite);
            Sprite hitSpr = boardService.getSprite(pHitInfo.hitsprite);
            if (hitSpr != null) {
                Console.out.println(hitSpr.toString());
            }
            return CommandResponse.SILENT_RESPONSE;
        }));

        Console.out.registerCommand(new OsdValueRange("interpolationtype", "interpolationtype 0: disabled, 1: default, 2: wang enagled, 3: wang disabled", 0, 3) {
            @Override
            public float getValue() {
                return nInterpolation;
            }

            @Override
            protected void setCheckedValue(float value) {
                nInterpolation = (int) value;
            }
        });
    }

    public static int Distance(int x1, int y1, int x2, int y2) {
        if ((x2 = x2 - x1) < 0) {
            x2 = -x2;
        }

        if ((y2 = y2 - y1) < 0) {
            y2 = -y2;
        }

        int min = Math.min(x2, y2);
        return (x2 + y2 - DIV2(min));
    }

    public static void InitLevelGlobals() {
        // A few IMPORTANT GLOBAL RESETS
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        ChopTics = 0;
        dimensionmode = 3;
        zoom = 768;
        wait_active_check_offset = 0;
        PlaxCeilGlobZadjust = PlaxFloorGlobZadjust = Z(500);
        pskyoff[0] = 0;
        pskybits = PlaxBits;
        FinishedLevel = false;
        AnimCnt = 0;
        left_foot = false;
        screenpeek = myconnectindex;

        gNet.TimeLimitClock = gNet.TimeLimit;

        serpwasseen = false;
        sumowasseen = false;
        zillawasseen = false;
        triedplay = false;
        Arrays.fill(BossSpriteNum,  -1);
    }

    public static void InitNewGame() {
        for (int i = 0; i < MAX_SW_PLAYERS; i++) {
            // don't jack with the playerreadyflag
            int ready_bak = Player[i].playerreadyflag;
            Player[i].reset();
            Player[i].playerreadyflag = ready_bak;
            puser[i].reset();
        }
    }

    public static void InitLevelGlobals2() {
        // GLOBAL RESETS NOT DONE for LOAD GAME
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        InitTimingVars();
        Kills = 0;
        TotalKillable = 0;
        Bunny_Count = 0;
    }

    public static boolean InitLevel(Entry levelMap, boolean NewGame) {
        Terminate3DSounds();

        // A few IMPORTANT GLOBAL RESETS
        InitLevelGlobals();
        if (gDemoScreen.demfile == null) {
            CDAudio_Stop();
        }

        InitLevelGlobals2();

        if (Level < 0) {
            Level = 0;
        }

        if (Level > MAX_LEVELS) {
            Level = 1;
        }

        PlayingLevel = Level;

        if (NewGame) {
            InitNewGame();
        }

        try {
            clearUser();

            LoadLevel(levelMap);

            ru.m210projects.Build.Types.Sector sec0 = boardService.getSector(0);
            if (sec0 != null && sec0.getExtra() != -1) {
                NormalVisibility = (visibility = sec0.getExtra());
                sec0.setExtra(0);
            } else {
                NormalVisibility = visibility;
            }

            //
            // Do Player stuff first
            //

            InitAllPlayers(NewGame);

            QueueReset();
            PreMapCombineFloors();
            InitMultiPlayerInfo();
            InitAllPlayerSprites(NewGame);

            //
            // Do setup for sprite, track, panel, sector, etc
            //

            // Set levels up
            InitTimingVars();

            SpriteSetup();

            SpriteSetupPost(); // post processing - already gone once through the loop
            InitLighting();

            TrackSetup();

            PlayerPanelSetup();
            if (!SectorSetup()) {
                return false;
            }
            JS_InitMirrors();
            JS_InitLockouts(); // Setup the lockout linked lists
            JS_ToggleLockouts(); // Init lockouts on/off

            PlaceSectorObjectsOnTracks();
            PlaceActorsOnTracks();
            PostSetupSectorObject();
            SetupMirrorTiles();

            return true;
        } catch (Exception e) {
            Console.out.println("Load map exception: " + e);
            if (e.getMessage() != null) {
                game.GameMessage("Load map exception:\r" + e.getMessage());
            } else {
                game.GameMessage("Can't load the map " + levelMap);
            }

        }

        return false;
    }

    public static void TerminateLevel() {
        System.err.println("TerminateLevel");

//		DemoTerm(); XXX

        Arrays.fill(Track, null);

        StopFX();
        Terminate3DSounds(); // Kill the 3d sounds linked list

        for (int stat = STAT_PLAYER0; stat < STAT_PLAYER0 + numplayers; stat++) {

            int pnum = stat - STAT_PLAYER0;

            for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = node.getNext()) {
                int i = node.getIndex();
                USER u = getUser(i);
                if (u != null) {
                    puser[pnum].copy(u);
                }
            }
        }

        // Kill User memory and delete sprites
        ListNode<Sprite> nexti;
        for (int stat = 0; stat < MAXSTATUS; stat++) {
            for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = nexti) {
                nexti = node.getNext();
                KillSprite(node.getIndex());
            }
        }

        // Clear Sprite Extension structure
        clearSecUser(); // 26.04.2024 was reset

        for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            PlayerStr pp = Player[pnum];

            // Free panel sprites for players
            pClearSpriteList(pp);

            pp.DoPlayerAction = null;

            pp.PlayerSprite = -1;
            pp.PlayerUnderSprite = -1;

            Arrays.fill(pp.HasKey, (byte) 0);

            // pp.WpnFlags = 0;
            pp.CurWpn = null;

            Arrays.fill(pp.Wpn, null);
            Arrays.fill(pp.InventorySprite, null);
            Arrays.fill(pp.InventoryTics, (short) 0);

            pp.Killer = -1;

            List.Init(pp.PanelSpriteList);
        }

        JS_UnInitLockouts();
    }

    public static void InitPlayerGameSettings() {
        if (game.nNetMode == NetMode.Single) {
            if (cfg.AutoAim) {
                Player[myconnectindex].Flags |= (PF_AUTO_AIM);
            } else {
                Player[myconnectindex].Flags &= ~(PF_AUTO_AIM);
            }
        }

        // everyone had their own Auto Run
        if (cfg.AutoRun) {
            Player[myconnectindex].Flags |= (PF_LOCK_RUN);
        } else {
            Player[myconnectindex].Flags &= ~(PF_LOCK_RUN);
        }
    }

    public static void InitTimingVars() {
        PlayClock = 0;

        totalsynctics = 0;
        engine.srand(17);
        game.pNet.ResetTimers();

        MoveSkip8 = 2;
        MoveSkip2 = false;
        MoveSkip4 = 1; // start slightly offset so these
        // don't move the same
        // as the Skip2's
        gNet.MoveThingsCount = 0;
    }

    public static char[] getMapName(int num) {
        Arrays.fill(buffer, (char) 0);
        if (mUserFlag != UserFlag.UserMap || boardfilename == null) {
            String name = currentGame.getMapTitle(num);
            if (name != null) {
                name.getChars(0, Math.min(name.length(), buffer.length), buffer, 0);
            }
        } else {
            int size = boardfilename.getName().length();
            System.arraycopy(boardfilename.getName().toCharArray(), 0, buffer, 0, Math.min(size, buffer.length));
        }
        return buffer;
    }

    public static boolean isOriginal() {
        return game.isCurrentScreen(gDemoScreen) && gDemoScreen.demfile != null && gDemoScreen.demfile.nVersion == 1;
    }

    public static int swGetAddon() {
        int addon = 0;
        if (currentGame.episode.length > 1 && currentGame.episode[1] == pWantonEp) {
            addon = 1;
        }
        if (currentGame.episode.length > 1 && currentGame.episode[1] == pTwinDragonEp) {
            addon = 2;
        }

        return addon;
    }
}
