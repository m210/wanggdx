package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Enemies.Ninja;
import ru.m210projects.Wang.Enemies.Skull;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Break.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Bunny.Bunny_Count;
import static ru.m210projects.Wang.Enemies.Bunny.DoBunnyRipHeart;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Enemies.Ripper.DoRipperRipHeart;
import static ru.m210projects.Wang.Enemies.Ripper2.DoRipper2RipHeart;
import static ru.m210projects.Wang.Enemies.Ripper2.Ripper2Hatch;
import static ru.m210projects.Wang.Enemies.Skull.*;
import static ru.m210projects.Wang.Enemies.Zombie.SpawnZombie2;
import static ru.m210projects.Wang.Factory.WangNetwork.Prediction;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.LUMINOUS;
import static ru.m210projects.Wang.JWeapon.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.MiscActr.s_TrashCanPain;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.*;
import static ru.m210projects.Wang.Panel.PlayerUpdateAmmo;
import static ru.m210projects.Wang.Panel.PlayerUpdateHealth;
import static ru.m210projects.Wang.Player.*;
import static ru.m210projects.Wang.Quake.*;
import static ru.m210projects.Wang.Rooms.*;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Shrap.*;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Tags.TAG_SPRITE_HIT_MATCH;
import static ru.m210projects.Wang.Tags.TAG_WALL_BREAK;
import static ru.m210projects.Wang.Track.*;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Vis.SpawnVis;
import static ru.m210projects.Wang.WallMove.*;
import static ru.m210projects.Wang.Weapons.Sword.SpawnSwordSparks;

public class Weapon {

    public static final Animator DoDefaultStat = new Animator((Animator.Runnable) Weapon::DoDefaultStat);
    public static final Animator DoSuicide = new Animator((Animator.Runnable) Weapon::DoSuicide);
    public static final Animator DoPuff = new Animator((Animator.Runnable) Weapon::DoPuff);
    public static final Animator DoRailPuff = new Animator((Animator.Runnable) Weapon::DoRailPuff);
    public static final Animator DoTracer = new Animator(Weapon::DoTracer);
    public static final Animator DoEMP = new Animator(Weapon::DoEMP);
    public static final Animator DoEMPBurst = new Animator((Animator.Runnable) Weapon::DoEMPBurst);
    public static final Animator DoVomit = new Animator((Animator.Runnable) Weapon::DoVomit);
    public static final Animator DoVomitSplash = new Animator((Animator.Runnable) Weapon::DoVomitSplash);
    public static final Animator DoFastShrapJumpFall = new Animator((Animator.Runnable) Weapon::DoFastShrapJumpFall);
    public static final Animator DoTankShell = new Animator(Weapon::DoTankShell);
    public static final Animator DoVehicleSmoke = new Animator((Animator.Runnable) Weapon::DoVehicleSmoke);
    public static final Animator SpawnVehicleSmoke = new Animator((Animator.Runnable) Weapon::SpawnVehicleSmoke);
    public static final Animator DoWaterSmoke = new Animator((Animator.Runnable) Weapon::DoWaterSmoke);
    private static final Animator DoUziSmoke = new Animator((Animator.Runnable) Weapon::DoUziSmoke);
    public static final Animator DoShotgunSmoke = new Animator((Animator.Runnable) Weapon::DoShotgunSmoke);
    public static final Animator DoUziBullet = new Animator(Weapon::DoUziBullet);
    public static final Animator DoBubble = new Animator(Weapon::DoBubble);
    private static final Animator DoCrossBolt = new Animator(Weapon::DoCrossBolt);
    public static final Animator DoStar = new Animator(Weapon::DoStar);
    public static final Animator SpawnShrapX = new Animator((Animator.Runnable) Weapon::SpawnShrapX);
    public static final Animator DoSectorExp = new Animator((Animator.Runnable) Weapon::DoSectorExp);
    public static final Animator DoLavaBoulder = new Animator(Weapon::DoLavaBoulder);
    public static final Animator DoShrapDamage = new Animator((Animator.Runnable) Weapon::DoShrapDamage);
    public static final Animator DoVulcanBoulder = new Animator(Weapon::DoVulcanBoulder);
    public static final Animator DoGrenade = new Animator(Weapon::DoGrenade);
    public static final Animator DoMine = new Animator((Animator.Runnable) Weapon::DoMine);
    public static final Animator DoMineStuck = new Animator((Animator.Runnable) Weapon::DoMineStuck);
    public static final Animator DoMineSpark = new Animator((Animator.Runnable) Weapon::DoMineSpark);
    public static final Animator DoMeteor = new Animator(Animator.NULL);
    public static final Animator DoMirvMissile = new Animator(Weapon::DoMirvMissile);
    public static final Animator DoSerpMeteor = new Animator(Weapon::DoSerpMeteor);
    public static final Animator DoSpear = new Animator(Weapon::DoSpear);
    public static final Animator DoRocket = new Animator(Weapon::DoRocket);
    public static final Animator DoRail = new Animator(Weapon::DoRail);
    public static final Animator DoLaser = new Animator(Weapon::DoLaser);
    public static final Animator DoMicro = new Animator(Weapon::DoMicro);
    public static final Animator DoMicroMini = new Animator(Weapon::DoMicroMini);
    public static final Animator DoBoltThinMan = new Animator(Weapon::DoBoltThinMan);
    public static final Animator DoBoltSeeker = new Animator(Weapon::DoBoltSeeker);
    public static final Animator DoBoltFatMan = new Animator(Animator.NULL);
    public static final Animator DoBoltShrapnel = new Animator(Animator.NULL);
    public static final Animator DoCoolgFire = new Animator(Weapon::DoCoolgFire);
    public static final Animator DoCoolgDrip = new Animator((Animator.Runnable) Weapon::DoCoolgDrip);
    public static final Animator DoPlasma = new Animator(Weapon::DoPlasma);
    public static final Animator DoPlasmaFountain = new Animator((Animator.Runnable) Weapon::DoPlasmaFountain);
    private static final Animator DoShrapJumpFall = new Animator((Animator.Runnable) Weapon::DoShrapJumpFall);
    public static final Animator DoPlasmaDone = new Animator((Animator.Runnable) Weapon::DoPlasmaDone);
    public static final Animator DoTeleRipper = new Animator((Animator.Runnable) Weapon::DoTeleRipper);  //Spawn a RIPPER teleport effect
    public static final Animator DoElectro = new Animator(Weapon::DoElectro);
    public static final Animator SpawnGrenadeSmallExp = new Animator((Animator.Runnable) Weapon::SpawnGrenadeSmallExp);
    public static final Animator DoMineExp = new Animator((Animator.Runnable) Weapon::DoMineExp);
    public static final Animator DoMineExpMine = new Animator((Animator.Runnable) Weapon::DoMineExpMine);
    public static final Animator DoExpDamageTest = new Animator((Animator.Runnable) Weapon::DoExpDamageTest);
    public static final Animator DoDamageTest = new Animator((Animator.Runnable) Weapon::DoDamageTest);
    public static final Animator DoFireballFlames = new Animator((Animator.Runnable) Weapon::DoFireballFlames);
    public static final Animator DoBreakFlames = new Animator((Animator.Runnable) Weapon::DoBreakFlames);
    public static final Animator DoFireball = new Animator(Weapon::DoFireball);
    public static final Animator DoRing = new Animator(Weapon::DoRing);
    public static final Animator DoNapalm = new Animator(Weapon::DoNapalm);
    public static final Animator DoBloodWorm = new Animator(Weapon::DoBloodWorm);
    public static final Animator DoMirv = new Animator(Weapon::DoMirv);
    public static final Animator DoTracerShrap = new Animator((Animator.Runnable) Weapon::DoTracerShrap);
    public static final Animator DoWallBlood = new Animator((Animator.Runnable) Weapon::DoWallBlood);
    public static final Animator DoFloorBlood = new Animator((Animator.Runnable) Weapon::DoFloorBlood);
    public static final Animator DoLavaErupt = new Animator((Animator.Runnable) Weapon::DoLavaErupt);
    public static final Animator GenerateDrips = new Animator(Weapon::GenerateDrips);
    public static final Animator DoTracerStart = new Animator(Weapon::DoTracerStart);
    public static final Animator DoLaserStart = new Animator(Weapon::DoLaserStart);
    public static final Animator DoRailStart = new Animator(Weapon::DoRailStart);

    private static final int[] lat_dist = {800, -800};
    private static int uziclock = 0;
    private static final int UZIFIRE_WAIT = 20;

    public static final int MAX_TURRET_MICRO = 10;
    private static int alternate;
    public static final int WALLBLOOD_DIST_MAX = 2500;
    public static final int FEET_IN_BLOOD_DIST = 300;
    public static final int RING_OUTER_DIST = 3200;
    public static final int RING_INNER_DIST = 800;
    public static final int SERP_RING_DIST = 2800; // Was 3500

    public static class MISSILE_PLACEMENT {
        public final int dist_over;
        public final int dist_out;
        public final int ang;

        public MISSILE_PLACEMENT(int dist_over, int dist_out, int ang) {
            this.dist_over = dist_over;
            this.dist_out = dist_out;
            this.ang =  ang;
        }
    }

    public static final MISSILE_PLACEMENT[] mp = {new MISSILE_PLACEMENT(600 * 6, 400, 512), new MISSILE_PLACEMENT(0, 1100, 0), new MISSILE_PLACEMENT(600 * 6, 400, -512)};
    private static final int[] dangs2 = {-128, 128};
    public static final int STAR_REPEAT = 26;
    public static final int STAR_HORIZ_ADJ = 100;
    private static final int[] dang3 = {-12, 12};
public static final int MICRO_ANG = 400;
    public static final int[] lat_ang = {512, -512};



    public static FootType FootMode;
    public static boolean GlobalSkipZrange = false;

//    public static final int NEW_ELECTRO = 1;
    public static final int HORIZ_MULT = 128;

    public static final int MAX_HOLE_QUEUE = 64;
    public static final int MAX_STAR_QUEUE = 32;
    public static final int MAX_WALLBLOOD_QUEUE = 32;
    public static final int MAX_FLOORBLOOD_QUEUE = 32;
    public static final int MAX_GENERIC_QUEUE = 32;
    public static final int MAX_LOWANGS_QUEUE = 16;

    // public static final int NUKE_RADIUS =16384;
    public static final int NUKE_RADIUS = 30000;
    public static final int RAIL_RADIUS = 3500;

    // Damage Times - takes damage after this many tics
    public static final int DAMAGE_BLADE_TIME = (10);

    // Player Missile Speeds
    public static final int STAR_VELOCITY = (1800);
//    public static final int BOLT_VELOCITY = (900);
    public static final int ROCKET_VELOCITY = (1350);
//    public static final int BOLT_SEEKER_VELOCITY = (820);
    public static final int FIREBALL_VELOCITY = (2000);
public static final int TRACER_VELOCITY = (1200);
    public static final int TANK_SHELL_VELOCITY = (1200);
    public static final int GRENADE_VELOCITY = (900);
    public static final int MINE_VELOCITY = (520); // Was 420
    public static final int CHEMBOMB_VELOCITY = (420);

    // Player Spell Missile Speeds
    public static final int BLOOD_WORM_VELOCITY = (800);
    public static final int NAPALM_VELOCITY = (800);
    public static final int MIRV_VELOCITY = (600);
//    public static final int SPIRAL_VELOCITY = (600);

    // Trap Speeds
    public static final int BOLT_TRAP_VELOCITY = (950);
//    public static final int SPEAR_TRAP_VELOCITY = (650);
    public static final int FIREBALL_TRAP_VELOCITY = (750);

    // NPC Missile Speeds
    public static final int NINJA_STAR_VELOCITY = (1800);
    public static final int NINJA_BOLT_VELOCITY = (500);
    public static final int GORO_FIREBALL_VELOCITY = (800);
    public static final int SKEL_ELECTRO_VELOCITY = (850);
    public static final int COOLG_FIRE_VELOCITY = (400);

    public static final int GRENADE_RECOIL_AMT = (12);
    public static final int ROCKET_RECOIL_AMT = (7);
    public static final int RAIL_RECOIL_AMT = (7);
    public static final int SHOTGUN_RECOIL_AMT = (12);
    // public static final int MICRO_RECOIL_AMT =(15);

    // Damage amounts that determine the type of player death
    // The standard flip over death is default
    public static final int PLAYER_DEATH_CRUMBLE_DAMMAGE_AMT = (25);
    public static final int PLAYER_DEATH_EXPLODE_DAMMAGE_AMT = (65);

    // electro weapon
//    public static final int ELECTRO_MAX_JUMP_DIST = 25000;

    public static final int RADIATION_CLOUD = 3258;
    public static final int MUSHROOM_CLOUD = 3280;
    public static final int PUFF = 1748;
    public static final int CALTROPS = 2218;
    public static final int PHOSPHORUS = 1397;
    public static final int MISSILEMOVETICS = 6;
    public static final int RINGMOVETICS = (MISSILEMOVETICS * 2);


    public static final int[] StatDamageList = {STAT_SO_SP_CHILD, STAT_ENEMY, STAT_ENEMY_SKIP4, STAT_PLAYER0, STAT_PLAYER1, STAT_PLAYER2, STAT_PLAYER3, STAT_PLAYER4, STAT_PLAYER5, STAT_PLAYER6, STAT_PLAYER7, STAT_PLAYER_UNDER0, STAT_PLAYER_UNDER1, STAT_PLAYER_UNDER2, STAT_PLAYER_UNDER3, STAT_PLAYER_UNDER4, STAT_PLAYER_UNDER5, STAT_PLAYER_UNDER6, STAT_PLAYER_UNDER7,
            // MINE MUST BE LAST
            STAT_MINE_STUCK};

    public static boolean left_foot = false;
    public static int FinishTimer = 0;

    // This is how many bullet shells have been spawned since the beginning of the
    // game.
    public static final int ShellCount = 0;

    public static int StarQueueHead = 0;
    public static final int[] StarQueue = new int[MAX_STAR_QUEUE];
    public static int HoleQueueHead = 0;
    public static final int[] HoleQueue = new int[MAX_HOLE_QUEUE];
    public static int WallBloodQueueHead = 0;
    public static final int[] WallBloodQueue = new int[MAX_WALLBLOOD_QUEUE];
    public static int FloorBloodQueueHead = 0;
    public static final int[] FloorBloodQueue = new int[MAX_FLOORBLOOD_QUEUE];
    public static int GenericQueueHead = 0;
    public static final int[] GenericQueue = new int[MAX_GENERIC_QUEUE];
    public static int LoWangsQueueHead = 0;
    public static final int[] LoWangsQueue = new int[MAX_LOWANGS_QUEUE];

    public static final DAMAGE_DATA[] DamageData = {new DAMAGE_DATA(WPN_FIST, Player_Action_Func.InitWeaponFist, 10, 40, 0, -1, -1, -1), new DAMAGE_DATA(WPN_STAR, Player_Action_Func.InitWeaponStar, 5, 10, 0, 99, 3, -1), new DAMAGE_DATA(WPN_SHOTGUN, Player_Action_Func.InitWeaponShotgun, 4, 4, 0, 52, 1, -1), new DAMAGE_DATA(WPN_UZI, Player_Action_Func.InitWeaponUzi, 5, 7, 0, 200, 1, -1), new DAMAGE_DATA(WPN_MICRO, Player_Action_Func.InitWeaponMicro, 15, 30, 0, 50, 1, -1), new DAMAGE_DATA(WPN_GRENADE, Player_Action_Func.InitWeaponGrenade, 15, 30, 0, 50, 1, -1), new DAMAGE_DATA(WPN_MINE, Player_Action_Func.InitWeaponMine, 5, 10, 0, 20, 1, -1), new DAMAGE_DATA(WPN_RAIL, Player_Action_Func.InitWeaponRail, 40, 60, 0, 20, 1, -1), new DAMAGE_DATA(WPN_HOTHEAD, Player_Action_Func.InitWeaponHothead, 10, 25, 0, 80, 1, -1), new DAMAGE_DATA(WPN_HEART, Player_Action_Func.InitWeaponHeart, 75, 100, 0, 5, 1, -1),

            new DAMAGE_DATA(WPN_NAPALM, Player_Action_Func.InitWeaponHothead, 50, 100, 0, 100, 40, WPN_HOTHEAD), new DAMAGE_DATA(WPN_RING, Player_Action_Func.InitWeaponHothead, 15, 50, 0, 100, 20, WPN_HOTHEAD), new DAMAGE_DATA(WPN_ROCKET, Player_Action_Func.InitWeaponMicro, 30, 60, 0, 100, 1, WPN_MICRO), new DAMAGE_DATA(WPN_SWORD, Player_Action_Func.InitWeaponSword, 50, 80, 0, -1, -1, -1),

            // extra weapons connected to other

            // spell
            new DAMAGE_DATA(DMG_NAPALM, null, 90, 150, 0, -1, -1, -1), new DAMAGE_DATA(DMG_MIRV_METEOR, null, 35, 65, 0, -1, -1, -1), new DAMAGE_DATA(DMG_SERP_METEOR, null, 7, 15, 0, -1, -1, -1),

            // radius damage
            new DAMAGE_DATA(DMG_ELECTRO_SHARD, null, 2, 6, 0, -1, -1, -1), new DAMAGE_DATA(DMG_SECTOR_EXP, null, 50, 100, 3200, -1, -1, -1), new DAMAGE_DATA(DMG_BOLT_EXP, null, 80, 160, 3200, -1, -1, -1), new DAMAGE_DATA(DMG_TANK_SHELL_EXP, null, 80, 200, 4500, -1, -1, -1), new DAMAGE_DATA(DMG_FIREBALL_EXP, null, -1, -1, 1000, -1, -1, -1), new DAMAGE_DATA(DMG_NAPALM_EXP, null, 60, 90, 3200, -1, -1, -1), new DAMAGE_DATA(DMG_SKULL_EXP, null, 40, 75, 4500, -1, -1, -1), new DAMAGE_DATA(DMG_BASIC_EXP, null, 10, 25, 1000, -1, -1, -1), new DAMAGE_DATA(DMG_GRENADE_EXP, null, 70, 140, 6500, -1, -1, -1), new DAMAGE_DATA(DMG_MINE_EXP, null, 85, 115, 6500, -1, -1, -1), new DAMAGE_DATA(DMG_MINE_SHRAP, null, 15, 30, 0, -1, -1, -1), new DAMAGE_DATA(DMG_MICRO_EXP, null, 50, 100, 4500, -1, -1, -1), new DAMAGE_DATA(DMG_NUCLEAR_EXP, null, 0, 800, 30000, -1, -1, -1), new DAMAGE_DATA(DMG_RADIATION_CLOUD, null, 2, 6, 5000, -1, -1, -1), new DAMAGE_DATA(DMG_FLASHBOMB, null, 100, 150, 16384, -1, -1, -1),

            new DAMAGE_DATA(DMG_FIREBALL_FLAMES, null, 2, 6, 300, -1, -1, -1),

            // actor
            new DAMAGE_DATA(DMG_RIPPER_SLASH, null, 10, 30, 0, -1, -1, -1), new DAMAGE_DATA(DMG_SKEL_SLASH, null, 10, 20, 0, -1, -1, -1), new DAMAGE_DATA(DMG_COOLG_BASH, null, 10, 20, 0, -1, -1, -1), new DAMAGE_DATA(DMG_COOLG_FIRE, null, 15, 30, 0, -1, -1, -1), new DAMAGE_DATA(DMG_GORO_CHOP, null, 20, 40, 0, -1, -1, -1), new DAMAGE_DATA(DMG_GORO_FIREBALL, null, 5, 20, 0, -1, -1, -1), new DAMAGE_DATA(DMG_SERP_SLASH, null, 75, 75, 0, -1, -1, -1), new DAMAGE_DATA(DMG_LAVA_BOULDER, null, 100, 100, 0, -1, -1, -1), new DAMAGE_DATA(DMG_LAVA_SHARD, null, 25, 25, 0, -1, -1, -1), new DAMAGE_DATA(DMG_HORNET_STING, null, 5, 10, 0, -1, -1, -1), new DAMAGE_DATA(DMG_EEL_ELECTRO, null, 10, 40, 3400, -1, -1, -1),

            // misc
            new DAMAGE_DATA(DMG_SPEAR_TRAP, null, 15, 20, 0, -1, -1, -1), new DAMAGE_DATA(DMG_VOMIT, null, 5, 15, 0, -1, -1, -1),

            // inanimate objects
            new DAMAGE_DATA(DMG_BLADE, null, 10, 20, 0, -1, -1, -1), new DAMAGE_DATA(MAX_WEAPONS, null, 10, 20, 0, -1, -1, -1)};

//////////////////////
//
//SPECIAL public static final StateS
//
//////////////////////

    //public static final State for sprites that are not restored
    public static final State[] s_NotRestored = {new State(2323, 100, null).setNext()};

    public static final State[] s_Suicide = {new State(1, 100, DoSuicide).setNext()};

    public static final State[] s_DeadLoWang = {new State(1160, 100, null).setNext()};

//////////////////////
//
//BREAKABLE public static final StateS
//
//////////////////////


    public static final int BREAK_LIGHT_RATE = 18;
    public static final State[] s_BreakLight = {new State(BREAK_LIGHT_ANIM, BREAK_LIGHT_RATE, null), new State(BREAK_LIGHT_ANIM + 1, BREAK_LIGHT_RATE, null), new State(BREAK_LIGHT_ANIM + 2, BREAK_LIGHT_RATE, null), new State(BREAK_LIGHT_ANIM + 3, BREAK_LIGHT_RATE, null), new State(BREAK_LIGHT_ANIM + 4, BREAK_LIGHT_RATE, null), new State(BREAK_LIGHT_ANIM + 5, BREAK_LIGHT_RATE, null).setNext()};

    public static final int BREAK_BARREL_RATE = 18;
    public static final State[] s_BreakBarrel = {new State(BREAK_BARREL + 4, BREAK_BARREL_RATE, null), new State(BREAK_BARREL + 5, BREAK_BARREL_RATE, null), new State(BREAK_BARREL + 6, BREAK_BARREL_RATE, null), new State(BREAK_BARREL + 7, BREAK_BARREL_RATE, null), new State(BREAK_BARREL + 8, BREAK_BARREL_RATE, null), new State(BREAK_BARREL + 9, BREAK_BARREL_RATE, DoDefaultStat).setNext()};

    public static final int BREAK_PEDISTAL_RATE = 28;
    public static final State[] s_BreakPedistal = {new State(BREAK_PEDISTAL + 1, BREAK_PEDISTAL_RATE, null), new State(BREAK_PEDISTAL + 2, BREAK_PEDISTAL_RATE, null), new State(BREAK_PEDISTAL + 3, BREAK_PEDISTAL_RATE, null), new State(BREAK_PEDISTAL + 4, BREAK_PEDISTAL_RATE, null).setNext()};

    public static final int BREAK_BOTTLE1_RATE = 18;
    public static final State[] s_BreakBottle1 = {new State(BREAK_BOTTLE1 + 1, BREAK_BOTTLE1_RATE, null), new State(BREAK_BOTTLE1 + 2, BREAK_BOTTLE1_RATE, null), new State(BREAK_BOTTLE1 + 3, BREAK_BOTTLE1_RATE, null), new State(BREAK_BOTTLE1 + 4, BREAK_BOTTLE1_RATE, null), new State(BREAK_BOTTLE1 + 5, BREAK_BOTTLE1_RATE, null), new State(BREAK_BOTTLE1 + 6, BREAK_BOTTLE1_RATE, null).setNext()};

    public static final int BREAK_BOTTLE2_RATE = 18;
    public static final State[] s_BreakBottle2 = {new State(BREAK_BOTTLE2 + 1, BREAK_BOTTLE2_RATE, null), new State(BREAK_BOTTLE2 + 2, BREAK_BOTTLE2_RATE, null), new State(BREAK_BOTTLE2 + 3, BREAK_BOTTLE2_RATE, null), new State(BREAK_BOTTLE2 + 4, BREAK_BOTTLE2_RATE, null), new State(BREAK_BOTTLE2 + 5, BREAK_BOTTLE2_RATE, null), new State(BREAK_BOTTLE2 + 6, BREAK_BOTTLE2_RATE, null).setNext()};

    public static final int PUFF_RATE = 8;


    public static final State[] s_Puff = {new State(PUFF, PUFF_RATE, DoPuff), new State(PUFF + 1, PUFF_RATE, DoPuff), new State(PUFF + 2, PUFF_RATE, DoPuff), new State(PUFF + 3, PUFF_RATE, DoPuff), new State(PUFF + 4, PUFF_RATE, DoPuff), new State(PUFF + 5, PUFF_RATE, DoPuff), new State(PUFF + 5, 100, DoSuicide)};

    public static final int RAIL_PUFF_R0 = 3969;
    public static final int RAIL_PUFF_R1 = 3985;
    public static final int RAIL_PUFF_R2 = 4001;
    public static final int RAIL_PUFF_RATE = 6;


    public static final State[][] s_RailPuff = {{new State(RAIL_PUFF_R0, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 1, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 2, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 3, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 4, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 5, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 6, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 7, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 8, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 9, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 10, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 11, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 12, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 13, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 14, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 15, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R0 + 15, 100, DoSuicide),}, {new State(RAIL_PUFF_R1, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 1, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 2, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 3, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 4, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 5, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 6, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 7, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 8, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 9, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 10, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 11, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 12, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 13, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 14, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 15, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R1 + 15, 100, DoSuicide),}, {new State(RAIL_PUFF_R2, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 1, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 2, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 3, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 4, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 5, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 6, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 7, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 8, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 9, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 10, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 11, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 12, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 13, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 14, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 15, RAIL_PUFF_RATE, DoRailPuff), new State(RAIL_PUFF_R2 + 15, 100, DoSuicide),}};

    public static final int LASER_PUFF = 3201;
    public static final int LASER_PUFF_RATE = 8;
    public static final State[] s_LaserPuff = {new State(LASER_PUFF, LASER_PUFF_RATE, null), new State(LASER_PUFF, 100, DoSuicide)};

    public static final int TRACER = 3201;
    public static final int TRACER_RATE = 6;


    public static final State[] s_Tracer = {new State(TRACER, TRACER_RATE, DoTracer), new State(TRACER + 1, TRACER_RATE, DoTracer), new State(TRACER + 2, TRACER_RATE, DoTracer), new State(TRACER + 3, TRACER_RATE, DoTracer), new State(TRACER + 4, TRACER_RATE, DoTracer), new State(TRACER + 5, TRACER_RATE, DoTracer)};

    public static final int EMP = 2058;
    public static final int EMP_RATE = 6;


    public static final State[] s_EMP = {new State(EMP, EMP_RATE, DoEMP), new State(EMP + 1, EMP_RATE, DoEMP), new State(EMP + 2, EMP_RATE, DoEMP)};


    public static final State[] s_EMPBurst = {new State(EMP, EMP_RATE, DoEMPBurst), new State(EMP + 1, EMP_RATE, DoEMPBurst), new State(EMP + 2, EMP_RATE, DoEMPBurst)};


    public static final State[] s_EMPShrap = {new State(EMP, EMP_RATE, DoFastShrapJumpFall), new State(EMP + 1, EMP_RATE, DoFastShrapJumpFall), new State(EMP + 2, EMP_RATE, DoFastShrapJumpFall)};

    public static final State[] s_TankShell = {new State(TRACER, 200, DoTankShell).setNext()};


    public static final int VEHICLE_SMOKE_RATE = 18;
    public static final State[] s_VehicleSmoke = {new State(PUFF, VEHICLE_SMOKE_RATE, DoVehicleSmoke), new State(PUFF + 1, VEHICLE_SMOKE_RATE, DoVehicleSmoke), new State(PUFF + 2, VEHICLE_SMOKE_RATE, DoVehicleSmoke), new State(PUFF + 3, VEHICLE_SMOKE_RATE, DoVehicleSmoke), new State(PUFF + 4, VEHICLE_SMOKE_RATE, DoVehicleSmoke), new State(PUFF + 5, VEHICLE_SMOKE_RATE, DoVehicleSmoke), new State(PUFF + 5, 100, DoSuicide).setNext()};


    public static final int WATER_SMOKE_RATE = 18;
    public static final State[] s_WaterSmoke = {new State(PUFF, WATER_SMOKE_RATE, DoWaterSmoke), new State(PUFF + 1, WATER_SMOKE_RATE, DoWaterSmoke), new State(PUFF + 2, WATER_SMOKE_RATE, DoWaterSmoke), new State(PUFF + 3, WATER_SMOKE_RATE, DoWaterSmoke), new State(PUFF + 4, WATER_SMOKE_RATE, DoWaterSmoke), new State(PUFF + 5, WATER_SMOKE_RATE, DoWaterSmoke), new State(PUFF + 5, 100, DoSuicide).setNext()};


    public static final int UZI_SPARK_REPEAT = 24;
    public static final int UZI_SMOKE_REPEAT = 24; // Was 32
    public static final int UZI_SMOKE_RATE = 16; // Was 9
    public static final State[] s_UziSmoke = {new State(UZI_SMOKE, UZI_SMOKE_RATE, DoUziSmoke), new State(UZI_SMOKE + 1, UZI_SMOKE_RATE, DoUziSmoke), new State(UZI_SMOKE + 2, UZI_SMOKE_RATE, DoUziSmoke), new State(UZI_SMOKE + 3, UZI_SMOKE_RATE, DoUziSmoke), new State(UZI_SMOKE + 3, 100, DoSuicide)};

    public static final int SHOTGUN_SMOKE_RATE = 16;
    public static final int SHOTGUN_SMOKE_REPEAT = 18; // Was 32
    public static final int SHOTGUN_SMOKE = UZI_SMOKE + 1;

    public static final State[] s_ShotgunSmoke = {new State(UZI_SMOKE, SHOTGUN_SMOKE_RATE, DoShotgunSmoke), new State(UZI_SMOKE + 1, SHOTGUN_SMOKE_RATE, DoShotgunSmoke), new State(UZI_SMOKE + 2, SHOTGUN_SMOKE_RATE, DoShotgunSmoke), new State(UZI_SMOKE + 3, SHOTGUN_SMOKE_RATE, DoShotgunSmoke), new State(UZI_SMOKE + 3, 100, DoSuicide)};

    public static final int UZI_BULLET_RATE = 100;
    public static final int UZI_BULLET = 717; // actually a bubble


    public static final State[] s_UziBullet = {new State(UZI_BULLET, UZI_BULLET_RATE, DoUziBullet).setNext()};

    public static final int UZI_SPARK_RATE = 8;

    public static final State[] s_UziSpark = {new State(UZI_SPARK, UZI_SPARK_RATE, null), new State(UZI_SPARK + 1, UZI_SPARK_RATE, null), new State(UZI_SPARK + 2, UZI_SPARK_RATE, null), new State(UZI_SPARK + 3, UZI_SPARK_RATE, null), new State(UZI_SPARK + 4, UZI_SPARK_RATE, null), new State(UZI_SPARK + 4, 100, DoSuicide)};

    public static final State[] s_UziPowerSpark = {new State(UZI_SPARK, UZI_SPARK_RATE, DoUziSmoke), new State(UZI_SPARK + 1, UZI_SPARK_RATE, DoUziSmoke), new State(UZI_SPARK + 2, UZI_SPARK_RATE, DoUziSmoke), new State(UZI_SPARK + 3, UZI_SPARK_RATE, DoUziSmoke), new State(UZI_SPARK + 4, UZI_SPARK_RATE, DoUziSmoke), new State(UZI_SPARK + 4, 100, DoSuicide)};

    public static final int BUBBLE = 716;
    public static final int BUBBLE_RATE = 100;


    public static final State[] s_Bubble = {new State(BUBBLE, BUBBLE_RATE, DoBubble).setNext()};

    public static final int SPLASH = 772;
    public static final int SPLASH_RATE = 10;

    public static final State[] s_Splash = {new State(SPLASH, SPLASH_RATE, null), new State(SPLASH + 1, SPLASH_RATE, null), new State(SPLASH + 2, SPLASH_RATE, null), new State(SPLASH + 3, SPLASH_RATE, null), new State(SPLASH + 4, SPLASH_RATE, null), new State(SPLASH + 4, 100, DoSuicide)};

    public static final int CROSSBOLT = 2230;
    public static final int CROSSBOLT_RATE = 100;


    public static final State[][] s_CrossBolt = {{new State(CROSSBOLT, CROSSBOLT_RATE, DoCrossBolt).setNext(),}, {new State(CROSSBOLT + 2, CROSSBOLT_RATE, DoCrossBolt).setNext(),}, {new State(CROSSBOLT + 3, CROSSBOLT_RATE, DoCrossBolt).setNext(),}, {new State(CROSSBOLT + 4, CROSSBOLT_RATE, DoCrossBolt).setNext(),}, {new State(CROSSBOLT + 1, CROSSBOLT_RATE, DoCrossBolt).setNext(),}};

    public static final int STAR = 2102;
    public static final int STAR_RATE = 6;


    public static final State[] s_Star = {new State(STAR, STAR_RATE, DoStar), new State(STAR + 1, STAR_RATE, DoStar), new State(STAR + 2, STAR_RATE, DoStar), new State(STAR + 3, STAR_RATE, DoStar)};

    public static final State[] s_StarStuck = {new State(STAR, STAR_RATE, null).setNext()};

    public static final int STAR_DOWN = 2066;
    public static final State[] s_StarDown = {new State(STAR_DOWN, STAR_RATE, DoStar), new State(STAR_DOWN + 1, STAR_RATE, DoStar), new State(STAR_DOWN + 2, STAR_RATE, DoStar), new State(STAR_DOWN + 3, STAR_RATE, DoStar)};

    public static final State[] s_StarDownStuck = {new State(STAR, STAR_RATE, null).setNext()};

//////////////////////
//
//LAVA BOSS
//
//////////////////////

    public static final int LAVA_BOULDER_RATE = 6;


    public static final State[] s_LavaBoulder = {new State(LAVA_BOULDER + 1, LAVA_BOULDER_RATE, DoLavaBoulder), new State(LAVA_BOULDER + 2, LAVA_BOULDER_RATE, DoLavaBoulder), new State(LAVA_BOULDER + 3, LAVA_BOULDER_RATE, DoLavaBoulder), new State(LAVA_BOULDER + 4, LAVA_BOULDER_RATE, DoLavaBoulder), new State(LAVA_BOULDER + 5, LAVA_BOULDER_RATE, DoLavaBoulder), new State(LAVA_BOULDER + 6, LAVA_BOULDER_RATE, DoLavaBoulder), new State(LAVA_BOULDER + 7, LAVA_BOULDER_RATE, DoLavaBoulder), new State(LAVA_BOULDER + 8, LAVA_BOULDER_RATE, DoLavaBoulder)};

    public static final int LAVA_SHARD = (LAVA_BOULDER + 1);

    public static final State[] s_LavaShard = {new State(LAVA_BOULDER + 1, LAVA_BOULDER_RATE, DoShrapDamage), new State(LAVA_BOULDER + 2, LAVA_BOULDER_RATE, DoShrapDamage), new State(LAVA_BOULDER + 3, LAVA_BOULDER_RATE, DoShrapDamage), new State(LAVA_BOULDER + 4, LAVA_BOULDER_RATE, DoShrapDamage), new State(LAVA_BOULDER + 5, LAVA_BOULDER_RATE, DoShrapDamage), new State(LAVA_BOULDER + 6, LAVA_BOULDER_RATE, DoShrapDamage), new State(LAVA_BOULDER + 7, LAVA_BOULDER_RATE, DoShrapDamage), new State(LAVA_BOULDER + 8, LAVA_BOULDER_RATE, DoShrapDamage)};

    public static final State[] s_VulcanBoulder = {new State(LAVA_BOULDER + 1, LAVA_BOULDER_RATE, DoVulcanBoulder), new State(LAVA_BOULDER + 2, LAVA_BOULDER_RATE, DoVulcanBoulder), new State(LAVA_BOULDER + 3, LAVA_BOULDER_RATE, DoVulcanBoulder), new State(LAVA_BOULDER + 4, LAVA_BOULDER_RATE, DoVulcanBoulder), new State(LAVA_BOULDER + 5, LAVA_BOULDER_RATE, DoVulcanBoulder), new State(LAVA_BOULDER + 6, LAVA_BOULDER_RATE, DoVulcanBoulder), new State(LAVA_BOULDER + 7, LAVA_BOULDER_RATE, DoVulcanBoulder), new State(LAVA_BOULDER + 8, LAVA_BOULDER_RATE, DoVulcanBoulder)};

//////////////////////
//
//GRENADE
//
//////////////////////

    public static final int GRENADE_FRAMES = 1;
    public static final int GRENADE_R0 = 2110;
    public static final int GRENADE_R1 = GRENADE_R0 + (GRENADE_FRAMES);
    public static final int GRENADE_R2 = GRENADE_R0 + (GRENADE_FRAMES * 2);
    public static final int GRENADE_R3 = GRENADE_R0 + (GRENADE_FRAMES * 3);
    public static final int GRENADE_R4 = GRENADE_R0 + (GRENADE_FRAMES * 4);

    public static final int GRENADE = GRENADE_R0;
    public static final int GRENADE_RATE = 8;

    public static final State[][] s_Grenade = {{new State(GRENADE_R0, GRENADE_RATE, DoGrenade).setNext(),}, {new State(GRENADE_R1, GRENADE_RATE, DoGrenade).setNext(),}, {new State(GRENADE_R2, GRENADE_RATE, DoGrenade).setNext(),}, {new State(GRENADE_R3, GRENADE_RATE, DoGrenade).setNext(),}, {new State(GRENADE_R4, GRENADE_RATE, DoGrenade).setNext(),}};

//////////////////////
//
//MINE
//
//////////////////////


    public static final int MINE = 2223;
//    public static final int MINE_SHRAP = 5011;
    public static final int MINE_RATE = 16;

    public static final State[] s_MineStuck = {new State(MINE, MINE_RATE, DoMineStuck).setNext()};

    public static final State[] s_Mine = {new State(MINE, MINE_RATE, DoMine), new State(MINE + 1, MINE_RATE, DoMine)};


    public static final State[] s_MineSpark = {new State(UZI_SPARK, UZI_SPARK_RATE, DoMineSpark), new State(UZI_SPARK + 1, UZI_SPARK_RATE, DoMineSpark), new State(UZI_SPARK + 2, UZI_SPARK_RATE, DoMineSpark), new State(UZI_SPARK + 3, UZI_SPARK_RATE, DoMineSpark), new State(UZI_SPARK + 4, UZI_SPARK_RATE, DoMineSpark), new State(UZI_SPARK + 4, 100, DoSuicide)};

//////////////////////
//
//METEOR
//
//////////////////////

    public static final int METEOR_R0 = 2098;
    public static final int METEOR_R1 = 2090;
    public static final int METEOR_R2 = 2094;
    public static final int METEOR_R3 = 2090;
    public static final int METEOR_R4 = 2098;

//    public static final int METEOR = STAR;
    public static final int METEOR_RATE = 8;

    public static final State[][] s_Meteor = {{new State(METEOR_R0, METEOR_RATE, DoMeteor), new State(METEOR_R0 + 1, METEOR_RATE, DoMeteor), new State(METEOR_R0 + 2, METEOR_RATE, DoMeteor), new State(METEOR_R0 + 3, METEOR_RATE, DoMeteor),}, {new State(METEOR_R1, METEOR_RATE, DoMeteor), new State(METEOR_R1 + 1, METEOR_RATE, DoMeteor), new State(METEOR_R1 + 2, METEOR_RATE, DoMeteor), new State(METEOR_R1 + 3, METEOR_RATE, DoMeteor),}, {new State(METEOR_R2, METEOR_RATE, DoMeteor), new State(METEOR_R2 + 1, METEOR_RATE, DoMeteor), new State(METEOR_R2 + 2, METEOR_RATE, DoMeteor), new State(METEOR_R2 + 3, METEOR_RATE, DoMeteor),}, {new State(METEOR_R3, METEOR_RATE, DoMeteor), new State(METEOR_R3 + 1, METEOR_RATE, DoMeteor), new State(METEOR_R3 + 2, METEOR_RATE, DoMeteor), new State(METEOR_R3 + 3, METEOR_RATE, DoMeteor),}, {new State(METEOR_R4, METEOR_RATE, DoMeteor), new State(METEOR_R4 + 1, METEOR_RATE, DoMeteor), new State(METEOR_R4 + 2, METEOR_RATE, DoMeteor), new State(METEOR_R4 + 3, METEOR_RATE, DoMeteor),}};

    public static final int METEOR_EXP = 2115;
    public static final int METEOR_EXP_RATE = 7;

    public static final State[] s_MeteorExp = {new State(METEOR_EXP, METEOR_EXP_RATE, null), new State(METEOR_EXP + 1, METEOR_EXP_RATE, null), new State(METEOR_EXP + 2, METEOR_EXP_RATE, null), new State(METEOR_EXP + 3, METEOR_EXP_RATE, null), new State(METEOR_EXP + 4, METEOR_EXP_RATE, null), new State(METEOR_EXP + 5, METEOR_EXP_RATE, null), new State(METEOR_EXP + 5, METEOR_EXP_RATE, DoSuicide).setNext()};

    public static final int MIRV_METEOR = METEOR_R0;

    public static final State[][] s_MirvMeteor = {{new State(METEOR_R0, METEOR_RATE, DoMirvMissile), new State(METEOR_R0 + 1, METEOR_RATE, DoMirvMissile), new State(METEOR_R0 + 2, METEOR_RATE, DoMirvMissile), new State(METEOR_R0 + 3, METEOR_RATE, DoMirvMissile),}, {new State(METEOR_R1, METEOR_RATE, DoMirvMissile), new State(METEOR_R1 + 1, METEOR_RATE, DoMirvMissile), new State(METEOR_R1 + 2, METEOR_RATE, DoMirvMissile), new State(METEOR_R1 + 3, METEOR_RATE, DoMirvMissile),}, {new State(METEOR_R2, METEOR_RATE, DoMirvMissile), new State(METEOR_R2 + 1, METEOR_RATE, DoMirvMissile), new State(METEOR_R2 + 2, METEOR_RATE, DoMirvMissile), new State(METEOR_R2 + 3, METEOR_RATE, DoMirvMissile),}, {new State(METEOR_R3, METEOR_RATE, DoMirvMissile), new State(METEOR_R3 + 1, METEOR_RATE, DoMirvMissile), new State(METEOR_R3 + 2, METEOR_RATE, DoMirvMissile), new State(METEOR_R3 + 3, METEOR_RATE, DoMirvMissile),}, {new State(METEOR_R4, METEOR_RATE, DoMirvMissile), new State(METEOR_R4 + 1, METEOR_RATE, DoMirvMissile), new State(METEOR_R4 + 2, METEOR_RATE, DoMirvMissile), new State(METEOR_R4 + 3, METEOR_RATE, DoMirvMissile),}};

    public static final State[] s_MirvMeteorExp = {new State(METEOR_EXP, METEOR_EXP_RATE, null), new State(METEOR_EXP + 1, METEOR_EXP_RATE, null), new State(METEOR_EXP + 2, METEOR_EXP_RATE, null), new State(METEOR_EXP + 3, METEOR_EXP_RATE, null), new State(METEOR_EXP + 4, METEOR_EXP_RATE, null), new State(METEOR_EXP + 5, METEOR_EXP_RATE, null), new State(METEOR_EXP + 5, METEOR_EXP_RATE, DoSuicide).setNext()};

    public static final int SERP_METEOR = METEOR_R0 + 1;

    public static final State[][] s_SerpMeteor = {{new State(2031, METEOR_RATE, DoSerpMeteor), new State(2031 + 1, METEOR_RATE, DoSerpMeteor), new State(2031 + 2, METEOR_RATE, DoSerpMeteor), new State(2031 + 3, METEOR_RATE, DoSerpMeteor),}, {new State(2031, METEOR_RATE, DoSerpMeteor), new State(2031 + 1, METEOR_RATE, DoSerpMeteor), new State(2031 + 2, METEOR_RATE, DoSerpMeteor), new State(2031 + 3, METEOR_RATE, DoSerpMeteor),}, {new State(2031, METEOR_RATE, DoSerpMeteor), new State(2031 + 1, METEOR_RATE, DoSerpMeteor), new State(2031 + 2, METEOR_RATE, DoSerpMeteor), new State(2031 + 3, METEOR_RATE, DoSerpMeteor),}, {new State(2031, METEOR_RATE, DoSerpMeteor), new State(2031 + 1, METEOR_RATE, DoSerpMeteor), new State(2031 + 2, METEOR_RATE, DoSerpMeteor), new State(2031 + 3, METEOR_RATE, DoSerpMeteor),}, {new State(2031, METEOR_RATE, DoSerpMeteor), new State(2031 + 1, METEOR_RATE, DoSerpMeteor), new State(2031 + 2, METEOR_RATE, DoSerpMeteor), new State(2031 + 3, METEOR_RATE, DoSerpMeteor),}};

    public static final State[] s_SerpMeteorExp = {new State(METEOR_EXP, METEOR_EXP_RATE, null), new State(METEOR_EXP + 1, METEOR_EXP_RATE, null), new State(METEOR_EXP + 2, METEOR_EXP_RATE, null), new State(METEOR_EXP + 3, METEOR_EXP_RATE, null), new State(METEOR_EXP + 4, METEOR_EXP_RATE, null), new State(METEOR_EXP + 5, METEOR_EXP_RATE, null), new State(METEOR_EXP + 5, METEOR_EXP_RATE, DoSuicide).setNext()};

//////////////////////
//
//SPEAR
//
//////////////////////

    public static final int SPEAR_RATE = 8;

    public static final State[][] s_Spear = {{new State(SPEAR_R0, SPEAR_RATE, DoSpear).setNext(),}, {new State(SPEAR_R1, SPEAR_RATE, DoSpear).setNext(),}, {new State(SPEAR_R2, SPEAR_RATE, DoSpear).setNext(),}, {new State(SPEAR_R3, SPEAR_RATE, DoSpear).setNext(),}, {new State(SPEAR_R4, SPEAR_RATE, DoSpear).setNext(),}};

//////////////////////
//
//ROCKET
//
//////////////////////

    public static final int ROCKET_FRAMES = 1;
    public static final int ROCKET_R0 = 2206;
    public static final int ROCKET_R1 = ROCKET_R0 + (ROCKET_FRAMES * 2);
    public static final int ROCKET_R2 = ROCKET_R0 + (ROCKET_FRAMES * 3);
    public static final int ROCKET_R3 = ROCKET_R0 + (ROCKET_FRAMES * 4);
    public static final int ROCKET_R4 = ROCKET_R0 + (ROCKET_FRAMES);


    public static final int ROCKET_RATE = 8;

    public static final State[][] s_Rocket = {{new State(ROCKET_R0, ROCKET_RATE, DoRocket).setNext(),}, {new State(ROCKET_R1, ROCKET_RATE, DoRocket).setNext(),}, {new State(ROCKET_R2, ROCKET_RATE, DoRocket).setNext(),}, {new State(ROCKET_R3, ROCKET_RATE, DoRocket).setNext(),}, {new State(ROCKET_R4, ROCKET_RATE, DoRocket).setNext(),}};

//////////////////////
//
//BUNNY ROCKET
//
//////////////////////

    public static final int BUNNYROCKET_FRAMES = 5;
    public static final int BUNNYROCKET_R0 = 4550;
    public static final int BUNNYROCKET_R1 = BUNNYROCKET_R0 + (BUNNYROCKET_FRAMES);
    public static final int BUNNYROCKET_R2 = BUNNYROCKET_R0 + (BUNNYROCKET_FRAMES * 2);
    public static final int BUNNYROCKET_R3 = BUNNYROCKET_R0 + (BUNNYROCKET_FRAMES * 3);
    public static final int BUNNYROCKET_R4 = BUNNYROCKET_R0 + (BUNNYROCKET_FRAMES * 4);

    public static final int BUNNYROCKET_RATE = 8;

    public static final State[][] s_BunnyRocket = {{new State(BUNNYROCKET_R0 + 2, BUNNYROCKET_RATE, DoRocket).setNext(),}, {new State(BUNNYROCKET_R1 + 2, BUNNYROCKET_RATE, DoRocket).setNext(),}, {new State(BUNNYROCKET_R2 + 2, BUNNYROCKET_RATE, DoRocket).setNext(),}, {new State(BUNNYROCKET_R3 + 2, BUNNYROCKET_RATE, DoRocket).setNext(),}, {new State(BUNNYROCKET_R4 + 2, BUNNYROCKET_RATE, DoRocket).setNext(),}};


    public static final int RAIL_RATE = 8;

    public static final State[][] s_Rail = {{new State(ROCKET_R0, RAIL_RATE, DoRail).setNext(),}, {new State(ROCKET_R1, RAIL_RATE, DoRail).setNext(),}, {new State(ROCKET_R2, RAIL_RATE, DoRail).setNext(),}, {new State(ROCKET_R3, RAIL_RATE, DoRail).setNext(),}, {new State(ROCKET_R4, RAIL_RATE, DoRail).setNext(),}};


    public static final int LASER_RATE = 8;

    public static final State[] s_Laser = {new State(ROCKET_R0, LASER_RATE, DoLaser).setNext()};

//////////////////////
//
//MICRO
//
//////////////////////

    public static final int MICRO_FRAMES = 1;
    public static final int MICRO_R0 = 2206;
    public static final int MICRO_R1 = MICRO_R0 + (MICRO_FRAMES * 2);
    public static final int MICRO_R2 = MICRO_R0 + (MICRO_FRAMES * 3);
    public static final int MICRO_R3 = MICRO_R0 + (MICRO_FRAMES * 4);
    public static final int MICRO_R4 = MICRO_R0 + (MICRO_FRAMES);


    public static final int MICRO_RATE = 8;

    public static final State[][] s_Micro = {{new State(MICRO_R0, MICRO_RATE, DoMicro).setNext(),}, {new State(MICRO_R1, MICRO_RATE, DoMicro).setNext(),}, {new State(MICRO_R2, MICRO_RATE, DoMicro).setNext(),}, {new State(MICRO_R3, MICRO_RATE, DoMicro).setNext(),}, {new State(MICRO_R4, MICRO_RATE, DoMicro).setNext(),}};


    public static final State[][] s_MicroMini = {{new State(MICRO_R0, MICRO_RATE, DoMicroMini).setNext(),}, {new State(MICRO_R1, MICRO_RATE, DoMicroMini).setNext(),}, {new State(MICRO_R2, MICRO_RATE, DoMicroMini).setNext(),}, {new State(MICRO_R3, MICRO_RATE, DoMicroMini).setNext(),}, {new State(MICRO_R4, MICRO_RATE, DoMicroMini).setNext(),}};

//////////////////////
//
//BOLT THINMAN
//
//////////////////////

    public static final int BOLT_THINMAN_RATE = 8;


    public static final State[][] s_BoltThinMan = {{new State(BOLT_THINMAN_R0, BOLT_THINMAN_RATE, DoBoltThinMan).setNext(),}, {new State(BOLT_THINMAN_R1, BOLT_THINMAN_RATE, DoBoltThinMan).setNext(),}, {new State(BOLT_THINMAN_R2, BOLT_THINMAN_RATE, DoBoltThinMan).setNext(),}, {new State(BOLT_THINMAN_R3, BOLT_THINMAN_RATE, DoBoltThinMan).setNext(),}, {new State(BOLT_THINMAN_R4, BOLT_THINMAN_RATE, DoBoltThinMan).setNext(),}};

    public static final int BOLT_SEEKER_RATE = 8;


    public static final State[][] s_BoltSeeker = {{new State(BOLT_THINMAN_R0, BOLT_SEEKER_RATE, DoBoltSeeker).setNext(),}, {new State(BOLT_THINMAN_R1, BOLT_SEEKER_RATE, DoBoltSeeker).setNext(),}, {new State(BOLT_THINMAN_R2, BOLT_SEEKER_RATE, DoBoltSeeker).setNext(),}, {new State(BOLT_THINMAN_R3, BOLT_SEEKER_RATE, DoBoltSeeker).setNext(),}, {new State(BOLT_THINMAN_R4, BOLT_SEEKER_RATE, DoBoltSeeker).setNext(),}};

    public static final int BOLT_FATMAN = STAR;
    public static final int BOLT_FATMAN_RATE = 8;


    public static final State[] s_BoltFatMan = {new State(BOLT_FATMAN, BOLT_FATMAN_RATE, DoBoltFatMan), new State(BOLT_FATMAN + 1, BOLT_FATMAN_RATE, DoBoltFatMan), new State(BOLT_FATMAN + 2, BOLT_FATMAN_RATE, DoBoltFatMan), new State(BOLT_FATMAN + 3, BOLT_FATMAN_RATE, DoBoltFatMan)};

    public static final int BOLT_SHRAPNEL = STAR;
    public static final int BOLT_SHRAPNEL_RATE = 8;


    public static final State[] s_BoltShrapnel = {new State(BOLT_SHRAPNEL, BOLT_SHRAPNEL_RATE, DoBoltShrapnel), new State(BOLT_SHRAPNEL + 1, BOLT_SHRAPNEL_RATE, DoBoltShrapnel), new State(BOLT_SHRAPNEL + 2, BOLT_SHRAPNEL_RATE, DoBoltShrapnel), new State(BOLT_SHRAPNEL + 3, BOLT_SHRAPNEL_RATE, DoBoltShrapnel)};

    public static final int COOLG_FIRE = 2430;
    //public static final int COOLG_FIRE =1465;
    public static final int COOLG_FIRE_RATE = 8;

    public static final State[] s_CoolgFire = {new State(2031, COOLG_FIRE_RATE, DoCoolgFire), new State(2031 + 1, COOLG_FIRE_RATE, DoCoolgFire), new State(2031 + 2, COOLG_FIRE_RATE, DoCoolgFire), new State(2031 + 3, COOLG_FIRE_RATE, DoCoolgFire)};

    public static final int COOLG_FIRE_DONE = 2410;
    public static final int COOLG_FIRE_DONE_RATE = 3;

    public static final State[] s_CoolgFireDone = {new State(COOLG_FIRE_DONE, COOLG_FIRE_DONE_RATE, null), new State(COOLG_FIRE_DONE + 1, COOLG_FIRE_DONE_RATE, null), new State(COOLG_FIRE_DONE + 2, COOLG_FIRE_DONE_RATE, null), new State(COOLG_FIRE_DONE + 3, COOLG_FIRE_DONE_RATE, null), new State(COOLG_FIRE_DONE + 4, COOLG_FIRE_DONE_RATE, null), new State(COOLG_FIRE_DONE + 4, COOLG_FIRE_DONE_RATE, DoSuicide).setNext()};


    public static final int COOLG_DRIP = 1720;
    public static final State[] s_CoolgDrip = {new State(COOLG_DRIP, 100, DoCoolgDrip).setNext()};

    public static final int GORE_FLOOR_SPLASH_RATE = 8;
    public static final int GORE_FLOOR_SPLASH = 1710;
    public static final State[] s_GoreFloorSplash = {new State(GORE_FLOOR_SPLASH, GORE_FLOOR_SPLASH_RATE, null), new State(GORE_FLOOR_SPLASH + 1, GORE_FLOOR_SPLASH_RATE, null), new State(GORE_FLOOR_SPLASH + 2, GORE_FLOOR_SPLASH_RATE, null), new State(GORE_FLOOR_SPLASH + 3, GORE_FLOOR_SPLASH_RATE, null), new State(GORE_FLOOR_SPLASH + 4, GORE_FLOOR_SPLASH_RATE, null), new State(GORE_FLOOR_SPLASH + 5, GORE_FLOOR_SPLASH_RATE, null), new State(GORE_FLOOR_SPLASH + 5, GORE_FLOOR_SPLASH_RATE, DoSuicide).setNext()};

    public static final int GORE_SPLASH_RATE = 8;
    public static final int GORE_SPLASH = 2410;
    public static final State[] s_GoreSplash = {new State(GORE_SPLASH, GORE_SPLASH_RATE, null), new State(GORE_SPLASH + 1, GORE_SPLASH_RATE, null), new State(GORE_SPLASH + 2, GORE_SPLASH_RATE, null), new State(GORE_SPLASH + 3, GORE_SPLASH_RATE, null), new State(GORE_SPLASH + 4, GORE_SPLASH_RATE, null), new State(GORE_SPLASH + 5, GORE_SPLASH_RATE, null), new State(GORE_SPLASH + 5, GORE_SPLASH_RATE, DoSuicide).setNext()};

//////////////////////////////////////////////
//
//HEART ATTACK & PLASMA
//
//////////////////////////////////////////////

    public static final int PLASMA = 1562; // 2058
    public static final int PLASMA_FOUNTAIN = 2058 + 1;
    public static final int PLASMA_RATE = 8;
    public static final int PLASMA_FOUNTAIN_TIME = (3 * 120);

    //regular bolt from heart
    public static final State[] s_Plasma = {new State(PLASMA, PLASMA_RATE, DoPlasma), new State(PLASMA + 1, PLASMA_RATE, DoPlasma), new State(PLASMA + 2, PLASMA_RATE, DoPlasma)};


    //follows actor spewing blood
    public static final int PLASMA_Drip = 1562; // 2420
    public static final State[] s_PlasmaFountain = {new State(PLASMA_Drip, PLASMA_RATE, DoPlasmaFountain), new State(PLASMA_Drip + 1, PLASMA_RATE, DoPlasmaFountain), new State(PLASMA_Drip + 2, PLASMA_RATE, DoPlasmaFountain), new State(PLASMA_Drip + 3, PLASMA_RATE, DoPlasmaFountain)};

    public static final int PLASMA_Drip_RATE = 12;


    public static final State[] s_PlasmaDrip = {new State(PLASMA_Drip, PLASMA_Drip_RATE, DoShrapJumpFall), new State(PLASMA_Drip + 1, PLASMA_Drip_RATE, DoShrapJumpFall), new State(PLASMA_Drip + 2, PLASMA_Drip_RATE, DoShrapJumpFall), new State(PLASMA_Drip + 3, PLASMA_Drip_RATE, DoShrapJumpFall), new State(PLASMA_Drip + 4, PLASMA_Drip_RATE, DoShrapJumpFall), new State(PLASMA_Drip + 5, PLASMA_Drip_RATE, DoShrapJumpFall), new State(PLASMA_Drip + 7, PLASMA_Drip_RATE, DoSuicide).setNext()};

//    public static final int PLASMA_DONE = 2061;
    public static final int PLASMA_DONE_RATE = 15;


    public static final State[] s_PlasmaDone = {new State(PLASMA, PLASMA_DONE_RATE, DoPlasmaDone), new State(PLASMA + 2, PLASMA_DONE_RATE, DoPlasmaDone), new State(PLASMA + 1, PLASMA_DONE_RATE, DoPlasmaDone)};

    public static final int TELEPORT_EFFECT = 3240;
    public static final int TELEPORT_EFFECT_RATE = 6;

    public static final State[] s_TeleportEffect = {new State(TELEPORT_EFFECT, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 1, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 2, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 3, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 4, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 5, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 6, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 7, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 8, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 9, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 10, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 11, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 12, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 13, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 14, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 15, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 16, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 17, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 17, TELEPORT_EFFECT_RATE, DoSuicide).setNext()};


    public static final State[] s_TeleportEffect2 = {new State(TELEPORT_EFFECT, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 1, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 2, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 3, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 4, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 5, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 6, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 7, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 8, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 9, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 10, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 11, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 12, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 13, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 14, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 15, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 16, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 17, TELEPORT_EFFECT_RATE, null), new State(TELEPORT_EFFECT + 17, SF_QUICK_CALL, DoTeleRipper), new State(TELEPORT_EFFECT + 17, TELEPORT_EFFECT_RATE, DoSuicide).setNext()};


//    public static final int ELECTRO_SNAKE = 2073;
    public static final int ELECTRO_PLAYER = (ELECTRO);
    public static final int ELECTRO_ENEMY = (ELECTRO + 1);
    public static final int ELECTRO_SHARD = (ELECTRO + 2);

    public static final State[] s_Electro = {new State(ELECTRO, 12, DoElectro), new State(ELECTRO + 1, 12, DoElectro), new State(ELECTRO + 2, 12, DoElectro), new State(ELECTRO + 3, 12, DoElectro)};

    public static final State[] s_ElectroShrap = {new State(ELECTRO, 12, DoShrapDamage), new State(ELECTRO + 1, 12, DoShrapDamage), new State(ELECTRO + 2, 12, DoShrapDamage), new State(ELECTRO + 3, 12, DoShrapDamage)};

    //////////////////////
//
//EXPS
//
//////////////////////
    public static final int GRENADE_EXP = 3121;
    public static final int GRENADE_EXP_RATE = 6;

    public static final State[] s_GrenadeSmallExp = {new State(GRENADE_EXP, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 1, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 2, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 3, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 4, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 5, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 6, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 7, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 8, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 9, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 10, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 11, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 12, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 13, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 14, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 15, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 16, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 17, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 18, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 19, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 20, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 21, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 21, 100, DoSuicide).setNext()};


    public static final State[] s_GrenadeExp = {new State(GRENADE_EXP, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 1, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 2, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 3, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 4, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 5, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 6, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 6, SF_QUICK_CALL, SpawnGrenadeSmallExp), new State(GRENADE_EXP + 7, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 8, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 9, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 10, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 11, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 12, SF_QUICK_CALL, SpawnGrenadeSmallExp), new State(GRENADE_EXP + 12, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 13, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 14, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 15, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 16, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 17, SF_QUICK_CALL, SpawnGrenadeSmallExp), new State(GRENADE_EXP + 17, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 18, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 19, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 20, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 21, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 21, 100, DoSuicide).setNext()};

    public static final int MINE_EXP = GRENADE_EXP + 1;


    public static final State[] s_MineExp = {new State(GRENADE_EXP, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 1, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 2, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 3, SF_QUICK_CALL, DoMineExp), new State(GRENADE_EXP + 3, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 4, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 5, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 6, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 7, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 8, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 9, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 10, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 11, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 12, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 13, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 14, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 15, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 16, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 17, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 17, SF_QUICK_CALL, DoMineExpMine), new State(GRENADE_EXP + 18, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 19, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 20, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 21, GRENADE_EXP_RATE, null), new State(GRENADE_EXP + 21, 100, DoSuicide).setNext()};

    public static final int EXP_RATE = 7;
    public static final int BOLT_EXP = EXP;
    public static final int FIREBALL_EXP = EXP + 1;
//    public static final int BASIC_EXP = EXP + 2;
    public static final int SECTOR_EXP = EXP + 3;
    public static final int MICRO_EXP = EXP + 5;
    public static final int TRACER_EXP = EXP + 6;
    public static final int TANK_SHELL_EXP = EXP + 7;

    public static final State[] s_BasicExp = {new State(EXP, EXP_RATE, null), new State(EXP + 1, EXP_RATE, null), new State(EXP + 2, EXP_RATE, null), new State(EXP + 3, EXP_RATE, null), new State(EXP + 4, EXP_RATE, null), new State(EXP + 5, EXP_RATE, null), new State(EXP + 6, EXP_RATE, null), new State(EXP + 7, EXP_RATE, null), new State(EXP + 8, EXP_RATE, null), new State(EXP + 9, EXP_RATE, null), new State(EXP + 10, EXP_RATE, null), new State(EXP + 11, EXP_RATE, null), new State(EXP + 12, EXP_RATE, null), new State(EXP + 13, EXP_RATE, null), new State(EXP + 14, EXP_RATE, null), new State(EXP + 15, EXP_RATE, null), new State(EXP + 16, EXP_RATE, null), new State(EXP + 17, EXP_RATE, null), new State(EXP + 18, EXP_RATE, null), new State(EXP + 19, EXP_RATE, null), new State(EXP + 20, 100, DoSuicide)};

    public static final int MICRO_EXP_RATE = 3;


    public static final State[] s_MicroExp = {new State(EXP, MICRO_EXP_RATE, null), new State(EXP, SF_QUICK_CALL, DoExpDamageTest), new State(EXP + 1, MICRO_EXP_RATE, null), new State(EXP + 2, MICRO_EXP_RATE, null), new State(EXP + 3, MICRO_EXP_RATE, null), new State(EXP + 4, MICRO_EXP_RATE, null), new State(EXP + 5, MICRO_EXP_RATE, null), new State(EXP + 6, MICRO_EXP_RATE, null), new State(EXP + 7, MICRO_EXP_RATE, null), new State(EXP + 8, MICRO_EXP_RATE, null), new State(EXP + 9, MICRO_EXP_RATE, null), new State(EXP + 10, MICRO_EXP_RATE, null), new State(EXP + 11, MICRO_EXP_RATE, null), new State(EXP + 12, MICRO_EXP_RATE, null), new State(EXP + 13, MICRO_EXP_RATE, null), new State(EXP + 14, MICRO_EXP_RATE, null), new State(EXP + 15, MICRO_EXP_RATE, null), new State(EXP + 16, MICRO_EXP_RATE, null), new State(EXP + 17, MICRO_EXP_RATE, null), new State(EXP + 18, MICRO_EXP_RATE, null), new State(EXP + 19, MICRO_EXP_RATE, null), new State(EXP + 20, MICRO_EXP_RATE, null), new State(EXP + 20, 100, DoSuicide).setNext()};

    public static final int BIG_GUN_FLAME_RATE = 15;
    public static final State[] s_BigGunFlame = {
// first 3 frames
            new State(EXP, BIG_GUN_FLAME_RATE, null), new State(EXP + 1, BIG_GUN_FLAME_RATE, null), new State(EXP + 2, BIG_GUN_FLAME_RATE, null),
// last 4 frames frames
            new State(EXP + 17, BIG_GUN_FLAME_RATE, null), new State(EXP + 18, BIG_GUN_FLAME_RATE, null), new State(EXP + 19, BIG_GUN_FLAME_RATE, null), new State(EXP + 20, BIG_GUN_FLAME_RATE, null), new State(EXP + 20, 100, DoSuicide)};

    public static final State[] s_BoltExp = {new State(EXP, EXP_RATE, null), new State(EXP, SF_QUICK_CALL, null), new State(EXP, SF_QUICK_CALL, SpawnShrapX), new State(EXP + 1, EXP_RATE, null), new State(EXP + 2, EXP_RATE, null), new State(EXP + 3, EXP_RATE, null), new State(EXP + 4, EXP_RATE, null), new State(EXP + 5, EXP_RATE, null), new State(EXP + 6, EXP_RATE, null), new State(EXP + 7, EXP_RATE, null), new State(EXP + 7, SF_QUICK_CALL, SpawnShrapX), new State(EXP + 8, EXP_RATE, null), new State(EXP + 9, EXP_RATE, null), new State(EXP + 10, EXP_RATE, null), new State(EXP + 11, EXP_RATE, null), new State(EXP + 12, EXP_RATE, null), new State(EXP + 13, EXP_RATE, null), new State(EXP + 14, EXP_RATE, null), new State(EXP + 15, EXP_RATE, null), new State(EXP + 16, EXP_RATE, null), new State(EXP + 17, EXP_RATE, null), new State(EXP + 18, EXP_RATE, null), new State(EXP + 19, EXP_RATE, null), new State(EXP + 20, EXP_RATE, null), new State(EXP + 20, 100, DoSuicide)};

    public static final State[] s_TankShellExp = {new State(EXP, EXP_RATE, null), new State(EXP, SF_QUICK_CALL, null), new State(EXP, SF_QUICK_CALL, SpawnShrapX), new State(EXP + 1, EXP_RATE, null), new State(EXP + 2, EXP_RATE, null), new State(EXP + 3, EXP_RATE, null), new State(EXP + 4, EXP_RATE, null), new State(EXP + 5, EXP_RATE, null), new State(EXP + 6, EXP_RATE, null), new State(EXP + 7, EXP_RATE, null), new State(EXP + 7, SF_QUICK_CALL, SpawnShrapX), new State(EXP + 8, EXP_RATE, null), new State(EXP + 9, EXP_RATE, null), new State(EXP + 10, EXP_RATE, null), new State(EXP + 11, EXP_RATE, null), new State(EXP + 12, EXP_RATE, null), new State(EXP + 13, EXP_RATE, null), new State(EXP + 14, EXP_RATE, null), new State(EXP + 15, EXP_RATE, null), new State(EXP + 16, EXP_RATE, null), new State(EXP + 17, EXP_RATE, null), new State(EXP + 18, EXP_RATE, null), new State(EXP + 19, EXP_RATE, null), new State(EXP + 20, EXP_RATE, null), new State(EXP + 20, 100, DoSuicide)};

    public static final int TRACER_EXP_RATE = 4;
    public static final State[] s_TracerExp = {new State(EXP, TRACER_EXP_RATE, null), new State(EXP, SF_QUICK_CALL, null), new State(EXP, SF_QUICK_CALL, null), new State(EXP + 1, TRACER_EXP_RATE, null), new State(EXP + 2, TRACER_EXP_RATE, null), new State(EXP + 3, TRACER_EXP_RATE, null), new State(EXP + 4, TRACER_EXP_RATE, null), new State(EXP + 5, TRACER_EXP_RATE, null), new State(EXP + 6, TRACER_EXP_RATE, null), new State(EXP + 7, TRACER_EXP_RATE, null), new State(EXP + 7, SF_QUICK_CALL, null), new State(EXP + 8, TRACER_EXP_RATE, null), new State(EXP + 9, TRACER_EXP_RATE, null), new State(EXP + 10, TRACER_EXP_RATE, null), new State(EXP + 11, TRACER_EXP_RATE, null), new State(EXP + 12, TRACER_EXP_RATE, null), new State(EXP + 13, TRACER_EXP_RATE, null), new State(EXP + 14, TRACER_EXP_RATE, null), new State(EXP + 15, TRACER_EXP_RATE, null), new State(EXP + 16, TRACER_EXP_RATE, null), new State(EXP + 17, TRACER_EXP_RATE, null), new State(EXP + 18, TRACER_EXP_RATE, null), new State(EXP + 19, TRACER_EXP_RATE, null), new State(EXP + 20, TRACER_EXP_RATE, null), new State(EXP + 20, 100, DoSuicide)};

    public static final State[] s_SectorExp = {new State(EXP, EXP_RATE, DoSectorExp), new State(EXP, SF_QUICK_CALL, SpawnShrapX), new State(EXP, SF_QUICK_CALL, DoSectorExp), new State(EXP + 1, EXP_RATE, DoSectorExp), new State(EXP + 2, EXP_RATE, DoSectorExp), new State(EXP + 3, EXP_RATE, DoSectorExp), new State(EXP + 4, EXP_RATE, DoSectorExp), new State(EXP + 5, EXP_RATE, DoSectorExp), new State(EXP + 6, EXP_RATE, DoSectorExp), new State(EXP + 7, EXP_RATE, DoSectorExp), new State(EXP + 7, SF_QUICK_CALL, DoSectorExp), new State(EXP + 8, EXP_RATE, DoSectorExp), new State(EXP + 9, EXP_RATE, DoSectorExp), new State(EXP + 10, EXP_RATE, DoSectorExp), new State(EXP + 11, EXP_RATE, DoSectorExp), new State(EXP + 12, EXP_RATE, DoSectorExp), new State(EXP + 13, EXP_RATE, DoSectorExp), new State(EXP + 14, EXP_RATE, DoSectorExp), new State(EXP + 15, EXP_RATE, DoSectorExp), new State(EXP + 16, EXP_RATE, DoSectorExp), new State(EXP + 17, EXP_RATE, DoSectorExp), new State(EXP + 18, EXP_RATE, DoSectorExp), new State(EXP + 19, EXP_RATE, DoSectorExp), new State(EXP + 20, EXP_RATE, DoSectorExp), new State(EXP + 20, 100, DoSuicide)};

    public static final int FIREBALL_DISS = 3196;
    public static final int FIREBALL_DISS_RATE = 8;
    public static final State[] s_FireballExp = {new State(FIREBALL_DISS, FIREBALL_DISS_RATE, null), new State(FIREBALL_DISS + 1, FIREBALL_DISS_RATE, null), new State(FIREBALL_DISS + 2, FIREBALL_DISS_RATE, null), new State(FIREBALL_DISS + 3, FIREBALL_DISS_RATE, null), new State(FIREBALL_DISS + 4, FIREBALL_DISS_RATE, null), new State(FIREBALL_DISS + 4, 100, DoSuicide)};

    public static final int NAP_EXP = (3072);
    public static final int NAP_EXP_RATE = 6;


    public static final State[] s_NapExp = {new State(NAP_EXP, NAP_EXP_RATE, null), new State(NAP_EXP, SF_QUICK_CALL, DoDamageTest), new State(NAP_EXP + 1, NAP_EXP_RATE, null), new State(NAP_EXP + 2, NAP_EXP_RATE, null), new State(NAP_EXP + 3, NAP_EXP_RATE, null), new State(NAP_EXP + 4, NAP_EXP_RATE, null), new State(NAP_EXP + 5, NAP_EXP_RATE, null), new State(NAP_EXP + 6, NAP_EXP_RATE, null), new State(NAP_EXP + 7, NAP_EXP_RATE, null), new State(NAP_EXP + 8, NAP_EXP_RATE, null), new State(NAP_EXP + 9, NAP_EXP_RATE, null), new State(NAP_EXP + 10, NAP_EXP_RATE, null), new State(NAP_EXP + 11, NAP_EXP_RATE, null), new State(NAP_EXP + 12, NAP_EXP_RATE, null), new State(NAP_EXP + 13, NAP_EXP_RATE, null), new State(NAP_EXP + 14, NAP_EXP_RATE, null), new State(NAP_EXP + 15, NAP_EXP_RATE - 2, null), new State(NAP_EXP + 16, NAP_EXP_RATE - 2, null), new State(NAP_EXP + 17, NAP_EXP_RATE - 2, null), new State(NAP_EXP + 18, NAP_EXP_RATE - 2, null), new State(NAP_EXP + 19, NAP_EXP_RATE - 2, null), new State(NAP_EXP + 20, NAP_EXP_RATE - 2, null), new State(NAP_EXP + 21, NAP_EXP_RATE - 2, null), new State(NAP_EXP + 21, NAP_EXP_RATE - 2, DoSuicide).setNext()};


    public static final int FLAME_RATE = 6;

    public static final State[] s_FireballFlames = {new State(FIREBALL_FLAMES, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 1, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 2, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 3, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 4, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 5, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 6, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 7, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 8, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 9, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 10, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 11, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 12, FLAME_RATE, DoFireballFlames), new State(FIREBALL_FLAMES + 13, FLAME_RATE, DoFireballFlames)};


    public static final State[] s_BreakFlames = {new State(FIREBALL_FLAMES, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 1, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 2, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 3, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 4, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 5, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 6, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 7, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 8, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 9, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 10, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 11, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 12, FLAME_RATE, DoBreakFlames), new State(FIREBALL_FLAMES + 13, FLAME_RATE, DoBreakFlames)};

//////////////////////
//
//FIREBALL
//
//////////////////////


//    public static final int FIREBALL_RATE = 8;
    public static final int GORO_FIREBALL = FIREBALL + 1;

    public static final State[] s_Fireball = {new State(FIREBALL, 12, DoFireball), new State(FIREBALL + 1, 12, DoFireball), new State(FIREBALL + 2, 12, DoFireball), new State(FIREBALL + 3, 12, DoFireball)};


    public static final State[] s_Ring = {new State(FIREBALL, 12, DoRing), new State(FIREBALL + 1, 12, DoRing), new State(FIREBALL + 2, 12, DoRing), new State(FIREBALL + 3, 12, DoRing)};

    public static final State[] s_Ring2 = {new State(2031, 12, DoRing), new State(2031 + 1, 12, DoRing), new State(2031 + 2, 12, DoRing), new State(2031 + 3, 12, DoRing)};


    public static final State[] s_Napalm = {new State(FIREBALL, 12, DoNapalm), new State(FIREBALL + 1, 12, DoNapalm), new State(FIREBALL + 2, 12, DoNapalm), new State(FIREBALL + 3, 12, DoNapalm)};


    public static final int BLOOD_WORM = FIREBALL + 5;
    public static final State[] s_BloodWorm = {new State(FIREBALL, 12, DoBloodWorm), new State(FIREBALL + 1, 12, DoBloodWorm), new State(FIREBALL + 2, 12, DoBloodWorm), new State(FIREBALL + 3, 12, DoBloodWorm)};

    public static final int PLASMA_EXP = (NAP_EXP + 1);
    public static final int PLASMA_EXP_RATE = 4;

    public static final State[] s_PlasmaExp = {new State(PLASMA_EXP, PLASMA_EXP_RATE, null), new State(PLASMA_EXP, SF_QUICK_CALL, DoDamageTest), new State(PLASMA_EXP + 1, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 2, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 3, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 4, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 5, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 6, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 7, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 8, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 9, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 10, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 11, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 9, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 8, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 7, PLASMA_EXP_RATE, null), new State(PLASMA_EXP + 6, PLASMA_EXP_RATE - 2, null), new State(PLASMA_EXP + 5, PLASMA_EXP_RATE - 2, null), new State(PLASMA_EXP + 4, PLASMA_EXP_RATE - 2, null), new State(PLASMA_EXP + 3, PLASMA_EXP_RATE - 2, null), new State(PLASMA_EXP + 2, PLASMA_EXP_RATE - 2, null), new State(PLASMA_EXP + 1, PLASMA_EXP_RATE - 2, null), new State(PLASMA_EXP + 1, PLASMA_EXP_RATE - 2, null), new State(PLASMA_EXP + 1, PLASMA_EXP_RATE - 2, DoSuicide).setNext()};


    public static final State[] s_Mirv = {new State(FIREBALL, 12, DoMirv), new State(FIREBALL + 1, 12, DoMirv), new State(FIREBALL + 2, 12, DoMirv), new State(FIREBALL + 3, 12, DoMirv)};

    public static final State[] s_MirvMissile = {new State(FIREBALL, 12, DoMirvMissile), new State(FIREBALL + 1, 12, DoMirvMissile), new State(FIREBALL + 2, 12, DoMirvMissile), new State(FIREBALL + 3, 12, DoMirvMissile)};

    public static final int Vomit1 = 1719;
    public static final int Vomit2 = 1721;
    public static final int VomitSplash = 1711;
    public static final int Vomit_RATE = 16;

    public static final State[] s_Vomit1 = {new State(Vomit1, Vomit_RATE, DoVomit).setNext()};

    public static final State[] s_Vomit2 = {new State(Vomit2, Vomit_RATE, DoVomit).setNext()};

    public static final State[] s_VomitSplash = {new State(VomitSplash, Vomit_RATE, DoVomitSplash).setNext()};

    public static final int GORE_Head = 1670;
    public static final int GORE_Head_RATE = 16;

    public static final State[] s_GoreHead = {new State(GORE_Head, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 1, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 2, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 3, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 4, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 5, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 6, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 7, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 8, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 9, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 10, GORE_Head_RATE, DoShrapJumpFall), new State(GORE_Head + 11, GORE_Head_RATE, DoShrapJumpFall)};

    public static final int GORE_Leg = 1689;
    public static final int GORE_Leg_RATE = 16;

    public static final State[] s_GoreLeg = {new State(GORE_Leg, GORE_Leg_RATE, DoShrapJumpFall), new State(GORE_Leg + 1, GORE_Leg_RATE, DoShrapJumpFall), new State(GORE_Leg + 2, GORE_Leg_RATE, DoShrapJumpFall)};

    public static final int GORE_Eye = 1692;
    public static final int GORE_Eye_RATE = 16;

    public static final State[] s_GoreEye = {new State(GORE_Eye, GORE_Eye_RATE, DoShrapJumpFall), new State(GORE_Eye + 1, GORE_Eye_RATE, DoShrapJumpFall), new State(GORE_Eye + 2, GORE_Eye_RATE, DoShrapJumpFall), new State(GORE_Eye + 3, GORE_Eye_RATE, DoShrapJumpFall)};

    public static final int GORE_Torso = 1696;
    public static final int GORE_Torso_RATE = 16;

    public static final State[] s_GoreTorso = {new State(GORE_Torso, GORE_Torso_RATE, DoShrapJumpFall), new State(GORE_Torso + 1, GORE_Torso_RATE, DoShrapJumpFall), new State(GORE_Torso + 2, GORE_Torso_RATE, DoShrapJumpFall), new State(GORE_Torso + 3, GORE_Torso_RATE, DoShrapJumpFall), new State(GORE_Torso + 4, GORE_Torso_RATE, DoShrapJumpFall), new State(GORE_Torso + 5, GORE_Torso_RATE, DoShrapJumpFall), new State(GORE_Torso + 6, GORE_Torso_RATE, DoShrapJumpFall), new State(GORE_Torso + 7, GORE_Torso_RATE, DoShrapJumpFall)};

    public static final int GORE_Arm = 1550;
    public static final int GORE_Arm_RATE = 16;

    public static final State[] s_GoreArm = {new State(GORE_Arm, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 1, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 2, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 3, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 4, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 5, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 6, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 7, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 8, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 9, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 10, GORE_Arm_RATE, DoShrapJumpFall), new State(GORE_Arm + 11, GORE_Arm_RATE, DoShrapJumpFall)};

    public static final int GORE_Lung = 903;
    public static final int GORE_Lung_RATE = 16;

    public static final State[] s_GoreLung = {new State(GORE_Lung, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 1, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 2, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 3, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 4, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 5, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 6, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 7, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 8, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 9, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 10, GORE_Lung_RATE, DoShrapJumpFall), new State(GORE_Lung + 11, GORE_Lung_RATE, DoShrapJumpFall)};

    public static final int GORE_Liver = 918;
    public static final int GORE_Liver_RATE = 16;

    public static final State[] s_GoreLiver = {new State(GORE_Liver, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 1, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 2, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 3, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 4, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 5, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 6, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 7, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 8, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 9, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 10, GORE_Liver_RATE, DoShrapJumpFall), new State(GORE_Liver + 11, GORE_Liver_RATE, DoShrapJumpFall)};

    public static final int GORE_SkullCap = 933;
    public static final int GORE_SkullCap_RATE = 16;

    public static final State[] s_GoreSkullCap = {new State(GORE_SkullCap, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 1, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 2, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 3, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 4, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 5, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 6, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 7, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 8, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 9, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 10, GORE_SkullCap_RATE, DoShrapJumpFall), new State(GORE_SkullCap + 11, GORE_SkullCap_RATE, DoShrapJumpFall)};

    public static final int GORE_ChunkS = 2430;
    public static final int GORE_ChunkS_RATE = 16;

    public static final State[] s_GoreChunkS = {new State(GORE_ChunkS, GORE_ChunkS_RATE, DoShrapJumpFall), new State(GORE_ChunkS + 1, GORE_ChunkS_RATE, DoShrapJumpFall), new State(GORE_ChunkS + 2, GORE_ChunkS_RATE, DoShrapJumpFall), new State(GORE_ChunkS + 3, GORE_ChunkS_RATE, DoShrapJumpFall)};

    public static final int GORE_Drip = 1562; // 2430
    public static final int GORE_Drip_RATE = 16;

    public static final State[] s_GoreDrip = {new State(GORE_Drip, GORE_Drip_RATE, DoShrapJumpFall), new State(GORE_Drip + 1, GORE_Drip_RATE, DoShrapJumpFall), new State(GORE_Drip + 2, GORE_Drip_RATE, DoShrapJumpFall), new State(GORE_Drip + 3, GORE_Drip_RATE, DoShrapJumpFall)};

    public static final State[] s_FastGoreDrip = {new State(GORE_Drip, GORE_Drip_RATE, DoFastShrapJumpFall), new State(GORE_Drip + 1, GORE_Drip_RATE, DoFastShrapJumpFall), new State(GORE_Drip + 2, GORE_Drip_RATE, DoFastShrapJumpFall), new State(GORE_Drip + 3, GORE_Drip_RATE, DoFastShrapJumpFall)};

///////////////////////////////////////////////
//
//This GORE mostly for the Accursed Heads
//
///////////////////////////////////////////////

    public static final int GORE_Flame = 847;
    public static final int GORE_Flame_RATE = 8;

    public static final State[] s_GoreFlame = {new State(GORE_Flame, GORE_Flame_RATE, DoFastShrapJumpFall), new State(GORE_Flame + 1, GORE_Flame_RATE, DoFastShrapJumpFall), new State(GORE_Flame + 2, GORE_Flame_RATE, DoFastShrapJumpFall), new State(GORE_Flame + 3, GORE_Flame_RATE, DoFastShrapJumpFall), new State(GORE_Flame + 4, GORE_Flame_RATE, DoFastShrapJumpFall), new State(GORE_Flame + 5, GORE_Flame_RATE, DoFastShrapJumpFall), new State(GORE_Flame + 6, GORE_Flame_RATE, DoFastShrapJumpFall), new State(GORE_Flame + 7, GORE_Flame_RATE, DoFastShrapJumpFall)};


    public static final State[] s_TracerShrap = {new State(GORE_Flame, GORE_Flame_RATE, DoTracerShrap), new State(GORE_Flame + 1, GORE_Flame_RATE, DoTracerShrap), new State(GORE_Flame + 2, GORE_Flame_RATE, DoTracerShrap), new State(GORE_Flame + 3, GORE_Flame_RATE, DoTracerShrap), new State(GORE_Flame + 4, GORE_Flame_RATE, DoTracerShrap), new State(GORE_Flame + 5, GORE_Flame_RATE, DoTracerShrap), new State(GORE_Flame + 6, GORE_Flame_RATE, DoTracerShrap), new State(GORE_Flame + 7, GORE_Flame_RATE, DoTracerShrap)};

    public static final int UZI_SHELL = 2152;
    public static final int UZISHELL_RATE = 8;
    //public static final Animator DoShellShrap;
    public static final State[] s_UziShellShrap = {new State(UZI_SHELL, UZISHELL_RATE, DoShrapJumpFall), new State(UZI_SHELL + 1, UZISHELL_RATE, DoShrapJumpFall), new State(UZI_SHELL + 2, UZISHELL_RATE, DoShrapJumpFall), new State(UZI_SHELL + 3, UZISHELL_RATE, DoShrapJumpFall), new State(UZI_SHELL + 4, UZISHELL_RATE, DoShrapJumpFall), new State(UZI_SHELL + 5, UZISHELL_RATE, DoShrapJumpFall)};

    public static final State[] s_UziShellShrapStill1 = {new State(UZI_SHELL, UZISHELL_RATE, null).setNext()};
    public static final State[] s_UziShellShrapStill2 = {new State(UZI_SHELL + 1, UZISHELL_RATE, null).setNext()};
    public static final State[] s_UziShellShrapStill3 = {new State(UZI_SHELL + 2, UZISHELL_RATE, null).setNext()};
    public static final State[] s_UziShellShrapStill4 = {new State(UZI_SHELL + 3, UZISHELL_RATE, null).setNext()};
    public static final State[] s_UziShellShrapStill5 = {new State(UZI_SHELL + 4, UZISHELL_RATE, null).setNext()};
    public static final State[] s_UziShellShrapStill6 = {new State(UZI_SHELL + 5, UZISHELL_RATE, null).setNext()};

    public static final int SHOT_SHELL = 2180;
    public static final int SHOTSHELL_RATE = 8;
    public static final State[] s_ShotgunShellShrap = {new State(SHOT_SHELL, SHOTSHELL_RATE, DoShrapJumpFall), new State(SHOT_SHELL + 1, SHOTSHELL_RATE, DoShrapJumpFall), new State(SHOT_SHELL + 2, SHOTSHELL_RATE, DoShrapJumpFall), new State(SHOT_SHELL + 3, SHOTSHELL_RATE, DoShrapJumpFall), new State(SHOT_SHELL + 4, SHOTSHELL_RATE, DoShrapJumpFall), new State(SHOT_SHELL + 5, SHOTSHELL_RATE, DoShrapJumpFall), new State(SHOT_SHELL + 6, SHOTSHELL_RATE, DoShrapJumpFall), new State(SHOT_SHELL + 7, SHOTSHELL_RATE, DoShrapJumpFall)};

    public static final State[] s_ShotgunShellShrapStill1 = {new State(SHOT_SHELL + 1, SHOTSHELL_RATE, null).setNext()};
    public static final State[] s_ShotgunShellShrapStill2 = {new State(SHOT_SHELL + 3, SHOTSHELL_RATE, null).setNext()};
    public static final State[] s_ShotgunShellShrapStill3 = {new State(SHOT_SHELL + 7, SHOTSHELL_RATE, null).setNext()};

    public static final int GORE_FlameChunkA = 839;
    public static final int GORE_FlameChunkA_RATE = 8;

    public static final State[] s_GoreFlameChunkA = {new State(GORE_FlameChunkA, GORE_FlameChunkA_RATE, DoShrapJumpFall), new State(GORE_FlameChunkA + 1, GORE_FlameChunkA_RATE, DoShrapJumpFall), new State(GORE_FlameChunkA + 2, GORE_FlameChunkA_RATE, DoShrapJumpFall), new State(GORE_FlameChunkA + 3, GORE_FlameChunkA_RATE, DoShrapJumpFall)};

    public static final int GORE_FlameChunkB = 843;
    public static final int GORE_FlameChunkB_RATE = 8;

    public static final State[] s_GoreFlameChunkB = {new State(GORE_FlameChunkB, GORE_FlameChunkB_RATE, DoShrapJumpFall), new State(GORE_FlameChunkB + 1, GORE_FlameChunkB_RATE, DoShrapJumpFall), new State(GORE_FlameChunkB + 2, GORE_FlameChunkB_RATE, DoShrapJumpFall), new State(GORE_FlameChunkB + 3, GORE_FlameChunkB_RATE, DoShrapJumpFall)};

/////////////////////////////////////////////////////////////////////
//
//General Breaking Shrapnel
//
/////////////////////////////////////////////////////////////////////

    public static final int COIN_SHRAP = 2530;
    public static final int CoinShrap_RATE = 12;

    public static final State[] s_CoinShrap = {new State(COIN_SHRAP, CoinShrap_RATE, DoShrapJumpFall), new State(COIN_SHRAP + 1, CoinShrap_RATE, DoShrapJumpFall), new State(COIN_SHRAP + 2, CoinShrap_RATE, DoShrapJumpFall), new State(COIN_SHRAP + 3, CoinShrap_RATE, DoShrapJumpFall)};

    public static final int MARBEL = 5096;
    public static final int Marbel_RATE = 12;

    public static final State[] s_Marbel = {new State(MARBEL, Marbel_RATE, DoShrapJumpFall).setNext()};

//
//Glass
//

    public static final int GLASS_SHRAP_A = 3864;
    public static final int GlassShrapA_RATE = 12;

    public static final State[] s_GlassShrapA = {new State(GLASS_SHRAP_A, GlassShrapA_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_A + 1, GlassShrapA_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_A + 2, GlassShrapA_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_A + 3, GlassShrapA_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_A + 4, GlassShrapA_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_A + 5, GlassShrapA_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_A + 6, GlassShrapA_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_A + 7, GlassShrapA_RATE, DoShrapJumpFall)};

    public static final int GLASS_SHRAP_B = 3872;
    public static final int GlassShrapB_RATE = 12;

    public static final State[] s_GlassShrapB = {new State(GLASS_SHRAP_B, GlassShrapB_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_B + 1, GlassShrapB_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_B + 2, GlassShrapB_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_B + 3, GlassShrapB_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_B + 4, GlassShrapB_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_B + 5, GlassShrapB_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_B + 6, GlassShrapB_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_B + 7, GlassShrapB_RATE, DoShrapJumpFall)};

    public static final int GLASS_SHRAP_C = 3880;
    public static final int GlassShrapC_RATE = 12;

    public static final State[] s_GlassShrapC = {new State(GLASS_SHRAP_C, GlassShrapC_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_C + 1, GlassShrapC_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_C + 2, GlassShrapC_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_C + 3, GlassShrapC_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_C + 4, GlassShrapC_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_C + 5, GlassShrapC_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_C + 6, GlassShrapC_RATE, DoShrapJumpFall), new State(GLASS_SHRAP_C + 7, GlassShrapC_RATE, DoShrapJumpFall)};

//
//Wood
//

    public static final int WOOD_SHRAP_A = 3924;
    public static final int WoodShrapA_RATE = 12;

    public static final State[] s_WoodShrapA = {new State(WOOD_SHRAP_A, WoodShrapA_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_A + 1, WoodShrapA_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_A + 2, WoodShrapA_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_A + 3, WoodShrapA_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_A + 4, WoodShrapA_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_A + 5, WoodShrapA_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_A + 6, WoodShrapA_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_A + 7, WoodShrapA_RATE, DoShrapJumpFall)};

    public static final int WOOD_SHRAP_B = 3932;
    public static final int WoodShrapB_RATE = 12;

    public static final State[] s_WoodShrapB = {new State(WOOD_SHRAP_B, WoodShrapB_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_B + 1, WoodShrapB_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_B + 2, WoodShrapB_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_B + 3, WoodShrapB_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_B + 4, WoodShrapB_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_B + 5, WoodShrapB_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_B + 6, WoodShrapB_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_B + 7, WoodShrapB_RATE, DoShrapJumpFall)};

    public static final int WOOD_SHRAP_C = 3941;
    public static final int WoodShrapC_RATE = 12;

    public static final State[] s_WoodShrapC = {new State(WOOD_SHRAP_C, WoodShrapC_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_C + 1, WoodShrapC_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_C + 2, WoodShrapC_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_C + 3, WoodShrapC_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_C + 4, WoodShrapC_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_C + 5, WoodShrapC_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_C + 6, WoodShrapC_RATE, DoShrapJumpFall), new State(WOOD_SHRAP_C + 7, WoodShrapC_RATE, DoShrapJumpFall)};

//
//Stone
//

    public static final int STONE_SHRAP_A = 3840;
    public static final int StoneShrapA_RATE = 12;

    public static final State[] s_StoneShrapA = {new State(STONE_SHRAP_A, StoneShrapA_RATE, DoShrapJumpFall), new State(STONE_SHRAP_A + 1, StoneShrapA_RATE, DoShrapJumpFall), new State(STONE_SHRAP_A + 2, StoneShrapA_RATE, DoShrapJumpFall), new State(STONE_SHRAP_A + 3, StoneShrapA_RATE, DoShrapJumpFall), new State(STONE_SHRAP_A + 4, StoneShrapA_RATE, DoShrapJumpFall), new State(STONE_SHRAP_A + 5, StoneShrapA_RATE, DoShrapJumpFall), new State(STONE_SHRAP_A + 6, StoneShrapA_RATE, DoShrapJumpFall), new State(STONE_SHRAP_A + 7, StoneShrapA_RATE, DoShrapJumpFall)};

    public static final int STONE_SHRAP_B = 3848;
    public static final int StoneShrapB_RATE = 12;

    public static final State[] s_StoneShrapB = {new State(STONE_SHRAP_B, StoneShrapB_RATE, DoShrapJumpFall), new State(STONE_SHRAP_B + 1, StoneShrapB_RATE, DoShrapJumpFall), new State(STONE_SHRAP_B + 2, StoneShrapB_RATE, DoShrapJumpFall), new State(STONE_SHRAP_B + 3, StoneShrapB_RATE, DoShrapJumpFall), new State(STONE_SHRAP_B + 4, StoneShrapB_RATE, DoShrapJumpFall), new State(STONE_SHRAP_B + 5, StoneShrapB_RATE, DoShrapJumpFall), new State(STONE_SHRAP_B + 6, StoneShrapB_RATE, DoShrapJumpFall), new State(STONE_SHRAP_B + 7, StoneShrapB_RATE, DoShrapJumpFall)};

    public static final int STONE_SHRAP_C = 3856;
    public static final int StoneShrapC_RATE = 12;

    public static final State[] s_StoneShrapC = {new State(STONE_SHRAP_C, StoneShrapC_RATE, DoShrapJumpFall), new State(STONE_SHRAP_C + 1, StoneShrapC_RATE, DoShrapJumpFall), new State(STONE_SHRAP_C + 2, StoneShrapC_RATE, DoShrapJumpFall), new State(STONE_SHRAP_C + 3, StoneShrapC_RATE, DoShrapJumpFall), new State(STONE_SHRAP_C + 4, StoneShrapC_RATE, DoShrapJumpFall), new State(STONE_SHRAP_C + 5, StoneShrapC_RATE, DoShrapJumpFall), new State(STONE_SHRAP_C + 6, StoneShrapC_RATE, DoShrapJumpFall), new State(STONE_SHRAP_C + 7, StoneShrapC_RATE, DoShrapJumpFall)};

//
//Metal
//

    public static final int METAL_SHRAP_A = 3888;
    public static final int MetalShrapA_RATE = 12;

    public static final State[] s_MetalShrapA = {new State(METAL_SHRAP_A, MetalShrapA_RATE, DoShrapJumpFall), new State(METAL_SHRAP_A + 1, MetalShrapA_RATE, DoShrapJumpFall), new State(METAL_SHRAP_A + 2, MetalShrapA_RATE, DoShrapJumpFall), new State(METAL_SHRAP_A + 3, MetalShrapA_RATE, DoShrapJumpFall), new State(METAL_SHRAP_A + 4, MetalShrapA_RATE, DoShrapJumpFall), new State(METAL_SHRAP_A + 5, MetalShrapA_RATE, DoShrapJumpFall), new State(METAL_SHRAP_A + 6, MetalShrapA_RATE, DoShrapJumpFall), new State(METAL_SHRAP_A + 7, MetalShrapA_RATE, DoShrapJumpFall)};

    public static final int METAL_SHRAP_B = 3896;
    public static final int MetalShrapB_RATE = 12;

    public static final State[] s_MetalShrapB = {new State(METAL_SHRAP_B, MetalShrapB_RATE, DoShrapJumpFall), new State(METAL_SHRAP_B + 1, MetalShrapB_RATE, DoShrapJumpFall), new State(METAL_SHRAP_B + 2, MetalShrapB_RATE, DoShrapJumpFall), new State(METAL_SHRAP_B + 3, MetalShrapB_RATE, DoShrapJumpFall), new State(METAL_SHRAP_B + 4, MetalShrapB_RATE, DoShrapJumpFall), new State(METAL_SHRAP_B + 5, MetalShrapB_RATE, DoShrapJumpFall), new State(METAL_SHRAP_B + 6, MetalShrapB_RATE, DoShrapJumpFall), new State(METAL_SHRAP_B + 7, MetalShrapB_RATE, DoShrapJumpFall)};

    public static final int METAL_SHRAP_C = 3904;
    public static final int MetalShrapC_RATE = 12;

    public static final State[] s_MetalShrapC = {new State(METAL_SHRAP_C, MetalShrapC_RATE, DoShrapJumpFall), new State(METAL_SHRAP_C + 1, MetalShrapC_RATE, DoShrapJumpFall), new State(METAL_SHRAP_C + 2, MetalShrapC_RATE, DoShrapJumpFall), new State(METAL_SHRAP_C + 3, MetalShrapC_RATE, DoShrapJumpFall), new State(METAL_SHRAP_C + 4, MetalShrapC_RATE, DoShrapJumpFall), new State(METAL_SHRAP_C + 5, MetalShrapC_RATE, DoShrapJumpFall), new State(METAL_SHRAP_C + 6, MetalShrapC_RATE, DoShrapJumpFall), new State(METAL_SHRAP_C + 7, MetalShrapC_RATE, DoShrapJumpFall)};

//
//Paper
//

    public static final int PAPER_SHRAP_A = 3924;
    public static final int PaperShrapA_RATE = 12;

    public static final State[] s_PaperShrapA = {new State(PAPER_SHRAP_A, PaperShrapA_RATE, DoShrapJumpFall), new State(PAPER_SHRAP_A + 1, PaperShrapA_RATE, DoShrapJumpFall), new State(PAPER_SHRAP_A + 2, PaperShrapA_RATE, DoShrapJumpFall), new State(PAPER_SHRAP_A + 3, PaperShrapA_RATE, DoShrapJumpFall)};

    public static final int PAPER_SHRAP_B = 3932;
    public static final int PaperShrapB_RATE = 12;

    public static final State[] s_PaperShrapB = {new State(PAPER_SHRAP_B, PaperShrapB_RATE, DoShrapJumpFall), new State(PAPER_SHRAP_B + 1, PaperShrapB_RATE, DoShrapJumpFall), new State(PAPER_SHRAP_B + 2, PaperShrapB_RATE, DoShrapJumpFall), new State(PAPER_SHRAP_B + 3, PaperShrapB_RATE, DoShrapJumpFall)};

    public static final int PAPER_SHRAP_C = 3941;
    public static final int PaperShrapC_RATE = 12;

    public static final State[] s_PaperShrapC = {new State(PAPER_SHRAP_C, PaperShrapC_RATE, DoShrapJumpFall), new State(PAPER_SHRAP_C + 1, PaperShrapC_RATE, DoShrapJumpFall), new State(PAPER_SHRAP_C + 2, PaperShrapC_RATE, DoShrapJumpFall), new State(PAPER_SHRAP_C + 3, PaperShrapC_RATE, DoShrapJumpFall)};

    public enum WeaponStateGroup implements StateGroup {
        sg_RailPuff(s_RailPuff[0], s_RailPuff[1], s_RailPuff[2], s_RailPuff[1], s_RailPuff[0]), sg_CrossBolt(s_CrossBolt[0], s_CrossBolt[1], s_CrossBolt[2], s_CrossBolt[3], s_CrossBolt[4]), sg_Grenade(s_Grenade[0], s_Grenade[1], s_Grenade[2], s_Grenade[3], s_Grenade[4]), sg_Meteor(s_Meteor[0], s_Meteor[1], s_Meteor[2], s_Meteor[3], s_Meteor[4]), sg_MirvMeteor(s_MirvMeteor[0], s_MirvMeteor[1], s_MirvMeteor[2], s_MirvMeteor[3], s_MirvMeteor[4]), sg_SerpMeteor(s_SerpMeteor[0], s_SerpMeteor[1], s_SerpMeteor[2], s_SerpMeteor[3], s_SerpMeteor[4]), sg_Spear(s_Spear[0], s_Spear[1], s_Spear[2], s_Spear[3], s_Spear[4]), sg_Rocket(s_Rocket[0], s_Rocket[1], s_Rocket[2], s_Rocket[3], s_Rocket[4]), sg_BunnyRocket(s_BunnyRocket[0], s_BunnyRocket[1], s_BunnyRocket[2], s_BunnyRocket[3], s_BunnyRocket[4]), sg_Rail(s_Rail[0], s_Rail[1], s_Rail[2], s_Rail[3], s_Rail[4]), sg_Micro(s_Micro[0], s_Micro[1], s_Micro[2], s_Micro[3], s_Micro[4]), sg_MicroMini(s_MicroMini[0], s_MicroMini[1], s_MicroMini[2], s_MicroMini[3], s_MicroMini[4]), sg_BoltThinMan(s_BoltThinMan[0], s_BoltThinMan[1], s_BoltThinMan[2], s_BoltThinMan[3], s_BoltThinMan[4]), sg_BoltSeeker(s_BoltSeeker[0], s_BoltSeeker[1], s_BoltSeeker[2], s_BoltSeeker[3], s_BoltSeeker[4]);

        private final State[][] group;
        private int index = -1;

        WeaponStateGroup(State[]... states) {
            group = states;
        }

        @Override
        public State getState(int rotation, int offset) {
            return group[rotation][offset];
        }

        @Override
        public State getState(int rotation) {
            return group[rotation][0];
        }

        @Override
        public int index() {
            return index;
        }

        @Override
        public void setIndex(int index) {
            this.index = index;
        }
    }

    public static final int FOOTPRINT1 = 2490;
    public static final int FOOTPRINT2 = 2491;
    public static final int FOOTPRINT3 = 2492;
    public static final int FOOTPRINT_RATE = 30;

    public static final State[] s_FootPrint1 = {new State(FOOTPRINT1, FOOTPRINT_RATE, null).setNext()};
    public static final State[] s_FootPrint2 = {new State(FOOTPRINT2, FOOTPRINT_RATE, null).setNext()};
    public static final State[] s_FootPrint3 = {new State(FOOTPRINT3, FOOTPRINT_RATE, null).setNext()};

    public static final int WALLBLOOD1 = 2500;
    public static final int WALLBLOOD2 = 2501;
    public static final int WALLBLOOD3 = 2502;
    public static final int WALLBLOOD4 = 2503;
    public static final int WALLBLOOD_RATE = 30;


    public static final State[] s_WallBlood1 = {new State(WALLBLOOD1, SF_QUICK_CALL, DoWallBlood), new State(WALLBLOOD1, WALLBLOOD_RATE, null)};
    public static final State[] s_WallBlood2 = {new State(WALLBLOOD2, SF_QUICK_CALL, DoWallBlood), new State(WALLBLOOD2, WALLBLOOD_RATE, null)};
    public static final State[] s_WallBlood3 = {new State(WALLBLOOD3, SF_QUICK_CALL, DoWallBlood), new State(WALLBLOOD3, WALLBLOOD_RATE, null)};
    public static final State[] s_WallBlood4 = {new State(WALLBLOOD4, SF_QUICK_CALL, DoWallBlood), new State(WALLBLOOD4, WALLBLOOD_RATE, null)};

    public static final int FLOORBLOOD1 = 389;
    public static final int FLOORBLOOD_RATE = 30;


    public static final State[] s_FloorBlood1 = {new State(FLOORBLOOD1, SF_QUICK_CALL, DoFloorBlood), new State(FLOORBLOOD1, FLOORBLOOD_RATE, null)};

    public static void InitWeaponStates() {
        for (WeaponStateGroup sg : WeaponStateGroup.values()) {
            for (int rot = 0; rot < 5; rot++) {
                State.InitState(sg.group[rot]);
            }
        }

        State.InitState(s_BreakLight);
        State.InitState(s_BreakBarrel);
        State.InitState(s_BreakPedistal);
        State.InitState(s_BreakBottle1);
        State.InitState(s_BreakBottle2);
        State.InitState(s_Puff);
        State.InitState(s_LaserPuff);
        State.InitState(s_Tracer);
        State.InitState(s_EMP);
        State.InitState(s_EMPBurst);
        State.InitState(s_EMPShrap);
        State.InitState(s_TankShell);
        State.InitState(s_VehicleSmoke);
        State.InitState(s_WaterSmoke);
        State.InitState(s_UziSmoke);
        State.InitState(s_ShotgunSmoke);
        State.InitState(s_UziBullet);
        State.InitState(s_UziSpark);
        State.InitState(s_UziPowerSpark);
        State.InitState(s_Bubble);
        State.InitState(s_Splash);
        State.InitState(s_Star);
        State.InitState(s_StarDown);
        State.InitState(s_LavaBoulder);
        State.InitState(s_LavaShard);
        State.InitState(s_VulcanBoulder);
        State.InitState(s_Mine);
        State.InitState(s_MineSpark);
        State.InitState(s_MeteorExp);
        State.InitState(s_MirvMeteorExp);
        State.InitState(s_SerpMeteorExp);
        State.InitState(s_BoltFatMan);
        State.InitState(s_BoltShrapnel);
        State.InitState(s_CoolgFire);
        State.InitState(s_CoolgFireDone);
        State.InitState(s_GoreFloorSplash);
        State.InitState(s_GoreSplash);
        State.InitState(s_Plasma);
        State.InitState(s_PlasmaFountain);
        State.InitState(s_PlasmaDrip);
        State.InitState(s_PlasmaDone);
        State.InitState(s_TeleportEffect);
        State.InitState(s_TeleportEffect2);
        State.InitState(s_Electro);
        State.InitState(s_ElectroShrap);
        State.InitState(s_GrenadeSmallExp);
        State.InitState(s_GrenadeExp);
        State.InitState(s_MineExp);
        State.InitState(s_BasicExp);
        State.InitState(s_MicroExp);
        State.InitState(s_BigGunFlame);
        State.InitState(s_BoltExp);
        State.InitState(s_TankShellExp);
        State.InitState(s_TracerExp);
        State.InitState(s_SectorExp);
        State.InitState(s_FireballExp);
        State.InitState(s_NapExp);
        State.InitState(s_FireballFlames);
        State.InitState(s_BreakFlames);
        State.InitState(s_Fireball);
        State.InitState(s_Ring);
        State.InitState(s_Ring2);
        State.InitState(s_Napalm);
        State.InitState(s_BloodWorm);
        State.InitState(s_PlasmaExp);
        State.InitState(s_Mirv);
        State.InitState(s_MirvMissile);
        State.InitState(s_GoreHead);
        State.InitState(s_GoreLeg);
        State.InitState(s_GoreEye);
        State.InitState(s_GoreTorso);
        State.InitState(s_GoreArm);
        State.InitState(s_GoreLung);
        State.InitState(s_GoreLiver);
        State.InitState(s_GoreSkullCap);
        State.InitState(s_GoreChunkS);
        State.InitState(s_GoreDrip);
        State.InitState(s_FastGoreDrip);
        State.InitState(s_GoreFlame);
        State.InitState(s_TracerShrap);
        State.InitState(s_UziShellShrap);
        State.InitState(s_ShotgunShellShrap);
        State.InitState(s_GoreFlameChunkA);
        State.InitState(s_GoreFlameChunkB);
        State.InitState(s_CoinShrap);
        State.InitState(s_GlassShrapA);
        State.InitState(s_GlassShrapB);
        State.InitState(s_GlassShrapC);
        State.InitState(s_WoodShrapA);
        State.InitState(s_WoodShrapB);
        State.InitState(s_WoodShrapC);
        State.InitState(s_StoneShrapA);
        State.InitState(s_StoneShrapB);
        State.InitState(s_StoneShrapC);
        State.InitState(s_MetalShrapA);
        State.InitState(s_MetalShrapB);
        State.InitState(s_MetalShrapC);
        State.InitState(s_PaperShrapA);
        State.InitState(s_PaperShrapB);
        State.InitState(s_PaperShrapC);
        State.InitState(s_WallBlood1);
        State.InitState(s_WallBlood2);
        State.InitState(s_WallBlood3);
        State.InitState(s_WallBlood4);
        State.InitState(s_FloorBlood1);
    }

    public static boolean MissileHitMatch(int Weapon, int WeaponNum, int hitsprite) {
        Sprite hsp = boardService.getSprite(hitsprite);
        if (hsp == null) {
            return false;
        }

        if (WeaponNum <= -1) {
            USER wu = getUser(Weapon);
            if (wu == null) {
                return false;
            }
            WeaponNum = wu.WeaponNum;

            // can be hit by SO only
            if (SP_TAG7(hsp) == 4) {
                if (TEST(wu.Flags2, SPR2_SO_MISSILE)) {
                    DoMatchEverything(null, hsp.getHitag(), -1);
                    return (true);
                } else {
                    return (false);
                }
            }
        }

        if (SP_TAG7(hsp) == 0) {
            switch (WeaponNum) {
                case WPN_RAIL:
                case WPN_MICRO:
                case WPN_NAPALM:
                case WPN_ROCKET:
                    DoMatchEverything(null, hsp.getHitag(), -1);
                    return (true);
            }
        } else if (SP_TAG7(hsp) == 1) {
            switch (WeaponNum) {
                case WPN_MICRO:
                case WPN_RAIL:
                case WPN_HOTHEAD:
                case WPN_NAPALM:
                case WPN_ROCKET:
                    DoMatchEverything(null, hsp.getHitag(), -1);
                    return (true);
            }
        } else if (SP_TAG7(hsp) == 2) {
            switch (WeaponNum) {
                case WPN_MICRO:
                case WPN_RAIL:
                case WPN_HOTHEAD:
                case WPN_NAPALM:
                case WPN_ROCKET:
                case WPN_UZI:
                case WPN_SHOTGUN:
                    DoMatchEverything(null, hsp.getHitag(), -1);
                    return (true);
            }
        } else if (SP_TAG7(hsp) == 3) {
            switch (WeaponNum) {
                case WPN_STAR:
                case WPN_SWORD:
                case WPN_FIST:
                case WPN_MICRO:
                case WPN_RAIL:
                case WPN_HOTHEAD:
                case WPN_NAPALM:
                case WPN_ROCKET:
                case WPN_UZI:
                case WPN_SHOTGUN:
                    DoMatchEverything(null, hsp.getHitag(), -1);
                    return (true);
            }
        }

        return (false);
    }

    public static void SpawnShrapX(int SpriteNum) {
        // For shrap that has no Weapon to send over
        SpawnShrap(SpriteNum, -1);
    }


    public static void DoLavaErupt(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        int pnum;
        PlayerStr pp;
        boolean found = false;

        if (TEST_BOOL1(sp)) {
            for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                pp = Player[pnum];
                Sector plrSec = boardService.getSector(pp.cursectnum);
                if (plrSec != null && TEST(plrSec.getExtra(), SECTFX_TRIGGER)) {
                    ListNode<Sprite> nexti;
                    for (ListNode<Sprite> node = boardService.getSectNode(pp.cursectnum); node != null; node = nexti) {
                        nexti = node.getNext();
                        Sprite tsp = node.get();

                        if (tsp.getStatnum() == STAT_TRIGGER && SP_TAG7(tsp) == 0 && SP_TAG5(tsp) == 1) {
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        break;
                    }
                }
            }

            if (!found) {
                return;
            }
        }

        if (!TEST(u.Flags, SPR_ACTIVE)) {
            // inactive
            if ((u.WaitTics -= synctics) <= 0) {
                u.Flags |= (SPR_ACTIVE);
                u.Counter = 0;
                u.WaitTics =  (SP_TAG9(sp) * 120);
            }
        } else {
            // active
            if ((u.WaitTics -= synctics) <= 0) {
                // Stop for this long
                u.Flags &= ~(SPR_ACTIVE);
                u.Counter = 0;
                u.WaitTics =  (SP_TAG10(sp) * 120);
            }

            // Counter controls the volume of lava erupting
            // starts out slow and increases to a max
            u.Counter += synctics;
            if (u.Counter > SP_TAG2(sp)) {
                u.Counter =  SP_TAG2(sp);
            }

            if ((RANDOM_P2(1024 << 6) >> 6) < u.Counter) {
                switch (SP_TAG3(sp)) {
                    case 0:
                        SpawnShrapX(SpriteNum);
                        break;
                    case 1:
                        InitVulcanBoulder(SpriteNum);
                        break;
                }
            }
        }
    }

    public static void DoShrapMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u != null) {
            u.moveSpriteReturn = move_missile(SpriteNum, u.xchange, u.ychange, 0, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS * 2);
        }
    }

    public static void DoVomit(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.Counter = NORM_ANGLE(u.Counter + (30 * MISSILEMOVETICS));
        sp.setXrepeat( (u.sx + ((12 * EngineUtils.cos(u.Counter)) >> 14)));
        sp.setYrepeat( (u.sy + ((12 * EngineUtils.sin(u.Counter)) >> 14)));

        if (TEST(u.Flags, SPR_JUMPING)) {
            DoJump(SpriteNum);
            DoJump(SpriteNum);
            DoShrapMove(SpriteNum);
        } else if (TEST(u.Flags, SPR_FALLING)) {
            DoFall(SpriteNum);
            DoFall(SpriteNum);
            DoShrapMove(SpriteNum);
        } else {
            ChangeState(SpriteNum, s_VomitSplash[0]);
            DoFindGroundPoint(SpriteNum);
            MissileWaterAdjust(SpriteNum);
            sp.setZ(u.loz);
            u.WaitTics = 60;
            u.sx = sp.getXrepeat();
            u.sy = sp.getYrepeat();
            return;
        }

        if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_SPRITE) {
            int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
            Sprite hitSpr = boardService.getSprite(hitsprite);
            if (hitSpr != null && TEST(hitSpr.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                DoDamage(hitsprite, SpriteNum);
            }
        }
    }

    public static void DoVomitSplash(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u != null && (u.WaitTics -= MISSILEMOVETICS) < 0) {
            KillSprite(SpriteNum);
        }
    }

    public static void DoFastShrapJumpFall(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setX(sp.getX() + u.xchange * 2);
        sp.setY(sp.getY() + u.ychange * 2);
        sp.setZ(sp.getZ() + u.zchange * 2);

        u.WaitTics -= MISSILEMOVETICS;
        if (u.WaitTics <= 0) {
            KillSprite(SpriteNum);
        }
    }

    public static void DoTracerShrap(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setX(sp.getX() + u.xchange);
        sp.setY(sp.getY() + u.ychange);
        sp.setZ(sp.getZ() + u.zchange);

        u.WaitTics -= MISSILEMOVETICS;
        if (u.WaitTics <= 0) {
            KillSprite(SpriteNum);
        }
    }

    public static void DoShrapJumpFall(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING)) {
            DoShrapVelocity(SpriteNum);
        } else if (TEST(u.Flags, SPR_FALLING)) {
            DoShrapVelocity(SpriteNum);
        } else {
            if (!TEST(u.Flags, SPR_BOUNCE)) {
                DoShrapVelocity(SpriteNum);
                return;
            }

            if (u.ID == GORE_Drip) {
                ChangeState(SpriteNum, s_GoreFloorSplash[0]);
            } else {
                ShrapKillSprite(SpriteNum);
            }
        }
    }

    public static void DoShrapDamage(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING)) {
            DoJump(SpriteNum);
            DoShrapMove(SpriteNum);
        } else if (TEST(u.Flags, SPR_FALLING)) {
            DoFall(SpriteNum);
            DoShrapMove(SpriteNum);
        } else {
            if (!TEST(u.Flags, SPR_BOUNCE)) {
                u.Flags |= (SPR_BOUNCE);
                u.jump_speed = -300;
                sp.setXvel(sp.getXvel() >> 2);
                DoBeginJump(SpriteNum);
                return;
            }

            KillSprite(SpriteNum);
            return;
        }

        if (u.moveSpriteReturn != 0) {
            if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_SPRITE) {
                WeaponMoveHit(SpriteNum);
                KillSprite(SpriteNum);
            }
        }
    }

    public static void SpawnBlood(int SpriteNum, int Weapon, int hitang, int hitx, int hity, int hitz) {
        Sprite sp = boardService.getSprite(SpriteNum);
        Sprite np;
        USER nu;
        int i, newsp;

        int dang = 0;

        SHRAP p = UziBlood[0];
        int shrap_shade = -15;
        int shrap_xsize = 20, shrap_ysize = 20;
        int shrap_pal = PALETTE_DEFAULT;
        int start_ang = 0;

        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        switch (u.ID) {
            case TRASHCAN:
            case PACHINKO1:
            case PACHINKO2:
            case PACHINKO3:
            case PACHINKO4:
            case 623:
            case ZILLA_RUN_R0:
                return; // Don't put blood on trashcan
        }

        // second sprite involved
        // most of the time is the weapon
        if (Weapon >= 0) {
            Sprite wp = boardService.getSprite(Weapon);
            USER wu = getUser(Weapon);
            if (wp == null || wu == null) {
                return;
            }

            switch (wu.ID) {
                case NINJA_RUN_R0: // sword
                    // if sprite and weapon are the same it must be HARIII-KARIII
                    if (sp == wp) {
                        p = HariKariBlood[0];
                        hitang = sp.getAng();
                        hitx = sp.getX();
                        hity = sp.getY();
                        hitz = SPRITEp_TOS(wp) + DIV16(SPRITEp_SIZE_Z(wp));
                    } else {
                        p = ExtraBlood[0];
                        hitang = NORM_ANGLE(wp.getAng() + 1024);
                        hitx = sp.getX();
                        hity = sp.getY();
                        hitz = SPRITEp_TOS(wp) + DIV4(SPRITEp_SIZE_Z(wp));

                        // ASSERT(wu.PlayerP);
                    }
                    break;
                case SERP_RUN_R0:
                    p = ExtraBlood[0];
                    hitang = NORM_ANGLE(wp.getAng() + 1024);
                    hitx = sp.getX();
                    hity = sp.getY();
                    hitz = SPRITEp_TOS(sp) + DIV4(SPRITEp_SIZE_Z(sp));
                    break;
                case BLADE1:
                case BLADE2:
                case BLADE3:
                case 5011:
                    p = SmallBlood[0];
                    hitang = NORM_ANGLE(ANG2SPRITE(sp, wp) + 1024);
                    hitx = sp.getX();
                    hity = sp.getY();
                    hitz = wp.getZ() - DIV2(SPRITEp_SIZE_Z(wp));
                    break;
                case STAR1:
                case CROSSBOLT:
                    p = SomeBlood[0];
                    hitang = NORM_ANGLE(wp.getAng() + 1024);
                    hitx = sp.getX();
                    hity = sp.getY();
                    hitz = wp.getZ();
                    break;
                case PLASMA_FOUNTAIN:
                    p = PlasmaFountainBlood[0];
                    hitang = wp.getAng();
                    hitx = sp.getX();
                    hity = sp.getY();
                    hitz = SPRITEp_TOS(sp) + DIV4(SPRITEp_SIZE_Z(sp));
                    break;
                default:
                    p = SomeBlood[0];
                    hitang = NORM_ANGLE(wp.getAng() + 1024);
                    hitx = sp.getX();
                    hity = sp.getY();
                    hitz = SPRITEp_TOS(wp) + DIV4(SPRITEp_SIZE_Z(wp));
                    break;
            }
        } else {
            hitang = NORM_ANGLE(hitang + 1024);
        }

        if (p.state != null) {
            if (!p.random_disperse) {
                start_ang = NORM_ANGLE(hitang - DIV2(p.ang_range) + 1024);
                dang =  (p.ang_range / p.num);
            }

            for (i = 0; i < p.num; i++) {
                newsp =  SpawnSprite(STAT_SKIP4, p.id, p.state[0], sp.getSectnum(), hitx, hity, hitz, hitang, 0);
                np = boardService.getSprite(newsp);
                if (np == null) {
                    return;
                }

                nu = getUser(newsp);
                if (nu == null) {
                    return;
                }

                switch (nu.ID) {
                    case ELECTRO_SHARD:
                        shrap_xsize = 7;
                        shrap_ysize = 7;
                        break;

                    case PLASMA_Drip:
                        // Don't do central blood splats for every hitscan
                        if (RANDOM_P2(1024) < 950) {
                            np.setXrepeat(0);
                            np.setYrepeat(0);
                        }
                        if (RANDOM_P2(1024) < 512) {
                            np.setCstat(np.getCstat() | (CSTAT_SPRITE_XFLIP));
                        }
                        break;
                }

                if (p.random_disperse) {
                    np.setAng( (hitang + (RANDOM_P2(p.ang_range << 5) >> 5) - DIV2(p.ang_range)));
                    np.setAng(NORM_ANGLE(np.getAng()));
                } else {
                    np.setAng( (start_ang + (i * dang)));
                    np.setAng(NORM_ANGLE(np.getAng()));
                }

                nu.Flags |= (SPR_BOUNCE);

                np.setShade((byte) shrap_shade);
                np.setXrepeat(shrap_xsize);
                np.setYrepeat(shrap_ysize);
                np.setClipdist(16 >> 2);
                nu.spal = (byte) shrap_pal;
                np.setPal(shrap_pal);

                np.setXvel(p.min_vel);
                np.setXvel(np.getXvel() + RANDOM_RANGE(p.max_vel - p.min_vel));

                // special case
                // blood coming off of actors should have the acceleration of the actor
                // so add it in
                np.setXvel(np.getXvel() + sp.getXvel());

                nu.ceiling_dist = nu.floor_dist =  Z(2);
                nu.jump_speed = p.min_jspeed;
                nu.jump_speed += RANDOM_RANGE(p.max_jspeed - p.min_jspeed);
                nu.jump_speed =  -nu.jump_speed;

                // setsprite(new, np.x, np.y, np.z);
                nu.xchange = MOVEx(np.getXvel(), np.getAng());
                nu.ychange = MOVEy(np.getXvel(), np.getAng());

                // for FastShrap
                nu.zchange = klabs(nu.jump_speed * 4) - RANDOM_RANGE(klabs(nu.jump_speed) * 8);
                nu.WaitTics =  (64 + RANDOM_P2(32));

                u.Flags |= (SPR_BOUNCE);

                DoBeginJump(newsp);
            }
        }

    }

    public static void VehicleMoveHit(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (u.moveSpriteReturn == 0) {
            return;
        }

        Sector_Object sop = SectorObject[u.sop_parent];

        // sprite controlling sop
        int controller = sop.controller;

        switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
            case HIT_SECTOR: {
                return;
            }

            case HIT_SPRITE: {
                int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                Sprite hsp = boardService.getSprite(hitsprite);
                if (hsp == null) {
                    return;
                }

                if (TEST(hsp.getExtra(), SPRX_BREAKABLE)) {
                    HitBreakSprite(hitsprite, u.ID);
                    return;
                }

                if (TEST(hsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    Sprite controllerSpr = boardService.getSprite(controller);
                    if (controllerSpr != null && hitsprite != controllerSpr.getOwner()) {
                        DoDamage(hitsprite, controller);
                        return;
                    }
                } else {
                    if (hsp.getStatnum() == STAT_MINE_STUCK) {
                        DoDamage(hitsprite, SpriteNum);
                        return;
                    }
                }

                return;
            }

            case HIT_WALL: {
                int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                Wall wph = boardService.getWall(hitwall);
                if (wph != null && TEST(wph.getExtra(), WALLFX_SECTOR_OBJECT)) {
                    // sector object collision
                    Sector_Object hsop = DetectSectorObjectByWall(wph);
                    if (hsop != null) {
                        SopDamage(hsop, sop.ram_damage);
                        if (hsop.max_damage <= 0) {
                            SopCheckKill(hsop);
                        } else {
                            DoSpawnSpotsForDamage(hsop.match_event);
                        }

                    }
                }
            }
        }

    }

    public static boolean WeaponMoveHit(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        if (u.moveSpriteReturn == 0) {
            return (false);
        }

        switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
            case HIT_PLAX_WALL:
                SetSuicide(SpriteNum);
                return (true);

            case HIT_SECTOR: {
                int hitsect;
                Sector sectp;

                hitsect = NORM_HIT_INDEX(u.moveSpriteReturn);
                sectp = boardService.getSector(hitsect);
                if (sectp == null) {
                    break;
                }

                // hit floor - closer to floor than ceiling
                if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                    // hit a floor sprite
                    Sprite loSpr = boardService.getSprite(u.lo_sp);
                    if (loSpr != null) {
                        if (loSpr.getLotag() == TAG_SPRITE_HIT_MATCH) {
                            if (MissileHitMatch(SpriteNum, -1, u.lo_sp)) {
                                return (true);
                            }
                        }

                        return (true);
                    }

                    Sect_User hsu = getSectUser(hitsect);
                    if (hsu != null && hsu.depth > 0) {
                        SpawnSplash(SpriteNum);
                        // SetSuicide(SpriteNum);
                        return (true);
                    }

                }
                // hit ceiling
                else {
                    // hit a floor sprite
                    Sprite hiSpr = boardService.getSprite(u.hi_sp);
                    if (hiSpr != null) {
                        if (hiSpr.getLotag() == TAG_SPRITE_HIT_MATCH) {
                            if (MissileHitMatch(SpriteNum, -1, u.hi_sp)) {
                                return (true);
                            }
                        }
                    }
                }

                if (TEST(sectp.getExtra(), SECTFX_SECTOR_OBJECT)) {
                    int sopi = DetectSectorObject(sectp);
                    if (sopi != -1) {
                        DoDamage(SectorObject[sopi].sp_child, SpriteNum);
                        return (true);
                    }
                }

                if (TEST(sectp.getCeilingstat(), CEILING_STAT_PLAX) && sectp.getCeilingpicnum() != FAF_MIRROR_PIC) {
                    if (klabs(sp.getZ() - sectp.getCeilingz()) < SPRITEp_SIZE_Z(sp)) {
                        SetSuicide(SpriteNum);
                        return (true);
                    }
                }

                return (true);
            }

            case HIT_SPRITE: {
                int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                Sprite hsp = boardService.getSprite(hitsprite);
                USER hu = getUser(hitsprite);
                if (hsp == null) {
                    break;
                }

                if (TEST(hsp.getExtra(), SPRX_BREAKABLE)) {
                    HitBreakSprite(hitsprite, u.ID);
                    return (true);
                }

                if (TEST(hsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    // make sure you didn't hit the owner of the missile
                    // if (sp.owner != -1 && hitsprite != sp.owner)
                    if (hitsprite != sp.getOwner()) {
                        if (hu != null && u.ID == STAR1) {
                            switch (hu.ID) {
                                case TRASHCAN:
                                    PlaySound(DIGI_TRASHLID, sp, v3df_none);
                                    PlaySound(DIGI_STARCLINK, sp, v3df_none);
                                    if (hu.WaitTics <= 0) {
                                        hu.WaitTics =  SEC(2);
                                        ChangeState(hitsprite, s_TrashCanPain[0]);
                                    }
                                    break;
                                case PACHINKO1:
                                case PACHINKO2:
                                case PACHINKO3:
                                case PACHINKO4:
                                case ZILLA_RUN_R0:
                                case 623: {
                                    PlaySound(DIGI_STARCLINK, sp, v3df_none);
                                }
                                break;
                            }
                        }
                        DoDamage(hitsprite, SpriteNum);
                        return (true);
                    }
                } else {
                    if (hsp.getStatnum() == STAT_MINE_STUCK) {
                        DoDamage(hitsprite, SpriteNum);
                        return (true);
                    }
                }

                if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                    if (MissileHitMatch(SpriteNum, -1, hitsprite)) {
                        return (true);
                    }
                }

                if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                    if (hsp.getLotag() != 0 || hsp.getHitag() != 0) {
                        ShootableSwitch(hitsprite, SpriteNum);
                        return (true);
                    }
                }

                return (true);
            }

            case HIT_WALL: {
                Wall wph;
                Sector_Object sop;

                int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                wph = boardService.getWall(hitwall);
                if (wph == null) {
                    break;
                }

                if (TEST(wph.getExtra(), WALLFX_SECTOR_OBJECT)) {
                    if ((sop = DetectSectorObjectByWall(wph)) != null) {
                        if (sop.max_damage != -999) {
                            DoDamage(sop.sp_child, SpriteNum);
                        }
                        return (true);
                    }
                }

                if (wph.getLotag() == TAG_WALL_BREAK) {
                    HitBreakWall(hitwall, sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), u.ID);
                    u.moveSpriteReturn = 0;
                    return (true);
                }

                // clipmove does not correctly return the sprite for WALL sprites
                // on walls, so look with hitscan

                engine.hitscan(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), // Start position
                        EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)), // X vector of 3D ang
                        EngineUtils.sin(NORM_ANGLE(sp.getAng())), // Y vector of 3D ang
                        sp.getZvel(), // Z vector of 3D ang
                        pHitInfo, CLIPMASK_MISSILE);

                if (pHitInfo.hitsect == -1) {
                    return (false);
                }

                Sprite hsp = boardService.getSprite(pHitInfo.hitsprite);
                if (hsp != null) {
                    if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                        if (MissileHitMatch(SpriteNum, -1, pHitInfo.hitsprite)) {
                            return (true);
                        }
                    }

                    if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                        if (hsp.getLotag() != 0 || hsp.getHitag() != 0) {
                            ShootableSwitch(pHitInfo.hitsprite, SpriteNum);
                            return (true);
                        }
                    }
                }

                return (true);
            }
        }

        return (false);
    }

    public static void DoUziSmoke(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null) {
            sp.setZ(sp.getZ() - 200); // !JIM! Make them float up
        }
    }

    public static void DoShotgunSmoke(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null) {
            sp.setZ(sp.getZ() - 200); // !JIM! Make them float up
        }
    }

    public static void DoMineSpark(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null && sp.getPicnum() != 0) {
            DoDamageTest(SpriteNum);
        }
    }

    public static void DoFireballFlames(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum), ap;
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        boolean jumping = false;

        // if no owner then stay where you are
        ap = boardService.getSprite(u.Attach);
        if (ap != null) {
            sp.setX(ap.getX());
            sp.setY(ap.getY());

            sp.setZ(DIV2(SPRITEp_TOS(ap) + SPRITEp_BOS(ap)));

            if (TEST(ap.getExtra(), SPRX_BURNABLE)) {
                if (MOD2(u.Counter2) == 0) {
                    ap.setShade(ap.getShade() + 1);
                    if (ap.getShade() > 10) {
                        ap.setShade(10);
                    }
                }
            }
        } else {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoJump(SpriteNum);
                jumping = true;
            } else if (TEST(u.Flags, SPR_FALLING)) {
                DoFall(SpriteNum);
                jumping = true;
            } else {
                Sect_User su = getSectUser(sp.getSectnum());
                if (su != null && su.depth > 0) {
                    Sector sec = boardService.getSector(sp.getSectnum());
                    if (sec != null && klabs(sec.getFloorz() - sp.getZ()) <= Z(4)) {
                        KillSprite(SpriteNum);
                        return;
                    }
                }

                if (TestDontStickSector(sp.getSectnum())) {
                    KillSprite(SpriteNum);
                    return;
                }
            }
        }

        if (!jumping) {
            if ((u.WaitTics += MISSILEMOVETICS) > 4 * 120) {
                // shrink and go away
                sp.setXrepeat(sp.getXrepeat() - 1);
                sp.setYrepeat(sp.getYrepeat() - 1);

                if (((byte) sp.getXrepeat()) == 0) {
                    if (u.Attach != -1) {
                        USER au = getUser(u.Attach);
                        if (au != null) {
                            au.flame = -1;
                        }
                    }
                    KillSprite(SpriteNum);
                    return;
                }
            } else {
                // grow until the right size
                if (sp.getXrepeat() <= u.Counter) {
                    sp.setXrepeat(sp.getXrepeat() + 3);
                    sp.setYrepeat(sp.getYrepeat() + 3);
                }
            }
        }

        u.Counter2++;
        if (u.Counter2 > 9) {
            u.Counter2 = 0;
            DoFlamesDamageTest(SpriteNum);
        }
    }

    public static void DoBreakFlames(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        boolean jumping = false;

        if (TEST(u.Flags, SPR_JUMPING)) {
            DoJump(SpriteNum);
            jumping = true;
        } else if (TEST(u.Flags, SPR_FALLING)) {
            DoFall(SpriteNum);
            jumping = true;
        } else {
            Sect_User su = getSectUser(sp.getSectnum());
            if (su != null && su.depth > 0) {
                Sector sec = boardService.getSector(sp.getSectnum());
                if (sec != null && klabs(sec.getFloorz() - sp.getZ()) <= Z(4)) {
                    KillSprite(SpriteNum);
                    return;
                }
            }

            if (TestDontStickSector(sp.getSectnum())) {
                KillSprite(SpriteNum);
                return;
            }
        }

        if (!jumping) {
            if ((u.WaitTics += MISSILEMOVETICS) > 4 * 120) {
                // shrink and go away
                sp.setXrepeat(sp.getXrepeat() - 1);
                sp.setYrepeat(sp.getYrepeat() - 1);

                if (((byte) sp.getXrepeat()) == 0) {
                    if (u.Attach != -1) {
                        USER ua = getUser(u.Attach);
                        if (ua != null) {
                            ua.flame = -1;
                        }
                    }
                    KillSprite(SpriteNum);
                    return;
                }
            } else {
                // grow until the right size
                if (sp.getXrepeat() <= u.Counter) {
                    sp.setXrepeat(sp.getXrepeat() + 3);
                    sp.setYrepeat(sp.getYrepeat() + 3);
                }

                if (u.WaitTics + MISSILEMOVETICS > 4 * 120) {
                    SpawnBreakStaticFlames(SpriteNum);
                }
            }
        }

        u.Counter2++;
        if (u.Counter2 > 9) {
            u.Counter2 = 0;
            DoFlamesDamageTest(SpriteNum);
        }
    }

    public static void SetSuicide(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.Flags |= (SPR_SUICIDE);
        u.RotNum = 0;
        ChangeState(SpriteNum, s_Suicide[0]);
    }

    public static void DoRipperGrow(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.scale_speed = 70;
        u.scale_value = sp.getXrepeat() << 8;
        u.scale_tgt =  (sp.getXrepeat() + 20);

        if (u.scale_tgt > 128) {
            u.scale_speed = 0;
            u.scale_tgt = 128;
        }
    }

    public static void UpdateSinglePlayKills(int SpriteNum, PlayerStr pKiller) {
        // single play and coop kill count
        if (gNet.MultiGameType != MultiGameTypes.MULTI_GAME_COMMBAT) {
            USER u = getUser(SpriteNum);
            if (u == null || TEST(u.Flags, SPR_SUICIDE)) {
                return;
            }

            switch (u.ID) {
                case COOLIE_RUN_R0:
                case NINJA_RUN_R0:
                case NINJA_CRAWL_R0:
                case GORO_RUN_R0:
                case 1441:
                case COOLG_RUN_R0:
                case EEL_RUN_R0:
                case SUMO_RUN_R0:
                case ZILLA_RUN_R0:
                case RIPPER_RUN_R0:
                case RIPPER2_RUN_R0:
                case SERP_RUN_R0:
                case LAVA_RUN_R0:
                case SKEL_RUN_R0:
                case HORNET_RUN_R0:
                case GIRLNINJA_RUN_R0:
                case SKULL_R0:
                case BETTY_R0:
                    if (pKiller != null) {
                        pKiller.Kills++;
                    }
                    Kills++;
                    break;
            }
        }
    }

    private static PlayerStr getKiller(int Weapon) {
        Sprite wp = boardService.getSprite(Weapon);
        if (game.nNetMode == NetMode.Single) {
            return Player[myconnectindex];
        }

        int owner = (wp != null && wp.getOwner() != -1) ? wp.getOwner() : Weapon;
        for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            PlayerStr pp = Player[pnum];
            if (owner == pp.PlayerSprite) {
                return pp;
            }
        }

        return null;
    }

    public static void ActorChooseDeath(int SpriteNum, int Weapon) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);
        if (u.Health > 0) {
            return;
        }

        UpdateSinglePlayKills(SpriteNum, getKiller(Weapon));

        if (u.Attrib != null) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_die, v3df_follow);
        }

        switch (u.ID) {
            case PACHINKO1:
            case PACHINKO2:
            case PACHINKO3:
            case PACHINKO4:
            case 623:
            case TOILETGIRL_R0:
            case WASHGIRL_R0:
            case CARGIRL_R0:
            case MECHANICGIRL_R0:
            case SAILORGIRL_R0:
            case PRUNEGIRL_R0:
            case TRASHCAN:
            case GORO_RUN_R0:
            case RIPPER2_RUN_R0:
                break;
            case BUNNY_RUN_R0: {
                Bunny_Count--; // Bunny died, decrease the population
            }
            break;
            default:
                ActorCoughItem(SpriteNum);
                break;
        }

        switch (u.ID) {
            case BETTY_R0: {
                DoBettyBeginDeath(SpriteNum);
                break;
            }
            case SKULL_R0: {
                DoSkullBeginDeath(SpriteNum);
                break;
            }
            case TOILETGIRL_R0:
            case WASHGIRL_R0:
            case CARGIRL_R0:
            case MECHANICGIRL_R0:
            case SAILORGIRL_R0:
            case PRUNEGIRL_R0:
            case TRASHCAN:
            case PACHINKO1:
            case PACHINKO2:
            case PACHINKO3:
            case PACHINKO4:
            case 623: {
                if (wu != null) {
                    if (u.ID == TOILETGIRL_R0 || u.ID == CARGIRL_R0 || u.ID == MECHANICGIRL_R0 || u.ID == SAILORGIRL_R0 || u.ID == PRUNEGIRL_R0 || u.ID == WASHGIRL_R0 &&
                            wu.ID == NINJA_RUN_R0 && wu.PlayerP != -1) {
                        if (wu.PlayerP != -1 && !TEST(Player[wu.PlayerP].Flags, PF_DIVING)) {
                            Player[wu.PlayerP].Bloody = true;
                        }
                        PlaySound(DIGI_TOILETGIRLSCREAM, sp, v3df_none);
                    }
                }
                if (SpawnShrap(SpriteNum, Weapon)) {
                    SetSuicide(SpriteNum);
                }
                break;
            }

            // These are player zombies
            case ZOMBIE_RUN_R0:
                InitBloodSpray(SpriteNum, true, 105);
                InitBloodSpray(SpriteNum, true, 105);
                if (SpawnShrap(SpriteNum, Weapon)) {
                    SetSuicide(SpriteNum);
                }
                break;

            default:
                if (wu == null) {
                    break;
                }

                switch (wu.ID) {
                    case NINJA_RUN_R0: // sword
                        if (wu.PlayerP != -1) {
                            PlayerStr pp = Player[wu.PlayerP];

                            if (wu.WeaponNum == WPN_FIST && STD_RANDOM_RANGE(1000) > 500 && pp == Player[myconnectindex]) {
                                byte choosesnd = (byte) STD_RANDOM_RANGE(6);

                                if (choosesnd == 0) {
                                    PlayerSound(DIGI_KUNGFU, v3df_follow | v3df_dontpan, pp);
                                } else if (choosesnd == 1) {
                                    PlayerSound(DIGI_PAYINGATTENTION, v3df_follow | v3df_dontpan, pp);
                                } else if (choosesnd == 2) {
                                    PlayerSound(DIGI_EATTHIS, v3df_follow | v3df_dontpan, pp);
                                } else if (choosesnd == 3) {
                                    PlayerSound(DIGI_TAUNTAI4, v3df_follow | v3df_dontpan, pp);
                                } else if (choosesnd == 4) {
                                    PlayerSound(DIGI_TAUNTAI5, v3df_follow | v3df_dontpan, pp);
                                } else if (choosesnd == 5) {
                                    PlayerSound(DIGI_HOWYOULIKEMOVE, v3df_follow | v3df_dontpan, pp);
                                }
                            } else if (wu.WeaponNum == WPN_SWORD && STD_RANDOM_RANGE(1000) > 500 && pp == Player[myconnectindex]) {
                                int choose_snd;

                                choose_snd =  STD_RANDOM_RANGE(1000);
                                if (choose_snd > 750) {
                                    PlayerSound(DIGI_SWORDGOTU1, v3df_follow | v3df_dontpan, pp);
                                } else if (choose_snd > 575) {
                                    PlayerSound(DIGI_SWORDGOTU2, v3df_follow | v3df_dontpan, pp);
                                } else if (choose_snd > 250) {
                                    PlayerSound(DIGI_SWORDGOTU3, v3df_follow | v3df_dontpan, pp);
                                } else {
                                    PlayerSound(DIGI_CANBEONLYONE, v3df_follow | v3df_dontpan, pp);
                                }
                            }
                            if (!TEST(pp.Flags, PF_DIVING)) {
                                pp.Bloody = true;
                            }
                        }

                        if (u.WeaponNum == WPN_FIST) {
                            DoActorDie(SpriteNum, Weapon);
                        } else if (u.ID == NINJA_RUN_R0 || RANDOM_RANGE(1000) < 500) {
                            DoActorDie(SpriteNum, Weapon);
                        } else {
                            // Can't gib bosses!
                            if (u.ID == SERP_RUN_R0 || u.ID == SUMO_RUN_R0 || u.ID == ZILLA_RUN_R0) {
                                DoActorDie(SpriteNum, Weapon);
                                break;
                            }

                            // Gib out the ones you can't cut in half
                            // Blood fountains
                            InitBloodSpray(SpriteNum, true, -1);

                            if (SpawnShrap(SpriteNum, Weapon)) {
                                SetSuicide(SpriteNum);
                            } else {
                                DoActorDie(SpriteNum, Weapon);
                            }

                        }
                        break;

                    case BOLT_THINMAN_R0:
                    case BOLT_THINMAN_R1:
                    case BOLT_THINMAN_R2:
                    case NAP_EXP:
                    case BOLT_EXP:
                    case TANK_SHELL_EXP:
                    case SECTOR_EXP:
                    case FIREBALL_EXP:
                    case GRENADE_EXP:
                    case MINE_EXP:
                    case SKULL_R0:
                    case BETTY_R0: {
                        byte choosesnd;

                        // For the Nuke, do residual radiation if he gibs
                        if (wu.Radius == NUKE_RADIUS) {
                            SpawnFireballFlames(SpriteNum, -1);
                        }

                        // Random chance of taunting the AI's here
                        if (RANDOM_RANGE(1000) > 400) {
                            PlayerStr pp;

                            if (wp != null && wp.getOwner() >= 0) {
                                USER wou = getUser(wp.getOwner());
                                if (wou != null && wou.PlayerP != -1) {
                                    pp = Player[wou.PlayerP];
                                    choosesnd = (byte) (STD_RANDOM_RANGE(MAX_TAUNTAI << 8) >> 8);
                                    if (pp != null && pp == Player[myconnectindex]) {
                                        PlayerSound(TauntAIVocs[choosesnd], v3df_dontpan | v3df_follow, pp);
                                    }
                                }
                            }
                        }

                        // These guys cough items only if gibbed
                        if (u.ID == GORO_RUN_R0 || u.ID == RIPPER2_RUN_R0) {
                            ActorCoughItem(SpriteNum);
                        }

                        // Blood fountains
                        InitBloodSpray(SpriteNum, true, -1);

                        // Bosses do not gib
                        if (u.ID == SERP_RUN_R0 || u.ID == SUMO_RUN_R0 || u.ID == ZILLA_RUN_R0) {
                            DoActorDie(SpriteNum, Weapon);
                            return;
                        }

                        if (SpawnShrap(SpriteNum, Weapon)) {
                            SetSuicide(SpriteNum);
                        } else {
                            DoActorDie(SpriteNum, Weapon);
                        }

                    }

                    break;
                    default:
                        DoActorDie(SpriteNum, Weapon);
                        break;
                }

                break;
        }

    }

    public static void ActorHealth(int SpriteNum, int amt) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (u.ID == TRASHCAN && amt > -75) {
            u.LastDamage = 100;
            return;
        }

        u.Flags |= (SPR_ATTACKED);

        u.Health += amt;

        if (u.ID == SERP_RUN_R0 && sp.getPal() != 16 && Level == 4) {
            if (u.Health < u.MaxHealth / 2) {
                ExitLevel = true;
                FinishAnim = ANIM_SERP;
                FinishedLevel = true;
                return;
            }
        }

        if (u.ID == SUMO_RUN_R0 && Level == 11) {
            if (u.Health <= 0) {
                FinishTimer = 7 * 120;
                FinishAnim = ANIM_SUMO;
            }
        }

        if (u.ID == ZILLA_RUN_R0 && Level == 20) {
            if (u.Health <= 0)
            // if (u.Health < u.MaxHealth)
            {
                FinishTimer = 15 * 120;
                FinishAnim = ANIM_ZILLA;
            }
        }

        if (u.Attrib != null && RANDOM_P2(1024) > 850) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_pain, v3df_follow | v3df_dontpan);
        }

        // keep track of the last damage
        if (amt < 0) {
            u.LastDamage =  -amt;
        }

        // Do alternate Death2 if it exists
        if (u.ActorActionSet != null && u.ActorActionSet.Death2 != null) {
            int DEATH2_HEALTH_VALUE = 15;

            if (u.Health <= DEATH2_HEALTH_VALUE) {
                // If he's dead, possibly choose a special death type

                if (u.ID == NINJA_RUN_R0) {
                    if (TEST(u.Flags2, SPR2_DYING)) {
                        return;
                    }

                    if (TEST(u.Flags, SPR_FALLING | SPR_JUMPING | SPR_CLIMBING)) {
                        return;
                    }

                    int rnd = (RANDOM_P2(1024 << 4) >> 4);
                    if (rnd < 950) {
                        return;
                    }
                    u.Flags2 |= (SPR2_DYING); // Only let it check this once!
                    u.WaitTics = (SEC(1) + SEC(RANDOM_RANGE(2)));
                    u.Health = 60;
                    PlaySound(DIGI_NINJACHOKE, sp, v3df_follow);
                    InitPlasmaFountain(null, SpriteNum);
                    InitBloodSpray(SpriteNum, false, 105);
                    Sprite tsp = boardService.getSprite(u.tgt_sp);
                    if (tsp != null) {
                        sp.setAng(NORM_ANGLE(EngineUtils.getAngle(tsp.getX() - sp.getX(), tsp.getY() - sp.getY()) + 1024));
                        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YFLIP));
                    }
                    NewStateGroup(SpriteNum, Ninja.sg_NinjaGrabThroat);
                }
            }
        }

    }

///////////////////////////////////////////////
//
//This GORE mostly for the Accursed Heads
//
///////////////////////////////////////////////

    public static void SopDamage(Sector_Object sop, int amt) {
        USER u = getUser(sop.sp_child);
        if (u == null) {
            return;
        }


        // does not have damage
        if (sop.max_damage == -9999) {
            return;
        }

        sop.max_damage += (short) amt;

        // keep track of the last damage
        if (amt < 0) {
            u.LastDamage = -amt;
        }

    }

    public static void SopCheckKill(Sector_Object sop) {
        boolean killed;

        if (TEST(sop.flags, SOBJ_BROKEN)) {
            return;
        }

        // does not have damage
        if (sop.max_damage == -9999) {
            return;
        }

        if (sop.max_damage <= 0) {
            // returns whether so was collapsed(killed)
            killed = TestKillSectorObject(sop);
            if (!killed) {
                VehicleSetSmoke(sop, SpawnVehicleSmoke);
                sop.flags |= (SOBJ_BROKEN);
            }
        }

    }

    public static void ActorPain(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        // if (u.LastDamage < u.PainThreshold) // This doesn't work well at all because
        // of
        // uzi/shotgun damages
        switch (u.ID) {
            case TOILETGIRL_R0:
            case WASHGIRL_R0:
            case CARGIRL_R0:
            case MECHANICGIRL_R0:
            case SAILORGIRL_R0:
            case PRUNEGIRL_R0:
                u.FlagOwner = 1;
                break;
        }

        if (RANDOM_RANGE(1000) < 875 || u.WaitTics > 0) {
            return;
        }

        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (u.ActorActionSet != null && u.ActorActionSet.Pain != null) {
                ActorLeaveTrack(SpriteNum);
                u.WaitTics = 60;
                NewStateGroup(SpriteNum, u.ActorActionSet.Pain);
            }
        }

    }

    public static void ActorPainPlasma(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING | SPR_ELECTRO_TOLERANT)) {
            if (u.ActorActionSet != null && u.ActorActionSet.Pain != null) {
                u.WaitTics = PLASMA_FOUNTAIN_TIME;
                NewStateGroup(SpriteNum, u.ActorActionSet.Pain);
            } else {
                u.Vis = PLASMA_FOUNTAIN_TIME;
                InitActorPause(SpriteNum);
            }
        }

    }

    public static void ActorStdMissile(int SpriteNum, int Weapon) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);

        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);
        if (sp == null || u == null || wu == null || wp == null) {
            return;
        }

        // Attack the player that is attacking you
        // Only if hes still alive
        if (wp.getOwner() >= 0) {
            USER wou = getUser(wp.getOwner());
            // Commented out line will make actors attack each other, it's been removed as
            // feature
            if (wou != null && wou.PlayerP != -1 && sp.getOwner() != wp.getOwner()) {
                u.tgt_sp = wp.getOwner();
            }
        }

        // Reset the weapons target before dying
        if (wu.WpnGoal >= 0) {
            USER wou = getUser(wu.WpnGoal);
            // attempt to see if it was killed
            // what if the target has been killed? This will crash!

            if (wou != null) {
                wou.Flags &= ~(SPR_TARGETED);
            }
        }
    }

    public static void ActorDamageSlide(int SpriteNum, int damage, int ang) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        int slide_vel, slide_dec;

        if (TEST(u.Flags, SPR_CLIMBING)) {
            return;
        }

        damage = klabs(damage);

        if (damage == 0) {
            return;
        }

        if (damage <= 10) {
            DoActorBeginSlide(SpriteNum, ang, 64, 5);
        } else if (damage <= 20) {
            DoActorBeginSlide(SpriteNum, ang, 128, 5);
        } else {
            slide_vel = (damage * 6) - (u.MaxHealth);

            if (slide_vel < -1000) {
                slide_vel = -1000;
            }
            slide_dec = 5;

            DoActorBeginSlide(SpriteNum, ang, slide_vel, slide_dec);

        }
    }

    public static void PlayerDamageSlide(PlayerStr pp, int damage, int ang) {
        int slide_vel;

        damage = klabs(damage);

        if (damage == 0) {
            return;
        }

        if (damage > 5) {
            if (damage <= 10) {
                // nudge
                pp.slide_xvect = MOVEx(16, ang) << 15;
                pp.slide_yvect = MOVEy(16, ang) << 15;
            } else if (damage <= 20) {
                // bigger nudge
                pp.slide_xvect = MOVEx(64, ang) << 15;
                pp.slide_yvect = MOVEy(64, ang) << 15;
            } else {
                slide_vel = (damage * 6);

                pp.slide_xvect = MOVEx(slide_vel, ang) << 15;
                pp.slide_yvect = MOVEy(slide_vel, ang) << 15;
            }
        }
    }

    public static int GetDamage(int SpriteNum, int Weapon, int DamageNdx) {
        DAMAGE_DATA d = DamageData[DamageNdx];

        // if ndx does radius
        if (d.radius > 0) {
            Sprite sp = boardService.getSprite(SpriteNum);
            Sprite wp = boardService.getSprite(Weapon);
            if (sp == null || wp == null) {
                return 0;
            }

            int damage_per_pixel, damage_force, damage_amt;
            int dist = DISTANCE(wp.getX(), wp.getY(), sp.getX(), sp.getY());

            // take off the box around the player or else you'll never get
            // the max_damage;
            dist -= (sp.getClipdist()) << (2);

            if (dist < 0) {
                dist = 0;
            }

            if (dist < d.radius) {
                damage_per_pixel = (d.damage_hi << 16) / d.radius;

                // the closer your distance is to 0 the more damage
                damage_force = (d.radius - dist);
                damage_amt = -((damage_force * damage_per_pixel) >> 16);

                // formula: damage_amt = 75% + random(25%)
                return (DIV2(damage_amt) + DIV4(damage_amt) + RANDOM_RANGE(DIV4(damage_amt)));
            } else {
                return (0);
            }
        }

        return (-(d.damage_lo + RANDOM_RANGE(d.damage_hi - d.damage_lo)));
    }

    public static boolean PlayerCheckDeath(PlayerStr pp, int Weapon) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (sp == null || u == null) {
            return false;
        }

        // Store off what player was struck by
        pp.HitBy =  Weapon;

        if (u.Health <= 0 && !TEST(pp.Flags, PF_DEAD)) {

            // pick a death type
            if (u.LastDamage >= PLAYER_DEATH_EXPLODE_DAMMAGE_AMT) {
                pp.DeathType = PLAYER_DEATH_EXPLODE;
            } else if (u.LastDamage >= PLAYER_DEATH_CRUMBLE_DAMMAGE_AMT) {
                pp.DeathType = PLAYER_DEATH_CRUMBLE;
            } else {
                pp.DeathType = PLAYER_DEATH_FLIP;
            }

            if (Weapon < 0) {
                pp.Killer = -1;
                DoPlayerBeginDie(pp.pnum);
                return (true);
            }

            Sprite wp = boardService.getSprite(Weapon);
            USER wu = getUser(Weapon);
            if (wu == null || wp == null) {
                return false;
            }

            if (wu.ID == RIPPER_RUN_R0 || wu.ID == RIPPER2_RUN_R0) {
                pp.DeathType = PLAYER_DEATH_RIPPER;
            }

            if (wu.ID == CALTROPS) {
                pp.DeathType = PLAYER_DEATH_FLIP;
            }

            if (wu.ID == NINJA_RUN_R0 && wu.PlayerP != -1) {
                pp.DeathType = PLAYER_DEATH_FLIP;
                Player[wu.PlayerP].Bloody = true;
            }

            // keep track of who killed you for death purposes
            // need to check all Killer variables when an enemy dies
            if (pp.Killer < 0) {
                if (wp.getOwner() >= 0) {
                    pp.Killer = wp.getOwner();
                } else if (TEST(wp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    pp.Killer =  Weapon;
                }
            }

            // start the death process
            DoPlayerBeginDie(pp.pnum);

            // for death direction
            // u.slide_ang = wp.ang;
            u.slide_ang =  EngineUtils.getAngle(sp.getX() - wp.getX(), sp.getY() - wp.getY());
            // for death velocity
            u.slide_vel = u.LastDamage * 5;

            return (true);
        }

        return (false);
    }

    public static boolean PlayerTakeDamage(PlayerStr pp, int Weapon) {
        int pnum = pp.pnum;
        USER u = getUser(pp.PlayerSprite);

        if (u == null || Weapon < 0) {
            return (true);
        }

        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);
        if (wu == null || wp == null) {
            return false;
        }

        if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_NONE) {
            // ZOMBIE special case for single play
            if (wu.ID == ZOMBIE_RUN_R0) {
                // if weapons owner the player
                return wp.getOwner() != pp.PlayerSprite;
            }

            return (true);
        }

        USER wou = getUser(wp.getOwner());
        if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COOPERATIVE) {
            // everything hurts you
            if (gNet.HurtTeammate) {
                return (true);
            }

            // if weapon IS the YOURSELF take damage
            if (wu.PlayerP == pnum) {
                return (true);
            }

            // if the weapons owner is YOURSELF take damage
            if (wp.getOwner() >= 0) {
                if (wou != null && wou.PlayerP != -1 && wou.PlayerP == pnum) {
                    return (true);
                }
            }

            // if weapon IS the player no damage
            if (wu.PlayerP != -1) {
                return (false);
            }

            // if the weapons owner is a player
            return wp.getOwner() < 0 || wou == null || wou.PlayerP == -1;
        } else if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT && gNet.TeamPlay) {
            // everything hurts you
            if (gNet.HurtTeammate) {
                return (true);
            }

            // if weapon IS the YOURSELF take damage
            if (wu.PlayerP == pnum) {
                return (true);
            }

            // if the weapons owner is YOURSELF take damage
            if (wp.getOwner() >= 0 && wou != null && wou.PlayerP != -1 && wou.PlayerP == pnum) {
                return (true);
            }

            if (wu.PlayerP != -1) {
                // if both on the same team then no damage
                if (wu.spal == u.spal) {
                    return (false);
                }
            }

            // if the weapons owner is a player
            if (wp.getOwner() >= 0 && wou != null && wou.PlayerP != -1) {
                // if both on the same team then no damage
                return wou.spal != u.spal;
            }
        }

        return (true);
    }

    public static void StarBlood(int SpriteNum, int Weapon) {
        USER u = getUser(SpriteNum);
        int blood_num = 1;
        int i;

        if (u != null && u.Health <= 0) {
            blood_num = 4;
        }

        for (i = 0; i < blood_num; i++) {
            SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);
        }
    }

    public static int ANG2PLAYER(PlayerStr pp, Sprite sp) {
        return (EngineUtils.getAngle((pp).posx - (sp).getX(), (pp).posy - (sp).getY()));
    }

    public static int ANG2SPRITE(Sprite sp, Sprite op) {
        return (EngineUtils.getAngle((sp).getX() - (op).getX(), (sp).getY() - (op).getY()));
    }

    /*
     *
     * !AIC KEY - This is where damage is assesed when missiles hit actors and other
     * objects.
     *
     */
    public static void DoDamage(int SpriteNum, int Weapon) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int damage = 0;

        // don't hit a dead player
        if (u.PlayerP != -1 && TEST(Player[u.PlayerP].Flags, PF_DEAD)) {
            SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);
            return;
        }

        if (TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);
        if (wp == null || wu == null) {
            return;
        }

        if (TEST(wu.Flags, SPR_SUICIDE)) {
            return;
        }

        if (u.Attrib != null && RANDOM_P2(1024) > 850) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_pain, v3df_follow);
        }

        if (TEST(u.Flags, SPR_DEAD)) {
            SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);
            return;
        }

        // special case for shooting mines
        if (sp.getStatnum() == STAT_MINE_STUCK) {
            SpawnMineExp(SpriteNum);
            KillSprite(SpriteNum);
            return;
        }

        // weapon is drivable object manned by player
        if (wu.PlayerP != -1 && Player[wu.PlayerP].sop != -1) {
            switch (SectorObject[Player[wu.PlayerP].sop].track) {
                case SO_TANK:
                    damage = -200;

                    if (u.sop_parent != -1) {
                        break;
                    } else if (u.PlayerP != -1) {
                        PlayerStr PlayerPP = Player[u.PlayerP];
                        PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                        if (PlayerTakeDamage(PlayerPP, Weapon)) {
                            PlayerUpdateHealth(PlayerPP, damage);
                            PlayerCheckDeath(PlayerPP, Weapon);
                        }
                    } else {
                        PlayerStr pp = Player[screenpeek];

                        ActorHealth(SpriteNum, damage);
                        if (u.Health <= 0) {
                            // Random chance of taunting the AI's here
                            if (STD_RANDOM_RANGE(1024) > 512 && pp == Player[myconnectindex]) {
                                byte choosesnd = (byte) RANDOM_RANGE(MAX_TAUNTAI);
                                PlayerSound(TauntAIVocs[choosesnd], v3df_dontpan | v3df_follow, pp);
                            }
                            SpawnShrap(SpriteNum, Weapon);
                            SetSuicide(SpriteNum);
                            return;
                        }
                    }
                    break;

                case SO_SPEED_BOAT:
                    damage = -100;

                    if (u.sop_parent != -1) {
                        break;
                    } else if (u.PlayerP != -1) {
                        PlayerStr PlayerPP = Player[u.PlayerP];
                        PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                        if (PlayerTakeDamage(PlayerPP, Weapon)) {
                            PlayerUpdateHealth(PlayerPP, damage);
                            PlayerCheckDeath(PlayerPP, Weapon);
                        }
                    } else {
                        ActorHealth(SpriteNum, damage);
                        ActorPain(SpriteNum);
                        ActorChooseDeath(SpriteNum, Weapon);
                    }

                    SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);
                    break;
            }
        }

        // weapon is the actor - no missile used - example swords, axes, etc
        switch (wu.ID) {
            case NINJA_RUN_R0:
                if (wu.WeaponNum == WPN_SWORD) {
                    damage = GetDamage(SpriteNum, Weapon, WPN_SWORD);
                } else {
                    damage = GetDamage(SpriteNum, Weapon, WPN_FIST);
                    // Add damage for stronger attacks!
                    switch (Player[wu.PlayerP].WpnKungFuMove) {
                        case 1:
                            damage -= 2 - RANDOM_RANGE(7);
                            break;
                        case 2:
                            damage -= 5 - RANDOM_RANGE(10);
                            break;
                    }
                    PlaySound(DIGI_CGTHIGHBONE, wp, v3df_follow | v3df_dontpan);
                }

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    // Is the player blocking?
                    if (PlayerPP.WpnKungFuMove == 3) {
                        damage /= 3;
                    }
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                    if (PlayerPP.Armor != 0) {
                        PlaySound(DIGI_ARMORHIT, PlayerPP, v3df_dontpan | v3df_follow | v3df_doppler);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                break;

            case SKEL_RUN_R0:
            case COOLG_RUN_R0:
            case GORO_RUN_R0:

                damage = GetDamage(SpriteNum, Weapon, DMG_SKEL_SLASH);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    // Is the player blocking?
                    if (PlayerPP.WpnKungFuMove == 3) {
                        damage /= 3;
                    }
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                break;

            case HORNET_RUN_R0:
                PlaySound(DIGI_HORNETSTING, sp, v3df_follow | v3df_dontpan);
                damage = GetDamage(SpriteNum, Weapon, DMG_HORNET_STING);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    // Is the player blocking?
                    if (PlayerPP.WpnKungFuMove == 3) {
                        damage /= 3;
                    }
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                // SpawnBlood(SpriteNum, Weapon,0, 0, 0, 0);

                break;

            case EEL_RUN_R0:
                damage = GetDamage(SpriteNum, Weapon, DMG_RIPPER_SLASH);
                damage /= 3;
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    // Is the player blocking?
                    if (PlayerPP.WpnKungFuMove == 3) {
                        damage /= 3;
                    }
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                break;

            case RIPPER_RUN_R0:
                damage = GetDamage(SpriteNum, Weapon, DMG_RIPPER_SLASH);
                damage /= 3; // Little rippers aren't as tough.
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    // Is the player blocking?
                    if (PlayerPP.WpnKungFuMove == 3) {
                        damage /= 3;
                    }
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        if (PlayerCheckDeath(PlayerPP, Weapon)) {
                            PlaySound(DIGI_RIPPERHEARTOUT, PlayerPP, v3df_dontpan | v3df_doppler);

                            DoRipperRipHeart(Weapon);
                        }
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                break;

            case RIPPER2_RUN_R0:
                damage = GetDamage(SpriteNum, Weapon, DMG_RIPPER_SLASH);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    // Is the player blocking?
                    if (PlayerPP.WpnKungFuMove == 3) {
                        damage /= 3;
                    }
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        if (PlayerCheckDeath(PlayerPP, Weapon)) {
                            PlaySound(DIGI_RIPPERHEARTOUT, PlayerPP, v3df_dontpan | v3df_doppler);

                            DoRipper2RipHeart(Weapon);
                        }
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                break;

            case BUNNY_RUN_R0:
                damage = GetDamage(SpriteNum, Weapon, DMG_RIPPER_SLASH);
                damage /= 3;
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    // Is the player blocking?
                    if (PlayerPP.WpnKungFuMove == 3) {
                        damage /= 3;
                    }
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        if (PlayerCheckDeath(PlayerPP, Weapon)) {
                            DoBunnyRipHeart(Weapon);
                        }
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                break;

            case SERP_RUN_R0:
                damage = GetDamage(SpriteNum, Weapon, DMG_SERP_SLASH);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    if (PlayerPP.WpnKungFuMove == 3) {
                        damage /= 3;
                    }
                    PlayerDamageSlide(PlayerPP, damage / 4, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);
                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);
                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                break;

            case BLADE1:
            case BLADE2:
            case BLADE3:
            case 5011:

                if (wu.ID == 5011) {
                    damage = -(3 + (RANDOM_RANGE(4 << 8) >> 8));
                } else {
                    damage = GetDamage(SpriteNum, Weapon, DMG_BLADE);
                }
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    if (PlayerPP.WpnKungFuMove == 3) {
                        damage /= 3;
                    }
                    if ((u.BladeDamageTics -= synctics) < 0) {
                        u.BladeDamageTics = DAMAGE_BLADE_TIME;
                        PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                        if (PlayerTakeDamage(PlayerPP, Weapon)) {
                            PlayerUpdateHealth(PlayerPP, damage);
                            PlayerCheckDeath(PlayerPP, Weapon);
                        }
                    }
                } else {
                    if ((u.BladeDamageTics -= ACTORMOVETICS) < 0) {
                        u.BladeDamageTics = DAMAGE_BLADE_TIME;
                        ActorHealth(SpriteNum, damage);
                    }

                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                break;

            case STAR1:
            case CROSSBOLT:
                damage = GetDamage(SpriteNum, Weapon, WPN_STAR);

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    // Is the player blocking?
                    if (PlayerPP.WpnKungFuMove == 3) {
                        damage /= 3;
                    }
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                    if (PlayerPP.Armor != 0) {
                        PlaySound(DIGI_ARMORHIT, PlayerPP, v3df_dontpan | v3df_follow | v3df_doppler);
                    }
                } else {

                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                    ActorDamageSlide(SpriteNum, damage, wp.getAng());
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                StarBlood(SpriteNum, Weapon);

                wu.ID = 0;
                SetSuicide(Weapon);
                break;

            case SPEAR_R0:
                damage = GetDamage(SpriteNum, Weapon, DMG_SPEAR_TRAP);

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                    if (PlayerPP.Armor != 0) {
                        PlaySound(DIGI_ARMORHIT, PlayerPP, v3df_dontpan | v3df_follow | v3df_doppler);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                    ActorDamageSlide(SpriteNum, damage, wp.getAng());
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                wu.ID = 0;
                SetSuicide(Weapon);
                break;

            case LAVA_BOULDER:
                damage = GetDamage(SpriteNum, Weapon, DMG_LAVA_BOULDER);

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                    ActorDamageSlide(SpriteNum, damage, wp.getAng());
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                wu.ID = 0;
                SetSuicide(Weapon);
                break;

            case LAVA_SHARD:
                damage = GetDamage(SpriteNum, Weapon, DMG_LAVA_SHARD);

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                    ActorDamageSlide(SpriteNum, damage, wp.getAng());
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBlood(SpriteNum, Weapon, 0, 0, 0, 0);

                wu.ID = 0;
                SetSuicide(Weapon);
                break;

            case UZI_SMOKE:
            case UZI_SMOKE + 2:
                if (wu.ID == UZI_SMOKE) {
                    damage = GetDamage(SpriteNum, Weapon, WPN_UZI);
                } else {
                    damage = GetDamage(SpriteNum, Weapon, WPN_UZI) / 3; // Enemy Uzi, 1/3 damage
                }

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    // PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    PlayerDamageSlide(PlayerPP, damage / 2, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                    if (PlayerPP.Armor != 0) {
                        PlaySound(DIGI_ARMORHIT, PlayerPP, v3df_dontpan | v3df_follow | v3df_doppler);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                    ActorDamageSlide(SpriteNum, damage, wp.getAng());
                    ActorChooseDeath(SpriteNum, Weapon);
                    switch (u.ID) {
                        case TRASHCAN:
                        case PACHINKO1:
                        case PACHINKO2:
                        case PACHINKO3:
                        case PACHINKO4:
                        case 623:
                        case ZILLA_RUN_R0:
                            break;
                        default:
                            if (RANDOM_RANGE(1000) > 900) {
                                InitBloodSpray(SpriteNum, false, 105);
                            }
                            if (RANDOM_RANGE(1000) > 900) {
                                SpawnMidSplash(SpriteNum);
                            }
                            break;
                    }
                }

                // SpawnBlood(SpriteNum, Weapon,0, 0, 0, 0);
                // reset id so no more damage is taken
                wu.ID = 0;
                break;

            case SHOTGUN_SMOKE:
                damage = GetDamage(SpriteNum, Weapon, WPN_SHOTGUN);

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                    if (PlayerPP.Armor != 0) {
                        PlaySound(DIGI_ARMORHIT, PlayerPP, v3df_dontpan | v3df_follow | v3df_doppler);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                    ActorDamageSlide(SpriteNum, damage, wp.getAng());
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                // SpawnBlood(SpriteNum, Weapon,0, 0, 0, 0);
                switch (u.ID) {
                    case TRASHCAN:
                    case PACHINKO1:
                    case PACHINKO2:
                    case PACHINKO3:
                    case PACHINKO4:
                    case 623:
                    case ZILLA_RUN_R0:
                        break;
                    default:
                        if (RANDOM_RANGE(1000) > 950) {
                            SpawnMidSplash(SpriteNum);
                        }
                        break;
                }

                // reset id so no more damage is taken
                wu.ID = 0;
                break;

            case MIRV_METEOR:

                // damage = -DAMAGE_MIRV_METEOR;
                damage = GetDamage(SpriteNum, Weapon, DMG_MIRV_METEOR);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                }

                SetSuicide(Weapon);
                break;

            case SERP_METEOR:

                // damage = -DAMAGE_SERP_METEOR;
                damage = GetDamage(SpriteNum, Weapon, DMG_SERP_METEOR);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                }

                SetSuicide(Weapon);
                break;

            case BOLT_THINMAN_R0:
                damage = GetDamage(SpriteNum, Weapon, WPN_ROCKET);

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                }

                if (wu.Radius == NUKE_RADIUS) {
                    SpawnNuclearExp(Weapon);
                } else {
                    SpawnBoltExp(Weapon);
                }
                SetSuicide(Weapon);
                break;

            case BOLT_THINMAN_R1:
                // damage = -(2000 + (65 + RANDOM_RANGE(40))); // -2000 makes armor not count
                damage = -(65 + RANDOM_RANGE(40));

                if (u.sop_parent != -1) {
                    Sector_Object sopp = SectorObject[u.sop_parent];
                    if (TEST(sopp.flags, SOBJ_DIE_HARD)) {
                        break;
                    }
                    SopDamage(sopp, damage);
                    SopCheckKill(sopp);
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                    if (PlayerPP.Armor != 0) {
                        PlaySound(DIGI_ARMORHIT, PlayerPP, v3df_dontpan | v3df_follow | v3df_doppler);
                    }
                } else {
                    // this is special code to prevent the Zombie from taking out the Bosses to
                    // quick
                    // if rail gun weapon owner is not player
                    USER wou = getUser(wp.getOwner());
                    if (wp.getOwner() >= 0 && wou != null && wou.PlayerP == -1) {
                        // if actor is a boss
                        if (u.ID == ZILLA_RUN_R0 || u.ID == SERP_RUN_R0 || u.ID == SUMO_RUN_R0) {
                            damage /= 2;
                        }
                    }

                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                    ActorDamageSlide(SpriteNum, damage >> 1, wp.getAng());
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                wu.ID = 0; // No more damage
                SpawnTracerExp(Weapon);
                SetSuicide(Weapon);
                break;

            case BOLT_THINMAN_R2:
                damage = (GetDamage(SpriteNum, Weapon, WPN_ROCKET) / 2);

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                }

                if (wu.Radius == NUKE_RADIUS) {
                    SpawnNuclearExp(Weapon);
                } else {
                    SpawnBoltExp(Weapon);
                }
                SetSuicide(Weapon);
                break;

            case BOLT_THINMAN_R4:
                damage = GetDamage(SpriteNum, Weapon, DMG_GRENADE_EXP);

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorDamageSlide(SpriteNum, damage, ANG2SPRITE(sp, wp));
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnBunnyExp(Weapon);
                SetSuicide(Weapon);
                break;

            case SUMO_RUN_R0:
                damage = GetDamage(SpriteNum, Weapon, DMG_FLASHBOMB);

                damage /= 3;
                if (u.sop_parent != -1) {
                    Sector_Object sopp = SectorObject[u.sop_parent];
                    if (TEST(sopp.flags, SOBJ_DIE_HARD)) {
                        break;
                    }
                    SopDamage(sopp, damage);
                    SopCheckKill(sopp);
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorDamageSlide(SpriteNum, damage, ANG2SPRITE(sp, wp));
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                break;

            case BOLT_EXP:
                damage = GetDamage(SpriteNum, Weapon, DMG_BOLT_EXP);
                if (u.sop_parent != -1) {
                    Sector_Object sopp = SectorObject[u.sop_parent];
                    if (TEST(sopp.flags, SOBJ_DIE_HARD)) {
                        break;
                    }
                    SopDamage(sopp, damage);
                    SopCheckKill(sopp);
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorDamageSlide(SpriteNum, damage, ANG2SPRITE(sp, wp));
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                break;

            case BLOOD_WORM:

                // Don't hurt blood worm zombies!
                if (u.ID == ZOMBIE_RUN_R0) {
                    break;
                }

                damage = GetDamage(SpriteNum, Weapon, WPN_HEART);

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    // PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        if (PlayerCheckDeath(PlayerPP, Weapon)) {
                            // degrade blood worm life
                            wu.Counter3 += (4 * 120) / MISSILEMOVETICS;
                        }
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorDamageSlide(SpriteNum, damage, ANG2SPRITE(sp, wp));
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                // degrade blood worm life
                wu.Counter3 += (2 * 120) / MISSILEMOVETICS;

                break;

            case TANK_SHELL_EXP:
                damage = GetDamage(SpriteNum, Weapon, DMG_TANK_SHELL_EXP);
                if (u.sop_parent != -1) {
                    Sector_Object sopp = SectorObject[u.sop_parent];
                    if (TEST(sopp.flags, SOBJ_DIE_HARD)) {
                        break;
                    }
                    SopDamage(sopp, damage);
                    SopCheckKill(sopp);
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorDamageSlide(SpriteNum, damage, ANG2SPRITE(sp, wp));
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                break;

            case MUSHROOM_CLOUD:
            case GRENADE_EXP:
                if (wu.Radius == NUKE_RADIUS) // Special Nuke stuff
                {
                    damage = (GetDamage(SpriteNum, Weapon, DMG_NUCLEAR_EXP));
                } else {
                    damage = GetDamage(SpriteNum, Weapon, DMG_GRENADE_EXP);
                }

                if (u.sop_parent != -1) {
                    Sector_Object sopp = SectorObject[u.sop_parent];
                    if (TEST(sopp.flags, SOBJ_DIE_HARD)) {
                        break;
                    }
                    SopDamage(sopp, damage);
                    SopCheckKill(sopp);
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    // Don't let it hurt the SUMO
                    USER wou = getUser(wp.getOwner());
                    if (wp.getOwner() != -1 && wou != null && wou.ID == SUMO_RUN_R0) {
                        break;
                    }
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorDamageSlide(SpriteNum, damage, ANG2SPRITE(sp, wp));
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                break;

            case MICRO_EXP:

                damage = GetDamage(SpriteNum, Weapon, DMG_MINE_EXP);

                if (u.sop_parent != -1) {
                    Sector_Object sopp = SectorObject[u.sop_parent];
                    if (TEST(sopp.flags, SOBJ_DIE_HARD)) {
                        break;
                    }
                    SopDamage(sopp, damage);
                    SopCheckKill(sopp);
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorDamageSlide(SpriteNum, damage, ANG2SPRITE(sp, wp));
                    ActorChooseDeath(SpriteNum, Weapon);
                }
                break;

            case MINE_EXP: {
                damage = GetDamage(SpriteNum, Weapon, DMG_MINE_EXP);
                USER wou = getUser(wp.getOwner());
                if (wp.getOwner() != -1 && wou != null && wou.ID == SERP_RUN_R0) {
                    damage /= 6;
                }

                if (u.sop_parent != -1) {
                    Sector_Object sopp = SectorObject[u.sop_parent];
                    if (TEST(sopp.flags, SOBJ_DIE_HARD)) {
                        break;
                    }
                    SopDamage(sopp, damage);
                    SopCheckKill(sopp);
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    // Don't let serp skulls hurt the Serpent God
                    if (wp.getOwner() != -1 && wou != null && wou.ID == SERP_RUN_R0) {
                        break;
                    }
                    // Don't let it hurt the SUMO
                    if (wp.getOwner() != -1 && wou != null && wou.ID == SUMO_RUN_R0) {
                        break;
                    }
                    if (u.ID == TRASHCAN) {
                        ActorHealth(SpriteNum, -500);
                    } else {
                        ActorHealth(SpriteNum, damage);
                    }
                    ActorPain(SpriteNum);
                    ActorDamageSlide(SpriteNum, damage, ANG2SPRITE(sp, wp));
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                // reset id so no more damage is taken
                wu.ID = 0;
                break;
            }
            case NAP_EXP: {
                damage = GetDamage(SpriteNum, Weapon, DMG_NAPALM_EXP);
                USER wou = getUser(wp.getOwner());

                // Sumo Nap does less
                if (wp.getOwner() != -1 && wou != null && wou.ID == SUMO_RUN_R0) {
                    damage /= 4;
                }

                if (u.sop_parent != -1) {
                    Sector_Object sopp = SectorObject[u.sop_parent];
                    if (TEST(sopp.flags, SOBJ_DIE_HARD)) {
                        break;
                    }
                    SopDamage(sopp, damage);
                    SopCheckKill(sopp);
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    // Don't let it hurt the SUMO
                    if (wp.getOwner() != -1 && wou != null && wou.ID == SUMO_RUN_R0) {
                        break;
                    }
                    ActorHealth(SpriteNum, damage);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SetSuicide(Weapon);
                break;
            }

            case Vomit1:
            case Vomit2:

                damage = GetDamage(SpriteNum, Weapon, DMG_VOMIT);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SetSuicide(Weapon);
                break;

            case COOLG_FIRE:
                damage = GetDamage(SpriteNum, Weapon, DMG_COOLG_FIRE);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

//			        u.ID = 0;
                SetSuicide(Weapon);
                break;

            // Skull Exp
            case SKULL_R0:
            case BETTY_R0:

                damage = GetDamage(SpriteNum, Weapon, DMG_SKULL_EXP);

                if (u.sop_parent != -1) {
                    Sector_Object sopp = SectorObject[u.sop_parent];
                    if (TEST(sopp.flags, SOBJ_DIE_HARD)) {
                        break;
                    }
                    SopDamage(sopp, damage);
                    SopCheckKill(sopp);
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, ANG2PLAYER(PlayerPP, wp));
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorDamageSlide(SpriteNum, damage, ANG2SPRITE(sp, wp));
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                break;

            // Serp ring of skull
            case SKULL_SERP:
                // DoSkullBeginDeath(Weapon);
                break;

            case FIREBALL1: {
                damage = GetDamage(SpriteNum, Weapon, WPN_HOTHEAD);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                    ActorDamageSlide(SpriteNum, damage, wp.getAng());
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                USER wou = getUser(wp.getOwner());
                if (wp.getOwner() >= 0 && wou != null) // For SerpGod Ring
                {
                    wou.Counter--;
                }
                SpawnFireballFlames(Weapon, SpriteNum);
                SetSuicide(Weapon);
                break;
            }
            case FIREBALL:
            case GORO_FIREBALL:

                damage = GetDamage(SpriteNum, Weapon, DMG_GORO_FIREBALL);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    PlayerDamageSlide(PlayerPP, damage, wp.getAng());
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                    if (PlayerPP.Armor != 0) {
                        PlaySound(DIGI_ARMORHIT, PlayerPP, v3df_dontpan | v3df_follow | v3df_doppler);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                    ActorDamageSlide(SpriteNum, damage, wp.getAng());
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SpawnGoroFireballExp(Weapon);
                SetSuicide(Weapon);
                break;

            case FIREBALL_FLAMES:

                damage = -DamageData[DMG_FIREBALL_FLAMES].damage_lo;

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                // SpawnFireballFlames(Weapon, SpriteNum);

                break;

            case RADIATION_CLOUD:

                damage = GetDamage(SpriteNum, Weapon, DMG_RADIATION_CLOUD);

                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        PlayerSound(DIGI_GASHURT, v3df_dontpan | v3df_follow | v3df_doppler, PlayerPP);
                        PlayerUpdateHealth(PlayerPP, damage - 1000);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    // Don't let it hurt the SUMO
                    USER wou = getUser(wp.getOwner());
                    if (wp.getOwner() != -1 && wou != null && wou.ID == SUMO_RUN_R0) {
                        break;
                    }
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                wu.ID = 0;
                break;

            case PLASMA:
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP == -1) {
                    if (u.ID == SKULL_R0 || u.ID == BETTY_R0) {
                        ActorHealth(SpriteNum, damage);
                        ActorStdMissile(SpriteNum, Weapon);
                        ActorChooseDeath(SpriteNum, Weapon);
                        SetSuicide(Weapon);
                        break;
                    } else if (u.ID == RIPPER_RUN_R0) {
                        DoRipperGrow(SpriteNum);
                        break;
                    }

                    ActorPainPlasma(SpriteNum);
                }

                InitPlasmaFountain(wp, SpriteNum);

                SetSuicide(Weapon);

                break;

            case CALTROPS:
                damage = GetDamage(SpriteNum, Weapon, DMG_MINE_SHRAP);
                if (u.sop_parent != -1) {
                    break;
                } else if (u.PlayerP != -1) {
                    PlayerStr PlayerPP = Player[u.PlayerP];
                    if (PlayerTakeDamage(PlayerPP, Weapon)) {
                        if (RANDOM_P2(1024 << 4) >> 4 < 800) {
                            PlayerSound(DIGI_STEPONCALTROPS, v3df_follow | v3df_dontpan, PlayerPP);
                        }
                        PlayerUpdateHealth(PlayerPP, damage);
                        PlayerCheckDeath(PlayerPP, Weapon);
                    }
                } else {
                    ActorHealth(SpriteNum, damage);
                    ActorPain(SpriteNum);
                    ActorStdMissile(SpriteNum, Weapon);
                    ActorChooseDeath(SpriteNum, Weapon);
                }

                SetSuicide(Weapon);
                break;

        }

        // If player take alot of damage, make him yell
        if (u.PlayerP != -1) {
            PlayerStr PlayerPP = Player[u.PlayerP];
            if (damage <= -40 && RANDOM_RANGE(1000) > 700) {
                PlayerSound(DIGI_SONOFABITCH, v3df_dontpan | v3df_follow, PlayerPP);
            } else if (damage <= -40 && RANDOM_RANGE(1000) > 700) {
                PlayerSound(DIGI_PAINFORWEAK, v3df_dontpan | v3df_follow, PlayerPP);
            } else if (damage <= -10) {
                PlayerSound(PlayerPainVocs[RANDOM_RANGE(MAX_PAIN)], v3df_dontpan | v3df_follow, PlayerPP);
            }
        }
    }

    // Select death text based on ID
    public static String DeathString(int SpriteNum) {
        USER ku = getUser(SpriteNum);
        if (ku == null) {
            return "";
        }

        switch (ku.ID) {
            case NINJA_RUN_R0:
                return (" ");
            case ZOMBIE_RUN_R0:
                return ("Zombie");
            case BLOOD_WORM:
                return ("Blood Worm");
            case SKEL_RUN_R0:
                return ("Skeletor Priest");
            case COOLG_RUN_R0:
                return ("Coolie Ghost");
            case GORO_RUN_R0:
                return ("Guardian");
            case HORNET_RUN_R0:
                return ("Hornet");
            case RIPPER_RUN_R0:
                return ("Ripper Hatchling");
            case RIPPER2_RUN_R0:
                return ("Ripper");
            case BUNNY_RUN_R0:
                return ("Killer Rabbit");
            case SERP_RUN_R0:
                return ("Serpent god");
            case GIRLNINJA_RUN_R0:
                return ("Girl Ninja");
            case BLADE1:
            case BLADE2:
            case BLADE3:
            case 5011:
                return ("blade");
            case STAR1:
                if (cfg.UseDarts) {
                    return ("dart");
                }
                return ("shuriken");
            case CROSSBOLT:
                return ("crossbow bolt");
            case SPEAR_R0:
                return ("spear");
            case LAVA_BOULDER:
            case LAVA_SHARD:
                return ("lava boulder");
            case UZI_SMOKE:
                return ("Uzi");
            case UZI_SMOKE + 2:
                return ("Evil Ninja Uzi");
            case SHOTGUN_SMOKE:
                return ("shotgun");
            case MIRV_METEOR:
            case SERP_METEOR:
                return ("meteor");
            case BOLT_THINMAN_R0:
                return ("rocket");
            case BOLT_THINMAN_R1:
                return ("rail gun");
            case BOLT_THINMAN_R2:
                return ("enemy rocket");
            case BOLT_THINMAN_R4: // BunnyRocket
                return ("bunny rocket");
            case BOLT_EXP:
                return ("explosion");
            case TANK_SHELL_EXP:
                return ("tank shell");
            case MUSHROOM_CLOUD:
                return ("nuclear bomb");
            case GRENADE_EXP:
                return ("40mm grenade");
            case MICRO_EXP:
                return ("micro missile");
            case MINE_EXP:
                // case MINE_SHRAP:
                return ("sticky bomb");
            case NAP_EXP:
                return ("napalm");
            case Vomit1:
            case Vomit2:
                return ("vomit");
            case COOLG_FIRE:
                return ("Coolie Ghost phlem");
            case SKULL_R0:
                return ("Accursed Head");
            case BETTY_R0:
                return ("Bouncing Betty");
            case SKULL_SERP:
                return ("Serpent god Protector");
            case FIREBALL1:
            case FIREBALL:
            case GORO_FIREBALL:
            case FIREBALL_FLAMES:
                return ("flames");
            case RADIATION_CLOUD:
                return ("radiation");
            case CALTROPS:
                return ("caltrops");
        }

        return null;
    }

    public static void DoDamageTest(int Weapon) {
        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);

        if (wp == null || wu == null) {
            return;
        }

        USER u;
        Sprite sp;
        int stat;
        int dist;

        for (stat = 0; stat < StatDamageList.length; stat++) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(StatDamageList[stat]); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                sp = node.get();
                u = getUser(i);
                if (u == null) {
                    continue;
                }

                dist = DISTANCE(sp.getX(), sp.getY(), wp.getX(), wp.getY());
                if (dist > wu.Radius + u.Radius) {
                    continue;
                }

                if (sp == wp) {
                    continue;
                }

                if (!TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK)) {
                    continue;
                }

                // !JIM! Put in a cansee so that you don't take damage through walls and such
                // For speed's sake, try limiting check only to radius weapons!
                if (wu.Radius > 200) {
                    if (!FAFcansee(sp.getX(), sp.getY(), SPRITEp_UPPER(sp), sp.getSectnum(), wp.getX(), wp.getY(), wp.getZ(), wp.getSectnum())) {
                        continue;
                    }
                }

                if (wp.getOwner() != i && SpriteOverlap(Weapon, i)) {
                    DoDamage(i, Weapon);
                }
            }
        }
    }

    public static void DoHitscanDamage(int Weapon, int hitsprite) {
        // this routine needs some sort of sprite generated from the hitscan
        // such as a smoke or spark sprite - reason is because of DoDamage()

        Sprite hitSpr = boardService.getSprite(hitsprite);
        if (hitSpr == null) {
            return;
        }

        for (int i : StatDamageList) {
            if (hitSpr.getStatnum() == i) {
                DoDamage(hitsprite, Weapon);
                break;
            }
        }
    }

    public static void DoFlamesDamageTest(int Weapon) {
        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);
        if (wp == null || wu == null) {
            return;
        }

        for (int j : StatDamageList) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();

                Sprite sp = node.get();
                USER u = getUser(i);
                if (u == null) {
                    continue;
                }

                switch (u.ID) {
                    case TRASHCAN:
                    case PACHINKO1:
                    case PACHINKO2:
                    case PACHINKO3:
                    case PACHINKO4:
                    case 623:
                        continue;
                }

                int dist = DISTANCE(sp.getX(), sp.getY(), wp.getX(), wp.getY());

                if (dist > wu.Radius + u.Radius) {
                    continue;
                }

                if (sp == wp) {
                    continue;
                }

                if (!TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN)) {
                    continue;
                }

                if (TEST(wp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                    continue;
                }

                if (wu.Radius > 200) // Note: No weaps have bigger radius than 200 cept explosion stuff
                {
                    if (FAFcansee(sp.getX(), sp.getY(), SPRITEp_MID(sp), sp.getSectnum(), wp.getX(), wp.getY(), SPRITEp_MID(wp), wp.getSectnum())) {
                        DoDamage(i, Weapon);
                    }
                } else if (SpriteOverlap(Weapon, i)) {
                    DoDamage(i, Weapon);
                }

            }
        }
    }

    public static int PrevWall(int wall_num) {
        int prev_wall;
        int start_wall = wall_num;

        do {
            prev_wall = wall_num;
            Wall w = boardService.getWall(wall_num);
            if (w == null) {
                break;
            }
            wall_num = w.getPoint2();
        } while (wall_num != start_wall);

        return (prev_wall);
    }

    private static final int[] sectlist = new int[MAXSECTORS]; // !JIM! Frank, 512 was not big enough for $dozer, was asserting out!

    public static void TraverseBreakableWalls(int start_sect, int x, int y, int z, int ang, int radius) {
        int k;

        int sectlistplc, sectlistend, sect, nextsector;
        int xmid, ymid;
        int dist;
        int break_count;
        int hitz = 0;

        sectlist[0] = start_sect;
        sectlistplc = 0;
        sectlistend = 1;

        // limit radius
        if (radius > 2000) {
            radius = 2000;
        }

        break_count = 0;
        while (sectlistplc < sectlistend) {
            sect = sectlist[sectlistplc++];
            Sector sec = boardService.getSector(sect);
            if (sec == null) {
                continue;
            }

            int endwall = (sec.getWallptr() + sec.getWallnum());
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                int j = wn.getIndex();
                if (j == endwall - 1) { // FIXME (as in original!)
                    break;
                }

                // see if this wall should be broken
                if (wal.getLotag() == TAG_WALL_BREAK) {
                    // find midpoint
                    Wall wal2 = boardService.getWall(j + 1); // FIXME (as in oiginal!)
                    if (wal2 == null) {
                        continue;
                    }

                    xmid = DIV2(wal.getX() + wal2.getX());
                    ymid = DIV2(wal.getY() + wal2.getY());

                    // don't need to go further if wall is too far out

                    dist = Distance(xmid, ymid, x, y);
                    if (dist > radius) {
                        continue;
                    }

                    if (WallBreakPosition(j, tmp_ptr[0].set(sect), tmp_ptr[1].set(0), tmp_ptr[2].set(0), tmp_ptr[3].set(hitz), tmp_ptr[4])) {
                        int sectnum =  tmp_ptr[0].value;
                        int hitx = tmp_ptr[1].value;
                        int hity = tmp_ptr[2].value;
                        hitz = tmp_ptr[3].value;
                        if (hitx != MAXLONG && sectnum >= 0 && FAFcansee(x, y, z, start_sect, hitx, hity, hitz, sectnum)) {
                            HitBreakWall(j, MAXLONG, MAXLONG, MAXLONG, ang, 0);
                            break_count++;
                            if (break_count > 4) {
                                return;
                            }
                        }
                    }
                }

                nextsector = wal.getNextsector();

                if (nextsector == -1) {
                    continue;
                }

                // make sure its not on the list
                for (k = sectlistend - 1; k >= 0; k--) {
                    if (sectlist[k] == nextsector) {
                        break;
                    }
                }

                // if its not on the list add it to the end
                if (k < 0) {
                    sectlist[sectlistend++] = nextsector;
                }
            }
        }
    }

    private static final int[] StatBreakList = {STAT_DEFAULT, STAT_BREAKABLE, STAT_NO_STATE, STAT_DEAD_ACTOR};

    public static void DoExpDamageTest(int Weapon) {
        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);
        if (wp == null || wu == null) {
            return;
        }

        USER u;
        Sprite sp;
        int i, stat;
        ListNode<Sprite> nexti;
        int dist;
        int max_stat;
        int break_count;

        Sprite found_sp = null;
        int found_dist = 999999;

        // crack sprites
        if (wu.ID != MUSHROOM_CLOUD) {
            WeaponExplodeSectorInRange(Weapon);
        }

        // Just like DoDamageTest() except that it doesn't care about the owner

        max_stat = StatDamageList.length;
        // don't check for mines if the weapon is a mine
        if (wp.getStatnum() == STAT_MINE_STUCK) {
            max_stat--;
        }

        for (stat = 0; stat < max_stat; stat++) {
            for (ListNode<Sprite> node = boardService.getStatNode(StatDamageList[stat]); node != null; node = nexti) {
                i = node.getIndex();
                nexti = node.getNext();

                sp = node.get();
                u = getUser(i);
                if (u == null) {
                    continue;
                }

                dist = DISTANCE(sp.getX(), sp.getY(), wp.getX(), wp.getY());

                if (dist > wu.Radius + u.Radius) {
                    continue;
                }

                if (sp == wp) {
                    continue;
                }

                if (StatDamageList[stat] == STAT_SO_SP_CHILD) {
                    DoDamage(i, Weapon);
                } else {
                    if (FindDistance3D(sp.getX() - wp.getX(), sp.getY() - wp.getY(), (sp.getZ() - wp.getZ()) >> 4) > wu.Radius + u.Radius) {
                        continue;
                    }

                    // added hitscan block because mines no long clip against actors/players
                    if (!TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN)) {
                        continue;
                    }

                    // Second parameter MUST have blocking bits set or cansee won't work
                    // added second check for FAF water - hitscans were hitting ceiling
                    if (!FAFcansee(wp.getX(), wp.getY(), wp.getZ(), wp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_UPPER(sp), sp.getSectnum()) && !FAFcansee(wp.getX(), wp.getY(), wp.getZ(), wp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_LOWER(sp), sp.getSectnum())) {
                        continue;
                    }

                    DoDamage(i, Weapon);
                }
            }
        }

        if (wu.ID == MUSHROOM_CLOUD) {
            return; // Central Nuke doesn't break stuff
                    // Only secondaries do that
        }

        TraverseBreakableWalls(wp.getSectnum(), wp.getX(), wp.getY(), wp.getZ(), wp.getAng(), wu.Radius);

        break_count = 0;
        max_stat = StatBreakList.length;
        // Breakable stuff
        for (stat = 0; stat < max_stat; stat++) {
            for (ListNode<Sprite> node = boardService.getStatNode(StatBreakList[stat]); node != null; node = nexti) {
                i = node.getIndex();
                nexti = node.getNext();
                sp = node.get();

                dist = DISTANCE(sp.getX(), sp.getY(), wp.getX(), wp.getY());
                if (dist > wu.Radius) {
                    continue;
                }

                dist = FindDistance3D(sp.getX() - wp.getX(), sp.getY() - wp.getY(), (SPRITEp_MID(sp) - wp.getZ()) >> 4);
                if (dist > wu.Radius) {
                    continue;
                }

                if (!FAFcansee(sp.getX(), sp.getY(), SPRITEp_MID(sp), sp.getSectnum(), wp.getX(), wp.getY(), wp.getZ(), wp.getSectnum())) {
                    continue;
                }

                if (TEST(sp.getExtra(), SPRX_BREAKABLE)) {
                    HitBreakSprite(i, wu.ID);
                    break_count++;
                    if (break_count > 6) {
                        break;
                    }
                }
            }
        }

        if (wu.ID == BLOOD_WORM) {
            return;
        }

        int found_spi = -1;
        // wall damaging
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_WALL_MOVE); node != null; node = nexti) {
            i = node.getIndex();
            nexti = node.getNext();
            sp = node.get();

            dist = DISTANCE(sp.getX(), sp.getY(), wp.getX(), wp.getY());
            if (dist > wu.Radius / 4) {
                continue;
            }

            if (TEST_BOOL1(sp)) {
                continue;
            }

            if (!CanSeeWallMove(wp, SP_TAG2(sp))) {
                continue;
            }

            if (dist < found_dist) {
                found_dist = dist;
                found_sp = sp;
                found_spi = i;
            }
        }

        if (found_sp != null) {
            if (SP_TAG2(found_sp) == 0) {
                // just do one
                DoWallMove(found_spi);
            } else {
                if (DoWallMoveMatch(SP_TAG2(found_sp))) {
                    DoSpawnSpotsForDamage(SP_TAG2(found_sp));
                }
            }
        }
    }

    public static void DoMineExpMine(int Weapon) {
        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);
        if (wp == null || wu == null) {
            return;
        }

        USER u;
        Sprite sp;
        int dist;
        int zdist;

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_MINE_STUCK); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            sp = node.get();
            u = getUser(i);
            if (u == null) {
                continue;
            }

            dist = DISTANCE(sp.getX(), sp.getY(), wp.getX(), wp.getY());
            if (dist > wu.Radius + u.Radius) {
                continue;
            }

            if (sp == wp) {
                continue;
            }

            if (!TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK_HITSCAN)) {
                continue;
            }

            // Explosions are spherical, not planes, so let's check that way, well
            // cylindrical at least.
            zdist = klabs(sp.getZ() - wp.getZ()) >> 4;
            if (SpriteOverlap(Weapon, i) || zdist < wu.Radius + u.Radius) {
                DoDamage(i, Weapon);
                // only explode one mine at a time
                break;
            }
        }
    }

    public static final int STAR_STICK_RNUM = 400;
    public static final int STAR_BOUNCE_RNUM = 600;

    public static boolean DoStar(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        USER su;
        int vel;

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            u.motion_blur_num = 0;
            ScaleSpriteVector(Weapon, 54000);

            vel = EngineUtils.sqrt(SQ(u.xchange) + SQ(u.ychange));

            if (vel > 100) {
                if ((RANDOM_P2(1024 << 4) >> 4) < 128) {
                    SpawnBubble(Weapon);
                }
            }

            sp.setZ(sp.getZ() + 128 * MISSILEMOVETICS);

            DoActorZrange(Weapon);
            MissileWaterAdjust(Weapon);

            if (sp.getZ() > u.loz) {
                KillSprite(Weapon);
                return (true);
            }
        } else {
            vel = EngineUtils.sqrt(SQ(u.xchange) + SQ(u.ychange));

            if (vel < 800) {
                u.Counter += 50;
                u.zchange += u.Counter;
            }
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);
        // DoDamageTest(Weapon);

        if (u.moveSpriteReturn != 0 && !TEST(u.Flags, SPR_UNDERWATER)) {
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_PLAX_WALL:
                    break;

                case HIT_WALL: {
                    int wall_ang;
                    Wall wph;

                    final int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    wph = boardService.getWall(hitwall);
                    if (wph == null) {
                        break;
                    }

                    if (wph.getLotag() == TAG_WALL_BREAK) {
                        HitBreakWall(hitwall, sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), u.ID);
                        u.moveSpriteReturn = 0;
                        break;
                    }

                    // special case with MissileSetPos - don't queue star
                    // from this routine
                    if (TEST(u.Flags, SPR_SET_POS_DONT_KILL)) {
                        break;
                    }

                    // chance of sticking
                    if (!TEST(u.Flags, SPR_BOUNCE) && RANDOM_P2(1024) < STAR_STICK_RNUM) {
                        u.motion_blur_num = 0;
                        ChangeState(Weapon, s_StarStuck[0]);
                        sp.setXrepeat(sp.getXrepeat() - 16);
                        sp.setYrepeat(sp.getYrepeat() - 16);
                        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                        sp.setClipdist(16 >> 2);
                        u.ceiling_dist =  Z(2);
                        u.floor_dist =  Z(2);
                        // treat this just like a KillSprite but don't kill
                        QueueStar(Weapon);
                        return false;
                    }

                    // chance of bouncing
                    if (RANDOM_P2(1024) < STAR_BOUNCE_RNUM) {
                        break;
                    }

                    Wall nw = wph.getWall2();
                    wall_ang = NORM_ANGLE(EngineUtils.getAngle(nw.getX() - wph.getX(), nw.getY() - wph.getY()) + 512);

                    WallBounce(Weapon, wall_ang);
                    ScaleSpriteVector(Weapon, 36000);
                    u.Flags |= (SPR_BOUNCE);
                    u.motion_blur_num = 0;
                    u.moveSpriteReturn = 0;
                    break;
                }

                case HIT_SECTOR: {
                    boolean hitwall;
                    int hitsect = NORM_HIT_INDEX(u.moveSpriteReturn);

                    if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                        Sect_User hitsu = getSectUser(hitsect);
                        if (hitsu != null && hitsu.depth > 0) {
                            SpawnSplash(Weapon);
                            KillSprite(Weapon);
                            return (true);
                            // hit water - will be taken care of in WeaponMoveHit
                            // break;
                        }
                    }

                    Sprite losp = boardService.getSprite(u.lo_sp);
                    Sprite hisp = boardService.getSprite(u.hi_sp);

                    if (losp != null) {
                        if (losp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                            break;
                        }
                    }
                    if (hisp != null) {
                        if (hisp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                            break;
                        }
                    }

                    ScaleSpriteVector(Weapon, 58000);

                    vel = EngineUtils.sqrt(SQ(u.xchange) + SQ(u.ychange));
                    if (vel < 500) {
                        break; // will be killed below - u.ret != 0
                    }

                    // 32000 to 96000
                    u.xchange = mulscale(u.xchange, 64000 + (RANDOM_RANGE(64000) - 32000), 16);
                    u.ychange = mulscale(u.ychange, 64000 + (RANDOM_RANGE(64000) - 32000), 16);

                    if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                        u.zchange = mulscale(u.zchange, 50000, 16); // floor
                    } else {
                        u.zchange = mulscale(u.zchange, 40000, 16); // ceiling
                    }

                    if (SlopeBounce(Weapon, tmp_ptr[0])) {
                        hitwall = tmp_ptr[0].value != 0;
                        if (hitwall) {
                            // chance of sticking
                            if (RANDOM_P2(1024) < STAR_STICK_RNUM) {
                                break;
                            }

                            // chance of bouncing
                            if (RANDOM_P2(1024) < STAR_BOUNCE_RNUM) {
                                break;
                            }

                        }
                        u.Flags |= (SPR_BOUNCE);
                        u.motion_blur_num = 0;
                        u.moveSpriteReturn = 0;

                        // BREAK HERE - LOOOK !!!!!!!!!!!!!!!!!!!!!!!!
                        break; // hit a slope
                    }

                    u.Flags |= (SPR_BOUNCE);
                    u.motion_blur_num = 0;
                    u.moveSpriteReturn = 0;
                    u.zchange = -u.zchange;

                    // 32000 to 96000
                    u.xchange = mulscale(u.xchange, 64000 + (RANDOM_RANGE(64000) - 32000), 16);
                    u.ychange = mulscale(u.ychange, 64000 + (RANDOM_RANGE(64000) - 32000), 16);
                    if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                        u.zchange = mulscale(u.zchange, 50000, 16); // floor
                    } else {
                        u.zchange = mulscale(u.zchange, 40000, 16); // ceiling
                    }

                    break;
                }
            }
        }

        if (u.moveSpriteReturn != 0) {
            int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
            if (hitsprite != -1 && DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_SPRITE) {
                su = getUser(hitsprite);
                if (su != null && (su.ID == TRASHCAN || su.ID == ZILLA_RUN_R0)) {
                    PlaySound(DIGI_STARCLINK, sp, v3df_none);
                }
            }

            if (DTEST(u.moveSpriteReturn, HIT_MASK) != HIT_SPRITE) // Don't clank on sprites
            {
                PlaySound(DIGI_STARCLINK, sp, v3df_none);
            }

            if (WeaponMoveHit(Weapon)) {
                KillSprite(Weapon);
                return (true);
            }
        }

        return (false);
    }

    public static boolean DoCrossBolt(int Weapon) {
        USER u = getUser(Weapon);
        if (u == null) {
            return false;
        }

        DoBlurExtend(Weapon, 0, 2);

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, Z(16), Z(16), CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                KillSprite(Weapon);
                return (true);
            }
        }

        return (false);
    }

    public static void DoPlasmaDone(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        sp.setXrepeat(sp.getXrepeat() + u.Counter);
        sp.setYrepeat(sp.getYrepeat() - 4);
        u.Counter += 2;

        if (sp.getYrepeat() < 6) {
            KillSprite(Weapon);
        }
    }

/////////////////////////////////////////////////////////////////////
//
//General Breaking Shrapnel
//
/////////////////////////////////////////////////////////////////////

    public static int PickEnemyTarget(int spi, int aware_range) {
        Sprite sp = boardService.getSprite(spi);
        if (sp == null) {
            return 0;
        }

        DoPickTarget(spi, aware_range, 0);
        for (int tsi = 0; tsi < TargetSortCount; tsi++) {
            Target_Sort ts = TargetSort[tsi];
            if (ts != null) {
                Sprite tsp = boardService.getSprite(ts.sprite_num);
                if (tsp != null) {
                    if (ts.sprite_num == sp.getOwner() || tsp.getOwner() == sp.getOwner()) {
                        continue;
                    }
                    return (ts.sprite_num);
                }
            }
        }

        return (-1);
    }

    public static void MissileSeek(int Weapon, int delay_tics, int aware_range) { //, int dang_shift, int turn_limit, int z_limit) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        int zh;
        int ang2tgt, delta_ang;

        if (u.WaitTics <= delay_tics) {
            u.WaitTics += MISSILEMOVETICS;
        }

        if (u.WpnGoal == -1) {
            if (u.WaitTics > delay_tics) {
                int hitsprite;

                if (TEST(u.Flags2, SPR2_DONT_TARGET_OWNER)) {
                    if ((hitsprite = PickEnemyTarget(Weapon, aware_range)) != -1) {
                        USER hu = getUser(hitsprite);

                        if (hu != null) {
                            u.WpnGoal = hitsprite;
                            hu.Flags |= (SPR_TARGETED);
                            hu.Flags |= (SPR_ATTACKED);
                        }
                    }
                } else if ((hitsprite = DoPickTarget(Weapon, aware_range, 0)) != -1) {
                    USER hu = getUser(hitsprite);
                    if (hu != null) {
                        u.WpnGoal = hitsprite;
                        hu.Flags |= (SPR_TARGETED);
                        hu.Flags |= (SPR_ATTACKED);
                    }
                }
            }
        }

        if (u.WpnGoal >= 0) {
            USER wu = getUser(Weapon);
            if (wu == null) {
                return;
            }

            Sprite hp = boardService.getSprite(wu.WpnGoal);
            if (hp == null) {
                return;
            }

            // move to correct angle
            ang2tgt = EngineUtils.getAngle(hp.getX() - sp.getX(), hp.getY() - sp.getY());

            delta_ang = GetDeltaAngle(sp.getAng(), ang2tgt);

            if (klabs(delta_ang) > 32) {
                if (delta_ang > 0) {
                    delta_ang = 32;
                } else {
                    delta_ang = -32;
                }
            }

            sp.setAng(sp.getAng() - delta_ang);

            zh = SPRITEp_TOS(hp) + DIV4(SPRITEp_SIZE_Z(hp));

            delta_ang =  ((zh - sp.getZ()) >> 1);

            if (klabs(delta_ang) > Z(16)) {
                if (delta_ang > 0) {
                    delta_ang =  Z(16);
                } else {
                    delta_ang =  -Z(16);
                }
            }

            sp.setZvel(delta_ang);

            u.xchange = MOVEx(sp.getXvel(), sp.getAng());
            u.ychange = MOVEy(sp.getXvel(), sp.getAng());
            u.zchange = sp.getZvel();
        }
    }

    // completely vector manipulation
    public static void VectorMissileSeek(int Weapon, int delay_tics, int turn_speed, int aware_range1, int aware_range2) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        int dist;
        int zh;
        if (u.WaitTics <= delay_tics) {
            u.WaitTics += MISSILEMOVETICS;
        }

        if (u.WpnGoal == -1) {
            if (u.WaitTics > delay_tics) {
                int hitsprite;

                if (TEST(u.Flags2, SPR2_DONT_TARGET_OWNER)) {
                    if ((hitsprite = PickEnemyTarget(Weapon, aware_range1)) != -1) {
                        USER hu = getUser(hitsprite);

                        if (hu != null) {
                            u.WpnGoal = hitsprite;
                            hu.Flags |= (SPR_TARGETED);
                            hu.Flags |= (SPR_ATTACKED);
                        }
                    } else if ((hitsprite = PickEnemyTarget(Weapon, aware_range2)) != -1) {
                        USER hu = getUser(hitsprite);
                        if (hu != null) {
                            u.WpnGoal = hitsprite;
                            hu.Flags |= (SPR_TARGETED);
                            hu.Flags |= (SPR_ATTACKED);
                        }
                    }
                } else {
                    if ((hitsprite = DoPickTarget(Weapon, aware_range1, 0)) != -1) {
                        USER hu = getUser(hitsprite);
                        if (hu != null) {
                            u.WpnGoal = hitsprite;
                            hu.Flags |= (SPR_TARGETED);
                            hu.Flags |= (SPR_ATTACKED);
                        }
                    } else if ((hitsprite = DoPickTarget(Weapon, aware_range2, 0)) != -1) {
                        USER hu = getUser(hitsprite);

                        if (hu != null) {
                            u.WpnGoal = hitsprite;
                            hu.Flags |= (SPR_TARGETED);
                            hu.Flags |= (SPR_ATTACKED);
                        }
                    }
                }
            }
        }

        if (u.WpnGoal >= 0) {
            USER wu = getUser(Weapon);
            if (wu == null) {
                return;
            }

            Sprite hp = boardService.getSprite(wu.WpnGoal);

            if (hp == null) {
                return;
            }

            zh = SPRITEp_TOS(hp) + DIV4(SPRITEp_SIZE_Z(hp));

            dist = EngineUtils.sqrt(SQ(sp.getX() - hp.getX()) + SQ(sp.getY() - hp.getY()) + (SQ(sp.getZ() - zh) >> 8));

            int ox = u.xchange;
            int oy = u.ychange;
            int oz = u.zchange;

            u.xchange = scale(sp.getXvel(), hp.getX() - sp.getX(), dist);
            u.ychange = scale(sp.getXvel(), hp.getY() - sp.getY(), dist);
            u.zchange = scale(sp.getXvel(), zh - sp.getZ(), dist);

            // the large turn_speed is the slower the turn

            u.xchange = (u.xchange + ox * (turn_speed - 1)) / turn_speed;
            u.ychange = (u.ychange + oy * (turn_speed - 1)) / turn_speed;
            u.zchange = (u.zchange + oz * (turn_speed - 1)) / turn_speed;

            sp.setAng(EngineUtils.getAngle(u.xchange, u.ychange));
        }
    }

    public static void DoBlurExtend(int Weapon, int interval, int blur_num) {
        USER u = getUser(Weapon);
        if (u == null) {
            return;
        }

        if (u.motion_blur_num >= blur_num) {
            return;
        }

        u.Counter2++;
        if (u.Counter2 > interval) {
            u.Counter2 = 0;
        }

        if (u.Counter2 == 0) {
            u.motion_blur_num++;
            if (u.motion_blur_num > blur_num) {
                u.motion_blur_num =  blur_num;
            }
        }
    }

    public static void InitPlasmaFountain(Sprite wp, int spnum) {
        Sprite sp = boardService.getSprite(spnum);
        if (sp == null) {
            return;
        }

        Sprite np;
        USER nu;
        int SpriteNum = SpawnSprite(STAT_MISSILE, PLASMA_FOUNTAIN, s_PlasmaFountain[0], sp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_BOS(sp), sp.getAng(), 0);
        if (SpriteNum == -1) {
            return;
        }

        np = boardService.getSprite(SpriteNum);
        nu = getUser(SpriteNum);
        if (np == null || nu == null) {
            return;
        }

        np.setShade(-40);
        if (wp != null) {
            SetOwner(wp.getOwner(), SpriteNum);
        }
        SetAttach(spnum, SpriteNum);
        np.setYrepeat(0);
        np.setClipdist(8 >> 2);

        // start off on a random frame
        nu.WaitTics = 120 + 60;
        nu.Radius = 50;
    }

    public static void DoPlasmaFountain(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        Sprite ap;
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        int bak_cstat;

        // if no owner then die
        if (u.Attach == -1) {
            KillSprite(Weapon);
            return;
        } else {
            ap = boardService.getSprite(u.Attach);
            if (ap == null) {
                KillSprite(Weapon);
                return;
            }

            // move with sprite
            engine.setspritez(Weapon, ap.getX(), ap.getY(), ap.getZ());
            sp.setAng(ap.getAng());

            u.Counter++;
            if (u.Counter > 3) {
                u.Counter = 0;
            }

            if (u.Counter == 0) {
                SpawnBlood(u.Attach, Weapon, 0, 0, 0, 0);
                if (RANDOM_RANGE(1000) > 600) {
                    InitBloodSpray(u.Attach, false, 105);
                }
            }
        }

        // kill the fountain
        if ((u.WaitTics -= MISSILEMOVETICS) <= 0) {
            u.WaitTics = 0;

            bak_cstat = sp.getCstat();
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
            // DoDamageTest(Weapon); // fountain not doing the damage an more
            sp.setCstat(bak_cstat);

            KillSprite(Weapon);
        }
    }

//
//Glass
//

    public static boolean DoPlasma(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        int dax, day, daz;
        int ox, oy, oz;

        ox = sp.getX();
        oy = sp.getY();
        oz = sp.getZ();

        // MissileSeek(int Weapon, int delay_tics, int aware_range, int
        // dang_shift, int turn_limit, int z_limit)
        // MissileSeek(Weapon, 20, 1024, 6, 80, 6);
        DoBlurExtend(Weapon, 0, 4);

        dax = MOVEx(sp.getXvel(), sp.getAng());
        day = MOVEy(sp.getXvel(), sp.getAng());
        daz = sp.getZvel();

        u.moveSpriteReturn = move_missile(Weapon, dax, day, daz, Z(16), Z(16), CLIPMASK_MISSILE, MISSILEMOVETICS);

        if (u.moveSpriteReturn != 0) {
            // this sprite is supposed to go through players/enemys
            // if hit a player/enemy back up and do it again with blocking reset
            if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_SPRITE) {
                int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                Sprite hsp = boardService.getSprite(hitsprite);
                USER hu = getUser(hitsprite);

                if (hsp != null && TEST(hsp.getCstat(), CSTAT_SPRITE_BLOCK) && !TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                    int hcstat = hsp.getCstat();

                    if (hu != null && hitsprite != u.WpnGoal) {
                        sp.setX(ox);
                        sp.setY(oy);
                        sp.setZ(oz);

                        hsp.setCstat(hsp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                        u.moveSpriteReturn = move_missile(Weapon, dax, day, daz, Z(16), Z(16), CLIPMASK_MISSILE, MISSILEMOVETICS);
                        hsp.setCstat(hcstat);
                    }
                }
            }
        }

        MissileHitDiveArea(Weapon);
        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                if (TEST(u.Flags, SPR_SUICIDE)) {
                    KillSprite(Weapon);
                    return (true);
                } else {
                    u.Counter = 4;
                    ChangeState(Weapon, s_PlasmaDone[0]);
                }

                return (true);
            }
        }

        return (false);
    }

    public static boolean DoCoolgFire(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);
        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                PlaySound(DIGI_CGMAGICHIT, sp, v3df_follow);
                ChangeState(Weapon, s_CoolgFireDone[0]);
                USER wou = getUser(sp.getOwner());
                if (sp.getOwner() != -1 && wou != null && wou.ID != RIPPER_RUN_R0) {
                    SpawnDemonFist(Weapon); // Just a red magic circle flash
                }
                return (true);
            }
        }

        return (false);
    }

    public static void ScaleSpriteVector(int SpriteNum, int scale) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.xchange = mulscale(u.xchange, scale, 16);
        u.ychange = mulscale(u.ychange, scale, 16);
        u.zchange = mulscale(u.zchange, scale, 16);
    }

    public static void WallBounce(int SpriteNum, int ang) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int old_ang;
        int k, l;
        int dax, day;

        u.bounce++;

        // k = cos(ang) * sin(ang) * 2
        k = mulscale(EngineUtils.sin(NORM_ANGLE(ang + 512)), EngineUtils.sin(ang), 13);
        // l = cos(ang * 2)
        l = EngineUtils.sin(NORM_ANGLE((ang * 2) + 512));

        dax = -u.xchange;
        day = -u.ychange;

        u.xchange = dmulscale(day, k, dax, l, 14);
        u.ychange = dmulscale(dax, k, -day, l, 14);

        old_ang = sp.getAng();
        sp.setAng(EngineUtils.getAngle(u.xchange, u.ychange));

        // hack to prevent missile from sticking to a wall
        //
        if (old_ang == sp.getAng()) {
            u.xchange = -u.xchange;
            u.ychange = -u.ychange;
            sp.setAng(EngineUtils.getAngle(u.xchange, u.ychange));
        }
    }

    public static boolean SlopeBounce(int SpriteNum, LONGp hitwall) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        int hiz, loz;
        int slope;
        int dax, day, daz;
        int hitsector;
        int daang;

        hitsector = NORM_HIT_INDEX(u.moveSpriteReturn);
        Sector hitSec = boardService.getSector(hitsector);
        if (hitSec == null) {
            return false;
        }

        engine.getzsofslope(hitsector, sp.getX(), sp.getY(), fz, cz);
        hiz = cz.get();
        loz = fz.get();

        // detect the ceiling and the hitwall
        if (sp.getZ() < DIV2(hiz + loz)) {
            if (!TEST(hitSec.getCeilingstat(), CEILING_STAT_SLOPE)) {
                slope = 0;
            } else {
                slope = hitSec.getCeilingheinum();
            }
        } else {
            if (!TEST(hitSec.getFloorstat(), FLOOR_STAT_SLOPE)) {
                slope = 0;
            } else {
                slope = hitSec.getFloorheinum();
            }
        }

        if (slope == 0) {
            return (false);
        }

        // if greater than a 45 degree angle
        if (klabs(slope) > 4096) {
            hitwall.value = 1;
        } else {
            hitwall.value = 0;
        }

        // get angle of the first wall of the sector
        int k = hitSec.getWallptr();
        Wall w = boardService.getWall(k);
        if (w == null) {
            return false;
        }

        daang = w.getWallAngle();

        // k is now the slope of the ceiling or floor

        // normal vector of the slope
        dax = mulscale(slope, EngineUtils.sin((daang) & 2047), 14);
        day = mulscale(slope, EngineUtils.sin((daang + 1536) & 2047), 14);
        daz = 4096; // 4096 = 45 degrees

        // reflection code
        k = ((u.xchange * dax) + (u.ychange * day)) + mulscale(u.zchange, daz, 4);
        int l = (dax * dax) + (day * day) + (daz * daz);

        // make sure divscale doesn't overflow
        if ((klabs(k) >> 14) < l) {
            k = divscale(k, l, 17);
            u.xchange -= mulscale(dax, k, 16);
            u.ychange -= mulscale(day, k, 16);
            u.zchange -= mulscale(daz, k, 12);

            sp.setAng(EngineUtils.getAngle(u.xchange, u.ychange));
        }

        return (true);
    }

    public static boolean DoGrenade(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            ScaleSpriteVector(Weapon, 50000);
        }
        u.Counter += 20;
        u.zchange += u.Counter;

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (u.moveSpriteReturn != 0) {
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_PLAX_WALL:
                    KillSprite(Weapon);
                    return (true);
                case HIT_SPRITE: {
                    int wall_ang;

                    PlaySound(DIGI_40MMBNCE, sp, v3df_dontpan);

                    int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Sprite hsp = boardService.getSprite(hitsprite);
                    if (hsp == null) {
                        break;
                    }

                    // special case so grenade can ring gong
                    if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                        if (TEST(SP_TAG8(hsp), BIT(3))) {
                            DoMatchEverything(null, hsp.getHitag(), -1);
                        }
                    }

                    if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                        wall_ang = NORM_ANGLE(hsp.getAng());
                        WallBounce(Weapon, wall_ang);
                        ScaleSpriteVector(Weapon, 32000);
                    } else {
                        if (u.Counter2 == 1) // It's a phosphorus grenade!
                        {
                            for (int i = 0; i < 5; i++) {
                                sp.setAng(NORM_ANGLE(RANDOM_RANGE(2048)));
                                InitPhosphorus(Weapon);
                            }
                        }
                        SpawnGrenadeExp(Weapon);
                        KillSprite( Weapon);
                        return (true);
                    }

                    break;
                }

                case HIT_WALL: {
                    int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Wall wph = boardService.getWall(hitwall);
                    if (wph == null) {
                        break;
                    }

                    if (wph.getLotag() == TAG_WALL_BREAK) {
                        HitBreakWall(hitwall, sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), u.ID);
                        u.moveSpriteReturn = 0;
                        break;
                    }

                    PlaySound(DIGI_40MMBNCE, sp, v3df_dontpan);

                    int wall_ang = NORM_ANGLE(wph.getWallAngle() + 512);

                    // sp.ang = NORM_ANGLE(sp.ang + 1);
                    WallBounce(Weapon, wall_ang);
                    ScaleSpriteVector(Weapon, 22000);

                    break;
                }

                case HIT_SECTOR: {
                    boolean hitwall;
                    if (SlopeBounce(Weapon, tmp_ptr[0])) {
                        hitwall = tmp_ptr[0].value != 0;
                        if (hitwall) {
                            // hit a wall
                            ScaleSpriteVector(Weapon, 22000); // 28000
                            u.moveSpriteReturn = 0;
                            u.Counter = 0;
                        } else {
                            // hit a sector
                            if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                                // hit a floor
                                if (!TEST(u.Flags, SPR_BOUNCE)) {
                                    u.Flags |= (SPR_BOUNCE);
                                    ScaleSpriteVector(Weapon, 40000); // 18000
                                    u.moveSpriteReturn = 0;
                                    u.zchange /= 4;
                                    u.Counter = 0;
                                } else {
                                    if (u.Counter2 == 1) // It's a phosphorus grenade!
                                    {
                                        for (int i = 0; i < 5; i++) {
                                            sp.setAng(NORM_ANGLE(RANDOM_RANGE(2048)));
                                            InitPhosphorus(Weapon);
                                        }
                                    }
                                    SpawnGrenadeExp(Weapon);
                                    KillSprite( Weapon);
                                    return (true);
                                }
                            } else {
                                // hit a ceiling
                                ScaleSpriteVector(Weapon, 22000);
                            }
                        }
                    } else {
                        // hit floor
                        if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                            if (TEST(u.Flags, SPR_UNDERWATER)) {
                                u.Flags |= (SPR_BOUNCE); // no bouncing underwater
                            }

                            Sect_User su = getSectUser(sp.getSectnum());
                            if (u.lo_sectp != -1 && su != null && su.depth != 0) {
                                u.Flags |= (SPR_BOUNCE); // no bouncing on shallow water
                            }

                            if (!TEST(u.Flags, SPR_BOUNCE)) {
                                u.Flags |= (SPR_BOUNCE);
                                u.moveSpriteReturn = 0;
                                u.Counter = 0;
                                u.zchange = -u.zchange;
                                ScaleSpriteVector(Weapon, 40000); // 18000
                                u.zchange /= 4;
                                PlaySound(DIGI_40MMBNCE, sp, v3df_dontpan);
                            } else {
                                if (u.Counter2 == 1) // It's a phosphorus grenade!
                                {
                                    for (int i = 0; i < 5; i++) {
                                        sp.setAng(NORM_ANGLE(RANDOM_RANGE(2048)));
                                        InitPhosphorus(Weapon);
                                    }
                                }
                                // WeaponMoveHit(Weapon);
                                SpawnGrenadeExp(Weapon);
                                KillSprite( Weapon);
                                return (true);
                            }
                        } else
                        // hit something above
                        {
                            u.zchange = -u.zchange;
                            ScaleSpriteVector(Weapon, 22000);
                            PlaySound(DIGI_40MMBNCE, sp, v3df_dontpan);
                        }
                    }
                    break;
                }
            }
        }

        if (u.bounce > 10) {
            SpawnGrenadeExp(Weapon);
            KillSprite(Weapon);
            return (true);
        }

        // if you haven't bounced or your going slow do some puffs
        if (!TEST(u.Flags, SPR_BOUNCE | SPR_UNDERWATER)) {
            int newsp =  SpawnSprite(STAT_MISSILE, PUFF, s_Puff[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 100);
            Sprite np = boardService.getSprite(newsp);
            USER nu = getUser(newsp);

            if (np == null || nu == null) {
                return false;
            }

            SetOwner(Weapon, newsp);
            np.setShade(-40);
            np.setXrepeat(40);
            np.setYrepeat(40);
            nu.ox = u.ox;
            nu.oy = u.oy;
            nu.oz = u.oz;
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
            np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

            nu.xchange = u.xchange;
            nu.ychange = u.ychange;
            nu.zchange = u.zchange;

            ScaleSpriteVector(newsp, 22000);

            if (TEST(u.Flags, SPR_UNDERWATER)) {
                nu.Flags |= (SPR_UNDERWATER);
            }
        }

        return (false);
    }

    public static boolean DoVulcanBoulder(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        u.Counter += 40;
        u.zchange += u.Counter;

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        int vel = EngineUtils.sqrt(SQ(u.xchange) + SQ(u.ychange));

        if (vel < 30) {
            SpawnLittleExp(Weapon);
            KillSprite(Weapon);
            return (true);
        }

        if (u.moveSpriteReturn != 0) {
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_PLAX_WALL:
                    KillSprite(Weapon);
                    return (true);
                case HIT_SPRITE: {
                    int wall_ang;
                    int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Sprite hsp = boardService.getSprite(hitsprite);
                    if (hsp == null) {
                        break;
                    }

                    if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                        wall_ang = NORM_ANGLE(hsp.getAng());
                        WallBounce(Weapon, wall_ang);
                        ScaleSpriteVector(Weapon, 40000);
                    } else {
                        // hit an actor
                        SpawnLittleExp(Weapon);
                        KillSprite( Weapon);
                        return (true);
                    }

                    break;
                }

                case HIT_WALL: {
                    int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Wall wph = boardService.getWall(hitwall);
                    if (wph == null) {
                        break;
                    }

                    if (wph.getLotag() == TAG_WALL_BREAK) {
                        HitBreakWall(hitwall, sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), u.ID);
                        u.moveSpriteReturn = 0;
                        break;
                    }

                    int wall_ang = NORM_ANGLE(wph.getWallAngle() + 512);
                    WallBounce(Weapon, wall_ang);
                    ScaleSpriteVector(Weapon, 40000);
                    break;
                }

                case HIT_SECTOR: {
                    boolean hitwall;

                    if (SlopeBounce(Weapon, tmp_ptr[0])) {
                        hitwall = tmp_ptr[0].value != 0;
                        if (hitwall) {
                            // hit a sloped sector - treated as a wall because of large slope
                            ScaleSpriteVector(Weapon, 30000);
                            u.moveSpriteReturn = 0;
                            u.Counter = 0;
                        } else {
                            // hit a sloped sector
                            if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                                // hit a floor
                                u.xchange = mulscale(u.xchange, 30000, 16);
                                u.ychange = mulscale(u.ychange, 30000, 16);
                                u.zchange = mulscale(u.zchange, 12000, 16);
                                u.moveSpriteReturn = 0;
                                u.Counter = 0;

                                // limit to a reasonable bounce value
                                if (u.zchange > Z(32)) {
                                    u.zchange = Z(32);
                                }
                            } else {
                                // hit a sloped ceiling
                                u.moveSpriteReturn = 0;
                                u.Counter = 0;
                                ScaleSpriteVector(Weapon, 30000);
                            }
                        }
                    } else {
                        // hit unsloped floor
                        if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                            u.moveSpriteReturn = 0;
                            u.Counter = 0;

                            u.xchange = mulscale(u.xchange, 20000, 16);
                            u.ychange = mulscale(u.ychange, 20000, 16);
                            u.zchange = mulscale(u.zchange, 32000, 16);

                            // limit to a reasonable bounce value
                            if (u.zchange > Z(24)) {
                                u.zchange = Z(24);
                            }

                            u.zchange = -u.zchange;

                        } else
                        // hit unsloped ceiling
                        {
                            u.zchange = -u.zchange;
                            ScaleSpriteVector(Weapon, 30000);
                        }
                    }

                    break;
                }
            }
        }

        return (false);
    }

    public static boolean OwnerIsPlayer(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon), uo;

        if (u == null || sp == null || sp.getOwner() == -1) {
            return (false);
        }

        uo = getUser(sp.getOwner());
        return uo != null && uo.PlayerP != -1;
    }

//
//Wood
//

    public static boolean DoMineRangeTest(int Weapon, int range) {
        Sprite wp = boardService.getSprite(Weapon);
        if (wp == null) {
            return false;
        }

        boolean ownerisplayer = OwnerIsPlayer(Weapon);
        for (int j : StatDamageList) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite sp = node.get();
                USER u = getUser(i);
                if (u == null) {
                    continue;
                }

                int dist = DISTANCE(sp.getX(), sp.getY(), wp.getX(), wp.getY());
                if (dist > range) {
                    continue;
                }

                if (sp == wp) {
                    continue;
                }

                if (!TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK)) {
                    continue;
                }

                if (!TEST(sp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    continue;
                }

                if (u.ID == GIRLNINJA_RUN_R0 && !ownerisplayer) {
                    continue;
                }

                dist = FindDistance3D(wp.getX() - sp.getX(), wp.getY() - sp.getY(), (wp.getZ() - sp.getZ()) >> 4);
                if (dist > range) {
                    continue;
                }

                if (!FAFcansee(sp.getX(), sp.getY(), SPRITEp_UPPER(sp), sp.getSectnum(), wp.getX(), wp.getY(), wp.getZ(), wp.getSectnum())) {
                    continue;
                }

                return (true);
            }
        }

        return (false);
    }

    public static final int MINE_DETONATE_STATE = 99;

    public static void DoMineStuck(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        // if no owner then die
        Sprite ap = boardService.getSprite(u.Attach);
        if (ap != null) {
            USER au = getUser(u.Attach);
            if (au != null) {
                // Is it attached to a dead actor? Blow it up if so.
                if (TEST(au.Flags, SPR_DEAD) && u.Counter2 < MINE_DETONATE_STATE) {
                    u.Counter2 = MINE_DETONATE_STATE;
                    u.WaitTics = (SEC(1) / 2);
                }

                engine.setspritez(Weapon, ap.getX(), ap.getY(), ap.getZ() - u.sz);
                sp.setZ(ap.getZ() - DIV2(SPRITEp_SIZE_Z(ap)));
            }
        }

        // not activated yet
        if (!TEST(u.Flags, SPR_ACTIVE)) {
            if ((u.WaitTics -= (MISSILEMOVETICS * 2)) > 0) {
                return;
            }

            // activate it
            // u.WaitTics = 65536;
            u.WaitTics = 32767;
            u.Counter2 = 0;
            u.Flags |= (SPR_ACTIVE);
        }

        // limit the number of times DoMineRangeTest is called
        u.Counter++;
        if (u.Counter > 1) {
            u.Counter = 0;
        }

        if (u.Counter2 != MINE_DETONATE_STATE) {
            if ((u.Counter2++) > 30) {
                PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                u.WaitTics = 32767; // Keep reseting tics to make it stay forever
                u.Counter2 = 0;
            }
        }

        if (u.Counter == 0) {
            // not already in detonate state
            if (u.Counter2 < MINE_DETONATE_STATE) {
                // if something came into range - detonate
                if (DoMineRangeTest(Weapon, 3000)) {
                    // move directly to detonate state
                    u.Counter2 = MINE_DETONATE_STATE;
                    u.WaitTics =  (SEC(1) / 2);
                }
            }
        }

        u.WaitTics -= (MISSILEMOVETICS * 2);

        // start beeping with pauses
        // quick and dirty beep countdown code
        switch (u.Counter2) {
            case 30:
                if (u.WaitTics < SEC(6)) {
                    PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                    u.Counter2 = MINE_DETONATE_STATE;
                }
                break;
            case MINE_DETONATE_STATE:
                if (u.WaitTics < 0) {
                    PlaySound(DIGI_MINEBEEP, sp, v3df_dontpan);
                    SpawnMineExp(Weapon);
                    KillSprite(Weapon);
                    return;
                }
                break;
        }
    }

    public static void SetMineStuck(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        // stuck
        u.Flags |= (SPR_BOUNCE);
        // not yet active for 1 sec
        u.Flags &= ~(SPR_ACTIVE);
        u.WaitTics =  SEC(3);
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK_HITSCAN));
        u.Counter = 0;
        change_sprite_stat(Weapon, STAT_MINE_STUCK);
        ChangeState(Weapon, s_MineStuck[0]);
    }

    public static void DoMine(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            // decrease velocity
            ScaleSpriteVector(Weapon, 50000);
            u.Counter += 20;
        } else {
            u.Counter += 40;
        }
        u.zchange += u.Counter;

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (u.moveSpriteReturn != 0) {
            // check to see if you hit a sprite
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_PLAX_WALL:
                    KillSprite(Weapon);
                    return;
                case HIT_SPRITE: {
                    final int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Sprite hsp = boardService.getSprite(hitsprite);
                    USER hu = getUser(hitsprite);
                    if (hsp == null) {
                        break;
                    }

                    SetMineStuck(Weapon);
                    // Set the Z position
                    sp.setZ(hsp.getZ() - DIV2(SPRITEp_SIZE_Z(hsp)));

                    // If it's not alive, don't stick it
                    if (hu != null && hu.Health <= 0) {
                        return;
                    }

                    // check to see if sprite is player or enemy
                    if (TEST(hsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                        USER uo;
                        PlayerStr pp;

                        // attach weapon to sprite
                        SetAttach(hitsprite, Weapon);
                        u.sz = hsp.getZ() - sp.getZ();

                        if (sp.getOwner() >= 0) {
                            uo = getUser(sp.getOwner());

                            if (uo != null && uo.PlayerP != -1) {
                                pp = Player[uo.PlayerP];

                                if (RANDOM_RANGE(1000) > 800) {
                                    PlayerSound(DIGI_STICKYGOTU1, v3df_follow | v3df_dontpan, pp);
                                } else if (RANDOM_RANGE(1000) > 800) {
                                    PlayerSound(DIGI_STICKYGOTU2, v3df_follow | v3df_dontpan, pp);
                                } else if (RANDOM_RANGE(1000) > 800) {
                                    PlayerSound(DIGI_STICKYGOTU3, v3df_follow | v3df_dontpan, pp);
                                } else if (RANDOM_RANGE(1000) > 800) {
                                    PlayerSound(DIGI_STICKYGOTU4, v3df_follow | v3df_dontpan, pp);
                                }
                            }
                        }
                    } else {
                        if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                            u.Flags2 |= (SPR2_ATTACH_WALL);
                        } else if (TEST(hsp.getCstat(), CSTAT_SPRITE_FLOOR)) {
                            // hit floor
                            if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                                u.Flags2 |= (SPR2_ATTACH_FLOOR);
                            } else {
                                u.Flags2 |= (SPR2_ATTACH_CEILING);
                            }
                        } else {
                            SpawnMineExp(Weapon);
                            KillSprite(Weapon);
                            return;
                        }
                    }

                    break;
                }

                case HIT_WALL: {
                    int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Wall wph = boardService.getWall(hitwall);
                    if (wph == null) {
                        break;
                    }

                    if (wph.getLotag() == TAG_WALL_BREAK) {
                        HitBreakWall(hitwall, sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), u.ID);
                        break;
                    }

                    SetMineStuck(Weapon);
                    u.Flags2 |= (SPR2_ATTACH_WALL);
                    if (TEST(wph.getExtra(), WALLFX_DONT_STICK)) {
                        SpawnMineExp(Weapon);
                        KillSprite(Weapon);
                        return;
                    }
                    break;
                }

                case HIT_SECTOR: {
                    int hitsect = NORM_HIT_INDEX(u.moveSpriteReturn);

                    SetMineStuck(Weapon);

                    // hit floor
                    if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                        u.Flags2 |= (SPR2_ATTACH_FLOOR);
                    } else {
                        u.Flags2 |= (SPR2_ATTACH_CEILING);
                    }

                    Sector sph = boardService.getSector(hitsect);
                    if (sph != null && TEST(sph.getExtra(), SECTFX_SECTOR_OBJECT)) {
                        SpawnMineExp(Weapon);
                        KillSprite(Weapon);
                        return;
                    }
                    break;
                }
            }

            u.moveSpriteReturn = 0;
        }
    }

    public static void DoPuff(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setX(sp.getX() + u.xchange);
        sp.setY(sp.getY() + u.ychange);
        sp.setZ(sp.getZ() + u.zchange);
    }

    public static void DoRailPuff(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        sp.setXrepeat(sp.getXrepeat() + 4);
        sp.setYrepeat(sp.getYrepeat() + 4);
    }

    public static boolean DoBoltThinMan(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        DoBlurExtend(Weapon, 0, 4);

        int dax = MOVEx(sp.getXvel(), sp.getAng());
        int day = MOVEy(sp.getXvel(), sp.getAng());
        int daz = sp.getZvel();

        u.moveSpriteReturn = move_missile(Weapon, dax, day, daz, CEILING_DIST, FLOOR_DIST, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (TEST(u.Flags, SPR_SUICIDE)) {
            return (true);
        }

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                SpawnBoltExp(Weapon);
                KillSprite(Weapon);
                return (true);
            }
        }

        return (false);
    }

    public static boolean DoTracer(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        for (int i = 0; i < 4; i++) {
            u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

            MissileHitDiveArea(Weapon);
            if (u.moveSpriteReturn != 0) {
                if (WeaponMoveHit(Weapon)) {
                    KillSprite(Weapon);
                    return (true);
                }
            }
        }

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE));
        return (false);
    }

    public static boolean DoEMP(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        for (int i = 0; i < 4; i++) {
            u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

            MissileHitDiveArea(Weapon);

            if (RANDOM_RANGE(1000) > 500) {
                sp.setXrepeat(52);
                sp.setYrepeat(10);
            } else {
                sp.setXrepeat(8);
                sp.setYrepeat(38);
            }

            if (u.moveSpriteReturn != 0) {
                if (WeaponMoveHit(Weapon)) {
                    KillSprite(Weapon);
                    return (true);
                }
            }
        }

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE));
        return (false);
    }

//
//Stone
//

    public static void DoEMPBurst(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        Sprite ap = boardService.getSprite(u.Attach);
        if (ap != null) {
            engine.setspritez(Weapon, ap.getX(), ap.getY(), ap.getZ() - u.sz);
            sp.setAng(NORM_ANGLE(ap.getAng() + 1024));
        }

        // not activated yet
        if (!TEST(u.Flags, SPR_ACTIVE)) {
            // activate it
            u.WaitTics =  SEC(7);
            u.Flags |= (SPR_ACTIVE);
        }

        if (RANDOM_RANGE(1000) > 500) {
            sp.setXrepeat(52);
            sp.setYrepeat(10);
        } else {
            sp.setXrepeat(8);
            sp.setYrepeat(38);
        }

        if ((RANDOM_P2(1024 << 6) >> 6) < 700) {
            SpawnShrapX(Weapon);
        }

        u.WaitTics -= (MISSILEMOVETICS * 2);

        if (u.WaitTics < 0) {
            KillSprite(Weapon);
        }
    }

    public static boolean DoTankShell(int Weapon) {
        USER u = getUser(Weapon);
        if (u == null) {
            return false;
        }

        for (int i = 0; i < 4; i++) {
            u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

            MissileHitDiveArea(Weapon);
            if (u.moveSpriteReturn != 0) {
                if (WeaponMoveHit(Weapon)) {
                    SpawnTankShellExp(Weapon);
                    KillSprite(Weapon);
                    return (true);
                }
            }
        }

        return (false);
    }

    public static boolean DoTracerStart(int Weapon) {
        USER u = getUser(Weapon);
        if (u == null) {
            return false;
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);
        MissileHitDiveArea(Weapon);

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                KillSprite(Weapon);
                return (true);
            }
        }

        return (false);
    }

    public static boolean DoLaser(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        int spawn_count = 0;
        while (true) {
            u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

            MissileHitDiveArea(Weapon);

            if (u.moveSpriteReturn != 0) {
                if (WeaponMoveHit(Weapon)) {
                    SpawnBoltExp(Weapon);
                    KillSprite( Weapon);
                    return (true);
                }
            }

            spawn_count++;
            if (spawn_count < 256) {
                int newsp =  SpawnSprite(STAT_MISSILE, PUFF, s_LaserPuff[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
                if (newsp == -1) {
                    return false;
                }

                Sprite np = boardService.getSprite(newsp);
                USER nu = getUser(newsp);
                if (np == null || nu == null) {
                    return false;
                }

                np.setShade(-40);
                np.setXrepeat(16);
                np.setYrepeat(16);
                nu.spal = PALETTE_RED_LIGHTING;
                np.setPal(PALETTE_RED_LIGHTING);

                np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
                np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

                nu.xchange = nu.ychange = nu.zchange = 0;
            }
        }
    }

    public static boolean DoLaserStart(int Weapon) {
        USER u = getUser(Weapon);
        if (u == null) {
            return false;
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                SpawnBoltExp(Weapon);
                KillSprite( Weapon);
                return (true);
            }
        }

        return false;
    }

    public static boolean DoRail(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        int spawn_count = 0;

        while (true) {
            u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

            MissileHitDiveArea(Weapon);

            if (u.moveSpriteReturn != 0) {
                if (WeaponMoveHit(Weapon) && u.moveSpriteReturn != 0) {
                    if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_SPRITE) {
                        int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                        Sprite hsp = boardService.getSprite(hitsprite);

                        if (hsp != null && TEST(hsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                            int cstat_save = hsp.getCstat();

                            hsp.setCstat(hsp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN | CSTAT_SPRITE_BLOCK_MISSILE));
                            DoRail(Weapon);
                            hsp.setCstat(cstat_save);
                        } else {
                            SpawnTracerExp(Weapon);
                            SpawnShrapX(Weapon);
                            KillSprite( Weapon);
                        }
                    } else {
                        SpawnTracerExp(Weapon);
                        SpawnShrapX(Weapon);
                        KillSprite( Weapon);
                    }
                    return (true);
                }
            }

            spawn_count++;
            if (spawn_count < 128) {
                int newsp =  SpawnSprite(STAT_MISSILE, PUFF, s_RailPuff[0][0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 20);
                if (newsp == -1) {
                    return false;
                }

                Sprite np = boardService.getSprite(newsp);
                USER nu = getUser(newsp);
                if (np == null || nu == null) {
                    return false;
                }

                np.setXvel(np.getXvel() + (RANDOM_RANGE(140) - RANDOM_RANGE(140)));
                np.setYvel(np.getYvel() + (RANDOM_RANGE(140) - RANDOM_RANGE(140)));
                np.setZvel(np.getZvel() + (RANDOM_RANGE(140) - RANDOM_RANGE(140)));

                nu.RotNum = 5;
                NewStateGroup(newsp, WeaponStateGroup.sg_RailPuff);

                np.setShade(-40);
                np.setXrepeat(10);
                np.setYrepeat(10);
                nu.ox = u.ox;
                nu.oy = u.oy;
                nu.oz = u.oz;
                np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
                np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

                np.setHitag(np.getHitag() | LUMINOUS); // Always full brightness

                nu.xchange = u.xchange;
                nu.ychange = u.ychange;
                nu.zchange = u.zchange;

                ScaleSpriteVector(newsp, 1500);

                if (TEST(u.Flags, SPR_UNDERWATER)) {
                    nu.Flags |= (SPR_UNDERWATER);
                }
            }
        }
    }

    public static boolean DoRailStart(int Weapon) {
        USER u = getUser(Weapon);
        if (u == null) {
            return false;
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                SpawnTracerExp(Weapon);
                SpawnShrapX(Weapon);
                KillSprite( Weapon);
                return (true);
            }
        }
        return false;
    }

    public static boolean DoRocket(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        if ((u.FlagOwner -= ACTORMOVETICS) <= 0 && u.spal == 20) {
            Sprite tsp = boardService.getSprite(u.tgt_sp);
            if (tsp != null) {
                u.FlagOwner = (DISTANCE(sp.getX(), sp.getY(), tsp.getX(), tsp.getY()) >> 6);
                // Special warn sound attached to each seeker spawned
                PlaySound(DIGI_MINEBEEP, sp, v3df_follow);
            }
        }

        if (TEST(u.Flags, SPR_FIND_PLAYER)) {
            // MissileSeek(Weapon, 10, 768, 3, 48, 6);
            VectorMissileSeek(Weapon, 30, 16, 128, 768);
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        // DoDamageTest(Weapon);

        if (TEST(u.Flags, SPR_SUICIDE)) {
            return (true);
        }

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon) && u.moveSpriteReturn != 0) {
                if (u.ID == BOLT_THINMAN_R4) {
                    SpawnBunnyExp(Weapon);
                } else if (u.Radius == NUKE_RADIUS) {
                    SpawnNuclearExp(Weapon);
                } else {
                    SpawnBoltExp(Weapon);
                }
                KillSprite( Weapon);
                return (true);
            }
        }

        if (u.Counter == 0) {
            int newsp =  SpawnSprite(STAT_MISSILE, PUFF, s_Puff[0], sp.getSectnum(), u.ox, u.oy, u.oz, sp.getAng(), 100);
            Sprite np = boardService.getSprite(newsp);
            USER nu = getUser(newsp);
            if (np == null || nu == null) {
                return false;
            }

            SetOwner(Weapon, newsp);
            np.setShade(-40);
            np.setXrepeat(40);
            np.setYrepeat(40);
            nu.ox = u.ox;
            nu.oy = u.oy;
            nu.oz = u.oz;
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
            np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

            nu.xchange = u.xchange;
            nu.ychange = u.ychange;
            nu.zchange = u.zchange;

            ScaleSpriteVector(newsp, 20000);

            if (TEST(u.Flags, SPR_UNDERWATER)) {
                nu.Flags |= (SPR_UNDERWATER);
            }
        }

        return (false);
    }

    public static boolean DoMicroMini(int Weapon) {
        USER u = getUser(Weapon);
        if (u == null) {
            return false;
        }

        for (int i = 0; i < 3; i++) {
            u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

            MissileHitDiveArea(Weapon);

            if (u.moveSpriteReturn != 0) {
                if (WeaponMoveHit(Weapon)) {
                    SpawnMicroExp(Weapon);
                    KillSprite( Weapon);
                    return (true);
                }
            }
        }

        return (false);
    }
//
//Metal
//

    public static void SpawnExtraMicroMini(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        int w = SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R0, s_Micro[0][0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), sp.getXvel());
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);

        if (wp == null || wu == null) {
            return;
        }

        SetOwner(sp.getOwner(), w);
        wp.setXrepeat(sp.getXrepeat());
        wp.setYrepeat(sp.getXrepeat());
        wp.setShade(sp.getShade());
        wp.setClipdist(sp.getClipdist());

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_MicroMini);
        wu.WeaponNum = u.WeaponNum;
        wu.Radius = u.Radius;
        wu.ceiling_dist = u.ceiling_dist;
        wu.floor_dist = u.floor_dist;
        wp.setCstat(sp.getCstat());

        wp.setAng(NORM_ANGLE(wp.getAng() + RANDOM_RANGE(64) - 32));
        wp.setZvel(sp.getZvel());
        wp.setZvel(wp.getZvel() + RANDOM_RANGE(Z(16)) - Z(8));

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();
    }

    public static boolean DoMicro(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (u.Counter == 0) {
            int newsp =  SpawnSprite(STAT_MISSILE, PUFF, s_Puff[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 100);
            Sprite np = boardService.getSprite(newsp);
            USER nu = getUser(newsp);
            if (np == null || nu == null) {
                return false;
            }

            SetOwner(sp.getOwner(), newsp);
            np.setShade(-40);
            np.setXrepeat(20);
            np.setYrepeat(20);
            nu.ox = u.ox;
            nu.oy = u.oy;
            nu.oz = u.oz;
            np.setZvel(sp.getZvel());
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
            np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

            nu.xchange = u.xchange;
            nu.ychange = u.ychange;
            nu.zchange = u.zchange;

            ScaleSpriteVector(newsp, 20000);

            if (TEST(u.Flags, SPR_UNDERWATER)) {
                nu.Flags |= (SPR_UNDERWATER);
            }

            // last smoke
            if ((u.WaitTics -= MISSILEMOVETICS) <= 0) {
                engine.setspritez(newsp, np.getX(), np.getY(), np.getZ());
                NewStateGroup(Weapon, WeaponStateGroup.sg_MicroMini);
                sp.setXrepeat(10);
                sp.setYrepeat(10);
                sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE));
                SpawnExtraMicroMini(Weapon);
                return (true);
            }
        }

        // hit something
        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                SpawnMicroExp(Weapon);
                KillSprite(Weapon);
                return (true);
            }
        }

        return (false);
    }

    public static boolean DoUziBullet(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        // call move_sprite twice for each movement
        // otherwize the moves are in too big an increment
        for (int i = 0; i < 2; i++) {
            int dax = MOVEx((sp.getXvel() >> 1), sp.getAng());
            int day = MOVEy((sp.getXvel() >> 1), sp.getAng());
            int daz = sp.getZvel() >> 1;
            int sx = sp.getX();
            int sy = sp.getY();

            u.moveSpriteReturn = move_missile(Weapon, dax, day, daz, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);
            u.Dist += Distance(sx, sy, sp.getX(), sp.getY());

            MissileHitDiveArea(Weapon);

            if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 128) {
                SpawnBubble(Weapon);
            }

            if (u.moveSpriteReturn != 0) {
                WeaponMoveHit(Weapon);

                int j =  SpawnSprite(STAT_MISSILE, UZI_SMOKE, s_UziSmoke[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
                Sprite wp = boardService.getSprite(j);
                if (wp == null) {
                    return false;
                }

                wp.setShade(-40);
                wp.setXrepeat(UZI_SMOKE_REPEAT);
                wp.setYrepeat(UZI_SMOKE_REPEAT);
                SetOwner(sp.getOwner(), j);
                wp.setAng(sp.getAng());
                wp.setClipdist(128 >> 2);
                wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_YCENTER));

                if (!TEST(u.Flags, SPR_UNDERWATER)) {
                    j =  SpawnSprite(STAT_MISSILE, UZI_SPARK, s_UziSpark[0], wp.getSectnum(), wp.getX(), wp.getY(), wp.getZ(), 0, 0);
                    if (j == -1) {
                        return false;
                    }

                    wp = boardService.getSprite(j);
                    if (wp != null) {
                        wp.setShade(-40);
                        wp.setXrepeat(UZI_SPARK_REPEAT);
                        wp.setYrepeat(UZI_SPARK_REPEAT);
                        // wp.owner = sp.owner;
                        SetOwner(sp.getOwner(), j);
                        wp.setAng(sp.getAng());
                        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
                    }
                }

                KillSprite(Weapon);

                return (true);
            } else if (u.Dist > 8000) {
                KillSprite( Weapon);
                return false;
            }
        }

        return (false);
    }

    public static boolean DoBoltSeeker(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        MissileSeek(Weapon, 30, 768); //, 4, 48, 6);
        DoBlurExtend(Weapon, 0, 4);

        int dax = MOVEx(sp.getXvel(), sp.getAng());
        int day = MOVEy(sp.getXvel(), sp.getAng());
        int daz = sp.getZvel();

        u.moveSpriteReturn = move_missile(Weapon, dax, day, daz, CEILING_DIST, FLOOR_DIST, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);
        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                SpawnBoltExp(Weapon);
                KillSprite( Weapon);
                return (true);
            }
        }

        return (false);
    }

    public static boolean DoElectro(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        DoBlurExtend(Weapon, 0, 4);

        // only seek on Electro's after a hit on an actor
        if (u.Counter > 0) {
            MissileSeek(Weapon, 30, 512); //, 3, 52, 2);
        }

        int dax = MOVEx(sp.getXvel(), sp.getAng());
        int day = MOVEy(sp.getXvel(), sp.getAng());
        int daz = sp.getZvel();

        u.moveSpriteReturn = move_missile(Weapon, dax, day, daz, CEILING_DIST, FLOOR_DIST, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);
        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }
        // DoDamageTest(Weapon);

        if (TEST(u.Flags, SPR_SUICIDE)) {
            return (true);
        }

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_SPRITE) {
                    Sprite hsp = boardService.getSprite(NORM_HIT_INDEX(u.moveSpriteReturn));
                    USER hu = getUser(NORM_HIT_INDEX(u.moveSpriteReturn));

                    if (hsp != null && hu != null && (!TEST(hsp.getExtra(), SPRX_PLAYER_OR_ENEMY) || hu.ID == SKULL_R0 || hu.ID == BETTY_R0)) {
                        SpawnShrap(Weapon, -1);
                    }
                } else {
                    SpawnShrap(Weapon, -1);
                }

                KillSprite(Weapon);
                return (true);
            }
        }

        return (false);
    }

    public static boolean DoLavaBoulder(int Weapon) {
        USER u = getUser(Weapon);
        if (u == null) {
            return false;
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);
        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (TEST(u.Flags, SPR_SUICIDE)) {
            return (true);
        }

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                SpawnShrap(Weapon, -1);
                KillSprite(Weapon);
                return (true);
            }
        }

        return (false);
    }

    public static boolean SpawnCoolieExp(int SpriteNum) {
        USER u = getUser(SpriteNum), eu;
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        int explosion;
        Sprite exp;
        int zh, nx, ny;

        u.Counter =  RANDOM_RANGE(120); // This is the wait til birth time!

        zh = sp.getZ() - SPRITEp_SIZE_Z(sp) + DIV4(SPRITEp_SIZE_Z(sp));
        nx = sp.getX() + MOVEx(64, sp.getAng() + 1024);
        ny = sp.getY() + MOVEy(64, sp.getAng() + 1024);

        PlaySound(DIGI_COOLIEEXPLODE, sp, v3df_none);

        explosion =  SpawnSprite(STAT_MISSILE, BOLT_EXP, s_BoltExp[0], sp.getSectnum(), nx, ny, zh, sp.getAng(), 0);
        exp = boardService.getSprite(explosion);
        eu = getUser(explosion);
        if (exp == null || eu == null) {
            return false;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(SpriteNum, explosion);
        exp.setShade(-40);
        eu.spal = u.spal;
        exp.setPal(u.spal);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        eu.Radius = DamageData[DMG_BOLT_EXP].radius;

        DoExpDamageTest(explosion);

        return (explosion) != 0;
    }

    public static void SpawnBreakStaticFlames(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int newsp =  SpawnSprite(STAT_STATIC_FIRE, FIREBALL_FLAMES, null, sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite np = boardService.getSprite(newsp);
        USER nu = getUser(newsp);
        if (np == null || nu == null) {
            return;
        }

        if (RANDOM_RANGE(1000) > 500) {
            np.setPicnum(3143);
        } else {
            np.setPicnum(3157);
        }

        np.setHitag(LUMINOUS); // Always full brightness
        np.setXrepeat(32);
        np.setYrepeat(32);

        np.setShade(-40);
        nu.spal = u.spal;
        np.setPal(u.spal);
        np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        nu.Radius = 200;

        nu.floor_dist = nu.ceiling_dist = 0;

        np.setZ(engine.getflorzofslope(np.getSectnum(), np.getX(), np.getY()));

        Set3DSoundOwner(newsp, PlaySound(DIGI_FIRE1, np, v3df_dontpan | v3df_doppler));

    }

    public static void SpawnFireballExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        PlaySound(DIGI_SMALLEXP, sp, v3df_none);

        int explosion =  SpawnSprite(STAT_MISSILE, FIREBALL_EXP, s_FireballExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        exp.setXrepeat(52);
        exp.setYrepeat(52);
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-40);
        eu.spal = u.spal;
        exp.setPal(u.spal);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        eu.Flags |= (DTEST(u.Flags, SPR_UNDERWATER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        //
        // All this stuff assures that explosions do not go into floors &
        // ceilings
        //

        SpawnExpZadjust(Weapon, explosion, Z(15), Z(15));

        if (RANDOM_P2(1024) < 150) {
            SpawnFireballFlames(explosion, -1);
        }
    }

    public static void SpawnGoroFireballExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        PlaySound(DIGI_MEDIUMEXP, sp, v3df_none);

        int explosion =  SpawnSprite(STAT_MISSILE, 0, s_FireballExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        exp.setXrepeat(16);
        exp.setYrepeat(16);
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-40);
        eu.spal = u.spal;
        exp.setPal(u.spal);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        //
        // All this stuff assures that explosions do not go into floors &
        // ceilings
        //

        SpawnExpZadjust(Weapon, explosion, Z(15), Z(15));
    }

    public static void SpawnBoltExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null) {
            return;
        }

        if (u != null && TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        PlaySound(DIGI_BOLTEXPLODE, sp, v3df_none);

        int explosion =  SpawnSprite(STAT_MISSILE, BOLT_EXP, s_BoltExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-40);
        exp.setXrepeat(76);
        exp.setYrepeat(76);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        if (RANDOM_P2(1024) > 512) {
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_XFLIP));
        }
        eu.Radius = DamageData[DMG_BOLT_EXP].radius;

        SpawnExpZadjust(Weapon, explosion, Z(40), Z(40));

        DoExpDamageTest(explosion);

        SetExpQuake(explosion); // !JIM! made rocket launcher shake things
        SpawnVis(-1, exp.getSectnum(), exp.getX(), exp.getY(), exp.getZ(), 16);

    }

    public static void SpawnBunnyExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        PlaySound(DIGI_BUNNYDIE3, sp, v3df_none);

        u.ID = BOLT_EXP; // Change id
        InitBloodSpray(Weapon, true, -1);
        InitBloodSpray(Weapon, true, -1);
        InitBloodSpray(Weapon, true, -1);
        DoExpDamageTest(Weapon);

    }

    public static void SpawnTankShellExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null) {
            return;
        }

        if (u != null && TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        PlaySound(DIGI_BOLTEXPLODE, sp, v3df_none);

        int explosion =  SpawnSprite(STAT_MISSILE, TANK_SHELL_EXP, s_TankShellExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-40);
        exp.setXrepeat(64 + 32);
        exp.setYrepeat(64 + 32);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        if (RANDOM_P2(1024) > 512) {
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_XFLIP));
        }
        eu.Radius = DamageData[DMG_TANK_SHELL_EXP].radius;

        SpawnExpZadjust(Weapon, explosion, Z(40), Z(40));
        DoExpDamageTest(explosion);
        SpawnVis(-1, exp.getSectnum(), exp.getX(), exp.getY(), exp.getZ(), 16);

    }

    public static void SpawnNuclearSecondaryExp(int Weapon, int ang) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return;
        }

        int explosion =  SpawnSprite(STAT_MISSILE, GRENADE_EXP, s_GrenadeExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 512);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-128);
        exp.setXrepeat(218);
        exp.setYrepeat(152);
        exp.setClipdist(sp.getClipdist());
        eu.ceiling_dist =  Z(16);
        eu.floor_dist =  Z(16);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        int vel = (2048 + 128) + RANDOM_RANGE(2048);
        eu.xchange = MOVEx(vel, ang);
        eu.ychange = MOVEy(vel, ang);
        eu.Radius = 200; // was NUKE_RADIUS
        eu.moveSpriteReturn = move_missile(explosion, eu.xchange, eu.ychange, 0, eu.ceiling_dist, eu.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        if (FindDistance3D(exp.getX() - sp.getX(), exp.getY() - sp.getY(), (exp.getZ() - sp.getZ()) >> 4) < 1024) {
            KillSprite(explosion);
            return;
        }

        SpawnExpZadjust(Weapon, explosion, Z(50), Z(10));

        InitChemBomb(explosion);

    }

    public static void SpawnNuclearExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null) {
            return;
        }

        int ang;
        PlayerStr pp = null;
        int rnd_rng;

        if (u != null && TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        PlaySound(DIGI_NUCLEAREXP, sp, v3df_dontpan | v3df_doppler);

        USER ou = getUser(sp.getOwner());
        if (sp.getOwner() != 0 && ou != null) {
            pp = Player[ou.PlayerP];
            rnd_rng =  RANDOM_RANGE(1000);

            if (rnd_rng > 990) {
                PlayerSound(DIGI_LIKEHIROSHIMA, v3df_follow | v3df_dontpan, pp);
            } else if (rnd_rng > 980) {
                PlayerSound(DIGI_LIKENAGASAKI, v3df_follow | v3df_dontpan, pp);
            } else if (rnd_rng > 970) {
                PlayerSound(DIGI_LIKEPEARL, v3df_follow | v3df_dontpan, pp);
            }
        }

        // Spawn big mushroom cloud
        int explosion =  SpawnSprite(STAT_MISSILE, MUSHROOM_CLOUD, s_NukeMushroom[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-128);
        exp.setXrepeat(255);
        exp.setYrepeat(255);
        exp.setClipdist(sp.getClipdist());
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        eu.spal = PALETTE_PLAYER1;
        exp.setPal(eu.spal); // Set nuke puff to gray

        InitChemBomb(explosion);

        // Do central explosion
        explosion =  SpawnSprite(STAT_MISSILE, MUSHROOM_CLOUD, s_GrenadeExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        exp = boardService.getSprite(explosion);
        eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-128);
        exp.setXrepeat(218);
        exp.setYrepeat(152);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        if (RANDOM_P2(1024) > 512) {
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_XFLIP));
        }

        eu.Radius = NUKE_RADIUS;

        SpawnExpZadjust(Weapon, explosion, Z(30), Z(30));

        DoExpDamageTest(explosion);

        // Nuclear effects
        SetNuclearQuake(explosion);

        if (pp != null) {
            SetFadeAmt(pp, -80, 1); // Nuclear flash
        }

        // Secondary blasts
        ang =  RANDOM_P2(2048);
        SpawnNuclearSecondaryExp(explosion, ang);
        ang =  (ang + 512 + RANDOM_P2(256));
        SpawnNuclearSecondaryExp(explosion, ang);
        ang =  (ang + 512 + RANDOM_P2(256));
        SpawnNuclearSecondaryExp(explosion, ang);
        ang =  (ang + 512 + RANDOM_P2(256));
        SpawnNuclearSecondaryExp(explosion, ang);

    }

    public static void SpawnTracerExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null) {
            return;
        }

        if (u != null && TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        int explosion;
        if (u != null && u.ID == BOLT_THINMAN_R1) {
            explosion =  SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R1, s_TracerExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        } else {
            explosion =  SpawnSprite(STAT_MISSILE, TRACER_EXP, s_TracerExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        }
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-40);
        exp.setXrepeat(4);
        exp.setYrepeat(4);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        if (RANDOM_P2(1024) > 512) {
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_XFLIP));
        }

        if (u != null && u.ID == BOLT_THINMAN_R1) {
            eu.Radius = DamageData[DMG_BASIC_EXP].radius;
            DoExpDamageTest(explosion);
        } else {
            eu.Radius = DamageData[DMG_BOLT_EXP].radius;
        }

    }

    public static void AddSpriteToSectorObject(int SpriteNum, int sopi) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u = getUser(SpriteNum);

        // make sure it has a user
        if (u == null) {
            u = SpawnUser(SpriteNum, 0, null);
        }

        if (sopi == -1) {
            return;
        }

        int sn;
        Sector_Object sop = SectorObject[sopi];
        // find a free place on this list
        for (sn = 0; sn < sop.sp_num.length; sn++) {
            if (sop.sp_num[sn] == -1) {
                break;
            }
        }

        sop.sp_num[sn] = SpriteNum;

        u.Flags |= (SPR_ON_SO_SECTOR | SPR_SO_ATTACHED);

        u.sx = sop.xmid - sp.getX();
        u.sy = sop.ymid - sp.getY();
        Sector sec = boardService.getSector(sop.mid_sector);
        if (sec != null) {
            u.sz = sec.getFloorz() - sp.getZ();
        }

        u.sang = sp.getAng();
    }

    public static void SpawnGrenadeSecondaryExp(int Weapon, int ang) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return;
        }

        int explosion =  SpawnSprite(STAT_MISSILE, GRENADE_EXP, s_GrenadeSmallExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 1024);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-40);
        exp.setXrepeat(32);
        exp.setYrepeat(32);
        exp.setClipdist(sp.getClipdist());
        eu.ceiling_dist =  Z(16);
        eu.floor_dist =  Z(16);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        // ang = RANDOM_P2(2048);
        int vel = (1024 + 512) + RANDOM_RANGE(1024);
        eu.xchange = MOVEx(vel, ang);
        eu.ychange = MOVEy(vel, ang);

        eu.moveSpriteReturn = move_missile(explosion, eu.xchange, eu.ychange, 0, eu.ceiling_dist, eu.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        if (FindDistance3D(exp.getX() - sp.getX(), exp.getY() - sp.getY(), (exp.getZ() - sp.getZ()) >> 4) < 1024) {
            KillSprite(explosion);
            return;
        }

        SpawnExpZadjust(Weapon, explosion, Z(50), Z(10));

        eu.ox = exp.getX();
        eu.oy = exp.getY();
        eu.oz = exp.getZ();

    }

    public static void SpawnGrenadeSmallExp(int Weapon) {
        int ang = RANDOM_P2(2048);
        SpawnGrenadeSecondaryExp(Weapon, ang);
    }

    public static void SpawnGrenadeExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return;
        }

        USER u = getUser(Weapon);
        int dx, dy, dz;

        if (u != null && TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        PlaySound(DIGI_30MMEXPLODE, sp, v3df_none);

        USER ou = getUser(sp.getOwner());
        if (RANDOM_RANGE(1000) > 990) {
            if (sp.getOwner() >= 0 && ou != null && ou.PlayerP != -1) {
                PlayerStr pp = Player[ou.PlayerP];
                PlayerSound(DIGI_LIKEFIREWORKS, v3df_follow | v3df_dontpan, pp);
            }
        }

        dx = sp.getX();
        dy = sp.getY();
        dz = sp.getZ();

        if (u != null && u.ID == ZILLA_RUN_R0) {
            dx += RANDOM_RANGE(1000) - RANDOM_RANGE(1000);
            dy += RANDOM_RANGE(1000) - RANDOM_RANGE(1000);
            dz = SPRITEp_MID(sp) + RANDOM_RANGE(1000) - RANDOM_RANGE(1000);
        }

        int explosion =  SpawnSprite(STAT_MISSILE, GRENADE_EXP, s_GrenadeExp[0], sp.getSectnum(), dx, dy, dz, sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-40);
        exp.setXrepeat(64 + 32);
        exp.setYrepeat(64 + 32);
        exp.setClipdist(sp.getClipdist());
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        eu.Radius = DamageData[DMG_GRENADE_EXP].radius;

        //
        // All this stuff assures that explosions do not go into floors &
        // ceilings
        //

        SpawnExpZadjust(Weapon, explosion, Z(100), Z(30));

        DoExpDamageTest(explosion);
        // InitMineShrap(explosion);

        SetExpQuake(explosion);
        SpawnVis(-1, exp.getSectnum(), exp.getX(), exp.getY(), exp.getZ(), 0);

    }

    public static void SpawnExpZadjust(int Weapon, int expi, int upper_zsize, int lower_zsize) {
        Sprite exp = boardService.getSprite(expi);
        USER u = getUser(Weapon);
        USER eu = getUser(expi);
        if (exp == null || eu == null) {
            return;
        }

        int tos_z, bos_z;
        if (u != null) {
            tos_z = exp.getZ() - upper_zsize;
            bos_z = exp.getZ() + lower_zsize;

            if (tos_z <= u.hiz + Z(4)) {
                exp.setZ(u.hiz + upper_zsize);
                exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YFLIP));
            } else if (bos_z > u.loz) {
                exp.setZ(u.loz - lower_zsize);
            }
        } else {
            engine.getzsofslope(exp.getSectnum(), exp.getX(), exp.getY(), fz, cz);

            tos_z = exp.getZ() - upper_zsize;
            bos_z = exp.getZ() + lower_zsize;

            if (tos_z <= cz.get() + Z(4)) {
                exp.setZ(cz.get() + upper_zsize);
                exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YFLIP));
            } else if (bos_z > fz.get()) {
                exp.setZ(fz.get() - lower_zsize);
            }
        }

        eu.oz = exp.getZ();
    }

    public static void SpawnMineExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return;
        }

        USER u = getUser(Weapon);

        if (u != null && TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        change_sprite_stat(Weapon, STAT_MISSILE);

        PlaySound(DIGI_MINEBLOW, sp, v3df_none);

        int explosion =  SpawnSprite(STAT_MISSILE, MINE_EXP, s_MineExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-40);
        exp.setXrepeat(64 + 44);
        exp.setYrepeat(64 + 44);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        eu.Radius = DamageData[DMG_MINE_EXP].radius;

        //
        // All this stuff assures that explosions do not go into floors &
        // ceilings
        //

        SpawnExpZadjust(Weapon, explosion, Z(100), Z(20));
        SpawnVis(-1, exp.getSectnum(), exp.getX(), exp.getY(), exp.getZ(), 16);

        SetExpQuake(explosion);

        // DoExpDamageTest(explosion);

    }

    public static void SpawnBigGunFlames(int Weapon, int Operator, Sector_Object sop) {
        boolean smallflames = false;

        if (Weapon < 0) {
            Weapon = klabs(Weapon);
            smallflames = true;
        }

        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return;
        }

        USER u = getUser(Weapon);

        int explosion = SpawnSprite(STAT_MISSILE, MICRO_EXP, s_BigGunFlame[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(Operator, explosion);
        exp.setShade(-40);
        if (smallflames) {
            exp.setXrepeat(12);
            exp.setYrepeat(12);
        } else {
            exp.setXrepeat(34);
            exp.setYrepeat(34);
        }
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        if (RANDOM_P2(1024) > 512) {
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_XFLIP));
        }
        if (RANDOM_P2(1024) > 512) {
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YFLIP));
        }

        // place all sprites on list
        int sn;
        for (sn = 0; sn < sop.sp_num.length; sn++) {
            if (sop.sp_num[sn] == -1) {
                break;
            }
        }

        sop.sp_num[sn] = explosion;

        // Place sprite exactly where shoot point is
        if (u == null) {
            return;
        }

        eu.Flags |= (DTEST(u.Flags, SPR_ON_SO_SECTOR | SPR_SO_ATTACHED));

        Sector sec;
        if (TEST(u.Flags, SPR_ON_SO_SECTOR)) {
            // move with sector its on
            sec = boardService.getSector(sp.getSectnum());
        } else {
            // move with the mid sector
            sec = boardService.getSector(sop.mid_sector);
        }

        if (sec != null) {
            exp.setZ(eu.oz = sec.getFloorz() - u.sz);
        }

        eu.sx = u.sx;
        eu.sy = u.sy;
        eu.sz = u.sz;

    }

    public static void DoMineExp(int SpriteNum) {
        DoExpDamageTest(SpriteNum);
    }

    public static void DoSectorExp(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setX(sp.getX() + u.xchange);
        sp.setY(sp.getY() + u.ychange);
    }

    public static int SpawnSectorExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return -1;
        }

        USER u = getUser(Weapon);

        if (u != null && TEST(u.Flags, SPR_SUICIDE)) {
            return (-1);
        }

        PlaySound(DIGI_30MMEXPLODE, sp, v3df_none);

        int explosion = SpawnSprite(STAT_MISSILE, GRENADE_EXP, s_SectorExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return -1;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        exp.setOwner(-1);
        exp.setShade(-40);
        exp.setXrepeat(90); // was 40,40
        exp.setYrepeat(90);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        eu.Radius = DamageData[DMG_SECTOR_EXP].radius;

        DoExpDamageTest(explosion);
        SetExpQuake(explosion);
        SpawnVis(-1, exp.getSectnum(), exp.getX(), exp.getY(), exp.getZ(), 16);

        return (explosion);
    }

    public static void SpawnMeteorExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return;
        }

        USER u = getUser(Weapon);
        if (u != null && TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        int explosion;
        if (u != null && u.spal == 25) // Serp ball
        {
            explosion =  SpawnSprite(STAT_MISSILE, METEOR_EXP, s_TeleportEffect2[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        } else {
            PlaySound(DIGI_MEDIUMEXP, sp, v3df_none);
            explosion =  SpawnSprite(STAT_MISSILE, METEOR_EXP, s_MeteorExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        }

        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        exp.setOwner(-1);
        exp.setShade(-40);
        if (sp.getYrepeat() < 64) {
            // small
            exp.setXrepeat(64);
            exp.setYrepeat(64);
        } else {
            // large - boss
            exp.setXrepeat(80);
            exp.setYrepeat(80);
        }

        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        eu.Radius = DamageData[DMG_BASIC_EXP].radius;

    }

    public static void SpawnLittleExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return;
        }

        PlaySound(DIGI_HEADSHOTHIT, sp, v3df_none);
        int explosion =  SpawnSprite(STAT_MISSILE, BOLT_EXP, s_SectorExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        exp.setOwner(-1);
        exp.setShade(-127);

        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        eu.Radius = DamageData[DMG_BASIC_EXP].radius;
        DoExpDamageTest(explosion);
        SpawnVis(-1, exp.getSectnum(), exp.getX(), exp.getY(), exp.getZ(), 16);

    }

    public static boolean DoFireball(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            int size = (sp.getYrepeat() - 1);
            sp.setXrepeat(size);
            sp.setYrepeat(size);
            if (sp.getXrepeat() <= 37) {
                SpawnSmokePuff(Weapon);
                KillSprite(Weapon);
                return (true);
            }
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (u.moveSpriteReturn != 0) {
            boolean hit_burn = false;

            if (WeaponMoveHit(Weapon)) {
                if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_SPRITE) {
                    int hspi = NORM_HIT_INDEX(u.moveSpriteReturn);

                    Sprite hsp = boardService.getSprite(hspi);
                    USER hu = getUser(hspi);

                    if (hsp != null && TEST(hsp.getExtra(), SPRX_BURNABLE)) {
                        if (hu == null) {
                            SpawnUser(hspi, hsp.getPicnum(), null);
                        }
                        SpawnFireballFlames(Weapon, hspi);
                        hit_burn = true;
                    }
                }

                if (!hit_burn) {
                    if (u.ID == GORO_FIREBALL) {
                        SpawnGoroFireballExp(Weapon);
                    } else {
                        SpawnFireballExp(Weapon);
                    }
                }

                KillSprite(Weapon);

                return (true);
            }
        }

        return (false);
    }

    public static void DoFindGround(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int hsp;
        int florhit;
        int save_cstat;
        int bak_cstat;

        // recursive routine to find the ground - either sector or floor sprite
        // skips over enemy and other types of sprites

        save_cstat = sp.getCstat();
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        FAFgetzrange(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), tmp_ptr[0].set(0), tmp_ptr[1].set(0), tmp_ptr[2].set(0), tmp_ptr[3].set(0), ((sp.getClipdist()) << 2) - GETZRANGE_CLIP_ADJ, CLIPMASK_PLAYER);

        u.hiz = tmp_ptr[0].value;
        u.loz = tmp_ptr[2].value;
        florhit = tmp_ptr[3].value;

        sp.setCstat(save_cstat);

        switch (DTEST(florhit, HIT_MASK)) {
            case HIT_SPRITE: {
                hsp = NORM_HIT_INDEX(florhit);
                Sprite s = boardService.getSprite(hsp);
                if (s == null) {
                    return;
                }

                if (TEST(s.getCstat(), CSTAT_SPRITE_FLOOR)) {
                    // found a sprite floor
                    u.lo_sp = hsp;
                    u.lo_sectp = -1;
                    return;
                } else {
                    // reset the blocking bit of what you hit and try again -
                    // recursive
                    bak_cstat = s.getCstat();
                    s.setCstat(s.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                    DoFindGround(SpriteNum);
                    s.setCstat(bak_cstat);
                }

                return;
            }
            case HIT_SECTOR: {
                u.lo_sectp = NORM_HIT_INDEX(florhit);
                u.lo_sp = -1;
                return;
            }

            default:
                break;
        }

    }

    public static void DoFindGroundPoint(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int hsp;
        int florhit;
        int save_cstat;
        int bak_cstat;

        // recursive routine to find the ground - either sector or floor sprite
        // skips over enemy and other types of sprites

        save_cstat = sp.getCstat();
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        FAFgetzrangepoint(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), tmp_ptr[0], tmp_ptr[1], tmp_ptr[2], tmp_ptr[3]); // &u.hiz,
        // &ceilhit,
        // &u.loz,
        // &florhit);
        u.hiz = tmp_ptr[0].value;
        u.loz = tmp_ptr[2].value;
        florhit = tmp_ptr[3].value;

        sp.setCstat(save_cstat);

        switch (DTEST(florhit, HIT_MASK)) {
            case HIT_SPRITE: {
                hsp = NORM_HIT_INDEX(florhit);
                Sprite s = boardService.getSprite(hsp);
                if (s == null) {
                    return;
                }

                if (TEST(s.getCstat(), CSTAT_SPRITE_FLOOR)) {
                    // found a sprite floor
                    u.lo_sp = hsp;
                    u.lo_sectp = -1;
                    return;
                } else {
                    // reset the blocking bit of what you hit and try again -
                    // recursive
                    bak_cstat = s.getCstat();
                    s.setCstat(s.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                    DoFindGroundPoint(SpriteNum);
                    s.setCstat(bak_cstat);
                }

                return;
            }
            case HIT_SECTOR: {
                u.lo_sectp = NORM_HIT_INDEX(florhit);
                u.lo_sp = -1;
                return;
            }

            default:
                break;
        }

    }

    public static boolean DoNapalm(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return false;
        }

        DoBlurExtend(Weapon, 1, 7);

        USER u = getUser(Weapon);
        if (u == null) {
            return false;
        }

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            int size = (sp.getYrepeat() - 1);
            sp.setXrepeat(size);
            sp.setYrepeat(size);
            if (size <= 30) {
                SpawnSmokePuff(Weapon);
                KillSprite(Weapon);
                return (true);
            }
        }

        int ox = sp.getX();
        int oy = sp.getY();
        int oz = sp.getZ();

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (u.moveSpriteReturn != 0) {
            // this sprite is suPlayerosed to go through players/enemys
            // if hit a player/enemy back up and do it again with blocking reset
            if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_SPRITE) {
                Sprite hsp = boardService.getSprite(NORM_HIT_INDEX(u.moveSpriteReturn));

                if (hsp != null && TEST(hsp.getCstat(), CSTAT_SPRITE_BLOCK) && !TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                    int hcstat = hsp.getCstat();

                    sp.setX(ox);
                    sp.setY(oy);
                    sp.setZ(oz);

                    hsp.setCstat(hsp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                    u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);
                    hsp.setCstat(hcstat);
                }
            }
        }

        u.Counter++;
        if (u.Counter > 2) {
            u.Counter = 0;
        }

        if (u.Counter == 0) {
            PlaySound(DIGI_NAPPUFF, sp, v3df_none);

            int explosion =  SpawnSprite(STAT_MISSILE, NAP_EXP, s_NapExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
            Sprite exp = boardService.getSprite(explosion);
            USER eu = getUser(explosion);
            if (exp == null || eu == null) {
                return false;
            }

            exp.setHitag(LUMINOUS); // Always full brightness
            // exp.owner = sp.owner;
            SetOwner(sp.getOwner(), explosion);
            exp.setShade(-40);
            exp.setCstat(sp.getCstat());
            exp.setXrepeat(48);
            exp.setYrepeat(64);
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
            if (RANDOM_P2(1024) < 512) {
                exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_XFLIP));
            }
            exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));
            eu.Radius = 1500;

            DoFindGroundPoint(explosion);
            MissileWaterAdjust(explosion);
            exp.setZ(eu.loz);
            eu.oz = exp.getZ();

            if (TEST(u.Flags, SPR_UNDERWATER)) {
                eu.Flags |= (SPR_UNDERWATER);
            }

        }

        // DoDamageTest(Weapon);

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                KillSprite( Weapon);

                return (true);
            }
        }

        return (false);
    }

    //called from SpawnShrap
    public static int SpawnLargeExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return 0;
        }

        PlaySound(DIGI_30MMEXPLODE, sp, v3df_none);

        int explosion =  SpawnSprite(STAT_MISSILE, GRENADE_EXP, s_SectorExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return -1;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        exp.setOwner(-1);
        exp.setShade(-40);
        exp.setXrepeat(90); // was 40,40
        exp.setYrepeat(90);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        eu.Radius = DamageData[DMG_SECTOR_EXP].radius;

        SpawnExpZadjust(Weapon, explosion, Z(50), Z(50));

        // Should not cause other sectors to explode
        DoExpDamageTest(explosion);
        SetExpQuake(explosion);
        SpawnVis(-1, exp.getSectnum(), exp.getX(), exp.getY(), exp.getZ(), 16);

        return (explosion);
    }

    public static void SpawnMicroExp(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return;
        }

        USER u = getUser(Weapon);

        if (u != null && TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        int explosion =  SpawnSprite(STAT_MISSILE, MICRO_EXP, s_MicroExp[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(sp.getOwner(), explosion);
        exp.setShade(-40);
        exp.setXrepeat(32);
        exp.setYrepeat(32);
        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        if (RANDOM_P2(1024) > 512) {
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_XFLIP));
        }
        if (RANDOM_P2(1024) > 512) {
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YFLIP));
        }
        eu.Radius = DamageData[DMG_BOLT_EXP].radius;

        //
        // All this stuff assures that explosions do not go into floors &
        // ceilings
        //

        SpawnExpZadjust(Weapon, explosion, Z(20), Z(20));
        SpawnVis(-1, exp.getSectnum(), exp.getX(), exp.getY(), exp.getZ(), 16);

    }

    public static void SpawnBreakFlames(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u = getUser(SpriteNum);
        int newsp = SpawnSprite(STAT_MISSILE, FIREBALL_FLAMES + 1, s_BreakFlames[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        Sprite np = boardService.getSprite(newsp);
        USER nu = getUser(newsp);
        if (np == null || nu == null) {
            return;
        }

        np.setHitag(LUMINOUS); // Always full brightness

        np.setXrepeat(16);
        np.setYrepeat(16);
        nu.Counter = 48; // max flame size

        // SetOwner(sp.owner, new);
        np.setShade(-40);
        if (u != null) {
            np.setPal(nu.spal = u.spal);
        }
        np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
        np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        nu.Radius = 200;

        nu.floor_dist = nu.ceiling_dist = 0;

        DoFindGround(newsp);
        nu.jump_speed = 0;
        DoBeginJump(newsp);

        Set3DSoundOwner(newsp, PlaySound(DIGI_FIRE1, np, v3df_dontpan | v3df_doppler));

    }

    public static void SpawnFireballFlames(int SpriteNum, int enemy) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u = getUser(SpriteNum);

        Sprite np;
        USER nu;
        int newsp;

        if (u != null && TEST(u.Flags, SPR_UNDERWATER)) {
            return;
        }

        USER eu = (enemy >= 0) ? getUser(enemy) : null;
        Sprite ep = (enemy >= 0) ? boardService.getSprite(enemy) : null;

        if (ep != null && eu != null) {
            // test for already burned
            if (TEST(ep.getExtra(), SPRX_BURNABLE) && ep.getShade() > 40) {
                return;
            }

            if (eu.flame >= 0) {
                int sizez = SPRITEp_SIZE_Z(ep) + DIV4(SPRITEp_SIZE_Z(ep));
                np = boardService.getSprite(eu.flame);
                nu = getUser(eu.flame);

                if (TEST(ep.getExtra(), SPRX_BURNABLE)) {
                    return;
                }

                if (nu != null && np != null) {
                    if (nu.Counter >= SPRITEp_SIZE_Z_2_YREPEAT(np, sizez)) {
                        // keep flame only slightly bigger than the enemy itself
                        nu.Counter = SPRITEp_SIZE_Z_2_YREPEAT(np, sizez);
                    } else {
                        // increase max size
                        nu.Counter += SPRITEp_SIZE_Z_2_YREPEAT(np, 8 << 8);
                    }

                    // Counter is max size
                    if (nu.Counter >= 230) {
                        // this is far too big
                        nu.Counter = 230;
                    }

                    if (nu.WaitTics < 2 * 120) {
                        nu.WaitTics = 2 * 120; // allow it to grow again
                    }
                }

                return;
            }
        }

        newsp =  SpawnSprite(STAT_MISSILE, FIREBALL_FLAMES, s_FireballFlames[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        np = boardService.getSprite(newsp);
        nu = getUser(newsp);
        if (np == null || nu == null) {
            return;
        }

        np.setHitag(LUMINOUS); // Always full brightness
        if (eu != null) {
            eu.flame = newsp;
        }

        np.setXrepeat(16);
        np.setYrepeat(16);
        if (ep != null) {
            // large flame for trees and such
            if (TEST(ep.getExtra(), SPRX_BURNABLE)) {
                int sizez = SPRITEp_SIZE_Z(ep) + DIV4(SPRITEp_SIZE_Z(ep));
                nu.Counter =  SPRITEp_SIZE_Z_2_YREPEAT(np, sizez);
            } else {
                nu.Counter =  SPRITEp_SIZE_Z_2_YREPEAT(np, SPRITEp_SIZE_Z(ep) >> 1);
            }
        } else {
            nu.Counter = 48; // max flame size
        }

        SetOwner(sp.getOwner(), newsp);
        np.setShade(-40);
        if (u != null) {
            np.setPal(nu.spal = u.spal);
        }
        np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
        np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        // nu.Radius = DamageData[DMG_FIREBALL_FLAMES].radius;
        nu.Radius = 200;

        if (enemy >= 0) {
            SetAttach(enemy, newsp);
        } else {
            if (TestDontStickSector(np.getSectnum())) {
                KillSprite(newsp);
                return;
            }

            nu.floor_dist = nu.ceiling_dist = 0;

            DoFindGround(newsp);
            nu.jump_speed = 0;
            DoBeginJump(newsp);
        }

        Set3DSoundOwner(newsp, PlaySound(DIGI_FIRE1, np, v3df_dontpan | v3df_doppler));

    }

    public static boolean DoSpear(int Weapon) {
        USER u = getUser(Weapon);
        if (u == null) {
            return false;
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (TEST(u.Flags, SPR_SUICIDE)) {
            return (true);
        }

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                // SpawnShrap(Weapon, -1);
                KillSprite(Weapon);
                return (true);
            }
        }

        return (false);
    }

    public static boolean DoBloodWorm(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        int ang;
        int xvect, yvect;
        int bx, by;
        long amt;
        int sectnum;

        u.moveSpriteReturn = move_ground_missile(Weapon, u.xchange, u.ychange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        if (u.moveSpriteReturn != 0) {
            u.xchange = -u.xchange;
            u.ychange = -u.ychange;
            u.moveSpriteReturn = 0;
            sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
            return (true);
        }

        MissileHitDiveArea(Weapon);

        if (u.z_tgt == 0) {
            // stay alive for 10 seconds
            if (++u.Counter3 > 3) {
                Sprite tsp;
                USER tu;

                InitBloodSpray(Weapon, false, 1);
                InitBloodSpray(Weapon, false, 1);
                InitBloodSpray(Weapon, false, 1);

                // Kill any old zombies you own
                ListNode<Sprite> nexti;
                for (ListNode<Sprite> node = boardService.getStatNode(STAT_ENEMY); node != null; node = nexti) {
                    int i = node.getIndex();
                    nexti = node.getNext();
                    tsp = node.get();
                    tu = getUser(i);

                    if (tu != null && tu.ID == ZOMBIE_RUN_R0 && tsp.getOwner() == sp.getOwner()) {
                        InitBloodSpray(i, true, 105);
                        InitBloodSpray(i, true, 105);
                        SetSuicide(i);
                        break;
                    }
                }

                SpawnZombie2(Weapon);
                KillSprite(Weapon);
                return (true);
            }
        }

        ang = NORM_ANGLE(sp.getAng() + 512);

        xvect = EngineUtils.sin(NORM_ANGLE(ang + 512));
        yvect = EngineUtils.sin(ang);

        bx = sp.getX();
        by = sp.getY();

        amt = RANDOM_P2(2048) - 1024;
        sp.setX(sp.getX() + mulscale(amt, xvect, 15));
        sp.setY(sp.getY() + mulscale(amt, yvect, 15));

        sectnum = sp.getSectnum();
        sectnum = engine.updatesectorz(sp.getX(), sp.getY(), sp.getZ(), sectnum);
        if (sectnum >= 0) {
            GlobalSkipZrange = true;
            InitBloodSpray(Weapon, false, 1);
            GlobalSkipZrange = false;
        }

        sp.setX(bx);
        sp.setY(by);

        return (false);
    }

    public static boolean DoSerpMeteor(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        int ox, oy, oz;

        ox = sp.getX();
        oy = sp.getY();
        oz = sp.getZ();

        sp.setXrepeat(sp.getXrepeat() + MISSILEMOVETICS * 2);
        if (sp.getXrepeat() > 80) {
            sp.setXrepeat(80);
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        if (u.moveSpriteReturn != 0) {
            // this sprite is supposed to go through players/enemys
            // if hit a player/enemy back up and do it again with blocking reset
            if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_SPRITE) {
                Sprite hsp = boardService.getSprite(NORM_HIT_INDEX(u.moveSpriteReturn));
                USER hu = getUser(NORM_HIT_INDEX(u.moveSpriteReturn));
                if (hsp == null) {
                    return false;
                }

                if (hu != null && hu.ID >= SKULL_R0 && hu.ID <= SKULL_SERP) {
                    int hcstat = hsp.getCstat();

                    sp.setX(ox);
                    sp.setY(oy);
                    sp.setZ(oz);

                    hsp.setCstat(hsp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                    u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);
                    hsp.setCstat(hcstat);
                }
            }
        }

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                SpawnMeteorExp(Weapon);

                KillSprite( Weapon);

                return (true);
            }
        }

        return (false);
    }

    public static boolean DoMirvMissile(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        sp.setXrepeat(sp.getXrepeat() + MISSILEMOVETICS * 2);
        if (sp.getXrepeat() > 80) {
            sp.setXrepeat(80);
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        if (u.moveSpriteReturn != 0) {
            if (WeaponMoveHit(Weapon)) {
                SpawnMeteorExp(Weapon);

                KillSprite( Weapon);

                return (true);
            }
        }

        return (false);
    }

    private static final int[] angs = {512, -512};

    public static boolean DoMirv(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        u.moveSpriteReturn = move_missile(Weapon, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS);

        MissileHitDiveArea(Weapon);

        if (TEST(u.Flags, SPR_UNDERWATER) && (RANDOM_P2(1024 << 4) >> 4) < 256) {
            SpawnBubble(Weapon);
        }

        u.Counter++;
        u.Counter &= 1;

        if (u.Counter == 0) {
            for (int i = 0; i < 2; i++) {
                int newsp =  SpawnSprite(STAT_MISSILE, MIRV_METEOR, WeaponStateGroup.sg_MirvMeteor.getState(0), sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), NORM_ANGLE(sp.getAng() + angs[i]), 800);
                Sprite np = boardService.getSprite(newsp);
                USER nu = getUser(newsp);
                if (np == null || nu == null) {
                    return false;
                }

                nu.RotNum = 5;
                NewStateGroup(newsp, WeaponStateGroup.sg_MirvMeteor);
                nu.StateEnd = s_MirvMeteorExp[0];

                // np.owner = Weapon;
                SetOwner(Weapon, newsp);
                np.setShade(-40);
                np.setXrepeat(40);
                np.setYrepeat(40);
                np.setClipdist(32 >> 2);
                np.setZvel(0);
                np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));

                nu.ceiling_dist =  Z(16);
                nu.floor_dist =  Z(16);
                nu.Dist = 200;
                // nu.Dist = 0;

                nu.xchange = MOVEx(np.getXvel(), np.getAng());
                nu.ychange = MOVEy(np.getXvel(), np.getAng());
                nu.zchange = np.getZvel();

                if (TEST(u.Flags, SPR_UNDERWATER)) {
                    nu.Flags |= (SPR_UNDERWATER);
                }
            }
        }

        if (u.moveSpriteReturn != 0) {
            SpawnMeteorExp(Weapon);
            KillSprite( Weapon);
            return (true);
        }

        return (false);
    }

    public static boolean MissileSetPos(int Weapon, Animator DoWeapon, int dist) {
        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);
        if (wp == null || wu == null) {
            return false;
        }

        int oldvel, oldzvel;
        int oldxc, oldyc, oldzc;
        boolean retval = false;

        // backup values
        oldxc = wu.xchange;
        oldyc = wu.ychange;
        oldzc = wu.zchange;
        oldvel = wp.getXvel();
        oldzvel = wp.getZvel();

        // make missile move in smaller increments
        wp.setXvel( ((dist * 6) / MISSILEMOVETICS));
        // wp.zvel = (wp.zvel*4) / MISSILEMOVETICS;
        wp.setZvel( ((wp.getZvel() * 6) / MISSILEMOVETICS));

        // some Weapon Animators use this
        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

        wu.Flags |= (SPR_SET_POS_DONT_KILL);

        if (DoWeapon.animatorInvoke(Weapon)) {
            retval = true;
        }

        wu.Flags &= ~(SPR_SET_POS_DONT_KILL);

        // reset values
        wu.xchange = oldxc;
        wu.ychange = oldyc;
        wu.zchange = oldzc;
        wp.setXvel( oldvel);
        wp.setZvel( oldzvel);

        // update for interpolation
        wu.ox = wp.getX();
        wu.oy = wp.getY();
        wu.oz = wp.getZ();

        return (retval);
    }

    public static boolean TestMissileSetPos(int Weapon, Animator DoWeapon, int dist, int zvel) {
        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);
        if (wp == null || wu == null) {
            return false;
        }

        int oldvel, oldzvel;
        int oldxc, oldyc, oldzc;
        boolean retval = false;

        // backup values
        oldxc = wu.xchange;
        oldyc = wu.ychange;
        oldzc = wu.zchange;
        oldvel = wp.getXvel();
        oldzvel = wp.getZvel();

        // make missile move in smaller increments
        wp.setXvel( ((dist * 6) / MISSILEMOVETICS));
        // wp.zvel = (wp.zvel*4) / MISSILEMOVETICS;
        zvel = (zvel * 6) / MISSILEMOVETICS;

        // some Weapon Animators use this
        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = zvel;

        wu.Flags |= (SPR_SET_POS_DONT_KILL);

        if (DoWeapon.animatorInvoke(Weapon)) {
            retval = true;
        }
        wu.Flags &= ~(SPR_SET_POS_DONT_KILL);

        // reset values
        wu.xchange = oldxc;
        wu.ychange = oldyc;
        wu.zchange = oldzc;
        wp.setXvel( oldvel);
        wp.setZvel( oldzvel);

        // update for interpolation
        wu.ox = wp.getX();
        wu.oy = wp.getY();
        wu.oz = wp.getZ();

        return (retval);
    }

    public static boolean DoRing(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return false;
        }

        USER uo = getUser(sp.getOwner());
        if (uo == null) {
            return false;
        }

        PlayerStr pp = Player[uo.PlayerP];
        Sprite so = boardService.getSprite(sp.getOwner());

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            int size = (sp.getYrepeat() - 1);
            sp.setXrepeat(size);
            sp.setYrepeat(size);
            if (size <= 30) {
                SpawnSmokePuff(Weapon);
                KillSprite(Weapon);
                return (true);
            }
        }

        Sprite os = boardService.getSprite(sp.getOwner());
        if (os == null) {
            return false;
        }

        // move the center with the player
        sp.setX(os.getX());
        sp.setY(os.getY());
        if (uo.PlayerP != -1) {
            sp.setZ(pp.posz + Z(20));
        } else {
            sp.setZ(SPRITEp_MID(so) + Z(30));
        }

        // go out until its time to come back in
        if (u.Counter2 == 0) {
            u.Dist += 8 * RINGMOVETICS;

            if (u.Dist > RING_OUTER_DIST) {
                u.Counter2 = 1;
            }
        } else {
            u.Dist -= 8 * RINGMOVETICS;

            if (u.Dist <= RING_INNER_DIST) {
                if (uo.PlayerP == -1) {
                    uo.Counter--;
                }
                KillSprite(Weapon);
                return false;
            }
        }

        // sp.ang = NORM_ANGLE(sp.ang - 512);

        // rotate the ring
        sp.setAng(NORM_ANGLE(sp.getAng() + (4 * RINGMOVETICS) + RINGMOVETICS));

        // put it out there
        sp.setX(sp.getX() + ((u.Dist * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512))) >> 14));
        sp.setY(sp.getY() + ((u.Dist * EngineUtils.sin(sp.getAng())) >> 14));
        if (uo.PlayerP != -1) {
            sp.setZ(sp.getZ() + ((u.Dist * ((100 - pp.getHorizi()) * HORIZ_MULT)) >> 9));
        }

        engine.setsprite(Weapon, sp.getX(), sp.getY(), sp.getZ());

        engine.getzsofslope(sp.getSectnum(), sp.getX(), sp.getY(), fz, cz);

        // bound the sprite by the sectors ceiling and floor
        if (sp.getZ() > fz.get()) {
            sp.setZ(fz.get());
        }

        if (sp.getZ() < cz.get() + SPRITEp_SIZE_Z(sp)) {
            sp.setZ(cz.get() + SPRITEp_SIZE_Z(sp));
        }

        // Done last - check for damage
        DoDamageTest(Weapon);

        return false;
    }

    public static void InitSpellRing(PlayerStr pp) {
        int ang, ang_diff, ang_start, SpriteNum, missiles;
        Sprite sp;
        USER u;
        int max_missiles = 16;
        int ammo;

        ammo = NAPALM_MIN_AMMO;

        if (pp.WpnAmmo[WPN_HOTHEAD] < ammo) {
            return;
        } else {
            PlayerUpdateAmmo(pp, WPN_HOTHEAD, -ammo);
        }

        ang_diff =  (2048 / max_missiles);

        ang_start = NORM_ANGLE(pp.getAnglei() - DIV2(2048));

        PlaySound(DIGI_RFWIZ, pp, v3df_none);

        for (missiles = 0, ang = ang_start; missiles < max_missiles; ang += ang_diff, missiles++) {
            SpriteNum =  SpawnSprite(STAT_MISSILE_SKIP4, FIREBALL1, s_Ring[0], pp.cursectnum, pp.posx, pp.posy, pp.posz, ang, 0);
            sp = boardService.getSprite(SpriteNum);
            u = getUser(SpriteNum);

            if (sp == null || u == null) {
                return;
            }

            sp.setHitag(LUMINOUS); // Always full brightness
            sp.setXvel(500);
            // sp.owner = pp.SpriteP - sprite;
            SetOwner(pp.PlayerSprite, SpriteNum);
            sp.setShade(-40);
            sp.setXrepeat(32);
            sp.setYrepeat(32);
            sp.setZvel(0);

            u.sz = Z(20);
            u.Dist = RING_INNER_DIST;
            u.Counter = max_missiles;
            u.Counter2 = 0;
            u.ceiling_dist =  Z(10);
            u.floor_dist =  Z(10);

            // put it out there
            sp.setX((int) (sp.getX() + (((long) u.Dist * (long) EngineUtils.cos(sp.getAng())) >> 14)));
            sp.setY((int) (sp.getY() + (((long) u.Dist * (long) EngineUtils.sin(sp.getAng())) >> 14)));
            sp.setZ(pp.posz + Z(20) + ((u.Dist * ((100 - pp.getHorizi()) * HORIZ_MULT)) >> 9));

            sp.setAng(NORM_ANGLE(sp.getAng() + 512));

            u.ox = sp.getX();
            u.oy = sp.getY();
            u.oz = sp.getZ();

            if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(sp)) {
                u.Flags |= (SPR_UNDERWATER);
            }
        }
    }

    private static final SHRAP[] UziBlood = {new SHRAP(s_GoreDrip, GORE_Drip, 1, Z_MID, 100, 250, 10, 20, true, 512), // 70,200
            // vels
    };

    public static void InitVulcanBoulder(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        int nx, ny, nz, nang;
        int zsize;
        int zvel, zvel_rand;
        int delta;
        int vel;

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ() - Z(40);

        if (SP_TAG7(sp) != 0) {
            delta =  SP_TAG5(sp);
            nang = sp.getAng() + (RANDOM_RANGE(delta) - DIV2(delta));
            nang = NORM_ANGLE(nang);
        } else {
            nang = RANDOM_P2(2048);
        }

        if (SP_TAG6(sp) != 0) {
            vel =  SP_TAG6(sp);
        } else {
            vel = 800;
        }

        // Spawn a shot
        int w =  SpawnSprite(STAT_MISSILE, LAVA_BOULDER, s_VulcanBoulder[0], sp.getSectnum(), nx, ny, nz, nang, (vel / 2 + vel / 4) + RANDOM_RANGE(vel / 4));
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        SetOwner(SpriteNum, w);
        int siz = (8 + RANDOM_RANGE(72));
        wp.setXrepeat(siz);
        wp.setYrepeat(siz);
        wp.setShade(-40);
        wp.setAng( nang);
        wu.Counter = 0;

        zsize = SPRITEp_SIZE_Z(wp);

        wu.Radius = 200;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setClipdist(256 >> 2);
        wu.ceiling_dist =  (zsize / 2);
        wu.floor_dist =  (zsize / 2);
        if (RANDOM_P2(1024) > 512) {
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_XFLIP));
        }
        if (RANDOM_P2(1024) > 512) {
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YFLIP));
        }

        if (SP_TAG7(sp) != 0) {
            zvel = SP_TAG7(sp);
            zvel_rand = SP_TAG8(sp);
        } else {
            zvel = 50;
            zvel_rand = 40;
        }

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = -Z(zvel) - Z(RANDOM_RANGE(zvel_rand));

    }

    private static final SHRAP[] SmallBlood = {new SHRAP(s_GoreDrip, GORE_Drip, 1, Z_TOP, 100, 250, 10, 20, true, 512)};

    private static final SHRAP[] PlasmaFountainBlood = {new SHRAP(s_PlasmaDrip, PLASMA_Drip, 1, Z_TOP, 200, 500, 100, 300, true, 16)};

    public static void InitSpellNapalm(PlayerStr pp) {
        int SpriteNum;
        Sprite sp;
        USER u;
        int i;
        int oclipdist;
        int ammo;
        Sprite psp = pp.getSprite();
        if (psp == null) {
            return;
        }

        ammo = NAPALM_MIN_AMMO;

        if (pp.WpnAmmo[WPN_HOTHEAD] < ammo) {
            return;
        } else {
            PlayerUpdateAmmo(pp, WPN_HOTHEAD, -ammo);
        }

        PlaySound(DIGI_NAPFIRE, pp, v3df_none);

        for (i = 0; i < mp.length; i++) {
            SpriteNum =  SpawnSprite(STAT_MISSILE, FIREBALL1, s_Napalm[0], pp.cursectnum, pp.posx, pp.posy, pp.posz + Z(12), pp.getAnglei(), NAPALM_VELOCITY * 2);
            sp = boardService.getSprite(SpriteNum);
            u = getUser(SpriteNum);
            if (sp == null || u == null) {
                return;
            }

            sp.setHitag(LUMINOUS); // Always full brightness

            if (i == 0) // Only attach sound to first projectile
            {
                Set3DSoundOwner(SpriteNum, PlaySound(DIGI_NAPWIZ, sp, v3df_follow));
            }

            // sp.owner = pp.SpriteP - sprite;
            SetOwner(pp.PlayerSprite, SpriteNum);
            sp.setShade(-40);
            sp.setXrepeat(32);
            sp.setYrepeat(32);
            sp.setClipdist(0);
            sp.setZvel( ((100 - pp.getHorizi()) * HORIZ_MULT));
            sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_YCENTER));
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
            u.Flags2 |= (SPR2_BLUR_TAPER_FAST);

            u.floor_dist =  Z(1);
            u.ceiling_dist =  Z(1);
            u.Dist = 200;

            oclipdist =  psp.getClipdist();
            psp.setClipdist(1);

            if (mp[i].dist_over != 0) {
                sp.setAng(NORM_ANGLE(sp.getAng() + mp[i].ang));
                HelpMissileLateral(SpriteNum, mp[i].dist_over);
                sp.setAng(NORM_ANGLE(sp.getAng() - mp[i].ang));
            }

            u.xchange = MOVEx(sp.getXvel(), sp.getAng());
            u.ychange = MOVEy(sp.getXvel(), sp.getAng());
            u.zchange = sp.getZvel();

            if (MissileSetPos(SpriteNum, DoNapalm, mp[i].dist_out)) {
                psp.setClipdist(oclipdist);
                KillSprite(SpriteNum);
                continue;
            }

            if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(sp)) {
                u.Flags |= (SPR_UNDERWATER);
            }

            psp.setClipdist(oclipdist);

            u.Counter = 0;
        }
    }

    private static final SHRAP[] SomeBlood = {new SHRAP(s_GoreDrip, GORE_Drip, 1, Z_TOP, 100, 250, 10, 20, true, 512)};

    private static final SHRAP[] ExtraBlood = {new SHRAP(s_GoreDrip, GORE_Drip, 4, Z_TOP, 100, 250, 10, 20, true, 512)};

    private static final SHRAP[] HariKariBlood = {new SHRAP(s_FastGoreDrip, GORE_Drip, 32, Z_TOP, 200, 650, 70, 100, true, 1024)};

    public static int CLOSE_RANGE_DIST_FUDGE(Sprite sp1, Sprite sp2, int fudge) {
        return (((sp1).getClipdist() << 2) + ((sp2).getClipdist() << 2) + (fudge));
    }

    public static void InitFistAttack(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite), tu;
        Sprite psp = pp.getSprite();
        if (u == null || psp == null) {
            return;
        }

        Sprite sp = null;
        int stat;
        long dist;
        int reach, face;

        PlaySound(DIGI_STAR, pp, v3df_dontpan | v3df_doppler);

        if (TEST(pp.Flags, PF_DIVING)) {
            int bubble;
            Sprite bp;
            int nx, ny;
            int random_amt;

            for (int j : dangs2) {
                bubble = SpawnBubble(pp.PlayerSprite);
                bp = boardService.getSprite(bubble);
                if (bp != null) {
                    bp.setAng(pp.getAnglei());

                    random_amt = ((RANDOM_P2(32 << 8) >> 8) - 16);

                    // back it up a bit to get it out of your face
                    nx = MOVEx((1024 + 256) * 3, NORM_ANGLE(bp.getAng() + j + random_amt));
                    ny = MOVEy((1024 + 256) * 3, NORM_ANGLE(bp.getAng() + j + random_amt));

                    move_missile(bubble, nx, ny, 0, u.ceiling_dist, u.floor_dist, CLIPMASK_PLAYER, 1);
                }
            }
        }

        for (stat = 0; stat < StatDamageList.length; stat++) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(StatDamageList[stat]); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                sp = node.get();
                USER nu = getUser(i);
                if (nu == null) {
                    continue;
                }

                if (nu.PlayerP == pp.pnum) {
                    break;
                }

                if (!TEST(sp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    continue;
                }

                dist = Distance(pp.posx, pp.posy, sp.getX(), sp.getY());

                if (pp.InventoryActive[2]) // Shadow Bombs give you demon fist
                {
                    face = 190;
                    reach = 2300;
                } else {
                    reach = 1000;
                    face = 200;
                }

                if (dist < CLOSE_RANGE_DIST_FUDGE(sp, psp, reach) && PLAYER_FACING_RANGE(pp, sp, face)) {
                    if (SpriteOverlapZ(pp.PlayerSprite, i, Z(20)) || face == 190) {
                        if (FAFcansee(sp.getX(), sp.getY(), SPRITEp_MID(sp), sp.getSectnum(), psp.getX(), psp.getY(), SPRITEp_MID(psp), psp.getSectnum())) {
                            DoDamage(i, pp.PlayerSprite);
                        }
                        if (face == 190) {
                            SpawnDemonFist(i);
                        }
                    }
                }
            }
        }

        // all this is to break glass
        {
            int hitsect, hitwall, hitsprite, daang;
            int hitx, hity, hitz, daz;

            daang = pp.getAnglei();
            daz = ((100 - pp.getHorizi()) * 2000) + (RANDOM_RANGE(24000) - 12000);

            FAFhitscan(pp.posx, pp.posy, pp.posz, pp.cursectnum, // Start position
                    EngineUtils.sin(NORM_ANGLE(daang + 512)), // X vector of 3D ang
                    EngineUtils.sin(NORM_ANGLE(daang)), // Y vector of 3D ang
                    daz, // Z vector of 3D ang
                    pHitInfo, CLIPMASK_MISSILE);

            hitsect = pHitInfo.hitsect;
            hitwall = pHitInfo.hitwall;
            hitsprite = pHitInfo.hitsprite;
            hitx = pHitInfo.hitx;
            hity = pHitInfo.hity;
            hitz = pHitInfo.hitz;

            if (hitsect == -1) {
                return;
            }

            if (FindDistance3D(pp.posx - hitx, pp.posy - hity, (pp.posz - hitz) >> 4) < 700) {
                if (hitsprite != -1) {
                    Sprite hsp = boardService.getSprite(hitsprite);
                    if (hsp == null) {
                        return;
                    }

                    tu = getUser(hitsprite);

                    if (tu != null) {
                        switch (tu.ID) {
                            case ZILLA_RUN_R0:
                            case PACHINKO1:
                            case PACHINKO2:
                            case PACHINKO3:
                            case PACHINKO4:
                            case 623:
                                SpawnSwordSparks(pp, hitsect, -1, hitx, hity, hitz, daang);
                                PlaySound(DIGI_ARMORHIT, sndCoords.set(hitx, hity, hitz), v3df_none);
                                break;
                            case TRASHCAN:
                                if (tu.WaitTics <= 0) {
                                    tu.WaitTics =  SEC(2);
                                    ChangeState(hitsprite, s_TrashCanPain[0]);
                                }
                                SpawnSwordSparks(pp, hitsect, -1, hitx, hity, hitz, daang);
                                PlaySound(DIGI_ARMORHIT, sndCoords.set(hitx, hity, hitz), v3df_none);
                                PlaySound(DIGI_TRASHLID, sp, v3df_none);
                                break;
                        }
                    }

                    if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                        if (MissileHitMatch(-1, WPN_STAR, hitsprite)) {
                            return;
                        }
                    }

                    if (TEST(hsp.getExtra(), SPRX_BREAKABLE)) {
                        HitBreakSprite(hitsprite, 0);
                    }

                    // hit a switch?
                    if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL) && (hsp.getLotag() != 0 || hsp.getHitag() != 0)) {
                        ShootableSwitch(hitsprite, -1);
                    }

                    switch (hsp.getPicnum()) {
                        case 5062:
                        case 5063:
                        case 4947:
                            SpawnSwordSparks(pp, hitsect, -1, hitx, hity, hitz, daang);
                            PlaySound(DIGI_ARMORHIT, sndCoords.set(hitx, hity, hitz), v3df_none);
                            if (RANDOM_RANGE(1000) > 700) {
                                PlayerUpdateHealth(pp, 1); // Give some health
                            }
                            hsp.setCstat(hsp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                            break;
                    }
                }

                if (hitwall != -1) {
                    Wall wph = boardService.getWall(hitwall);
                    if (wph != null) {
                        Sector nsec = boardService.getSector(wph.getNextsector());
                        if (nsec != null) {
                            if (TEST(nsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                                if (hitz < nsec.getCeilingz()) {
                                    return;
                                }
                            }
                        }

                        if (wph.getLotag() == TAG_WALL_BREAK) {
                            HitBreakWall(hitwall, hitx, hity, hitz, daang, u.ID);
                        }
                        // hit non breakable wall - do sound and puff
                        else {
                            SpawnSwordSparks(pp, hitsect, hitwall, hitx, hity, hitz, daang);
                            PlaySound(DIGI_ARMORHIT, sndCoords.set(hitx, hity, hitz), v3df_none);
                            if (PlayerTakeDamage(pp, -1)) {
                                PlayerUpdateHealth(pp, -(RANDOM_RANGE(2 << 8) >> 8));
                                PlayerCheckDeath(pp, -1);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void InitSumoNapalm(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum), wp;
        USER u = getUser(SpriteNum), wu;
        if (sp == null || u == null) {
            return;
        }

        int dist;
        int j, ang;
        int oclipdist;

        PlaySound(DIGI_NAPFIRE, sp, v3df_none);

        ang = sp.getAng();
        for (j = 0; j < 4; j++) {
            int w =  SpawnSprite(STAT_MISSILE, FIREBALL1, s_Napalm[0], sp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_TOS(sp), ang, NAPALM_VELOCITY);
            wp = boardService.getSprite(w);
            wu = getUser(w);
            if (wp == null || wu == null) {
                return;
            }

            wp.setHitag(LUMINOUS); // Always full brightness

            Set3DSoundOwner(w, PlaySound(DIGI_NAPWIZ, wp, v3df_follow));

            SetOwner(SpriteNum, w);
            wp.setShade(-40);
            wp.setXrepeat(32);
            wp.setYrepeat(32);
            wp.setClipdist(0);
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_YCENTER));
            wp.setCstat(wp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
            wu.Flags2 |= (SPR2_BLUR_TAPER_FAST);

            wu.floor_dist =  Z(1);
            wu.ceiling_dist =  Z(1);
            wu.Dist = 200;

            oclipdist =  sp.getClipdist();
            sp.setClipdist(1);

            if (mp[1].dist_over != 0) {
                wp.setAng(NORM_ANGLE(wp.getAng() + mp[1].ang));
                HelpMissileLateral(w, mp[1].dist_over);
                wp.setAng(NORM_ANGLE(wp.getAng() - mp[1].ang));
            }

            Sprite tsp = boardService.getSprite(u.tgt_sp);
            // find the distance to the target (player)
            if (tsp != null) {
                dist = Distance(wp.getX(), wp.getY(), tsp.getX(), tsp.getY());
                if (dist != 0) {
                    wp.setZvel(((wp.getXvel() * (SPRITEp_UPPER(tsp) - wp.getZ())) / dist));
                }
            }

            wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
            wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
            wu.zchange = wp.getZvel();

            MissileSetPos(w, DoNapalm, mp[1].dist_out);

            sp.setClipdist(oclipdist);

            u.Counter = 0;
            ang += 512;
        }
    }

    public static void InitSumoSkull(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum), nu;
        if (sp == null || u == null) {
            return;
        }

        Sprite np;
        int newsp;

        PlaySound(DIGI_SERPSUMMONHEADS, sp, v3df_none);

        newsp =  SpawnSprite(STAT_ENEMY, SKULL_R0, s_SkullWait[0][0], sp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_MID(sp), sp.getAng(), 0);
        np = boardService.getSprite(newsp);
        nu = getUser(newsp);
        if (np == null || nu == null) {
            return;
        }


        np.setXvel(500);
        SetOwner(SpriteNum, newsp);
        np.setShade(-20);
        np.setXrepeat(64);
        np.setYrepeat(64);
        np.setPal(0);

        // randomize the head turning angle
        np.setAng( (RANDOM_P2(2048 << 5) >> 5));

        // control direction of spinning
        u.Flags ^= (SPR_BOUNCE);
        nu.Flags |= (DTEST(u.Flags, SPR_BOUNCE));

        nu.StateEnd = s_SkullExplode[0];
        nu.setRot(Skull.sg_SkullWait);

        nu.Attrib = SkullAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        nu.Counter =  RANDOM_P2(2048);
        nu.sz = np.getZ();
        nu.Health = 100;

        // defaults do change the statnum
        EnemyDefaults(newsp, null, null);
        // change_sprite_stat(new, STAT_SKIP4);
        np.setExtra(np.getExtra() | (SPRX_PLAYER_OR_ENEMY));

        np.setClipdist((128 + 64) >> 2);
        nu.Flags |= (SPR_XFLIP_TOGGLE);
        np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));

        nu.Radius = 400;
    }

    public static void InitSumoStompAttack(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum), tsp;
        if (sp == null) {
            return;
        }

        int stat;
        long dist;
        int reach;

        PlaySound(DIGI_30MMEXPLODE, sp, v3df_dontpan | v3df_doppler);

        for (stat = 0; stat < StatDamageList.length; stat++) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(StatDamageList[stat]); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                tsp = node.get();

                if (u != null && i != u.tgt_sp) {
                    break;
                }

                if (!TEST(tsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    continue;
                }

                dist = Distance(sp.getX(), sp.getY(), tsp.getX(), tsp.getY());

                reach = 16384;

                if (dist < CLOSE_RANGE_DIST_FUDGE(tsp, sp, reach)) {
                    if (FAFcansee(tsp.getX(), tsp.getY(), SPRITEp_MID(tsp), tsp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_MID(sp), sp.getSectnum())) {
                        DoDamage(i, SpriteNum);
                    }
                }
            }
        }
    }

    public static void InitMiniSumoClap(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }

        int dist = Distance(sp.getX(), sp.getY(), tsp.getX(), tsp.getY());

        int reach = 10000;

        if (dist < CLOSE_RANGE_DIST_FUDGE(tsp, sp, 1000)) {
            if (SpriteOverlapZ(SpriteNum, u.tgt_sp, Z(20))) {
                if (FAFcansee(tsp.getX(), tsp.getY(), SPRITEp_MID(tsp), tsp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_MID(sp), sp.getSectnum())) {
                    PlaySound(DIGI_CGTHIGHBONE, sp, v3df_follow | v3df_dontpan);
                    DoDamage(u.tgt_sp, SpriteNum);
                }
            }
        } else if (dist < CLOSE_RANGE_DIST_FUDGE(tsp, sp, reach)) {
            if (FAFcansee(tsp.getX(), tsp.getY(), SPRITEp_MID(tsp), tsp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_MID(sp), sp.getSectnum())) {
                PlaySound(DIGI_30MMEXPLODE, sp, v3df_none);
                SpawnFireballFlames(SpriteNum, u.tgt_sp);
            }
        }
    }

    public static int WeaponAutoAim(int spnum, int Missile, int ang, int test) {
        USER wu = getUser(Missile);
        USER u = getUser(spnum);
        Sprite wp = boardService.getSprite(Missile);
        int hitsprite;
        int dist;
        int zh;

        if (u != null && u.PlayerP != -1) {
            if (!TEST(Player[u.PlayerP].Flags, PF_AUTO_AIM)) {
                return (-1);
            }
        }

        if (wp == null || wu == null) {
            return -1;
        }

        if ((hitsprite = DoPickTarget(spnum, ang, test)) != -1) {
            Sprite hp = boardService.getSprite(hitsprite);
            USER hu = getUser(hitsprite);
            if (hp == null || hu == null) {
                return -1;
            }

            wu.WpnGoal =  hitsprite;
            hu.Flags |= (SPR_TARGETED);
            hu.Flags |= (SPR_ATTACKED);

            wp.setAng(NORM_ANGLE(EngineUtils.getAngle(hp.getX() - wp.getX(), hp.getY() - wp.getY())));
            dist = FindDistance2D(wp.getX() - hp.getX(), wp.getY() - hp.getY());

            if (dist != 0) {
                int tos, diff, siz;

                tos = SPRITEp_TOS(hp);
                diff = wp.getZ() - tos;
                siz = SPRITEp_SIZE_Z(hp);

                // hitsprite is below
                if (diff < -Z(50)) {
                    zh = tos + DIV2(siz);
                } else
                    // hitsprite is above
                    if (diff > Z(50)) {
                        zh = tos + DIV8(siz);
                    } else {
                        zh = tos + DIV4(siz);
                    }

                wp.setZvel( ((wp.getXvel() * (zh - wp.getZ())) / dist));
            }
        }

        return (hitsprite);
    }

    public static int WeaponAutoAimZvel(int spnum, int Missile, LONGp zvel, int ang, int test) {
        USER wu = getUser(Missile);
        USER u = getUser(spnum);
        Sprite wp = boardService.getSprite(Missile);
        int hitsprite;
        int dist;
        int zh;

        if (u != null && u.PlayerP != -1) {
            if (!TEST(Player[u.PlayerP].Flags, PF_AUTO_AIM)) {
                return (-1);
            }
        }

        if (wp == null || wu == null) {
            return -1;
        }

        if ((hitsprite =  DoPickTarget(spnum, ang, test)) != -1) {
            Sprite hp = boardService.getSprite(hitsprite);
            USER hu = getUser(hitsprite);
            if (hp == null || hu == null) {
                return -1;
            }

            wu.WpnGoal = hitsprite;
            hu.Flags |= (SPR_TARGETED);
            hu.Flags |= (SPR_ATTACKED);

            wp.setAng(NORM_ANGLE(EngineUtils.getAngle(hp.getX() - wp.getX(), hp.getY() - wp.getY())));
            // dist = FindDistance2D(wp.x, wp.y, hp.x, hp.y);
            dist = FindDistance2D(wp.getX() - hp.getX(), wp.getY() - hp.getY());

            if (dist != 0) {

                int tos, diff, siz;

                tos = SPRITEp_TOS(hp);
                diff = wp.getZ() - tos;
                siz = SPRITEp_SIZE_Z(hp);

                // hitsprite is below
                if (diff < -Z(50)) {
                    zh = tos + DIV2(siz);
                } else
                    // hitsprite is above
                    if (diff > Z(50)) {
                        zh = tos + DIV8(siz);
                    } else {
                        zh = tos + DIV4(siz);
                    }

                zvel.value = (wp.getXvel() * (zh - wp.getZ())) / dist;
            }
        }

        return (hitsprite);
    }

    public static int AimHitscanToTarget(int spnum, LONGp z, LONGp ang, int z_ratio) {
        Sprite sp = boardService.getSprite(spnum);
        USER u = getUser(spnum);
        if (sp == null || u == null) {
            return 0;
        }

        int hitsprite;
        int dist;
        int zh;
        int xvect;
        int yvect;
        Sprite hp;
        USER hu;

        if (u.tgt_sp == -1) {
            return (-1);
        }

        hitsprite =  u.tgt_sp;
        hp = boardService.getSprite(hitsprite);
        hu = getUser(hitsprite);
        if (hp == null || hu == null) {
            return -1;
        }

        hu.Flags |= (SPR_TARGETED);
        hu.Flags |= (SPR_ATTACKED);

        // Global set by DoPickTarget
        ang.value = EngineUtils.getAngle(hp.getX() - sp.getX(), hp.getY() - sp.getY());

        // find the distance to the target
        dist = EngineUtils.sqrt(SQ(sp.getX() - hp.getX()) + SQ(sp.getY() - hp.getY()));

        if (dist != 0) {
            zh = SPRITEp_UPPER(hp);

            xvect = EngineUtils.sin(NORM_ANGLE(ang.value + 512));
            yvect = EngineUtils.sin(NORM_ANGLE(ang.value));

            if (hp.getX() - sp.getX() != 0)
            // *z = xvect * ((zh - *z)/(hp.x - sp.x));
            {
                z.value = scale(xvect, zh - z.value, hp.getX() - sp.getX());
            } else if (hp.getY() - sp.getY() != 0)
            // *z = yvect * ((zh - *z)/(hp.y - sp.y));
            {
                z.value = scale(yvect, zh - z.value, hp.getY() - sp.getY());
            } else {
                z.value = 0;
            }

            // so actors won't shoot straight up at you
            // need to be a bit of a distance away
            // before they have a valid shot
            if (klabs(z.value / dist) > z_ratio) {
                return (-1);
            }
        }

        return (hitsprite);
    }

    public static int WeaponAutoAimHitscan(int spnum, LONGp z, LONGp ang, int test) {
        Sprite sp = boardService.getSprite(spnum);
        if (sp == null) {
            return -1;
        }

        USER u = getUser(spnum);
        int hitsprite;
        int dist;
        int zh;
        int xvect;
        int yvect;

        if (u != null && u.PlayerP != -1) {
            if (!TEST(Player[u.PlayerP].Flags, PF_AUTO_AIM)) {
                return (-1);
            }
        }

        if ((hitsprite =  DoPickTarget(spnum, ang.value, test)) != -1) {
            Sprite hp = boardService.getSprite(hitsprite);
            USER hu = getUser(hitsprite);
            if (hp == null || hu == null) {
                return -1;
            }

            hu.Flags |= (SPR_TARGETED);
            hu.Flags |= (SPR_ATTACKED);

            // Global set by DoPickTarget
            // *ang = target_ang;
            ang.value = NORM_ANGLE(EngineUtils.getAngle(hp.getX() - sp.getX(), hp.getY() - sp.getY()));

            // find the distance to the target
            dist = EngineUtils.sqrt(SQ(sp.getX() - hp.getX()) + SQ(sp.getY() - hp.getY()));

            if (dist != 0) {
                zh = SPRITEp_TOS(hp) + DIV4(SPRITEp_SIZE_Z(hp));

                xvect = EngineUtils.sin(NORM_ANGLE(ang.value + 512));
                yvect = EngineUtils.sin(NORM_ANGLE(ang.value));

                if (hp.getX() - sp.getX() != 0)
                // *z = xvect * ((zh - *z)/(hp.x - sp.x));
                {
                    z.value = scale(xvect, zh - z.value, hp.getX() - sp.getX());
                } else if (hp.getY() - sp.getY() != 0)
                // *z = yvect * ((zh - *z)/(hp.y - sp.y));
                {
                    z.value = scale(yvect, zh - z.value, hp.getY() - sp.getY());
                } else {
                    z.value = 0;
                }
            }
        }

        return (hitsprite);
    }

    public static void WeaponHitscanShootFeet(Sprite sp, Sprite hp, LONGp zvect) {
        int dist;
        int zh;
        int xvect;
        int yvect;
        int z;
        int ang;

        // Global set by DoPickTarget
        // *ang = target_ang;
        ang = NORM_ANGLE(EngineUtils.getAngle(hp.getX() - sp.getX(), hp.getY() - sp.getY()));

        // find the distance to the target
        dist = EngineUtils.sqrt(SQ(sp.getX() - hp.getX()) + SQ(sp.getY() - hp.getY()));

        if (dist != 0) {
            zh = SPRITEp_BOS(hp) + Z(20);
            z = sp.getZ();

            xvect = EngineUtils.sin(NORM_ANGLE(ang + 512));
            yvect = EngineUtils.sin(NORM_ANGLE(ang));

            if (hp.getX() - sp.getX() != 0) {
                zvect.value = scale(xvect, zh - z, hp.getX() - sp.getX());
            } else if (hp.getY() - sp.getY() != 0) {
                zvect.value = scale(yvect, zh - z, hp.getY() - sp.getY());
            } else {
                zvect.value = 0;
            }
        }
    }

    public static void InitStar(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz;
        int w;
        int zvel;
        int i;
        Sprite np;
        USER nu;
        int nw;

        PlayerUpdateAmmo(pp, u.WeaponNum, -3);

        nx = pp.posx;
        ny = pp.posy;

        nz = pp.posz + pp.bob_z + Z(8);

        PlaySound(DIGI_STAR, pp, v3df_dontpan | v3df_doppler);

        // Spawn a shot
        // Inserting and setting up variables

        w =  SpawnSprite(STAT_MISSILE, STAR1, s_Star[0], pp.cursectnum, nx, ny, nz, pp.getAnglei(), STAR_VELOCITY);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(STAR_REPEAT);
        wp.setXrepeat(STAR_REPEAT);
        wp.setShade(-25);
        wp.setClipdist(32 >> 2);
        // wp.zvel was overflowing with this calculation - had to move to a local
        // long var
        zvel = ((100 - pp.getHorizi()) * (HORIZ_MULT + STAR_HORIZ_ADJ));

        wu.ceiling_dist =  Z(1);
        wu.floor_dist =  Z(1);
        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 100;
        wu.Counter = 0;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        // zvel had to be tweaked alot for this weapon
        // MissileSetPos seemed to be pushing the sprite too far up or down when
        // the horizon was tilted. Never figured out why.
        wp.setZvel( (zvel >> 1));
        if (MissileSetPos(w, DoStar, 1000)) {
            KillSprite(w);
            return;
        }

        if (WeaponAutoAim(pp.PlayerSprite, w, 32, 0) != -1) {
            zvel = wp.getZvel();
        }

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = zvel;

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        wu.ox = wp.getX();
        wu.oy = wp.getY();
        wu.oz = wp.getZ();

        for (i = 0; i < dang3.length; i++) {
            nw =  SpawnSprite(STAT_MISSILE, STAR1, s_Star[0], pp.cursectnum, nx, ny, nz, NORM_ANGLE(wp.getAng() + dang3[i]), wp.getXvel());
            np = boardService.getSprite(nw);
            nu = getUser(nw);
            if (np == null || nu == null) {
                return;
            }

            SetOwner(wp.getOwner(), nw);
            np.setYrepeat(STAR_REPEAT);
            np.setXrepeat(STAR_REPEAT);
            np.setShade(wp.getShade());

            np.setExtra(wp.getExtra());
            np.setClipdist(wp.getClipdist());
            nu.WeaponNum = wu.WeaponNum;
            nu.Radius = wu.Radius;
            nu.ceiling_dist = wu.ceiling_dist;
            nu.floor_dist = wu.floor_dist;
            nu.Flags2 = wu.Flags2;

            if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(np)) {
                nu.Flags |= (SPR_UNDERWATER);
            }

            zvel = ((100 - pp.getHorizi()) * (HORIZ_MULT + STAR_HORIZ_ADJ));
            np.setZvel( (zvel >> 1));

            if (MissileSetPos(nw, DoStar, 1000)) {
                KillSprite(nw);
                return;
            }

            // move the same as middle star
            zvel = wu.zchange;

            nu.xchange = MOVEx(np.getXvel(), np.getAng());
            nu.ychange = MOVEy(np.getXvel(), np.getAng());
            nu.zchange = zvel;

            nu.ox = np.getX();
            nu.oy = np.getY();
            nu.oz = np.getZ();
        }
    }

    public static void InitHeartAttack(PlayerStr pp) {
        int SpriteNum;
        Sprite sp;
        USER u;
        int i = 1;
        int oclipdist;

        PlayerUpdateAmmo(pp, WPN_HEART, -1);

        SpriteNum =  SpawnSprite(STAT_MISSILE_SKIP4, BLOOD_WORM, s_BloodWorm[0], pp.cursectnum, pp.posx, pp.posy, pp.posz + Z(12), pp.getAnglei(), BLOOD_WORM_VELOCITY * 2);
        sp = boardService.getSprite(SpriteNum);
        u = getUser(SpriteNum);
        Sprite psp = pp.getSprite();
        if (sp == null || u == null || psp == null) {
            return;
        }

        sp.setHitag(LUMINOUS); // Always full brightness

        SetOwner(pp.PlayerSprite, SpriteNum);
        sp.setShade(-10);
        sp.setXrepeat(52);
        sp.setYrepeat(52);
        sp.setClipdist(0);
        sp.setZvel( ((100 - pp.getHorizi()) * HORIZ_MULT));
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        u.Flags2 |= (SPR2_DONT_TARGET_OWNER);
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));

        u.floor_dist =  Z(1);
        u.ceiling_dist =  Z(1);
        u.Dist = 200;

        oclipdist =  psp.getClipdist();
        psp.setClipdist(1);

        u.xchange = MOVEx(sp.getXvel(), sp.getAng());
        u.ychange = MOVEy(sp.getXvel(), sp.getAng());
        u.zchange = sp.getZvel();

        MissileSetPos(SpriteNum, DoBloodWorm, mp[i].dist_out);

        psp.setClipdist(oclipdist);
        u.Counter = 0;
        u.Counter2 = 0;
        u.Counter3 = 0;
        u.WaitTics = 0;
    }

    public static void ContinueHitscan(PlayerStr pp, int sectnum, int x, int y, int z, int ang, int xvect, int yvect, int zvect) {
        int j;
        int hitsect, hitwall, hitsprite;
        int hitx, hity, hitz;
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        FAFhitscan(x, y, z, sectnum, xvect, yvect, zvect, pHitInfo, CLIPMASK_MISSILE);

        hitsect = pHitInfo.hitsect;
        hitwall = pHitInfo.hitwall;
        hitsprite = pHitInfo.hitsprite;
        hitx = pHitInfo.hitx;
        hity = pHitInfo.hity;
        hitz = pHitInfo.hitz;

        if (hitsect == -1) {
            return;
        }

        if (hitsprite == -1 && hitwall == -1) {
            Sector hsec = boardService.getSector(hitsect);
            if (hsec != null && klabs(hitz - hsec.getCeilingz()) <= Z(1)) {
                hitz += Z(16);
                if (TEST(hsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                    return;
                }
            }
        }

        if (hitwall != -1) {
            Wall wph = boardService.getWall(hitwall);
            if (wph != null) {
                Sector nsec = boardService.getSector(wph.getNextsector());
                if (nsec != null) {
                    if (TEST(nsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                        if (hitz < nsec.getCeilingz()) {
                            return;
                        }
                    }
                }

                if (wph.getLotag() == TAG_WALL_BREAK) {
                    HitBreakWall(hitwall, hitx, hity, hitz, ang, u.ID);
                    return;
                }

                QueueHole(ang, hitsect, hitwall, hitx, hity, hitz);
            }
        }

        // hit a sprite?
        if (hitsprite != -1) {
            Sprite hsp = boardService.getSprite(hitsprite);
            if (hsp == null) {
                return;
            }

            if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                if (MissileHitMatch(-1, WPN_SHOTGUN, hitsprite)) {
                    return;
                }
            }

            if (TEST(hsp.getExtra(), SPRX_BREAKABLE)) {
                HitBreakSprite(hitsprite, 0);
                return;
            }

            if (BulletHitSprite(pp.PlayerSprite, hitsprite, hitsect, hitwall, hitx, hity, hitz, 0)) {
                return;
            }

            // hit a switch?
            if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL) && (hsp.getLotag() != 0 || hsp.getHitag() != 0)) {
                ShootableSwitch(hitsprite, -1);
            }
        }

        j =  SpawnShotgunSparks(pp, hitsect, hitwall, hitx, hity, hitz, ang);
        DoHitscanDamage(j, hitsprite);

    }

    public static void InitShotgun(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        Sprite hsp;
        int daang, ndaang, i, j;
        int hitsect, hitwall, hitsprite, nsect;
        int hitx, hity, hitz, daz, ndaz;
        int nx, ny, nz;
        int xvect, yvect, zvect;
        Sprite sp;

        PlayerUpdateAmmo(pp, u.WeaponNum, -1);

        PlaySound(DIGI_RIOTFIRE2, pp, v3df_dontpan | v3df_doppler);

        // Make sprite shade brighter
        u.Vis = 128;

        if (pp.WpnShotgunAuto != 0) {
            if (pp.WpnShotgunType == 1) {
                pp.WpnShotgunAuto--;
            }
        }

        nx = pp.posx;
        ny = pp.posy;
        daz = nz = pp.posz + pp.bob_z;
        nsect = pp.cursectnum;
        sp = pp.getSprite();

        daang = 64;
        if (WeaponAutoAimHitscan(pp.PlayerSprite, tmp_ptr[0].set(daz), tmp_ptr[1].set(daang), 0) != -1) {
            daz = tmp_ptr[0].value;
            daang =  tmp_ptr[1].value;
        } else {
            daz = (100 - pp.getHorizi()) * 2000;
            daang = pp.getAnglei();
        }

        for (i = 0; i < 12; i++) {
            if (pp.WpnShotgunType == 0) {
                ndaz = daz + (RANDOM_RANGE(Z(120)) - Z(45));
                ndaang = NORM_ANGLE(daang + (RANDOM_RANGE(30) - 15));
            } else {
                ndaz = daz + (RANDOM_RANGE(Z(200)) - Z(65));
                ndaang = NORM_ANGLE(daang + (RANDOM_RANGE(70) - 30));
            }

            xvect = EngineUtils.sin(NORM_ANGLE(ndaang + 512));
            yvect = EngineUtils.sin(NORM_ANGLE(ndaang));
            zvect = ndaz;
            FAFhitscan(nx, ny, nz, nsect, // Start position
                    xvect, yvect, zvect, pHitInfo, CLIPMASK_MISSILE);

            hitsect = pHitInfo.hitsect;
            hitwall = pHitInfo.hitwall;
            hitsprite = pHitInfo.hitsprite;
            hitx = pHitInfo.hitx;
            hity = pHitInfo.hity;
            hitz = pHitInfo.hitz;

            if (hitsect == -1) {
                continue;
            }

            if (hitsprite == -1 && hitwall == -1) {
                Sector hsec = boardService.getSector(hitsect);
                if (hsec != null) {
                    if (klabs(hitz - hsec.getCeilingz()) <= Z(1)) {
                        hitz += Z(16);

                        if (TEST(hsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                            continue;
                        }

                        if (SectorIsUnderwaterArea(hitsect)) {
                            WarpToSurface(tmp_ptr[0].set(hitsect), tmp_ptr[1].set(hitx), tmp_ptr[2].set(hity), tmp_ptr[3].set(hitz));
                            hitsect = tmp_ptr[0].value;
                            hitx = tmp_ptr[1].value;
                            hity = tmp_ptr[2].value;
                            hitz = tmp_ptr[3].value;
                            ContinueHitscan(pp, hitsect, hitx, hity, hitz, ndaang, xvect, yvect, zvect);
                            continue;
                        }
                    } else if (klabs(hitz - hsec.getFloorz()) <= Z(1)) {
                        if (DTEST(hsec.getExtra(), SECTFX_LIQUID_MASK) != SECTFX_LIQUID_NONE) {
                            SpawnSplashXY(hitx, hity, hitz, hitsect);

                            if (SectorIsDiveArea(hitsect)) {
                                WarpToUnderwater(tmp_ptr[0].set(hitsect), tmp_ptr[1].set(hitx), tmp_ptr[2].set(hity), tmp_ptr[3].set(hitz));
                                hitsect = tmp_ptr[0].value;
                                hitx = tmp_ptr[1].value;
                                hity = tmp_ptr[2].value;
                                hitz = tmp_ptr[3].value;
                                ContinueHitscan(pp, hitsect, hitx, hity, hitz, ndaang, xvect, yvect, zvect);
                            }

                            continue;
                        }
                    }
                }
            }

            if (hitwall != -1) {
                Wall wph = boardService.getWall(hitwall);
                if (wph != null) {
                    Sector nsec = boardService.getSector(wph.getNextsector());
                    if (nsec != null) {
                        if (TEST(nsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                            if (hitz < nsec.getCeilingz()) {
                                continue;
                            }
                        }
                    }

                    if (wph.getLotag() == TAG_WALL_BREAK) {
                        HitBreakWall(hitwall, hitx, hity, hitz, ndaang, u.ID);
                        continue;
                    }

                    QueueHole(ndaang, hitsect, hitwall, hitx, hity, hitz);
                }
            }

            // hit a sprite?
            if (hitsprite != -1) {
                hsp = boardService.getSprite(hitsprite);
                USER hu = getUser(hitsprite);
                if (hsp == null) {
                    continue;
                }

                if (hu != null && hu.ID == TRASHCAN) {
                    PlaySound(DIGI_TRASHLID, sp, v3df_none);
                    if (hu.WaitTics <= 0) {
                        hu.WaitTics =  SEC(2);
                        ChangeState(hitsprite, s_TrashCanPain[0]);
                    }
                }

                if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                    if (MissileHitMatch(-1, WPN_SHOTGUN, hitsprite)) {
                        continue;
                    }
                }

                if (TEST(hsp.getExtra(), SPRX_BREAKABLE)) {
                    HitBreakSprite(hitsprite, 0);
                    continue;
                }

                if (BulletHitSprite(pp.PlayerSprite, hitsprite, hitsect, hitwall, hitx, hity, hitz, SHOTGUN_SMOKE)) {
                    continue;
                }

                // hit a switch?
                if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL) && (hsp.getLotag() != 0 || hsp.getHitag() != 0)) {
                    ShootableSwitch(hitsprite, -1);
                }
            }

            j =  SpawnShotgunSparks(pp, hitsect, hitwall, hitx, hity, hitz, ndaang);
            DoHitscanDamage(j, hitsprite);
        }

        DoPlayerBeginRecoil(pp, SHOTGUN_RECOIL_AMT);
    }

    public static void InitRail(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz;
        int w;
        int oclipdist;
        int zvel;

        DoPlayerBeginRecoil(pp, RAIL_RECOIL_AMT);

        PlayerUpdateAmmo(pp, u.WeaponNum, -1);

        PlaySound(DIGI_RAILFIRE, pp, v3df_dontpan | v3df_doppler);

        // Make sprite shade brighter
        u.Vis = 128;

        nx = pp.posx;
        ny = pp.posy;

        nz = pp.posz + pp.bob_z + Z(11);

        // Spawn a shot
        // Inserting and setting up variables

        w =  SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R1, s_Rail[0][0], pp.cursectnum, nx, ny, nz, pp.getAnglei(), 1200);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        Sprite psp = pp.getSprite();

        if (wp == null || wu == null || psp == null) {
            return;
        }

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(52);
        wp.setXrepeat(52);
        wp.setShade(-15);
        zvel = ((100 - pp.getHorizi()) * (HORIZ_MULT + 17));

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_Rail);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = RAIL_RADIUS;
        wu.ceiling_dist =  Z(1);
        wu.floor_dist =  Z(1);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER | CSTAT_SPRITE_INVISIBLE));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        // at certain angles the clipping box was big enough to block the
        // initial positioning

        oclipdist = psp.getClipdist();
        psp.setClipdist(0);
        wp.setClipdist(32 >> 2);

        wp.setAng(NORM_ANGLE(wp.getAng() + 512));
        HelpMissileLateral(w, 700);
        wp.setAng(NORM_ANGLE(wp.getAng() - 512));

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        if (TestMissileSetPos(w, DoRailStart, 1200, zvel)) {
            psp.setClipdist(oclipdist);
            KillSprite(w);
            return;
        }

        psp.setClipdist(oclipdist);

        wp.setZvel( (zvel >> 1));
        if (WeaponAutoAim(pp.PlayerSprite, w, 32, 0) == -1) {
            wp.setAng(NORM_ANGLE(wp.getAng() - 4));
        } else {
            zvel = wp.getZvel(); // Let autoaiming set zvel now
        }

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = zvel;
    }

    public static void InitRocket(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz;
        int w;
        int oclipdist;
        int zvel;

        DoPlayerBeginRecoil(pp, ROCKET_RECOIL_AMT);

        PlayerUpdateAmmo(pp, u.WeaponNum, -1);

        PlaySound(DIGI_RIOTFIRE, pp, v3df_dontpan | v3df_doppler);

        // Make sprite shade brighter
        u.Vis = 128;

        nx = pp.posx;
        ny = pp.posy;

        // Spawn a shot
        // Inserting and setting up variables
        nz = pp.posz + pp.bob_z + Z(8);
        w =  SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R0, s_Rocket[0][0], pp.cursectnum, nx, ny, nz, pp.getAnglei(), ROCKET_VELOCITY);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        Sprite psp = pp.getSprite();

        if (wp == null || wu == null || psp == null) {
            return;
        }

        // wp.owner = pp.PlayerSprite;
        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(90);
        wp.setXrepeat(90);
        wp.setShade(-15);
        zvel = ((100 - pp.getHorizi()) * (HORIZ_MULT + 35));

        wp.setClipdist(64 >> 2);

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_Rocket);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 2000;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        // Set default palette
        wp.setPal(wu.spal = 17); // White

        if (pp.WpnRocketHeat != 0) {
            if (pp.WpnRocketType == 1) {
                pp.WpnRocketHeat--;
                wu.Flags |= (SPR_FIND_PLAYER);
                wp.setPal(wu.spal = 20); // Yellow
            }
        }

        // at certain angles the clipping box was big enough to block the
        // initial positioning of the fireball.

        oclipdist =  psp.getClipdist();
        psp.setClipdist(0);

        wp.setAng(NORM_ANGLE(wp.getAng() + 512));
        HelpMissileLateral(w, 900);
        wp.setAng(NORM_ANGLE(wp.getAng() - 512));

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        // cancel smoke trail
        wu.Counter = 1;
        if (TestMissileSetPos(w, DoRocket, 1200, zvel)) {
            psp.setClipdist(oclipdist);
            KillSprite(w);
            return;
        }
        // inable smoke trail
        wu.Counter = 0;

        psp.setClipdist(oclipdist);

        wp.setZvel( (zvel >> 1));
        if (WeaponAutoAim(pp.PlayerSprite, w, 32, 0) == -1) {
            wp.setAng(NORM_ANGLE(wp.getAng() - 5));
        } else {
            zvel = wp.getZvel(); // Let autoaiming set zvel now
        }

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = zvel;
    }

    public static void InitBunnyRocket(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sprite psp = pp.getSprite();
        if (psp == null || u == null) {
            return;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz;
        int w;
        int oclipdist;
        int zvel;

        DoPlayerBeginRecoil(pp, ROCKET_RECOIL_AMT);

        PlayerUpdateAmmo(pp, u.WeaponNum, -1);

        PlaySound(DIGI_BUNNYATTACK, pp, v3df_dontpan | v3df_doppler);

        nx = pp.posx;
        ny = pp.posy;

        // Spawn a shot
        // Inserting and setting up variables

        nz = pp.posz + pp.bob_z + Z(8);
        w =  SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R4, s_BunnyRocket[0][0], pp.cursectnum, nx, ny, nz, pp.getAnglei(), ROCKET_VELOCITY);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        // wp.owner = pp.PlayerSprite;
        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(64);
        wp.setXrepeat(64);
        wp.setShade(-15);
        zvel = ((100 - pp.getHorizi()) * (HORIZ_MULT + 35));

        wp.setClipdist(64 >> 2);

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_BunnyRocket);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 2000;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        wu.Flags |= (SPR_XFLIP_TOGGLE);

        if (pp.WpnRocketHeat != 0) {
            if (pp.WpnRocketType == 1) {
                pp.WpnRocketHeat--;
                wu.Flags |= (SPR_FIND_PLAYER);
            }
        }

        // at certain angles the clipping box was big enough to block the
        // initial positioning of the fireball.
        oclipdist =  psp.getClipdist();
        psp.setClipdist(0);

        wp.setAng(NORM_ANGLE(wp.getAng() + 512));
        HelpMissileLateral(w, 900);
        wp.setAng(NORM_ANGLE(wp.getAng() - 512));

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        // cancel smoke trail
        wu.Counter = 1;
        if (TestMissileSetPos(w, DoRocket, 1200, zvel)) {
            psp.setClipdist(oclipdist);
            KillSprite(w);
            return;
        }
        // inable smoke trail
        wu.Counter = 0;

        psp.setClipdist(oclipdist);

        wp.setZvel( (zvel >> 1));
        if (WeaponAutoAim(pp.PlayerSprite, w, 32, 0) == -1) {
            wp.setAng(NORM_ANGLE(wp.getAng() - 5));
        } else {
            zvel = wp.getZvel(); // Let autoaiming set zvel now
        }

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = zvel;
        wu.spal = PALETTE_PLAYER1;
        wp.setPal(PALETTE_PLAYER1);
    }

    public static void InitNuke(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sprite psp = pp.getSprite();
        if (u == null || psp == null) {
            return;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz;
        int w;
        int oclipdist;
        int zvel;

        if (pp.WpnRocketNuke > 0) {
            pp.WpnRocketNuke = 0; // Bye Bye little nukie.
        } else {
            return;
        }

        DoPlayerBeginRecoil(pp, ROCKET_RECOIL_AMT * 12);

        PlaySound(DIGI_RIOTFIRE, pp, v3df_dontpan | v3df_doppler);

        // Make sprite shade brighter
        u.Vis = 128;

        nx = pp.posx;
        ny = pp.posy;

        // Spawn a shot
        // Inserting and setting up variables
        // nz = pp.posz + pp.bob_z + Z(12);
        nz = pp.posz + pp.bob_z + Z(8);
        w =  SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R0, s_Rocket[0][0], pp.cursectnum, nx, ny, nz, pp.getAnglei(), 700);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        // wp.owner = pp.PlayerSprite;
        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(128);
        wp.setXrepeat(128);
        wp.setShade(-15);
        zvel = ((100 - pp.getHorizi()) * (HORIZ_MULT - 36));
        wp.setClipdist(64 >> 2);

        // Set to red palette
        wp.setPal(wu.spal = 19);

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_Rocket);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = NUKE_RADIUS;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        // at certain angles the clipping box was big enough to block the
        // initial positioning of the fireball.
        oclipdist =  psp.getClipdist();
        psp.setClipdist(0);

        wp.setAng(NORM_ANGLE(wp.getAng() + 512));
        HelpMissileLateral(w, 900);
        wp.setAng(NORM_ANGLE(wp.getAng() - 512));

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        // cancel smoke trail
        wu.Counter = 1;
        if (TestMissileSetPos(w, DoRocket, 1200, zvel)) {
            psp.setClipdist(oclipdist);
            KillSprite(w);
            return;
        }
        // inable smoke trail
        wu.Counter = 0;

        psp.setClipdist(oclipdist);

        wp.setZvel( (zvel >> 1));
        if (WeaponAutoAim(pp.PlayerSprite, w, 32, 0) == -1) {
            wp.setAng(NORM_ANGLE(wp.getAng() - 5));
        } else {
            zvel = wp.getZvel(); // Let autoaiming set zvel now
        }

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = zvel;

        PlayerDamageSlide(pp, -40, NORM_ANGLE(pp.getAnglei() + 1024)); // Recoil slide
    }

    public static boolean WallSpriteInsideSprite(Sprite wsp, Sprite sp) {

        int x1, y1, x2, y2;
        int xoff;
        int dax, day;
        int xsiz, mid_dist;

        x1 = wsp.getX();
        y1 = wsp.getY();

        xoff = TILE_XOFF(wsp.getPicnum()) + wsp.getXoffset();

        if (TEST(wsp.getCstat(), CSTAT_SPRITE_XFLIP)) {
            xoff = -xoff;
        }

        // x delta
        dax = EngineUtils.sin(wsp.getAng()) * wsp.getXrepeat();
        // y delta
        day = EngineUtils.sin(NORM_ANGLE(wsp.getAng() + 1024 + 512)) * wsp.getXrepeat();

        xsiz = engine.getTile(wsp.getPicnum()).getWidth();
        mid_dist = DIV2(xsiz) + xoff;

        // starting from the center find the first point
        x1 -= mulscale(dax, mid_dist, 16);
        // starting from the first point find the end point
        x2 = x1 + mulscale(dax, xsiz, 16);

        y1 -= mulscale(day, mid_dist, 16);
        y2 = y1 + mulscale(day, xsiz, 16);

        return (engine.clipinsideboxline(sp.getX(), sp.getY(), x1, y1, x2, y2, (sp.getClipdist()) << 2) != 0);
    }

    public static void DoBladeDamage(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        int dist;

        ListNode<Sprite> nexti;
        for (int j : StatDamageList) {
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite hp = node.get();

                if (i == SpriteNum) {
                    break;
                }

                if (!TEST(hp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    continue;
                }

                dist = DISTANCE(hp.getX(), hp.getY(), sp.getX(), sp.getY());

                if (dist > 2000) {
                    continue;
                }

                dist = FindDistance3D(sp.getX() - hp.getX(), sp.getY() - hp.getY(), (sp.getZ() - hp.getZ()) >> 4);

                if (dist > 2000) {
                    continue;
                }

                if (WallSpriteInsideSprite(sp, hp)) {
                    DoDamage(i, SpriteNum);
//	                    PlaySound(PlayerPainVocs[RANDOM_RANGE(MAX_PAIN)], sp.x, sp.y, sp.z, v3df_none);
                }
            }
        }
    }

    public static void DoStaticFlamesDamage(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        int dist;

        ListNode<Sprite> nexti;
        for (int j : StatDamageList) {
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite hp = node.get();

                if (i == SpriteNum) {
                    break;
                }

                if (!TEST(hp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    continue;
                }

                dist = DISTANCE(hp.getX(), hp.getY(), sp.getX(), sp.getY());

                if (dist > 2000) {
                    continue;
                }

                dist = FindDistance3D(sp.getX() - hp.getX(), sp.getY() - hp.getY(), (sp.getZ() - hp.getZ()) >> 4);

                if (dist > 2000) {
                    continue;
                }

                if (SpriteOverlap(SpriteNum, i)) // If sprites are overlapping, cansee will fail!
                {
                    DoDamage(i, SpriteNum);
                } else if (u.Radius > 200) {
                    if (FAFcansee(sp.getX(), sp.getY(), SPRITEp_MID(sp), sp.getSectnum(), hp.getX(), hp.getY(), SPRITEp_MID(hp), hp.getSectnum())) {
                        DoDamage(i, SpriteNum);
                    }
                }
            }
        }
    }

    public static void SpawnDemonFist(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SUICIDE)) {
            return;
        }

        // PlaySound(DIGI_ITEM_SPAWN, sp, v3df_none);
        int explosion =  SpawnSprite(STAT_MISSILE, 0, s_TeleportEffect[0], sp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_MID(sp), sp.getAng(), 0);
        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setHitag(LUMINOUS); // Always full brightness
        exp.setOwner(-1);
        exp.setShade(-40);
        exp.setXrepeat(32);
        exp.setYrepeat(32);
        eu.spal = 25;
        exp.setPal(25);

        exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YCENTER));
        exp.setCstat(exp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        eu.Radius = DamageData[DMG_BASIC_EXP].radius;

        if (RANDOM_P2(1024 << 8) >> 8 > 600) {
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_XFLIP));
        }
        if (RANDOM_P2(1024 << 8) >> 8 > 600) {
            exp.setCstat(exp.getCstat() | (CSTAT_SPRITE_YFLIP));
        }

    }
    // asserting out!

    public static void DoTeleRipper(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        PlaySound(DIGI_ITEM_SPAWN, sp, v3df_none);
        Ripper2Hatch(SpriteNum);
    }

    public static void DoCoolgDrip(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.Counter += 220;
        sp.setZ(sp.getZ() + u.Counter);

        if (sp.getZ() > u.loz - u.floor_dist) {
            sp.setZ(u.loz - u.floor_dist);
            sp.setXrepeat(32);
            sp.setYrepeat(32);
            ChangeState(SpriteNum, s_GoreFloorSplash[0]);
            if (u.spal == PALETTE_BLUE_LIGHTING) {
                PlaySound(DIGI_DRIP, sp, v3df_none);
            }
        }
    }

    public static void InitCoolgDrip(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum), wp;
        if (sp == null) {
            return;
        }
        USER wu;
        int nx, ny, nz;

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ();

        int w = SpawnSprite(STAT_MISSILE, COOLG_DRIP, s_CoolgDrip[0], sp.getSectnum(), nx, ny, nz, sp.getAng(), 0);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        // wp.owner = SpriteNum;
        SetOwner(SpriteNum, w);
        wp.setXrepeat(20);
        wp.setYrepeat(20);
        wp.setShade(-5);
        wp.setZvel(0);
        wp.setClipdist(16 >> 2);
        wu.ceiling_dist =  Z(4);
        wu.floor_dist =  Z(4);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        DoFindGroundPoint(SpriteNum);

    }


    public static boolean GenerateDrips(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum), wp;
        USER u = getUser(SpriteNum), wu;
        if (sp == null || u == null) {
            return false;
        }

        int nx, ny, nz;
        int w = -1;

        if ((u.WaitTics -= ACTORMOVETICS) <= 0) {
            if (sp.getLotag() == 0) {
                u.WaitTics =  (RANDOM_P2(256 << 8) >> 8);
            } else {
                u.WaitTics =  ((sp.getLotag() * 120) + SEC(RANDOM_RANGE(3 << 8) >> 8));
            }

            if (TEST_BOOL2(sp)) {
                w =  SpawnBubble(SpriteNum);
                return (w) != -1;
            }

            nx = sp.getX();
            ny = sp.getY();
            nz = sp.getZ();

            w =  SpawnSprite(STAT_SHRAP, COOLG_DRIP, s_CoolgDrip[0], sp.getSectnum(), nx, ny, nz, sp.getAng(), 0);
            wp = boardService.getSprite(w);
            wu = getUser(w);
            if (wp == null || wu == null) {
                return false;
            }

            // wp.owner = SpriteNum;
            SetOwner(SpriteNum, w);
            wp.setXrepeat(20);
            wp.setYrepeat(20);
            wp.setShade(-10);
            wp.setZvel(0);
            wp.setClipdist(16 >> 2);
            wu.ceiling_dist =  Z(4);
            wu.floor_dist =  Z(4);
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
            if (TEST_BOOL1(sp)) {
                wu.spal = PALETTE_BLUE_LIGHTING;
                wp.setPal(PALETTE_BLUE_LIGHTING);
            }

            DoFindGroundPoint(SpriteNum);
        }
        return w != -1;
    }

    public static void InitFireballTrap(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum), wp;
        if (sp == null) {
            return;
        }

        USER wu;
        int nx, ny, nz;
        int w;

        PlaySound(DIGI_FIREBALL1, sp, v3df_none);

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ() - SPRITEp_SIZE_Z(sp);

        // Spawn a shot
        w =  SpawnSprite(STAT_MISSILE, FIREBALL, s_Fireball[0], sp.getSectnum(), nx, ny, nz, sp.getAng(), FIREBALL_TRAP_VELOCITY);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        // wp.owner = SpriteNum;
        wp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(SpriteNum, w);
        wp.setXrepeat(wp.getXrepeat() - 20);
        wp.setYrepeat(wp.getYrepeat() - 20);
        wp.setShade(-40);
        wp.setClipdist(32 >> 2);
        wp.setZvel(0);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wu.WeaponNum = WPN_HOTHEAD;

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

    }

    public static void InitBoltTrap(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        int nx, ny, nz;
        int w;

        PlaySound(DIGI_RIOTFIRE, sp, v3df_none);

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ() - SPRITEp_SIZE_Z(sp);

        // Spawn a shot
        w = SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R0, s_Rocket[0][0], sp.getSectnum(), nx, ny, nz, sp.getAng(), BOLT_TRAP_VELOCITY);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        // wp.owner = SpriteNum;
        SetOwner(SpriteNum, w);
        wp.setYrepeat(32);
        wp.setXrepeat(32);
        wp.setShade(-15);
        wp.setZvel(0);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_Rocket);
        wu.Radius = 200;

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

    }

    public static void InitSpearTrap(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if  (sp == null) {
            return;
        }

        int nx = sp.getX();
        int ny = sp.getY();
        int nz = SPRITEp_MID(sp);

        // Spawn a shot
        int w =  SpawnSprite(STAT_MISSILE, CROSSBOLT, s_CrossBolt[0][0], sp.getSectnum(), nx, ny, nz, sp.getAng(), 750);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        // wp.owner = SpriteNum;
        SetOwner(SpriteNum, w);
        wp.setXrepeat(16);
        wp.setYrepeat(26);
        wp.setShade(-25);
        wp.setClipdist(64 >> 2);

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_CrossBolt);

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

        wu.Flags |= (SPR_XFLIP_TOGGLE);

        PlaySound(DIGI_STAR, sp, v3df_none);
    }

    public static void DoSuicide(int SpriteNum) {
        KillSprite(SpriteNum);
    }

    public static void DoDefaultStat(int SpriteNum) {
        change_sprite_stat(SpriteNum, STAT_DEFAULT);
    }

    public static void InitTracerUzi(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sprite psp = pp.getSprite();
        if (u == null || psp == null) {
            return;
        }

        int nx, ny, nz;
        int w;
        int oclipdist;

        nx = pp.posx;
        ny = pp.posy;
        nz = pp.posz + Z(8) + ((100 - pp.getHorizi()) * 72);

        // Spawn a shot
        // Inserting and setting up variables
        w = SpawnSprite(STAT_MISSILE, 0, s_Tracer[0], pp.cursectnum, nx, ny, nz, pp.getAnglei(), TRACER_VELOCITY);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        wp.setHitag(LUMINOUS); // Always full brightness
        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(10);
        wp.setXrepeat(10);
        wp.setShade(-40);
        wp.setZvel(0);
        wp.setClipdist(32 >> 2);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 50;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_INVISIBLE));

        oclipdist = psp.getClipdist();
        psp.setClipdist(0);

        wp.setAng(NORM_ANGLE(wp.getAng() + 512));
        if (TEST(pp.Flags, PF_TWO_UZI) && pp.WpnUziType == 0) {
            HelpMissileLateral(w, lat_dist[RANDOM_P2(2 << 8) >> 8]);
        } else {
            HelpMissileLateral(w, lat_dist[0]);
        }
        wp.setAng(NORM_ANGLE(wp.getAng() - 512));

        if (MissileSetPos(w, DoTracerStart, 800)) {
            psp.setClipdist(oclipdist);
            KillSprite(w);
            return;
        }

        wp.setZvel( ((100 - pp.getHorizi()) * (wp.getXvel() / 8)));

        psp.setClipdist(oclipdist);

        WeaponAutoAim(pp.PlayerSprite, w, 32, 0);

        // a bit of randomness
        wp.setAng(NORM_ANGLE(wp.getAng() + RANDOM_RANGE(30) - 15));

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

    }

    public static void InitTracerTurret(int SpriteNum, int Operator, int horiz) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        int nx, ny, nz;
        int w;

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ() + ((100 - horiz) * 72);

        // Spawn a shot
        // Inserting and setting up variables

        w = SpawnSprite(STAT_MISSILE, 0, s_Tracer[0], sp.getSectnum(), nx, ny, nz, sp.getAng(), TRACER_VELOCITY);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        wp.setHitag(LUMINOUS); // Always full brightness
        if (Operator >= 0) {
            SetOwner(Operator, w);
        }
        wp.setYrepeat(10);
        wp.setXrepeat(10);
        wp.setShade(-40);
        wp.setZvel(0);
        wp.setClipdist(8 >> 2);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 50;
        wu.ceiling_dist =  Z(1);
        wu.floor_dist =  Z(1);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_INVISIBLE));

        wp.setZvel( ((100 - horiz) * (wp.getXvel() / 8)));

        WeaponAutoAim(SpriteNum, w, 32, 0);

        // a bit of randomness
        wp.setAng(NORM_ANGLE(wp.getAng() + RANDOM_RANGE(30) - 15));

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

        if (SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

    }

    public static void InitTracerAutoTurret(int SpriteNum, int Operator, int xchange, int ychange, int zchange) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        int nx, ny, nz;
        int w;

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ();

        // Spawn a shot
        // Inserting and setting up variables

        w = SpawnSprite(STAT_MISSILE, 0, s_Tracer[0], sp.getSectnum(), nx, ny, nz, sp.getAng(), TRACER_VELOCITY);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        wp.setHitag(LUMINOUS); // Always full brightness
        if (Operator >= 0) {
            SetOwner(Operator, w);
        }
        wp.setYrepeat(10);
        wp.setXrepeat(10);
        wp.setShade(-40);
        wp.setZvel(0);
        wp.setClipdist(8 >> 2);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 50;
        wu.ceiling_dist =  Z(1);
        wu.floor_dist =  Z(1);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_INVISIBLE));

        wu.xchange = xchange;
        wu.ychange = ychange;
        wu.zchange = zchange;

        if (SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

    }

    public static boolean BulletHitSprite(int spi, int hitsprite, int ignoredSect, int ignoredWall, int hitx, int hity, int hitz, int ID) {
        Sprite sp = boardService.getSprite(spi);
        Sprite hsp = boardService.getSprite(hitsprite);
        USER hu = getUser(hitsprite);
        USER u = getUser(spi);
        if (sp == null || hsp == null || hu == null || u == null) {
            return false;
        }

        Sprite wp;
        int newsp;
        int id;

        // hit a NPC or PC?
        if (TEST(hsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
            // spawn a red splotch
            // !FRANK! this if was incorrect - its not who is HIT, its who is SHOOTING
            // if(!hu.PlayerP)
            if (u.PlayerP != -1) {
                id = UZI_SMOKE;
            } else if (TEST(u.Flags, SPR_SO_ATTACHED)) {
                id = UZI_SMOKE;
            } else // Spawn NPC uzi with less damage
            {
                id = UZI_SMOKE + 2;
            }

            if (ID > 0) {
                id =  ID;
            }

            newsp =  SpawnSprite(STAT_MISSILE, id, s_UziSmoke[0], 0, hitx, hity, hitz, sp.getAng(), 0);
            wp = boardService.getSprite(newsp);
            if (wp == null) {
                return false;
            }

            wp.setShade(-40);

            if (hu.PlayerP != -1) {
                wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
            }

            switch (hu.ID) {
                case TRASHCAN:
                case PACHINKO1:
                case PACHINKO2:
                case PACHINKO3:
                case PACHINKO4:
                case 623:
                case ZILLA_RUN_R0:
                    wp.setXrepeat(UZI_SMOKE_REPEAT);
                    wp.setYrepeat(UZI_SMOKE_REPEAT);
                    if (RANDOM_P2(1024) > 800) {
                        SpawnShrapX(hitsprite);
                    }
                    break;
                default:
                    wp.setXrepeat(UZI_SMOKE_REPEAT / 3);
                    wp.setYrepeat(UZI_SMOKE_REPEAT / 3);
                    wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
                    // wu.spal = wp.pal = PALETTE_RED_LIGHTING;
                    break;
            }

            SetOwner(spi, newsp);
            wp.setAng(sp.getAng());

            engine.setspritez(newsp, hitx, hity, hitz);
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
            wp.setCstat(wp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

            if ((RANDOM_P2(1024 << 5) >> 5) < 512 + 128) {
                if (hu.PlayerP == -1) {
                    SpawnBlood(hitsprite, -1, sp.getAng(), hitx, hity, hitz);
                } else {
                    SpawnBlood(hitsprite, -1, sp.getAng(), hitx, hity, hitz + Z(20));
                }

                // blood comes out the other side?
                if ((RANDOM_P2(1024 << 5) >> 5) < 256) {
                    if (hu.PlayerP == -1) {
                        SpawnBlood(hitsprite, -1, NORM_ANGLE(sp.getAng() + 1024), hitx, hity, hitz);
                    }
                    if (hu.ID != TRASHCAN && hu.ID != ZILLA_RUN_R0) {
                        QueueWallBlood(hitsprite, sp.getAng()); // QueueWallBlood needs bullet angle.
                    }
                }
            }

            DoHitscanDamage(newsp, hitsprite);

            return (true);
        }

        return (false);
    }

    public static void HitscanSpriteAdjust(int SpriteNum, int hitwall) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        int wall_ang;
        int xvect, yvect;
        int sectnum;

        int ang = sp.getAng();
        if (hitwall != -1) {
            Wall wph = boardService.getWall(hitwall);
            if (wph != null) {
                wall_ang = wph.getWallAngle();
                ang = NORM_ANGLE(wall_ang + 512);
                sp.setAng(ang);
            }
        }

        xvect = EngineUtils.cos(ang) << 4;
        yvect = EngineUtils.sin(ang) << 4;

        clipmoveboxtracenum = 1;

        // must have this
        sectnum = sp.getSectnum();
        engine.clipmove(sp.getX(), sp.getY(), sp.getZ(), sectnum, xvect, yvect, 4, 4 << 8, 4 << 8, CLIPMASK_MISSILE);

        sp.setX(clipmove_x);
        sp.setY(clipmove_y);
        sp.setZ(clipmove_z);
        sectnum = clipmove_sectnum;

        clipmoveboxtracenum = 3;

        if (sp.getSectnum() != sectnum) {
            engine.changespritesect(SpriteNum, sectnum);
        }

    }

    public static void InitUzi(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        Sprite wp, hsp;
        USER wu;
        int daang, j;
        int hitsect, hitwall, hitsprite;
        int hitx, hity, hitz, daz, nz;
        int xvect, yvect, zvect;
        int cstat = 0;
        byte pal = 0;
        // static char alternate=0;

        int clockdiff;
        boolean FireSnd = false;

        PlayerUpdateAmmo(pp, u.WeaponNum, -1);

        if (uziclock > engine.getTotalClock()) {
            uziclock = engine.getTotalClock();
            FireSnd = true;
        }

        clockdiff = engine.getTotalClock() - uziclock;
        if (clockdiff > UZIFIRE_WAIT) {
            uziclock = engine.getTotalClock();
            FireSnd = true;
        }

        if (FireSnd) {
            PlaySound(DIGI_UZIFIRE, pp, v3df_dontpan | v3df_doppler);
        }

        // Make sprite shade brighter
        u.Vis = 128;

        if (RANDOM_P2(1024) < 400) {
            InitTracerUzi(pp);
        }

        nz = pp.posz + pp.bob_z;
        daz = pp.posz + pp.bob_z;
        daang = 32;

        if (WeaponAutoAimHitscan(pp.PlayerSprite, tmp_ptr[0].set(daz), tmp_ptr[1].set(daang), 0) != -1) {
            daz = tmp_ptr[0].value;
            daang =  tmp_ptr[1].value;
            daang += RANDOM_RANGE(24) - 12;
            daang = NORM_ANGLE(daang);
            daz += RANDOM_RANGE(10000) - 5000;
        } else {
            // daang = NORM_ANGLE(pp.getAnglei() + (RANDOM_RANGE(50) - 25));
            daang = NORM_ANGLE(pp.getAnglei() + (RANDOM_RANGE(24) - 12));
            daz = ((100 - pp.getHorizi()) * 2000) + (RANDOM_RANGE(24000) - 12000);
        }

        xvect = EngineUtils.sin(NORM_ANGLE(daang + 512));
        yvect = EngineUtils.sin(NORM_ANGLE(daang));
        zvect = daz;
        FAFhitscan(pp.posx, pp.posy, nz, pp.cursectnum, // Start position
                xvect, yvect, zvect, pHitInfo, CLIPMASK_MISSILE);

        hitsect = pHitInfo.hitsect;
        hitwall = pHitInfo.hitwall;
        hitsprite = pHitInfo.hitsprite;
        hitx = pHitInfo.hitx;
        hity = pHitInfo.hity;
        hitz = pHitInfo.hitz;

        if (hitsect == -1) {
            return;
        }

        SetVisHigh();

        // check to see what you hit
        if (hitsprite == -1 && hitwall < 0) {
            Sector hsec = boardService.getSector(hitsect);
            if (hsec != null) {
                if (klabs(hitz - hsec.getCeilingz()) <= Z(1)) {
                    hitz += Z(16);
                    cstat |= (CSTAT_SPRITE_YFLIP);

                    if (TEST(hsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                        return;
                    }

                    if (SectorIsUnderwaterArea(hitsect)) {
                        WarpToSurface(tmp_ptr[0].set(hitsect), tmp_ptr[1].set(hitx), tmp_ptr[2].set(hity), tmp_ptr[3].set(hitz));
                        hitsect = tmp_ptr[0].value;
                        hitx = tmp_ptr[1].value;
                        hity = tmp_ptr[2].value;
                        hitz = tmp_ptr[3].value;

                        ContinueHitscan(pp, hitsect, hitx, hity, hitz, daang, xvect, yvect, zvect);
                        return;
                    }
                } else if (klabs(hitz - hsec.getFloorz()) <= Z(1)) {
                    if (DTEST(hsec.getExtra(), SECTFX_LIQUID_MASK) != SECTFX_LIQUID_NONE) {
                        SpawnSplashXY(hitx, hity, hitz, hitsect);

                        if (SectorIsDiveArea(hitsect)) {
                            WarpToUnderwater(tmp_ptr[0].set(hitsect), tmp_ptr[1].set(hitx), tmp_ptr[2].set(hity), tmp_ptr[3].set(hitz));
                            hitsect = tmp_ptr[0].value;
                            hitx = tmp_ptr[1].value;
                            hity = tmp_ptr[2].value;
                            hitz = tmp_ptr[3].value;

                            ContinueHitscan(pp, hitsect, hitx, hity, hitz, daang, xvect, yvect, zvect);
                            return;
                        }

                        return;
                    }
                }
            }
        }

        if (hitwall != -1) {
            Wall wph = boardService.getWall(hitwall);
            if (wph != null) {
                Sector nsec = boardService.getSector(wph.getNextsector());
                if (nsec != null) {
                    if (TEST(nsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                        if (hitz < nsec.getCeilingz()) {
                            return;
                        }
                    }
                }

                if (wph.getLotag() == TAG_WALL_BREAK) {
                    HitBreakWall(hitwall, hitx, hity, hitz, daang, u.ID);
                    return;
                }

                QueueHole(daang, hitsect, hitwall, hitx, hity, hitz);
            }
        }

        // hit a sprite?
        if (hitsprite != -1) {
            USER hu = getUser(hitsprite);
            hsp = boardService.getSprite(hitsprite);
            if (hsp == null) {
                return;
            }

            if (hu != null && hu.ID == TRASHCAN) {

                PlaySound(DIGI_TRASHLID, hsp, v3df_none);
                if (hu.WaitTics <= 0) {
                    hu.WaitTics =  SEC(2);
                    ChangeState(hitsprite, s_TrashCanPain[0]);
                }
            }

            if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                if (MissileHitMatch(-1, WPN_UZI, hitsprite)) {
                    return;
                }
            }

            if (TEST(hsp.getExtra(), SPRX_BREAKABLE) && HitBreakSprite(hitsprite, 0)) {
                return;
            }

            if (BulletHitSprite(pp.PlayerSprite, hitsprite, hitsect, hitwall, hitx, hity, hitz, 0)) {
                return;
            }

            // hit a switch?
            if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL) && (hsp.getLotag() != 0 || hsp.getHitag() != 0)) {
                ShootableSwitch(hitsprite, -1);
            }
        }

        j =  SpawnSprite(STAT_MISSILE, UZI_SMOKE, s_UziSmoke[0], hitsect, hitx, hity, hitz, daang, 0);
        wp = boardService.getSprite(j);
        if (wp == null) {
            return;
        }

        wp.setShade(-40);
        wp.setXrepeat(UZI_SMOKE_REPEAT);
        wp.setYrepeat(UZI_SMOKE_REPEAT);
        SetOwner(pp.PlayerSprite, j);
        // SET(wp.cstat, cstat | CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_YCENTER);
        wp.setCstat(wp.getCstat() | (cstat | CSTAT_SPRITE_YCENTER));
        wp.setClipdist(8 >> 2);

        HitscanSpriteAdjust(j, hitwall);
        DoHitscanDamage(j, hitsprite);

        j =  SpawnSprite(STAT_MISSILE, UZI_SPARK, s_UziSpark[0], hitsect, hitx, hity, hitz, daang, 0);
        wp = boardService.getSprite(j);
        wu = getUser(j);
        if (wp == null || wu == null) {
            return;
        }

        wp.setShade(-40);
        wp.setXrepeat(UZI_SPARK_REPEAT);
        wp.setYrepeat(UZI_SPARK_REPEAT);
        SetOwner(pp.PlayerSprite, j);
        wu.spal = pal;
        wp.setPal(pal);
        wp.setCstat(wp.getCstat() | (cstat | CSTAT_SPRITE_YCENTER));
        wp.setClipdist(8 >> 2);

        HitscanSpriteAdjust(j, hitwall);

        if (RANDOM_P2(1024) < 100) {
            PlaySound(DIGI_RICHOCHET1, wp, v3df_none);
        } else if (RANDOM_P2(1024) < 100) {
            PlaySound(DIGI_RICHOCHET2, wp, v3df_none);
        }

    }

    // Electro Magnetic Pulse gun
    public static void InitEMP(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        Sprite hsp;
        int daang, j;
        int hitsect, hitwall, hitsprite;
        int hitx, hity, hitz, daz, nz;
        int cstat = 0;

        PlayerUpdateAmmo(pp, u.WeaponNum, -1);

        PlaySound(DIGI_RAILFIRE, pp, v3df_dontpan | v3df_doppler);

        InitTracerUzi(pp);

        daz = nz = pp.posz + pp.bob_z;
        daang = 64;
        if (WeaponAutoAimHitscan(pp.PlayerSprite, tmp_ptr[0].set(daz), tmp_ptr[1].set(daang), 0) != -1) {
            daz = tmp_ptr[0].value;
            daang =  tmp_ptr[1].value;
        } else {
            daz = (100 - pp.getHorizi()) * 2000;
            daang = pp.getAnglei();
        }

        FAFhitscan(pp.posx, pp.posy, nz, pp.cursectnum, // Start position
                EngineUtils.sin(NORM_ANGLE(daang + 512)), // X vector of 3D ang
                EngineUtils.sin(NORM_ANGLE(daang)), // Y vector of 3D ang
                daz, // Z vector of 3D ang
                pHitInfo, CLIPMASK_MISSILE);

        hitsect = pHitInfo.hitsect;
        hitwall = pHitInfo.hitwall;
        hitsprite = pHitInfo.hitsprite;
        hitx = pHitInfo.hitx;
        hity = pHitInfo.hity;
        hitz = pHitInfo.hitz;

        j =  SpawnSprite(STAT_MISSILE, EMP, s_EMPBurst[0], hitsect, hitx, hity, hitz, daang, 0);
        Sprite wp = boardService.getSprite(j);
        USER wu = getUser(j);
        if (wp == null || wu == null) {
            return;
        }

        wu.WaitTics =  SEC(7);
        wp.setShade(-127);
        SetOwner(pp.PlayerSprite, j);
        wp.setCstat(wp.getCstat() | (cstat | CSTAT_SPRITE_YCENTER));
        wp.setClipdist(8 >> 2);

        if (hitsect == -1) {

            return;
        }

        SetVisHigh();

        // check to see what you hit
        if (hitsprite == -1 && hitwall < 0) {
            Sector hsec = boardService.getSector(hitsect);
            if (hsec != null) {
                if (klabs(hitz - hsec.getCeilingz()) <= Z(1)) {
                    hitz += Z(16);
                    if (TEST(hsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                        return;
                    }
                } else if (klabs(hitz - hsec.getFloorz()) <= Z(1)) {
                    if (DTEST(hsec.getExtra(), SECTFX_LIQUID_MASK) != SECTFX_LIQUID_NONE) {
                        SpawnSplashXY(hitx, hity, hitz, hitsect);
                        return;
                    }
                }
            }
        }

        if (hitwall != -1) {
            Wall wph = boardService.getWall(hitwall);
            if (wph != null) {
                Sector nsec = boardService.getSector(wph.getNextsector());
                if (nsec != null) {
                    if (TEST(nsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                        if (hitz < nsec.getCeilingz()) {
                            return;
                        }
                    }
                }

                HitscanSpriteAdjust(j, hitwall);
                if (wph.getLotag() == TAG_WALL_BREAK) {
                    HitBreakWall(hitwall, hitx, hity, hitz, daang, u.ID);
                    return;
                }
            }
        }

        // hit a sprite?
        if (hitsprite != -1) {
            hsp = boardService.getSprite(hitsprite);
            if (hsp == null) {
                return;
            }

            if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                if (MissileHitMatch(-1, WPN_UZI, hitsprite)) {
                    return;
                }
            }

            if (TEST(hsp.getExtra(), SPRX_BREAKABLE)) {
                HitBreakSprite(hitsprite, 0);
                // return(0);
            }

            if (BulletHitSprite(pp.PlayerSprite, hitsprite, hitsect, hitwall, hitx, hity, hitz, 0))
            // return(0);

            // hit a switch?
            {
                if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL) && (hsp.getLotag() != 0 || hsp.getHitag() != 0)) {
                    ShootableSwitch(hitsprite, -1);
                }
            }

            if (TEST(hsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                // attach weapon to sprite
                SetAttach(hitsprite, j);
                wu.sz = hsp.getZ() - wp.getZ();
                if (RANDOM_RANGE(1000) > 500) {
                    PlayerSound(DIGI_YOULOOKSTUPID, v3df_follow | v3df_dontpan, pp);
                }
            } else {
                if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                    wu.Flags2 |= (SPR2_ATTACH_WALL);
                } else if (TEST(hsp.getCstat(), CSTAT_SPRITE_FLOOR)) {
                    // hit floor
                    if (wp.getZ() > DIV2(wu.hiz + wu.loz)) {
                        wu.Flags2 |= (SPR2_ATTACH_FLOOR);
                    } else {
                        wu.Flags2 |= (SPR2_ATTACH_CEILING);
                    }
                } else {
                    KillSprite(j);
                }
            }
        }
    }

    public static void InitTankShell(int SpriteNum, PlayerStr pp) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        PlaySound(DIGI_CANNON, pp, v3df_dontpan | v3df_doppler);

        int w =  SpawnSprite(STAT_MISSILE, 0, s_TankShell[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), TANK_SHELL_VELOCITY);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(8);
        wp.setXrepeat(8);
        wp.setShade(-40);
        wp.setZvel(0);
        wp.setClipdist(32 >> 2);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 50;
        wu.ceiling_dist =  Z(4);
        wu.floor_dist =  Z(4);
        wu.Flags2 |= (SPR2_SO_MISSILE);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_INVISIBLE));

        wp.setZvel( ((100 - pp.getHorizi()) * (wp.getXvel() / 8)));

        WeaponAutoAim(SpriteNum, w, 64, 0);
        // a bit of randomness
        wp.setAng(wp.getAng() + RANDOM_RANGE(30) - 15);
        wp.setAng(NORM_ANGLE(wp.getAng()));

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

        if (SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }
    }

    public static void InitTurretMicro(int SpriteNum, PlayerStr pp) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER pu = getUser(pp.PlayerSprite);
        if (sp == null || pu == null) {
            return;
        }

        USER hu;
        Sprite hp = null;
        int nz, dist;
        int w;
        int i, ang;

        int nx = sp.getX();
        int ny = sp.getY();

        DoPickTarget(pp.PlayerSprite, 256, 0);

        if (TargetSortCount > MAX_TURRET_MICRO) {
            TargetSortCount = MAX_TURRET_MICRO;
        }

        int tsi = 0;
        for (i = 0; i < MAX_TURRET_MICRO; i++) {
            Target_Sort ts = TargetSort[tsi];
            if (ts != null) {
                hp = boardService.getSprite(ts.sprite_num);
            }

            if (tsi < TargetSortCount && hp != null && ts != null) {
                hu = getUser(ts.sprite_num);
                ang = EngineUtils.getAngle(hp.getX() - nx, hp.getY() - ny);
                tsi++;
            } else {
                hp = null;
                hu = null;
                ang = sp.getAng();
            }

            nz = sp.getZ();
            nz += Z(RANDOM_RANGE(20)) - Z(10);

            // Spawn a shot
            // Inserting and setting up variables

            w =  SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R0, s_Micro[0][0], sp.getSectnum(), nx, ny, nz, ang, 1200);
            Sprite wp = boardService.getSprite(w);
            USER wu = getUser(w);
            if (wp == null || wu == null) {
                return;
            }

            SetOwner(pp.PlayerSprite, w);
            wp.setYrepeat(24);
            wp.setXrepeat(24);
            wp.setShade(-15);
            wp.setZvel( ((100 - pp.getHorizi()) * HORIZ_MULT));
            wp.setClipdist(64 >> 2);

            // randomize zvelocity
            wp.setZvel(wp.getZvel() + RANDOM_RANGE(Z(8)) - Z(5));

            wu.RotNum = 5;
            NewStateGroup(w, WeaponStateGroup.sg_Micro);

            wu.WeaponNum = pu.WeaponNum;
            wu.Radius = 200;
            wu.ceiling_dist =  Z(2);
            wu.floor_dist =  Z(2);
            wp.setCstat(wp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_INVISIBLE));

            wu.WaitTics =  (10 + RANDOM_RANGE(40));

            if (hp != null && hu != null) {
                dist = Distance(wp.getX(), wp.getY(), hp.getX(), hp.getY());
                if (dist != 0) {
                    int zh;
                    zh = SPRITEp_TOS(hp) + DIV4(SPRITEp_SIZE_Z(hp));
                    wp.setZvel( ((wp.getXvel() * (zh - wp.getZ())) / dist));
                }

                wu.WpnGoal =  ts.sprite_num;
                hu.Flags |= (SPR_TARGETED);
                hu.Flags |= (SPR_ATTACKED);
            } else {
                wp.setAng(NORM_ANGLE(wp.getAng() + (RANDOM_RANGE(MICRO_ANG) - DIV2(MICRO_ANG)) - 16));
            }

            wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
            wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
            wu.zchange = wp.getZvel();
        }
    }

    public static void InitTurretRocket(int SpriteNum, PlayerStr pp) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int w = SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R0, s_Rocket[0][0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), ROCKET_VELOCITY);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(40);
        wp.setXrepeat(40);
        wp.setShade(-40);
        wp.setZvel(0);
        wp.setClipdist(32 >> 2);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 50;
        wu.ceiling_dist =  Z(4);
        wu.floor_dist =  Z(4);
        wu.Flags2 |= (SPR2_SO_MISSILE);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        wp.setZvel( ((100 - pp.getHorizi()) * (wp.getXvel() / 8)));

        WeaponAutoAim(SpriteNum, w, 64, 0);
        // a bit of randomness
        // wp.ang += RANDOM_RANGE(30) - 15;

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

        if (SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }
    }

    public static void InitTurretFireball(int SpriteNum, PlayerStr pp) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int w =  SpawnSprite(STAT_MISSILE, FIREBALL, s_Fireball[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), FIREBALL_VELOCITY);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(40);
        wp.setXrepeat(40);
        wp.setShade(-40);
        wp.setZvel(0);
        wp.setClipdist(32 >> 2);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 50;
        wu.ceiling_dist =  Z(4);
        wu.floor_dist =  Z(4);
        wu.Flags2 |= (SPR2_SO_MISSILE);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        wp.setZvel( ((100 - pp.getHorizi()) * (wp.getXvel() / 8)));

        WeaponAutoAim(SpriteNum, w, 64, 0);
        // a bit of randomness
        wp.setAng(wp.getAng() + RANDOM_RANGE(30) - 15);
        wp.setAng(NORM_ANGLE(wp.getAng()));

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

        if (SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }
    }

    public static void InitTurretRail(int SpriteNum, PlayerStr pp) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int nx, ny, nz;
        int w;

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ();

        // Spawn a shot
        // Inserting and setting up variables

        w =  SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R1, s_Rail[0][0], pp.cursectnum, nx, ny, nz, sp.getAng(), 1200);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(52);
        wp.setXrepeat(52);
        wp.setShade(-15);
        wp.setZvel( ((100 - pp.getHorizi()) * HORIZ_MULT));

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_Rail);

        wu.Radius = 200;
        wu.ceiling_dist =  Z(1);
        wu.floor_dist =  Z(1);
        wu.Flags2 |= (SPR2_SO_MISSILE);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER | CSTAT_SPRITE_INVISIBLE));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        wp.setClipdist(64 >> 2);

        if (WeaponAutoAim(pp.PlayerSprite, w, 32, 0) == -1) {
            wp.setAng(NORM_ANGLE(wp.getAng()));
        }

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();
    }

    public static void InitTurretLaser(int SpriteNum, PlayerStr pp) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int nx, ny, nz;
        int w;

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ();

        // Spawn a shot
        // Inserting and setting up variables

        w = SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R0, s_Laser[0], pp.cursectnum, nx, ny, nz, sp.getAng(), 300);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(52);
        wp.setXrepeat(52);
        wp.setShade(-15);

        // the slower the missile travels the less of a zvel it needs
        wp.setZvel( ((100 - pp.getHorizi()) * HORIZ_MULT));
        wp.setZvel(wp.getZvel() / 4);

        wu.Radius = 200;
        wu.ceiling_dist =  Z(1);
        wu.floor_dist =  Z(1);
        wu.Flags2 |= (SPR2_SO_MISSILE);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        wp.setClipdist(64 >> 2);

        if (WeaponAutoAim(SpriteNum, w, 32, 0) == -1) {
            wp.setAng(NORM_ANGLE(wp.getAng()));
        }

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();
    }

    public static void InitSobjMachineGun(int SpriteNum, PlayerStr pp) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        Sprite hsp;
        int daang;
        int hitsect, hitwall, hitsprite, nsect;
        int hitx, hity, hitz, daz;
        int nx, ny, nz;
        int spark;

        PlaySound(DIGI_BOATFIRE, pp, v3df_dontpan | v3df_doppler);

        nx = sp.getX();
        ny = sp.getY();
        daz = nz = sp.getZ();
        nsect = sp.getSectnum();

        if (RANDOM_P2(1024) < 200) {
            InitTracerTurret(SpriteNum, pp.PlayerSprite, pp.getHorizi());
        }

        daang = 64;
        if (WeaponAutoAimHitscan(SpriteNum, tmp_ptr[0].set(daz), tmp_ptr[1].set(daang), 0) != -1) {
            daz = tmp_ptr[0].value;
            daang =  tmp_ptr[1].value;

            daz += RANDOM_RANGE(Z(30)) - Z(15);
        } else {
            int horiz;
            horiz = pp.getHorizi();
            if (horiz < 75) {
                horiz = 75;
            }

            daz = ((100 - horiz) * 2000) + (RANDOM_RANGE(Z(80)) - Z(40));
            daang = sp.getAng();
        }

        FAFhitscan(nx, ny, nz, nsect, // Start position
                EngineUtils.sin(NORM_ANGLE(daang + 512)), // X vector of 3D ang
                EngineUtils.sin(NORM_ANGLE(daang)), // Y vector of 3D ang
                daz, // Z vector of 3D ang
                pHitInfo, CLIPMASK_MISSILE);

        hitsect = pHitInfo.hitsect;
        hitwall = pHitInfo.hitwall;
        hitsprite = pHitInfo.hitsprite;
        hitx = pHitInfo.hitx;
        hity = pHitInfo.hity;
        hitz = pHitInfo.hitz;

        if (hitsect == -1) {
            return;
        }

        if (hitsprite == -1 && hitwall < 0) {
            Sector hsec = boardService.getSector(hitsect);
            if (hsec != null) {
                if (klabs(hitz - hsec.getCeilingz()) <= Z(1)) {
                    hitz += Z(16);

                    if (TEST(hsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                        return;
                    }
                } else if (klabs(hitz - hsec.getFloorz()) <= Z(1)) {
                    if (DTEST(hsec.getExtra(), SECTFX_LIQUID_MASK) != SECTFX_LIQUID_NONE) {
                        SpawnSplashXY(hitx, hity, hitz, hitsect);
                        return;
                    }
                }
            }

        }

        // hit a sprite?
        if (hitsprite != -1) {
            hsp = boardService.getSprite(hitsprite);
            if (hsp == null) {
                return;
            }

            if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                // spawn sparks here and pass the sprite as SO_MISSILE
                spark = SpawnBoatSparks(pp, hitsect, hitwall, hitx, hity, hitz, daang);
                USER su = getUser(spark);
                if (su != null) {
                    su.Flags2 |= (SPR2_SO_MISSILE);
                    MissileHitMatch(spark, -1, hitsprite);
                }
                return;
            }

            if (TEST(hsp.getExtra(), SPRX_BREAKABLE)) {
                HitBreakSprite(hitsprite, 0);
                return;
            }

            if (BulletHitSprite(pp.PlayerSprite, hitsprite, hitsect, hitwall, hitx, hity, hitz, 0)) {
                return;
            }

            // hit a switch?
            if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL) && (hsp.getLotag() != 0 || hsp.getHitag() != 0)) {
                ShootableSwitch(hitsprite, -1);
            }
        }

        spark =  SpawnBoatSparks(pp, hitsect, hitwall, hitx, hity, hitz, daang);
        DoHitscanDamage(spark, hitsprite);
    }

    public static void InitSobjGun(PlayerStr pp) {
        boolean first = false;
        if (pp.sop == -1) {
            return;
        }

        Sector_Object sop = SectorObject[pp.sop];

        for (int i = 0; sop.sp_num[i] != -1; i++) {
            Sprite sp = boardService.getSprite(sop.sp_num[i]);
            if (sp == null) {
                continue;
            }

            if (sp.getStatnum() == STAT_SO_SHOOT_POINT) {
                // match when firing
                if (SP_TAG2(sp) != 0) {
                    DoMatchEverything(pp, SP_TAG2(sp), -1);
                    if (TEST_BOOL1(sp)) {
                        sp.setLotag(0);
                    }
                }

                // inert shoot point
                if (SP_TAG3(sp) == 255) {
                    return;
                }

                if (!first) {
                    first = true;
                    if (SP_TAG6(sp) != 0) {
                        DoSoundSpotMatch(SP_TAG6(sp), 1, SoundType.SOUND_OBJECT_TYPE);
                    }
                }

                switch (SP_TAG3(sp)) {
                    case 32:
                    case 0:
                        SpawnVis(sop.sp_num[i], -1, -1, -1, -1, 8);
                        SpawnBigGunFlames(sop.sp_num[i], pp.PlayerSprite, sop);
                        SetGunQuake(sop.sp_num[i]);
                        InitTankShell(sop.sp_num[i], pp);
                        if (SP_TAG5(sp) == 0) {
                            pp.FirePause = 80;
                        } else {
                            pp.FirePause =  SP_TAG5(sp);
                        }
                        break;
                    case 1:
                        SpawnVis(sop.sp_num[i], -1, -1, -1, -1, 32);
                        SpawnBigGunFlames(-(sop.sp_num[i]), pp.PlayerSprite, sop);
                        InitSobjMachineGun(sop.sp_num[i], pp);
                        if (SP_TAG5(sp) == 0) {
                            pp.FirePause = 10;
                        } else {
                            pp.FirePause =  SP_TAG5(sp);
                        }
                        break;

                    case 2:
                        SpawnVis(sop.sp_num[i], -1, -1, -1, -1, 32);
                        InitTurretLaser(sop.sp_num[i], pp);
                        if (SP_TAG5(sp) == 0) {
                            pp.FirePause = 120;
                        } else {
                            pp.FirePause =  SP_TAG5(sp);
                        }
                        break;
                    case 3:
                        SpawnVis(sop.sp_num[i], -1, -1, -1, -1, 32);
                        InitTurretRail(sop.sp_num[i], pp);
                        if (SP_TAG5(sp) == 0) {
                            pp.FirePause = 120;
                        } else {
                            pp.FirePause =  SP_TAG5(sp);
                        }
                        break;
                    case 4:
                        SpawnVis(sop.sp_num[i], -1, -1, -1, -1, 32);
                        InitTurretFireball(sop.sp_num[i], pp);
                        if (SP_TAG5(sp) == 0) {
                            pp.FirePause = 20;
                        } else {
                            pp.FirePause =  SP_TAG5(sp);
                        }
                        break;
                    case 5:
                        SpawnVis(sop.sp_num[i], -1, -1, -1, -1, 32);
                        InitTurretRocket(sop.sp_num[i], pp);
                        if (SP_TAG5(sp) == 0) {
                            pp.FirePause = 100;
                        } else {
                            pp.FirePause =  SP_TAG5(sp);
                        }
                        break;
                    case 6:
                        SpawnVis(sop.sp_num[i], -1, -1, -1, -1, 32);
                        InitTurretMicro(sop.sp_num[i], pp);
                        if (SP_TAG5(sp) == 0) {
                            pp.FirePause = 100;
                        } else {
                            pp.FirePause =  SP_TAG5(sp);
                        }
                        break;
                }
            }
        }
    }

    public static int SpawnBoatSparks(PlayerStr pp, int hitsect, int hitwall, int hitx, int hity, int hitz, int hitang) {
        int j =  SpawnSprite(STAT_MISSILE, UZI_SMOKE, s_UziSmoke[0], hitsect, hitx, hity, hitz, hitang, 0);
        Sprite wp = boardService.getSprite(j);
        if (wp == null) {
            return -1;
        }

        wp.setShade(-40);
        wp.setXrepeat(UZI_SMOKE_REPEAT + 12);
        wp.setYrepeat(UZI_SMOKE_REPEAT + 12);
        SetOwner(pp.PlayerSprite, j);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_YCENTER));

        wp.setHitag(LUMINOUS); // Always full brightness
        // Sprite starts out with center exactly on wall.
        // This moves it back enough to see it at all angles.

        wp.setClipdist(32 >> 2);

        HitscanSpriteAdjust(j, hitwall);

        j =  SpawnSprite(STAT_MISSILE, UZI_SPARK, s_UziSpark[0], hitsect, hitx, hity, hitz, hitang, 0);
        wp = boardService.getSprite(j);
        USER wu = getUser(j);
        if (wp == null || wu == null) {
            return -1;
        }

        wp.setShade(-40);
        wp.setXrepeat(UZI_SPARK_REPEAT + 10);
        wp.setYrepeat(UZI_SPARK_REPEAT + 10);
        SetOwner(pp.PlayerSprite, j);

        wu.spal = PALETTE_DEFAULT;
        wp.setPal(PALETTE_DEFAULT);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        wp.setClipdist(32 >> 2);

        HitscanSpriteAdjust(j, hitwall);

        if (RANDOM_P2(1024) < 100) {
            PlaySound(DIGI_RICHOCHET1, wp, v3df_none);
        }

        return (j);
    }

    public static int SpawnTurretSparks(int hitsect, int hitwall, int hitx, int hity, int hitz, int hitang) {
        int j = SpawnSprite(STAT_MISSILE, UZI_SMOKE, s_UziSmoke[0], hitsect, hitx, hity, hitz, hitang, 0);
        Sprite wp = boardService.getSprite(j);
        if (wp == null) {
            return -1;
        }

        wp.setShade(-40);
        wp.setXrepeat(UZI_SMOKE_REPEAT + 12);
        wp.setYrepeat(UZI_SMOKE_REPEAT + 12);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_YCENTER));
        wp.setHitag(LUMINOUS); // Always full brightness

        // Sprite starts out with center exactly on wall.
        // This moves it back enough to see it at all angles.

        wp.setClipdist(32 >> 2);
        HitscanSpriteAdjust(j, hitwall);

        j =  SpawnSprite(STAT_MISSILE, UZI_SPARK, s_UziSpark[0], hitsect, hitx, hity, hitz, hitang, 0);
        wp = boardService.getSprite(j);
        USER wu = getUser(j);
        if (wp == null || wu == null) {
            return -1;
        }


        wp.setShade(-40);
        wp.setXrepeat(UZI_SPARK_REPEAT + 10);
        wp.setYrepeat(UZI_SPARK_REPEAT + 10);
        wu.spal = PALETTE_DEFAULT;
        wp.setPal(PALETTE_DEFAULT);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        wp.setClipdist(32 >> 2);
        HitscanSpriteAdjust(j, hitwall);

        if (RANDOM_P2(1024) < 100) {
            PlaySound(DIGI_RICHOCHET1, wp, v3df_none);
        }

        return (j);
    }

    public static int SpawnShotgunSparks(PlayerStr pp, int hitsect, int hitwall, int hitx, int hity, int hitz, int hitang) {
        int j =  SpawnSprite(STAT_MISSILE, UZI_SPARK, s_UziSpark[0], hitsect, hitx, hity, hitz, hitang, 0);
        Sprite wp = boardService.getSprite(j);
        USER wu = getUser(j);
        if (wp == null || wu == null) {
            return -1;
        }

        wp.setShade(-40);
        wp.setXrepeat(UZI_SPARK_REPEAT);
        wp.setYrepeat(UZI_SPARK_REPEAT);
        SetOwner(pp.PlayerSprite, j);
        wu.spal = PALETTE_DEFAULT;
        wp.setPal(PALETTE_DEFAULT);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        wp.setClipdist(32 >> 2);

        HitscanSpriteAdjust(j, hitwall);

        j =  SpawnSprite(STAT_MISSILE, SHOTGUN_SMOKE, s_ShotgunSmoke[0], hitsect, hitx, hity, hitz, hitang, 0);
        wp = boardService.getSprite(j);
        if (wp == null) {
            return -1;
        }

        wp.setShade(-40);
        wp.setXrepeat(SHOTGUN_SMOKE_REPEAT);
        wp.setYrepeat(SHOTGUN_SMOKE_REPEAT);
        SetOwner(pp.PlayerSprite, j);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_YCENTER));

        wp.setHitag(LUMINOUS); // Always full brightness
        // Sprite starts out with center exactly on wall.
        // This moves it back enough to see it at all angles.

        wp.setClipdist(32 >> 2);

        HitscanSpriteAdjust(j, hitwall);

        return (j);
    }

    public static void InitTurretMgun(Sector_Object sop) {
        Sprite hsp;
        int daang, i, j;
        int hitsect, hitwall, hitsprite, nsect;
        int hitx, hity, hitz, daz;
        int nx, ny, nz;
        int delta;
        int xvect, yvect, zvect;

        PlaySound(DIGI_BOATFIRE, sop, v3df_dontpan | v3df_doppler);

        for (i = 0; sop.sp_num[i] != -1; i++) {
            Sprite sp = boardService.getSprite(sop.sp_num[i]);
            if (sp != null && sp.getStatnum() == STAT_SO_SHOOT_POINT) {
                nx = sp.getX();
                ny = sp.getY();
                daz = nz = sp.getZ();
                nsect = sp.getSectnum();

                // if its not operated by a player
                if (sop.Animator != null) {
                    // only auto aim for Z
                    daang = 512;

                    hitsprite =  WeaponAutoAimHitscan(sop.sp_num[i], tmp_ptr[0].set(daz), tmp_ptr[1].set(daang), 0);
                    daz = tmp_ptr[0].value;
                    daang =  tmp_ptr[1].value;

                    if (hitsprite != -1) {
                        delta =  klabs(GetDeltaAngle(daang, sp.getAng()));
                        if (delta > 128) {
                            // don't shoot if greater than 128
                            return;
                        } else if (delta > 24) {
                            Sprite hp = boardService.getSprite(hitsprite);
                            if (hp != null) {
                                // always shoot the ground when tracking
                                // and not close
                                WeaponHitscanShootFeet(sp, hp, tmp_ptr[0].set(daz));
                                daz = tmp_ptr[0].value;
                                daang = sp.getAng();
                                daang = NORM_ANGLE(daang + RANDOM_P2(32) - 16);
                            }
                        } else {
                            // randomize the z for shots
                            daz += RANDOM_RANGE(Z(120)) - Z(60);
                            // never auto aim the angle
                            daang = sp.getAng();
                            daang = NORM_ANGLE(daang + RANDOM_P2(64) - 32);
                        }
                    }
                } else {
                    daang = 64;
                    if (WeaponAutoAimHitscan(sop.sp_num[i], tmp_ptr[0].set(daz), tmp_ptr[1].set(daang), 0) != -1) {
                        daz = tmp_ptr[0].value;
                        daang =  tmp_ptr[1].value;
                        daz += RANDOM_RANGE(Z(30)) - Z(15);
                    }
                }

                xvect = EngineUtils.sin(NORM_ANGLE(daang + 512));
                yvect = EngineUtils.sin(NORM_ANGLE(daang));
                zvect = daz;

                FAFhitscan(nx, ny, nz, nsect, // Start position
                        xvect, yvect, zvect, pHitInfo, CLIPMASK_MISSILE);

                hitsect = pHitInfo.hitsect;
                hitwall = pHitInfo.hitwall;
                hitsprite = pHitInfo.hitsprite;
                hitx = pHitInfo.hitx;
                hity = pHitInfo.hity;
                hitz = pHitInfo.hitz;

                if (RANDOM_P2(1024) < 400) {
                    InitTracerAutoTurret(sop.sp_num[i], -1, xvect >> 4, yvect >> 4, zvect >> 4);
                }

                if (hitsect == -1) {
                    continue;
                }

                if (hitsprite == -1 && hitwall < 0) {
                    Sector hsec = boardService.getSector(hitsect);
                    if (hsec != null) {
                        if (klabs(hitz - hsec.getCeilingz()) <= Z(1)) {
                            hitz += Z(16);

                            if (TEST(hsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                                continue;
                            }
                        } else if (klabs(hitz - hsec.getFloorz()) <= Z(1)) {
                            if (DTEST(hsec.getExtra(), SECTFX_LIQUID_MASK) != SECTFX_LIQUID_NONE) {
                                SpawnSplashXY(hitx, hity, hitz, hitsect);
                                continue;
                            }
                        }
                    }
                }

                if (hitwall != -1) {
                    Wall wph = boardService.getWall(hitwall);
                    if (wph != null) {
                        Sector nsec = boardService.getSector(wph.getNextsector());
                        if (nsec != null) {
                            if (TEST(nsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                                if (hitz < nsec.getCeilingz()) {
                                    return;
                                }
                            }
                        }

                        if (wph.getLotag() == TAG_WALL_BREAK) {
                            HitBreakWall(hitwall, hitx, hity, hitz, daang, 0);
                            continue;
                        }

                        QueueHole(daang, hitsect, hitwall, hitx, hity, hitz);
                    }
                }

                // hit a sprite?
                if (hitsprite != -1) {
                    hsp = boardService.getSprite(hitsprite);
                    if (hsp == null) {
                        return;
                    }

                    if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                        if (MissileHitMatch(-1, WPN_UZI, hitsprite)) {
                            continue;
                        }
                    }

                    if (TEST(hsp.getExtra(), SPRX_BREAKABLE)) {
                        HitBreakSprite(hitsprite, 0);
                        continue;
                    }

                    if (BulletHitSprite(sop.sp_num[i], hitsprite, hitsect, hitwall, hitx, hity, hitz, 0)) {
                        continue;
                    }

                    // hit a switch?
                    if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL) && (hsp.getLotag() != 0 || hsp.getHitag() != 0)) {
                        ShootableSwitch(hitsprite, -1);
                    }
                }

                j =  SpawnTurretSparks(hitsect, hitwall, hitx, hity, hitz, daang);
                DoHitscanDamage(j, hitsprite);
            }
        }
    }

    public static void InitEnemyUzi(final int SpriteNum) {
        final Sprite sp = boardService.getSprite(SpriteNum);
        final USER u = getUser(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        int daang = 0, j;
        int hitsect, hitwall, hitsprite;
        int hitx, hity, hitz, daz;
        int zh;

        // Make sprite shade brighter
        u.Vis = 128;

        engine.setspritez(SpriteNum, sp.getX(), sp.getY(), sp.getZ());

        if (u.ID == ZILLA_RUN_R0) {
            zh = SPRITEp_TOS(sp);
            zh += Z(20);
        } else {
            zh = SPRITEp_SIZE_Z(sp);
            zh -= DIV4(zh);
        }
        daz = sp.getZ() - zh;

        if (AimHitscanToTarget(SpriteNum, tmp_ptr[0].set(daz), tmp_ptr[1].set(daang), 200) != -1) {
            // set angle to player and also face player when attacking
            daz = tmp_ptr[0].value;
            daang =  tmp_ptr[1].value;

            sp.setAng(daang);
            daang += RANDOM_RANGE(24) - 12;
            daang = NORM_ANGLE(daang);
            daz += RANDOM_RANGE(Z(40)) - Z(20);
        } else {
            // couldn't shoot target for some reason

            // don't bother wasting processing 50% of the time
            if (RANDOM_P2(1024) < 512) {
                return;
            }

            daz = 0;
            daang = NORM_ANGLE(sp.getAng() + (RANDOM_P2(128)) - 64);
        }

        FAFhitscan(sp.getX(), sp.getY(), sp.getZ() - zh, sp.getSectnum(), // Start position
                EngineUtils.sin(NORM_ANGLE(daang + 512)), // X vector of 3D ang
                EngineUtils.sin(NORM_ANGLE(daang)), // Y vector of 3D ang
                daz, // Z vector of 3D ang
                pHitInfo, CLIPMASK_MISSILE);

        hitsect = pHitInfo.hitsect;
        hitwall = pHitInfo.hitwall;
        hitsprite = pHitInfo.hitsprite;
        hitx = pHitInfo.hitx;
        hity = pHitInfo.hity;
        hitz = pHitInfo.hitz;

        if (hitsect == -1) {
            return;
        }

        if (RANDOM_P2(1024 << 4) >> 4 > 700) {
            if (u.ID == TOILETGIRL_R0 || u.ID == WASHGIRL_R0 || u.ID == CARGIRL_R0) {
                SpawnShell(SpriteNum, -3);
            } else {
                SpawnShell(SpriteNum, -2); // Enemy Uzi shell
            }
        }

        if ((alternate++) > 2) {
            alternate = 0;
        }
        if (alternate == 0) {
            if (sp.getPal() == PALETTE_PLAYER3 || sp.getPal() == PALETTE_PLAYER5 || sp.getPal() == PAL_XLAT_LT_GREY || sp.getPal() == PAL_XLAT_LT_TAN) {
                PlaySound(DIGI_M60, sp, v3df_none);
            } else {
                PlaySound(DIGI_NINJAUZIATTACK, sp, v3df_none);
            }
        }

        if (hitwall != -1) {
            Wall wph = boardService.getWall(hitwall);
            if (wph != null) {
                Sector nsec = boardService.getSector(wph.getNextsector());
                if (nsec != null) {
                    if (TEST(nsec.getCeilingstat(), CEILING_STAT_PLAX)) {
                        if (hitz < nsec.getCeilingz()) {
                            return;
                        }
                    }
                }

                if (wph.getLotag() == TAG_WALL_BREAK) {
                    HitBreakWall(hitwall, hitx, hity, hitz, daang, u.ID);
                    return;
                }

                QueueHole(daang, hitsect, hitwall, hitx, hity, hitz);
            }
        }

        if (hitsprite != -1) {
            if (BulletHitSprite(SpriteNum, hitsprite, hitsect, hitwall, hitx, hity, hitz, 0)) {
                return;
            }
        }

        j =  SpawnSprite(STAT_MISSILE, UZI_SMOKE + 2, s_UziSmoke[0], hitsect, hitx, hity, hitz, daang, 0);
        Sprite wp = boardService.getSprite(j);
        USER wu = getUser(j);
        if (wp == null || wu == null) {
            return;
        }

        wp.setShade(-40);
        wp.setXrepeat(UZI_SMOKE_REPEAT);
        wp.setYrepeat(UZI_SMOKE_REPEAT);

        if (u.ID == ZOMBIE_RUN_R0) {
            SetOwner(sp.getOwner(), j);
        } else {
            SetOwner(SpriteNum, j);
        }

        wu.WaitTics = 63;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        wp.setClipdist((32 >> 2));

        j =  SpawnSprite(STAT_MISSILE, UZI_SMOKE, s_UziSmoke[0], hitsect, hitx, hity, hitz, daang, 0);
        wp = boardService.getSprite(j);
        wu = getUser(j);
        if (wp == null || wu == null) {
            return;
        }

        wp.setShade(-40);
        wp.setXrepeat(UZI_SMOKE_REPEAT);
        wp.setYrepeat(UZI_SMOKE_REPEAT);
        SetOwner(SpriteNum, j);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setClipdist(8 >> 2);

        HitscanSpriteAdjust(j, hitwall);
        DoHitscanDamage(j, hitsprite);

        j =  SpawnSprite(STAT_MISSILE, UZI_SPARK, s_UziSpark[0], hitsect, hitx, hity, hitz, daang, 0);
        wp = boardService.getSprite(j);
        wu = getUser(j);
        if (wp == null || wu == null) {
            return;
        }

        wp.setShade(-40);
        wp.setXrepeat(UZI_SPARK_REPEAT);
        wp.setYrepeat(UZI_SPARK_REPEAT);
        SetOwner(SpriteNum, j);
        wu.spal = (byte) wp.getPal();
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setClipdist(8 >> 2);

        HitscanSpriteAdjust(j, hitwall);

        if (RANDOM_P2(1024) < 100) {
            PlaySound(DIGI_RICHOCHET1, wp, v3df_none);
        } else if (RANDOM_P2(1024) < 100) {
            PlaySound(DIGI_RICHOCHET2, wp, v3df_none);
        }
    }

    public static void InitGrenade(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sprite psp = pp.getSprite();
        if (u == null || psp == null) {
            return;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz;
        int w;
        int oclipdist;
        int zvel;
        boolean auto_aim = false;

        DoPlayerBeginRecoil(pp, GRENADE_RECOIL_AMT);

        PlayerUpdateAmmo(pp, u.WeaponNum, -1);

        PlaySound(DIGI_30MMFIRE, pp, v3df_dontpan | v3df_doppler);

        // Make sprite shade brighter
        u.Vis = 128;

        nx = pp.posx;
        ny = pp.posy;
        nz = pp.posz + pp.bob_z + Z(8);

        // Spawn a shot
        // Inserting and setting up variables
        w =  SpawnSprite(STAT_MISSILE, GRENADE, s_Grenade[0][0], pp.cursectnum, nx, ny, nz, pp.getAnglei(), GRENADE_VELOCITY);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        // don't throw it as far if crawling
        if (TEST(pp.Flags, PF_CRAWLING)) {
            wp.setXvel(wp.getXvel() - DIV4(wp.getXvel()));
        }

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_Grenade);
        wu.Flags |= (SPR_XFLIP_TOGGLE);

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(32);
        wp.setXrepeat(32);
        wp.setShade(-15);
        // wp.clipdist = 80L>>2;
        wp.setClipdist(32 >> 2);
        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 200;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wu.Counter = 0;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK));

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        wp.setZvel( ((100 - pp.getHorizi()) * HORIZ_MULT));

        oclipdist =  psp.getClipdist();
        psp.setClipdist(0);

        wp.setAng(NORM_ANGLE(wp.getAng() + 512));
        HelpMissileLateral(w, 800);
        wp.setAng(NORM_ANGLE(wp.getAng() - 512));

        // don't do smoke for this movement
        wu.Flags |= (SPR_BOUNCE);
        MissileSetPos(w, DoGrenade, 1000);
        wu.Flags &= ~(SPR_BOUNCE);

        psp.setClipdist(oclipdist);

        zvel = wp.getZvel();
        if (WeaponAutoAim(pp.PlayerSprite, w, 32, 0) >= 0) {
            auto_aim = true;
        }
        wp.setZvel( zvel);

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

        if (!auto_aim) {
            // adjust xvel according to player velocity
            wu.xchange += pp.xvect >> 14;
            wu.ychange += pp.yvect >> 14;
        }

        wu.Counter2 = 1; // Phosphorus Grenade
    }

    public static void InitSpriteGrenade(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz;
        int w;

        PlaySound(DIGI_30MMFIRE, sp, v3df_dontpan | v3df_doppler);

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ() - Z(40);

        // Spawn a shot
        // Inserting and setting up variables
        w =  SpawnSprite(STAT_MISSILE, GRENADE, s_Grenade[0][0], sp.getSectnum(), nx, ny, nz, sp.getAng(), GRENADE_VELOCITY);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_Grenade);
        wu.Flags |= (SPR_XFLIP_TOGGLE);

        if (u.ID == ZOMBIE_RUN_R0) {
            SetOwner(sp.getOwner(), w);
        } else {
            SetOwner(SpriteNum, w);
        }
        wp.setYrepeat(32);
        wp.setXrepeat(32);
        wp.setShade(-15);
        // wp.clipdist = 80L>>2;
        wp.setClipdist(32 >> 2);
        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 200;
        wu.ceiling_dist =  Z(3);
        wu.floor_dist =  Z(3);
        wu.Counter = 0;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK));

        // wp.zvel = (-RANDOM_RANGE(100) * HORIZ_MULT);
        wp.setZvel(-2000);

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = wp.getZvel();

        wp.setAng(NORM_ANGLE(wp.getAng() + 512));
        HelpMissileLateral(w, 800);
        wp.setAng(NORM_ANGLE(wp.getAng() - 512));

        // don't do smoke for this movement
        wu.Flags |= (SPR_BOUNCE);
        MissileSetPos(w, DoGrenade, 400);
        wu.Flags &= ~(SPR_BOUNCE);
    }

    public static void InitMine(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz;
        int w;
        int dot;

        PlayerUpdateAmmo(pp, u.WeaponNum, -1);

        PlaySound(DIGI_MINETHROW, pp, v3df_dontpan | v3df_doppler);

        nx = pp.posx;
        ny = pp.posy;
        nz = pp.posz + pp.bob_z + Z(8);

        // Spawn a shot
        // Inserting and setting up variables
        w =  SpawnSprite(STAT_MISSILE, MINE, s_Mine[0], pp.cursectnum, nx, ny, nz, pp.getAnglei(), MINE_VELOCITY);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        SetOwner(pp.PlayerSprite, w);
        wp.setYrepeat(32);
        wp.setXrepeat(32);
        wp.setShade(-15);
        wp.setClipdist(128 >> 2);
        wp.setZvel( ((100 - pp.getHorizi()) * HORIZ_MULT));
        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 200;
        wu.ceiling_dist =  Z(5);
        wu.floor_dist =  Z(5);
        wu.Counter = 0;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        wu.spal = u.spal;
        wp.setPal(wu.spal); // Set sticky color

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        MissileSetPos(w, DoMine, 800);

        wu.zchange = wp.getZvel() >> 1;
        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());

        dot = DOT_PRODUCT_2D(pp.xvect, pp.yvect, EngineUtils.sin(NORM_ANGLE(pp.getAnglei() + 512)), EngineUtils.sin(pp.getAnglei()));

        // don't adjust for strafing
        if (klabs(dot) > 10000) {
            // adjust xvel according to player velocity
            wu.xchange += pp.xvect >> 13;
            wu.ychange += pp.yvect >> 13;
        }
    }

    public static void HelpMissileLateral(int Weapon, int dist) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        int xchange, ychange;
        int old_xvel;
        int old_clipdist;

        old_xvel = sp.getXvel();
        old_clipdist =  sp.getClipdist();

        sp.setXvel( dist);
        xchange = MOVEx(sp.getXvel(), sp.getAng());
        ychange = MOVEy(sp.getXvel(), sp.getAng());

        sp.setClipdist(32 >> 2);

        u.moveSpriteReturn = move_missile(Weapon, xchange, ychange, 0, Z(16), Z(16), 0, 1);

        sp.setXvel(old_xvel);
        sp.setClipdist(old_clipdist);

        u.ox = sp.getX();
        u.oy = sp.getY();
        u.oz = sp.getZ();
    }

    public static void InitFireball(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sprite psp = pp.getSprite();
        if (u == null || psp == null) {
            return;
        }

        Sprite wp;
        int nx = 0, ny = 0, nz;
        int w;
        USER wu;
        int oclipdist;
        int zvel;

        PlayerUpdateAmmo(pp, WPN_HOTHEAD, -1);

        PlaySound(DIGI_HEADFIRE, pp, v3df_none);

        // Make sprite shade brighter
        u.Vis = 128;

        nx += pp.posx;
        ny += pp.posy;

        nz = pp.posz + pp.bob_z + Z(15);

        w =  SpawnSprite(STAT_MISSILE, FIREBALL1, s_Fireball[0], pp.cursectnum, nx, ny, nz, pp.getAnglei(), FIREBALL_VELOCITY);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        wp.setHitag(LUMINOUS); // Always full brightness
        wp.setXrepeat(40);
        wp.setYrepeat(40);
        wp.setShade(-40);
        wp.setClipdist(32 >> 2);
        SetOwner(pp.PlayerSprite, w);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wu.Radius = 100;

        wu.ceiling_dist =  Z(6);
        wu.floor_dist =  Z(6);
        zvel = ((100 - pp.getHorizi()) * (240));

        // wu.RotNum = 5;
        // NewStateGroup(w, &sg_Fireball);
        // SET(wu.Flags, SPR_XFLIP_TOGGLE);

        // at certain angles the clipping box was big enough to block the
        // initial positioning of the fireball.
        oclipdist =  psp.getClipdist();
        psp.setClipdist(0);

        wp.setAng(NORM_ANGLE(wp.getAng() + 512));
        HelpMissileLateral(w, 2100);
        wp.setAng(NORM_ANGLE(wp.getAng() - 512));

        if (TEST(pp.Flags, PF_DIVING) || SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        if (TestMissileSetPos(w, DoFireball, 1200, mulscale(zvel, 44000, 16))) {
            psp.setClipdist(oclipdist);
            KillSprite(w);
            return;
        }

        psp.setClipdist(oclipdist);

        wp.setZvel( (zvel >> 1));
        if (WeaponAutoAimZvel(pp.PlayerSprite, w, tmp_ptr[0].set(zvel), 32, 0) == -1) {
            wp.setAng(NORM_ANGLE(wp.getAng() - 9));
        }

        wu.xchange = MOVEx(wp.getXvel(), wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(), wp.getAng());
        wu.zchange = zvel;
    }

    public static void WarpToUnderwater(LONGp sectnum, LONGp x, LONGp y, LONGp z) {
        Sect_User sectu = getSectUser(sectnum.value);
        Sprite under_sp = null, over_sp = null;
        boolean Found = false;

        int sx, sy;

        // 0 not valid for water match tags
        if (sectu == null || sectu.number == 0) {
            return;
        }

        // search for DIVE_AREA "over" sprite for reference point
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_DIVE_AREA); node != null; node = nexti) {
            nexti = node.getNext();
            over_sp = node.get();
            Sector sec = boardService.getSector(over_sp.getSectnum());
            Sect_User su = getSectUser(over_sp.getSectnum());

            if (sec != null && TEST(sec.getExtra(), SECTFX_DIVE_AREA) && su != null && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return;
        }

        Found = false;

        // search for UNDERWATER "under" sprite for reference point
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_UNDERWATER); node != null; node = nexti) {
            nexti = node.getNext();
            under_sp = node.get();
            Sector sec = boardService.getSector(under_sp.getSectnum());
            Sect_User su = getSectUser(under_sp.getSectnum());

            if (sec != null && TEST(sec.getExtra(), SECTFX_UNDERWATER) && su != null && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return;
        }

        // get the offset from the sprite
        sx = over_sp.getX() - x.value;
        sy = over_sp.getY() - y.value;

        // update to the new x y position
        x.value = under_sp.getX() - sx;
        y.value = under_sp.getY() - sy;

        if (GetOverlapSector(x.value, y.value, tmp_ptr[0].set(over_sp.getSectnum()), tmp_ptr[1].set(under_sp.getSectnum())) == 2) {
            sectnum.value = tmp_ptr[1].value;
        } else {
            sectnum.value = tmp_ptr[0].value;
        }

        Sector sec = boardService.getSector(under_sp.getSectnum());
        if (sec != null) {
            z.value = sec.getCeilingz() + Z(1);
        }
    }

    public static void WarpToSurface(LONGp sectnum, LONGp x, LONGp y, LONGp z) {
        Sect_User sectu = getSectUser(sectnum.value);
        int sx, sy;

        Sprite under_sp = null, over_sp = null;
        boolean Found = false;

        // 0 not valid for water match tags
        if (sectu == null || sectu.number == 0) {
            return;
        }

        // search for UNDERWATER "under" sprite for reference point
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_UNDERWATER); node != null; node = nexti) {
            nexti = node.getNext();
            under_sp = node.get();
            Sector sec = boardService.getSector(under_sp.getSectnum());
            Sect_User su = getSectUser(under_sp.getSectnum());

            if (sec != null && TEST(sec.getExtra(), SECTFX_UNDERWATER) && su != null && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return;
        }

        Found = false;

        // search for DIVE_AREA "over" sprite for reference point
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_DIVE_AREA); node != null; node = nexti) {
            nexti = node.getNext();
            over_sp = node.get();
            Sector sec = boardService.getSector(over_sp.getSectnum());
            Sect_User su = getSectUser(over_sp.getSectnum());

            if (sec != null && TEST(sec.getExtra(), SECTFX_DIVE_AREA) && su != null && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return;
        }

        // get the offset from the under sprite
        sx = under_sp.getX() - x.value;
        sy = under_sp.getY() - y.value;

        // update to the new x y position
        x.value = over_sp.getX() - sx;
        y.value = over_sp.getY() - sy;

        if (GetOverlapSector(x.value, y.value, tmp_ptr[0].set(over_sp.getSectnum()), tmp_ptr[1].set(under_sp.getSectnum())) != 0) {
            sectnum.value = tmp_ptr[0].value;
        }

        Sector sec = boardService.getSector(over_sp.getSectnum());
        if (sec != null) {
            z.value = sec.getFloorz() - Z(2);
        }

    }

    public static void SpriteWarpToUnderwater(int spi) {
        Sprite sp = boardService.getSprite(spi);
        USER u = getUser(spi);
        if (sp == null) {
            return;
        }

        Sect_User sectu = getSectUser(sp.getSectnum());
        Sprite under_sp = null, over_sp = null;
        boolean Found = false;
        int sx, sy;

        // 0 not valid for water match tags
        if (sectu == null || sectu.number == 0) {
            return;
        }

        // search for DIVE_AREA "over" sprite for reference point
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_DIVE_AREA); node != null; node = nexti) {
            nexti = node.getNext();
            over_sp = node.get();
            Sector sec = boardService.getSector(over_sp.getSectnum());
            Sect_User su = getSectUser(over_sp.getSectnum());

            if (sec != null && TEST(sec.getExtra(), SECTFX_DIVE_AREA) && su != null && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return;
        }

        Found = false;

        // search for UNDERWATER "under" sprite for reference point
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_UNDERWATER); node != null; node = nexti) {
            nexti = node.getNext();
            under_sp = node.get();
            Sector sec = boardService.getSector(under_sp.getSectnum());
            Sect_User su = getSectUser(under_sp.getSectnum());

            if (sec != null && TEST(sec.getExtra(), SECTFX_UNDERWATER) && su != null && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return;
        }

        // get the offset from the sprite
        sx = over_sp.getX() - sp.getX();
        sy = over_sp.getY() - sp.getY();

        // update to the new x y position
        sp.setX(under_sp.getX() - sx);
        sp.setY(under_sp.getY() - sy);

        if (GetOverlapSector(sp.getX(), sp.getY(), tmp_ptr[0].set(over_sp.getSectnum()), tmp_ptr[1].set(under_sp.getSectnum())) == 2) {
            engine.changespritesect(spi,  tmp_ptr[1].value);
        } else {
            engine.changespritesect(spi,  tmp_ptr[0].value);
        }

        // sp.z = boardService.getSector(under_sp.sectnum).ceilingz + Z(6);
        Sector sec = boardService.getSector(under_sp.getSectnum());
        if (sec != null && u != null) {
            sp.setZ(sec.getCeilingz() + u.ceiling_dist + Z(1));

            u.ox = sp.getX();
            u.oy = sp.getY();
            u.oz = sp.getZ();
        }

    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean SpriteWarpToSurface(int spi) {
        Sprite sp = boardService.getSprite(spi);
        USER u = getUser(spi);
        if (sp == null) {
            return false;
        }

        Sect_User sectu = getSectUser(sp.getSectnum());

        int sx, sy;

        Sprite under_sp = null, over_sp = null;
        boolean Found = false;

        // 0 not valid for water match tags
        if (sectu == null || sectu.number == 0) {
            return (false);
        }

        // search for UNDERWATER "under" sprite for reference point
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_UNDERWATER); node != null; node = nexti) {
            nexti = node.getNext();
            under_sp = node.get();
            Sector sec = boardService.getSector(under_sp.getSectnum());
            Sect_User su = getSectUser(under_sp.getSectnum());

            if (sec != null && TEST(sec.getExtra(), SECTFX_UNDERWATER) && su != null && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return false;
        }

        if (under_sp.getLotag() == 0) {
            return (false);
        }

        Found = false;

        // search for DIVE_AREA "over" sprite for reference point
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_DIVE_AREA); node != null; node = nexti) {
            nexti = node.getNext();
            over_sp = node.get();
            Sector sec = boardService.getSector(over_sp.getSectnum());
            Sect_User su = getSectUser(over_sp.getSectnum());

            if (sec != null && TEST(sec.getExtra(), SECTFX_DIVE_AREA) && su != null && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return false;
        }

        // get the offset from the under sprite
        sx = under_sp.getX() - sp.getX();
        sy = under_sp.getY() - sp.getY();

        // update to the new x y position
        sp.setX(over_sp.getX() - sx);
        sp.setY(over_sp.getY() - sy);

        if (GetOverlapSector(sp.getX(), sp.getY(), tmp_ptr[0].set(over_sp.getSectnum()), tmp_ptr[1].set(under_sp.getSectnum())) != 0) {
            engine.changespritesect(spi,  tmp_ptr[0].value);
        }

        Sector sec = boardService.getSector(over_sp.getSectnum());
        if (sec != null && u != null) {
            sp.setZ(sec.getFloorz() - Z(2));

            // set z range and wade depth so we know how high to set view
            DoActorZrange(spi);
            MissileWaterAdjust(spi);

            u.ox = sp.getX();
            u.oy = sp.getY();
            u.oz = sp.getZ();
        }

        return (true);
    }

    public static void SpawnSplash(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        Sector sectp = boardService.getSector(sp.getSectnum());
        if (sectp == null) {
            return;
        }

        if (Prediction) {
            return;
        }

        if ((DTEST(sectp.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_NONE)) {
            return;
        }

        if (TEST(sectp.getFloorstat(), FLOOR_STAT_PLAX)) {
            return;
        }

        PlaySound(DIGI_SPLASH1, sp, v3df_none);

        DoActorZrange(SpriteNum);
        MissileWaterAdjust(SpriteNum);

        int w =  SpawnSprite(STAT_MISSILE, SPLASH, s_Splash[0], sp.getSectnum(), sp.getX(), sp.getY(), u.loz, sp.getAng(), 0);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        if (DTEST(sectp.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_LAVA) {
            wu.spal = PALETTE_RED_LIGHTING;
            wp.setPal(PALETTE_RED_LIGHTING);
        }

        wp.setXrepeat(45);
        wp.setYrepeat(42);
        wp.setShade((byte) (sectp.getFloorshade() - 10));
    }

    public static void SpawnSplashXY(int hitx, int hity, int hitz, int sectnum) {
        USER wu;
        Sprite wp;
        int w;

        Sector sectp;

        if (Prediction) {
            return;
        }

        sectp = boardService.getSector(sectnum);
        if (sectp == null) {
            return;
        }

        if ((DTEST(sectp.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_NONE)) {
            return;
        }

        if (TEST(sectp.getFloorstat(), FLOOR_STAT_PLAX)) {
            return;
        }

        w = SpawnSprite(STAT_MISSILE, SPLASH, s_Splash[0], sectnum, hitx, hity, hitz, 0, 0);
        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        if (DTEST(sectp.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_LAVA) {
            wu.spal = PALETTE_RED_LIGHTING;
            wp.setPal(PALETTE_RED_LIGHTING);
        }

        wp.setXrepeat(45);
        wp.setYrepeat(42);
        wp.setShade((byte) (sectp.getFloorshade() - 10));
    }

    public static void MissileHitDiveArea(final int SpriteNum) {
        final Sprite sp = boardService.getSprite(SpriteNum);
        final USER u = getUser(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        int hitsect;
        // correctly set underwater bit for missiles
        // in Stacked water areas.
        if (FAF_ConnectArea(sp.getSectnum())) {
            if (SectorIsUnderwaterArea(sp.getSectnum())) {
                u.Flags |= (SPR_UNDERWATER);
            } else {
                u.Flags &= ~(SPR_UNDERWATER);
            }
        }

        if (u.moveSpriteReturn == 0) {
            return;
        }

        if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_SECTOR) {
            hitsect = NORM_HIT_INDEX(u.moveSpriteReturn);
            Sector hsec = boardService.getSector(hitsect);
            if (hsec == null) {
                return;
            }

            if (SpriteInDiveArea(sp)) {
                // make sure you are close to the floor
                if (sp.getZ() < DIV2(u.hiz + u.loz)) {
                    return;
                }

                // Check added by Jim because of sprite bridge over water
                if (sp.getZ() < (hsec.getFloorz() - Z(20))) {
                    return;
                }

                u.Flags |= (SPR_UNDERWATER);
                SpawnSplash(SpriteNum);
                SpriteWarpToUnderwater(SpriteNum);
                u.moveSpriteReturn = 0;
                PlaySound(DIGI_PROJECTILEWATERHIT, sp, v3df_none);
            } else if (SpriteInUnderwaterArea(sp)) {
                // make sure you are close to the ceiling
                if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                    return;
                }

                u.Flags &= ~(SPR_UNDERWATER);
                if (!SpriteWarpToSurface(SpriteNum)) {
                    return;
                }
                SpawnSplash(SpriteNum);
                u.moveSpriteReturn = 0;
            }
        }

    }

    public static int SpawnBubble(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum), bp;
        if (sp == null) {
            return -1;
        }

        if (Prediction) {
            return (-1);
        }

        int b = SpawnSprite(STAT_MISSILE, BUBBLE, s_Bubble[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
        bp = boardService.getSprite(b);
        USER bu = getUser(b);

        if (bp == null || bu == null) {
            return -1;
        }

        // PlaySound(DIGI_BUBBLES, &sp.x, &sp.y, &sp.z, v3df_none);

        bp.setXrepeat( (8 + (RANDOM_P2(8 << 8) >> 8)));
        bp.setYrepeat(bp.getXrepeat());
        bu.sx = bp.getXrepeat();
        bu.sy = bp.getYrepeat();
        bu.ceiling_dist =  Z(1);
        bu.floor_dist =  Z(1);
        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec != null) {
            bp.setShade((byte) (sec.getFloorshade() - 10));
        }
        bu.WaitTics = 120 * 120;
        bp.setZvel(512);
        bp.setClipdist(12 >> 2);
        bp.setCstat(bp.getCstat() | (CSTAT_SPRITE_YCENTER));
        bu.Flags |= (SPR_UNDERWATER);
        bp.setShade(-60); // Make em brighter

        return (b);
    }

    public static void DoVehicleSmoke(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setZ(sp.getZ() - sp.getZvel());
        sp.setX(sp.getX() + u.xchange);
        sp.setY(sp.getY() + u.ychange);
    }

    public static void DoWaterSmoke(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null) {
            sp.setZ(sp.getZ() - sp.getZvel());
        }
    }

    public static void SpawnVehicleSmoke(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        if (MoveSkip2) {
            return;
        }

        int newsp = SpawnSprite(STAT_MISSILE, PUFF, s_VehicleSmoke[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ() - RANDOM_P2(Z(8)), sp.getAng(), 0);
        Sprite np = boardService.getSprite(newsp);
        USER nu = getUser(newsp);
        if (np == null || nu == null) {
            return;
        }

        nu.WaitTics = 120;
        np.setShade(-40);
        np.setXrepeat(64);
        np.setYrepeat(64);
        np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
        np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        if (RANDOM_P2(1024) < 512) {
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_XFLIP));
        }
        if (RANDOM_P2(1024) < 512) {
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_YFLIP));
        }

        np.setAng( RANDOM_P2(2048));
        np.setXvel( RANDOM_P2(32));
        nu.xchange = MOVEx(np.getXvel(), np.getAng());
        nu.ychange = MOVEy(np.getXvel(), np.getAng());
        np.setZvel( (Z(4) + RANDOM_P2(Z(4))));
    }

    public static void SpawnSmokePuff(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        int newsp = SpawnSprite(STAT_MISSILE, PUFF, s_WaterSmoke[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ() - RANDOM_P2(Z(8)), sp.getAng(), 0);
        Sprite np = boardService.getSprite(newsp);
        USER nu = getUser(newsp);
        if (np == null || nu == null) {
            return;
        }

        nu.WaitTics = 120;
        np.setShade(-40);
        np.setXrepeat(64);
        np.setYrepeat(64);
        np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));
        np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        if (RANDOM_P2(1024) < 512) {
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_XFLIP));
        }
        if (RANDOM_P2(1024) < 512) {
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_YFLIP));
        }

        np.setAng( RANDOM_P2(2048));
        np.setXvel( RANDOM_P2(32));
        nu.xchange = MOVEx(np.getXvel(), np.getAng());
        nu.ychange = MOVEy(np.getXvel(), np.getAng());
        np.setZvel( (Z(1) + RANDOM_P2(Z(2))));
    }

    public static boolean DoBubble(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        sp.setZ(sp.getZ() - sp.getZvel());
        sp.setZvel(sp.getZvel() + 32);

        if (sp.getZvel() > 768) {
            sp.setZvel(768);
        }

        u.sx += 1;
        u.sy += 1;

        if (u.sx > 32) {
            u.sx = 32;
            u.sy = 32;
        }

        sp.setXrepeat( (u.sx + (RANDOM_P2(8 << 8) >> 8) - 4));
        sp.setYrepeat( (u.sy + (RANDOM_P2(8 << 8) >> 8) - 4));

        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec != null && sp.getZ() < sec.getCeilingz()) {
            if (SectorIsUnderwaterArea(u.hi_sectp)) {
                if (!SpriteWarpToSurface(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (true);
                }

                u.Flags &= ~(SPR_UNDERWATER);
                // stick around above water for this long
                u.WaitTics =  (RANDOM_P2(64 << 8) >> 8);
            } else {
                KillSprite(SpriteNum);
                return (true);
            }
        }

        if ((u.WaitTics -= MISSILEMOVETICS) <= 0) {
            KillSprite(SpriteNum);
            return (true);
        }

        return (false);
    }

    // this needs to be called before killsprite
// whenever killing a sprite that you aren't completely sure what it is, like
// with the drivables, copy sectors, break sprites, etc
    public static void SpriteQueueDelete(int SpriteNum) {
        int i;

        for (i = 0; i < MAX_STAR_QUEUE; i++) {
            if (StarQueue[i] == SpriteNum) {
                StarQueue[i] = -1;
            }
        }

        for (i = 0; i < MAX_HOLE_QUEUE; i++) {
            if (HoleQueue[i] == SpriteNum) {
                HoleQueue[i] = -1;
            }
        }

        for (i = 0; i < MAX_WALLBLOOD_QUEUE; i++) {
            if (WallBloodQueue[i] == SpriteNum) {
                WallBloodQueue[i] = -1;
            }
        }

        for (i = 0; i < MAX_FLOORBLOOD_QUEUE; i++) {
            if (FloorBloodQueue[i] == SpriteNum) {
                FloorBloodQueue[i] = -1;
            }
        }

        for (i = 0; i < MAX_GENERIC_QUEUE; i++) {
            if (GenericQueue[i] == SpriteNum) {
                GenericQueue[i] = -1;
            }
        }

        for (i = 0; i < MAX_LOWANGS_QUEUE; i++) {
            if (LoWangsQueue[i] == SpriteNum) {
                LoWangsQueue[i] = -1;
            }
        }
    }

    public static void QueueReset() {
        int i;
        StarQueueHead = 0;
        HoleQueueHead = 0;
        WallBloodQueueHead = 0;
        FloorBloodQueueHead = 0;
        GenericQueueHead = 0;
        LoWangsQueueHead = 0;

        for (i = 0; i < MAX_STAR_QUEUE; i++) {
            StarQueue[i] = -1;
        }

        for (i = 0; i < MAX_HOLE_QUEUE; i++) {
            HoleQueue[i] = -1;
        }

        for (i = 0; i < MAX_WALLBLOOD_QUEUE; i++) {
            WallBloodQueue[i] = -1;
        }

        for (i = 0; i < MAX_FLOORBLOOD_QUEUE; i++) {
            FloorBloodQueue[i] = -1;
        }

        for (i = 0; i < MAX_GENERIC_QUEUE; i++) {
            GenericQueue[i] = -1;
        }

        for (i = 0; i < MAX_LOWANGS_QUEUE; i++) {
            LoWangsQueue[i] = -1;
        }
    }

    public static boolean TestDontStick(int SpriteNum, int ignoredSec, int hitwall, int ignoredZ) {
        if (hitwall < 0) {
            USER u = getUser(SpriteNum);
            if (u != null) {
                hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
            }
        }

        Wall wp = boardService.getWall(hitwall);
        if (wp == null) {
            return false;
        }

        if (TEST(wp.getExtra(), WALLFX_DONT_STICK)) {
            return (true);
        }

        // if blocking red wallo
        return TEST(wp.getCstat(), CSTAT_WALL_BLOCK) && wp.getNextwall() >= 0;
    }

    public static boolean TestDontStickSector(int hitsect) {
        Sector hsec = boardService.getSector(hitsect);
        if (hsec != null) {
            return TEST(hsec.getExtra(), SECTFX_DYNAMIC_AREA | SECTFX_SECTOR_OBJECT);
        }
        return (false);
    }

    public static void QueueStar(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        if (TestDontStick(SpriteNum, -1, -1, sp.getZ())) {
            KillSprite(SpriteNum);
            return;
        }

        // can and should kill the user portion of the star
        if (StarQueue[StarQueueHead] == -1) {
            // new star
            USER u = getUser(SpriteNum);
            if (u != null) {
                setUser(SpriteNum, null);
            }
            change_sprite_stat(SpriteNum, STAT_STAR_QUEUE);
            StarQueue[StarQueueHead] =  SpriteNum;
        } else {
            // move old star to new stars place
            Sprite osp = boardService.getSprite(StarQueue[StarQueueHead]);
            if (osp != null) {
                osp.setX(sp.getX());
                osp.setY(sp.getY());
                osp.setZ(sp.getZ());
                engine.changespritesect(StarQueue[StarQueueHead], sp.getSectnum());
                KillSprite(SpriteNum);
            }
        }

        StarQueueHead = ((StarQueueHead + 1) & (MAX_STAR_QUEUE - 1));
    }

    public static void QueueHole(int ignoredAng, int hitsect, int hitwall, int hitx, int hity, int hitz) {
        int wall_ang;
        int SpriteNum;
        int nx, ny;
        Sprite sp;
        int sectnum;

        if (TestDontStick(-1, hitsect, hitwall, hitz)) {
            return;
        }

        if (HoleQueue[HoleQueueHead] == -1) {
            SpriteNum = COVERinsertsprite(hitsect, STAT_HOLE_QUEUE);
            HoleQueue[HoleQueueHead] =  SpriteNum;
        } else {
            SpriteNum = HoleQueue[HoleQueueHead];
        }

        HoleQueueHead = ((HoleQueueHead + 1) & (MAX_HOLE_QUEUE - 1));

        sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        sp.setOwner(-1);
        sp.setXrepeat(16);
        sp.setYrepeat(16);
        sp.setCstat(0);
        sp.setPal(0);
        sp.setShade(0);
        sp.setExtra(0);
        sp.setClipdist(0);
        sp.setXoffset(0);
        sp.setYoffset(0);
        sp.setX(hitx);
        sp.setY(hity);
        sp.setZ(hitz);
        sp.setPicnum(2151);
        engine.changespritesect(SpriteNum, hitsect);

        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_WALL));
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_ONE_SIDE));
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        Wall wal = boardService.getWall(hitwall);
        if (wal == null) {
            return;
        }

        wall_ang = NORM_ANGLE(wal.getWallAngle() + 512);
        sp.setAng(wall_ang);

        // move it back some
        nx = EngineUtils.sin((512 + sp.getAng()) & 2047) << 4;
        ny = EngineUtils.sin(sp.getAng()) << 4;

        sectnum = sp.getSectnum();

        clipmoveboxtracenum = 1;
        engine.clipmove(sp.getX(), sp.getY(), sp.getZ(), sectnum, nx, ny, 0, 0, 0, CLIPMASK_MISSILE);

        sp.setX(clipmove_x);
        sp.setY(clipmove_y);
        sp.setZ(clipmove_z);
        sectnum = clipmove_sectnum;

        clipmoveboxtracenum = 3;

        if (sp.getSectnum() != sectnum) {
            engine.changespritesect(SpriteNum, sectnum);
        }

    }

    public static boolean QueueFloorBlood(int hitsprite) {
        Sprite hsp = boardService.getSprite(hitsprite);
        USER u = getUser(hitsprite);
        if (hsp == null || u == null) {
            return false;
        }

        int SpriteNum;
        Sprite sp;
        Sector sectp = boardService.getSector(hsp.getSectnum());
        if (sectp == null) {
            return false;
        }

        if (TEST(sectp.getExtra(), SECTFX_SINK) || TEST(sectp.getExtra(), SECTFX_CURRENT)) {
            return false; //(-1); // No blood in water or current areas
        }

        if (TEST(u.Flags, SPR_UNDERWATER) || SpriteInUnderwaterArea(hsp) || SpriteInDiveArea(hsp)) {
            return false; //(-1); // No blood underwater!
        }

        if (DTEST(sectp.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_WATER) {
            return false; //(-1); // No prints liquid areas!
        }

        if (DTEST(sectp.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_LAVA) {
            return false; //(-1); // Not in lave either
        }

        if (TestDontStickSector(hsp.getSectnum())) {
            return false; //(-1); // Not on special sectors you don't
        }

        if (FloorBloodQueue[FloorBloodQueueHead] != -1) {
            KillSprite(FloorBloodQueue[FloorBloodQueueHead]);
        }

        SpriteNum =  SpawnSprite(STAT_SKIP4, FLOORBLOOD1, s_FloorBlood1[0], hsp.getSectnum(), hsp.getX(), hsp.getY(), hsp.getZ(), hsp.getAng(), 0);
        sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return false;
        }

        FloorBloodQueue[FloorBloodQueueHead] = SpriteNum;
        FloorBloodQueueHead =  ((FloorBloodQueueHead + 1) & (MAX_FLOORBLOOD_QUEUE - 1));

        sp.setOwner(-1);
        // Stupid hack to fix the blood under the skull to not show through
        // x,y repeat of floor blood MUST be smaller than the sprite above it or
        // clipping probs.
        if (u.ID == GORE_Head) {
            sp.setHitag(9995);
        } else {
            sp.setHitag(0);
        }
        sp.setXrepeat(8);
        sp.setYrepeat(8);

        sp.setCstat(0);
        sp.setPal(0);
        sp.setShade(0);
        sp.setExtra(0);
        sp.setClipdist(0);
        sp.setXoffset(0);
        sp.setYoffset(0);

        sp.setX(hsp.getX());
        sp.setY(hsp.getY());
        sp.setZ(hsp.getZ() - Z(1));
        sp.setAng( RANDOM_P2(2048)); // Just make it any old angle
        sp.setShade(sp.getShade() - 5); // Brighten it up just a bit

        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_FLOOR));
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_ONE_SIDE));
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        u.Flags &= ~(SPR_SHADOW);

        return true;
    }

    public static void QueueFootPrint(int hitsprite) {
        Sprite hsp = boardService.getSprite(hitsprite);
        USER u = getUser(hitsprite);
        if (hsp == null || u == null) {
            return;
        }

        int SpriteNum;
        Sprite sp;
        USER nu;
        int rnd_num;
        boolean Found = false;

        Sector sectp = boardService.getSector(hsp.getSectnum());
        if (sectp == null) {
            return;
        }

        if (TEST(sectp.getExtra(), SECTFX_SINK) || TEST(sectp.getExtra(), SECTFX_CURRENT)) {
            return; // No blood in water or current areas
        }

        Sector psec = boardService.getSector(Player[u.PlayerP].cursectnum);
        if (u.PlayerP != -1 && psec != null) {
            if (TEST(Player[u.PlayerP].Flags, PF_DIVING)) {
                Found = true;
            }

            // Stupid masked floor stuff! Damn your weirdness!
            if (TEST(psec.getCeilingstat(), CEILING_STAT_PLAX)) {
                Found = true;
            }
            if (TEST(psec.getFloorstat(), CEILING_STAT_PLAX)) {
                Found = true;
            }
        }

        if (TEST(u.Flags, SPR_UNDERWATER) || SpriteInUnderwaterArea(hsp) || Found || SpriteInDiveArea(hsp)) {
            return; // No prints underwater!
        }

        if (DTEST(sectp.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_WATER) {
            return; // No prints liquid areas!
        }

        if (DTEST(sectp.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_LAVA) {
            return; // Not in lave either
        }

        if (TestDontStickSector(hsp.getSectnum())) {
            return; // Not on special sectors you don't
        }

        // So, are we like, done checking now!?
        if (FloorBloodQueue[FloorBloodQueueHead] != -1) {
            KillSprite(FloorBloodQueue[FloorBloodQueueHead]);
        }

        rnd_num =  RANDOM_RANGE(1024);
        State state;
        int id;
        if (rnd_num > 683) {
            id = FOOTPRINT1;
            state = s_FootPrint1[0];
        } else if (rnd_num > 342) {
            id = FOOTPRINT2;
            state = s_FootPrint2[0];
        } else {
            id = FOOTPRINT3;
            state = s_FootPrint3[0];
        }

        SpriteNum =  SpawnSprite(STAT_FLOORBLOOD_QUEUE, id, state, hsp.getSectnum(), hsp.getX(), hsp.getY(), hsp.getZ(), hsp.getAng(), 0);
        if (SpriteNum == -1) {
            return;
        }

        FloorBloodQueue[FloorBloodQueueHead] = SpriteNum;
        FloorBloodQueueHead =  ((FloorBloodQueueHead + 1) & (MAX_FLOORBLOOD_QUEUE - 1));

        // Decrease footprint count
        if (u.PlayerP != -1) {
            Player[u.PlayerP].NumFootPrints--;
        }

        sp = boardService.getSprite(SpriteNum);
        nu = getUser(SpriteNum);
        if (sp == null || nu == null) {
            return;
        }

        sp.setHitag(0);
        sp.setOwner(-1);
        sp.setXrepeat(48);
        sp.setYrepeat(54);
        sp.setCstat(0);
        sp.setPal(0);
        sp.setShade(0);
        sp.setExtra(0);
        sp.setClipdist(0);
        sp.setXoffset(0);
        sp.setYoffset(0);
        sp.setX(hsp.getX());
        sp.setY(hsp.getY());
        sp.setZ(hsp.getZ());
        sp.setAng(hsp.getAng());
        nu.Flags &= ~(SPR_SHADOW);
        if (FootMode == FootType.BLOOD_FOOT) {
            nu.spal = (byte) PALETTE_PLAYER3; // Turn blue to blood red
        } else {
            nu.spal = (byte) PALETTE_PLAYER1; // Gray water
        }
        sp.setPal(nu.spal);

        // Alternate the feet
        left_foot = !left_foot;
        if (left_foot) {
            sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_XFLIP));
        }
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_FLOOR));
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_ONE_SIDE));
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

    }

    public static int QueueWallBlood(final int hitsprite, int ang) {
        Sprite hsp = boardService.getSprite(hitsprite);
        USER u = getUser(hitsprite);
        if (hsp == null || u == null) {
            return 0;
        }

        int wall_ang, dang;
        int SpriteNum;
        int nx, ny;
        Sprite sp;
        int sectnum;
        int rndnum;
        int daz;

        if (TEST(u.Flags, SPR_UNDERWATER) || SpriteInUnderwaterArea(hsp) || SpriteInDiveArea(hsp)) {
            return (-1); // No blood underwater!
        }

        daz = Z(RANDOM_P2(128)) << 3;
        daz -= DIV2(Z(128) << 3);
        dang =  ((ang + (RANDOM_P2(128 << 5) >> 5)) - DIV2(128));

        FAFhitscan(hsp.getX(), hsp.getY(), hsp.getZ() - Z(30), hsp.getSectnum(), // Start position
                EngineUtils.sin(NORM_ANGLE(dang + 512)), // X vector of 3D ang
                EngineUtils.sin(NORM_ANGLE(dang)), // Y vector of 3D ang
                daz, // Z vector of 3D ang
                pHitInfo, CLIPMASK_MISSILE);

        if (pHitInfo.hitsect == -1) {
            return (-1);
        }

        if (Distance(pHitInfo.hitx, pHitInfo.hity, hsp.getX(), hsp.getY()) > WALLBLOOD_DIST_MAX) {
            return (-1);
        }

        // hit a sprite?
        if (pHitInfo.hitsprite != -1) {
            return (0); // Don't try to put blood on a sprite
        }

        if (pHitInfo.hitwall != -1) // Don't check if blood didn't hit a wall, otherwise the ASSERT fails!
        {
            if (TestDontStick(-1, pHitInfo.hitsect, pHitInfo.hitwall, pHitInfo.hitz)) {
                return (-1);
            }
        } else {
            return (-1);
        }

        Wall wph = boardService.getWall(pHitInfo.hitwall);
        if (wph == null) {
            return -1;
        }

        if (WallBloodQueue[WallBloodQueueHead] != -1) {
            KillSprite(WallBloodQueue[WallBloodQueueHead]);
        }

        // Randomly choose a wall blood sprite
        rndnum =  RANDOM_RANGE(1024);
        int id;
        State state;
        if (rndnum > 768) {
            id = WALLBLOOD1;
            state = s_WallBlood1[0];
        } else if (rndnum > 512) {
            id = WALLBLOOD2;
            state = s_WallBlood2[0];
        } else if (rndnum > 128) {
            id = WALLBLOOD3;
            state = s_WallBlood3[0];
        } else {
            id = WALLBLOOD4;
            state = s_WallBlood4[0];
        }

        SpriteNum =  SpawnSprite(STAT_WALLBLOOD_QUEUE, id, state, pHitInfo.hitsect, pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz, ang, 0);
        if (SpriteNum == -1) {
            return -1;
        }

        WallBloodQueue[WallBloodQueueHead] = SpriteNum;
        WallBloodQueueHead =  ((WallBloodQueueHead + 1) & (MAX_WALLBLOOD_QUEUE - 1));

        sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return -1;
        }

        sp.setOwner(-1);
        sp.setXrepeat(30);
        sp.setYrepeat(40); // yrepeat will grow towards 64, it's default size
        sp.setCstat(0);
        sp.setPal(0);
        sp.setShade(0);
        sp.setExtra(0);
        sp.setClipdist(0);
        sp.setXoffset(0);
        sp.setYoffset(0);
        sp.setX(pHitInfo.hitx);
        sp.setY(pHitInfo.hity);
        sp.setZ(pHitInfo.hitz);
        sp.setShade(sp.getShade() - 5); // Brighten it up just a bit
        sp.setYvel(pHitInfo.hitwall); // pass hitwall in yvel
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_WALL));
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_ONE_SIDE));
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YCENTER));
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));


        wall_ang = NORM_ANGLE(wph.getWallAngle() + 512);
        sp.setAng(wall_ang);

        // move it back some
        nx = EngineUtils.sin((512 + sp.getAng()) & 2047) << 4;
        ny = EngineUtils.sin(sp.getAng()) << 4;

        sectnum = sp.getSectnum();

        clipmoveboxtracenum = 1;
        engine.clipmove(sp.getX(), sp.getY(), sp.getZ(), sectnum, nx, ny, 0, 0, 0, CLIPMASK_MISSILE);

        sp.setX(clipmove_x);
        sp.setY(clipmove_y);
        sp.setZ(clipmove_z);
        sectnum = clipmove_sectnum;

        clipmoveboxtracenum = 3;

        if (sp.getSectnum() != sectnum) {
            engine.changespritesect(SpriteNum, sectnum);
        }

        return (SpriteNum);
    }

    public static void DoFloorBlood(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int dist;
        int pnum;
        PlayerStr pp;
        int xsiz, ysiz;

        if (sp.getHitag() == 9995) {
            xsiz = 12;
            ysiz = 12;
        } else {
            xsiz = 40;
            ysiz = 40;
        }

        // Make pool of blood seem to grow
        if (sp.getXrepeat() < xsiz && sp.getXrepeat() != 4) {
            sp.setXrepeat(sp.getXrepeat() + 1);
        }

        if (sp.getYrepeat() < ysiz && sp.getXrepeat() != xsiz && sp.getXrepeat() != 4) {
            sp.setYrepeat(sp.getYrepeat() + 1);
        }

        // See if any players stepped in blood
        if (sp.getXrepeat() != 4 && sp.getYrepeat() > 4) {
            for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                pp = Player[pnum];

                dist = DISTANCE(sp.getX(), sp.getY(), pp.posx, pp.posy);

                if (dist < FEET_IN_BLOOD_DIST) {
                    if (pp.NumFootPrints <= 0 || FootMode != FootType.BLOOD_FOOT) {
                        pp.NumFootPrints =  (RANDOM_RANGE(10) + 3);
                        FootMode = FootType.BLOOD_FOOT;
                    }

                    // If blood has already grown to max size, we can shrink it
                    if (sp.getXrepeat() == 40 && sp.getYrepeat() > 10) {
                        sp.setYrepeat(sp.getYrepeat() - 10);
                        if (sp.getYrepeat() <= 10) // Shrink it down and don't use it anymore
                        {
                            sp.setXrepeat(4);
                            sp.setYrepeat(4);
                        }
                    }
                }
            }
        }
    }

    public static void DoWallBlood(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        // Make blood drip down the wall
        if (sp != null && sp.getYrepeat() < 80) {
            sp.setYrepeat(sp.getYrepeat() + 1);
            sp.setZ(sp.getZ() + 128);
        }
    }

    // This is the FAST queue, it doesn't call any Animator functions or states
    public static void QueueGeneric(int SpriteNum, int pic) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        Sprite osp;
        int xrepeat, yrepeat;
        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec == null) {
            return;
        }

        if (DTEST(sec.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_WATER) {
            KillSprite(SpriteNum);
            return;
        }

        if (DTEST(sec.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_LAVA) {
            KillSprite(SpriteNum);
            return;
        }

        if (TestDontStickSector(sp.getSectnum())) {
            KillSprite(SpriteNum);
            return;
        }

        xrepeat = sp.getXrepeat();
        yrepeat = sp.getYrepeat();

        // can and should kill the user portion
        if (GenericQueue[GenericQueueHead] == -1) {
            USER u = getUser(SpriteNum);
            if (u != null) {
                setUser(SpriteNum, null);
            }
            change_sprite_stat(SpriteNum, STAT_GENERIC_QUEUE);
            GenericQueue[GenericQueueHead] =  SpriteNum;
        } else {
            // move old sprite to new sprite's place
            osp = boardService.getSprite(GenericQueue[GenericQueueHead]);
            if (osp != null) {
                // setsprite(GenericQueue[GenericQueueHead],sp.x,sp.y,sp.z);
                osp.setX(sp.getX());
                osp.setY(sp.getY());
                osp.setZ(sp.getZ());
                engine.changespritesect(GenericQueue[GenericQueueHead], sp.getSectnum());
                KillSprite(SpriteNum);
                SpriteNum = GenericQueue[GenericQueueHead];
            }
        }

        sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        sp.setPicnum( pic);
        sp.setXrepeat(xrepeat);
        sp.setYrepeat(yrepeat);
        sp.setCstat(0);
        switch (sp.getPicnum()) {
            case 900:
            case 901:
            case 902:
            case 915:
            case 916:
            case 917:
            case 930:
            case 931:
            case 932:
            case GORE_Head:
                change_sprite_stat(SpriteNum, STAT_DEFAULT); // Breakable
                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BREAKABLE));
                sp.setExtra(sp.getExtra() | (SPRX_BREAKABLE));
                break;
            default:
                sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BREAKABLE));
                sp.setExtra(sp.getExtra() & ~(SPRX_BREAKABLE));
                break;
        }

        GenericQueueHead =  ((GenericQueueHead + 1) & (MAX_GENERIC_QUEUE - 1));

    }

    public static void SpawnShell(int SpriteNum, int ShellNum) {
        InitShell(SpriteNum, ShellNum);
    }

    public static void DoShrapVelocity(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_UNDERWATER) || SpriteInUnderwaterArea(sp)) {
            ScaleSpriteVector(SpriteNum, 20000);
            u.Counter += 8 * 4; // These are MoveSkip4 now
        } else {
            u.Counter += 60 * 4;
        }
        u.zchange += u.Counter;

        u.moveSpriteReturn = move_missile(SpriteNum, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS * 2);

        MissileHitDiveArea(SpriteNum);

        if (u.moveSpriteReturn != 0) {
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_PLAX_WALL:
                    KillSprite(SpriteNum);
                    return;
                case HIT_SPRITE: {
//	                PlaySound(DIGI_DHCLUNK, sp, v3df_dontpan);

                    int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Sprite hsp = boardService.getSprite(hitsprite);
                    if (hsp == null) {
                        break;
                    }

                    int wall_ang = NORM_ANGLE(hsp.getAng());
                    WallBounce(SpriteNum, wall_ang);
                    ScaleSpriteVector(SpriteNum, 32000);

                    break;
                }

                case HIT_WALL: {
                    int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Wall wph = boardService.getWall(hitwall);
                    if (wph == null) {
                        break;
                    }

//	                PlaySound(DIGI_DHCLUNK, sp, v3df_dontpan);

                    int wall_ang = NORM_ANGLE(wph.getWallAngle() + 512);

                    WallBounce(SpriteNum, wall_ang);
                    ScaleSpriteVector(SpriteNum, 32000);
                    break;
                }

                case HIT_SECTOR: {
                    boolean hitwall;

                    if (SlopeBounce(SpriteNum, tmp_ptr[0])) {
                        hitwall = tmp_ptr[0].value != 0;
                        if (hitwall) {
                            // hit a wall
                            ScaleSpriteVector(SpriteNum, 28000);
                            u.moveSpriteReturn = 0;
                            u.Counter = 0;
                        } else {
                            // hit a sector
                            if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                                // hit a floor
                                if (!TEST(u.Flags, SPR_BOUNCE)) {
                                    u.Flags |= (SPR_BOUNCE);
                                    ScaleSpriteVector(SpriteNum, 18000);
                                    u.moveSpriteReturn = 0;
                                    u.Counter = 0;
                                } else {
                                    if (u.ID == GORE_Drip) {
                                        ChangeState(SpriteNum, s_GoreFloorSplash[0]);
                                    } else {
                                        ShrapKillSprite(SpriteNum);
                                    }
                                    return;
                                }
                            } else {
                                // hit a ceiling
                                ScaleSpriteVector(SpriteNum, 22000);
                            }
                        }
                    } else {
                        // hit floor
                        if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                            sp.setZ(u.loz);
                            if (TEST(u.Flags, SPR_UNDERWATER)) {
                                u.Flags |= (SPR_BOUNCE); // no bouncing underwater
                            }

                            Sect_User su = getSectUser(sp.getSectnum());
                            if (u.lo_sectp != -1 && su != null && su.depth != 0) {
                                u.Flags |= (SPR_BOUNCE); // no bouncing on shallow water
                            }

                            if (!TEST(u.Flags, SPR_BOUNCE)) {
                                u.Flags |= (SPR_BOUNCE);
                                u.moveSpriteReturn = 0;
                                u.Counter = 0;
                                u.zchange = -u.zchange;
                                ScaleSpriteVector(SpriteNum, 18000);
                                switch (u.ID) {
                                    case UZI_SHELL:
                                        PlaySound(DIGI_SHELL, sp, v3df_none);
                                        break;
                                    case SHOT_SHELL:
                                        PlaySound(DIGI_SHOTSHELLSPENT, sp, v3df_none);
                                        break;
                                }
                            } else {
                                if (u.ID == GORE_Drip) {
                                    ChangeState(SpriteNum, s_GoreFloorSplash[0]);
                                } else {
                                    ShrapKillSprite(SpriteNum);
                                }
                                return;
                            }
                        } else
                        // hit something above
                        {
                            u.zchange = -u.zchange;
                            ScaleSpriteVector(SpriteNum, 22000);
                        }
                    }
                    break;
                }
            }
        }

        // just outright kill it if its boucing around alot
        if (u.bounce > 10) {
            KillSprite(SpriteNum);
        }

    }

    public static void ShrapKillSprite(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int rnd_num =  RANDOM_RANGE(1024);
        switch (u.ID) {
            case UZI_SHELL:
                if (rnd_num > 854) {
                    QueueGeneric(SpriteNum, UZI_SHELL);
                } else if (rnd_num > 684) {
                    QueueGeneric(SpriteNum, UZI_SHELL + 1);
                } else if (rnd_num > 514) {
                    QueueGeneric(SpriteNum, UZI_SHELL + 2);
                } else if (rnd_num > 344) {
                    QueueGeneric(SpriteNum, UZI_SHELL + 3);
                } else if (rnd_num > 174) {
                    QueueGeneric(SpriteNum, UZI_SHELL + 4);
                } else {
                    QueueGeneric(SpriteNum, UZI_SHELL + 5);
                }

                return;
            case SHOT_SHELL:
                if (rnd_num > 683) {
                    QueueGeneric(SpriteNum, SHOT_SHELL + 1);
                } else if (rnd_num > 342) {
                    QueueGeneric(SpriteNum, SHOT_SHELL + 3);
                } else {
                    QueueGeneric(SpriteNum, SHOT_SHELL + 7);
                }
                return;
            case GORE_Lung:
                if (RANDOM_RANGE(1000) > 500) {
                    break;
                }
                sp.setClipdist(SPRITEp_SIZE_X(sp));
                SpawnFloorSplash(SpriteNum);
                if (RANDOM_RANGE(1000) < 500) {
                    PlaySound(DIGI_GIBS1, sp, v3df_none);
                } else {
                    PlaySound(DIGI_GIBS2, sp, v3df_none);
                }
                if (rnd_num > 683) {
                    QueueGeneric(SpriteNum, 900);
                } else if (rnd_num > 342) {
                    QueueGeneric(SpriteNum, 901);
                } else {
                    QueueGeneric(SpriteNum, 902);
                }
                return;
            case GORE_Liver:
                if (RANDOM_RANGE(1000) > 500) {
                    break;
                }
                sp.setClipdist(SPRITEp_SIZE_X(sp));
                SpawnFloorSplash(SpriteNum);
                if (RANDOM_RANGE(1000) < 500) {
                    PlaySound(DIGI_GIBS1, sp, v3df_none);
                } else {
                    PlaySound(DIGI_GIBS2, sp, v3df_none);
                }
                if (rnd_num > 683) {
                    QueueGeneric(SpriteNum, 915);
                } else if (rnd_num > 342) {
                    QueueGeneric(SpriteNum, 916);
                } else {
                    QueueGeneric(SpriteNum, 917);
                }
                return;
            case GORE_SkullCap:
                if (RANDOM_RANGE(1000) > 500) {
                    break;
                }
                sp.setClipdist(SPRITEp_SIZE_X(sp));
                SpawnFloorSplash(SpriteNum);
                if (rnd_num > 683) {
                    QueueGeneric(SpriteNum, 930);
                } else if (rnd_num > 342) {
                    QueueGeneric(SpriteNum, 931);
                } else {
                    QueueGeneric(SpriteNum, 932);
                }
                return;
            case GORE_Head:
                if (RANDOM_RANGE(1000) > 500) {
                    break;
                }
                sp.setClipdist(SPRITEp_SIZE_X(sp));
                QueueFloorBlood(SpriteNum);
                QueueGeneric(SpriteNum, GORE_Head);
                return;
        }

        // If it wasn't in the switch statement, kill it.
        KillSprite(SpriteNum);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean CheckBreakToughness(Break_Info break_info, int ID) {
        if (TEST(break_info.flags, BF_TOUGH)) {
            switch (ID) {
                case LAVA_BOULDER:
                case MIRV_METEOR:
                case SERP_METEOR:
                case BOLT_THINMAN_R0:
                case BOLT_THINMAN_R1:
                case BOLT_THINMAN_R2:
                case BOLT_EXP:
                case TANK_SHELL_EXP:
                case GRENADE_EXP:
                case MICRO_EXP:
                case MINE_EXP:
                case NAP_EXP:
                case SKULL_R0:
                case BETTY_R0:
                case SKULL_SERP:
                case FIREBALL1:
                case GORO_FIREBALL:
                    return (true); // All the above stuff will break tough things
            }
            return (false); // False means it won't break with current weapon
        }

        return (true); // It wasn't tough, go ahead and break it
    }

    public static boolean DoItemFly(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        if (TEST(u.Flags, SPR_UNDERWATER)) {
            ScaleSpriteVector(SpriteNum, 50000);
            u.Counter += 20 * 2;
        } else {
            u.Counter += 60 * 2;
        }
        u.zchange += u.Counter;

        u.moveSpriteReturn = move_missile(SpriteNum, u.xchange, u.ychange, u.zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, MISSILEMOVETICS * 2);

        MissileHitDiveArea(SpriteNum);

        if (u.moveSpriteReturn != 0) {
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_SPRITE: {
                    int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Sprite hsp = boardService.getSprite(hitsprite);
                    if (hsp == null) {
                        break;
                    }

                    if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                        int wall_ang = NORM_ANGLE(hsp.getAng());
                        WallBounce(SpriteNum, wall_ang);
                        ScaleSpriteVector(SpriteNum, 32000);
                    } else {
                        u.xchange = -u.xchange;
                        u.ychange = -u.ychange;
                    }

                    break;
                }

                case HIT_WALL: {
                    int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Wall wph = boardService.getWall(hitwall);
                    if (wph == null) {
                        break;
                    }

                    int wall_ang = NORM_ANGLE(wph.getWallAngle() + 512);
                    WallBounce(SpriteNum, wall_ang);
                    ScaleSpriteVector(SpriteNum, 32000);
                    break;
                }

                case HIT_SECTOR: {
                    // hit floor
                    if (sp.getZ() > DIV2(u.hiz + u.loz)) {
                        sp.setZ(u.loz);
                        u.Counter = 0;
                        sp.setXvel(0);
                        u.zchange = u.xchange = u.ychange = 0;
                        return (false);
                    } else
                    // hit something above
                    {
                        u.zchange = -u.zchange;
                        ScaleSpriteVector(SpriteNum, 22000);
                    }
                    break;
                }
            }
        }

        return (true);
    }

    //This is the FAST queue, it doesn't call any Animator functions or states
    public static void QueueLoWangs(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec != null && DTEST(sec.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_WATER) {
            return;
        }

        if (sec != null && DTEST(sec.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_LAVA) {
            return;
        }

        if (TestDontStickSector(sp.getSectnum())) {
            return;
        }

        int NewSprite;
        if (LoWangsQueue[LoWangsQueueHead] == -1) {
            NewSprite = SpawnSprite(STAT_GENERIC_QUEUE, sp.getPicnum(), s_DeadLoWang[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 0);
            if (NewSprite == -1) {
                return;
            }

            LoWangsQueue[LoWangsQueueHead] = NewSprite;
        } else {
            // move old sprite to new sprite's place
            engine.setspritez(LoWangsQueue[LoWangsQueueHead], sp.getX(), sp.getY(), sp.getZ());
            NewSprite = LoWangsQueue[LoWangsQueueHead];
        }

        // Point passed in sprite to ps
        Sprite ps = sp;
        sp = boardService.getSprite(NewSprite);
        USER u = getUser(NewSprite);
        if (sp == null || u == null) {
            return;
        }

        sp.setOwner(-1);
        sp.setCstat(0);
        sp.setXrepeat(ps.getXrepeat());
        sp.setYrepeat(ps.getYrepeat());
        sp.setShade(ps.getShade());
        u.spal = (byte) ps.getPal();
        sp.setPal(u.spal);
        change_sprite_stat(NewSprite, STAT_DEFAULT); // Breakable
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BREAKABLE));
        sp.setExtra(sp.getExtra() | (SPRX_BREAKABLE));
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK_HITSCAN));

        LoWangsQueueHead =  ((LoWangsQueueHead + 1) & (MAX_LOWANGS_QUEUE - 1));

    }

    public static void WeaponSaveable() {
        SaveData(DoCoolgDrip);
        SaveData(DoMineExp);
        SaveData(DoMineExpMine);
        SaveData(DoExpDamageTest);
        SaveData(SpawnShrapX);
        SaveData(DoLavaErupt);
        SaveData(DoVomit);
        SaveData(DoVomit);
        SaveData(DoVomitSplash);
        SaveData(DoFastShrapJumpFall);
        SaveData(DoTracerShrap);
        SaveData(DoShrapJumpFall);
        SaveData(DoShrapDamage);
        SaveData(DoUziSmoke);
        SaveData(DoShotgunSmoke);
        SaveData(DoMineSpark);
        SaveData(DoFireballFlames);
        SaveData(DoBreakFlames);
        SaveData(DoDamageTest);
        SaveData(DoStar);
        SaveData(DoCrossBolt);
        SaveData(DoPlasmaDone);
        SaveData(DoPlasmaFountain);
        SaveData(DoPlasma);
        SaveData(DoCoolgFire);
        SaveData(DoGrenade);
        SaveData(DoVulcanBoulder);
        SaveData(DoMineStuck);
        SaveData(DoMine);
        SaveData(DoPuff);
        SaveData(DoRailPuff);
        SaveData(DoBoltThinMan);
        SaveData(DoTracer);
        SaveData(DoEMP);
        SaveData(DoEMPBurst);
        SaveData(DoTankShell);
        SaveData(DoTracerStart);
        SaveData(DoLaser);
        SaveData(DoLaserStart);
        SaveData(DoRail);
        SaveData(DoRailStart);
        SaveData(DoRocket);
        SaveData(DoMicroMini);
        SaveData(DoMicro);
        SaveData(DoUziBullet);
        SaveData(DoBoltSeeker);
        SaveData(DoBoltShrapnel);
        SaveData(DoBoltFatMan);
        SaveData(DoElectro);
        SaveData(DoLavaBoulder);
        SaveData(DoSpear);
        SaveData(SpawnGrenadeSmallExp);
        SaveData(DoSectorExp);
        SaveData(DoFireball);
        SaveData(DoNapalm);
        SaveData(DoBloodWorm);
        SaveData(DoBloodWorm);
        SaveData(DoMeteor);
        SaveData(DoSerpMeteor);
        SaveData(DoMirvMissile);
        SaveData(DoMirv);
        SaveData(DoRing);
        SaveData(DoTeleRipper);
        SaveData(GenerateDrips);
        SaveData(DoSuicide);
        SaveData(DoDefaultStat);
        SaveData(DoVehicleSmoke);
        SaveData(DoWaterSmoke);
        SaveData(SpawnVehicleSmoke);
        SaveData(DoBubble);
        SaveData(DoFloorBlood);
        SaveData(DoWallBlood);
        SaveData(s_NotRestored);
        SaveData(s_Suicide);
        SaveData(s_DeadLoWang);
        SaveData(s_BreakLight);
        SaveData(s_BreakBarrel);
        SaveData(s_BreakPedistal);
        SaveData(s_BreakBottle1);
        SaveData(s_BreakBottle2);
        SaveData(s_Puff);
        SaveData(s_RailPuff);
        SaveData(s_LaserPuff);
        SaveData(s_Tracer);
        SaveData(s_EMP);
        SaveData(s_EMPBurst);
        SaveData(s_EMPShrap);
        SaveData(s_TankShell);
        SaveData(s_VehicleSmoke);
        SaveData(s_WaterSmoke);
        SaveData(s_UziSmoke);
        SaveData(s_ShotgunSmoke);
        SaveData(s_UziBullet);
        SaveData(s_UziSpark);
        SaveData(s_UziPowerSpark);
        SaveData(s_Bubble);
        SaveData(s_Splash);
        SaveData(s_CrossBolt);
        SaveGroup(WeaponStateGroup.sg_CrossBolt);
        SaveData(s_Star);
        SaveData(s_StarStuck);
        SaveData(s_StarDown);
        SaveData(s_StarDownStuck);
        SaveData(s_LavaBoulder);
        SaveData(s_LavaShard);
        SaveData(s_VulcanBoulder);
        SaveData(s_Grenade);
        SaveData(s_Grenade);
        SaveGroup(WeaponStateGroup.sg_Grenade);
        SaveData(s_MineStuck);
        SaveData(s_Mine);
        SaveData(s_MineSpark);
        SaveData(s_Meteor);
        SaveGroup(WeaponStateGroup.sg_Meteor);
        SaveData(s_MeteorExp);
        SaveData(s_MirvMeteor);
        SaveGroup(WeaponStateGroup.sg_MirvMeteor);
        SaveData(s_MirvMeteorExp);
        SaveData(s_SerpMeteor);
        SaveGroup(WeaponStateGroup.sg_SerpMeteor);
        SaveData(s_SerpMeteorExp);
        SaveData(s_Spear);
        SaveGroup(WeaponStateGroup.sg_Spear);
        SaveData(s_Rocket);
        SaveGroup(WeaponStateGroup.sg_Rocket);
        SaveData(s_BunnyRocket);
        SaveGroup(WeaponStateGroup.sg_BunnyRocket);
        SaveData(s_Rail);
        SaveGroup(WeaponStateGroup.sg_Rail);
        SaveData(s_Laser);
        SaveData(s_Micro);
        SaveGroup(WeaponStateGroup.sg_Micro);
        SaveData(s_MicroMini);
        SaveGroup(WeaponStateGroup.sg_MicroMini);
        SaveData(s_BoltThinMan);
        SaveGroup(WeaponStateGroup.sg_BoltThinMan);
        SaveData(s_BoltSeeker);
        SaveGroup(WeaponStateGroup.sg_BoltSeeker);
        SaveData(s_BoltFatMan);
        SaveData(s_BoltShrapnel);
        SaveData(s_CoolgFire);
        SaveData(s_CoolgFireDone);
        SaveData(s_CoolgDrip);
        SaveData(s_GoreFloorSplash);
        SaveData(s_GoreSplash);
        SaveData(s_Plasma);
        SaveData(s_PlasmaFountain);
        SaveData(s_PlasmaDrip);
        SaveData(s_PlasmaDone);
        SaveData(s_TeleportEffect);
        SaveData(s_TeleportEffect2);
        SaveData(s_Electro);
        SaveData(s_ElectroShrap);
        SaveData(s_GrenadeExp);
        SaveData(s_GrenadeSmallExp);
        SaveData(s_GrenadeExp);
        SaveData(s_MineExp);
        SaveData(s_BasicExp);
        SaveData(s_MicroExp);
        SaveData(s_BigGunFlame);
        SaveData(s_BoltExp);
        SaveData(s_TankShellExp);
        SaveData(s_TracerExp);
        SaveData(s_SectorExp);
        SaveData(s_FireballExp);
        SaveData(s_NapExp);
        SaveData(s_FireballFlames);
        SaveData(s_BreakFlames);
        SaveData(s_Fireball);
        SaveData(s_Fireball);
        SaveData(s_Ring);
        SaveData(s_Ring);
        SaveData(s_Ring2);
        SaveData(s_Napalm);
        SaveData(s_BloodWorm);
        SaveData(s_BloodWorm);
        SaveData(s_PlasmaExp);
        SaveData(s_PlasmaExp);
        SaveData(s_Mirv);
        SaveData(s_MirvMissile);
        SaveData(s_Vomit1);
        SaveData(s_Vomit2);
        SaveData(s_VomitSplash);
        SaveData(s_GoreHead);
        SaveData(s_GoreLeg);
        SaveData(s_GoreEye);
        SaveData(s_GoreTorso);
        SaveData(s_GoreArm);
        SaveData(s_GoreLung);
        SaveData(s_GoreLiver);
        SaveData(s_GoreSkullCap);
        SaveData(s_GoreChunkS);
        SaveData(s_GoreDrip);
        SaveData(s_FastGoreDrip);
        SaveData(s_GoreFlame);
        SaveData(s_TracerShrap);
        SaveData(s_UziShellShrap);
        SaveData(s_UziShellShrapStill1);
        SaveData(s_UziShellShrapStill2);
        SaveData(s_UziShellShrapStill3);
        SaveData(s_UziShellShrapStill4);
        SaveData(s_UziShellShrapStill5);
        SaveData(s_UziShellShrapStill6);
        SaveData(s_ShotgunShellShrap);
        SaveData(s_ShotgunShellShrapStill1);
        SaveData(s_ShotgunShellShrapStill2);
        SaveData(s_ShotgunShellShrapStill3);
        SaveData(s_GoreFlameChunkA);
        SaveData(s_GoreFlameChunkB);
        SaveData(s_CoinShrap);
        SaveData(s_Marbel);
        SaveData(s_GlassShrapA);
        SaveData(s_GlassShrapB);
        SaveData(s_GlassShrapC);
        SaveData(s_WoodShrapA);
        SaveData(s_WoodShrapB);
        SaveData(s_WoodShrapC);
        SaveData(s_StoneShrapA);
        SaveData(s_StoneShrapB);
        SaveData(s_StoneShrapC);
        SaveData(s_MetalShrapA);
        SaveData(s_MetalShrapB);
        SaveData(s_MetalShrapC);
        SaveData(s_PaperShrapA);
        SaveData(s_PaperShrapB);
        SaveData(s_PaperShrapC);
        SaveData(s_FloorBlood1);
        SaveData(s_FootPrint1);
        SaveData(s_FootPrint2);
        SaveData(s_FootPrint3);
        SaveData(s_WallBlood1);
        SaveData(s_WallBlood2);
        SaveData(s_WallBlood3);
        SaveData(s_WallBlood4);
    }
}
