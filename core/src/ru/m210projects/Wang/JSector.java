package ru.m210projects.Wang;

import ru.m210projects.Build.Render.Mirror;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Wang.Type.*;
import ru.m210projects.Wang.Type.MirrorType.MirrorState;

import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.Engine.MAXTILES;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Draw.*;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.ICON_BOOSTER;
import static ru.m210projects.Wang.Names.ICON_HEAT_CARD;
import static ru.m210projects.Wang.Names.ICON_REPAIR_KIT;
import static ru.m210projects.Wang.Names.ICON_SM_MEDKIT;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.*;
import static ru.m210projects.Wang.Player.*;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.ResourceHandler.InitSpecialTextures;
import static ru.m210projects.Wang.Weapon.GenerateDrips;

public class JSector {

     public static final int MAXMIRRORMONSTERS = 4; // Max monsters any one magic mirror can spawn

    // This header is used for reserving tile space for programatic purposes
    // MAXTILES is currently at 6144 in size - anything under this is ok

    public static final int MAXMIRRORS = 8;
    // This is just some, high, blank tile number not used
    // by real graphics to put the MAXMIRRORS mirrors in
    public static final int MIRRORLABEL = 6000;

    public static final int MIRROR = 340;
    public static final int CAMSPRITE = 3830;
    public static final int MAXCAMDIST = 8000;
    public static final MirrorType[] mirror = new MirrorType[MAXMIRRORS];
    public static int camloopcnt = 0; // Timer to cycle through player
    // views
    public static int camplayerview = 1; // Don't show yourself!
    public static boolean mirrorinview;
    public static int mirrorcnt;
    //////////////////////////////////////////////////////////////////////////////////////////////
    // Parental Lockout Stuff
    //////////////////////////////////////////////////////////////////////////////////////////////
    public static final List<OrgTile> orgwalllist = new ArrayList<>(); // The list containing
    // orginal wall
    // pics
    public static final List<OrgTile> orgwalloverlist = new ArrayList<>(); // The list containing
    // orginal wall
    // over pics
    public static final List<OrgTile> orgsectorceilinglist = new ArrayList<>(); // The list
    // containing
    // orginal sector
    // ceiling pics
    public static final List<OrgTile> orgsectorfloorlist = new ArrayList<>(); // The list containing
    static int MirrorMoveSkip16 = 0;

    /////////////////////////////////////////////////////
    // Initialize the mirrors
    /////////////////////////////////////////////////////
    static final boolean bAutoSize = true; // Autosizing on/off
    // Rotation angles for sprites
    static int rotang = 0;
//    static int on_cam = 0;

    public static void InitJSectorStructs() {
        for (int i = 0; i < MAXTILES; i++) {
            aVoxelArray[i] = new ParentalStruct();
        }
    }

    /////////////////////////////////////////////////////
    //Initialize any of my special use sprites
    /////////////////////////////////////////////////////
    public static void JS_SpriteSetup() {
        ListNode<Sprite> NextSprite;
        USER u;
        VOC3D voc;

        for (ListNode<Sprite> node = boardService.getStatNode(0); node != null; node = NextSprite) {
            final int SpriteNum = node.getIndex();
            NextSprite = node.getNext();
            Sprite sp = node.get();
            int tag = sp.getHitag();

            // Stupid Redux's fixes
            if (Level == 9 && swGetAddon() == 2 && SpriteNum == 11 && sp.getPicnum() == BETTY_R0) {
                sp.setExtra(0);
            }

            // Non-static camera. Camera sprite will be drawn!
            if (tag == MIRROR_CAM && sp.getPicnum() != ST1) {
                // Just change it to static, sprite has all the info I need
                change_sprite_stat(SpriteNum, STAT_SPAWN_SPOT);
            }

            switch (sp.getPicnum()) {
                case ST1:
                    if (tag == MIRROR_CAM) {
                        // Just change it to static, sprite has all the info I need
                        // ST1 cameras won't move with SOBJ's!
                        change_sprite_stat(SpriteNum, STAT_ST1);
                    } else if (tag == MIRROR_SPAWNSPOT) {
                        // Just change it to static, sprite has all the info I need
                        change_sprite_stat(SpriteNum, STAT_ST1);
                    } else if (tag == AMBIENT_SOUND) {
                        change_sprite_stat(SpriteNum, STAT_AMBIENT);
                    } else if (tag == TAG_ECHO_SOUND) {
                        change_sprite_stat(SpriteNum, STAT_ECHO);
                    } else if (tag == TAG_DRIPGEN) {
                        u = SpawnUser(SpriteNum, 0, null);

                        u.RotNum = 0;
                        u.WaitTics =  (sp.getLotag() * 120);

                        u.ActorActionFunc = GenerateDrips;

                        change_sprite_stat(SpriteNum, STAT_NO_STATE);
                        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
                    }
                    break;
                // Sprites in editart that should play ambient sounds
                // automatically
                case 380:
                case 396:
                case 430:
                case 443:
                case 512:
                case 521:
                case 541:
                case 2720:
                case 3143:
                case 3157:
                    if ((voc = PlaySound(DIGI_FIRE1, sp, v3df_follow | v3df_dontpan | v3df_doppler)) != null) {
                        Set3DSoundOwner(SpriteNum, voc);
                    }
                    break;
                case 795:
                case 880:
                    if ((voc = PlaySound(DIGI_WATERFLOW1, sp, v3df_follow | v3df_dontpan | v3df_doppler)) != null) {
                        Set3DSoundOwner(SpriteNum, voc);
                    }
                    break;
                case 460: // Wind Chimes
                    if ((voc = PlaySound(79, sp, v3df_ambient | v3df_init | v3df_doppler | v3df_follow)) != null) {
                        Set3DSoundOwner(SpriteNum, voc);
                    }
                    break;
            }
        }
        Wall[] walls = boardService.getBoard().getWalls();
        // Check for certain walls to make sounds
        for (Wall wall : walls) {
            int picnum = wall.getPicnum();

            // Set don't stick the bit for liquid tiles
            switch (picnum) {
                case 175:
                case 179:
                case 300:
                case 320:
                case 330:
                case 352:
                case 780:
                case 890:
                case 2608:
                case 2616:
                    wall.setExtra(wall.getExtra() | (WALLFX_DONT_STICK));
                    break;
            }

        }
    }

    public static void JS_InitMirrors() {
        // Set all the mirror struct values to -1
        for (int i = 0; i < MAXMIRRORS; i++) {
            if (mirror[i] == null) {
                mirror[i] = new MirrorType();
            } else {
                mirror[i].reset();
            }
        }

        mirrorinview = false; // Initially set global mirror flag
        // to no mirrors seen

        // Scan wall tags for mirrors
        mirrorcnt = 0;

        InitSpecialTextures();
        for (int i = 0; i < MAXMIRRORS; i++) {
            mirror[i].campic = -1;
            mirror[i].camsprite = -1;
            mirror[i].camera = -1;
            mirror[i].ismagic = false;
        }

        Wall[] walls = boardService.getBoard().getWalls();
        // Check for certain walls to make sounds
        for (int i = 0; i < walls.length; i++) {
            Wall wal = walls[i];
            int s = wal.getNextsector();
            Sector nsec = boardService.getSector(s);
            if (nsec != null && (wal.getOverpicnum() == MIRROR) && (wal.getCstat() & 32) != 0) {
                if ((nsec.getFloorstat() & 1) == 0) {
                    wal.setOverpicnum( (MIRRORLABEL + mirrorcnt));
                    wal.setPicnum( (MIRRORLABEL + mirrorcnt));

                    nsec.setCeilingpicnum( (MIRRORLABEL + mirrorcnt));
                    nsec.setFloorpicnum( (MIRRORLABEL + mirrorcnt));
                    nsec.setFloorstat(nsec.getFloorstat() | 1);
                    mirror[mirrorcnt].mirrorwall =  i;
                    mirror[mirrorcnt].mirrorsector =  s;
                    mirror[mirrorcnt].numspawnspots = 0;
                    mirror[mirrorcnt].ismagic = false;
                    if (wal.getLotag() == TAG_WALL_MAGIC_MIRROR) {
                        Sprite sp;

                        mirror[mirrorcnt].ismagic = true;
                        boolean Found_Cam = false;

                        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ST1); node != null; node = node.getNext()) {
                            sp = node.get();
                            // if correct type and matches
                            if (sp.getHitag() == MIRROR_CAM && sp.getLotag() == wal.getHitag()) {
                                mirror[mirrorcnt].camera =  node.getIndex();
                                // Set up camera varialbes
                                sp.setXvel(sp.getAng()); // Set current angle to
                                // sprite angle
                                Found_Cam = true;
                            }
                        }

                        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPAWN_SPOT); node != null; node = node.getNext()) {
                            sp = node.get();

                            // if correct type and matches
                            if (sp.getHitag() == MIRROR_CAM && sp.getLotag() == wal.getHitag()) {
                                mirror[mirrorcnt].camera =  node.getIndex();
                                // Set up camera varialbes
                                sp.setXvel(sp.getAng()); // Set current angle to
                                // sprite angle
                                Found_Cam = true;
                            }
                        }

                        if (!Found_Cam) {
                            throw new WarningException("Cound not find the camera view sprite for match " + wal.getHitag() + "\r\nMap Coordinates: x = " + wal.getX() + ", y = " + wal.getY());
                        }

                        Found_Cam = false;
                        Sprite camSpr = boardService.getSprite(mirror[mirrorcnt].camera);
                        if (camSpr != null && TEST_BOOL1(camSpr)) {
                            for (ListNode<Sprite> node = boardService.getStatNode(0); node != null; node = node.getNext()) {
                                sp = node.get();
                                if (sp.getPicnum() >= CAMSPRITE && sp.getPicnum() < CAMSPRITE + 8 && sp.getHitag() == wal.getHitag()) {
                                    mirror[mirrorcnt].campic = sp.getPicnum();
                                    mirror[mirrorcnt].camsprite =  node.getIndex();
                                    Found_Cam = true;
                                }
                            }

                            if (!Found_Cam) {
                                throw new WarningException("Did not find drawtotile for camera number " + mirrorcnt + "\r\nwall[" + i + "].hitag == " + wal.getHitag() + "\r\nMap Coordinates: x = " + wal.getX() + ", y = " + wal.getY());
                            }
                        }

                        // For magic mirrors, set allowable viewing time to 30
                        // secs
                        // Base rate is supposed to be 120, but time is double
                        // what I expect
                        mirror[mirrorcnt].maxtics = 60 * 30;

                    }

                    mirror[mirrorcnt].mstate = MirrorState.m_normal;

                    // Set tics used to none
                    mirror[mirrorcnt].tics = 0;
                    mirrorcnt++;
                } else {
                    wal.setOverpicnum(nsec.getCeilingpicnum());
                }
            }
        }

        // Invalidate textures in sector behind mirror
        for (int i = 0; i < mirrorcnt; i++) {
            Sector sec = boardService.getSector(mirror[i].mirrorsector);
            if (sec != null) {
                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    wal.setPicnum(MIRROR);
                    wal.setOverpicnum(MIRROR);
                }
            }
        }
    } // InitMirrors

    /////////////////////////////////////////////////////
    // Draw a 3d screen to a specific tile
    /////////////////////////////////////////////////////
    public static void drawroomstotile(int daposx, int daposy, int daposz, int daang, int dahoriz, int dacursectnum, int tilenume, int smoothratio) {
        Renderer renderer = game.getRenderer();
        DynamicArtEntry pic = engine.getTileManager().getDynamicTile(tilenume);
        renderer.setviewtotile(pic);

        renderer.drawrooms(daposx, daposy, daposz, daang, dahoriz,  dacursectnum);
        analyzesprites(daposx, daposy, daposz, false, smoothratio);
        renderer.drawmasks();

        renderer.setviewback();

        if (renderer.getType() == Renderer.RenderType.Software) {
            engine.getTileManager().squarerotatetile(pic);
        }
        pic.invalidate();
    }

    public static void JS_ProcessEchoSpot() {

        Sprite tp;
        long j, dist;
        PlayerStr pp = Player[screenpeek];
        int reverb;
        boolean reverb_set = false;

        // Process echo sprites
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ECHO); node != null; node = node.getNext()) {
            dist = 0x7fffffff;

            tp = node.get();

            j = klabs(tp.getX() - pp.posx);
            j += klabs(tp.getY() - pp.posy);
            if (j < dist) {
                dist = j;
            }

            if (dist <= SP_TAG4(tp)) // tag4 = ang
            {
                reverb =  SP_TAG2(tp);
                if (reverb > 200) {
                    reverb = 200;
                }
                if (reverb < 100) {
                    reverb = 100;
                }

                COVER_SetReverb(reverb);
                reverb_set = true;
            }
        }
        if (!TEST(pp.Flags, PF_DIVING) && !reverb_set && pp.Reverb <= 0) {
            COVER_SetReverb(0);
        }
    }

    public static void JS_DrawMirrors(PlayerStr pp, int tx, int ty, int tz, float tpang, float tphoriz, int smoothratio) {
        int j, dx, dy, cnt;
        int dist;
        boolean bIsWallMirror;

        MirrorMoveSkip16 = (MirrorMoveSkip16 + 1) & 15;

        camloopcnt += (engine.getTotalClock() - game.pNet.ototalclock);
        if (camloopcnt > (60 * 5)) // 5 seconds per player view
        {
            camloopcnt = 0;
            camplayerview++;
            if (camplayerview >= numplayers) {
                camplayerview = 1;
            }
        }

        // WARNING! Assuming (MIRRORLABEL&31) = 0 and MAXMIRRORS = 64
        Renderer renderer = game.getRenderer();
        byte[] gotpic = renderer.getRenderedPics();
        if ((gotpic[MIRRORLABEL >> 3] | gotpic[(MIRRORLABEL >> 3) + 1]) != 0) {
            for (cnt = MAXMIRRORS - 1; cnt >= 0; cnt--) {
                if (TEST_GOTPIC(cnt + MIRRORLABEL) || TEST_GOTPIC(mirror[cnt].campic)) {
                    bIsWallMirror = false;
                    if (TEST_GOTPIC(cnt + MIRRORLABEL)) {
                        bIsWallMirror = true;
                        RESET_GOTPIC(cnt + MIRRORLABEL);
                    } else if (TEST_GOTPIC(mirror[cnt].campic)) {
                        RESET_GOTPIC(mirror[cnt].campic);
                    }

                    mirrorinview = true;

                    dist = 0x7fffffff;

                    if (bIsWallMirror && mirror[cnt].mirrorwall != -1) {
                        Wall wal = boardService.getWall(mirror[cnt].mirrorwall);
                        if (wal != null) {
                            j = klabs(wal.getX() - tx);
                            j += klabs(wal.getY() - ty);
                            if (j < dist) {
                                dist = j;
                            }
                        }
                    } else if (mirror[cnt].camsprite != -1) {
                        Sprite tp = boardService.getSprite(mirror[cnt].camsprite);
                        if (tp != null) {
                            j = klabs(tp.getX() - tx);
                            j += klabs(tp.getY() - ty);
                            if (j < dist) {
                                dist = j;
                            }
                        }
                    }

                    if (mirror[cnt].ismagic) {
                        Sprite sp = boardService.getSprite(mirror[cnt].camera);

                        // Calculate the angle of the mirror wall
                        final int w = mirror[cnt].mirrorwall;
                        Wall wall = boardService.getWall(w);
                        if (wall == null || sp == null) {
                            continue;
                        }

                        // Get wall midpoint for offset in mirror view
                        int midx = (wall.getX() + wall.getWall2().getX()) / 2;
                        int midy = (wall.getY() + wall.getWall2().getY()) / 2;

                        // Finish finding offsets
                        int tdx = klabs(midx - tx);
                        int tdy = klabs(midy - ty);

                        if (midx >= tx) {
                            dx = sp.getX() - tdx;
                        } else {
                            dx = sp.getX() + tdx;
                        }

                        if (midy >= ty) {
                            dy = sp.getY() - tdy;
                        } else {
                            dy = sp.getY() + tdy;
                        }

                        int dz;
                        int tdz = klabs(tz - sp.getZ());
                        if (tz >= sp.getZ()) {
                            dz = sp.getZ() + tdz;
                        } else {
                            dz = sp.getZ() - tdz;
                        }

                        // Is it a TV cam or a teleporter that shows destination?
                        // true = It's a TV cam
                        mirror[cnt].mstate = MirrorState.m_normal;
                        if (TEST_BOOL1(sp)) {
                            mirror[cnt].mstate = MirrorState.m_viewon;
                        }

                        // Show teleport destination
                        // NOTE: Adding MAXSECTORS lets you draw a room, even if
                        // you are outside of it!
                        if (mirror[cnt].mstate != MirrorState.m_viewon) {
                            renderer.drawrooms(dx, dy, dz, tpang, tphoriz,  (sp.getSectnum() + boardService.getSectorCount()));
                            analyzesprites(dx, dy, dz, false, smoothratio);
                            renderer.drawmasks();
                        } else {
                            boolean DoCam = false;

                            if (mirror[cnt].campic == -1) {
                                throw new AssertException("Missing campic for mirror " + cnt + "\r\nMap Coordinates: x = " + midx + ", y = " + midy);
                            }

                            // BOOL2 = Oscilate camera
                            if (TEST_BOOL2(sp) && !MoveSkip2) {
                                if (TEST_BOOL3(sp)) // If true add increment to
                                // angle else subtract
                                {
                                    // Store current angle in TAG5
                                    sp.setXvel(NORM_ANGLE(sp.getXvel() + 4));

                                    // TAG6 = Turn radius
                                    if (klabs(GetDeltaAngle(SP_TAG5(sp), sp.getAng())) >= SP_TAG6(sp)) {
                                        RESET_BOOL3(sp); // Reverse turn
                                        // direction.
                                    }
                                } else {
                                    // Store current angle in TAG5
                                    sp.setXvel(NORM_ANGLE(sp.getXvel() - 4));

                                    // TAG6 = Turn radius
                                    if (klabs(GetDeltaAngle(SP_TAG5(sp), sp.getAng())) >= SP_TAG6(sp)) {
                                        SET_BOOL3(sp); // Reverse turn
                                        // direction.
                                    }
                                }
                            } else if (!TEST_BOOL2(sp)) {
                                sp.setXvel(sp.getAng()); // Copy sprite angle to tag5
                            }

                            // See if there is a horizon value. 0 defaults to 100!
                            int camhoriz = 100; // Default
                            if (SP_TAG7(sp) != 0) {
                                camhoriz = SP_TAG7(sp);
                                if (camhoriz > PLAYER_HORIZ_MAX) {
                                    camhoriz = PLAYER_HORIZ_MAX;
                                } else if (camhoriz < PLAYER_HORIZ_MIN) {
                                    camhoriz = PLAYER_HORIZ_MIN;
                                }
                            }

                            // If player is dead still then update at MoveSkip4
                            // rate.
                            if (pp.posx == pp.oposx && pp.posy == pp.oposy && pp.posz == pp.oposz) {
                                DoCam = true;
                            }

                            // Set up the tile for drawing
                            if (MirrorMoveSkip16 == 0 || (DoCam && (MoveSkip4 == 0))) {
                                if (dist < MAXCAMDIST) {
                                    PlayerStr cp = Player[camplayerview];

                                    if (TEST_BOOL11(sp) && (numplayers > 1 || gNet.FakeMultiplayer)) {
                                        drawroomstotile(cp.posx, cp.posy, cp.posz, cp.getAnglei(), cp.getHorizi(), cp.cursectnum, mirror[cnt].campic, smoothratio);
                                    } else {
                                        drawroomstotile(sp.getX(), sp.getY(), sp.getZ(), SP_TAG5(sp), camhoriz, sp.getSectnum(), mirror[cnt].campic, smoothratio);
                                    }
                                }
                            }
                        }
                    } else if (mirror[cnt].mirrorwall != -1) { // It's just a mirror
                        // Prepare drawrooms for drawing mirror and calculate
                        // reflected
                        // position into tposx, tposy, and tang (tposz == cposz)
                        // Must call preparemirror before drawrooms and
                        // completemirror after drawrooms

                        Mirror mirr = renderer.preparemirror(tx, ty, tz, tpang, tphoriz, mirror[cnt].mirrorwall, mirror[cnt].mirrorsector);

                        renderer.drawrooms(mirr.getX(), mirr.getY(), tz, mirr.getAngle(), tphoriz, (mirror[cnt].mirrorsector + boardService.getSectorCount()));

                        analyzesprites((int) mirr.getX(), (int) mirr.getY(), tz, true, smoothratio);
                        renderer.drawmasks();

                        renderer.completemirror(); // Reverse screen x-wise in this
                        // function
                    }

                    // Clean up anything that the camera view might have done
                    Wall mirrorWall = boardService.getWall(mirror[cnt].mirrorwall);
                    if (mirrorWall != null) {
                        mirrorWall.setOverpicnum( (MIRRORLABEL + cnt));
                    }
                } else {
                    mirrorinview = false;
                }
            }
        }
    }

    public static void DoAutoSize(Sprite tspr) {
        if (!bAutoSize) {
            return;
        }

        switch (tspr.getPicnum()) {
            case ICON_STAR: // 1793
                break;
            case ICON_UZI: // 1797
            case ICON_UZIFLOOR: // 1807
                tspr.setXrepeat(43);
                tspr.setYrepeat(40);
                break;
            case ICON_LG_UZI_AMMO: // 1799
                break;
            case ICON_HEART: // 1824
                break;
            case ICON_HEART_LG_AMMO: // 1820
                break;
            case ICON_GUARD_HEAD: // 1814
                break;
            case ICON_FIREBALL_LG_AMMO: // 3035
                break;
            case ICON_ROCKET: // 1843
                break;
            case ICON_SHOTGUN: // 1794
                tspr.setXrepeat(57);
                tspr.setYrepeat(58);
                break;
            case ICON_LG_ROCKET: // 1796
                break;
            case ICON_LG_SHOTSHELL: // 1823
                break;
            case ICON_MICRO_GUN: // 1818
                break;
            case ICON_MICRO_BATTERY: // 1800
                break;
            case ICON_GRENADE_LAUNCHER: // 1817
                tspr.setXrepeat(54);
                tspr.setYrepeat(52);
                break;
            case ICON_LG_GRENADE: // 1831
                break;
            case ICON_LG_MINE: // 1842
                break;
            case ICON_RAIL_GUN: // 1811
                tspr.setXrepeat(50);
                tspr.setYrepeat(54);
                break;
            case ICON_RAIL_AMMO: // 1812
                break;
            case ICON_SM_MEDKIT: // 1802
                break;
            case ICON_MEDKIT: // 1803
                break;
            case ICON_CHEMBOMB:
                tspr.setXrepeat(64);
                tspr.setYrepeat(47);
                break;
            case ICON_FLASHBOMB:
                tspr.setXrepeat(32);
                tspr.setYrepeat(34);
                break;
            case ICON_CALTROPS:
                tspr.setXrepeat(37);
                tspr.setYrepeat(30);
                break;
            case ICON_BOOSTER: // 1810
                tspr.setXrepeat(30);
                tspr.setYrepeat(38);
                break;
            case ICON_HEAT_CARD: // 1819
                tspr.setXrepeat(46);
                tspr.setYrepeat(47);
                break;
            case ICON_REPAIR_KIT: // 1813
                break;
            case ICON_EXPLOSIVE_BOX: // 1801
                break;
            case ICON_ENVIRON_SUIT: // 1837
                break;
            case ICON_FLY: // 1782
                break;
            case ICON_CLOAK: // 1826
                break;
            case ICON_NIGHT_VISION: // 3031
                tspr.setXrepeat(59);
                tspr.setYrepeat(71);
                break;
            case ICON_NAPALM: // 3046
                break;
            case ICON_RING: // 3050
                break;
            case ICON_RINGAMMO: // 3054
                break;
            case ICON_NAPALMAMMO: // 3058
                break;
            case ICON_GRENADE: // 3059
                break;
            case ICON_ARMOR: // 3030
                tspr.setXrepeat(82);
                tspr.setYrepeat(84);
                break;
            case BLUE_KEY: // 1766
                break;
            case RED_KEY: // 1770
                break;
            case GREEN_KEY: // 1774
                break;
            case YELLOW_KEY: // 1778
                break;
            case BLUE_CARD:
            case RED_CARD:
            case GREEN_CARD:
            case YELLOW_CARD:
                tspr.setXrepeat(36);
                tspr.setYrepeat(33);
                break;
            case GOLD_SKELKEY:
            case SILVER_SKELKEY:
            case BRONZE_SKELKEY:
            case RED_SKELKEY:
                tspr.setXrepeat(39);
                tspr.setYrepeat(45);
                break;
            case SKEL_LOCKED:
            case SKEL_UNLOCKED:
                tspr.setXrepeat(47);
                tspr.setYrepeat(40);
                break;
            default:
                break;
        }
    }

    public static void JAnalyzeSprites(Sprite tspr) {
        rotang += 4;
        if (rotang > 2047) {
            rotang = 0;
        }

        // Take care of autosizing
        DoAutoSize(tspr);

        if (tspr.getPicnum() == 764) { // Gun barrel
            tspr.setCstat(tspr.getCstat() | 16);
        }
    }
    // orginal sector
    // floor pics

    public static OrgTile InitOrgTile(List<OrgTile> thelist) {
        OrgTile tp = new OrgTile();
        thelist.add(tp);

        return tp;
    }

    public static OrgTile FindOrgTile(int index, List<OrgTile> thelist) {
        if (thelist.isEmpty()) {
            return null;
        }

        for (OrgTile t : thelist) {
            if (t.index == index) {
                return (t);
            }
        }

        return null;
    }

    public static void JS_UnInitLockouts() {
        orgwalllist.clear();
        orgwalloverlist.clear();
        orgsectorceilinglist.clear();
        orgsectorfloorlist.clear();
    }

    /////////////////////////////////////////////////////
    // Initialize the original tiles list
    // Creates a list of all orginal tiles and their
    // replacements. Several tiles can use the same
    // replacement tilenum, so the list is built
    // using the original tilenums as a basis for
    // memory allocation
    // t == 1 - wall
    // t == 2 - overpicnum
    // t == 3 - ceiling
    // t == 4 - floor
    /////////////////////////////////////////////////////
    public static void JS_PlockError(int wall_num, int t) {
        throw new WarningException("ERROR: JS_InitLockouts(), out of range tile number " + wall_num + " " + t);
    }

    public static void JS_InitLockouts() {
        OrgTile tp;

        orgwalllist.clear(); // The list containing orginal wall
        // pics
        orgwalloverlist.clear(); // The list containing orginal wall
        // over pics
        orgsectorceilinglist.clear(); // The list containing orginal sector
        // ceiling pics
        orgsectorfloorlist.clear(); // The list containing orginal sector
        // floor pics

        // Check all walls
        Wall[] walls = boardService.getBoard().getWalls();
        // Check for certain walls to make sounds
        for (int i = 0; i < walls.length; i++) {
            Wall wal = walls[i];

            int picnum = wal.getPicnum();

            if (aVoxelArray[picnum].Parental >= INVISTILE) {
                JS_PlockError(i, 1);
            }

            if (aVoxelArray[picnum].Parental >= 0) {
                if ((tp = FindOrgTile(i, orgwalllist)) == null) {
                    tp = InitOrgTile(orgwalllist);
                }
                tp.index = i;
                tp.orgpicnum = wal.getPicnum();
            }

            picnum = wal.getOverpicnum();
            if (aVoxelArray[picnum].Parental >= INVISTILE) {
                JS_PlockError(i, 2);
            }

            if (aVoxelArray[picnum].Parental >= 0) {
                if ((tp = FindOrgTile(i, orgwalloverlist)) == null) {
                    tp = InitOrgTile(orgwalloverlist);
                }
                tp.index =  i;
                tp.orgpicnum = wal.getOverpicnum();
            }
        }

        // Check all ceilings and floors
        Sector[] sectors = boardService.getBoard().getSectors();
        for (int i = 0; i < sectors.length; i++) {
            int picnum = sectors[i].getCeilingpicnum();
            if (aVoxelArray[picnum].Parental >= INVISTILE) {
                JS_PlockError(i, 3);
            }

            if (aVoxelArray[picnum].Parental >= 0) {
                if ((tp = FindOrgTile(i, orgsectorceilinglist)) == null) {
                    tp = InitOrgTile(orgsectorceilinglist);
                }
                tp.index =  i;
                tp.orgpicnum = sectors[i].getCeilingpicnum();
            }

            picnum = sectors[i].getFloorpicnum();
            if (aVoxelArray[picnum].Parental >= INVISTILE) {
                JS_PlockError(i, 2);
            }

            if (aVoxelArray[picnum].Parental >= 0) {
                if ((tp = FindOrgTile(i, orgsectorfloorlist)) == null) {
                    tp = InitOrgTile(orgsectorfloorlist);
                }
                tp.index =  i;
                tp.orgpicnum = sectors[i].getFloorpicnum();
            }
        }
    }

    /////////////////////////////////////////////////////
    // Switch back and forth between locked out stuff
    /////////////////////////////////////////////////////
    public static void JS_ToggleLockouts() {
        OrgTile tp;

        // Check all walls
        Wall[] walls = boardService.getBoard().getWalls();
        // Check for certain walls to make sounds
        for (int i = 0; i < walls.length; i++) {
            int picnum;
            Wall wal = walls[i];

            if (cfg.ParentalLock) {
                picnum = wal.getPicnum();
                // be invisible
                if (aVoxelArray[picnum].Parental >= 0) {
                    wal.setPicnum(aVoxelArray[picnum].Parental);
                }
            } else if ((tp = FindOrgTile(i, orgwalllist)) != null) {
                wal.setPicnum(tp.orgpicnum); // Restore them
            }

            if (cfg.ParentalLock) {
                picnum = wal.getOverpicnum();

                if (aVoxelArray[picnum].Parental >= 0) {
                    wal.setOverpicnum(aVoxelArray[picnum].Parental);
                }
            } else if ((tp = FindOrgTile(i, orgwalloverlist)) != null) {
                wal.setOverpicnum(tp.orgpicnum); // Restore them
            }
        }

        // Check all sectors
        Sector[] sectors = boardService.getBoard().getSectors();
        for (int i = 0; i < sectors.length; i++) {
            Sector sec = sectors[i];
            int picnum;

            if (cfg.ParentalLock) {
                picnum = sec.getCeilingpicnum();

                if (aVoxelArray[picnum].Parental >= 0) {
                    sec.setCeilingpicnum(aVoxelArray[picnum].Parental);
                }
            } else if ((tp = FindOrgTile(i, orgsectorceilinglist)) != null) {
                sec.setCeilingpicnum(tp.orgpicnum); // Restore them
            }

            if (cfg.ParentalLock) {
                picnum = sec.getFloorpicnum();

                if (aVoxelArray[picnum].Parental >= 0) {
                    sec.setFloorpicnum(aVoxelArray[picnum].Parental);
                }
            } else if ((tp = FindOrgTile(i, orgsectorfloorlist)) != null) {
                sec.setFloorpicnum(tp.orgpicnum); // Restore them
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static void UnlockKeyLock(int key_num, int hitsprite) {
        int color = 0;

        // Get palette by looking at key number
        switch (key_num - 1) {
            case 0: // RED_KEY
            case 7: // RED_SKELKEY
                color = PALETTE_PLAYER9;
                break;
            case 1: // BLUE_KEY
                color = PALETTE_PLAYER7;
                break;
            case 2: // GREEN_KEY
                color = PALETTE_PLAYER6;
                break;
            case 3: // YELLOW_KEY
            case 4: // SILVER_SKELKEY
                color = PALETTE_PLAYER4;
                break;
            case 5: // GOLD_SKELKEY
                color = PALETTE_PLAYER1;
                break;
            case 6: // BRONZE_SKELKEY
                color = PALETTE_PLAYER8;
                break;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(0); node != null; node = node.getNext()) {
            Sprite sp = node.get();

            switch (sp.getPicnum()) {
                case SKEL_LOCKED:
                    if (sp.getPal() == color) {
                        PlaySound(DIGI_UNLOCK, sp, v3df_doppler | v3df_dontpan);
                        if (node.getIndex() == hitsprite) {
                            sp.setPicnum(SKEL_UNLOCKED);
                        }
                    }
                    break;
                case RAMCARD_LOCKED:
                    if (sp.getPal() == color) {
                        PlaySound(DIGI_CARDUNLOCK, sp, v3df_doppler | v3df_dontpan);
                        sp.setPicnum(RAMCARD_UNLOCKED);
                    }
                    break;
                case CARD_LOCKED:
                    if (sp.getPal() == color) {
                        PlaySound(DIGI_RAMUNLOCK, sp, v3df_doppler | v3df_dontpan);
                        if (node.getIndex() == hitsprite) {
                            sp.setPicnum(CARD_UNLOCKED);
                        } else {
                            sp.setPicnum(CARD_UNLOCKED + 1);
                        }
                    }
                    break;
            }
        }
    }

}
