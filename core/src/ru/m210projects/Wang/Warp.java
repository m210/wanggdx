package ru.m210projects.Wang;

import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;

import java.util.Arrays;

import static ru.m210projects.Wang.Factory.WangNetwork.Prediction;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Names.STAT_WARP;
import static ru.m210projects.Wang.Stag.*;
import static ru.m210projects.Wang.Type.MyTypes.TEST;

public class Warp {

    private static final int[] match_rand = new int[16];
    public static final BuildPos warp = new BuildPos();
    private static Sprite WarpPlaneSectorInfo_fl, WarpPlaneSectorInfo_ceil;

    private static boolean WarpPlaneSectorInfo(int sectnum) {
        WarpPlaneSectorInfo_fl = null;
        WarpPlaneSectorInfo_ceil = null;

        if (Prediction) {
            return false;
        }

        Sector sec = boardService.getSector(sectnum);
        if (sec != null && !TEST(sec.getExtra(), SECTFX_WARP_SECTOR)) {
            return false;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_WARP); node != null; node = node.getNext()) {
            Sprite sp = node.get();

            if (sp.getSectnum() == sectnum) {
                // skip - don't teleport
                if (SP_TAG10(sp) == 1) {
                    continue;
                }

                if (sp.getHitag() == WARP_CEILING_PLANE) {
                    WarpPlaneSectorInfo_ceil = sp;
                } else if (sp.getHitag() == WARP_FLOOR_PLANE) {
                    WarpPlaneSectorInfo_fl = sp;
                }
            }
        }

        return true;
    }

    public static Sprite WarpPlane(int x, int y, int z, int sectnum) {
        if (Prediction) {
            return null;
        }

        if (!WarpPlaneSectorInfo(sectnum)) {
            return null;
        }

        if (WarpPlaneSectorInfo_ceil != null) {
            if (z <= WarpPlaneSectorInfo_ceil.getZ()) {
                return (WarpToArea(WarpPlaneSectorInfo_ceil, x, y, z));
            }
        }

        if (WarpPlaneSectorInfo_fl != null) {
            if (z >= WarpPlaneSectorInfo_fl.getZ()) {
                return (WarpToArea(WarpPlaneSectorInfo_fl, x, y, z));
            }
        }

        return null;
    }

    private static Sprite WarpToArea(Sprite sp_from, int x, int y, int z) {
        Sprite sp = sp_from;

        int to_tag = 0;
        int z_adj = 0;
        int xoff = x - sp.getX();
        int yoff = y - sp.getY();
        int zoff = z - sp.getZ();
        int match = sp.getLotag();

        Arrays.fill(match_rand,  0);

        switch (sp.getHitag()) {
            case WARP_TELEPORTER:
                to_tag = WARP_TELEPORTER;

                // if tag 5 has something this is a random teleporter
                if (SP_TAG5(sp) != 0) {
                    int ndx = 0;
                    match_rand[ndx++] =  SP_TAG2(sp);
                    match_rand[ndx++] =  SP_TAG5(sp);
                    if (SP_TAG6(sp) != 0) {
                        match_rand[ndx++] =  SP_TAG6(sp);
                    }
                    if (SP_TAG7(sp) != 0) {
                        match_rand[ndx++] =  SP_TAG7(sp);
                    }
                    if (SP_TAG8(sp) != 0) {
                        match_rand[ndx++] =  SP_TAG8(sp);
                    }

                    // reset the match you are looking for
                    match = match_rand[RANDOM_RANGE(ndx)];
                }
                break;
            case WARP_CEILING_PLANE:
                to_tag = WARP_FLOOR_PLANE;
                // make sure you warp outside of warp plane
                z_adj = -Z(2);
                break;
            case WARP_FLOOR_PLANE:
                to_tag = WARP_CEILING_PLANE;
                // make sure you warp outside of warp plane
                z_adj = Z(2);
                break;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_WARP); node != null; node = node.getNext()) {
            sp = node.get();

            if (sp.getLotag() == match && sp != sp_from) {
                // exp: WARP_CEILING or WARP_CEILING_PLANE
                if (sp.getHitag() == to_tag) {
                    // determine new x,y,z position
                    x = warp.x = sp.getX() + xoff;
                    y = warp.y = sp.getY() + yoff;
                    z = sp.getZ() + zoff;

                    // make sure you warp outside of warp plane
                    z += z_adj;

                    warp.z = z;

                    // get new sector
                    int sectnum = warp.sectnum = engine.updatesector(x, y, sp.getSectnum());
                    if (sectnum >= 0) {
                        return (sp);
                    }
                }
            }
        }

        return null;
    }

    ////////////////////////////////////////////////////////////////////////////////
    //
    // Warp - Teleporter style
    //
    ////////////////////////////////////////////////////////////////////////////////

    public static Sprite WarpSectorInfo(int sectnum) {
        Sector sec = boardService.getSector(sectnum);
        Sprite sp_warp = null;

        if (sec != null && !TEST(sec.getExtra(), SECTFX_WARP_SECTOR)) {
            return null;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_WARP); node != null; node = node.getNext()) {
            Sprite sp = node.get();

            if (sp.getSectnum() == sectnum) {
                // skip - don't teleport
                if (SP_TAG10(sp) == 1) {
                    continue;
                }

                if (sp.getHitag() == WARP_TELEPORTER) {
                    sp_warp = sp;
                }
            }
        }

        return sp_warp;
    }

    public static Sprite WarpM(int x, int y, int z, int sectnum) {
        if (Prediction) {
            return null;
        }

        Sprite sp_warp = WarpSectorInfo(sectnum);
        if (sp_warp != null) {
            return (WarpToArea(sp_warp, x, y, z));
        }

        return null;
    }

}
