package ru.m210projects.Wang.Screens;

import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.MenuItems.MenuItem;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.Renderer;

import static ru.m210projects.Wang.Main.cfg;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Screens.MenuScreen.DrawBackground;

public class WangMessageScreen extends MessageScreen {

    public WangMessageScreen(BuildGame game, String header, String message, MessageType type) {
        super(game, header, message, game.getFont(2), game.getFont(1), type);

        for (MenuItem item : messageItems) {
            item.pal = 4;
        }

        for (MenuItem item : variantItems) {
            item.pal = 4;
        }
    }

    @Override
    public void show() {
        super.show();
        byte[] palette = engine.getPaletteManager().getBasePalette();
        engine.setbrightness(cfg.getPaletteGamma(), palette);
    }

    @Override
    public void drawBackground(Renderer renderer) {
        renderer.clearview(117);
        DrawBackground(engine);
    }
}
