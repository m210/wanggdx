package ru.m210projects.Wang.Screens;


import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Wang.Game;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Type.CompareService.CompareService;
import ru.m210projects.Wang.Type.DemoFile;
import ru.m210projects.Wang.Type.PlayerStr;

import java.io.InputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Border.SetBorder;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.synctics;
import static ru.m210projects.Wang.LoadSave.lastload;
import static ru.m210projects.Wang.Main.*;

public class DemoScreen extends GameScreen {

    protected Entry lastDemoFile = DUMMY_ENTRY;
    public int nDemonum = -1;
    public final Map<Group, List<Entry>> demofiles = new HashMap<>();
    public DemoFile demfile;

    public DemoScreen(Main game) {
        super(game);
    }

    @Override
    public void show() {
        lastload = null;
    }

    public boolean showDemo(Entry entry, Entry ignoredIni) {
        onStopRecord();

        demfile = null;
        try (InputStream is = entry.getInputStream()) {
            demfile = new DemoFile(is);
        } catch (Exception e) {
            Console.out.println("Can't play the demo file: " + entry.getName(), OsdColor.RED);
            return false;
        }

        gNet.FakeMultiplayer = demfile.numplayers > 1;
        if (gNet.FakeMultiplayer) {
            gNet.FakeMultiNumPlayers = demfile.numplayers;
        }

        if (numplayers > 1) {
            game.pNet.NetDisconnect(myconnectindex);
        }

        if (demfile.nVersion != 1) {
            gNet.KillLimit = demfile.KillLimit;
            gNet.TimeLimit = demfile.TimeLimit;
            gNet.TimeLimitClock = demfile.TimeLimitClock;
            gNet.MultiGameType = demfile.MultiGameType;

            gNet.TeamPlay = demfile.TeamPlay;
            gNet.HurtTeammate = demfile.HurtTeammate;
            gNet.SpawnMarkers = demfile.SpawnMarkers;
            gNet.NoRespawn = demfile.NoRespawn;
            gNet.Nuke = demfile.Nuke;
        }

        GodMode = false;

        Object item = null;
        UserFlag userFlag = demfile.userFlag;
        if (userFlag == UserFlag.Addon) {
            item = demfile.addon;
        } else if (userFlag == UserFlag.UserMap) {
            item = game.getCache().getEntry(FileUtils.getPath(demfile.mapname), true);
        }

        // CompareService.prepare(Paths.get("D:\\DemosData\\SW\\" + entry.getName() + ".jdm"), CompareService.Type.Write);

        lastDemoFile = entry;
        gDemoScreen.newgame(gNet.FakeMultiplayer, item, demfile.Episode, demfile.Level - 1, demfile.Skill);

        Console.out.println("Playing demo " + entry.getName());

        return true;
    }

    @Override
    protected void startboard(final Runnable startboard) {
        game.doPrecache(() -> {
            startboard.run(); //call faketimehandler
            pNet.ResetTimers(); //reset ototalclock
            totalsynctics = 0;
            pNet.ready2send = false;
        });
    }

    @Override
    public void render(float delta) {
//        if (gNet.FakeMultiplayer) {
//             pEngine.faketimerhandler();
//        }

        if (numplayers > 1) {
            pNet.GetPackets();
        }

        DemoRender();

        float smoothratio = 65536;
        if (!game.gPaused) {
            smoothratio = pEngine.getTimer().getsmoothratio(delta);
            if (smoothratio < 0 || smoothratio > 0x10000) {
                smoothratio = BClipRange(smoothratio, 0, 0x10000);
            }
        }

        game.pInt.dointerpolations(smoothratio);
        DrawWorld(smoothratio);

        DrawHud(smoothratio);
        game.pInt.restoreinterpolations();

        if (pMenu.gShowMenu) {
            pMenu.mDrawMenu();
        }

        PostFrame(pNet);

        pEngine.nextpage(delta);
    }

    private void DemoRender() {
        pNet.ready2send = false;

        if (!game.isCurrentScreen(this)) {
            return;
        }

        if (!game.gPaused && demfile != null) {

            while (engine.getTotalClock() >= (totalsynctics + synctics)) {
                CompareService.update(demfile.rcnt);
                for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
                    pNet.gFifoInput[pNet.gNetFifoHead[j] & 0xFF][j].Copy(demfile.recsync[j][demfile.rcnt]);
                    pNet.gNetFifoHead[j]++;
                }
                demfile.reccnt--;

                if (demfile.reccnt <= 0) {
                    demfile = null;
                    Gdx.app.postRunnable(Game::TerminateLevel);
                    Group group = lastDemoFile.getParent();
                    if (!showDemo(group)) {
                        game.changeScreen(gMenuScreen);
                    }
                    return;
                }

                demfile.rcnt++;
                game.pInt.clearinterpolations();
                game.pIntSkip2.clearinterpolations();
                game.pIntSkip4.clearinterpolations();

                ProcessFrame(pNet);
            }
        } else {
            totalsynctics = engine.getTotalClock();
        }
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        if (gameKeyDownCommon(gameKey, false)) {
            return true;
        }

        PlayerStr pp = Player[myconnectindex];
        if (GameKeys.Enlarge_Screen.equals(gameKey)) {
            SetBorder(pp, cfg.BorderNum + 1);
        }

        if (GameKeys.Shrink_Screen.equals(gameKey)) {
            SetBorder(pp, cfg.BorderNum - 1);
        }

        return false;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean showDemo(Group group) {
        List<Entry> list = checkDemoEntry(group);
        switch (cfg.gDemoSeq) {
            case 0: //OFF
                return false;
            case 1: //Consistently
                if (nDemonum < (list.size() - 1)) {
                    nDemonum++;
                } else {
                    nDemonum = 0;
                }
                break;
            case 2: //Accidentally
                int nextnum = nDemonum;
                if (list.size() > 1) {
                    while (nextnum == nDemonum) {
                        nextnum = (int) (Math.random() * (list.size()));
                    }
                }
                nDemonum = BClipRange(nextnum, 0, list.size() - 1); // #GDX 28.12.2024
                break;
        }

        if (!list.isEmpty()) {
            return showDemo(list.get(nDemonum), null);
        }

        return false;
    }

    public static boolean checkDemoEntry(Entry file) {
        return file.exists() && file.isExtension("dmo");
    }

    public List<Entry> checkDemoEntry(Group group) {
        if (demofiles.containsKey(group)) {
            return demofiles.get(group);
        }

        nDemonum = -1;
        List<Entry> demos = group.stream()
                .filter(DemoScreen::checkDemoEntry)
                .sorted(Entry::compareTo)
                .collect(Collectors.toList());

        demofiles.put(group, demos);
        Console.out.println("There are " + demos.size() + " demo(s) in the loop", OsdColor.YELLOW);

        if (cfg.gDemoSeq == 2) {
            int nextnum = nDemonum;
            if (demos.size() > 1) {
                while (nextnum == nDemonum) {
                    nextnum = (int) (Math.random() * (demos.size()));
                }
            }
            nDemonum = nextnum;
        }

        return demos;
    }

    public void onStopRecord() {
        if (rec == null) {
            return;
        }

        CompareService.close();
        rec.close();
        rec = null;
//        recstat = DEMOSTAT_NULL;
    }

    public static boolean isDemoPlaying() {
        return false;
    }

}
