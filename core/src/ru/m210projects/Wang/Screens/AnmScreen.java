package ru.m210projects.Wang.Screens;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.MovieScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Wang.Type.AnimFile;

import static ru.m210projects.Build.Engine.MAXTILES;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Sound.*;

public class AnmScreen extends MovieScreen {

    private final String[] ANIMname = {"sw.anm", "swend.anm", "sumocinm.anm", "zfcin.anm"};
    private int ANIMnum;
    private float ANIMrate;

    public AnmScreen(BuildGame game) {
        super(game, MAXTILES - 2);

        this.nFlags = 4 | 8 | 64;
    }

    public boolean init(int anim_num) {
        if (isInited()) {
            return false;
        }

        if (anim_num < 0 || anim_num >= ANIMname.length) {
            return false;
        }

        if (!open(ANIMname[anim_num])) {
            return false;
        }
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        ArtEntry pic = engine.getTile(TILE_MOVIE);
        float kt = pic.getHeight() / (float) pic.getWidth();
        float kv = xdim / (float) ydim;

        float scale;
        if (kv >= kt) {
            scale = (ydim / (float) pic.getWidth());
        } else {
            scale = (xdim / (float) pic.getHeight());
        }

        nScale = (int) (scale * 65536.0f);
        nPosX = (xdim / 2);
        nPosY = (ydim / 2);

        ANIMnum = anim_num;
        ANIMrate = 0;
        frame = 1;

        return true;
    }

    @Override
    protected MovieFile GetFile(String file) {
        Entry movieEntry = game.getCache().getEntry(file, true);
        if (!movieEntry.exists()) {
            return null;
        }

        return new AnimFile(movieEntry.getBytes()) {
            @Override
            public float getRate() {
                return ANIMrate;
            }
        };
    }

    @Override
    protected void StopAllSounds() {
        COVER_SetReverb(0);
        if (ANIMnum != ANIM_INTRO) {
            StopSound();
        }
    }

    @Override
    protected byte[] DoDrawFrame(int i) {
        ANIMrate = 0;
        switch (ANIMnum) {
            case ANIM_INTRO:
                AnimShareIntro(i, mvfil.getFrames());
                break;
            case ANIM_SERP:
                AnimSerp(i, mvfil.getFrames());
                break;
            case ANIM_SUMO:
                AnimSumo(i, mvfil.getFrames());
                break;
            case ANIM_ZILLA:
                AnimZilla(i, mvfil.getFrames());
                break;
        }

        return mvfil.getFrame(frame);
    }

    @Override
    protected Font GetFont() {
        return game.getFont(0);
    }

    @Override
    protected void DrawEscText(Font font, int pal) {
        int shade = 16 + mulscale(16, EngineUtils.sin((20 * engine.getTotalClock()) & 2047), 16);
        font.drawTextScaled(game.getRenderer(), 160, 5, "Press ESC to skip", 1.0f, shade, pal, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }

    protected void AnimShareIntro(int frame, int numframes) {
        if (frame == numframes - 1) {
            ANIMrate += 1000.0f;
        } else if (frame == 1) {
            PlaySound(DIGI_NOMESSWITHWANG, null, v3df_none);
            ANIMrate += 1000.0f * 3;
        } else {
            ANIMrate += 60.0f;
        }

        if (frame == 5) {
            PlaySound(DIGI_INTRO_SLASH, null, v3df_none);
        } else if (frame == 15) {
            PlaySound(DIGI_INTRO_WHIRL, null, v3df_none);
        }
    }

    protected void AnimSerp(int frame, int numframes) {
        ANIMrate += 130.0f;

        if (frame == numframes - 1) {
            ANIMrate += 1250.0f;
        }

        if (frame == 1) {
            PlaySound(DIGI_SERPTAUNTWANG, null, v3df_none);
        } else if (frame == 16) {
            PlaySound(DIGI_SHAREND_TELEPORT, null, v3df_none);
        } else if (frame == 35) {
            PlaySound(DIGI_WANGTAUNTSERP1, null, v3df_none);
        } else if (frame == 51) {
            PlaySound(DIGI_SHAREND_UGLY1, null, v3df_none);
        } else if (frame == 64) {
            PlaySound(DIGI_SHAREND_UGLY2, null, v3df_none);
        }
    }

    protected void AnimSumo(int frame, int numframes) {
        ANIMrate += 60.0f;

        if (frame == numframes - 1) {
            ANIMrate += 1000.0f;
        }

        if (frame == 1) {
            ANIMrate += 250.0f;
        }

        if (frame == 2) {
            // hungry
            PlaySound(DIGI_JG41012, null, v3df_none);
        } else if (frame == 30) {
            PlaySound(DIGI_HOTHEADSWITCH, null, v3df_none);
        } else if (frame == 42) {
            PlaySound(DIGI_HOTHEADSWITCH, null, v3df_none);
        } else if (frame == 59) {
            PlaySound(DIGI_JG41028, null, v3df_none);
        }
    }

    protected void AnimZilla(int frame, int numframes) {
        ANIMrate += 130.0f;

        if (frame == numframes - 1) {
            ANIMrate += 1250.0f;
        }

        if (frame == 1) {
            PlaySound(DIGI_ZC1, null, v3df_none);
        } else if (frame == 5) {
            PlaySound(DIGI_JG94024, null, v3df_none);
        } else if (frame == 14) {
            PlaySound(DIGI_ZC2, null, v3df_none);
        } else if (frame == 30) {
            PlaySound(DIGI_ZC3, null, v3df_none);
        } else if (frame == 32) {
            PlaySound(DIGI_ZC4, null, v3df_none);
        } else if (frame == 37) {
            PlaySound(DIGI_ZC5, null, v3df_none);
        } else if (frame == 63) {
            PlaySound(DIGI_Z16043, null, v3df_none);
            PlaySound(DIGI_ZC6, null, v3df_none);
            PlaySound(DIGI_ZC7, null, v3df_none);
        } else if (frame == 72) {
            PlaySound(DIGI_ZC7, null, v3df_none);
        } else if (frame == 73) {
            PlaySound(DIGI_ZC4, null, v3df_none);
        } else if (frame == 77) {
            PlaySound(DIGI_ZC5, null, v3df_none);
        } else if (frame == 87) {
            PlaySound(DIGI_ZC8, null, v3df_none);
        } else if (frame == 103) {
            PlaySound(DIGI_ZC7, null, v3df_none);
        } else if (frame == 108) {
            PlaySound(DIGI_ZC9, null, v3df_none);
        } else if (frame == 120) {
            PlaySound(DIGI_JG94039, null, v3df_none);
        }
    }

    public int getAnim() {
        return ANIMnum;
    }
}
