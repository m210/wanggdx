package ru.m210projects.Wang.Screens;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.LoadingAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.Build.Strhandler.toCharArray;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Screens.MenuScreen.DrawBackground;

public class LoadingScreen extends LoadingAdapter {

    public LoadingScreen(BuildGame game) {
        super(game);
    }

    @Override
    public void show() {
        super.show();
        byte[] palette = engine.getPaletteManager().getBasePalette();
        engine.setbrightness(cfg.getPaletteGamma(), palette);
    }

    @Override
    protected void draw(String title, float delta) {
        Renderer renderer = game.getRenderer();
        renderer.clearview(117);
        DrawBackground(engine);

        if (title == null) {
            game.getFont(1).drawTextScaled(renderer, 160, 170, toCharArray("Please wait"), 1.0f, -128, 4, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        } else {
            if (mUserFlag == UserFlag.UserMap) {
                game.getFont(1).drawTextScaled(renderer, 160, 170, toCharArray("Entering usermap"),1.0f, -128, 4, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            } else {
                game.getFont(1).drawTextScaled(renderer, 160, 170, toCharArray("Entering "), 1.0f,-128, 4, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            }
            game.getFont(1).drawTextScaled(renderer, 160, 170 + 15, toCharArray(title), 1.0f,-128, 4, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }
    }

}
