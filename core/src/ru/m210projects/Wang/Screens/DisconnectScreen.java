package ru.m210projects.Wang.Screens;


import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.USER;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Build.Strhandler.Bitoa;
import static ru.m210projects.Build.Strhandler.buildString;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.getUser;
import static ru.m210projects.Wang.Main.gNet;
import static ru.m210projects.Wang.Names.STAT_SCREEN_PIC;
import static ru.m210projects.Wang.Palette.PALETTE_PLAYER0;
import static ru.m210projects.Wang.Sound.COVER_SetReverb;
import static ru.m210projects.Wang.Sound.StopSound;
import static ru.m210projects.Wang.Text.DisplayMiniBarSmString;

public class DisconnectScreen extends StatisticScreen {

    protected final int STAT_START_X = 20;
    protected final int STAT_START_Y = 85;
    protected final int STAT_OFF_Y = 9;
    protected final int STAT_HEADER_Y = 14;
    protected final int STAT_TABLE_X = (STAT_START_X + 15 * 4);
    protected final int STAT_TABLE_XOFF = 6 * 4;
    protected final int[] death_total = new int[8];
    protected final int[] kills = new int[8];
    protected final List<Integer> playerList;
    protected boolean skipRequest;

    public DisconnectScreen(Main app) {
        super(app);
        playerList = new ArrayList<>();
    }

    public void updateList() {
        playerList.clear();
        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            playerList.add(i);
        }
    }

    @Override
    public void show() {
        super.show();

        skipRequest = false;
        COVER_SetReverb(0);
        StopSound();
    }

    @Override
    public void hide() {
        updateList();
    }

    @Override
    public void draw(float delta) {
        checkMusic();

        if (numplayers > 1) {
            game.pNet.GetPackets();
        }

        if (dobonus()) {
            Gdx.app.postRunnable(() -> game.show());
        }

        engine.nextpage(delta);
    }

    @Override
    public void skip() {
        skipRequest = true;
    }

    public boolean dobonus() {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, STAT_SCREEN_PIC, 0, 0, 2 | 8);

        app.getFont(1).drawTextScaled(renderer,  160, 68, "MULTIPLAYER TOTALS", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

        int x = STAT_START_X;
        int y = STAT_START_Y;

        PlayerStr mpp = Player[myconnectindex];

        buildString(bonusbuf, 0, "  NAME ");
        DisplayMiniBarSmString(mpp, x, y, 0, bonusbuf, 0);
        x = STAT_TABLE_X;
        for (int i = 0; i < 8; i++) {
            Bitoa(i + 1, bonusbuf);
            DisplayMiniBarSmString(mpp, x, y, 0, bonusbuf, 0);
            x += STAT_TABLE_XOFF;
        }
        buildString(bonusbuf, 0, "  KILLS ");
        DisplayMiniBarSmString(mpp, x, y, 0, bonusbuf, 0);

        int rows = playerList.size();
        int cols = playerList.size();

        y += STAT_HEADER_Y;

        if (gNet.MultiGameType != MultiGameTypes.MULTI_GAME_COOPERATIVE) {

            Arrays.fill(death_total, 0);
            Arrays.fill(kills, 0);

            for (int i = 0; i < rows; i++) {
                int num = playerList.get(i);
                x = STAT_START_X;
                PlayerStr pp = Player[num];
                USER pu = getUser(pp.PlayerSprite);

                int offs = Bitoa(i + 1, bonusbuf);
                buildString(bonusbuf, offs, " ", pp.getName());
                DisplayMiniBarSmString(mpp, x, y,
                        (pp.PlayerSprite != -1 && pu != null) ? pu.spal : (PALETTE_PLAYER0 + pp.TeamColor), bonusbuf, 0);

                x = STAT_TABLE_X;
                for (int j = 0; j < cols; j++) {
                    int pal = 0;
                    death_total[j] += pp.KilledPlayer[j];

                    if (i == j) {
                        // don't add kill for self or team player
                        pal = PALETTE_PLAYER0 + 4;
                        kills[i] -= pp.KilledPlayer[j]; // subtract self kills
                    } else if (gNet.TeamPlay && pp.PlayerSprite != -1) {
                        USER pu2 = getUser(Player[j].PlayerSprite);
                        if (pu2 != null && pu != null && pu.spal == pu2.spal) {
                            // don't add kill for self or team player
                            pal = PALETTE_PLAYER0 + 4;
                            kills[i] -= pp.KilledPlayer[j]; // subtract self kills
                        } else {
                            kills[i] += pp.KilledPlayer[j]; // kills added here
                        }
                    } else {
                        kills[i] += pp.KilledPlayer[j]; // kills added here
                    }

                    Bitoa(pp.KilledPlayer[j], bonusbuf);
                    DisplayMiniBarSmString(mpp, x, y, pal, bonusbuf, 0);
                    x += STAT_TABLE_XOFF;
                }

                y += STAT_OFF_Y;
            }

            // Deaths

            x = STAT_START_X;
            y += STAT_OFF_Y;

            buildString(bonusbuf, 0, "   DEATHS");
            DisplayMiniBarSmString(mpp, x, y, 0, bonusbuf, 0);
            x = STAT_TABLE_X;

            for (int j = 0; j < cols; j++) {
                Bitoa(death_total[j], bonusbuf);
                DisplayMiniBarSmString(mpp, x, y, 0, bonusbuf, 0);
                x += STAT_TABLE_XOFF;
            }

            // Kills
            x = STAT_TABLE_X + 50 * 4;
            y = STAT_START_Y + STAT_HEADER_Y;

            for (int i = 0; i < rows; i++) {
                Bitoa(kills[i], bonusbuf);
                DisplayMiniBarSmString(mpp, x, y, 0, bonusbuf, 0);

                y += STAT_OFF_Y;
            }
        } else {
            for (int num = 0; num < rows; num++) {
                int i = playerList.get(num);

                PlayerStr pp = Player[i];

                x = STAT_START_X;
                int offs = Bitoa(i + 1, bonusbuf);
                buildString(bonusbuf, offs, " ", pp.getName());
                USER pu = getUser(pp.PlayerSprite);
                DisplayMiniBarSmString(mpp, x, y,
                        (pp.PlayerSprite != -1 && pu != null) ? pu.spal : (PALETTE_PLAYER0 + pp.TeamColor), bonusbuf, 0);

                x = STAT_TABLE_X + 50 * 4;
                Bitoa(pp.Kills, bonusbuf);
                app.getFont(0).drawTextScaled(renderer, x, y, bonusbuf, 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                y += STAT_OFF_Y;
            }
        }

        app.getFont(1).drawTextScaled(renderer, 160, 189, "PRESS ANY KEY TO CONTINUE", 1.0f, 0, 2, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

        return skipRequest && engine.getTotalClock() > (60 * 2);
    }

}
