package ru.m210projects.Wang.Screens;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.ScreenAdapters.MenuAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Wang.Factory.WangMenuHandler;
import ru.m210projects.Wang.Main;

import static ru.m210projects.Build.Gameutils.coordsConvertXScaled;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Wang.Factory.WangMenuHandler.MAIN;
import static ru.m210projects.Wang.Main.gNet;
import static ru.m210projects.Wang.Names.LOADSCREEN;
import static ru.m210projects.Wang.Sound.CDAudio_Play;
import static ru.m210projects.Wang.Sound.COVER_SetReverb;
import static ru.m210projects.Wang.Text.DrawFragBar;

public class MenuScreen extends MenuAdapter {

    private final WangMenuHandler menu;

    public MenuScreen(Main game) {
        super(game, game.menu.mMenus[MAIN]);
        this.menu = game.menu;
    }

    public static void DrawBackground(Engine engine) {
        Renderer renderer = Main.game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        int scale = divscale(ydim, 240, 16);
        int width = mulscale(engine.getTile(9220).getWidth(), scale, 16);

        if (width != 0) {
            int framesnum = xdim / width;
            int fy = scale(20, ydim, 240);

            int statusx1 = coordsConvertXScaled(160 - engine.getTile(LOADSCREEN).getWidth() / 2, ConvertType.Normal);
            int statusx2 = coordsConvertXScaled(160 + engine.getTile(LOADSCREEN).getWidth() / 2, ConvertType.Normal);

            int x = 0;
            for (int i = 0; i <= framesnum; i++) {
                if (x - width <= statusx1 || x + width >= statusx2) {
                    renderer.rotatesprite(x << 16, fy << 16, scale, 0, 9220, 0, 0, 8 | 16 | 256);
                }
                x += width;
            }
        }
        renderer.rotatesprite(xdim << 15, ydim << 15, scale, 0, LOADSCREEN, 0, 0, 8);
    }

    @Override
    public void show() {
        byte[] palette = engine.getPaletteManager().getBasePalette();
        engine.setbrightness(Main.cfg.getPaletteGamma(), palette);
        if (!menu.gShowMenu) {
            menu.mOpen(menu.mMenus[MAIN], -1);
        }
        CDAudio_Play(2, true);
        COVER_SetReverb(0);
    }

    @Override
    public void process(float delta) {
        if (numplayers > 1 || gNet.FakeMultiplayer) {
            DrawFragBar();
        }

        if (!game.gPaused) {
            game.pNet.GetPackets();
        }
    }

    @Override
    public void draw(float delta) {
        game.getRenderer().clearview(117);

        DrawBackground(engine);
    }
}
