package ru.m210projects.Wang.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.ScreenAdapters.GameAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Wang.Config.SwKeys;
import ru.m210projects.Wang.Factory.DemoRecorder;
import ru.m210projects.Wang.Factory.WangMenuHandler;
import ru.m210projects.Wang.Factory.WangNetwork;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Sound;
import ru.m210projects.Wang.Type.GameInfo;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.USER;
import ru.m210projects.Wang.Type.VOC3D;

import java.io.FileOutputStream;
import java.nio.file.Path;

import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Strhandler.toCharArray;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Border.SetBorder;
import static ru.m210projects.Wang.Cheats.InfinityAmmo;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Draw.*;
import static ru.m210projects.Wang.Factory.WangMenuHandler.*;
import static ru.m210projects.Wang.Factory.WangNetwork.*;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Inv.MAX_INVENTORY;
import static ru.m210projects.Wang.JPlayer.adduserquote;
import static ru.m210projects.Wang.LoadSave.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Palette.*;
import static ru.m210projects.Wang.Player.domovethings;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Text.PutStringInfoLine;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.ResourceHandler.checkEpisodeResources;
import static ru.m210projects.Wang.Type.ResourceHandler.resetEpisodeResources;

public class GameScreen extends GameAdapter {

    private final Main game;
    private int nonsharedtimer;
    private boolean NewGame = false;

    public GameScreen(Main game) {
        super(game, gLoadingScreen);

        this.game = game;
    }

    @Override
    public void ProcessFrame(BuildNet net) {
        domovethings(net);
    }

    @Override
    public void DrawWorld(float smooth) {
        // backpos should be saved before interpolation, because some objects added to both interpolators and backpos will be rewritten after pIntSkip2 in pIntSkip4
        game.pIntSkip2.saveinterpolations();
        game.pIntSkip4.saveinterpolations();

        game.pIntSkip2.dointerpolations(smooth);
        game.pIntSkip4.dointerpolations(smooth);
        drawscreen(Player[screenpeek], (int) smooth);
    }

    @Override
    public void DrawHud(float smooth) {
        drawhud(Player[screenpeek]);
        Renderer renderer = game.getRenderer();
        if (game.net.bOutOfSync) {
            game.getFont(1).drawTextScaled(renderer, 160, 20, toCharArray("Out of sync!"), 1.0f, 0, 12, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

            switch (game.net.bOutOfSyncByte / 4) {
                case 0: // bseed
                    game.getFont(1).drawTextScaled(renderer, 160, 30, toCharArray("seed checksum error"), 1.0f, 0, 12, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                    break;
                case 1: // player
                    game.getFont(1).drawTextScaled(renderer, 160, 30, toCharArray("player struct checksum error"), 1.0f, 0, 12, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                    break;
                case 2: // sprite
                    game.getFont(1).drawTextScaled(renderer, 160, 30, toCharArray("player sprite checksum error"), 1.0f, 0, 12, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                    break;
                case 3: // missile
                    game.getFont(1).drawTextScaled(renderer, 160, 30, toCharArray("missile sprites checksum error"), 1.0f, 0, 12, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                    break;
            }
        }

        game.pIntSkip2.restoreinterpolations();
        game.pIntSkip4.restoreinterpolations();

        if (Player[screenpeek].FadeAmt != 0) {
            renderer.showScreenFade(WANG_SCREEN_FADE);
        }
    }

    @Override
    public void PostFrame(BuildNet net) {
        if (gQuickSaving) {
            if (captBuffer != null) {
                savegame(game.getUserDirectory(), "[quicksave_" + quickslot + "]", "quicksav" + quickslot + ".sav");
                quickslot ^= 1;
                gQuickSaving = false;
            } else {
                gGameScreen.capture(160, 100);
            }
        }

        if (gAutosaveRequest) {
            if (captBuffer != null) {
                savegame(game.getUserDirectory(), "[autosave]", "autosave.sav");
                gAutosaveRequest = false;
            } else {
                gGameScreen.capture(160, 100);
            }
        }
    }

    protected boolean gameKeyDownCommon(GameKey gameKey, boolean inGame) {
        // the super has console button handling
        if (super.gameKeyDown(gameKey)) {
            return true;
        }

        boolean shiftPressed = Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT);
        boolean ctrlPressed = Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT);
        if (shiftPressed || ctrlPressed) {
            return false;
        }

        WangMenuHandler menu = game.menu;
        if (GameKeys.Menu_Toggle.equals(gameKey)) {
            if (inGame) {
                menu.mOpen(menu.mMenus[GAME], -1);
            } else {
                menu.mOpen(menu.mMenus[MAIN], -1);
            }
            return true;
        }

        if (SwKeys.Show_Help.equals(gameKey)) {
            menu.mOpen(menu.mMenus[HELP], -1);
            return true;
        }

        if (SwKeys.Show_Loadmenu.equals(gameKey)) {
            if (numplayers > 1 || game.net.FakeMultiplayer) {
                return false;
            }
            menu.mOpen(menu.mMenus[LOADGAME], -1);
            return true;
        }

        PlayerStr pp = Player[myconnectindex];
        if ((dimensionmode == 5 || dimensionmode == 6)) {
            if (SwKeys.Map_Follow_Mode.equals(gameKey)) {
                ScrollMode2D = !ScrollMode2D;

                if (ScrollMode2D) {
                    Follow_posx = pp.oposx;
                    Follow_posy = pp.oposy;
//	   	             ud.fola = (int) pp.oang;
                }

                PutStringInfoLine(pp, "ScrollMode " + (ScrollMode2D ? "ON" : "OFF"));
                return true;
            }
        }

        if (SwKeys.Show_Sounds.equals(gameKey)) {
            menu.mOpen(menu.mMenus[SOUNDSET], -1);
            return true;
        }

        if (SwKeys.Show_Options.equals(gameKey)) {
            menu.mOpen(menu.mMenus[OPTIONS], -1);
            return true;
        }

        if (SwKeys.Gamma.equals(gameKey)) {
            openGamma(menu);
            return true;
        }

        if (SwKeys.Messages.equals(gameKey)) {
            cfg.Messages = !cfg.Messages;

            PutStringInfoLine(pp, "Messages " + (cfg.Messages ? "ON" : "OFF"));
            return true;
        }

        if (SwKeys.Toggle_Crosshair.equals(gameKey)) {
            cfg.Crosshair = !cfg.Crosshair;
            PutStringInfoLine(pp, "Crosshair " + (cfg.Crosshair ? "ON" : "OFF"));
            return true;
        }

        if (SwKeys.Quit.equals(gameKey)) {
            menu.mOpen(menu.mMenus[QUIT], -1);
            return true;
        }

        if (SwKeys.Screenshot.equals(gameKey)) {
            makeScreenshot();
            return true;
        }

        if (SwKeys.See_Coop_View.equals(gameKey)) {
            if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COOPERATIVE || gNet.FakeMultiplayer) {
                screenpeek = connectpoint2[screenpeek];
                if (screenpeek < 0) {
                    screenpeek = connecthead;
                }

                ResetPalette(Player[screenpeek], FORCERESET); // screenpeek
                DoPlayerDivePalette(Player[screenpeek]);
                DoPlayerNightVisionPalette(Player[screenpeek]);
            }
            return true;
        }

        return false;
    }

    @Override
    public void processInput(GameProcessor processor) {
        if ((dimensionmode == 5 || dimensionmode == 6)) {
            int j = engine.getTotalClock() - nonsharedtimer;
            nonsharedtimer += j;
            if (processor.isGameKeyPressed(GameKeys.Enlarge_Screen)) {
                zoom += mulscale(j, Math.max(zoom, 256), 6);
            }
            if (processor.isGameKeyPressed(GameKeys.Shrink_Screen)) {
                zoom -= mulscale(j, Math.max(zoom, 256), 6);
            }

            if ((zoom > 2048)) {
                zoom = 2048;
            }
            if ((zoom < 48)) {
                zoom = 48;
            }
        }
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        boolean shiftPressed = Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT);
        boolean ctrlPressed = Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT);

        if (gameKeyDownCommon(gameKey, true)) {
            return true;
        }

        PlayerStr pp = Player[myconnectindex];
        WangMenuHandler menu = game.menu;

        if (SwKeys.See_Chase_View.equals(gameKey)) {
            if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT)) {
                if (TEST(pp.Flags, PF_VIEW_FROM_OUTSIDE)) {
                    pp.view_outside_dang = NORM_ANGLE(pp.view_outside_dang + 256);
                }
            } else {
                if (TEST(pp.Flags, PF_VIEW_FROM_OUTSIDE)) {
                    pp.Flags &= ~(PF_VIEW_FROM_OUTSIDE);
                } else {
                    pp.Flags |= (PF_VIEW_FROM_OUTSIDE);
                    pp.camera_dist = 0;
                }
            }
        }

        if (shiftPressed || ctrlPressed) {
            return false;
        }

        if (SwKeys.Show_Savemenu.equals(gameKey)) {
            if (numplayers > 1 || game.net.FakeMultiplayer) {
                return false;
            }
            if (!TEST(pp.Flags, PF_DEAD)) {
                gGameScreen.capture(160, 100);
                menu.mOpen(menu.mMenus[SAVEGAME], -1);
            }
        }

        if (SwKeys.Quicksave.equals(gameKey)) { // quick save
            quicksave();
        }

        if (SwKeys.Quickload.equals(gameKey)) { // quick load
            quickload();
        }

        if (dimensionmode != 5 && dimensionmode != 6) {
            if (GameKeys.Enlarge_Screen.equals(gameKey)) {
                SetBorder(pp, cfg.BorderNum + 1);
            }
            if (GameKeys.Shrink_Screen.equals(gameKey)) {
                SetBorder(pp, cfg.BorderNum - 1);
            }
        }

        return false;
    }

    @Override
    public void sndHandlePause(boolean pause) {
        Sound.sndHandlePause(pause);
    }

    @Override
    protected boolean prepareboard(Entry map) {
        gNameShowTime = 500;
        FinishAnim = 0;
        if (!InitLevel(map, NewGame)) {
            return false;
        }

        boardfilename = map;
        // auto aim / auto run / etc
        if (GameScreen.this != gDemoScreen) {
            gDemoScreen.demfile = null; // reset demo flag
            InitPlayerGameSettings();
        } else {
            Player[myconnectindex].Flags = gDemoScreen.demfile.Flags[myconnectindex];
            if (gDemoScreen.demfile.nVersion != 1) {
                Player[myconnectindex].Armor = (short) gDemoScreen.demfile.Armors[myconnectindex];
                Player[myconnectindex].WpnFlags = (short) gDemoScreen.demfile.WpnFlags[myconnectindex];
                Player[myconnectindex].MaxHealth = (short) gDemoScreen.demfile.MaxHealth[myconnectindex];

                for (int i = 0; i < MAX_WEAPONS; i++) {
                    Player[myconnectindex].WpnAmmo[i] = (short) gDemoScreen.demfile.WpnAmmo[myconnectindex][i];
                }
                Player[myconnectindex].WpnRocketHeat = gDemoScreen.demfile.WpnRocketHeats[myconnectindex];
                Player[myconnectindex].WpnRocketNuke = gDemoScreen.demfile.WpnRocketNukes[myconnectindex];

                for (int i = 0; i < MAX_INVENTORY; i++) {
                    Player[myconnectindex].InventoryPercent[i] = (short) gDemoScreen.demfile.InventoryPercent[myconnectindex][i];
                    Player[myconnectindex].InventoryAmount[i] = (short) gDemoScreen.demfile.InventoryAmount[myconnectindex][i];
                }
                Player[myconnectindex].InventoryNum = gDemoScreen.demfile.InventoryNums[myconnectindex];

                USER u = getUser(Player[myconnectindex].PlayerSprite);
                if (u != null) {
                    u.Health = (short) gDemoScreen.demfile.Health[myconnectindex];
                }
            }
        }

        // send packets with player info
        gNet.InitNetPlayerOptions();

        // Initialize Game part of network code (When ready2send != 0)
        gNet.ResetTimers();

        gNet.InitPrediction(Player[myconnectindex]);

        gNet.WaitForAllPlayers(0);
        engine.getTimer().reset();

        // IMPORTANT - MUST be right before game loop AFTER waitforeverybody
        InitTimingVars();

        StartMusic();
        StartAmbientSound();

        if (DemoRecording) {
            int a, b, c, d, democount = 0;
            do {
                a = ((democount / 1000) % 10);
                b = ((democount / 100) % 10);
                c = ((democount / 10) % 10);
                d = (democount % 10);

                String fn = "demo" + a + b + c + d + ".dmo";
                if (!game.getCache().getGameDirectory().getEntry(fn).exists()) {
                    try {
                        Path path = game.getCache().getGameDirectory().getPath().resolve(fn);
                        rec = new DemoRecorder(new FileOutputStream(path.toFile()), path, 2, map.getName());
                        Console.out.println("Start recording to " + fn);
                    } catch (Exception e) {
                        Console.out.println("Can't start demo record: " + e, OsdColor.RED);
                    }
                    break;
                }

                democount++;
            } while (democount < 9999);
        }

        if (!NewGame && game.nNetMode == NetMode.Single && GameScreen.this != gDemoScreen) {
            gAutosaveRequest = true;
        }

        NewGame = false;

        System.err.println(map);
        return true;
    }

    public void newgame(final boolean isMultiplayer, final Object item, final int nEpisode, final int nLevel, final int nDifficulty) {

        if (rec != null) {
            rec.close();
        }

        if (numplayers > 1 && game.pNet.bufferJitter >= 0 && myconnectindex == connecthead) {
            for (int i = 0; i <= game.pNet.bufferJitter; i++) {
                game.pNet.GetNetworkInput(); // wait for other player before level end
            }
        }

        boardfilename = DUMMY_ENTRY;
        pNet.ready2send = false;
        game.changeScreen(load);
        load.init(() -> {
            InfinityAmmo = false;

            if (!isMultiplayer) {
                if (numplayers > 1) {
                    pNet.NetDisconnect(myconnectindex);
                }

                connecthead = 0;
                connectpoint2[0] = -1;
                CommPlayers = 1;
                game.nNetMode = NetMode.Single;
                gNet.FakeMultiplayer = false;

                if (GameScreen.this != gDemoScreen) {
                    gNet.KillLimit = 0;
                    gNet.TimeLimit = 0;
                    gNet.TimeLimitClock = 0;
                    gNet.MultiGameType = MultiGameTypes.MULTI_GAME_NONE;
                    gNet.TeamPlay = false;
                    gNet.HurtTeammate = false;
                    gNet.SpawnMarkers = false;
                    gNet.NoRespawn = false;
                    gNet.Nuke = true;
                }
            } else {
                if (gNet.FakeMultiplayer) {
                    connecthead = 0;
                    CommPlayers = gNet.FakeMultiNumPlayers;
                    for (int i = 0; i < MAXPLAYERS; i++) {
                        connectpoint2[i] = (short) (i + 1);
                    }
                    connectpoint2[gNet.FakeMultiNumPlayers - 1] = -1;
                } else {
                    CommPlayers = numplayers;
                }

                if (GameScreen.this != gDemoScreen) {
                    gNet.HurtTeammate = pNetInfo.nFriendlyFire == 1;
                    gNet.SpawnMarkers = pNetInfo.SpawnMarkers;
                    gNet.TeamPlay = pNetInfo.TeamPlay;
                    gNet.Nuke = pNetInfo.NetNuke;
                    gNet.KillLimit = pNetInfo.KillLimit * 10;
                    gNet.TimeLimit = TimeLimitTable[pNetInfo.TimeLimit] * 60 * 120;

                    gNet.TimeLimitClock = gNet.TimeLimit;
                    gNet.MultiGameType = MultiGameTypes.values()[pNetInfo.nGameType + 1];
                    // settings for No Respawn Commbat mode
                    if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT_NO_RESPAWN) {
                        gNet.MultiGameType = MultiGameTypes.MULTI_GAME_COMMBAT;
                        gNet.NoRespawn = true;
                    } else {
                        gNet.NoRespawn = false;
                    }
                }

                GodMode = false;
                game.nNetMode = NetMode.Multiplayer;
            }

            UserFlag flag = UserFlag.None;
            if (item instanceof GameInfo && !item.equals(defGame)) {
                flag = UserFlag.Addon;
                GameInfo game = (GameInfo) item;
                checkEpisodeResources(game);
                Console.out.println("Start user episode: " + game.Title);
            } else {
                resetEpisodeResources();
            }

            if (item instanceof FileEntry) {
                flag = UserFlag.UserMap;
                boardfilename = (Entry) item;
                Level = 0;
                Console.out.println("Start user map: " + boardfilename.getName());
            }
            mUserFlag = flag;

            if (GameScreen.this != gDemoScreen) {
                if (!isMultiplayer) {
                    VOC3D skillvoice = null;
                    switch (nDifficulty) {
                        case 0:
                            skillvoice = PlaySound(DIGI_TAUNTAI3, null, v3df_none);
                            break;
                        case 1:
                            skillvoice = PlaySound(DIGI_NOFEAR, null, v3df_none);
                            break;
                        case 2:
                            skillvoice = PlaySound(DIGI_WHOWANTSWANG, null, v3df_none);
                            break;
                        case 3:
                            skillvoice = PlaySound(DIGI_NOPAIN, null, v3df_none);
                            break;
                    }

                    long startTime = System.currentTimeMillis();
                    while (skillvoice != null && skillvoice.isPlaying() && skillvoice.isActive()) {
                        if (System.currentTimeMillis() - startTime > 5000) {
                            break;
                        }
                    }
                }
            }

            if (nEpisode == 0) {
                Level = nLevel + 1;
            } else if (nEpisode == 1) {
                Level = 5 + nLevel;
            } else if (nEpisode == 2) {
                Level = 23 + nLevel;
            }
            Skill = nDifficulty;
            FinishAnim = 0;

            if (flag == UserFlag.Addon && game.nNetMode == NetMode.Single && GameScreen.this != gDemoScreen && nLevel == 0) {
                Entry currentAnmEntry = game.getCache().getEntry("sw.anm", true);
                Entry defAnmEntry = game.getCache().getEntry("sw.anm", false);

                if (currentAnmEntry.exists() && defAnmEntry.exists() && currentAnmEntry.getChecksum() != defAnmEntry.getChecksum()) {
                    if (gAnmScreen.init(0)) {
                        StopSound();
                        game.changeScreen(gAnmScreen.setCallback(() -> {
                            NewGame = true;
                            if (!enterlevel(getTitle())) {
                                game.show();
                            }
                        }).escSkipping(true));
                        return;
                    }
                }
            }

            NewGame = true;
            if (!enterlevel(getTitle())) {
                game.show();
            }
        });
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean enterlevel(String title) {
        if (title == null) {
            return false;
        }

        if (GameScreen.this != gDemoScreen) {
            DemoRecording = isDemoRecording;
        }

        Entry map;
        if (mUserFlag == UserFlag.UserMap) {
            map = boardfilename;
        } else {
            map = game.getCache().getEntry(currentGame.getMapPath(Level), true);
        }

        loadboard(map, null).setTitle(title);
        return true;
    }

    public String getTitle() {
        String title = "";
        if (mUserFlag != UserFlag.UserMap) {
            return currentGame.getMapTitle(Level);
        } else {
            Entry file = boardfilename;
            if (file.exists()) {
                title = file.getName();
            }
        }
        return title;
    }

    protected void openGamma(WangMenuHandler menu) {
        menu.mOpen(menu.mMenus[COLORCORR], -1);
    }

    protected void makeScreenshot() {
        String name;
        Entry map = boardfilename;
        if (mUserFlag == UserFlag.UserMap) {
            name = "scr-" + map.getName() + "-xxxx.png";
        } else {
            name = "scr-" + getTitle() + "-xxxx.png";
        }

        Renderer renderer = game.getRenderer();
        String filename = renderer.screencapture(game.getUserDirectory(), name);
        if (filename != null) {
            adduserquote(filename + " saved");
        } else {
            adduserquote("Screenshot not saved. Access denied!");
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        boolean shiftPressed = Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT);
        boolean ctrlPressed = Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT);

        if (shiftPressed || ctrlPressed) {
            int fkey = -1;
            for (int i = 0; i < 10; i++) {
                if (keycode == (i + com.badlogic.gdx.Input.Keys.F1)) {
                    fkey = i;
                    break;
                }
            }

            if (fkey >= 0) {
                if (ctrlPressed) {
                    if (PlaySoundRTS(fkey)) {
                        if (CommPlayers > 1) {
                            WangNetwork.PacketType.RTS_Sound.setData(fkey);
                            int l = WangNetwork.PacketType.RTS_Sound.Send(netbuf);
                            game.net.sendtoall(netbuf, l);
                        }
                    }
                }

                if (shiftPressed) {
                    String message = cfg.WangBangMacro[fkey];
                    adduserquote(message);

                    if (CommPlayers > 1) {
                        PacketType.Message.setData(-1, message);
                        int l = PacketType.Message.Send(netbuf);
                        game.net.sendtoall(netbuf, l);
                    }
                }
                return true;
            }
        }

        return super.keyDown(keycode);
    }

}
