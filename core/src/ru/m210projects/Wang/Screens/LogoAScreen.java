package ru.m210projects.Wang.Screens;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.LogoScreen;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;

import static ru.m210projects.Wang.Main.cfg;
import static ru.m210projects.Wang.Names.THREED_REALMS_PIC;
import static ru.m210projects.Wang.Sound.CDAudio_Play;
import static ru.m210projects.Wang.Sound.StopSound;

public class LogoAScreen extends LogoScreen {

    public LogoAScreen(BuildGame game, float gShowTime) {
        super(game, gShowTime);
        this.setTile(THREED_REALMS_PIC);
    }

    @Override
    public void show() {
        super.show();

        StopSound();
        CDAudio_Play(2, true);

        Entry fil = game.getCache().getEntry("3drealms.pal", true);
        fil.load(is -> engine.setbrightness(cfg.getPaletteGamma(), StreamUtils.readBytes(is, 768)));
    }
}
