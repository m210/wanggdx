package ru.m210projects.Wang.Screens;

import ru.m210projects.Build.Pattern.ScreenAdapters.ConnectAdapter;
import ru.m210projects.Wang.Main;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Wang.Factory.WangMenuHandler.NETWORKGAME;
import static ru.m210projects.Wang.Factory.WangNetwork.CommPlayers;
import static ru.m210projects.Wang.Game.screenpeek;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.LOADSCREEN;
import static ru.m210projects.Wang.Sound.StopFX;

public class NetScreen extends ConnectAdapter {

    private final Main app;

    public NetScreen(Main game) {
        super(game, LOADSCREEN, game.getFont(1));
        this.app = game;
    }

    @Override
    public void show() {
        super.show();
        StopFX();
    }

    @Override
    public void back() {
        game.changeScreen(gMenuScreen);
    }

    @Override
    public void connect() {
        gDisconnectScreen.updateList();
        screenpeek = myconnectindex;
        CommPlayers = numplayers;

        gNet.InitNetPlayerProfile();

        app.changeScreen(gMenuScreen);

        app.menu.mClose();
        app.menu.mOpen(app.menu.mMenus[NETWORKGAME], -1);
    }
}
