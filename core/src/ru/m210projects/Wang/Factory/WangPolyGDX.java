package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.GdxRender.GDXOrtho;
import ru.m210projects.Build.Render.GdxRender.GDXRenderer;
import ru.m210projects.Build.settings.GameConfig;

import static ru.m210projects.Wang.Gameutils.FAF_MIRROR_PIC;
import static ru.m210projects.Wang.Gameutils.FAF_PLACE_MIRROR_PIC;
import static ru.m210projects.Wang.JSector.MIRROR;
import static ru.m210projects.Wang.JSector.MIRRORLABEL;

public class WangPolyGDX extends GDXRenderer {

    public WangPolyGDX(GameConfig config) {
        super(config);
    }

    @Override
    protected GDXOrtho allocOrphoRenderer(Engine engine) {
        return new GDXOrtho(this, new WangMapSettings(engine.getBoardService()));
    }

    @Override
    protected int[] getMirrorTextures() {
        return new int[]{MIRROR, FAF_PLACE_MIRROR_PIC, FAF_MIRROR_PIC, MIRRORLABEL, MIRRORLABEL + 1, MIRRORLABEL + 2, MIRRORLABEL + 3, MIRRORLABEL + 4,
                MIRRORLABEL + 5, MIRRORLABEL + 6, MIRRORLABEL + 7};
    }
}
