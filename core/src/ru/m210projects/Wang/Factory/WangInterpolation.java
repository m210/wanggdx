package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.Pattern.Tools.Interpolation;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;

public abstract class WangInterpolation extends Interpolation {

    public abstract int getSkipValue();

    public abstract int getSkipMax();

    @Override
    public void clearinterpolations() {
        if (getSkipValue() != (getSkipMax() - 1)) {
            return;
        }

        super.clearinterpolations();
    }

    @Override
    public void dospriteinterp(Sprite tsp, int smoothratio) {
        smoothratio += getSkipValue() << 16;
        smoothratio /= getSkipMax();

        super.dospriteinterp(tsp, smoothratio);
    }

    @Override
    public void dointerpolations(float smoothratio) {
        smoothratio += getSkipValue() << 16;
        smoothratio /= getSkipMax();

        for (int i = 0; i < InterpolationCount; i++) {
            IData gInt = gInterpolationData[i];
            Object obj = gInt.ptr;

            int value = (int) (gInt.oldpos + ((getValue(gInt) - gInt.oldpos) * smoothratio / 65536.0f));
            switch (gInt.type) {
                case WallX:
                    ((Wall) obj).setX(value);
                    break;
                case WallY:
                    ((Wall) obj).setY(value);
                    break;
                case FloorZ:
                    ((Sector) obj).setFloorz(value);
                    break;
                case CeilZ:
                    ((Sector) obj).setCeilingz(value);
                    break;
                case FloorH:
                    ((Sector) obj).setFloorheinum(value);
                    break;
                case CeilH:
                    ((Sector) obj).setCeilingheinum(value);
                    break;
            }
        }
    }

    public void saveinterpolations() {
        for (int i = 0; i < InterpolationCount; i++) {
            IData gInt = gInterpolationData[i];
            gInt.bakpos = getValue(gInt);
        }
    }
}
