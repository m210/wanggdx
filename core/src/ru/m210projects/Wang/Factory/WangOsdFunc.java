package ru.m210projects.Wang.Factory;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Pattern.ScreenAdapters.InitScreen;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.DefaultOsdFunc;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Wang.Cheats.handleCheat;
import static ru.m210projects.Wang.Main.game;

public class WangOsdFunc extends DefaultOsdFunc {

    public WangOsdFunc() {
        super(game.getRenderer());

        BGTILE = 220;
        BGCTILE = 2366;
        BORDTILE = 0;

        BITSTH = 1 + 8 + 16;
        PALETTE = 4;

        OsdColor.WHITE.setPal(4);
        OsdColor.RED.setPal(14);
        OsdColor.BLUE.setPal(28);
        OsdColor.YELLOW.setPal(20);
        OsdColor.BROWN.setPal(21);
        OsdColor.GREY.setPal(26);
        OsdColor.GREEN.setPal(27);
    }

    protected Font getFont() {
        return game.getFont(1);
    }

    @Override
    public void showOsd(boolean cursorCatched) {
        super.showOsd(cursorCatched);

        if (!(game.getScreen() instanceof InitScreen) && !game.pMenu.gShowMenu) {
            Gdx.input.setCursorCatched(!cursorCatched);
        }
    }

    @Override
    public boolean textHandler(String message) {
        if (numplayers > 1) {
            return false;
        }
        return handleCheat(message);
    }
}
