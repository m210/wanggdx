package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Software.Software;
import ru.m210projects.Build.Render.Software.SoftwareOrpho;
import ru.m210projects.Build.Render.Types.ScreenFade;
import ru.m210projects.Build.settings.GameConfig;

import static ru.m210projects.Wang.Main.cfg;
import static ru.m210projects.Wang.Palette.WANG_SCREEN_FADE;

public class WangSoftware extends Software {

    public WangSoftware(GameConfig config) {
        super(config);
    }

    @Override
    protected SoftwareOrpho allocOrphoRenderer(Engine engine) {
        return new SoftwareOrpho(this, new WangMapSettings(engine.getBoardService()));
    }

    @Override
    public void showScreenFade(ScreenFade screenFade) {
        engine.setbrightness(cfg.getPaletteGamma(), WANG_SCREEN_FADE.getPalette());
    }
}
