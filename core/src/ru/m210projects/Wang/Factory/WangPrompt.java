package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.OsdCommandPrompt;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Cheats.handleCheat;
import static ru.m210projects.Wang.Game.MessageInputMode;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JPlayer.*;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Names.COINCURSOR;

public class WangPrompt extends OsdCommandPrompt implements OsdCommandPrompt.ActionListener {
    public WangPrompt() {
        super(512, 32);
        setActionListener(this);
    }

    @Override
    public void onEnter(String message) {
        setCaptureInput(false);
        MessageInputMode = false;

        if (game.net.TeamPlay) {
            for (short pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                if (pnum != myconnectindex) {
                    USER myUser = getUser(Player[myconnectindex].PlayerSprite);
                    USER pUser = getUser(Player[pnum].PlayerSprite);
                    if (myUser != null && pUser != null && myUser.spal == pUser.spal) {
                        game.net.SendMessage(pnum, message);
                    }
                }
            }
        } else {
            for (short pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                if (pnum != myconnectindex) {
                    game.net.SendMessage(pnum, message);
                }
            }
        }

        if (numplayers > 1 || !handleCheat(message)) {
            adduserquote(message);
            quotebot += 8;
            quotebotgoal = quotebot;
        }
    }

    public void draw() {
        final String text = getTextInput();
        final int cursorPos = getCursorPosition();
        final Font font = game.getFont(0);
        Renderer renderer = game.getRenderer();

        int pos = 160 - font.getWidth(text, 1.0f) / 2;

        int y = MESSAGE_LINE;
        int curX = pos;
        for (int i = 0; i < text.length(); i++) {
            pos += font.drawCharScaled(renderer, pos, y, text.charAt(i), 1.0f, 0, 4, Transparent.None, ConvertType.Normal, true);
            if (i == cursorPos - 1) {
                curX = pos;
            }
        }

        // Used to make cursor fade in and out
        int pal = isOsdOverType() ? 3 : 0;
        int stat = 4 - (EngineUtils.sin((engine.getTotalClock() << 4) & 2047) >> 11);
        renderer.rotatesprite((curX + 4) << 16, (y + 3) << 16, 20000, 0, COINCURSOR + ((engine.getTotalClock() >> 3) % 7), stat, pal, ROTATE_SPRITE_SCREEN_CLIP);
    }
}
