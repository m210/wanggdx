package ru.m210projects.Wang.Factory;

import org.jetbrains.annotations.Nullable;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Type.USER;

public class WangSprite extends Sprite {

    private USER user;

    @Nullable
    public USER getUser() {
        return user;
    }

    public void setUser(USER user) {
        this.user = user;
    }

    public void reset() {
        super.reset();
        this.user = null;
    }

}
