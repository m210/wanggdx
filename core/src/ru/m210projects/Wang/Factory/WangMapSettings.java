package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Render.DefaultMapSettings;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;

import static ru.m210projects.Wang.Draw.ScrollMode2D;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.screenpeek;
import static ru.m210projects.Wang.Main.gNet;
import static ru.m210projects.Wang.Palette.LT_GREY;
import static ru.m210projects.Wang.Palette.RED;

public class WangMapSettings extends DefaultMapSettings {

    public WangMapSettings(BoardService boardService) {
        super(boardService);
    }

    @Override
    public int getWallColor(int w, int s) {
        Wall wal = boardService.getWall(w);
        if (wal != null && boardService.isValidSector(wal.getNextsector())) // red wall
        {
            return RED + 6;
        }
        return LT_GREY + 2; // white wall
    }

    @Override
    public boolean isSpriteVisible(MapView view, int index) {
        return false;
    }

    @Override
    public int getPlayerSprite(int player) {
        return Player[player].PlayerSprite;
    }

    @Override
    public boolean isShowAllPlayers() {
        return gNet.MultiGameType != MultiGameTypes.MULTI_GAME_COMMBAT;
    }

    @Override
    public boolean isScrollMode() {
        return ScrollMode2D;
    }

    @Override
    public int getViewPlayer() {
        return screenpeek;
    }
}
