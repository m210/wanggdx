package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.FontHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.SliderDrawable;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Renderer.RenderType;
import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.OsdFunc;
import ru.m210projects.Wang.Fonts.GameFont;
import ru.m210projects.Wang.Fonts.MenuFont;
import ru.m210projects.Wang.Fonts.MiniFont;
import ru.m210projects.Wang.Main;

import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Wang.Main.gNet;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Names.LOADSCREEN;

public class WangFactory extends BuildFactory {

    private final Main app;

    public WangFactory(Main app) {
        super("sw.grp");
        this.app = app;
        OsdColor.DEFAULT.setPal(4);
    }

    @Override
    public Engine engine() throws Exception {
        return Main.engine = new WangEngine(app);
    }

    @Override
    public Renderer renderer(RenderType type) {
        if (type == RenderType.Software) {
            return new WangSoftware(app.pCfg);
        } else if (type == RenderType.PolyGDX) {
            return new WangPolyGDX(app.pCfg);
        } else {
            return new WangPolymost(app.pCfg);
        }
    }

    @Override
    public DefScript getBaseDef(Engine engine) {
        return new DefScript(engine);
    }

    @Override
    public OsdFunc getOsdFunc() {
        return new WangOsdFunc();
    }

    @Override
    public MenuHandler menus() {
        return new WangMenuHandler(app);
    }

    @Override
    public FontHandler fonts() {
        return new FontHandler(3) {
            @Override
            protected Font init(int i) {
                if (i == 0) {
                    return new MiniFont(app.pEngine);
                }
                if (i == 1) {
                    return new GameFont(app.pEngine);
                }
                return new MenuFont(app.pEngine);
            }
        };
    }

    @Override
    public BuildNet net() {
        return gNet = app.net = new WangNetwork(app);
    }

    @Override
    public SliderDrawable slider() {
        return new WangSliderDrawable();
    }

    @Override
    public void drawInitScreen() {
        Renderer renderer = game.getRenderer();
        renderer.clearview(117);
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        renderer.rotatesprite(xdim << 15, ydim << 15, divscale(ydim, 240, 16), 0, LOADSCREEN, 0, 0, 8, 0, 0, xdim - 1,
                ydim - 1);
    }
}
