package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Types.LittleEndian;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Menus.Network.MenuNetwork;
import ru.m210projects.Wang.Type.*;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.Build.Strhandler.toCharArray;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Digi.DIGI_PMESSAGE;
import static ru.m210projects.Wang.Factory.WangMenuHandler.NETWORKGAME;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JPlayer.adduserquote;
import static ru.m210projects.Wang.JPlayer.computergetinput;
import static ru.m210projects.Wang.JWeapon.InitBloodSpray;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.STAT_MISSILE;
import static ru.m210projects.Wang.Palette.*;
import static ru.m210projects.Wang.Player.DoPlayerSectorUpdatePostMove;
import static ru.m210projects.Wang.Player.DoPlayerSectorUpdatePreMove;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Type.MyTypes.BIT;
import static ru.m210projects.Wang.Type.SafeLoader.findAddon;

public class WangNetwork extends BuildNet {

    public static final short[] TimeLimitTable = {0, 3, 5, 10, 15, 20, 30, 45, 60};
    public final static short nNetVersion = 500;
    private final static int TypeOffset = 8;
    public static boolean PredictionOn = true;
    public static boolean Prediction = false;
    public static int PlayerSyncTrail = -1;
    public static int PlayerSyncIndex = -1;
    public static PlayerStr PlayerSync = null;
    public static Sprite PlayerSpriteSync = null;
    public static final byte[] netbuf = new byte[MAXPAKSIZ];
    public static int CommPlayers = 0;
    private static boolean SavePrediction;
    public final PlayerStr ppp = new PlayerStr();

    public final Predict[] predictFifo = new Predict[kNetFifoSize];
    public int MoveThingsCount;
    public boolean FakeMultiplayer;
    public int FakeMultiNumPlayers;
    public boolean BotMode = false;
    public int BotSkill = -1;
    public int KillLimit;
    public int TimeLimit;
    public int TimeLimitClock;
    public MultiGameTypes MultiGameType = MultiGameTypes.MULTI_GAME_NONE; // used to be a stand alone global
    public boolean TeamPlay;
    public boolean HurtTeammate;
    public boolean SpawnMarkers;
    public boolean NoRespawn; // for commbat type games
    public boolean Nuke = true;
    public final byte[] gContentFound = new byte[MAXPLAYERS];
    private final USER PredictUser = new USER();
    private final WangSprite PredictSprite = new WangSprite() {
        @Override
        public String toString() {
            String out = "Prediction sprite \r\n";
            out += super.toString();
            return out;
        }
    };
    private final Main app;

    public WangNetwork(Main game) {
        super(game);
        this.app = game;

        for (int i = 0; i < kNetFifoSize; i++) {
            predictFifo[i] = new Predict();
        }
    }

    public void PauseMultiPlay() {
        // check for pause of multi-play game
        for (short pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            PlayerStr pp = Player[pnum];

            if (TEST_SYNC_KEY(pp, SK_PAUSE)) {
                if (FLAG_KEY_PRESSED(pp, SK_PAUSE)) {
                    FLAG_KEY_RELEASE(pp, SK_PAUSE);

                    game.gPaused = !game.gPaused;

                    if (game.gPaused) {
                        SavePrediction = PredictionOn;
                        PredictionOn = false;
                    } else {
                        PredictionOn = SavePrediction;
                    }
                }
            } else {
                FLAG_KEY_RESET(pp, SK_PAUSE);
            }
        }
    }

    public void InitNetPlayerProfile() {
        // if you don't have a name :(
        if (cfg.getpName() == null) {
            cfg.setpName("PLAYER " + (myconnectindex + 1));
        }
        Player[myconnectindex].PlayerName = cfg.getpName();
        Player[myconnectindex].TeamColor = cfg.NetColor;
        if (cfg.AutoAim) {
            Player[myconnectindex].Flags |= (PF_AUTO_AIM);
        } else {
            Player[myconnectindex].Flags &= ~(PF_AUTO_AIM);
        }

        int l = PacketType.Profile.Send(netbuf);

        WaitForSend();
        sendtoall(netbuf, l);

        GetPackets();
    }

    public void InitNetPlayerOptions() {
        PlayerStr pp = Player[myconnectindex];

        USER pu = getUser(pp.PlayerSprite);
        Sprite psp = pp.getSprite();
        if (psp == null || pu == null) {
            return;
        }

        // myconnectindex palette
        pp.TeamColor = cfg.NetColor;
        psp.setPal( (PALETTE_PLAYER0 + pp.TeamColor));
        pu.spal = (byte) pp.getSprite().getPal();

        if (game.nNetMode != NetMode.Single) {
            InitNetPlayerProfile();
        }
    }

    public void SendMessage(int sendmessagecommand, String message) {
        PacketType.Message.setData(sendmessagecommand, message);
        int l = PacketType.Message.Send(netbuf);

        if (myconnectindex != connecthead) {
            sendpacket(connecthead, netbuf, l);
        } else {
            sendtoall(netbuf, l);
        }
    }

    public boolean MyCommPlayerQuit() {
        for (short i = connecthead; i != -1; i = connectpoint2[i]) {
            PlayerStr pp = Player[i];

            if (TEST_SYNC_KEY(pp, SK_QUIT_GAME)) {
                if (i == myconnectindex) {
                    QuitFlag = true;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public NetInput newInstance() {
        return new Input();
    }

    @Override
    public int GetPackets(byte[] p, int ptr, int len, final int nPlayer) {
        PacketType[] types = PacketType.values();
        int nPacket = p[0];
        if (nPacket == kPacketDisconnect) {
            return GetDisconnectPacket(p, 1, len, nPlayer, nDelete -> {
                if (rec != null) {
                    rec.close();
                }

                if (game.isCurrentScreen(gGameScreen)) {
                    PlayerStr qpp = Player[nDelete];
                    Sprite psp = qpp.getSprite();
                    if (psp != null) {
                        psp.setCstat(psp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
                        psp.setCstat(psp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN
                                | CSTAT_SPRITE_BLOCK_MISSILE));
                        InitBloodSpray(qpp.PlayerSprite, true, -2);
                        InitBloodSpray(qpp.PlayerSprite, false, -2);
                        psp.setAng(NORM_ANGLE(psp.getAng() + 1024));
                        InitBloodSpray(qpp.PlayerSprite, false, -1);
                        InitBloodSpray(qpp.PlayerSprite, true, -1);
                    }
                    qpp.input.bits |= (1 << SK_QUIT_GAME);
                    adduserquote(qpp.getName() + " has quit the game.");
                }

                // for COOP mode
                if (screenpeek == nDelete) {
                    screenpeek = connectpoint2[nDelete];
                    if (screenpeek < 0) {
                        screenpeek = connecthead;
                    }

                    ResetPalette(Player[screenpeek], FORCERESET);  // screenpeek
                    DoPlayerDivePalette(Player[screenpeek]);
                    DoPlayerNightVisionPalette(Player[screenpeek]);
                }
                CommPlayers--;
            });
        }
        if (nPacket >= TypeOffset) {
            return types[nPacket - TypeOffset].Get(nPlayer, p, len);
        }

        System.err.println("Unsupported packet " + nPacket);
        return 0;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean WaitForContentCheck(byte[] data, int timeout) {
        Arrays.fill(gContentFound, (byte) -1);
        if (numplayers < 2) {
            return true;
        }

        WaitForSend();

        PacketType.ContentRequest.setData(data);
        int l = PacketType.ContentRequest.Send(packbuf);
        sendtoall(packbuf, l);
        gContentFound[myconnectindex] = 1;

        long starttime = System.currentTimeMillis();
        while (true) {
            long time = System.currentTimeMillis() - starttime;
            if ((timeout != 0 && time > timeout)) {
                Console.out.println("Connection timed out!", OsdColor.YELLOW);
                return false;
            }

            GetPackets();

            int i;
            for (i = connecthead; i >= 0; i = connectpoint2[i]) {
                if (gContentFound[i] == -1) {
                    break;
                }
                if (myconnectindex != connecthead) {
                    i = -1;
                    break;
                } // slaves in M/S mode only wait for master
            }

            if (i < 0) {
                for (i = connecthead; i >= 0; i = connectpoint2[i]) {
                    if (gContentFound[i] != 1) {
                        return false;
                    }
                }
                return true;
            }
        }
    }

    @Override
    public void ComputerInput(int i) {
        if (BotMode) {
            computergetinput(i, (Input) gFifoInput[gNetFifoHead[i] & 0xFF][i]);
        }
    }

    @Override
    public void UpdatePrediction(NetInput input) {
        // routine called from MoveLoop
        if (!PredictionOn) {
            gPredictTail++;
            return;
        }

        ppp.input.Copy(input);

        // get rid of input bits so it doesn't go into other code branches that would
        // get it out of sync
        ppp.input.bits &= ~(BIT(SK_SHOOT) | BIT(SK_OPERATE) | BIT(SK_INV_LEFT) | BIT(SK_INV_RIGHT) | BIT(SK_INV_USE)
                | BIT(SK_HIDE_WEAPON) | BIT(SK_AUTO_AIM) | BIT(SK_CENTER_VIEW) | SK_WEAPON_MASK | SK_INV_HOTKEY_MASK);

        ppp.KeyPressFlags |= (BIT(SK_SHOOT) | BIT(SK_OPERATE) | BIT(SK_INV_LEFT) | BIT(SK_INV_RIGHT) | BIT(SK_INV_USE)
                | BIT(SK_HIDE_WEAPON) | BIT(SK_AUTO_AIM) | BIT(SK_CENTER_VIEW) | SK_WEAPON_MASK | SK_INV_HOTKEY_MASK);

        // back up things so they won't get stepped on
        List<Sprite> sprites = boardService.getBoard().getSprites();
        int bakrandomseed = engine.getrand();
        final USER u = getUser(ppp.PlayerSprite);
        final Sprite spr = ppp.getSprite();
        sprites.set(ppp.PlayerSprite, PredictSprite);
        setUser(ppp.PlayerSprite, PredictUser);
        PredictSprite.setCstat(0);

        ppp.oang = ppp.getAnglef();
        ppp.oposx = ppp.posx;
        ppp.oposy = ppp.posy;
        ppp.oposz = ppp.posz;
        ppp.ohoriz = ppp.getHorizf();
        ppp.obob_z = ppp.bob_z;

        // go through the player MOVEMENT code only
        Prediction = true;
        DoPlayerSectorUpdatePreMove(ppp);
        ppp.DoPlayerAction.invoke(ppp);
        DoPlayerSectorUpdatePostMove(ppp);
        Prediction = false;

        // restore things
        sprites.set(ppp.PlayerSprite, spr);
        setUser(ppp.PlayerSprite, u);


        engine.srand(bakrandomseed);

        predictFifo[gPredictTail & kFifoMask].set(ppp);
        gPredictTail++;
    }

    public void InitPrediction(PlayerStr pp) {
        if (!PredictionOn) {
            return;
        }

        USER pu = getUser(pp.PlayerSprite);
        Sprite psp = pp.getSprite();
        if (psp == null || pu == null) {
            return;
        }

        // make a copy of player struct and sprite
        ppp.copy(pp);
        PredictUser.copy(pu);
        PredictSprite.set(psp);
    }

    @Override
    public void CorrectPrediction() {
        if (numplayers < 2 || !PredictionOn) {
            return;
        }

        Predict predict = predictFifo[(gNetFifoTail - 1) & kFifoMask];
        PlayerStr p = Player[myconnectindex];

        if (predict.ang == p.getAnglef() && predict.horiz == p.getHorizf() && predict.x == p.posx && predict.y == p.posy
                && predict.z == p.posz) {
            return;
        }

        InitPrediction(p);
        gPredictTail = gNetFifoTail;
        while (gPredictTail < gNetFifoHead[myconnectindex]) {
            UpdatePrediction(gFifoInput[gPredictTail & kFifoMask][myconnectindex]);
        }
    }

    private long PlayerSync(PlayerStr pp) {
        USER pu = getUser(pp.PlayerSprite);
        if (pu == null) {
            return -1;
        }

        long crc = pp.posx;
        crc += pp.posy;
        crc += pp.posz;
        crc += pp.getAnglei();
        crc += pp.getHorizi();
        crc += pu.Health;
        crc += pp.bcnt;

        return crc;
    }

    private long MissileSync() {
        long crc = 0;

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_MISSILE); node != null; node = node.getNext()) {
            Sprite spr = node.get();
            crc += spr.getX();
            crc += spr.getY();
            crc += spr.getZ();
            crc += spr.getAng();
        }

        return crc;
    }

    @Override
    public void CalcChecksum() {
        if ((numplayers >= 2 || gNet.FakeMultiplayer) && ((gNetFifoTail & 7) == 7)) // build sync variables
        {
            Arrays.fill(gChecksum, 0);
            gChecksum[0] = engine.getrand();
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                PlayerStr pp = Player[i];
                Sprite psp = pp.getSprite();

                gChecksum[1] ^= (int) PlayerSync(pp); // Checksum(pp.getBytes(), PlayerStr.sizeof);
                if (psp != null) {
                    gChecksum[2] ^= checksum(psp);
                }
            }
            gChecksum[3] ^= (int) MissileSync();
            for (int i = 0; i < gChecksum.length; i++) {
                LittleEndian.putInt(gCheckFifo[myconnectindex],
                        CheckSize * (gCheckHead[myconnectindex] & kFifoMask) + 4 * i, gChecksum[i]);
            }
            gCheckHead[myconnectindex]++;
        }
    }

    @Override
    public void CheckSync() {
        if (PlayerSync != null || PlayerSpriteSync != null) {
            if (gNetFifoTail < PlayerSyncTrail) {
                return;
            }

            Console.out.println("gNetFifoTail: " + gNetFifoTail + ", PlayerSyncTrail: " + PlayerSyncTrail);
            if (PlayerSync != null) {
                PlayerSync.compare(Player[PlayerSync.pnum]);
            }
            if (PlayerSpriteSync != null) {
                Sprite psp = boardService.getSprite(PlayerSyncIndex);
                if (psp != null) {
                    engine.compare(PlayerSpriteSync, psp);
                }
            }

            PlayerSyncTrail = -1;
            PlayerSyncIndex = -1;
            PlayerSync = null;
            PlayerSpriteSync = null;
        }

//		if (AUTOCOMPARE && MAXPAKSIZ >= PlayerStr.sizeof) {
//			if (numplayers == 1)
//				return;
//
//			while (true) {
//				for (int nPlayer = connecthead; nPlayer >= 0; nPlayer = connectpoint2[nPlayer]) {
//					if (gCheckHead[nPlayer] <= gCheckTail)
//						return;
//				}
//
//				bOutOfSync = false;
//				for (int nPlayer = connectpoint2[connecthead]; nPlayer >= 0; nPlayer = connectpoint2[nPlayer]) {
//					for (int i = 0; i < CheckSize; i++) {
//						if (gCheckFifo[nPlayer][(CheckSize * (gCheckTail & kFifoMask))
//								+ i] != gCheckFifo[connecthead][(CheckSize * (gCheckTail & kFifoMask)) + i]) {
//							bOutOfSyncByte = i;
//							bOutOfSync = true;
//
//							if (i == 4) {
//								PacketType.PlayerData.setData(nPlayer);
//								int l = PacketType.PlayerData.Send(netbuf);
//								sendtoall(netbuf, l);
//							}
//						}
//					}
//				}
//				gCheckTail++;
//			}
//		} else
        super.CheckSync();
    }

    @Override
    public void NetDisconnect(int nPlayer) {
        super.NetDisconnect(nPlayer);
        app.Disconnect();
    }

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, KillLimit);
        StreamUtils.writeInt(os, TimeLimit);
        StreamUtils.writeInt(os, TimeLimitClock);

        StreamUtils.writeByte(os, MultiGameType != null ? MultiGameType.ordinal() : -1);

        StreamUtils.writeBoolean(os, TeamPlay);
        StreamUtils.writeBoolean(os, HurtTeammate);
        StreamUtils.writeBoolean(os, SpawnMarkers);
        StreamUtils.writeBoolean(os, NoRespawn);
        StreamUtils.writeBoolean(os, Nuke);
    }

    @SuppressWarnings("unused")
    public void load(InputStream is) throws IOException {
        KillLimit = StreamUtils.readInt(is);
        TimeLimit = StreamUtils.readInt(is);
        TimeLimitClock = StreamUtils.readInt(is);

        int i = StreamUtils.readByte(is);
        MultiGameType = i != -1 ? MultiGameTypes.values()[i] : null;

        TeamPlay = StreamUtils.readBoolean(is);
        HurtTeammate = StreamUtils.readBoolean(is);
        SpawnMarkers = StreamUtils.readBoolean(is);
        NoRespawn = StreamUtils.readBoolean(is);
        Nuke = StreamUtils.readBoolean(is);
    }

    public enum MultiGameTypes {
        MULTI_GAME_NONE, MULTI_GAME_COMMBAT, MULTI_GAME_COMMBAT_NO_RESPAWN, // JUST a place holder for menus. DO NOT
        // USE!!!
        MULTI_GAME_COOPERATIVE, MULTI_GAME_AI_BOTS
    }

    public enum PacketType {
        LevelStart {
            @Override
            public int Get(int fromPlayer, byte[] buf, int len) {
                gNet.retransmit(fromPlayer, buf, len);

                try(ByteArrayInputStream is = new ByteArrayInputStream(buf, 1, len)) {
                    int nCheckVersion = StreamUtils.readShort(is);
                    pNetInfo.readObject(is);

                    if (nCheckVersion != nNetVersion) {
                        gNet.NetDisconnect(myconnectindex);
                        throw new AssertException("These versions of Shadow Warrior cannot play together.");
                    }

                    if (gNet.WaitForAllPlayers(0)) {
                        gGameScreen.newgame(true, ((MenuNetwork) Main.game.menu.mMenus[NETWORKGAME]).getFile(),
                                pNetInfo.nEpisode, pNetInfo.nLevel, pNetInfo.nDifficulty);
                    }

                } catch (Exception e) {
                    throw new AssertException(e.toString());
                }
                return 0;
            }

            @Override
            public int Send(byte[] buf) {
                try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                    StreamUtils.writeByte(os, ordinal() + TypeOffset);
                    StreamUtils.writeShort(os, nNetVersion);
                    pNetInfo.writeObject(os);

                    System.arraycopy(os.toByteArray(), 0, buf, 0, os.size());
                    return os.size();
                } catch (Exception e) {
                    Console.out.println(e.toString(), OsdColor.RED);
                }
                return 0;
            }
        },
        Profile {
            @Override
            public int Get(int fromPlayer, byte[] buf, int len) {
                gNet.retransmit(fromPlayer, buf, len);
                int nP = buf[1];
                boolean AutoRun = buf[2] == 1;
                byte NetColor = buf[3];
                boolean AutoAim = buf[4] == 1;

                len = 0;
                for (int i = 5; buf[i] != 0; i++) {
                    len++;
                }
                PlayerStr pp = Player[nP];

                if (AutoRun) {
                    pp.Flags |= (PF_LOCK_RUN);
                } else {
                    pp.Flags &= ~(PF_LOCK_RUN);
                }

                if (AutoAim) {
                    pp.Flags |= (PF_AUTO_AIM);
                } else {
                    pp.Flags &= ~(PF_AUTO_AIM);
                }

                pp.PlayerName = new String(buf, 5, len);
                pp.TeamColor = NetColor;

                if (pp.PlayerSprite != -1 && getUser(pp.PlayerSprite) != null) { // if in the game
                    USER pu = getUser(pp.PlayerSprite);
                    Sprite psp = pp.getSprite();
                    if (psp != null && pu != null) {
                        psp.setPal((PALETTE_PLAYER0 + pp.TeamColor));
                        pu.spal = (byte) pp.getSprite().getPal();
                    }
                }
                return 0;
            }

            @Override
            public int Send(byte[] buf) {
                buf[0] = (byte) (ordinal() + TypeOffset);
                buf[1] = (byte) myconnectindex;
                buf[2] = cfg.AutoRun ? (byte) 1 : 0;
                buf[3] = cfg.NetColor;
                buf[4] = cfg.AutoAim ? (byte) 1 : 0;
                int l = 5;

                char[] name = toCharArray(cfg.getpName());
                for (int i = 0; i < cfg.getpName().length() && name[i] != 0; i++) {
                    buf[l++] = (byte) name[i];
                }
                buf[l++] = 0;
                return l;
            }
        },
        Message {
            private int sendmessagecommand;
            private String message;

            public void setData(Object... opts) {
                if (opts.length < 2) {
                    return;
                }
                sendmessagecommand = (int) opts[0];
                message = (String) opts[1];
            }

            @Override
            public int Get(int fromMaster, byte[] buf, int len) {
                byte fromPlayer = buf[1];
                byte nPlayer = buf[2];
                gNet.retransmit(fromMaster, buf, len);
                if (nPlayer == -1 || nPlayer == myconnectindex) {
                    if (nPlayer != -1) {
                        adduserquote(Player[fromPlayer].getName() + ": " + new String(buf, 3, len - 3));
                    } else {
                        adduserquote(new String(buf, 3, len - 3));
                    }
                    PlaySound(DIGI_PMESSAGE, null, v3df_dontpan);
                }
                return 0;
            }

            @Override
            public int Send(byte[] buf) {
                ByteBuffer bb = ByteBuffer.wrap(buf).order(ByteOrder.LITTLE_ENDIAN);
                bb.put((byte) (ordinal() + TypeOffset));
                bb.put((byte) myconnectindex);
                bb.put((byte) sendmessagecommand);
                bb.put(message.getBytes());
                bb.put((byte) 0);
                return bb.position();
            }
        },
        RTS_Sound {
            int num = 0;

            public void setData(Object... opts) {
                this.num = (int) opts[0];
            }

            @Override
            public int Get(int fromPlayer, byte[] buf, int len) {
                gNet.retransmit(fromPlayer, buf, len);
                PlaySoundRTS(buf[1]);
                return 0;
            }

            @Override
            public int Send(byte[] buf) {
                PlaySoundRTS(num);

                buf[0] = (byte) (ordinal() + TypeOffset);
                buf[1] = (byte) num;
                return 2;
            }
        },
        PlayerData { // Attention! Mmulti.java MAXPAKSIZ should be 2048 to make it works
            int num = 0;

            public void setData(Object... opts) {
                this.num = (int) opts[0];
            }

            @Override
            public int Get(int fromPlayer, byte[] buf, int len) {
                gNet.retransmit(fromPlayer, buf, len);
                try (InputStream is = new ByteArrayInputStream(buf)) {
                    StreamUtils.readByte(is); // pack
                    PlayerSyncTrail = StreamUtils.readInt(is);
                    PlayerSyncIndex = StreamUtils.readInt(is);

                    PlayerSpriteSync = new WangSprite();
                    PlayerSpriteSync.readObject(is);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                return 0;
            }

            @Override
            public int Send(byte[] buf) {
                try(ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                    StreamUtils.writeByte(os, (ordinal() + TypeOffset));
                    int trail = gNet.gNetFifoTail;
                    if (myconnectindex == connecthead) {
                        trail += 1;
                    }
                    StreamUtils.writeInt(os, trail);
                    Sprite psp = Player[num].getSprite();
                    StreamUtils.writeInt(os, psp != null ? Player[num].PlayerSprite : -1);
                    if (psp != null) {
                        psp.writeObject(os);
                    }
                    System.arraycopy(os.toByteArray(), 0, buf, 0, os.size());
                    return os.size();
                } catch (Exception e) {
                    return 0;
                }
            }
        },
        ContentRequest {
            private byte[] data;

            public void setData(Object... opts) {
                data = (byte[]) opts[0];
            }

            @Override
            public int Get(int fromPlayer, byte[] p, int len) {
                gNet.retransmit(fromPlayer, p, len);

                byte found = 0;
                try (ByteArrayInputStream is = new ByteArrayInputStream(p, 5, len)) {
                    boolean isAddon = StreamUtils.readBoolean(is);
                    String path = StreamUtils.readDataString(is);
                    long crc32 = StreamUtils.readLong(is);

                    FileEntry fil = DUMMY_ENTRY;
                    GameInfo ini = null;
                    if (isAddon) {
                        ini = findAddon(path);
                    } else {
                        fil = Main.game.getCache().getGameDirectory().getEntry(FileUtils.getPath(path));
                    }

                    if (fil.exists() || ini != null) {
                        MenuNetwork network = (MenuNetwork) Main.game.menu.mMenus[NETWORKGAME];
                        if (ini != null) {
                            long mycrc = ini.getEpisodeEntry().getFileEntry().getChecksum();
                            if (mycrc == crc32) {
                                found = 1;
                                network.setEpisode(ini);
                            } else {
                                found = 2;
                                Console.out.println("Player" + fromPlayer + " - " + Player[fromPlayer].getName()
                                        + " tried to set user content. User content found, but has a different checksum!", OsdColor.RED);
                                Console.out.println("Make sure that you have the same content: " + File.separator + path, OsdColor.RED);
                                if (!Console.out.isShowing()) {
                                    Console.out.onToggle();
                                }
                            }
                        } else if (fil.exists() && fil.isExtension("map")) {
                            long mycrc = fil.getChecksum();
                            if (mycrc == crc32) {
                                found = 1;
                                network.setMap(fil);
                            } else {
                                found = 2;
                                Console.out.println("Player" + fromPlayer + " - " + Player[fromPlayer].getName()
                                        + " tried to set user content. User content found, but has a different checksum!", OsdColor.RED);
                                Console.out.println("Make sure that you have the same content: " + File.separator + path, OsdColor.RED);
                                if (!Console.out.isShowing()) {
                                    Console.out.onToggle();
                                }
                            }
                        }
                    } else {
                        Console.out.println("Player" + fromPlayer + " - " + Player[fromPlayer].getName()
                                + " tried to set user content. User content not found!", OsdColor.RED);
                        Console.out.println("Make sure that you have content at the same path: " + File.separator + path, OsdColor.RED);
                        if (!Console.out.isShowing()) {
                            Console.out.onToggle();
                        }
                    }
                } catch (Exception e) {
                    Console.out.println(e.toString(), OsdColor.RED);
                }

                ContentAnswer.setData(found);
                int l = ContentAnswer.Send(netbuf);
                sendpacket(fromPlayer, netbuf, l);
                return 0;
            }

            @Override
            public int Send(byte[] buf) {
                ByteBuffer bb = ByteBuffer.wrap(buf).order(ByteOrder.LITTLE_ENDIAN);
                bb.put((byte) (ordinal() + TypeOffset));
                int len = Math.min(data.length, 246); // 255 - 1 - 4 - 4
                bb.putInt(len);
                bb.put(data);

                return bb.position();
            }
        },
        ContentAnswer {
            private byte found;

            public void setData(Object... opts) {
                found = (byte) opts[0];
            }

            @Override
            public int Get(int fromPlayer, byte[] p, int len) {
                gNet.retransmit(fromPlayer, p, len);
                gNet.gContentFound[fromPlayer] = p[1];
                return 0;
            }

            @Override
            public int Send(byte[] buf) {
                ByteBuffer bb = ByteBuffer.wrap(buf).order(ByteOrder.LITTLE_ENDIAN);
                bb.put((byte) (ordinal() + TypeOffset));
                bb.put(found);
                return bb.position();
            }
        };

        public abstract int Get(int fromPlayer, byte[] buf, int len);

        @SuppressWarnings("unused")
        public abstract int Send(byte[] buf);

        @SuppressWarnings("unused")
        public void setData(Object... opts) {
            /* nothing */
        }
    }
}
