package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.LegacyTimer;
import ru.m210projects.Build.Timer;
import ru.m210projects.Wang.Main;

import static ru.m210projects.Wang.Game.nInterpolation;

public class WangTimer extends Timer {

    public WangTimer(int tickspersecond, int frameTicks) {
        super(tickspersecond, frameTicks);
    }

    @Override
    public boolean update(float delta, boolean vsync) {
        if (super.update(delta, vsync)) {
            Main.game.pIntSkip2.clearinterpolations();
            Main.game.pIntSkip4.clearinterpolations();
            return true;
        }

        return false;
    }

    public int getsmoothratio(float deltaTime) {
        if (nInterpolation == 0 || nInterpolation == 3) {
            return 65536;
        }

//        return super.getsmoothratio(deltaTime);
        return (int) (frametime += ((deltaTime * 1000.0f) / 32) * 65536.0f);
    }

    public static class Legacy extends LegacyTimer {

        public Legacy(int tickspersecond, int frameTicks) {
            super(tickspersecond, frameTicks);
        }

        @Override
        public boolean update(float delta, boolean vsync) {
            if (super.update(delta, vsync)) {
                Main.game.pIntSkip2.clearinterpolations();
                Main.game.pIntSkip4.clearinterpolations();
                return true;
            }

            return false;
        }

        public int getsmoothratio(float deltaTime) {
            if (nInterpolation == 0 || nInterpolation == 3) {
                return 65536;
            }

            return super.getsmoothratio(deltaTime);
        }
    }
}
