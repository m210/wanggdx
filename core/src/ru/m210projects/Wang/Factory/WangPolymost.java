package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Polymost.Polymost;
import ru.m210projects.Build.Render.Polymost.Polymost2D;
import ru.m210projects.Build.settings.GameConfig;

import static ru.m210projects.Wang.Palette.PALETTE_DIVE;

public class WangPolymost extends Polymost {

    public WangPolymost(GameConfig config) {
        super(config);
    }

    @Override
    protected Polymost2D allocOrphoRenderer(Engine engine) {
        return new Polymost2D(this, new WangMapSettings(engine.getBoardService()));
    }

    @Override
    protected void calc_and_apply_fog(int shade, int vis, int pal) {
        if (globalpal == PALETTE_DIVE) {
            pal = globalpal;
        }
        ((WangPaletteManager) engine.getPaletteManager()).updateUnderwaterGLFog(pal);
        super.calc_and_apply_fog(shade, vis, pal);
    }
}
