package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.net.Mmulti;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Wang.Game;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.USER;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.MAX_WEAPONS;
import static ru.m210projects.Wang.Gameutils.getUser;
import static ru.m210projects.Wang.Inv.MAX_INVENTORY;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Type.DemoFile.header;

public class DemoRecorder {

    private final ByteArrayOutputStream baos;
    private final FileOutputStream os;
    private final Path filepath;
    public int reccnt;
    public int totalreccnt;
    public int recversion;

    public DemoRecorder(FileOutputStream os, Path fn, int ver, String map) throws IOException {
        this.filepath = fn;
        Console.out.println("Start recording to " + fn);
       
        if (ver != 1) {
            StreamUtils.writeString(os, header);
            StreamUtils.writeInt(os, ver);
            StreamUtils.writeInt(os, 0);

            byte warp_on = 0;
            if (mUserFlag == UserFlag.Addon) {
                warp_on = (byte) ((currentGame != null) ? 1 : 2);
            }

            if (mUserFlag == UserFlag.UserMap) {
                warp_on = 2;
            }
            StreamUtils.writeByte(os, warp_on);

            if (warp_on == 1) {
                String path = currentGame.getPath();
                StreamUtils.writeDataString(os, path);
            }

            StreamUtils.writeDataString(os, map);
            StreamUtils.writeByte(os, Mmulti.numplayers);
            int level = Game.Level;
            int episode;
            if (level < 5) {
                episode = 0;
            } else if (level < 23) {
                episode = 1;
                level -= 4;
            } else {
                episode = 2;
                level -= 22;
            }

            StreamUtils.writeByte(os, episode);
            StreamUtils.writeByte(os, level);
            for (int p = 0; p < Mmulti.numplayers; p++) {
                USER u = getUser(Player[p].PlayerSprite);
                StreamUtils.writeInt(os, Player[p].Flags);

                StreamUtils.writeInt(os, Player[p].Armor);
                StreamUtils.writeInt(os, u != null ? u.Health : 100);
                StreamUtils.writeInt(os, Player[p].MaxHealth);
                StreamUtils.writeInt(os, Player[p].WpnFlags);
                StreamUtils.writeByte(os, Player[p].WpnRocketHeat);
                StreamUtils.writeByte(os, Player[p].WpnRocketNuke);
                for (int i = 0; i < MAX_WEAPONS; i++) {
                    StreamUtils.writeInt(os, Player[p].WpnAmmo[i]);
                }

                for (int i = 0; i < MAX_INVENTORY; i++) {
                    StreamUtils.writeInt(os, Player[p].InventoryPercent[i]);
                    StreamUtils.writeInt(os, Player[p].InventoryAmount[i]);
                }
                StreamUtils.writeInt(os, Player[p].InventoryNum);
            }
            StreamUtils.writeShort(os, Game.Skill);

            StreamUtils.writeInt(os, gNet.KillLimit);
            StreamUtils.writeInt(os, gNet.TimeLimit);
            StreamUtils.writeInt(os, gNet.TimeLimitClock);
            StreamUtils.writeShort(os, gNet.MultiGameType != null ? gNet.MultiGameType.ordinal() : -1);
            StreamUtils.writeByte(os, gNet.TeamPlay ? 1 : 0);
            StreamUtils.writeByte(os, gNet.HurtTeammate ? 1 : 0);
            StreamUtils.writeByte(os, gNet.SpawnMarkers ? 1 : 0);
            StreamUtils.writeByte(os, cfg.AutoAim ? 1 : 0);
            StreamUtils.writeByte(os, gNet.NoRespawn ? 1 : 0);
            StreamUtils.writeByte(os, gNet.Nuke ? 1 : 0);
        }

        recversion = ver;
        totalreccnt = 0;
        reccnt = 0;

        this.os = os;
        this.baos = new ByteArrayOutputStream(RECSYNCBUFSIZ);
    }

    public void record() {
        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            try {
                PlayerStr pp = Player[i];
                pp.input.writeObject(baos, recversion);

                reccnt++;
                totalreccnt++;

                if (reccnt >= RECSYNCBUFSIZ) {
                    os.write(baos.toByteArray());
                    baos.reset();
                    reccnt = 0;
                }
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
                close();
            }
        }
    }

    public void close() {
        if (DemoRecording) {
            DemoRecording = false;

            try {
                if (baos.size() != 0) {
                    os.write(baos.toByteArray());
                }
                StreamUtils.seek(os, 9);
                StreamUtils.writeInt(os, totalreccnt);
                os.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            Console.out.println("Stop recording");
            Directory dir = game.getCache().getGameDirectory();
            dir.revalidate();

            Entry entry = dir.getEntry(dir.getPath().relativize(filepath));
            if (entry.exists()) {
                List<Entry> demos = gDemoScreen.demofiles.computeIfAbsent(dir, e -> new ArrayList<>());
                demos.add(entry);
            }
        }
    }
}
