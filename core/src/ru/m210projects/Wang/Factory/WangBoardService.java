package ru.m210projects.Wang.Factory;

import org.jetbrains.annotations.Nullable;
import ru.m210projects.Build.Board;
import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.SpriteMap;
import ru.m210projects.Build.Types.collections.ValueSetter;

import java.util.List;

import static ru.m210projects.Build.Engine.*;

public class WangBoardService extends BoardService {

    public WangBoardService() {
        registerBoard(7, Sector.class, Wall.class, WangSprite.class);
        registerBoard(8, Sector.class, Wall.class, WangSprite.class);
    }

    protected void initSpriteLists(Board board) {
        List<Sprite> sprites = board.getSprites();
        this.spriteStatMap = createSpriteMap(MAXSTATUS, sprites, MAXSPRITES, Sprite::setStatnum);
        this.spriteSectMap = createSpriteMap(MAXSECTORS, sprites, MAXSPRITES, Sprite::setSectnum);
    }

    protected SpriteMap createSpriteMap(int listCount, List<Sprite> spriteList, int spriteCount, ValueSetter<Sprite> valueSetter) {
        return new SpriteMap(listCount, spriteList, spriteCount, valueSetter) {
            @Override
            protected WangSprite getInstance() {
                WangSprite spr = new WangSprite();
                spr.setStatnum(MAXSTATUS);
                spr.setSectnum(MAXSECTORS);
                return spr;
            }
        };
    }

    @Override
    public @Nullable WangSprite getSprite(int index) {
        return (WangSprite) super.getSprite(index);
    }
}
