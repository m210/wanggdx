package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Types.Color;
import ru.m210projects.Build.Types.DefaultFastColorLookup;
import ru.m210projects.Build.Types.DefaultPaletteLoader;
import ru.m210projects.Build.Types.DefaultPaletteManager;
import ru.m210projects.Build.Types.FastColorLookup;

import java.io.IOException;

import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.screenpeek;
import static ru.m210projects.Wang.Gameutils.SectorIsUnderwaterArea;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Palette.*;

public class WangPaletteManager extends DefaultPaletteManager {

    public static byte[] tempbuf = new byte[256];
    public static final COLOR_MAP BrownRipper = new COLOR_MAP(31, 32, LT_GREY, LT_TAN);
    public static final COLOR_MAP SkelGore = new COLOR_MAP(16, 16, RED, BRIGHT_GREEN);
    public static final COLOR_MAP ElectroGore = new COLOR_MAP(16, 16, RED, DK_BLUE);
    public static final COLOR_MAP MenuHighlight = new COLOR_MAP(16, 16, RED, FIRE);
    public static final COLOR_MAP[][] PlayerColorMap = {{new COLOR_MAP(32, 32, LT_BLUE, LT_BROWN),}, {new COLOR_MAP(32, 31, LT_BLUE, LT_GREY),}, {new COLOR_MAP(32, 16, LT_BLUE, PURPLE),}, {new COLOR_MAP(32, 16, LT_BLUE, RUST_RED),}, {new COLOR_MAP(32, 16, LT_BLUE, YELLOW),}, {new COLOR_MAP(32, 16, LT_BLUE, DK_GREEN),}, {new COLOR_MAP(32, 16, LT_BLUE, GREEN),}, {new COLOR_MAP(32, 32, LT_BLUE, LT_BLUE), // Redundant, but has to be here for position
    }, {new COLOR_MAP(32, 32, LT_BLUE, LT_TAN),}, {new COLOR_MAP(32, 16, LT_BLUE, RED),}, {new COLOR_MAP(32, 16, LT_BLUE, DK_GREY),}, {new COLOR_MAP(32, 16, LT_BLUE, BRIGHT_GREEN),}, {new COLOR_MAP(32, 16, LT_BLUE, DK_BLUE),}, {new COLOR_MAP(32, 16, LT_BLUE, FIRE),}, {new COLOR_MAP(32, 16, LT_BLUE, FIRE),}};
    public static final COLOR_MAP[] AllToRed = {new COLOR_MAP(31, 16, LT_GREY, RED), new COLOR_MAP(32, 16, LT_BROWN, RED), new COLOR_MAP(32, 16, LT_TAN, RED), new COLOR_MAP(16, 16, RUST_RED, RED), new COLOR_MAP(16, 16, YELLOW, RED), new COLOR_MAP(16, 16, BRIGHT_GREEN, RED), new COLOR_MAP(16, 16, DK_GREEN, RED), new COLOR_MAP(16, 16, GREEN, RED), new COLOR_MAP(32, 16, LT_BLUE, RED), new COLOR_MAP(16, 16, PURPLE, RED), new COLOR_MAP(16, 16, FIRE, RED)};
    public static final COLOR_MAP[] AllToBlue = {new COLOR_MAP(31, 32, LT_GREY, LT_BLUE), new COLOR_MAP(32, 32, LT_BROWN, LT_BLUE), new COLOR_MAP(32, 32, LT_TAN, LT_BLUE), new COLOR_MAP(16, 32, RUST_RED, LT_BLUE), new COLOR_MAP(16, 32, YELLOW, LT_BLUE), new COLOR_MAP(16, 32, BRIGHT_GREEN, LT_BLUE), new COLOR_MAP(16, 32, DK_GREEN, LT_BLUE), new COLOR_MAP(16, 32, GREEN, LT_BLUE), new COLOR_MAP(16, 32, RED, LT_BLUE), new COLOR_MAP(16, 32, PURPLE, LT_BLUE), new COLOR_MAP(16, 32, FIRE, LT_BLUE)};
    public static final COLOR_MAP[] AllToGreen = {new COLOR_MAP(31, 16, LT_GREY, GREEN), new COLOR_MAP(32, 16, LT_BROWN, GREEN), new COLOR_MAP(32, 16, LT_TAN, GREEN), new COLOR_MAP(16, 16, RUST_RED, GREEN), new COLOR_MAP(16, 16, YELLOW, GREEN), new COLOR_MAP(16, 16, BRIGHT_GREEN, GREEN), new COLOR_MAP(16, 16, DK_GREEN, GREEN), new COLOR_MAP(16, 16, GREEN, GREEN), new COLOR_MAP(32, 16, LT_BLUE, GREEN), new COLOR_MAP(16, 16, RED, GREEN), new COLOR_MAP(16, 16, PURPLE, GREEN), new COLOR_MAP(16, 16, FIRE, GREEN)};
    public static final COLOR_MAP[] NinjaBasic = {new COLOR_MAP(32, 16, LT_TAN, DK_GREY), new COLOR_MAP(32, 16, LT_BROWN, DK_GREY), new COLOR_MAP(32, 31, LT_BLUE, LT_GREY), new COLOR_MAP(16, 16, DK_GREEN, DK_GREY), new COLOR_MAP(16, 16, GREEN, DK_GREY), new COLOR_MAP(16, 16, YELLOW, DK_GREY)};
    public static final COLOR_MAP[] NinjaRed = {new COLOR_MAP(16, 16, DK_TAN, DK_GREY), new COLOR_MAP(16, 16, GREEN, DK_TAN), new COLOR_MAP(16, 8, DK_BROWN, RED + 8), new COLOR_MAP(32, 16, LT_BLUE, RED)};
    public static final COLOR_MAP[] NinjaGreen = {new COLOR_MAP(16, 16, DK_TAN, DK_GREY), new COLOR_MAP(16, 16, GREEN, DK_TAN), new COLOR_MAP(16, 8, DK_BROWN, GREEN + 6),

            new COLOR_MAP(32, 16, LT_BLUE, GREEN)};
    public static final COLOR_MAP[] Illuminate = {new COLOR_MAP(16, 8, LT_GREY, BRIGHT_GREEN), new COLOR_MAP(16, 8, DK_GREY, BRIGHT_GREEN), new COLOR_MAP(16, 8, LT_BROWN, BRIGHT_GREEN), new COLOR_MAP(16, 8, DK_BROWN, BRIGHT_GREEN), new COLOR_MAP(16, 8, LT_TAN, BRIGHT_GREEN), new COLOR_MAP(16, 8, DK_TAN, BRIGHT_GREEN), new COLOR_MAP(16, 8, RUST_RED, BRIGHT_GREEN), new COLOR_MAP(16, 8, YELLOW, BRIGHT_GREEN), new COLOR_MAP(16, 8, DK_GREEN, BRIGHT_GREEN), new COLOR_MAP(16, 8, GREEN, BRIGHT_GREEN), new COLOR_MAP(32, 8, LT_BLUE, BRIGHT_GREEN), new COLOR_MAP(16, 8, RED, BRIGHT_GREEN), new COLOR_MAP(16, 8, PURPLE, BRIGHT_GREEN), new COLOR_MAP(16, 8, FIRE, BRIGHT_GREEN)};

    public static final Color WATER_FOG_COLOR = new Color(0, 0, 15, 0);

    public WangPaletteManager(Engine engine) throws IOException {
        super(engine, new DefaultPaletteLoader(game.getCache().getEntry("palette.dat", true)) {
            @Override
            public FastColorLookup getFastColorLookup() {
                return new DefaultFastColorLookup(getBasePalette(), 1, 1, 1);
            }
        });
        initPalette();
    }

    public void updateUnderwaterGLFog(int pal) {
        if (SectorIsUnderwaterArea(Player[screenpeek].cursectnum) && pal == PALETTE_DIVE) {
            palookupfog[0] = WATER_FOG_COLOR;
        } else {
            palookupfog[0] = DEFAULT_COLOR;
        }
    }

    public void initPalette() {
        //
        // Dive palettes
        //

        tempbuf = new byte[256];
        for (int i = 0; i < 256; i++) {
            tempbuf[i] = (byte) i;
        }

        // palette for underwater
        makePalookup(PALETTE_DIVE, tempbuf, 0, 0, 15, 1);

        int FOG_AMT = 15;
        makePalookup(PALETTE_FOG, tempbuf, FOG_AMT, FOG_AMT, FOG_AMT, 1);

        makePalookup(PALETTE_DIVE_LAVA, tempbuf, 11, 0, 0, 1);

        //
        // 1 Range changes
        //

        MapColors(PALETTE_BROWN_RIPPER, BrownRipper, 1);
        makePalookup(PALETTE_BROWN_RIPPER, tempbuf, 0, 0, 0, 1);

        MapColors(PALETTE_SKEL_GORE, SkelGore, 1);
        makePalookup(PALETTE_SKEL_GORE, tempbuf, 0, 0, 0, 1);

        MapColors(PALETTE_ELECTRO_GORE, ElectroGore, 1);
        makePalookup(PALETTE_ELECTRO_GORE, tempbuf, 0, 0, 0, 1);

        MapColors(PALETTE_MENU_HIGHLIGHT, MenuHighlight, 1);
        makePalookup(PALETTE_MENU_HIGHLIGHT, tempbuf, 0, 0, 0, 1);

        //
        // Multiple range changes
        //

        MapColors(PALETTE_BASIC_NINJA, NinjaBasic[0], 1);
        for (int i = 1; i < NinjaBasic.length; i++) {
            MapColors(PALETTE_BASIC_NINJA, NinjaBasic[i], 0);
        }
        makePalookup(PALETTE_BASIC_NINJA, tempbuf, 0, 0, 0, 1);

        MapColors(PALETTE_RED_NINJA, NinjaRed[0], 1);
        for (int i = 1; i < NinjaRed.length; i++) {
            MapColors(PALETTE_RED_NINJA, NinjaRed[i], 0);
        }
        makePalookup(PALETTE_RED_NINJA, tempbuf, 0, 0, 0, 1);

        MapColors(PALETTE_GREEN_NINJA, NinjaGreen[0], 1);
        for (int i = 1; i < NinjaGreen.length; i++) {
            MapColors(PALETTE_GREEN_NINJA, NinjaGreen[i], 0);
        }
        makePalookup(PALETTE_GREEN_NINJA, tempbuf, 0, 0, 0, 1);

        MapColors(PALETTE_GREEN_LIGHTING, AllToGreen[0], 1);
        for (int i = 1; i < AllToGreen.length; i++) {
            MapColors(PALETTE_GREEN_LIGHTING, AllToGreen[i], 0);
        }
        makePalookup(PALETTE_GREEN_LIGHTING, tempbuf, 0, 0, 0, 1);

        MapColors(PALETTE_RED_LIGHTING, AllToRed[0], 1);
        for (int i = 1; i < AllToRed.length; i++) {
            MapColors(PALETTE_RED_LIGHTING, AllToRed[i], 0);
        }
        makePalookup(PALETTE_RED_LIGHTING, tempbuf, 0, 0, 0, 1);

        MapColors(PALETTE_BLUE_LIGHTING, AllToBlue[0], 1);
        for (int i = 1; i < AllToBlue.length; i++) {
            MapColors(PALETTE_BLUE_LIGHTING, AllToBlue[i], 0);
        }
        makePalookup(PALETTE_BLUE_LIGHTING, tempbuf, 0, 0, 0, 1);

        MapColors(PALETTE_ILLUMINATE, Illuminate[0], 1);
        for (int i = 1; i < Illuminate.length; i++) {
            MapColors(PALETTE_ILLUMINATE, Illuminate[i], 0);
        }
        makePalookup(PALETTE_ILLUMINATE, tempbuf, 0, 0, 0, 1);

        // PLAYER COLORS - ALSO USED FOR OTHER THINGS
        for (int play = 0; play < PLAYER_COLOR_MAPS; play++) {
            MapColors(PALETTE_PLAYER0 + play, PlayerColorMap[play][0], 1);
            MapColors(PALETTE_PLAYER0 + play, PlayerColorMap[play][0], 0);
            makePalookup(PALETTE_PLAYER0 + play, tempbuf, 0, 0, 0, 1);
        }

        //
        // Special Brown sludge
        //

        for (int i = 0; i < 256; i++) {
            tempbuf[i] = (byte) i;
        }

        // invert the brown palette
        for (int i = 0; i < 32; i++) {
            tempbuf[LT_BROWN + i] = (byte) ((LT_BROWN + 32) - i);
        }
        makePalookup(PALETTE_SLUDGE, tempbuf, 0, 0, 0, 1);
    }

    private static void MapColors(int num, COLOR_MAP cm, int create) {
        int i;
        float inc;

        if (create != 0) {
            for (i = 0; i < 256; i++) {
                tempbuf[i] = (byte) i;
            }
        }

        if (cm.FromRange == 0 || num <= 0 || num >= 256) {
            return;
        }

        inc = cm.ToRange / ((float) cm.FromRange);

        for (i = 0; i < cm.FromRange; i++) {
            tempbuf[i + cm.FromColor] = (byte) ((i * inc) + cm.ToColor);
        }
    }

    public static class COLOR_MAP {
        public final int FromRange;
        public final int ToRange;
        public final int FromColor;
        public final int ToColor;

        public COLOR_MAP(int FromRange, int ToRange, int FromColor, int ToColor) {
            this.FromRange = FromRange;
            this.ToRange = ToRange;
            this.FromColor = FromColor;
            this.ToColor = ToColor;
        }
    }
}
