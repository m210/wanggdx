package ru.m210projects.Wang.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.Tools.Interpolation;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.CommandResponse;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.commands.OsdCommand;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Type.LONGp;

import static ru.m210projects.Build.Pragmas.dmulscale;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Wang.Game.nInterpolation;
import static ru.m210projects.Wang.Gameutils.NORM_ANGLE;
import static ru.m210projects.Wang.Gameutils.synctics;
import static ru.m210projects.Wang.Rooms.cz;
import static ru.m210projects.Wang.Rooms.fz;

public class WangEngine extends Engine {

    public static String randomStack = "";

    public WangEngine(BuildGame game) throws Exception {
        super(game);
        inittimer(game.pCfg.isLegacyTimer(), 120, synctics);

        Console.out.registerCommand(new OsdCommand("fastdemo", "fastdemo \"demo speed\"") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (argv.length != 1) {
                    return CommandResponse.DESCRIPTION_RESPONSE;
                }

                try {
                    getTimer().setSkipTicks(Math.max(0, Integer.parseInt(argv[0])));
                } catch (Exception e) {
                    return CommandResponse.BAD_ARGUMENT_RESPONSE;
                }
                return CommandResponse.OK_RESPONSE;
            }
        });
    }

    @Override
    public void inittimer(boolean isLegacy, int tickspersecond, int frameTicks) { // jfBuild
        int totalclock = 0;
        if (timer != null) {
            // reinit timer
            totalclock = timer.getTotalClock();
        }

        if (isLegacy) {
            this.timer = new WangTimer.Legacy(tickspersecond, frameTicks);
        } else {
            this.timer = new WangTimer(tickspersecond, frameTicks);
        }
        timer.setTotalClock(totalclock);
    }

    @Override
    protected BoardService createBoardService() {
        return new WangBoardService();
    }

    public WangBoardService getBoardService() {
        return (WangBoardService) boardService;
    }

    public PaletteManager loadpalette() throws Exception { // jfBuild
        return new WangPaletteManager(this);
    }

    public Interpolation getInterpolation(int type) {
        if (nInterpolation >= 2) {
            switch (type) {
                case 2:
                    return Main.game.pIntSkip2;
                case 4:
                    return Main.game.pIntSkip4;
            }
        }
        return game.pInt;
    }

    @Override
    public int clipmove(int x, int y, int z, int sectnum, long xvect, long yvect, int walldist, int ceildist,
                        int flordist, int cliptype) {

        clipmove.setTraceNum(clipmoveboxtracenum);
        return super.clipmove(x, y, z, sectnum, xvect, yvect, walldist, ceildist, flordist, cliptype);
    }

    public void dragpoint(int moveskip, int pointhighlight, int dax, int day) {
        if (nInterpolation == 1) {
            super.dragpoint(pointhighlight, dax, day);
            return;
        }

        Wall wall = boardService.getWall(pointhighlight);
        if (wall == null) {
            return;
        }

        // Wang Interpolation
        Interpolation pInt = getInterpolation(moveskip);
        pInt.setwallinterpolate(pointhighlight, wall);
        wall.setX(dax);
        wall.setY(day);

        int cnt = MAXWALLS;
        int tempshort = pointhighlight; // search points CCW
        do {
            Wall tempWall = boardService.getWall(tempshort);
            if (tempWall == null) {
                return;
            }

            Wall nwall = boardService.getWall(tempWall.getNextwall());
            if (nwall != null) {
                tempshort = nwall.getPoint2();
                tempWall = boardService.getWall(tempshort);
                if (tempWall != null) {
                    pInt.setwallinterpolate(tempshort, tempWall);
                    tempWall.setX(dax);
                    tempWall.setY(day);
                }
            } else {
                tempshort = pointhighlight; // search points CW if not searched all the way around
                do {
                    Wall lwall = boardService.getWall(lastwall(tempshort));
                    if (lwall != null && lwall.getNextwall() >= 0) {
                        tempshort = lwall.getNextwall();
                        tempWall = boardService.getWall(tempshort);
                        if (tempWall != null) {
                            pInt.setwallinterpolate(tempshort, tempWall);
                            tempWall.setX(dax);
                            tempWall.setY(day);
                        }
                    } else {
                        break;
                    }

                    cnt--;
                } while ((tempshort != pointhighlight) && (cnt > 0));
                break;
            }
            cnt--;
        } while ((tempshort != pointhighlight) && (cnt > 0));
    }

    @Override
    public int krand() {
//        if (gDemoScreen != null && gDemoScreen.demfile != null && conditionCheck()) {
//            StringBuilder sb = new StringBuilder();
//            randomStack += "\r\n";
//            randomStack += "bseed: " + randomseed;
//            for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
//                if (element.getMethodName().equalsIgnoreCase("domovethings")) {
//                    break;
//                }
//
//                sb.append("\t").append(element.toString());
//                sb.append("\r\n");
//            }
//            randomStack += sb.toString();
//        }

        randomseed = ((randomseed * 21 + 1) & 65535);
        return (randomseed);
    }

    public void getzrange(int x, int y, int z, int sectnum, LONGp hiz, LONGp ceilhit, LONGp loz, LONGp florhit, int walldist, int cliptype) {
        super.getzrange(x, y, z, sectnum, walldist, cliptype);
        if (hiz != null) {
            hiz.value = zr_ceilz;
        }
        if (ceilhit != null) {
            ceilhit.value = zr_ceilhit;
        }
        if (loz != null) {
            loz.value = zr_florz;
        }
        if (florhit != null) {
            florhit.value = zr_florhit;
        }
    }

    public void setspritez(int spritenum, int newx, int newy, int newz) {
        Sprite sprite = boardService.getSprite(spritenum);
        if (sprite == null) {
            return;
        }

        sprite.setX(newx);
        sprite.setY(newy);
        sprite.setZ(newz);

        int tempsectnum = updatesectorz(newx, newy, newz, sprite.getSectnum());
        if (tempsectnum < 0) {
            return;
        }
        if (tempsectnum != sprite.getSectnum()) {
            changespritesect(spritenum, tempsectnum);
        }

    }

    public void getzrangepoint(int x, int y, int z, int sectnum, LONGp ceilz, LONGp ceilhit, LONGp florz, LONGp florhit) {
        Sprite spr;
        int j, k, l, dax, day, daz, xspan, yspan, xoff, yoff;
        int x1, y1, x2, y2, x3, y3, x4, y4, cosang, sinang, tilenum;
        short cstat;
        int clipyou;

        if (!boardService.isValidSector(sectnum)) {
            if (ceilz != null) {
                ceilz.value = 0x80000000;
            }
            if (ceilhit != null) {
                ceilhit.value = -1;
            }
            if (florz != null) {
                florz.value = 0x7fffffff;
            }
            if (florhit != null) {
                florhit.value = -1;
            }
            return;
        }

        // Initialize z's and hits to the current sector's top&bottom
        getzsofslope(sectnum, x, y, fz, cz);
        if (ceilz != null) {
            ceilz.value = cz.get();
        }
        if (florz != null) {
            florz.value = fz.get();
        }
        if (ceilhit != null) {
            ceilhit.value = sectnum | HIT_SECTOR;
        }
        if (florhit != null) {
            florhit.value = sectnum | HIT_SECTOR;
        }

        // Go through sprites of only the current sector
        for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = node.getNext()) {
            j = node.getIndex();
            spr = node.get();
            cstat = spr.getCstat();
            if ((cstat & 49) != 33) {
                continue; // Only check blocking floor sprites
            }

            daz = spr.getZ();

            // Only check if sprite's 2-sided or your on the 1-sided side
            if (((cstat & 64) != 0) && ((z > daz) == ((cstat & 8) == 0))) {
                continue;
            }

            // Calculate and store centering offset information into xoff&yoff
            tilenum = spr.getPicnum();
            ArtEntry pic = getTile(tilenum);
            xoff = (byte) (pic.getOffsetX() + (spr.getXoffset()));
            yoff = (byte) (pic.getOffsetY() + (spr.getYoffset()));
            if ((cstat & 4) != 0) {
                xoff = -xoff;
            }
            if ((cstat & 8) != 0) {
                yoff = -yoff;
            }

            // Calculate all 4 points of the floor sprite.
            // (x1,y1),(x2,y2),(x3,y3),(x4,y4)
            // These points will already have (x,y) subtracted from them
            cosang = EngineUtils.sin(NORM_ANGLE(spr.getAng() + 512));
            sinang = EngineUtils.sin(NORM_ANGLE(spr.getAng()));
            xspan = pic.getWidth();
            dax = ((xspan >> 1) + xoff) * spr.getXrepeat();
            yspan = pic.getHeight();
            day = ((yspan >> 1) + yoff) * spr.getYrepeat();
            x1 = spr.getX() + dmulscale(sinang, dax, cosang, day, 16) - x;
            y1 = spr.getY() + dmulscale(sinang, day, -cosang, dax, 16) - y;
            l = xspan * spr.getXrepeat();
            x2 = x1 - mulscale(sinang, l, 16);
            y2 = y1 + mulscale(cosang, l, 16);
            l = yspan * spr.getYrepeat();
            k = -mulscale(cosang, l, 16);
            x3 = x2 + k;
            x4 = x1 + k;
            k = -mulscale(sinang, l, 16);
            y3 = y2 + k;
            y4 = y1 + k;

            // Check to see if point (0,0) is inside the 4 points by seeing if
            // the number of lines crossed as a line is shot outward is odd
            clipyou = 0;
            if ((y1 ^ y2) < 0) // If y1 and y2 have different signs
            // (- / +)
            {
                if ((x1 ^ x2) < 0) {
                    clipyou ^= (x1 * y2 < x2 * y1 ? 1 : 0) ^ (y1 < y2 ? 1 : 0);
                } else if (x1 >= 0) {
                    clipyou ^= 1;
                }
            }
            if ((y2 ^ y3) < 0) {
                if ((x2 ^ x3) < 0) {
                    clipyou ^= (x2 * y3 < x3 * y2 ? 1 : 0) ^ (y2 < y3 ? 1 : 0);
                } else if (x2 >= 0) {
                    clipyou ^= 1;
                }
            }
            if ((y3 ^ y4) < 0) {
                if ((x3 ^ x4) < 0) {
                    clipyou ^= (x3 * y4 < x4 * y3 ? 1 : 0) ^ (y3 < y4 ? 1 : 0);
                } else if (x3 >= 0) {
                    clipyou ^= 1;
                }
            }
            if ((y4 ^ y1) < 0) {
                if ((x4 ^ x1) < 0) {
                    clipyou ^= (x4 * y1 < x1 * y4 ? 1 : 0) ^ (y4 < y1 ? 1 : 0);
                } else if (x4 >= 0) {
                    clipyou ^= 1;
                }
            }
            if (clipyou == 0) {
                continue; // Point is not inside, don't clip
            }

            // Clipping time!
            if (z > daz) {
                if (ceilz != null && daz > ceilz.value) {
                    ceilz.value = daz;
                    if (ceilhit != null) {
                        ceilhit.value = j | HIT_SPRITE;
                    }
                }
            } else {
                if (florz != null && daz < florz.value) {
                    florz.value = daz;
                    if (florhit != null) {
                        florhit.value = j | HIT_SPRITE;
                    }
                }
            }
        }
    }

    public void compare(Sprite src, Sprite dst) {
        Console.out.println("Comparing...", OsdColor.GREEN);
        if (dst.getX() != src.getX()) {
            Console.out.println("Not match x: " + dst.getX() + " != " + src.getX(), OsdColor.RED);
        }
        if (dst.getY() != src.getY()) {
            Console.out.println("Not match y: " + dst.getY() + " != " + src.getY(), OsdColor.RED);
        }
        if (dst.getZ() != src.getZ()) {
            Console.out.println("Not match z: " + dst.getZ() + " != " + src.getZ(), OsdColor.RED);
        }
        if (dst.getCstat() != src.getCstat()) {
            Console.out.println("Not match cstat: " + dst.getCstat() + " != " + src.getCstat(), OsdColor.RED);
        }
        if (dst.getPicnum() != src.getPicnum()) {
            Console.out.println("Not match picnum: " + dst.getPicnum() + " != " + src.getPicnum(), OsdColor.RED);
        }
        if (dst.getShade() != src.getShade()) {
            Console.out.println("Not match shade: " + dst.getShade() + " != " + src.getShade(), OsdColor.RED);
        }
        if (dst.getPal() != src.getPal()) {
            Console.out.println("Not match pal: " + dst.getPal() + " != " + src.getPal(), OsdColor.RED);
        }
        if (dst.getClipdist() != src.getClipdist()) {
            Console.out.println("Not match clipdist: " + dst.getClipdist() + " != " + src.getClipdist(), OsdColor.RED);
        }
        if (dst.getDetail() != src.getDetail()) {
            Console.out.println("Not match detail: " + dst.getDetail() + " != " + src.getDetail(), OsdColor.RED);
        }
        if (dst.getXrepeat() != src.getXrepeat()) {
            Console.out.println("Not match xrepeat: " + dst.getXrepeat() + " != " + src.getXrepeat(), OsdColor.RED);
        }
        if (dst.getYrepeat() != src.getYrepeat()) {
            Console.out.println("Not match xrepeat: " + dst.getYrepeat() + " != " + src.getYrepeat(), OsdColor.RED);
        }
        if (dst.getXoffset() != src.getXoffset()) {
            Console.out.println("Not match xoffset: " + dst.getXoffset() + " != " + src.getXoffset(), OsdColor.RED);
        }
        if (dst.getYoffset() != src.getYoffset()) {
            Console.out.println("Not match yoffset: " + dst.getYoffset() + " != " + src.getYoffset(), OsdColor.RED);
        }
        if (dst.getSectnum() != src.getSectnum()) {
            Console.out.println("Not match sectnum: " + dst.getSectnum() + " != " + src.getSectnum(), OsdColor.RED);
        }
        if (dst.getStatnum() != src.getStatnum()) {
            Console.out.println("Not match statnum: " + dst.getStatnum() + " != " + src.getStatnum(), OsdColor.RED);
        }
        if (dst.getAng() != src.getAng()) {
            Console.out.println("Not match ang: " + dst.getAng() + " != " + src.getAng(), OsdColor.RED);
        }
        if (dst.getOwner() != src.getOwner()) {
            Console.out.println("Not match owner: " + dst.getOwner() + " != " + src.getOwner(), OsdColor.RED);
        }
        if (dst.getXvel() != src.getXvel()) {
            Console.out.println("Not match xvel: " + dst.getXvel() + " != " + src.getXvel(), OsdColor.RED);
        }
        if (dst.getYvel() != src.getYvel()) {
            Console.out.println("Not match yvel: " + dst.getYvel() + " != " + src.getYvel(), OsdColor.RED);
        }
        if (dst.getZvel() != src.getZvel()) {
            Console.out.println("Not match zvel: " + dst.getZvel() + " != " + src.getZvel(), OsdColor.RED);
        }
        if (dst.getLotag() != src.getLotag()) {
            Console.out.println("Not match lotag: " + dst.getLotag() + " != " + src.getLotag(), OsdColor.RED);
        }
        if (dst.getHitag() != src.getHitag()) {
            Console.out.println("Not match hitag: " + dst.getHitag() + " != " + src.getHitag(), OsdColor.RED);
        }
        if (dst.getExtra() != src.getExtra()) {
            Console.out.println("Not match extra: " + dst.getExtra() + " != " + src.getExtra(), OsdColor.RED);
        }
        Console.out.println("Compare completed", OsdColor.GREEN);
    }

}
