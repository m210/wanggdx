package ru.m210projects.Wang.Factory;


import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuRendererSettings;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Menus.*;
import ru.m210projects.Wang.Menus.Network.MenuMultiplayer;
import ru.m210projects.Wang.Menus.Network.MenuNetwork;
import ru.m210projects.Wang.Screens.MenuScreen;

import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Pragmas.scale;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Game.BACKBUTTON;
import static ru.m210projects.Wang.Game.MOUSECURSOR;
import static ru.m210projects.Wang.Gameutils.ROTATE_SPRITE_SCREEN_CLIP;
import static ru.m210projects.Wang.Names.COINCURSOR;
import static ru.m210projects.Wang.Sound.PlaySound;
import static ru.m210projects.Wang.Sound.v3df_dontpan;

public class WangMenuHandler extends MenuHandler {

    public static final int MAIN = 0;
    public static final int GAME = 1;
    public static final int NEWGAME = 2;
    public static final int SOUNDSET = 3;
    public static final int DIFFICULTY = 4;
    public static final int OPTIONS = 5;
    public static final int HELP = 6;
    public static final int LOADGAME = 7;
    public static final int SAVEGAME = 8;
    public static final int QUIT = 9;
    public static final int QUITTITLE = 10;
    public static final int NETWORKGAME = 11;
    public static final int MULTIPLAYER = 12;
    public static final int COLORCORR = 13;
    public static final int NEWADDON = 14;
    public static final int USERCONTENT = 15;
    public static final int LASTSAVE = 16;
    public static final int CORRUPTLOAD = 17;

    public static final int pic_yinyang = 2870;

    private final Engine engine;
    private final BuildGame app;

    public WangMenuHandler(Main app) {
        super(app);
        app.pMenu = app.menu = this;
        mMenus = new BuildMenu[19];
        this.engine = app.pEngine;
        this.app = app;

        mMenus[DIFFICULTY] = new MenuDifficulty(app);
        mMenus[NEWGAME] = new MenuNewGame(app);
        mMenus[LOADGAME] = new MenuLoad(app);
        mMenus[SAVEGAME] = new MenuSave(app);
        mMenus[QUIT] = new MenuQuit(app, false);
        mMenus[QUITTITLE] = new MenuQuit(app, true);
        mMenus[SOUNDSET] = new MenuSound(app);
        mMenus[OPTIONS] = new MenuOptions(app);
        mMenus[NEWADDON] = new MenuNewAddon(app);
        mMenus[NETWORKGAME] = new MenuNetwork(app);
        mMenus[MULTIPLAYER] = new MenuMultiplayer(app);
        mMenus[LASTSAVE] = new MenuLastLoad(app);
        mMenus[CORRUPTLOAD] = new MenuCorruptGame(app);

        mMenus[MAIN] = new MenuMain(app);
        mMenus[GAME] = new MenuGame(app);
    }

    @Override
    public void mDrawMenu() {
        if (!(app.getScreen() instanceof MenuScreen) && !(app.pMenu.getCurrentMenu() instanceof BuildMenuList)
                && !(app.pMenu.getCurrentMenu() instanceof MenuInterface)) {

            Renderer renderer = game.getRenderer();
            int tile = 2324;
            ArtEntry pic = renderer.getTile(tile);
            int xdim = renderer.getWidth();
            int ydim = renderer.getHeight();

            float kt = xdim / (float) ydim;
            float kv = pic.getWidth() / (float) pic.getHeight();
            float scale;
            if (kv >= kt) {
                scale = (ydim + 1) / (float) pic.getHeight();
            } else {
                scale = (xdim + 1) / (float) pic.getWidth();
            }

            renderer.rotatesprite(0, 0, (int) (scale * 65536), 0, tile, 127, 4, 8 | 16 | 1);
        }

        super.mDrawMenu();
    }

    @Override
    public int getShade(MenuItem item) {
        if (item != null) {
            if (!item.isEnabled()) {
                return 16;
            }

            if (item.font.equals(app.getFont(0))) {
                int shade = 8;
                if (item.isFocused()) {
                    shade = 8 + mulscale(16, EngineUtils.sin((32 * engine.getTotalClock()) & 2047), 16);
                }
                return shade;
            }
        }
        return 0;
    }

    @Override
    public int getPal(Font font, MenuItem item) {

        // for UserContent list
        if (item != null && item.font.equals(app.getFont(0))) // MiniFont
        {
            if (item.isFocused()) {
                return 24;
            }
        }

        return 16;
    }

    @Override
    public void mPostDraw(MenuItem item) {
        Renderer renderer = game.getRenderer();
        if (item.isFocused()) {
            if (item instanceof MenuList) {
                if (item instanceof MenuJoyList) {
                    MenuList list = (MenuList) item;

                    int px = list.x;

                    int focus = list.l_nFocus;
                    if (focus == -1 || focus < list.l_nMin || focus >= list.l_nMin + list.rowCount) {
                        return;
                    }

                    int py = list.y + (focus - list.l_nMin) * (list.mFontOffset());

                    renderer.rotatesprite(px - 6 << 16, py + 3 << 16, 20000, 0, COINCURSOR + ((engine.getTotalClock() >> 3) % 7), 0,
                            0, ROTATE_SPRITE_SCREEN_CLIP);
                } else if (item instanceof MenuKeyboardList) {
                    MenuList list = (MenuList) item;

                    int px = list.x;

                    int focus = list.l_nFocus;
                    if (focus == -1 || focus < list.l_nMin || focus >= list.l_nMin + list.rowCount) {
                        return;
                    }

                    int py = list.y + (focus - list.l_nMin) * (list.mFontOffset());

                    renderer.rotatesprite(px - 6 << 16, py + 3 << 16, 20000, 0, COINCURSOR + ((engine.getTotalClock() >> 3) % 7), 0,
                            0, ROTATE_SPRITE_SCREEN_CLIP);
                } else if (item instanceof MenuSlotList) {
                    MenuSlotList list = (MenuSlotList) item;
                    if (!list.deleteQuestion && !list.isTyping()) {
                        int px = list.x;
                        int focus = list.l_nFocus;
                        if (focus == -1 || focus < list.l_nMin || focus >= list.l_nMin + list.rowCount) {
                            return;
                        }

                        int py = list.y + (focus - list.l_nMin) * (list.mFontOffset());
                        renderer.rotatesprite((px + 125) << 16, py + 2 << 16, 20000, 0, COINCURSOR + ((engine.getTotalClock() >> 3) % 7), 0,
                                0, ROTATE_SPRITE_SCREEN_CLIP);
                    }
                } else if (item instanceof MenuResolutionList) {
                    MenuList list = (MenuList) item;

                    int px = list.x;

                    int focus = list.l_nFocus;
                    if (focus == -1 || focus < list.l_nMin || focus >= list.l_nMin + list.rowCount) {
                        return;
                    }

                    int py = list.y + (focus - list.l_nMin) * (list.mFontOffset());

                    renderer.rotatesprite(px << 16, py + 3 << 16, 20000, 0, COINCURSOR + ((engine.getTotalClock() >> 3) % 7), 0,
                            0, ROTATE_SPRITE_SCREEN_CLIP);
                } else {
                    MenuList list = (MenuList) item;

                    ArtEntry pic = engine.getTile(pic_yinyang);

                    int scale = 32768;
                    int px = list.x;
                    int focus = list.l_nFocus;
                    if (focus == -1 || focus < list.l_nMin || focus >= list.l_nMin + list.rowCount) {
                        return;
                    }
                    int py = list.y + (focus - list.l_nMin) * (list.mFontOffset());
                    int xoff = 0, yoff = 0;
                    if (item.font.equals(app.getFont(1))) {
                        xoff = -(pic.getWidth() / 2) + 10;
                        yoff = 3;
                    } else if (item.font.equals(app.getFont(2))) {
                        xoff = -(pic.getWidth() / 2) + 10;
                        yoff = 8;
                    }

                    renderer.rotatesprite((px + xoff) << 16, (py + yoff) << 16, scale, 0, pic_yinyang, 0, 0, 10);
                }
            } else if (item instanceof MenuFileBrowser) {
                MenuFileBrowser list = (MenuFileBrowser) item;
                int px = list.x;

                int focus = list.getFocus();
                if (focus == -1 || focus < list.getMin() || focus >= list.getMin() + list.getRowCount()) {
                    return;
                }

                int py = list.y + (focus - list.getMin()) * (list.mFontOffset()) + 23;
                int column = list.getColumn();

                String value = list.getText(column, focus);
                int textWidth = list.font.getWidth(value, 1.0f);
                if (column == 1) { // FILE
                    px += list.width - textWidth - 17;
                } else {
                    px += textWidth + 17;
                }

                renderer.rotatesprite(px << 16, py + 3 << 16, 20000, 0, COINCURSOR + ((engine.getTotalClock() >> 3) % 7), 0,
                        0, ROTATE_SPRITE_SCREEN_CLIP);
            } else if (!(item instanceof MenuVariants)) {
                int scale = 32768;
                int px = item.x;
                int py = item.y;
                int xoff = 0, yoff = 0;
                ArtEntry pic = engine.getTile(pic_yinyang);

                if (item.font.equals(app.getFont(1))) {
                    xoff = -(pic.getWidth() / 2) + 10;
                    yoff = 3;
                }

                if (item.font.equals(app.getFont(2))) {
                    xoff = -(pic.getWidth() / 2) + 10;
                    yoff = 8;
                }

                renderer.rotatesprite((px + xoff) << 16, (py + yoff) << 16, scale, 0, pic_yinyang, 0, 0, 10);
            }
        }
    }

    @Override
    public void mDrawMouse(int x, int y) {
        if (!app.pCfg.isMenuMouse()) {
            return;
        }

        Renderer renderer = game.getRenderer();
        int zoom = scale(0x10000, renderer.getHeight(), 200);
        int czoom = mulscale(0x8000, mulscale(zoom, app.pCfg.getgMouseCursorSize(), 16), 16);
        int xoffset = 0;
        int yoffset = 0;
        int ang = 0;

        renderer.rotatesprite((x + xoffset) << 16, (y + yoffset) << 16, czoom, ang, MOUSECURSOR, 0, 0, 8);
    }

    @Override
    public void mDrawBackButton() {
        if (!app.pCfg.isMenuMouse()) {
            return;
        }

        Renderer renderer = game.getRenderer();
        ArtEntry pic = renderer.getTile(BACKBUTTON);
        int ydim = renderer.getHeight();

        int zoom = scale(16384, ydim, 200);
        if (mCount > 1) {
            // Back button
            int shade = 4 + mulscale(16, EngineUtils.sin((20 * engine.getTotalClock()) & 2047), 16);
            renderer.rotatesprite(0, (ydim - mulscale(pic.getHeight(), zoom, 16)) << 16, zoom, 0, BACKBUTTON, shade,
                    0, 8 | 16, 0, 0, mulscale(zoom, pic.getWidth() - 1, 16), ydim - 1);
        }
    }

    @Override
    public boolean mCheckBackButton(int mx, int my) {
        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();

        int zoom = scale(16384, ydim, 200);
        ArtEntry pic = renderer.getTile(BACKBUTTON);

        int size = mulscale(pic.getWidth(), zoom, 16);
        int bx = 0;
        int by = ydim - mulscale(pic.getHeight(), zoom, 16);
        if (mx >= bx && mx < bx + size) {
            return my >= by && my < by + size;
        }
        return false;
    }

    @Override
    public void mSound(MenuItem item, MenuOpt opt) {
        switch (opt) {
            case Open:
                if (mCount > 1) {
                    PlaySound(DIGI_SWORDSWOOSH, null, v3df_dontpan);
                }
                break;
            case ENTER:
                PlaySound(DIGI_SWORDSWOOSH, null, v3df_dontpan);
                break;
            case UP:
            case DW:
            case LEFT:
            case RIGHT:
            case PGUP:
            case PGDW:
            case HOME:
            case END:
            case MCHANGE:
                PlaySound(DIGI_STAR, null, v3df_dontpan);
                break;
            case Close:
                if (mCount == 1) {
                    PlaySound(DIGI_STARCLINK, null, v3df_dontpan);
                } else {
                    PlaySound(DIGI_SWORDSWOOSH, null, v3df_dontpan);
                }
                break;
            default:
                break;
        }
    }

}
