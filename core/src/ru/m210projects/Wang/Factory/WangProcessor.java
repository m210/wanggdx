package ru.m210projects.Wang.Factory;

import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Build.Pattern.BuildNet.NetInput;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Wang.Config.SwKeys;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Type.Input;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Wang.Draw.*;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Inv.*;
import static ru.m210projects.Wang.JPlayer.computergetinput;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Text.PutStringInfo;
import static ru.m210projects.Wang.Type.MyTypes.BIT;
import static ru.m210projects.Wang.Type.MyTypes.TEST;

public class WangProcessor extends GameProcessor {

    public static final int MAXANGVEL = 100;
    public final int TURBOTURNTIME = (120 / 8);
    public final int NORMALTURN = (12 + 6);
//    public final int RUNTURN = (28);
    public final int PREAMBLETURN = 3;
    public final int NORMALKEYMOVE = 35;
    public final int MAXVEL = ((NORMALKEYMOVE * 2) + 10);
    public final int MAXSVEL = ((NORMALKEYMOVE * 2) + 10);
    public int turnheldtime; // MED

    private final WangPrompt wangPrompt;

    public WangProcessor(Main main) {
        super(main);
        this.wangPrompt = new WangPrompt();
    }

    public WangPrompt getWangPrompt() {
        return wangPrompt;
    }

    private void SET_LOC_KEY(Input loc, int flags, GameKey key) {
        loc.bits |= isGameKeyPressed(key) ? 1 << flags : 0;
    }

    @Override
    public void fillInput(NetInput input) {
        mouseDelta.x /= 1.5f;
        mouseDelta.y /= 1.5f;

        float daang;
        int tics = 4;
        boolean running;
        int turnamount;
        int keymove;

        PlayerStr pp = Player[myconnectindex];
        if (pp.PlayerSprite == -1) {
            return;
        }

        Input loc = (Input) input;
        loc.reset();

        int vel;
        int svel;
        float horiz;
        float angvel;
        if (game.pMenu.gShowMenu || Console.out.isShowing() || MessageInputMode
                || (game.gPaused /*&& !ctrlKeyStatus(KEY_PAUSE)*/)) { // fixme
            loc.vel = 0;
            loc.svel = 0;
            loc.angvel = 0;
            loc.aimvel = 0;
            loc.bits = 0;

            if (Console.out.isShowing()) {
                MessageInputMode = false;
            }
            return;
        }

        if(isGameKeyPressed(GameKeys.Send_Message)) {
            MessageInputMode = true;
            wangPrompt.setCaptureInput(true);
        }

        svel = vel = 0;
        horiz = angvel = 0;

        if (isGameKeyJustPressed(GameKeys.Mouse_Aiming)) {
            cfg.setgMouseAim(!cfg.isgMouseAim());
            if (!cfg.isgMouseAim()) {
                loc.bits |= (1 << SK_LOOK_UP);
                PutStringInfo(pp, "Mouse Aiming Off");
            } else {
                PutStringInfo(pp, "Mouse Aiming On");
            }
        }

        // MAP KEY
        if (isGameKeyJustPressed(GameKeys.Map_Toggle)) {

            // Init follow coords
            Follow_posx = pp.posx;
            Follow_posy = pp.posy;

            if (dimensionmode == 3) {
                dimensionmode = 5;
            } else if (dimensionmode == 5) {
                dimensionmode = 6;
            } else {
                dimensionmode = 3;
                ScrollMode2D = false;
            }
        }

        if (NETTEST) {
            gNet.BotSkill = 3;
            if (myconnectindex != connecthead) {
                computergetinput(myconnectindex, loc);
                return;
            }
        }

        if (GodMode) {
            loc.bits |= isKeyJustPressed(com.badlogic.gdx.Input.Keys.J) ? (1 << SK_FLY) : 0;
        }

        if (isGameKeyJustPressed(SwKeys.Show_Opp_Weapon)) {
            cfg.ShowWeapon = !cfg.ShowWeapon;
        }

        SET_LOC_KEY(loc, SK_CENTER_VIEW, SwKeys.Aim_Center);

        SET_LOC_KEY(loc, SK_RUN, GameKeys.Run);
        SET_LOC_KEY(loc, SK_SHOOT, GameKeys.Weapon_Fire);

        // actually snap
        SET_LOC_KEY(loc, SK_SNAP_UP, SwKeys.Aim_Up);
        SET_LOC_KEY(loc, SK_SNAP_DOWN, SwKeys.Aim_Down);

        // actually just look
        SET_LOC_KEY(loc, SK_LOOK_UP, GameKeys.Look_Up);
        SET_LOC_KEY(loc, SK_LOOK_DOWN, GameKeys.Look_Down);

        SET_LOC_KEY(loc, SK_TILT_LEFT, SwKeys.Tilt_Left);
        SET_LOC_KEY(loc, SK_TILT_RIGHT, SwKeys.Tilt_Right);

        for (int i = 0; i < MAX_WEAPONS_KEYS; i++) {
            if (isGameKeyJustPressed(cfg.getKeymap()[i + cfg.weaponIndex])) {
                loc.bits |= (i + 1);
                break;
            }
        }

        if (isGameKeyJustPressed(SwKeys.Last_Weap_Switch)) {
            loc.bits |= pp.last_used_weapon + 1;
        }

        if (isGameKeyJustPressed(SwKeys.Special_Fire)) {
            USER u = getUser(pp.PlayerSprite);
            if (u != null) {
                loc.bits |= u.WeaponNum + 1;
            }
        }

        if (isGameKeyJustPressed(GameKeys.Previous_Weapon)) {
            USER u = getUser(pp.PlayerSprite);
            if (u != null) {
                int prev_weapon;
                int start_weapon = u.WeaponNum - 1;

                if (u.WeaponNum == WPN_SWORD) {
                    prev_weapon = 13;
                } else if (u.WeaponNum == WPN_STAR) {
                    prev_weapon = 14;
                } else {
                    for (int i = start_weapon; ; i--) {
                        if (i <= -1) {
                            i = WPN_HEART;
                        }

                        if (TEST(pp.WpnFlags, BIT(i)) && pp.WpnAmmo[i] != 0) {
                            prev_weapon = i;
                            break;
                        }
                    }
                }
                loc.bits |= prev_weapon + 1;
            }
        }

        if (isGameKeyJustPressed(GameKeys.Next_Weapon)) {
            USER u = getUser(pp.PlayerSprite);
            if (u != null) {
                int next_weapon;
                int start_weapon = u.WeaponNum + 1;

                if (u.WeaponNum == WPN_SWORD) {
                    start_weapon = WPN_STAR;
                }

                if (u.WeaponNum == WPN_FIST) {
                    next_weapon = 14;
                } else {
                    for (int i = start_weapon; ; i++) {
                        if (i >= MAX_WEAPONS_KEYS) {
                            next_weapon = 13;
                            break;
                        }

                        if (TEST(pp.WpnFlags, BIT(i)) && pp.WpnAmmo[i] != 0) {
                            next_weapon = i;
                            break;
                        }
                    }
                }
                loc.bits |= next_weapon + 1;
            }
        }

        int inv_hotkey = 0;
        if (isGameKeyJustPressed(SwKeys.MedKit)) {
            inv_hotkey = INVENTORY_MEDKIT + 1;
        }
        if (isGameKeyJustPressed(SwKeys.SmokeBomb)) {
            inv_hotkey = INVENTORY_CLOAK + 1;
        }
        if (isGameKeyJustPressed(SwKeys.NightVision)) {
            inv_hotkey = INVENTORY_NIGHT_VISION + 1;
        }
        if (isGameKeyJustPressed(SwKeys.GasBomb)) {
            inv_hotkey = INVENTORY_CHEMBOMB + 1;
        }
        if (isGameKeyJustPressed(SwKeys.FlashBomb) && dimensionmode == 3) {
            inv_hotkey = INVENTORY_FLASHBOMB + 1;
        }
        if (isGameKeyJustPressed(SwKeys.Caltrops)) {
            inv_hotkey = INVENTORY_CALTROPS + 1;
        }

        loc.bits |= (inv_hotkey << SK_INV_HOTKEY_BIT0);

        SET_LOC_KEY(loc, SK_INV_USE, SwKeys.Inventory_Use);

        SET_LOC_KEY(loc, SK_OPERATE, GameKeys.Open);
        SET_LOC_KEY(loc, SK_JUMP, GameKeys.Jump);
        SET_LOC_KEY(loc, SK_CRAWL, GameKeys.Crouch);

        SET_LOC_KEY(loc, SK_TURN_180, GameKeys.Turn_Around);

        SET_LOC_KEY(loc, SK_INV_LEFT, SwKeys.Inventory_Left);
        SET_LOC_KEY(loc, SK_INV_RIGHT, SwKeys.Inventory_Right);

        SET_LOC_KEY(loc, SK_HIDE_WEAPON, SwKeys.Holster_Weapon);

        SET_LOC_KEY(loc, SK_CRAWL_LOCK, SwKeys.Crouch_toggle);

        SET_LOC_KEY(loc, SK_RUN_LOCK, SwKeys.AutoRun);

        loc.bits |= (isGameKeyPressed(GameKeys.Open) /*|| ctrlKeyStatus(Keys.SPACE)*/) ? 1 << SK_SPACE_BAR : 0;
        loc.bits |= isKeyJustPressed(com.badlogic.gdx.Input.Keys.PAUSE) ? (1 << SK_PAUSE) : 0;

        running = ((!TEST(pp.Flags, PF_LOCK_RUN) && TEST(loc.bits, BIT(SK_RUN)))
                || (!TEST(loc.bits, BIT(SK_RUN)) && TEST(pp.Flags, PF_LOCK_RUN)));

        if (running) {
            turnamount = NORMALTURN << 1;
            keymove = NORMALKEYMOVE << 1;
        } else {
            turnamount = NORMALTURN;
            keymove = NORMALKEYMOVE;
        }

        if (pp.sop_control != -1) {
            turnamount *= 3;
        }

        if (isGameKeyPressed(GameKeys.Strafe) && pp.sop == -1) {
            if (isGameKeyPressed(GameKeys.Turn_Left)) {
                svel -= -keymove;
            }
            if (isGameKeyPressed(GameKeys.Turn_Right)) {
                svel -= keymove;
            }
            svel = (int) BClipRange(svel - 20 * ctrlGetMouseStrafe(), -keymove, keymove);
        } else {
            if (isGameKeyPressed(GameKeys.Turn_Left)) {
                turnheldtime += tics;
                if (turnheldtime >= TURBOTURNTIME) {
                    angvel -= turnamount;
                } else {
                    angvel -= PREAMBLETURN;
                }
            } else if (isGameKeyPressed(GameKeys.Turn_Right)) {
                turnheldtime += tics;
                if (turnheldtime >= TURBOTURNTIME) {
                    angvel += turnamount;
                } else {
                    angvel += PREAMBLETURN;
                }
            } else {
                turnheldtime = 0;
            }
            angvel = BClipRange(angvel + ctrlGetMouseTurn(), -1024, 1024);
        }

        if (isGameKeyPressed(GameKeys.Strafe_Left)) {
            if (pp.sop == -1) {
                svel += keymove;
            } else {
                angvel -= turnamount;
            }
        }
        if (isGameKeyPressed(GameKeys.Strafe_Right)) {
            if (pp.sop == -1) {
                svel -= keymove;
            } else {
                angvel += turnamount;
            }
        }

        if (isGameKeyPressed(GameKeys.Move_Forward)) {
            vel += keymove;
        }
        if (isGameKeyPressed(GameKeys.Move_Backward)) {
            vel -= keymove;
        }

        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();

        if (cfg.isgMouseAim()) {
            horiz = BClipRange(ctrlGetMouseLook(!cfg.isgInvertmouse()), -(ydim >> 1), 100 + (ydim >> 1));
        } else {
            vel = (int) BClipRange(vel - ctrlGetMouseMove(), -4 * keymove, 4 * keymove);
        }

        Vector2 stick1 = ctrlGetStick(JoyStick.LOOKING);
        Vector2 stick2 = ctrlGetStick(JoyStick.MOVING);

        float lookx = stick1.x;
        float looky = stick1.y;

        if (looky != 0) {
            float k = 8.0f;
            horiz = BClipRange(horiz - k * looky * cfg.getJoyLookSpeed(), -(ydim >> 1), 100 + (ydim >> 1));
        }

        if (lookx != 0) {
            float k = 8.0f;
            angvel = BClipRange(angvel + k * lookx * cfg.getJoyTurnSpeed(), -1024, 1024);
        }

        if (stick2.y != 0) {
            vel = (int) BClipRange(vel - (keymove * stick2.y), -4 * keymove, 4 * keymove);
        }

        if (stick2.x != 0) {
            svel = (int) BClipRange(svel - (keymove * stick2.x), -4 * keymove, 4 * keymove);
        }

        if (vel < -MAXVEL) {
            vel = -MAXVEL;
        }
        if (vel > MAXVEL) {
            vel = MAXVEL;
        }
        if (svel < -MAXSVEL) {
            svel = -MAXSVEL;
        }
        if (svel > MAXSVEL) {
            svel = MAXSVEL;
        }
        if (angvel < -MAXANGVEL) {
            angvel = -MAXANGVEL;
        }
        if (angvel > MAXANGVEL) {
            angvel = MAXANGVEL;
        }
        if (horiz < -128) {
            horiz = -128;
        }
        if (horiz > 127) {
            horiz = 127;
        }

        daang = pp.getAnglef();

        int momx = (int) (vel * BCosAngle(BClampAngle(daang)) / 512.0f);
        int momy = (int) (vel * BSinAngle(BClampAngle(daang)) / 512.0f);

        momx += (int) (svel * BSinAngle(BClampAngle(daang)) / 512.0f);
        momy += (int) (svel * BCosAngle(BClampAngle(daang + 1024)) / 512.0f);

        if (ScrollMode2D && (dimensionmode == 5 || dimensionmode == 6)) {
            Follow_posx += momx / 4;
            Follow_posy += momy / 4;

            Follow_posx = Math.max(Follow_posx, x_min_bound);
            Follow_posy = Math.max(Follow_posy, y_min_bound);
            Follow_posx = Math.min(Follow_posx, x_max_bound);
            Follow_posy = Math.min(Follow_posy, y_max_bound);

            loc.vel = 0;
            loc.svel = 0;
            loc.angvel = 0;
            loc.aimvel = 0;
            return;
        }

        loc.vel =  momx;
        loc.svel =  momy;
        loc.angvel = angvel;
        loc.aimvel = horiz;
    }

    @Override
    public boolean keyRepeat(int keycode) {
        if (wangPrompt.isCaptured()) {
            return wangPrompt.keyRepeat(keycode);
        }
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        if (wangPrompt.isCaptured()) {
            if (keycode == com.badlogic.gdx.Input.Keys.ESCAPE) {
                wangPrompt.setCaptureInput(false);
                wangPrompt.clear();
                MessageInputMode = false;
            } else {
                wangPrompt.keyDown(keycode);
            }
            return true;
        }
        return super.keyDown(keycode);
    }

    @Override
    public boolean keyUp(int i) {
        wangPrompt.keyUp(i);
        return super.keyUp(i);
    }

    @Override
    public boolean keyTyped(char i) {
        if (wangPrompt.isCaptured()) {
            wangPrompt.keyTyped(i);
            return true;
        }
        return super.keyTyped(i);
    }

}
