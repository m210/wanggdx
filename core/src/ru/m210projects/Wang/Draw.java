package ru.m210projects.Wang;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;
import ru.m210projects.Build.Render.RenderedSpriteList;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.font.BitmapFont;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClampAngle;
import static ru.m210projects.Build.Gameutils.coordsConvertXScaled;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Build.Strhandler.Bitoa;
import static ru.m210projects.Build.Strhandler.buildString;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Cheats.LocationInfo;
import static ru.m210projects.Wang.Enemies.Sumo.BossHealthMeter;
import static ru.m210projects.Wang.Factory.WangNetwork.PredictionOn;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Inv.DrawInventory;
import static ru.m210projects.Wang.Inv.InventoryData;
import static ru.m210projects.Wang.JPlayer.*;
import static ru.m210projects.Wang.JSector.JAnalyzeSprites;
import static ru.m210projects.Wang.JSector.JS_DrawMirrors;
import static ru.m210projects.Wang.JTags.LUMINOUS;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.*;
import static ru.m210projects.Wang.Panel.*;
import static ru.m210projects.Wang.Player.*;
import static ru.m210projects.Wang.Quake.QuakeViewChange;
import static ru.m210projects.Wang.Rooms.*;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Sprites.MoveSkip2;
import static ru.m210projects.Wang.Sprites.MoveSkip4;
import static ru.m210projects.Wang.Text.*;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Vis.VisViewChange;
import static ru.m210projects.Wang.Weapon.*;

public class Draw {

    public static final int INVISTILE = 6145;
    public static final int MINI_BAR_HEALTH_BOX_PIC = 2437;
    public static final int MINI_BAR_AMMO_BOX_PIC = 2437;
    public static final int MINI_BAR_INVENTORY_BOX_PIC = 2438;
    public static final int MINI_BAR_HEALTH_BOX_X = 4;
    private static final short[] RotTable8 = {0, 7, 6, 5, 4, 3, 2, 1};
    private static final short[] RotTable5 = {0, 1, 2, 3, 4, 3, 2, 1};
    private static final char[] txt_buffer = new char[256];
    private static final int DART_PIC = 2526;
    private static final int DART_REPEAT = 16;
    private static final short[] CompassPic = {COMPASS_EAST, COMPASS_EAST2, COMPASS_TIC, COMPASS_TIC2, COMPASS_MID_TIC,
            COMPASS_MID_TIC2, COMPASS_TIC, COMPASS_TIC2,

            COMPASS_SOUTH, COMPASS_SOUTH2, COMPASS_TIC, COMPASS_TIC2, COMPASS_MID_TIC, COMPASS_MID_TIC2, COMPASS_TIC,
            COMPASS_TIC2,

            COMPASS_WEST, COMPASS_WEST2, COMPASS_TIC, COMPASS_TIC2, COMPASS_MID_TIC, COMPASS_MID_TIC2, COMPASS_TIC,
            COMPASS_TIC2,

            COMPASS_NORTH, COMPASS_NORTH2, COMPASS_TIC, COMPASS_TIC2, COMPASS_MID_TIC, COMPASS_MID_TIC2, COMPASS_TIC,
            COMPASS_TIC2};
    public static int gPlayerIndex = -1;
    public static final boolean[] bCopySpriteOffs = new boolean[MAXSPRITES];
    public static int dimensionmode;
    public static boolean ScrollMode2D;

    public static final boolean NoMeters = false;
    public static int gNameShowTime;
    /*
     * !AIC - At draw time this is called for actor rotation. GetRotation() is more
     * complex than needs to be in part because importing of actor rotations and
     * x-flip directions was not standardized.
     */
    public static int Follow_posx, Follow_posy, zoom = 768;
    public static final ParentalStruct[] aVoxelArray = new ParentalStruct[MAXTILES];
    public static boolean OverlapDraw = false;
    public static final short[] CompassShade = {25, 19, 15, 9, 1, 1, 9, 15, 19, 25};

    private static final short[] viewWeaponTile = new short[]{-1, ICON_STAR, ICON_SHOTGUN, ICON_UZI, ICON_MICRO_GUN, ICON_GRENADE_LAUNCHER, 2223, ICON_RAIL_GUN, ICON_GUARD_HEAD, ICON_HEART, -1, -1, -1, -1};
    // private static short[] viewWeaponTile = {-1, -1, 524, 559, 558, 526, 589, 618, 539, 800, 525, 811, 810, -1};
    private static int ang = 0;
    // last valid stuff
    private static int lv_sectnum = -1;
    private static int lv_x, lv_y, lv_z;

    public static void ShadeSprite(Sprite tsp) {
        Sector sec = boardService.getSector(tsp.getSectnum());
        if (sec == null) {
            return;
        }

        // set shade of sprite
        tsp.setShade((byte) (sec.getFloorshade() - 25));
        if (tsp.getShade() > -3) {
            tsp.setShade(-3);
        }

        if (tsp.getShade() < -30) {
            tsp.setShade(-30);
        }
    }

    public static int GetRotation(TSprite tsp, int viewx, int viewy) {
        USER tu = getUser(tsp.getOwner());

        if (tu == null || tu.RotNum == 0) {
            return (0);
        }

        // Get which of the 8 angles of the sprite to draw (0-7)
        // rotation ranges from 0-7
        int angle2 = EngineUtils.getAngle(tsp.getX() - viewx, tsp.getY() - viewy);
        int rotation = ((tsp.getAng() + 3072 + 128 - angle2) & 2047);
        rotation = (rotation >> 8) & 7;

        if (tu.RotNum == 5) {
            if (TEST(tu.Flags, SPR_XFLIP_TOGGLE)) {
                if (rotation <= 4) {
                    // leave rotation alone
                    tsp.setCstat(tsp.getCstat() & ~(CSTAT_SPRITE_XFLIP));
                } else {
                    rotation = (8 - rotation);
                    tsp.setCstat(tsp.getCstat() | (CSTAT_SPRITE_XFLIP)); // clear x-flipping bit
                }
            } else {
                if (rotation > 3 || rotation == 0) {
                    // leave rotation alone
                    tsp.setCstat(tsp.getCstat() & ~(CSTAT_SPRITE_XFLIP)); // clear x-flipping bit
                } else {
                    rotation = (8 - rotation);
                    tsp.setCstat(tsp.getCstat() | (CSTAT_SPRITE_XFLIP)); // set
                }
            }

            // Special case bunk
            if (tu.ID == TOILETGIRL_R0 || tu.ID == WASHGIRL_R0 || tu.ID == TRASHCAN || tu.ID == CARGIRL_R0
                    || tu.ID == MECHANICGIRL_R0 || tu.ID == PRUNEGIRL_R0 || tu.ID == SAILORGIRL_R0) {
                tsp.setCstat(tsp.getCstat() & ~(CSTAT_SPRITE_XFLIP)); // clear x-flipping bit
            }

            return (RotTable5[rotation]);
        }

        return (RotTable8[rotation]);
    }

    public static void SetActorRotation(TSprite tsp, int viewx, int viewy) {
        USER tu = getUser(tsp.getOwner());
        if (tu == null || tu.RotNum == 0 || tu.getRot() == null) {
            return;
        }

        // don't modify ANY tu vars - back them up!
        State State = tu.State;
        State StateStart = tu.StateStart;

        // Get the offset into the State animation
        int StateOffset = State.id - StateStart.id;

        // Get the rotation angle
        int Rotation = GetRotation(tsp, viewx, viewy);

        if (Rotation >= 5) {
            return; // assert
        }

        // Reset the State animation start based on the Rotation
//        StateStart = tu.Rot.getState(Rotation);

        // Set the sprites state
        State = tu.getRot().getState(Rotation, StateOffset);

        // set the picnum here - may be redundant, but we just changed states and
        // thats a big deal
        tsp.setPicnum(State.Pic);
    }

    public static int DoShadowFindGroundPoint(Sprite sp) {
        // USES TSPRITE !!!!!

        // recursive routine to find the ground - either sector or floor sprite
        // skips over enemy and other types of sprites

        // IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // This will return invalid FAF ceiling and floor heights inside of
        // analyzesprite
        // because the ceiling and floors get moved out of the way for drawing.

        int save_cstat = sp.getCstat();
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        FAFgetzrangepoint(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), tmp_ptr[0], tmp_ptr[1], tmp_ptr[2], tmp_ptr[3]); // &hiz,
        // &ceilhit,
        // &loz,
        // &florhit);
        sp.setCstat(save_cstat);
        int loz = tmp_ptr[2].value;
        int florhit = tmp_ptr[3].value;

        switch (DTEST(florhit, HIT_MASK)) {
            case HIT_SPRITE: {
                Sprite hsp = boardService.getSprite(NORM_HIT_INDEX(florhit));
                if (hsp == null) {
                    return loz;
                }

                if (TEST(hsp.getCstat(), CSTAT_SPRITE_FLOOR)) {
                    // found a sprite floor
                    return (loz);
                } else {
                    // reset the blocking bit of what you hit and try again -
                    // recursive
                    int bak_cstat = hsp.getCstat();
                    hsp.setCstat(hsp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                    loz = DoShadowFindGroundPoint(sp);
                    hsp.setCstat(bak_cstat);
                }
            }

            case HIT_SECTOR:
                return (loz);
        }

        return (loz);
    }

    public static void DoShadows(TSprite tsp, int viewx, int viewy, int viewz) {
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        final USER tu = getUser(tsp.getOwner());
        if (tu == null) {
            return;
        }

        // make sure its the correct sector
        // DoShadowFindGroundPoint calls FAFgetzrangepoint and this is sensitive
        // updatesectorz(tsp.x, tsp.y, tsp.z, &sectnum);
        int sectnum = engine.updatesector(tsp.getX(), tsp.getY(), tsp.getSectnum());
        if (sectnum == -1 || renderedSpriteList.getSize() >= MAXSPRITESONSCREEN) {
            return;
        }

        TSprite newtsp = renderedSpriteList.obtain();
        newtsp.set(tsp);
        newtsp.update(tsp.getX(), tsp.getY(), tsp.getZ(), sectnum);

        // shadow is ALWAYS draw last - status is priority
        newtsp.setStatnum(MAXSTATUS);

        int xrepeat, yrepeat;
        if ((tsp.getYrepeat() >> 2) > 4) {
            xrepeat = newtsp.getXrepeat();
            yrepeat = ((tsp.getYrepeat() >> 2) - (SPRITEp_SIZE_Y(tsp) / 64) * 2);
        } else {
            xrepeat = newtsp.getXrepeat();
            yrepeat = newtsp.getYrepeat();
        }

        newtsp.setShade(127);
        newtsp.setCstat(newtsp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT));

        int loz = tu.loz;
        Sprite losp = boardService.getSprite(tu.lo_sp);
        if (losp != null) {
            if (!TEST(losp.getCstat(), CSTAT_SPRITE_WALL | CSTAT_SPRITE_FLOOR)) {
                loz = DoShadowFindGroundPoint(tsp);
            }
        }

        int camangle = EngineUtils.getAngle(viewx - tsp.getX(), viewy - tsp.getY());
        newtsp.setX(newtsp.getX() - mulscale(EngineUtils.sin((camangle + 512) & 2047), 100, 16));
        newtsp.setY(newtsp.getY() + mulscale(EngineUtils.sin((camangle + 1024) & 2047), 100, 16));
        // need to find the ground here
        newtsp.setZ(loz);

        // if below or close to sprites z don't bother to draw it
        if ((viewz - loz) > -Z(8)) {
            renderedSpriteList.removeLast();
            return;
        }

        // if close to shadows z shrink it
        int view_dist = klabs(loz - viewz) >> 8;
        if (view_dist < 32) {
            view_dist = 256 / view_dist;
        } else {
            view_dist = 0;
        }

        // make shadow smaller depending on height from ground
        int ground_dist = klabs(loz - SPRITEp_BOS(tsp)) >> 8;
        ground_dist = DIV16(ground_dist);

        xrepeat = Math.max(xrepeat - ground_dist - view_dist, 4);
        yrepeat = Math.max(yrepeat - ground_dist - view_dist, 4);
        xrepeat = Math.min(xrepeat, 255);
        yrepeat = Math.min(yrepeat, 255);

        newtsp.setXrepeat(xrepeat);
        newtsp.setYrepeat(yrepeat);

        // Check for voxel items and use a round generic pic if so
    }

    public static void DoMotionBlur(Sprite tsp) {
        Renderer renderer = game.getRenderer();
        USER tu = getUser(tsp.getOwner());
        if (tu == null || tsp.getXvel() == 0) {
            return;
        }

        ang = NORM_ANGLE(tsp.getAng() + 1024);
        int z_amt_per_pixel;
        if (TEST(tsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
            z_amt_per_pixel = ((-tu.jump_speed * ACTORMOVETICS) << 16) / tsp.getXvel();
        } else {
            z_amt_per_pixel = ((-tsp.getZvel()) << 16) / tsp.getXvel();
        }

        int dx, dy, nx, ny, nz;
        switch (tu.motion_blur_dist) {
            case 64:
                dx = nx = MOVEx(64, ang);
                dy = ny = MOVEy(64, ang);
                nz = (z_amt_per_pixel * 64) >> 16;
                break;
            case 128:
                dx = nx = MOVEx(128, ang);
                dy = ny = MOVEy(128, ang);
                nz = (z_amt_per_pixel * 128) >> 16;
                break;
            case 256:
                dx = nx = MOVEx(256, ang);
                dy = ny = MOVEy(256, ang);
                nz = (z_amt_per_pixel * 256) >> 16;
                break;
            case 512:
                dx = nx = MOVEx(512, ang);
                dy = ny = MOVEy(512, ang);
                nz = (z_amt_per_pixel * 512) >> 16;
                break;
            default:
                dx = nx = MOVEx(tu.motion_blur_dist, ang);
                dy = ny = MOVEy(tu.motion_blur_dist, ang);
                nz = 0;
                break;
        }

        int dz = nz;
        int xrepeat = tsp.getXrepeat();
        int yrepeat = tsp.getYrepeat();
        int repeat_adj = 0;

        switch (DTEST(tu.Flags2, SPR2_BLUR_TAPER)) {
            case 0:
                break;
            case SPR2_BLUR_TAPER_SLOW:
                repeat_adj = (xrepeat / (tu.motion_blur_num * 2));
                break;
            case SPR2_BLUR_TAPER_FAST:
                repeat_adj = (xrepeat / tu.motion_blur_num);
                break;
        }

        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();
        for (int i = 0; i < tu.motion_blur_num; i++) {
            if (renderedSpriteList.getSize() >= MAXSPRITESONSCREEN) {
                return;
            }

            Sprite newsp = renderedSpriteList.obtain();
            newsp.set(tsp);
            newsp.setCstat(newsp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_TRANS_FLIP));

            newsp.setX(newsp.getX() + dx);
            newsp.setY(newsp.getY() + dy);
            dx += nx;
            dy += ny;

            newsp.setZ(newsp.getZ() + dz);
            dz += nz;

            newsp.setXrepeat(xrepeat);
            newsp.setYrepeat(yrepeat);

            xrepeat -= repeat_adj;
            yrepeat -= repeat_adj;
        }

    }

    public static void WarpCopySprite() {
        // look for the first one
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        ListNode<Sprite> nsn, nsn2, next_spnum;
        for (ListNode<Sprite> n = boardService.getStatNode(STAT_WARP_COPY_SPRITE1); n != null; n = nsn) {
            nsn = n.getNext();
            Sprite sp1 = n.get();
            int match = sp1.getLotag();

            // look for the second one
            for (ListNode<Sprite> n2 = boardService.getStatNode(STAT_WARP_COPY_SPRITE2); n2 != null; n2 = nsn2) {
                nsn2 = n2.getNext();
                Sprite sp = n2.get();

                if (sp.getLotag() == match) {
                    int sect1 = sp1.getSectnum();
                    int sect2 = sp.getSectnum();

                    for (ListNode<Sprite> node = boardService.getSectNode(sect1); node != null; node = next_spnum) {
                        int spnum = node.getIndex();
                        next_spnum = node.getNext();
                        Sprite spr = node.get();
                        if (spr == sp1) {
                            continue;
                        }

                        if (spr.getPicnum() == ST1) {
                            continue;
                        }

                        if (renderedSpriteList.getSize() >= MAXSPRITESONSCREEN) {
                            continue;
                        }

                        TSprite newtsp = renderedSpriteList.obtain();
                        newtsp.set(spr);

                        newtsp.setOwner(spnum);
                        newtsp.setStatnum(0);

                        int xoff = sp1.getX() - newtsp.getX();
                        int yoff = sp1.getY() - newtsp.getY();
                        int zoff = sp1.getZ() - newtsp.getZ();

                        ILoc oldLoc = game.pInt.getsprinterpolate(spnum);
                        if (oldLoc != null && !bCopySpriteOffs[spnum]) {
                            oldLoc.x += (sp.getX() - sp1.getX());
                            oldLoc.y += (sp.getY() - sp1.getY());
                            oldLoc.z += (sp.getZ() - sp1.getZ());
                            bCopySpriteOffs[spnum] = true;
                        }

                        newtsp.update(sp.getX() - xoff, sp.getY() - yoff, sp.getZ() - zoff, sp.getSectnum());
                    }

                    for (ListNode<Sprite> node = boardService.getSectNode(sect2); node != null; node = next_spnum) {
                        int spnum = node.getIndex();
                        next_spnum = node.getNext();
                        Sprite spr = node.get();
                        if (spr == sp) {
                            continue;
                        }

                        if (spr.getPicnum() == ST1) {
                            continue;
                        }

                        if (renderedSpriteList.getSize() >= MAXSPRITESONSCREEN) {
                            continue;
                        }

                        TSprite newtsp = renderedSpriteList.obtain();
                        newtsp.set(spr);
                        newtsp.setOwner(spnum);
                        newtsp.setStatnum(0);

                        int xoff = sp.getX() - newtsp.getX();
                        int yoff = sp.getY() - newtsp.getY();
                        int zoff = sp.getZ() - newtsp.getZ();

                        ILoc oldLoc = game.pInt.getsprinterpolate(spnum);
                        if (oldLoc != null && !bCopySpriteOffs[spnum]) {
                            oldLoc.x += (sp.getX() - sp1.getX());
                            oldLoc.y += (sp.getY() - sp1.getY());
                            oldLoc.z += (sp.getZ() - sp1.getZ());
                            bCopySpriteOffs[spnum] = true;
                        }

                        newtsp.update(sp1.getX() - xoff, sp1.getY() - yoff, sp1.getZ() - zoff, sp1.getSectnum());
                    }
                }
            }
        }
    }

    public static void DoStarView(Sprite tsp, USER tu, int viewz) {
        int zdiff = viewz - tsp.getZ();

        if (klabs(zdiff) > Z(24)) {
            if (tu.StateStart == s_StarStuck[0]) {
                tsp.setPicnum(s_StarDownStuck[tu.State.id - s_StarStuck[0].id].Pic);
            } else {
                tsp.setPicnum(s_StarDown[tu.State.id - s_Star[0].id].Pic);
            }
        }

        if (zdiff > 0) {
            tsp.setCstat(tsp.getCstat() | (CSTAT_SPRITE_YFLIP));
        }
    }

    public static void analyzesprites(int viewx, int viewy, int viewz, boolean mirror, int smoothratio) {
        Renderer renderer = game.getRenderer();
        PlayerStr pp = Player[screenpeek];
        Sprite psp = pp.getSprite();
        if (psp == null) {
            return;
        }

        ang = NORM_ANGLE(ang + 12);

        int smr4 = (int) (smoothratio + ((long) MoveSkip4 << 16));
        int smr2 = smoothratio + ((MoveSkip2 ? 1 : 0) << 16);
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        if (OverlapDraw) {
            PreDrawStackedWater();
        }

        for (int tSpriteNum = renderedSpriteList.getSize() - 1; tSpriteNum >= 0; tSpriteNum--) {
            TSprite tsp = renderedSpriteList.get(tSpriteNum);
            final int SpriteNum = tsp.getOwner();
            if (SpriteNum == -1) {
                continue;
            }

            USER tu = getUser(SpriteNum);

            // don't draw these
            if (tsp.getStatnum() >= STAT_DONT_DRAW) {
                tsp.setOwner(-1);
                continue;
            }

            // Diss any parentally locked sprites
            if (cfg.ParentalLock || Global_PLock) {
                if (aVoxelArray[tsp.getPicnum()].Parental == 6145) {
                    tsp.setOwner(-1);
                    tu = null;
                } else if (aVoxelArray[tsp.getPicnum()].Parental > 0) {
                    tsp.setPicnum(aVoxelArray[tsp.getPicnum()].Parental); // Change the pic
                }
            }

            if (mirror) {
                // only interpolate certain moving things
                game.pInt.dospriteinterp(tsp, smoothratio);
                game.pIntSkip2.dospriteinterp(tsp, smoothratio);
                game.pIntSkip4.dospriteinterp(tsp, smoothratio);
            }

            if (tu != null) {
                if (tsp.getStatnum() != STAT_DEFAULT) {
                    if (TEST(tu.Flags, SPR_SKIP4)) {
                        if (tsp.getStatnum() <= STAT_SKIP4_INTERP_END) {
                            tsp.setX(tu.ox + mulscale(tsp.getX() - tu.ox, smr4, 18));
                            tsp.setY(tu.oy + mulscale(tsp.getY() - tu.oy, smr4, 18));
                            tsp.setZ(tu.oz + mulscale(tsp.getZ() - tu.oz, smr4, 18));
                        }
                    }

                    if (TEST(tu.Flags, SPR_SKIP2)) {
                        if (tsp.getStatnum() <= STAT_SKIP2_INTERP_END) {
                            tsp.setX(tu.ox + mulscale(tsp.getX() - tu.ox, smr2, 17));
                            tsp.setY(tu.oy + mulscale(tsp.getY() - tu.oy, smr2, 17));
                            tsp.setZ(tu.oz + mulscale(tsp.getZ() - tu.oz, smr2, 17));
                        }
                    }
                }

                if (cfg.Shadows && TEST(tu.Flags, SPR_SHADOW)) {
                    DoShadows(tsp, viewx, viewy, viewz);
                }

                if (cfg.UseDarts) {
                    if (tu.ID == 1793 || tsp.getPicnum() == 1793) {
                        tsp.setPicnum(2519);
                        tsp.setXrepeat(27);
                        tsp.setYrepeat(29);
                    }
                }

                if (tu.ID == STAR1) {
                    if (cfg.UseDarts) {
                        tsp.setPicnum(DART_PIC);
                        tsp.setAng(NORM_ANGLE(tsp.getAng() - 512 - 24));
                        tsp.setXrepeat(DART_REPEAT);
                        tsp.setYrepeat(DART_REPEAT);
                        tsp.setCstat(tsp.getCstat() | CSTAT_SPRITE_WALL);
                    } else {
                        DoStarView(tsp, tu, viewz);
                    }
                } else if (tu.ID == BETTY_R0) { // Mine fix
                    int fz = engine.getflorzofslope(tsp.getSectnum(), tsp.getX(), tsp.getY());
                    if (tsp.getZ() > fz) {
                        tsp.setZ(fz);
                    }
                }

                // rotation
                if (tu.RotNum > 0) {
                    SetActorRotation(tsp, viewx, viewy);
                }

                if (tu.motion_blur_num != 0) {
                    DoMotionBlur(tsp);
                }

                Sector sec = boardService.getSector(tsp.getSectnum());
                if (sec == null) {
                    continue;
                }

                // set palette lookup correctly
                if (tsp.getPal() != sec.getFloorpal()) {
                    if (sec.getFloorpal() == PALETTE_DEFAULT) {
                        // default pal for sprite is stored in tu.spal
                        // mostly for players and other monster types
                        tsp.setPal(tu.spal);
                    } else {
                        // if sector pal is something other than default
                        Sect_User sectu = getSectUser(tsp.getSectnum());
                        int pal = sec.getFloorpal();
                        boolean nosectpal = false;

                        // sprite does not take on the new pal if sector flag is set
                        if (sectu != null && TEST(sectu.flags, SECTFU_DONT_COPY_PALETTE)) {
                            pal = PALETTE_DEFAULT;
                            nosectpal = true;
                        }

                        // if(tu.spal == PALETTE_DEFAULT)
                        if (tsp.getHitag() != SECTFU_DONT_COPY_PALETTE && tsp.getHitag() != LUMINOUS && !nosectpal
                                && pal != PALETTE_FOG && pal != PALETTE_DIVE && pal != PALETTE_DIVE_LAVA) {
                            tsp.setPal(pal);
                        } else {
                            tsp.setPal(tu.spal);
                        }

                    }
                }

                // Sprite debug information mode
                if (tsp.getHitag() == 9997) {
                    tsp.setPal(PALETTE_RED_LIGHTING);
                    // Turn it off, it gets reset by PrintSpriteInfo
                    Sprite spr = boardService.getSprite(SpriteNum);
                    if (spr != null) {
                        spr.setHitag(0);
                    }
                }
            }

            if (cfg.UseDarts) {
                if (tsp.getStatnum() == STAT_STAR_QUEUE) {
                    tsp.setPicnum(DART_PIC);
                    tsp.setAng(NORM_ANGLE(tsp.getAng() - 512));
                    tsp.setXrepeat(DART_REPEAT);
                    tsp.setYrepeat(DART_REPEAT);
                    tsp.setCstat(tsp.getCstat() | CSTAT_SPRITE_WALL);
                }
            }

            // Call my sprite handler
            // Does autosizing and voxel handling
            JAnalyzeSprites(tsp);

            // only do this of you are a player sprite
            if (tu != null && tu.PlayerP != -1) {
                // Shadow spell
                if (!TEST(tsp.getCstat(), CSTAT_SPRITE_TRANSLUCENT)) {
                    ShadeSprite(tsp);
                }

                // sw if its your playersprite
                if (Player[screenpeek].PlayerSprite == SpriteNum) {
                    pp = Player[screenpeek];
                    if (mirror || TEST(pp.Flags, PF_VIEW_FROM_OUTSIDE | PF_VIEW_FROM_CAMERA)) {
                        if (TEST(pp.Flags, PF_VIEW_FROM_OUTSIDE)) {
                            tsp.setCstat(tsp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT));
                        }

                        if (TEST(pp.Flags, PF_CLIMBING)) {
                            // move sprite forward some so he looks like he's
                            // climbing
                            tsp.setX(pp.six + MOVEx(128 + 80, tsp.getAng()));
                            tsp.setY(pp.siy + MOVEy(128 + 80, tsp.getAng()));
                        } else {
                            tsp.setX(pp.six);
                            tsp.setY(pp.siy);
                        }

                        tsp.setZ(tsp.getZ() + pp.siz);
                        tsp.setAng(pp.siang);
                        // continue;
                    } else {
                        // dont draw your sprite
                        tsp.setOwner(-1);
                        // SET(tsp.cstat, CSTAT_SPRITE_INVISIBLE);
                    }
                }

                int tx = tsp.getX() - viewx;
                int ty = tsp.getY() - viewy;
                int angle = ((1024 + EngineUtils.getAngle(tx, ty) - pp.getAnglei()) & 0x7FF) - 1024;
                long dist = EngineUtils.qdist(tx, ty);

                if (klabs(mulscale(angle, dist, 14)) < 4) {
                    int horizoff = 100 - pp.getHorizi();
                    long z1 = mulscale(dist, horizoff, 3) + viewz;

                    int zTop = tsp.getZ();
                    int zBot = zTop;
                    ArtEntry pic = engine.getTile(tsp.getPicnum());
                    int yoffs = pic.getOffsetY();
                    zTop -= (yoffs + pic.getHeight()) * (tsp.getYrepeat() << 2);
                    zBot += -yoffs * (tsp.getYrepeat() << 2);

                    if ((z1 < zBot) && (z1 > zTop)) {
                        if (engine.cansee(viewx, viewy, viewz, psp.getSectnum(), tsp.getX(), tsp.getY(), tsp.getZ(),
                                tsp.getSectnum())) {
                            gPlayerIndex = tu.PlayerP;
                        }
                    }
                }
            }

            //
            // kens original sprite shade code he moved out of the engine
            //

            switch (tsp.getStatnum()) {
                case STAT_ENEMY:
                case STAT_DEAD_ACTOR:
                case STAT_FAF_COPY:
                    break;
                default:
                    int newshade = tsp.getShade();
                    newshade += 6;
                    if (newshade > 127) {
                        newshade = 127;
                    }
                    tsp.setShade((byte) newshade);
            }

            Sector sec = boardService.getSector(tsp.getSectnum());
            if (sec == null) {
                continue;
            }

            int newshade = tsp.getShade();
            if (TEST(sec.getCeilingstat(), CEILING_STAT_PLAX)) {
                newshade += sec.getCeilingshade();
            } else {
                newshade += sec.getFloorshade();
            }
            if (newshade > 127) {
                newshade = 127;
            }
            if (newshade < -128) {
                newshade = -128;
            }
            tsp.setShade(newshade);

            if (tsp.getHitag() == 9998) {
                tsp.setShade(127); // Invisible enemy ninjas
            }

            // Correct shades for luminous sprites
            if (tsp.getHitag() == LUMINOUS) {
                tsp.setShade(-128);
            }

            if (pp.NightVision && TEST(tsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                if (tu != null && tu.ID == TRASHCAN) {
                    continue; // Don't light up trashcan
                }

                tsp.setPal(PALETTE_ILLUMINATE); // Make sprites REALLY bright green.
                tsp.setShade(-128);
            }

            if (tu != null && tu.PlayerP != -1) {
                if (TEST(tu.Flags2, SPR2_VIS_SHADING)) {
                    if (Player[screenpeek].PlayerSprite != SpriteNum) {
                        if (!TEST(Player[tu.PlayerP].Flags, PF_VIEW_FROM_OUTSIDE)) {
                            tsp.setCstat(tsp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));
                        }
                    }

                    tsp.setShade((byte) (12 - STD_RANDOM_RANGE(30)));
                }
            }
        }

        WarpCopySprite();
    }

    private static void show_weapon(USER pp, Sprite tspr) {
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        if (renderedSpriteList.getSize() >= MAXSPRITESONSCREEN) {
            return;
        }

        int WeaponNum = pp.WeaponNum;
        if (viewWeaponTile[WeaponNum] == -1) {
            return;
        }

        Sprite pTEffect = renderedSpriteList.obtain();
        pTEffect.set(tspr);
        pTEffect.setZ(tspr.getZ() - 0x5000);
        pTEffect.setCstat(0);
        pTEffect.setAng(pTEffect.getAng() + 512);
        pTEffect.setAng(pTEffect.getAng() & 0x7FF);
        pTEffect.setPicnum(viewWeaponTile[WeaponNum]);
        pTEffect.setShade(tspr.getShade());
        pTEffect.setXrepeat(32);
        pTEffect.setYrepeat(32);
    }

    public static Sprite get_tsprite(int SpriteNum) {
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        for (int tSpriteNum = renderedSpriteList.getSize() - 1; tSpriteNum >= 0; tSpriteNum--) {
            Sprite tsp = renderedSpriteList.get(tSpriteNum);
            if (tsp.getOwner() == SpriteNum) {
                return tsp;
            }
        }

        return null;
    }

    public static void post_analyzesprites(int smoothratio) {
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        for (int tSpriteNum = renderedSpriteList.getSize() - 1; tSpriteNum >= 0; tSpriteNum--) {
            TSprite tsp = renderedSpriteList.get(tSpriteNum);
            int SpriteNum = tsp.getOwner();
            if (SpriteNum == -1) {
                continue;
            }

            USER tu = getUser(SpriteNum);

            // only interpolate certain moving things
            game.pInt.dospriteinterp(tsp, smoothratio);
            game.pIntSkip2.dospriteinterp(tsp, smoothratio);
            game.pIntSkip4.dospriteinterp(tsp, smoothratio);

            if (tu != null) {
                if (tu.ID == FIREBALL_FLAMES && tu.Attach != -1) {
                    // SPRITEp atsp = &boardService.getSprite(tu.Attach);
                    Sprite atsp;

                    atsp = get_tsprite(tu.Attach);

                    if (atsp == null) {
                        continue;
                    }

                    tsp.setX(atsp.getX());
                    tsp.setY(atsp.getY());
                    // statnum is priority - draw this ALWAYS first at 0
                    // statnum is priority - draw this ALWAYS last at MAXSTATUS
                    if (TEST(atsp.getExtra(), SPRX_BURNABLE)) {
                        atsp.setStatnum(1);
                        tsp.setStatnum(0);
                    } else {
                        tsp.setStatnum(MAXSTATUS);
                    }
                }

                if (tu.PlayerP != -1 && tu.PlayerP != screenpeek) {
                    if (cfg.ShowWeapon && gNet.MultiGameType != MultiGameTypes.MULTI_GAME_NONE) {
                        show_weapon(tu, tsp);
                    }
                }
            }
        }
    }

    public static void BackView(LONGp nx, LONGp ny, LONGp nz, LONGp vsect, LONGp nang, int horiz) {
        PlayerStr pp = Player[screenpeek];
        int ang = (nang.value + pp.view_outside_dang);

        // Calculate the vector (nx,ny,nz) to shoot backwards
        int vx = (EngineUtils.sin(NORM_ANGLE(ang + 1536)) >> 3);
        int vy = (EngineUtils.sin(NORM_ANGLE(ang + 1024)) >> 3);
        int vz = (horiz - 100) * 256;

        // Player sprite of current view
        Sprite sp = pp.getSprite();
        if (sp == null) {
            return;
        }

        int bakcstat = sp.getCstat();
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        // Make sure sector passed to FAFhitscan is correct
        // COVERupdatesector(*nx, *ny, vsect);

        engine.hitscan(nx.value, ny.value, nz.value, vsect.value, vx, vy, vz, pHitInfo, CLIPMASK_PLAYER);

        sp.setCstat(bakcstat); // Restore cstat

        int hx = pHitInfo.hitx - (nx.value);
        int hy = pHitInfo.hity - (ny.value);

        // If something is in the way, make pp.camera_dist lower if necessary
        if (klabs(vx) + klabs(vy) > klabs(hx) + klabs(hy)) {
            Wall hitWall = boardService.getWall(pHitInfo.hitwall);
            if (hitWall != null) // Push you a little bit off the wall
            {
                vsect.value = pHitInfo.hitsect;
                int daang = EngineUtils.getAngle(hitWall.getWall2().getX() - hitWall.getX(),
                        hitWall.getWall2().getY() - hitWall.getY());

                int i = vx * EngineUtils.sin(daang) + vy * EngineUtils.sin(NORM_ANGLE(daang + 1536));
                if (klabs(vx) > klabs(vy)) {
                    hx -= mulscale(vx, i, 28);
                } else {
                    hy -= mulscale(vy, i, 28);
                }
            } else if (pHitInfo.hitsprite == -1) // Push you off the ceiling/floor
            {
                vsect.value = pHitInfo.hitsect;

                if (klabs(vx) > klabs(vy)) {
                    hx -= (vx >> 5);
                } else {
                    hy -= (vy >> 5);
                }
            } else {
                Sprite hsp = boardService.getSprite(pHitInfo.hitsprite);

                // if you hit a sprite that's not a wall sprite - try again
                if (hsp != null && !TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                    int flag_backup = hsp.getCstat();
                    hsp.setCstat(hsp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

                    BackView(nx, ny, nz, vsect, nang, horiz);
                    hsp.setCstat(flag_backup);
                    return;
                } else {
                    // same as wall calculation
                    int daang = NORM_ANGLE(sp.getAng() - 512);

                    int i = vx * EngineUtils.sin(daang) + vy * EngineUtils.sin(NORM_ANGLE(daang + 1536));
                    if (klabs(vx) > klabs(vy)) {
                        hx -= mulscale(vx, i, 28);
                    } else {
                        hy -= mulscale(vy, i, 28);
                    }
                }

            }

            int i;
            if (klabs(vx) > klabs(vy)) {
                i = (hx << 16) / vx;
            } else {
                i = (hy << 16) / vy;
            }

            if (i < pp.camera_dist) {
                pp.camera_dist = i;
            }
        }

        // Actually move you! (Camerdist is 65536 if nothing is in the way)
        nx.value += mulscale(vx, pp.camera_dist, 16);
        ny.value += mulscale(vy, pp.camera_dist, 16);
        nz.value += mulscale(vz, pp.camera_dist, 16);

        // Slowly increase pp.camera_dist until it reaches 65536
        // Synctics is a timer variable so it increases the same rate
        // on all speed computers
        pp.camera_dist = Math.min(pp.camera_dist + (3 << 10), 65536);

        // Make sure vsect is correct
        vsect.value = engine.updatesectorz(nx.value, ny.value, nz.value, vsect.value);

        nang.value = ang;
    }

    public static void PrintLocationInfo(PlayerStr pp) {
        Renderer renderer = game.getRenderer();
        int windowx1 = 0;
        int windowy1 = 0;

        int x = windowx1 + 5;
        int y = windowy1 + 5;

        if (LocationInfo != 0) {
            BitmapFont smallFont = EngineUtils.getSmallFont();
            buildString(txt_buffer, 0, "FPS: ", Gdx.graphics.getFramesPerSecond());
            smallFont.drawText(renderer, x, y, txt_buffer, 1.0f, 0, 1, TextAlign.Left, Transparent.None, false);

            if (LocationInfo > 1) {
                y += 7;

                buildString(txt_buffer, 0, "POSX: ", pp.posx);
                smallFont.drawText(renderer, x, y, txt_buffer, 1.0f, 0, 1, TextAlign.Left, Transparent.None, false);

                y += 7;
                buildString(txt_buffer, 0, "POSY: ", pp.posy);
                smallFont.drawText(renderer, x, y, txt_buffer, 1.0f, 0, 1, TextAlign.Left, Transparent.None, false);

                y += 7;
                buildString(txt_buffer, 0, "POSZ: ", pp.posz);
                smallFont.drawText(renderer, x, y, txt_buffer, 1.0f, 0, 1, TextAlign.Left, Transparent.None, false);

                y += 7;
                buildString(txt_buffer, 0, "ANG: ", pp.getAnglei());
                smallFont.drawText(renderer, x, y, txt_buffer, 1.0f, 0, 1, TextAlign.Left, Transparent.None, false);
            }
        }
    }

    public static void SecretInfo(int zoom) {
        Renderer renderer = game.getRenderer();
        int x = 5;
        int y = 150;
        if (cfg.BorderNum <= BORDER_BAR - 2) {
            y = 170;
        }

        if (cfg.Stats == 0 || cfg.BorderNum == 0) {
            return;
        }

        boolean shadows = true;
        float viewzoom = (zoom / 65536.0f);
        Font f = game.getFont(0);

        buildString(txt_buffer, 0, "Kills ");
        int alignx = f.getWidth(txt_buffer, viewzoom);

        int yoffset = (int) (3.2f * f.getSize() * viewzoom);
        y -= yoffset;

        if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT) {
            y += (int) (8 * viewzoom);
        }

        int staty = y;

        f.drawTextScaled(renderer, x, staty, txt_buffer, viewzoom, 0, PAL_XLAT_BRIGHT_GREEN, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, shadows);

        int offs = Bitoa(gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT ? Player[myconnectindex].Kills : Kills,
                txt_buffer);

        if (gNet.MultiGameType != MultiGameTypes.MULTI_GAME_COMMBAT) {
            buildString(txt_buffer, offs, "/", TotalKillable);
        }
        f.drawTextScaled(renderer, (int) (x + (alignx + 2) * viewzoom), staty, txt_buffer, viewzoom, 0, PAL_XLAT_BROWN, TextAlign.Left,
                Transparent.None, ConvertType.AlignLeft, shadows);

        staty = y + (int) (8 * viewzoom);
        if (gNet.MultiGameType != MultiGameTypes.MULTI_GAME_COMMBAT) {
            buildString(txt_buffer, 0, "Secrets");
            f.drawTextScaled(renderer, x, staty, txt_buffer, viewzoom, 0, PAL_XLAT_BRIGHT_GREEN, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, shadows);
            alignx = f.getWidth(txt_buffer, viewzoom);
            offs = Bitoa(Player[connecthead].SecretsFound, txt_buffer);
            buildString(txt_buffer, offs, " / ", LevelSecrets);
            f.drawTextScaled(renderer, x + (alignx + 2), staty, txt_buffer, viewzoom, 0, PAL_XLAT_BROWN, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, shadows);

            staty = y + (int) (16 * viewzoom);
        }

        buildString(txt_buffer, 0, "Time ");
        f.drawTextScaled(renderer, x, staty, txt_buffer, viewzoom, 0, PAL_XLAT_BRIGHT_GREEN, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, shadows);
        alignx = f.getWidth(txt_buffer, viewzoom);

        int second_tics = (PlayClock / 120);
        int minutes = (second_tics / 60);
        int sec = (second_tics % 60);

        offs = Bitoa(minutes, txt_buffer, 2);
        buildString(txt_buffer, offs, ":", sec, 2);
        f.drawTextScaled(renderer, x + (alignx + 2), staty, txt_buffer, viewzoom, 0, PAL_XLAT_BROWN, TextAlign.Left,
                Transparent.None, ConvertType.AlignLeft, shadows);
    }

    public static void DrawMessageInput() {
        if (Console.out.isShowing()) {
            return;
        }

        if (MessageInputMode) {
            if (game.getProcessor().getWangPrompt().isCaptured()) {
                game.getProcessor().getWangPrompt().draw();
            }
        }
    }

    public static void DrawCrosshair(PlayerStr pp) {
        Renderer renderer = game.getRenderer();
        if (!cfg.Crosshair) {
            return;
        }

        if (TEST(pp.Flags, PF_VIEW_FROM_OUTSIDE)) {
            return;
        }

        if (dimensionmode == 6) {
            return;
        }

        int y = 100;
        if (cfg.BorderNum >= BORDER_BAR) {
            y -= 24;
        }

        y += (klabs(pp.lookang) / 9);

        renderer.rotatesprite(158 - (pp.lookang >> 1) << 16, y << 16, cfg.CrosshairSize, 0, 2326, 10, 0, ROTATE_SPRITE_SCREEN_CLIP);
    }

    public static void CameraView(PlayerStr pp, LONGp tx, LONGp ty, LONGp tz, LONGp tsectnum, LONGp tang,
                                  LONGp thoriz) {

        boolean found_camera = false;
        boolean player_in_camera = false;
        Sprite psp = pp.getSprite();
        if (psp == null) {
            return;
        }

        if (pp == Player[screenpeek]) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_DEMO_CAMERA); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite sp = node.get();

                ang = EngineUtils.getAngle(tx.value - sp.getX(), ty.value - sp.getY());
                boolean ang_test = GetDeltaAngle(sp.getAng(), ang) < sp.getLotag();

                boolean FAFcansee_test = (FAFcansee(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), tx.value, ty.value, tz.value, pp.cursectnum)
                        || FAFcansee(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), tx.value, ty.value,
                        tz.value + SPRITEp_SIZE_Z(psp), pp.cursectnum));

                player_in_camera = ang_test && FAFcansee_test;

                if (player_in_camera || pp.camera_check_time_delay > 0) {

                    // if your not in the camera but are still looking
                    // make sure that only the last camera shows you

                    if (!player_in_camera) {
                        if (pp.last_camera_sp != i) {
                            continue;
                        }
                    }

                    pp.last_camera_sp = i;
//                    if (sp.getClipdist() == 1) {
//                        CircleCamera(tx, ty, tz, tsectnum, tang, 100);
//                    } else {
                        int xvect, yvect, zvect, zdiff;

                        xvect = EngineUtils.sin(NORM_ANGLE(ang + 512)) >> 3;
                        yvect = EngineUtils.sin(NORM_ANGLE(ang)) >> 3;

                        zdiff = sp.getZ() - tz.value;
                        if (klabs(sp.getX() - tx.value) > 1000) {
                            zvect = scale(xvect, zdiff, sp.getX() - tx.value);
                        } else if (klabs(sp.getY() - ty.value) > 1000) {
                            zvect = scale(yvect, zdiff, sp.getY() - ty.value);
                        } else if (sp.getX() - tx.value != 0) {
                            zvect = scale(xvect, zdiff, sp.getX() - tx.value);
                        } else if (sp.getY() - ty.value != 0) {
                            zvect = scale(yvect, zdiff, sp.getY() - ty.value);
                        } else {
                            zvect = 0;
                        }

                        // new horiz to player
                        thoriz.value = 100 - (zvect / 256);
                        thoriz.value = Math.max(thoriz.value, PLAYER_HORIZ_MIN);
                        thoriz.value = Math.min(thoriz.value, PLAYER_HORIZ_MAX);

                        tang.value = ang;
                        tx.value = sp.getX();
                        ty.value = sp.getY();
                        tz.value = sp.getZ();
                        tsectnum.value = sp.getSectnum();

//                    }
                    found_camera = true;
                }

                if (found_camera) {
                    break;
                }
            }
        }

        // if you player_in_camera you definately have a camera
        if (player_in_camera) {
            pp.camera_check_time_delay = 120 / 2;
            pp.Flags |= (PF_VIEW_FROM_CAMERA);
        } else
        // if you !player_in_camera you still might have a camera
        // for a split second
        {
            if (found_camera) {
                pp.Flags |= (PF_VIEW_FROM_CAMERA);
            } else {
                pp.circle_camera_ang = 0;
                pp.circle_camera_dist = CIRCLE_CAMERA_DIST_MIN;
                pp.Flags &= ~(PF_VIEW_FROM_CAMERA);
            }
        }
    }

    public static void PreDraw() {
//        PreDrawStackedWater();

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FLOOR_SLOPE_DONT_DRAW); node != null; node = node.getNext()) {
            Sprite sp = node.get();
            Sector sec = boardService.getSector(sp.getSectnum());
            if (sec != null) {
                sec.setFloorstat(sec.getFloorstat() & ~(FLOOR_STAT_SLOPE));
            }
        }
    }

    public static void PostDraw() {
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FLOOR_SLOPE_DONT_DRAW); node != null; node = node.getNext()) {
            Sprite sp = node.get();
            Sector sec = boardService.getSector(sp.getSectnum());
            if (sec != null) {
                sec.setFloorstat(sec.getFloorstat() | (FLOOR_STAT_SLOPE));
            }
        }
    }

    public static TSprite CopySprite(Sprite sprite, int newsector) {
        TSprite tspr = insertTSprite(newsector, sprite.getStatnum()); // STAT_FAF_COPY

        tspr.setX(sprite.getX());
        tspr.setY(sprite.getY());
        tspr.setZ(sprite.getZ());
        tspr.setCstat(sprite.getCstat());
        tspr.setPicnum(sprite.getPicnum());
        tspr.setPal(sprite.getPal());
        tspr.setXrepeat(sprite.getXrepeat());
        tspr.setYrepeat(sprite.getYrepeat());
        tspr.setXoffset(sprite.getXoffset());
        tspr.setYoffset(sprite.getYoffset());
        tspr.setAng(sprite.getAng());
        tspr.setXvel(sprite.getXvel());
        tspr.setYvel(sprite.getYvel());
        tspr.setZvel(sprite.getZvel());
        tspr.setShade(sprite.getShade());
        tspr.setOwner(sprite.getOwner());

        tspr.setCstat(tspr.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        return tspr;
    }

    public static TSprite ConnectCopySprite(Sprite sprite) {
        int spriteSectnum = sprite.getSectnum();
        Sector sec = boardService.getSector(spriteSectnum);
        if (FAF_ConnectCeiling(sec)) {
            int newsector = spriteSectnum;
            int testz = SPRITEp_TOS(sprite) - Z(10);

            if (testz < sec.getCeilingz()) {
                newsector = engine.updatesectorz(sprite.getX(), sprite.getY(), testz, newsector);
            }

            if (newsector != -1 && newsector != sprite.getSectnum()) {
                return CopySprite(sprite, newsector);
            }
        }

        if (FAF_ConnectFloor(sec)) {
            int newsector = spriteSectnum;
            int testz = SPRITEp_BOS(sprite) + Z(10);

            if (testz > sec.getFloorz()) {
                newsector = engine.updatesectorz(sprite.getX(), sprite.getY(), testz, newsector);
            }

            if (newsector >= 0 && newsector != sprite.getSectnum()) {
                return CopySprite(sprite, newsector);
            }
        }

        return null;
    }

    public static void PreDrawStackedWater() {
        ListNode<Sprite> nexti, snexti;
        for (ListNode<Sprite> n = boardService.getStatNode(STAT_CEILING_FLOOR_PIC_OVERRIDE); n != null; n = snexti) {
            snexti = n.getNext();
            for (ListNode<Sprite> node = boardService.getSectNode(n.get().getSectnum()); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite spr = node.get();
                if (getUser(i) != null) {
                    if (spr.getStatnum() <= STAT_DEFAULT || spr.getStatnum() > STAT_PLAYER0 + MAX_SW_PLAYERS) {
                        continue;
                    }

                    if (spr.getStatnum() == STAT_ITEM) {
                        continue;
                    }

                    TSprite tsp = ConnectCopySprite(spr);
                    if (tsp != null) {
                        tsp.setOwner(i);
                    }
                }
            }
        }
    }

    public static void FAF_DrawRooms(int x, int y, int z, float ang, float horiz, int sectnum) {
        Renderer renderer = game.getRenderer();
        ListNode<Sprite> nexti;

//        if (gDemoScreen.demfile == null) {
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_CEILING_FLOOR_PIC_OVERRIDE); node != null; node = nexti) {
                nexti = node.getNext();
                Sprite sp = node.get();
                ru.m210projects.Build.Types.Sector sec = boardService.getSector(sp.getSectnum());
                if (sec == null) {
                    continue;
                }

                if (SP_TAG3(sp) == 0) {
                    // back up ceilingpicnum and ceilingstat
                    sp.setXvel(sec.getCeilingpicnum());
                    sp.setAng(sec.getCeilingstat());
                    sec.setCeilingpicnum(SP_TAG2(sp));
                    sec.setCeilingstat(sec.getCeilingstat() | (SP_TAG6(sp)));
                    sec.setCeilingstat(sec.getCeilingstat() & ~(CEILING_STAT_PLAX));
                } else if (SP_TAG3(sp) == 1) {
                    sp.setXvel(sec.getFloorpicnum());
                    sp.setAng(sec.getFloorstat());
                    sec.setFloorpicnum(SP_TAG2(sp));
                    sec.setFloorstat(sec.getFloorstat() | (SP_TAG6(sp)));
                    sec.setFloorstat(sec.getFloorstat() & ~(FLOOR_STAT_PLAX));
                }
            }
//        }

        renderer.drawrooms(x, y, z, ang, horiz, sectnum);

//        if (gDemoScreen.demfile == null) {
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_CEILING_FLOOR_PIC_OVERRIDE); node != null; node = nexti) {
                nexti = node.getNext();
                Sprite sp = node.get();
                ru.m210projects.Build.Types.Sector sec = boardService.getSector(sp.getSectnum());
                if (sec == null) {
                    continue;
                }

                // manually set gotpic
                if (TEST_GOTSECTOR(sp.getSectnum())) {
                    SET_GOTPIC(FAF_MIRROR_PIC);
                }

                if (SP_TAG3(sp) == 0) {
                    // restore ceilingpicnum and ceilingstat
                    sec.setCeilingpicnum(SP_TAG5(sp));
                    sec.setCeilingstat(SP_TAG4(sp));
                    sec.setCeilingstat(sec.getCeilingstat() & ~(CEILING_STAT_PLAX));
                } else if (SP_TAG3(sp) == 1) {
                    sec.setFloorpicnum(SP_TAG5(sp));
                    sec.setFloorstat(SP_TAG4(sp));
                    sec.setFloorstat(sec.getFloorstat() & ~(FLOOR_STAT_PLAX));
                }
            }
//        }
    }

    public static void drawscreen(PlayerStr pp, int smoothratio) {
        int bob_amt = 0;
        int windowy1 = 0;

        gPlayerIndex = -1;
        PlayerStr camerapp; // prediction player if prediction is on, else regular player

        Renderer renderer = game.getRenderer();

        PreDraw();

        // TENSW: when rendering with prediction, the only thing that counts should
        // be the predicted player.
        if (PredictionOn && numplayers > 1 && pp == Player[myconnectindex]) {
            camerapp = gNet.ppp;
        } else {
            camerapp = pp;
        }

        int tx = camerapp.oposx + mulscale(camerapp.posx - camerapp.oposx, smoothratio, 16);
        int ty = camerapp.oposy + mulscale(camerapp.posy - camerapp.oposy, smoothratio, 16);
        int tz = camerapp.oposz + mulscale(camerapp.posz - camerapp.oposz, smoothratio, 16);
        float tang = camerapp.oang + (BClampAngle(camerapp.pang + 1024 - camerapp.oang) - 1024) * smoothratio / 65536.0f;
        float thoriz = (camerapp.ohoriz + ((camerapp.horiz - camerapp.ohoriz) * smoothratio) / 65536.0f);
        int tsectnum = camerapp.cursectnum;

        tsectnum = COVERupdatesector(tx, ty, tsectnum);
        tang += camerapp.lookang;

        if (tsectnum == -1) {
            // if we hit an invalid sector move to the last valid position for drawing
            tsectnum = lv_sectnum;
            tx = lv_x;
            ty = lv_y;
            tz = lv_z;
        } else {
            // last valid stuff
            lv_sectnum = tsectnum;
            lv_x = tx;
            lv_y = ty;
            lv_z = tz;
        }

        pp.six = tx;
        pp.siy = ty;
        pp.siz = tz - pp.posz;
        pp.siang = (int) tang;

        QuakeViewChange(camerapp, tmp_ptr[0], tmp_ptr[1], tmp_ptr[2], tmp_ptr[3]);
        int quake_z = tmp_ptr[0].value;
        int quake_x = tmp_ptr[1].value;
        int quake_y = tmp_ptr[2].value;
        int quake_ang = tmp_ptr[3].value;

        VisViewChange(camerapp, tmp_ptr[0].set(visibility));
        visibility = tmp_ptr[0].value;

        tz = tz + quake_z;
        tx = tx + quake_x;
        ty = ty + quake_y;
        tang += quake_ang;
        tang = BClampAngle(tang);

        if (pp.sop_remote != -1) {
            Sprite remoteSpr = boardService.getSprite(pp.remote_sprite);
            if (remoteSpr != null) {
                if (TEST_BOOL1(remoteSpr)) {
                    tang = remoteSpr.getAng();
                } else {
                    int xmid = pp.remote.oposx + mulscale(pp.remote.posx - pp.remote.oposx, smoothratio, 16);
                    int ymid = pp.remote.oposy + mulscale(pp.remote.posy - pp.remote.oposy, smoothratio, 16);
                    tang = EngineUtils.getAngle(xmid - tx, ymid - ty);
                }
            }
        }

        if (TEST(pp.Flags, PF_VIEW_FROM_OUTSIDE)) {
            BackView(tmp_ptr[0].set(tx), tmp_ptr[1].set(ty), tmp_ptr[2].set(tz), tmp_ptr[3].set(tsectnum),
                    tmp_ptr[4].set((int) tang), (int) thoriz);

            tx = tmp_ptr[0].value;
            ty = tmp_ptr[1].value;
            tz = tmp_ptr[2].value;
            tsectnum = tmp_ptr[3].value;
            tang = tmp_ptr[4].value;
        } else {
            bob_amt = camerapp.obob_amt + mulscale(camerapp.bob_amt - camerapp.obob_amt, smoothratio, 16);

            if (gDemoScreen.demfile != null) {
                CameraView(camerapp, tmp_ptr[0].set(tx), tmp_ptr[1].set(ty), tmp_ptr[2].set(tz),
                        tmp_ptr[3].set(tsectnum), tmp_ptr[4].set((int) tang), tmp_ptr[5].set((int) thoriz));

                tx = tmp_ptr[0].value;
                ty = tmp_ptr[1].value;
                tz = tmp_ptr[2].value;
                tsectnum = tmp_ptr[3].value;
                tang = tmp_ptr[4].value;
                thoriz = tmp_ptr[5].value;
            }
        }

        if (!TEST(pp.Flags, PF_VIEW_FROM_CAMERA | PF_VIEW_FROM_OUTSIDE)) {
            tz += bob_amt;
            tz += camerapp.obob_z + mulscale(camerapp.bob_z - camerapp.obob_z, smoothratio, 16);

            // recoil only when not in camera
            thoriz = thoriz + pp.recoil_horizoff;
            thoriz = Math.max(thoriz, PLAYER_HORIZ_MIN);
            thoriz = Math.min(thoriz, PLAYER_HORIZ_MAX);
        }

        if (tsectnum >= 0) {
            engine.getzsofslope(tsectnum, tx, ty, fz, cz);
            if (tz < cz.get() + (4 << 8)) {
                tz = cz.get() + (4 << 8);
            }
            if (tz > fz.get() - (4 << 8)) {
                tz = fz.get() - (4 << 8);
            }
        }

        if (dimensionmode != 6) {
            OverlapDraw = true;
            DrawOverlapRoom(tx, ty, tz, tang, thoriz, tsectnum, smoothratio);
            OverlapDraw = false;

            if (dimensionmode != 6 && !game.isScreenSaving()) {
                // TEST this! Changed to camerapp
                JS_DrawMirrors(pp, tx, ty, tz, tang, thoriz, smoothratio);
            }

            {
                FAF_DrawRooms(tx, ty, tz, tang, thoriz, tsectnum);
                PicInView(FAF_MIRROR_PIC, false);
            }

            analyzesprites(tx, ty, tz, false, smoothratio);
            post_analyzesprites(smoothratio);
            renderer.drawmasks();

            // Only animate lava if its picnum is on screen
            // gotpic is a bit array where the tile number's bit is set
            // whenever it is drawn (ceilings, walls, sprites, etc.)
            byte[] gotpic = renderer.getRenderedPics();
            if ((gotpic[SLIME >> 3] & (1 << (SLIME & 7))) > 0) {
                gotpic[SLIME >> 3] &= ~(1 << (SLIME & 7));
            }
        }

        Sector psec = boardService.getSector(pp.cursectnum);
        if (psec != null) {
            show2dsector.setBit(pp.cursectnum);
            for (ListNode<Wall> wn = psec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                Sector nsec = boardService.getSector(wal.getNextsector());
                if (nsec == null) {
                    continue;
                }

                if ((wal.getCstat() & 0x0071) != 0) {
                    continue;
                }

                Wall nwall = boardService.getWall(wal.getNextwall());
                if (nwall != null && (nwall.getCstat() & 0x0071) != 0) {
                    continue;
                }

                if (nsec.getLotag() == 32767) {
                    continue;
                }

                if (nsec.getCeilingz() >= nsec.getFloorz()) {
                    continue;
                }

                show2dsector.setBit(wal.getNextsector());
            }
        }

        if ((dimensionmode == 5 || dimensionmode == 6)) {
            tx = camerapp.oposx;
            ty = camerapp.oposy;
            if (ScrollMode2D) {
                tx = Follow_posx;
                ty = Follow_posy;
            }

            // FIXME this shit
            java.util.List<Sprite> spriteList = boardService.getBoard().getSprites();
            for (Sprite spr : spriteList) {
                // Don't show sprites tagged with 257
                if (spr.getLotag() == 257) {
                    if (TEST(spr.getCstat(), CSTAT_SPRITE_FLOOR)) {
                        spr.setCstat(spr.getCstat() & ~(CSTAT_SPRITE_FLOOR));
                        spr.setOwner(-2);
                    }
                }
            }

            if (dimensionmode == 6) {
                renderer.clearview(0);
                renderer.drawmapview(tx, ty, zoom, (int) tang);
            }

            // Draw the line map on top of texture 2d map or just stand alone
            renderer.drawoverheadmap(tx, ty, zoom, (short) tang);

            int txt_x = 7, txt_y = windowy1 + 5;
            if (game.nNetMode != NetMode.Single) {
                txt_y += 10;
            }
            if (ScrollMode2D) {
                minigametext(txt_x, txt_y + 7, "Follow Mode", 0, Transparent.None, ConvertType.AlignLeft);
            }
            game.getFont(0).drawTextScaled(renderer, txt_x, txt_y, getMapName(Level), 1.0f, 0, 4, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, true);
        }

        // FIXME this shit
        java.util.List<Sprite> spriteList = boardService.getBoard().getSprites();
        for (Sprite spr : spriteList) {
            // Don't show sprites tagged with 257
            if (spr.getLotag() == 257 && spr.getOwner() == -2) {
                spr.setCstat(spr.getCstat() | (CSTAT_SPRITE_FLOOR));
            }
        }

        PostDraw();
    }

    public static void drawhud(PlayerStr pp) {
        Renderer renderer = game.getRenderer();

        int windowy1 = 0;

        PreUpdatePanel();
        UpdatePanel();

        PrintLocationInfo(pp);

        if (cfg.Stats == 1 || (cfg.Stats == 2 && (dimensionmode == 5 || dimensionmode == 6))) {
            SecretInfo(cfg.gStatSize);
        }

        DrawCrosshair(pp);

        operatefta(); // Draw all the user quotes in the quote array

        DoPlayerDiveMeter(pp); // Do the underwater breathing bar

        // Boss Health Meter, if Boss present
        BossHealthMeter();

        DrawMessageInput(); // This is only used for non-multiplayer input now

        DrawFullBar(pp);

        UpdateAltBar(pp);

        UpdateMiniBar(pp);

        if (game.gPaused) {
            int w = game.getFont(1).getWidth(MSG_GAME_PAUSED, 1.0f);
            gametext(TEXT_TEST_COL(w), 100, MSG_GAME_PAUSED, 0);
        }

        if (game.nNetMode == NetMode.Multiplayer) {
            DrawFragBar();
        }

        int ydim = renderer.getHeight();
        if (gPlayerIndex != -1 && gPlayerIndex != screenpeek) {
            USER pu = getUser(Player[gPlayerIndex].PlayerSprite);
            if (pu != null) {
                int len = buildString(txt_buffer, 0, Player[gPlayerIndex].getName());
                len = buildString(txt_buffer, len, " (", pu.Health);
                buildString(txt_buffer, len, "hp)");

                int shade = 16 - (engine.getTotalClock() & 0x3F);

                int y = scale(windowy1, 200, ydim) + 100;
                if (cfg.BorderNum < BORDER_BAR) {
                    y += engine.getTile(STATUS_BAR).getHeight() / 2;
                }

                game.getFont(0).drawTextScaled(renderer, 160, y, txt_buffer, 1.0f, shade, pu.spal,
                        TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            }
        }

        if (screenpeek != myconnectindex) {
            buildString(txt_buffer, 0, "View from \"", Player[screenpeek].getName(), "\"");
            int shade = 16 - (engine.getTotalClock() & 0x3F);

            game.getFont(1).drawTextScaled(renderer, 160, scale(windowy1, 200, ydim) + 30, txt_buffer, 1.0f, shade, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        if (game.isCurrentScreen(gGameScreen) && engine.getTotalClock() < gNameShowTime) {
            Transparent transp = Transparent.None;
            if (engine.getTotalClock() > gNameShowTime - 20) {
                transp = Transparent.Bit1;
            }
            if (engine.getTotalClock() > gNameShowTime - 10) {
                transp = Transparent.Bit2;
            }

            if (cfg.showMapInfo != 0 && !game.menu.gShowMenu) {
                game.getFont(2).drawTextScaled(renderer, 160, 110 + 16 + 8, getMapName(Level), 1.0f, -128, 0, TextAlign.Center,
                        transp, ConvertType.Normal, false);
            }
        }
    }

    private static void DrawFullBar(PlayerStr pp) {
        if (cfg.BorderNum != BORDER_BAR) {
            return;
        }
        Renderer renderer = game.getRenderer();
        ArtEntry pic = renderer.getTile(53);
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        int scale = divscale(ydim, 4L * pic.getWidth(), 16);
        int width = mulscale(pic.getWidth(), scale, 16);

        int framesnum = xdim / width;
        pic = renderer.getTile(STATUS_BAR);
        int fy = ydim - scale(pic.getHeight(), ydim, 200);

        int statusx1 = coordsConvertXScaled(160 - pic.getWidth() / 2, ConvertType.Normal);
        int statusx2 = coordsConvertXScaled(160 + pic.getWidth() / 2, ConvertType.Normal);

        int x = 0;
        for (int i = 0; i <= framesnum; i++) {
            if (x - width <= statusx1 || x + width >= statusx2) {
                renderer.rotatesprite(x << 16, fy << 16, scale, 0, 53, 0, 0, 8 | 16 | 256);
            }
            x += width;
        }
        renderer.rotatesprite(160 << 16, (200 - pic.getHeight() / 2) << 16, 1 << 16, 0, STATUS_BAR, 0, 0, 10, 0, 0,
                xdim - 1, ydim - 1);

        DrawCompass(pp);

        PlayerUpdatePanelInfo(pp);
    }

    public static void UpdateAltBar(PlayerStr pp) {
        if (cfg.BorderNum != BORDER_ALTBAR) {
            return;
        }

        Renderer renderer = game.getRenderer();
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        INVENTORY_DATA id;
        int x = 187;
        int y = 155;

        renderer.rotatesprite(x << 16, y << 16, (1 << 16), 0, ALTHUDRIGHT, 0, 0,
                ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | 512);

        if (pp.InventoryAmount[pp.InventoryNum] != 0) {
            id = InventoryData[pp.InventoryNum];
            // Inventory pic
            renderer.rotatesprite(x + 45 << 16, y + 17 << 16, (1 << 16), 0, id.State.picndx, 0, 0,
                    ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | 512);
            DrawInventory(pp, x + 43, y + 16, 512);
        }

        if (u.WeaponNum != WPN_SWORD && u.WeaponNum != WPN_FIST) {
            DisplayPanelNumber(pp, x + 11, y + 20, pp.WpnAmmo[u.WeaponNum], 512);
        }

        if (gNet.MultiGameType != MultiGameTypes.MULTI_GAME_COMMBAT) {
            PlayerUpdateKeys(pp, x + 89, y + 18, 512);
        } else {
            if (gNet.TimeLimit != 0) {
                int seconds = gNet.TimeLimitClock / 120;
                int offs = Bitoa(seconds / 60, txt_buffer, 2);
                buildString(txt_buffer, offs, ":", seconds % 60, 2);
                DisplaySummaryString(pp, x + 92, y + 23, 0, 0, txt_buffer, 512);
            }
        }

        x = 0;
        renderer.rotatesprite(x, y << 16, (1 << 16), 0, ALTHUDLEFT, 0, 0,
                ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | 256);

        DisplayMiniBarNumber(pp, x + 23, y + 20, u.Health, 256);

        if (pp.Armor != 0) {
            DisplayMiniBarNumber(pp, x + 59, y + 20, pp.Armor, 256);
        }
    }

    public static void UpdateMiniBar(PlayerStr pp) {
        if (cfg.BorderNum != BORDER_MINI_BAR) {
            return;
        }

        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        Renderer renderer = game.getRenderer();
        INVENTORY_DATA id;

        int x = MINI_BAR_HEALTH_BOX_X;
        int y = 200 - 26;

        renderer.rotatesprite(x << 16, y << 16, (1 << 16), 0, MINI_BAR_HEALTH_BOX_PIC, 0, 0,
                ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | 256);

        DisplayMiniBarNumber(pp, x + 3, y + 5, u.Health, 256);

        if (pp.Armor != 0) {
            x += 28;
            renderer.rotatesprite(x << 16, y << 16, (1 << 16), 0, MINI_BAR_AMMO_BOX_PIC, 0, 0,
                    ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | 256);

            DisplayMiniBarNumber(pp, x + 3, y + 5, pp.Armor, 256);
        }

        if (u.WeaponNum != WPN_SWORD && u.WeaponNum != WPN_FIST) {
            x += 28;
            renderer.rotatesprite(x << 16, y << 16, (1 << 16), 0, MINI_BAR_AMMO_BOX_PIC, 0, 0,
                    ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | 256);

            DisplayMiniBarNumber(pp, x + 3, y + 5, pp.WpnAmmo[u.WeaponNum], 256);
        }

        if (gNet.MultiGameType != MultiGameTypes.MULTI_GAME_COMMBAT) {
            boolean hasKey = false;
            for (int i = 0; i < NUM_KEYS; i++) {
                if (pp.HasKey[i] != 0) {
                    hasKey = true;
                    break;
                }
            }

            if (hasKey) {
                x += 28;
                renderer.rotatesprite(x << 16, y << 16, (1 << 16), 0, MINI_BAR_AMMO_BOX_PIC, 0, 0,
                        ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | 256);
                PlayerUpdateKeys(pp, x + 2, y + 3, 256);
            }
        } else if (gNet.TimeLimit != 0) {
            x += 28;
            renderer.rotatesprite(x << 16, y << 16, (1 << 16), 0, MINI_BAR_AMMO_BOX_PIC, 0, 0,
                    ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | 256);

            int seconds = gNet.TimeLimitClock / 120;
            int offs = Bitoa(seconds / 60, txt_buffer, 2);
            buildString(txt_buffer, offs, ":", seconds % 60, 2);
            DisplaySummaryString(pp, x + 5, y + 8, 0, 0, txt_buffer, 0);
        }

        if (pp.InventoryAmount[pp.InventoryNum] != 0) {
            // Inventory Box
            x += 28;

            renderer.rotatesprite(x << 16, y << 16, (1 << 16), 0, MINI_BAR_INVENTORY_BOX_PIC, 0, 0,
                    ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | 256);

            id = InventoryData[pp.InventoryNum];

            // Inventory pic
            renderer.rotatesprite(x + 2 << 16, y + 3 << 16, (1 << 16), 0, id.State.picndx, 0, 0,
                    ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | 256);

            // will update the AUTO and % inventory values
            DrawInventory(pp, x, y + 1, 256);
        }
    }

    public static int NORM_CANG(int ang) {
        return (((ang) + 32) & 31);
    }

    public static void DrawCompass(PlayerStr pp) {
        if (cfg.BorderNum < BORDER_BAR || pp != Player[screenpeek]) {
            return;
        }

        int ang = pp.getAnglei();
        if (pp.sop_remote != -1) {
            ang = 0;
        }

        Renderer renderer = game.getRenderer();
        int start_ang = ((ang + 32) >> 6);
        start_ang = NORM_CANG(start_ang - 4);

        int flags = ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER;

        int x_size = engine.getTile(COMPASS_NORTH).getWidth();
        for (int i = 0, x = COMPASS_X; i < 10; i++) {
            renderer.rotatesprite(x << 16, COMPASS_Y << 16, (1 << 16), 0, CompassPic[NORM_CANG(start_ang + i)],
                    CompassShade[i], 0, flags);
            x += x_size;
        }
    }

    public static TSprite insertTSprite(int sectnum, int stat) {
        Renderer renderer = game.getRenderer();
        TSprite pTSprite = renderer.getRenderedSprites().obtain();
        pTSprite.reset((byte) 0);

        pTSprite.setOwner(-1);
        pTSprite.setSectnum(sectnum);
        pTSprite.setStatnum(stat);

        return pTSprite;
    }

}
