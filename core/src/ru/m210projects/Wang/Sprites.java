package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Factory.WangSprite;
import ru.m210projects.Wang.Type.Anim.AnimType;
import ru.m210projects.Wang.Type.*;

import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.isValidTile;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Actor.DoActorDebris;
import static ru.m210projects.Wang.Ai.DoActorPickClosePlayer;
import static ru.m210projects.Wang.Break.SetupSpriteForBreak;
import static ru.m210projects.Wang.Cheats.InfinityAmmo;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Bunny.BunnyHatch2;
import static ru.m210projects.Wang.Enemies.Bunny.SetupBunny;
import static ru.m210projects.Wang.Enemies.Coolg.SetupCoolg;
import static ru.m210projects.Wang.Enemies.Coolie.SetupCoolie;
import static ru.m210projects.Wang.Enemies.Eel.SetupEel;
import static ru.m210projects.Wang.Enemies.GirlNinj.SetupGirlNinja;
import static ru.m210projects.Wang.Enemies.Goro.SetupGoro;
import static ru.m210projects.Wang.Enemies.Hornet.SetupHornet;
import static ru.m210projects.Wang.Enemies.Lava.SetupLava;
import static ru.m210projects.Wang.Enemies.Ninja.SetupNinja;
import static ru.m210projects.Wang.Enemies.Ripper.SetupRipper;
import static ru.m210projects.Wang.Enemies.Ripper2.SetupRipper2;
import static ru.m210projects.Wang.Enemies.Serp.SetupSerp;
import static ru.m210projects.Wang.Enemies.Skel.SetupSkel;
import static ru.m210projects.Wang.Enemies.Skull.SetupBetty;
import static ru.m210projects.Wang.Enemies.Skull.SetupSkull;
import static ru.m210projects.Wang.Enemies.Sumo.SetupSumo;
import static ru.m210projects.Wang.Enemies.Zilla.SetupZilla;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Inv.*;
import static ru.m210projects.Wang.JPlayer.adduserquote;
import static ru.m210projects.Wang.JSector.JS_SpriteSetup;
import static ru.m210projects.Wang.JTags.LUMINOUS;
import static ru.m210projects.Wang.JTags.TAG_NORESPAWN_FLAG;
import static ru.m210projects.Wang.JWeapon.s_CarryFlag;
import static ru.m210projects.Wang.JWeapon.s_CarryFlagNoDet;
import static ru.m210projects.Wang.Light.LIGHT_ShadeInc;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.MiscActr.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.*;
import static ru.m210projects.Wang.Panel.*;
import static ru.m210projects.Wang.Player.DoSpawnTeleporterEffectPlace;
import static ru.m210projects.Wang.Rooms.*;
import static ru.m210projects.Wang.Rotator.DoRotator;
import static ru.m210projects.Wang.Rotator.SetRotatorActive;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Slidor.*;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Spike.*;
import static ru.m210projects.Wang.Stag.*;
import static ru.m210projects.Wang.Tags.*;
import static ru.m210projects.Wang.Text.PutStringInfo;
import static ru.m210projects.Wang.Track.MAX_TRACKS;
import static ru.m210projects.Wang.Track.Track;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Vator.*;
import static ru.m210projects.Wang.Warp.*;
import static ru.m210projects.Wang.Weapon.*;
import static ru.m210projects.Wang.Weapons.Grenade.InitWeaponGrenade;
import static ru.m210projects.Wang.Weapons.Heart.InitWeaponHeart;
import static ru.m210projects.Wang.Weapons.HotHead.InitWeaponHothead;
import static ru.m210projects.Wang.Weapons.Micro.*;
import static ru.m210projects.Wang.Weapons.Mine.InitWeaponMine;
import static ru.m210projects.Wang.Weapons.Rail.InitWeaponRail;
import static ru.m210projects.Wang.Weapons.Shotgun.InitWeaponShotgun;
import static ru.m210projects.Wang.Weapons.Star.InitWeaponStar;
import static ru.m210projects.Wang.Weapons.Uzi.InitWeaponUzi;

public class Sprites {

    public static final Animator DoSpriteFade = new Animator((Animator.Runnable) Sprites::DoSpriteFade);    public static final Animator DoGet = new Animator((Animator.Runnable) Sprites::DoGet);
    public static final int ACTIVE_CHECK_TIME = (3 * 120);    public static final Animator DoKey = new Animator((Animator.Runnable) Sprites::DoKey);
    public static final State[] s_DebrisNinja = {new State(NINJA_DIE + 3, 100, DoActorDebris).setNext()};
    public static final State[] s_DebrisRat = {new State(750, 100, DoActorDebris).setNext()};
    public static final State[] s_DebrisCrab = {new State(423, 100, DoActorDebris).setNext()};    public static final Animator DoCoin = new Animator((Animator.Runnable) Sprites::DoCoin);
    public static final State[] s_DebrisStarFish = {new State(426, 100, DoActorDebris).setNext()};
    //    public static final State[] Debris = {s_DebrisNinja[0], s_DebrisRat[0], s_DebrisCrab[0], s_DebrisStarFish[0]};
//    public static final int[] DirArr = {NORTH, NE, EAST, SE, SOUTH, SW, WEST, NW, NORTH, NE, EAST, SE, SOUTH, SW, WEST, NW};
//    public static final int SCROLL_RATE = 20;
//    public static final int SCROLL_FIRE_RATE = 20;
    // temporary
    public static final int ICON_REPAIR_KIT = 1813;
    public static final int REPAIR_KIT_RATE = 1100;
    //    public static final int KEY_RATE = 25;
    public static final int Red_COIN = 2440;
    public static final int Yellow_COIN = 2450;
    public static final int Green_COIN = 2460;
    public static final int RED_COIN_RATE = 10;
    public static final int YELLOW_COIN_RATE = 8;
    public static final int GREEN_COIN_RATE = 6;
    public static final int FIREBALL_LG_AMMO_RATE = 12;
    public static final int HEART_LG_AMMO_RATE = 12;
    public static final int ICON_SPELL_RATE = 8;
    public static final int ICON_SM_MEDKIT = 1802;
    public static final int ICON_BOOSTER = 1810;
    public static final int ICON_HEAT_CARD = 1819;    public static final State[] s_RepairKit = {new State(ICON_REPAIR_KIT, REPAIR_KIT_RATE, DoGet).setNext()};
    public static final int MAX_FLOORS = 32;
    public static final int GRATE_FACTOR = 3;    public static final State[] s_GoldSkelKey = {new State(GOLD_SKELKEY, 100, DoGet).setNext()};
    public static final int MAX_FORTUNES = 16;
    // With PLOCK on, max = 11
    public static final String[] ReadFortune = {"You never going to score.", "26-31-43-82-16-29", "Sorry, you no win this time, try again.", "You try harder get along. Be a nice man.", "No man is island, except Lo Wang.", "There is much death in future.", "You should kill all business associates.", "(c)1997,3DRealms fortune cookie company.", "Your chi attracts many chicks.", "Don't you know you the scum of society!?", "You should not scratch yourself there.", "Man who stand on toilet, high on pot.", "Man who fart in church sit in own pew.", "Man trapped in pantry has ass in jam.", "Baseball wrong.  Man with 4 balls cannot walk.", "Man who buy drowned cat pay for wet pussy."};    public static final State[] s_BlueKey = {new State(BLUE_KEY, 100, DoGet).setNext()};
    public static final String[] KeyMsg = {"Got the RED key!", "Got the BLUE key!", "Got the GREEN key!", "Got the YELLOW key!", "Got the GOLD master key!", "Got the SILVER master key!", "Got the BRONZE master key!", "Got the RED master key!"};
    public static final int ITEMFLASHAMT = -8;    public static final State[] s_BlueCard = {new State(BLUE_CARD, 100, DoGet).setNext()};
    public static final int ITEMFLASHCLR = 144;
    // This function mostly only adjust the active_range field
    public static final int TIME_TILL_INACTIVE = (4 * 120);    public static final State[] s_SilverSkelKey = {new State(SILVER_SKELKEY, 100, DoGet).setNext()};
    //    public static final int INLINE_STATE = 1;
    private static final int[] KeyPal = {PALETTE_PLAYER9, PALETTE_PLAYER7, PALETTE_PLAYER6, PALETTE_PLAYER4, PALETTE_PLAYER9, PALETTE_PLAYER7, PALETTE_PLAYER6, PALETTE_PLAYER4, PALETTE_PLAYER4, PALETTE_PLAYER1, PALETTE_PLAYER8, PALETTE_PLAYER9};
    private static final int[] sectlist = new int[MAXSECTORS];    public static final State[] s_RedKey = {new State(RED_KEY, 100, DoGet).setNext()};
    private static final Sprite[] BoundList = new Sprite[MAX_FLOORS];
    public static final Sector_Object[] SectorObject = new Sector_Object[MAX_SECTOR_OBJECTS];    public static final State[] s_RedCard = {new State(RED_CARD, 100, DoGet).setNext()};
    public static int wait_active_check_offset;
    public static final Animator DoGrating = new Animator((Animator.Runnable) Sprites::DoGrating);    public static final State[] s_BronzeSkelKey = {new State(BRONZE_SKELKEY, 100, DoGet).setNext()};
    public static int globhiz, globloz, globhihit, globlohit;
    public static final Animator DoFireFly = new Animator((Animator.Runnable) Actor::DoFireFly);    public static final State[] s_GreenKey = {new State(GREEN_KEY, 100, DoGet).setNext()};
    public static final State[] s_FireFly = {new State(FIRE_FLY0, FIRE_FLY_RATE * 4, DoFireFly).setNext()};
    public static boolean MoveSkip2;
    public static final State[] s_GreenCard = {new State(GREEN_CARD, 100, DoGet).setNext()};
    public static int MoveSkip4, MoveSkip8;    // !JIM! Frank, I made coins go progressively faster
    public static final short[] MissileStats = {STAT_MISSILE, STAT_MISSILE_SKIP4};    public static final State[] s_RedSkelKey = {new State(RED_SKELKEY, 100, DoGet).setNext()};

    public static void InitSprStates() {
        State.InitState(s_RedCoin);
        State.InitState(s_YellowCoin);
        State.InitState(s_GreenCoin);
        State.InitState(s_IconFireballLgAmmo);
        State.InitState(s_IconHeart);
        State.InitState(s_IconHeartLgAmmo);
        State.InitState(s_IconElectro);
        State.InitState(s_IconSpell);
        State.InitState(s_IconFly);
        State.InitState(s_IconNightVision);
        State.InitState(s_IconFlag);
    }

    public static void SetOwner(int owner, int child) {
        Sprite cp = boardService.getSprite(child);
        if (cp == null) {
            return;
        }

        if (owner >= 0) {
            USER u = getUser(owner);
            if (u != null) {
                u.Flags2 |= (SPR2_CHILDREN);
            }
        }
        cp.setOwner(owner);
    }    public static final State[] s_YellowKey = {new State(YELLOW_KEY, 100, DoGet).setNext()};

    public static void SetAttach(int owner, int child) {
        USER cu = getUser(child);
        USER ou = getUser(owner);

        if (ou != null && cu != null) {
            ou.Flags2 |= (SPR2_CHILDREN);
            cu.Attach = owner;
        }
    }

    public static void KillSprite(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u = getUser(SpriteNum);

        //////////////////////////////////////////////
        // Check sounds list to kill attached sounds
        DeleteNoSoundOwner(SpriteNum);
        DeleteNoFollowSoundOwner(sp);
        //////////////////////////////////////////////

        if (u != null) {
            if (u.WallShade != null) {
                u.WallShade = null;
            }

            // doing a MissileSetPos - don't allow killing
            if (TEST(u.Flags, SPR_SET_POS_DONT_KILL)) {
                return;
            }

            // for attached sprites that are getable make sure they don't have
            // any Anims attached
            AnimDelete(u, AnimType.UserZ);
            AnimDelete(sp, AnimType.SpriteZ);

            // adjust sprites attached to sector objects
            if (TEST(u.Flags, SPR_SO_ATTACHED)) {
                Sector_Object sop;
                int sn, FoundSpriteNdx = -1;

                for (int s = 0; s < MAX_SECTOR_OBJECTS; s++) {
                    sop = SectorObject[s];
                    for (sn = 0; sop.sp_num[sn] != -1; sn++) {
                        if (sop.sp_num[sn] == SpriteNum) {
                            FoundSpriteNdx = sn;
                        }
                    }

                    if (FoundSpriteNdx >= 0) {
                        // back up sn so it points to the last valid sprite num
                        sn--;

                        // replace the one to be deleted with the last ndx
                        sop.sp_num[FoundSpriteNdx] = sop.sp_num[sn];
                        // the last ndx is not -1
                        sop.sp_num[sn] = -1;

                        break;
                    }
                }

            }

            // if a player is dead and watching this sprite
            // reset it.
            for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                PlayerStr pp = Player[pnum];

                if (pp.Killer > -1) {
                    if (pp.Killer == SpriteNum) {
                        pp.Killer = -1;
                    }
                }
            }

            // if on a track and died reset the track to non-occupied
            if (sp.getStatnum() == STAT_ENEMY) {
                if (u.track != -1 && Track[u.track] != null) {
                    if (Track[u.track].flags != 0) {
                        Track[u.track].flags &= ~(TF_TRACK_OCCUPIED);
                    }
                }
            }

            // if missile is heading for the sprite, the missile need to know
            // that it is already dead
            if (TEST(sp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                ListNode<Sprite> nexti;
                for (short missileStat : MissileStats) {
                    for (ListNode<Sprite> node = boardService.getStatNode(missileStat); node != null; node = nexti) {
                        nexti = node.getNext();
                        USER mu = getUser(node.getIndex());

                        if (mu != null && mu.WpnGoal == SpriteNum) {
                            mu.WpnGoal = -1;
                        }
                    }
                }
            }

            // much faster
            if (TEST(u.Flags2, SPR2_CHILDREN)) {
                // check for children and allert them that the owner is dead
                // don't bother th check if you've never had children
                ListNode<Sprite> nexti;
                for (int stat = 0; stat < STAT_DONT_DRAW; stat++) {
                    for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = nexti) {
                        nexti = node.getNext();
                        Sprite sp1 = node.get();
                        USER mu = getUser(node.getIndex());
                        if (sp1.getOwner() == SpriteNum) {
                            sp1.setOwner(-1);
                        }

                        if (mu != null && mu.Attach == SpriteNum) {
                            mu.Attach = -1;
                        }
                    }
                }
            }

            if (sp.getStatnum() == STAT_ENEMY) {
                ListNode<Sprite> nexti;
                for (ListNode<Sprite> node = boardService.getStatNode(STAT_ENEMY); node != null; node = nexti) {
                    int i = node.getIndex();
                    nexti = node.getNext();
                    USER ui = getUser(i);
                    if (ui != null && ui.tgt_sp == SpriteNum) {
                        DoActorPickClosePlayer(i);
                    }
                }
            }

            if (u.flame >= 0) {
                SetSuicide(u.flame);
            }

            if (u.getRotator() != null) {
                if (u.getRotator().orig != null) {
                    u.getRotator().orig = null;
                }

                u.setRotator(null);
            }

            setUser(SpriteNum, null);
        }

        engine.deletesprite(SpriteNum);
        // shred your garbage - but not statnum
        int statnum = sp.getStatnum();
        int sectnum = sp.getSectnum();
        sp.reset();
        sp.setStatnum(statnum);
        sp.setSectnum(sectnum);
    }    public static final State[] s_YellowCard = {new State(YELLOW_CARD, 100, DoGet).setNext()};

    public static void ChangeState(int SpriteNum, State statep) {
        USER u = getUser(SpriteNum);
        if (u == null || statep == null) {
            return;
        }

        u.Tics = 0;
        u.State = u.StateStart = statep;
        // Just in case
        PicAnimOff(u.State.Pic);
    }

    public static void change_sprite_stat(int SpriteNum, int stat) {
        USER u = getUser(SpriteNum);
        engine.changespritestat(SpriteNum, stat);

        if (u != null) {
            u.Flags &= ~(SPR_SKIP2 | SPR_SKIP4);

            if (stat >= STAT_SKIP4_START && stat <= STAT_SKIP4_END) {
                u.Flags |= (SPR_SKIP4);
            }

            if (stat >= STAT_SKIP2_START && stat <= STAT_SKIP2_END) {
                u.Flags |= (SPR_SKIP2);
            }

            switch (stat) {
                case STAT_ENEMY_SKIP4:
                case STAT_ENEMY:
                    // for enemys - create offsets so all enemys don't call FAFcansee at once
                    wait_active_check_offset += ACTORMOVETICS * 3;
                    if (wait_active_check_offset > ACTIVE_CHECK_TIME) {
                        wait_active_check_offset = 0;
                    }
                    u.wait_active_check = wait_active_check_offset;
                    // don't do a break here
                    u.Flags |= (SPR_SHADOW);
                    break;
            }

        }
    }    public static final State[][] s_Key = {s_RedKey, s_BlueKey, s_GreenKey, s_YellowKey, s_RedCard, s_BlueCard, s_GreenCard, s_YellowCard, s_GoldSkelKey, s_SilverSkelKey, s_BronzeSkelKey, s_RedSkelKey};

    public static USER SpawnUser(int SpriteNum, int id, State state) {
        WangSprite sp = boardService.getSprite(SpriteNum);
        USER u = new USER();
        if (sp == null) {
            return u;
        }

        setUser(SpriteNum, u);

        // be careful State can be null
        u.State = u.StateStart = state;

        change_sprite_stat(SpriteNum, sp.getStatnum());

        u.ID = id;
        u.Health = 100;
        u.WpnGoal = -1; // for weapons
        u.Attach = -1;
        u.track = -1;
        u.tgt_sp = Player[0].PlayerSprite;
        u.Radius = 220;
        u.Sibling = -1;
        u.flame = -1;
        u.WaitTics = 0;
        u.OverlapZ = Z(4);
        u.WallShade = null;
        u.setRotator(null);
        u.bounce = 0;

        u.motion_blur_num = 0;
        u.motion_blur_dist = 256;

        u.ox = sp.getX();
        u.oy = sp.getY();
        u.oz = sp.getZ();

        u.active_range = MIN_ACTIVE_RANGE;

        // default

        // based on clipmove z of 48 pixels off the floor
        u.floor_dist = (Z(48) - Z(28));
        u.ceiling_dist = Z(8);

        // Problem with sprites spawned really close to white sector walls
        // cant do a getzrange there
        // Just put in some valid starting values

        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec != null) {
            u.loz = sec.getFloorz();
            u.hiz = sec.getCeilingz();
            u.lo_sp = -1;
            u.hi_sp = -1;
            u.lo_sectp = sp.getSectnum();
            u.hi_sectp = sp.getSectnum();
        }

        return (u);
    }

    public static Sect_User SetupSectUser(int sectnum) {
        Sect_User su = getSectUser(sectnum);
        if (su != null) {
            return su;
        }

        su = new Sect_User();
        setSectUser(sectnum, su);
        return su;
    }

    public static int SpawnSprite(int stat, int id, State state, int sectnum, int x, int y, int z, int init_ang, int vel) {
        if (!boardService.isValidSector(sectnum)) {
            return -1;
        }

        int SpriteNum = COVERinsertsprite(sectnum, stat);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return -1;
        }

        sp.setPal(0);
        sp.setX(x);
        sp.setY(y);
        sp.setZ(z);
        sp.setCstat(0);

        USER u = SpawnUser(SpriteNum, id, state);
        setUser(SpriteNum, u);

        // be careful State can be null
        if (u.State != null) {
            sp.setPicnum(u.State.Pic);
            PicAnimOff(sp.getPicnum());
        }

        sp.setShade(0);
        sp.setXrepeat(64);
        sp.setYrepeat(64);
        sp.setAng(NORM_ANGLE(init_ang));

        sp.setXvel(vel);
        sp.setZvel(0);
        sp.setOwner(-1);
        sp.setLotag(0);
        sp.setHitag(0);
        sp.setExtra(0);
        sp.setXoffset(0);
        sp.setYoffset(0);
        sp.setClipdist(0);

        return (SpriteNum);
    }

    public static void PicAnimOff(int picnum) {
        ArtEntry pic = engine.getTile(picnum);
        if (!TEST(pic.getFlags(), TILE_ANIM_TYPE)) {
            return;
        }

        int flags = pic.getFlags();
        flags &= ~(TILE_ANIM_TYPE);
        pic.setFlags(flags);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean IconSpawn(Sprite sp) {
        // if multi item and not a modem game
        if (TEST(sp.getExtra(), SPRX_MULTI_ITEM)) {
            if ((numplayers <= 1 && !gNet.FakeMultiplayer) || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COOPERATIVE) {
                return (false);
            }
        }

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN | CSTAT_SPRITE_ONE_SIDE | CSTAT_SPRITE_FLOOR));

        return (true);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean ActorTestSpawn(final int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null && sp.getStatnum() == STAT_DEFAULT && sp.getLotag() == TAG_SPAWN_ACTOR) {
            int newsp = COVERinsertsprite(sp.getSectnum(), STAT_DEFAULT);
            Sprite sp1 = boardService.getSprite(newsp);
            if (sp1 != null) {
                sp1.set(sp);

                change_sprite_stat(newsp, STAT_SPAWN_TRIGGER);
                sp1.setCstat(sp1.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                return (false);
            }
        }

        if (DebugActor) {
            return (false);
        }

        // Skill ranges from -1 (No Monsters) to 3
        if (sp != null && DTEST(sp.getExtra(), SPRX_SKILL) > Skill) {
            if (Skill > -1) {
                // always spawn girls in addons
                if ((sp.getPicnum() == TOILETGIRL_R0 || sp.getPicnum() == WASHGIRL_R0 || sp.getPicnum() == MECHANICGIRL_R0 || sp.getPicnum() == CARGIRL_R0 || sp.getPicnum() == PRUNEGIRL_R0 || sp.getPicnum() == SAILORGIRL_R0) && swGetAddon() != 0) {
                    return true;
                }

                // JBF: hack to fix Wanton Destruction's missing Sumos, Serpents, and Zilla on
                // Skill < 2
                return (((sp.getPicnum() == SERP_RUN_R0 || sp.getPicnum() == SUMO_RUN_R0) && sp.getLotag() > 0 && sp.getLotag() != TAG_SPAWN_ACTOR && sp.getExtra() > 0) || sp.getPicnum() == ZILLA_RUN_R0) && swGetAddon() != 0;
            }

            return (false);
        }

        return (true);
    }

    public static boolean ActorSpawn(int SpriteNum) {
        boolean ret = true;
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return false;
        }

        switch (sp.getPicnum()) {
            case COOLIE_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupCoolie(SpriteNum);

                break;
            }

            case NINJA_RUN_R0:
            case NINJA_CRAWL_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupNinja(SpriteNum);

                break;
            }

            case GORO_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupGoro(SpriteNum);
                break;
            }

            case 1441:
            case COOLG_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupCoolg(SpriteNum);
                break;
            }

            case EEL_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupEel(SpriteNum);
                break;
            }

            case SUMO_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupSumo(SpriteNum);

                break;
            }

            case ZILLA_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupZilla(SpriteNum);

                break;
            }

            case TOILETGIRL_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupToiletGirl(SpriteNum);

                break;
            }

            case WASHGIRL_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupWashGirl(SpriteNum);

                break;
            }

            case CARGIRL_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupCarGirl(SpriteNum);

                break;
            }

            case MECHANICGIRL_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupMechanicGirl(SpriteNum);

                break;
            }

            case SAILORGIRL_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupSailorGirl(SpriteNum);

                break;
            }

            case PRUNEGIRL_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupPruneGirl(SpriteNum);

                break;
            }

            case TRASHCAN: {
                PicAnimOff(sp.getPicnum());
                SetupTrashCan(SpriteNum);

                break;
            }

            case BUNNY_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupBunny(SpriteNum);
                break;
            }

            case RIPPER_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupRipper(SpriteNum);
                break;
            }

            case RIPPER2_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupRipper2(SpriteNum);
                break;
            }

            case SERP_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupSerp(SpriteNum);
                break;
            }

            case LAVA_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupLava(SpriteNum);
                break;
            }

            case SKEL_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupSkel(SpriteNum);
                break;
            }

            case HORNET_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum) || cfg.DisableHornets) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupHornet(SpriteNum);
                break;
            }

            case SKULL_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupSkull(SpriteNum);
                break;
            }

            case BETTY_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupBetty(SpriteNum);
                break;
            }

            case 623: // Pachinko win light
            {
                PicAnimOff(sp.getPicnum());
                SetupPachinkoLight(SpriteNum);
                break;
            }

            case PACHINKO1: {
                PicAnimOff(sp.getPicnum());
                SetupPachinko1(SpriteNum);
                break;
            }

            case PACHINKO2: {
                PicAnimOff(sp.getPicnum());
                SetupPachinko2(SpriteNum);
                break;
            }

            case PACHINKO3: {
                PicAnimOff(sp.getPicnum());
                SetupPachinko3(SpriteNum);
                break;
            }

            case PACHINKO4: {
                PicAnimOff(sp.getPicnum());
                SetupPachinko4(SpriteNum);
                break;
            }

            case GIRLNINJA_RUN_R0: {
                if (!ActorTestSpawn(SpriteNum)) {
                    KillSprite(SpriteNum);
                    return (false);
                }

                PicAnimOff(sp.getPicnum());
                SetupGirlNinja(SpriteNum);
                break;
            }

            default:
                ret = false;
                break;
        }

        return (ret);
    }

    public static void IconDefault(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        change_sprite_stat(SpriteNum, STAT_ITEM);

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        u.Radius = 650;

        DoActorZrange(SpriteNum);
    }    public static final State[] s_RedCoin = {new State(Red_COIN, RED_COIN_RATE, DoCoin), new State(Red_COIN + 1, RED_COIN_RATE, DoCoin), new State(Red_COIN + 2, RED_COIN_RATE, DoCoin), new State(Red_COIN + 3, RED_COIN_RATE, DoCoin), new State(Red_COIN + 4, RED_COIN_RATE, DoCoin), new State(Red_COIN + 5, RED_COIN_RATE, DoCoin), new State(Red_COIN + 6, RED_COIN_RATE, DoCoin), new State(Red_COIN + 7, RED_COIN_RATE, DoCoin)};

    public static void PreMapCombineFloors() {
        Arrays.fill(BoundList, null);

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(0); node != null; node = nexti) {
            int SpriteNum = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();

            if (sp.getPicnum() != ST1) {
                continue;
            }

            if (sp.getHitag() == BOUND_FLOOR_OFFSET || sp.getHitag() == BOUND_FLOOR_BASE_OFFSET) {
                BoundList[sp.getLotag()] = sp;
                change_sprite_stat(SpriteNum, STAT_FAF);
            }
        }

        int base_offset = 0;
        for (int i = 0; i < MAX_FLOORS; i++) {
            // blank so continue
            if (BoundList[i] == null) {
                continue;
            }

            if (BoundList[i].getHitag() == BOUND_FLOOR_BASE_OFFSET) {
                base_offset = i;
                continue;
            }

            int dx = BoundList[base_offset].getX() - BoundList[i].getX();
            int dy = BoundList[base_offset].getY() - BoundList[i].getY();

            sectlist[0] = BoundList[i].getSectnum();
            int sectlistplc = 0;
            int sectlistend = 1;
            while (sectlistplc < sectlistend) {
                int dasect = sectlist[sectlistplc++];
                Sector sec = boardService.getSector(dasect);
                if (sec == null) {
                    continue;
                }

                for (ListNode<Sprite> node = boardService.getSectNode(dasect); node != null; node = node.getNext()) {
                    Sprite jsp = node.get();
                    jsp.setX(jsp.getX() + dx);
                    jsp.setY(jsp.getY() + dy);
                }

                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    wal.setX(wal.getX() + dx);
                    wal.setY(wal.getY() + dy);

                    int nextsector = wal.getNextsector();
                    if (nextsector == -1) {
                        continue;
                    }

                    int k = sectlistend - 1;
                    for (; k >= 0; k--) {
                        if (sectlist[k] == nextsector) {
                            break;
                        }
                    }

                    if (k < 0) {
                        sectlist[sectlistend++] = nextsector;
                    }
                }

            }

            for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                PlayerStr pp = Player[pnum];
                int dasect = pp.cursectnum;
                for (int j = 0; j < sectlistend; j++) {
                    if (sectlist[j] == dasect) {
                        pp.posx += dx;
                        pp.posy += dy;
                        pp.oposx = pp.oldposx = pp.posx;
                        pp.oposy = pp.oldposy = pp.posy;
                        break;
                    }
                }
            }

        }

        // get rid of the sprites used
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FAF); node != null; node = nexti) {
            nexti = node.getNext();
            KillSprite(node.getIndex());
        }
    }

    public static void SpriteSetupPost() {
        // Post processing of some sprites after gone through the main SpriteSetup()
        // routine

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> n = boardService.getStatNode(STAT_FLOOR_PAN); n != null; n = n.getNext()) {
            for (ListNode<Sprite> node = boardService.getSectNode(n.get().getSectnum()); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite ds = node.get();

                if (ds.getPicnum() == ST1) {
                    continue;
                }

                if (TEST(ds.getCstat(), CSTAT_SPRITE_WALL | CSTAT_SPRITE_FLOOR)) {
                    continue;
                }

                if (getUser(i) != null) {
                    continue;
                }

                engine.getzsofslope(ds.getSectnum(), ds.getX(), ds.getY(), fz, cz);
                if (klabs(ds.getZ() - fz.get()) > Z(4)) {
                    continue;
                }

                USER u = SpawnUser(i, 0, null);
                change_sprite_stat(i, STAT_NO_STATE);
                u.ceiling_dist = Z(4);
                u.floor_dist = -Z(2);

                u.ActorActionFunc = DoActorDebris;

                ds.setCstat(ds.getCstat() | (CSTAT_SPRITE_BREAKABLE));
                ds.setExtra(ds.getExtra() | (SPRX_BREAKABLE));
            }
        }
    }    public static final State[] s_YellowCoin = {new State(Yellow_COIN, YELLOW_COIN_RATE, DoCoin), new State(Yellow_COIN + 1, YELLOW_COIN_RATE, DoCoin), new State(Yellow_COIN + 2, YELLOW_COIN_RATE, DoCoin), new State(Yellow_COIN + 3, YELLOW_COIN_RATE, DoCoin), new State(Yellow_COIN + 4, YELLOW_COIN_RATE, DoCoin), new State(Yellow_COIN + 5, YELLOW_COIN_RATE, DoCoin), new State(Yellow_COIN + 6, YELLOW_COIN_RATE, DoCoin), new State(Yellow_COIN + 7, YELLOW_COIN_RATE, DoCoin)};

    public static void SpriteSetup() {
        // special case for player
        PicAnimOff(PLAYER_NINJA_RUN_R0);

        // Clear Sprite Extension structure
        for (int s = 0; s < MAXSECTORS; s++) {
            Sect_User su = getSectUser(s);
            if (su != null) {
                su.reset();
            }
        }

        // Clear all extra bits - they are set by sprites
        for (int i = 0; i < boardService.getSectorCount(); i++) {
            Sector sec = boardService.getSector(i);
            if (sec != null) {
                sec.setExtra(0);
            }
        }

        // Call my little sprite setup routine first
        JS_SpriteSetup();

        ListNode<Sprite> NextSprite;
        for (ListNode<Sprite> node = boardService.getStatNode(0); node != null; node = NextSprite) {
            final int SpriteNum = node.getIndex();
            NextSprite = node.getNext();
            final Sprite sp = node.get();

            // not used yetv
            engine.getzsofslope(sp.getSectnum(), sp.getX(), sp.getY(), fz, cz);

            if (sp.getZ() > DIV2(cz.get() + fz.get())) {
                // closer to a floor
                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_CLOSE_FLOOR));
            }

            // CSTAT_SPIN is insupported - get rid of it
            if (DTEST(sp.getCstat(), CSTAT_SPRITE_SLAB) == CSTAT_SPRITE_SLAB) {
                sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_SLAB));
            }

            // if BLOCK is set set BLOCK_HITSCAN
            // Hope this doesn't screw up anything
            if (TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK)) {
                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK_HITSCAN));
            }

            ////////////////////////////////////////////
            //
            // BREAKABLE CHECK
            //
            ////////////////////////////////////////////

            // USER SETUP - TAGGED BY USER
            // Non ST1 sprites that are tagged like them
            if (TEST_BOOL1(sp) && sp.getPicnum() != ST1) {
                sp.setExtra(sp.getExtra() & ~(SPRX_BOOL4 | SPRX_BOOL5 | SPRX_BOOL6 | SPRX_BOOL7 | SPRX_BOOL8 | SPRX_BOOL9 | SPRX_BOOL10));

                if (sp.getHitag() == BREAKABLE) {// need something that tells missiles to hit them
                    // but allows actors to move through them
                    sp.setClipdist(SPRITEp_SIZE_X(sp));
                    sp.setExtra(sp.getExtra() | (SPRX_BREAKABLE));
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BREAKABLE));
                }
            } else {
                // BREAK SETUP TABLE AUTOMATED
                SetupSpriteForBreak(sp);
            }

            if (sp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                // if multi item and not a modem game
                if (TEST(sp.getExtra(), SPRX_MULTI_ITEM)) {
                    if ((numplayers <= 1 && !gNet.FakeMultiplayer) || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COOPERATIVE) {
                        KillSprite(SpriteNum);
                        continue;
                    }
                }

                // crack sprite
                if (sp.getPicnum() == 80) {
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK_HITSCAN | CSTAT_SPRITE_BLOCK_MISSILE));
                } else {
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK_HITSCAN | CSTAT_SPRITE_BLOCK_MISSILE));
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
                }

                if (TEST(SP_TAG8(sp), BIT(0))) {
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
                }

                if (TEST(SP_TAG8(sp), BIT(1))) {
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE));
                }

                change_sprite_stat(SpriteNum, STAT_SPRITE_HIT_MATCH);
                continue;
            }

            if (sp.getPicnum() >= TRACK_SPRITE && sp.getPicnum() <= TRACK_SPRITE + MAX_TRACKS) {
                // skip this sprite, just for numbering walls/sectors
                if (TEST(sp.getCstat(), CSTAT_SPRITE_WALL)) {
                    continue;
                }

                int track_num = (sp.getPicnum() - TRACK_SPRITE);

                change_sprite_stat(SpriteNum, STAT_TRACK + track_num);

                continue;
            }

            if (ActorSpawn(SpriteNum)) {
                continue;
            }

            switch (sp.getPicnum()) {
                case ST_QUICK_JUMP:
                    change_sprite_stat(SpriteNum, STAT_QUICK_JUMP);
                    break;
                case ST_QUICK_JUMP_DOWN:
                    change_sprite_stat(SpriteNum, STAT_QUICK_JUMP_DOWN);
                    break;
                case ST_QUICK_SUPER_JUMP:
                    change_sprite_stat(SpriteNum, STAT_QUICK_SUPER_JUMP);
                    break;
                case ST_QUICK_SCAN:
                    change_sprite_stat(SpriteNum, STAT_QUICK_SCAN);
                    break;
                case ST_QUICK_EXIT:
                    change_sprite_stat(SpriteNum, STAT_QUICK_EXIT);
                    break;
                case ST_QUICK_OPERATE:
                    change_sprite_stat(SpriteNum, STAT_QUICK_OPERATE);
                    break;
                case ST_QUICK_DUCK:
                    change_sprite_stat(SpriteNum, STAT_QUICK_DUCK);
                    break;
                case ST_QUICK_DEFEND:
                    change_sprite_stat(SpriteNum, STAT_QUICK_DEFEND);
                    break;

                case ST1: {
                    Sector sectp = boardService.getSector(sp.getSectnum());
                    if (sectp == null) {
                        break;
                    }

                    // get rid of defaults
                    if (SP_TAG3(sp) == 32) {
                        sp.setClipdist(0);
                    }

                    int tag = sp.getHitag();

                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));

                    // for bounding sector objects
                    if (tag >= 500 && tag < 600 || tag == SECT_SO_CENTER) {
                        // NOTE: These will get deleted by the sector object
                        // setup code
                        change_sprite_stat(SpriteNum, STAT_ST1);
                        break;
                    }

                    if (tag < 16) {
                        int bit = (1 << (tag));
                        sectp.setExtra(sectp.getExtra() | bit);

                        if (TEST(bit, SECTFX_SINK)) {
                            Sect_User sectu = SetupSectUser(sp.getSectnum());
                            sectu.depth = sp.getLotag();
                            KillSprite(SpriteNum);
                        } else if (TEST(bit, SECTFX_OPERATIONAL)) {
                            KillSprite(SpriteNum);
                        } else if (TEST(bit, SECTFX_CURRENT)) {
                            Sect_User sectu = SetupSectUser(sp.getSectnum());
                            sectu.speed = sp.getLotag();
                            sectu.ang = sp.getAng();
                            KillSprite(SpriteNum);
                        } else if (TEST(bit, SECTFX_NO_RIDE)) {
                            change_sprite_stat(SpriteNum, STAT_NO_RIDE);
                        } else if (TEST(bit, SECTFX_DIVE_AREA)) {
                            Sect_User sectu = SetupSectUser(sp.getSectnum());
                            sectu.number = sp.getLotag();
                            change_sprite_stat(SpriteNum, STAT_DIVE_AREA);
                        } else if (TEST(bit, SECTFX_UNDERWATER)) {
                            Sect_User sectu = SetupSectUser(sp.getSectnum());
                            sectu.number = sp.getLotag();
                            change_sprite_stat(SpriteNum, STAT_UNDERWATER);
                        } else if (TEST(bit, SECTFX_UNDERWATER2)) {
                            Sect_User sectu = SetupSectUser(sp.getSectnum());
                            sectu.number = sp.getLotag();
                            if (sp.getClipdist() == 1) {
                                sectu.flags |= (SECTFU_CANT_SURFACE);
                            }
                            change_sprite_stat(SpriteNum, STAT_UNDERWATER2);
                        }
                    } else {
                        switch (tag) {
                            case SECT_MATCH: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());
                                sectu.number = sp.getLotag();
                                KillSprite(SpriteNum);
                                break;
                            }
                            case SLIDE_SECTOR: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());
                                sectu.flags |= (SECTFU_SLIDE_SECTOR);
                                sectu.speed = SP_TAG2(sp);
                                KillSprite(SpriteNum);
                                break;
                            }
                            case SECT_DAMAGE: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());
                                if (TEST_BOOL1(sp)) {
                                    sectu.flags |= (SECTFU_DAMAGE_ABOVE_SECTOR);
                                }
                                sectu.damage = sp.getLotag();
                                KillSprite(SpriteNum);
                                break;
                            }

                            case PARALLAX_LEVEL: {
                                pskybits = sp.getLotag();
                                if (SP_TAG4(sp) > 2048) {
                                    Renderer renderer = game.getRenderer();
                                    renderer.setParallaxScale(SP_TAG4(sp));
                                }
                                KillSprite(SpriteNum);
                                break;
                            }

                            case BREAKABLE:
                                // used for wall info
                                change_sprite_stat(SpriteNum, STAT_BREAKABLE);
                                break;

                            case SECT_DONT_COPY_PALETTE: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());

                                sectu.flags |= (SECTFU_DONT_COPY_PALETTE);
                                KillSprite(SpriteNum);
                                break;
                            }

                            case SECT_FLOOR_PAN: {
                                // if moves with SO
                                if (TEST_BOOL1(sp)) {
                                    sp.setXvel(0);
                                } else {
                                    sp.setXvel(sp.getLotag());
                                }

                                change_sprite_stat(SpriteNum, STAT_FLOOR_PAN);
                                break;
                            }

                            case SECT_CEILING_PAN: {
                                // if moves with SO
                                if (TEST_BOOL1(sp)) {
                                    sp.setXvel(0);
                                } else {
                                    sp.setXvel(sp.getLotag());
                                }
                                change_sprite_stat(SpriteNum, STAT_CEILING_PAN);
                                break;
                            }

                            case SECT_WALL_PAN_SPEED: {
                                engine.hitscan(sp.getX(), sp.getY(), sp.getZ() - Z(8), sp.getSectnum(), // Start position
                                        EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)), // X vector of 3D ang
                                        EngineUtils.sin(sp.getAng()), // Y vector of 3D ang
                                        0, // Z vector of 3D ang
                                        pHitInfo, CLIPMASK_MISSILE);

                                int hitsect = pHitInfo.hitsect;
                                int hitwall = pHitInfo.hitwall;

                                if (hitwall == -1) {
                                    KillSprite(SpriteNum);
                                    break;
                                }

                                sp.setOwner(hitwall);
                                // if moves with SO
                                if (TEST_BOOL1(sp)) {
                                    sp.setXvel(0);
                                } else {
                                    sp.setXvel(sp.getLotag());
                                }
                                sp.setAng(SP_TAG6(sp));
                                // attach to the sector that contains the wall
                                engine.changespritesect(SpriteNum, hitsect);
                                change_sprite_stat(SpriteNum, STAT_WALL_PAN);
                                break;
                            }

                            case WALL_DONT_STICK: {
                                engine.hitscan(sp.getX(), sp.getY(), sp.getZ() - Z(8), sp.getSectnum(), // Start position
                                        EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)), // X vector of 3D ang
                                        EngineUtils.sin(sp.getAng()), // Y vector of 3D ang
                                        0, // Z vector of 3D ang
                                        pHitInfo, CLIPMASK_MISSILE);

                                int hitwall = pHitInfo.hitwall;
                                Wall hitwal = boardService.getWall(hitwall);
                                if (hitwal == null) {
                                    KillSprite(SpriteNum);
                                    break;
                                }

                                hitwal.setExtra(hitwal.getExtra() | (WALLFX_DONT_STICK));
                                KillSprite(SpriteNum);
                                break;
                            }

                            case TRIGGER_SECTOR: {
                                sectp.setExtra(sectp.getExtra() | (SECTFX_TRIGGER));
                                change_sprite_stat(SpriteNum, STAT_TRIGGER);
                                break;
                            }

                            case DELETE_SPRITE: {
                                change_sprite_stat(SpriteNum, STAT_DELETE_SPRITE);
                                break;
                            }

                            case SPAWN_ITEMS: {
                                if (TEST(sp.getExtra(), SPRX_MULTI_ITEM)) {
                                    if ((numplayers <= 1 && !gNet.FakeMultiplayer) || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COOPERATIVE) {
                                        KillSprite(SpriteNum);
                                        break;
                                    }
                                }

                                change_sprite_stat(SpriteNum, STAT_SPAWN_ITEMS);
                                break;
                            }

                            case CEILING_FLOOR_PIC_OVERRIDE: {
                                // block hitscans depending on translucency
                                if (SP_TAG7(sp) == 0 || SP_TAG7(sp) == 1) {
                                    if (SP_TAG3(sp) == 0) {
                                        sectp.setCeilingstat(sectp.getCeilingstat() | (CEILING_STAT_FAF_BLOCK_HITSCAN));
                                    } else {
                                        sectp.setFloorstat(sectp.getFloorstat() | (FLOOR_STAT_FAF_BLOCK_HITSCAN));
                                    }
                                } else if (TEST_BOOL1(sp)) {
                                    if (SP_TAG3(sp) == 0) {
                                        sectp.setCeilingstat(sectp.getCeilingstat() | (CEILING_STAT_FAF_BLOCK_HITSCAN));
                                    } else {
                                        sectp.setFloorstat(sectp.getFloorstat() | (FLOOR_STAT_FAF_BLOCK_HITSCAN));
                                    }
                                }

                                // copy tag 7 to tag 6 and pre-shift it
                                sp.setYvel(SP_TAG7(sp));
                                sp.setYvel(sp.getYvel() << 7);
                                change_sprite_stat(SpriteNum, STAT_CEILING_FLOOR_PIC_OVERRIDE);
                                break;
                            }

                            case QUAKE_SPOT: {
                                change_sprite_stat(SpriteNum, STAT_QUAKE_SPOT);
                                SET_SP_TAG13(sp, ((SP_TAG6(sp) * 10) * 120));
                                break;
                            }

                            case SECT_CHANGOR: {
                                change_sprite_stat(SpriteNum, STAT_CHANGOR);
                                break;
                            }

                            case SECT_VATOR: {

                                USER u = SpawnUser(SpriteNum, 0, null);

                                // vator already set - ceiling AND floor vator
                                if (TEST(sectp.getExtra(), SECTFX_VATOR)) {
                                    Sect_User sectu = SetupSectUser(sp.getSectnum());
                                    sectu.flags |= (SECTFU_VATOR_BOTH);
                                }
                                sectp.setExtra(sectp.getExtra() | (SECTFX_VATOR));
                                SetSectorWallBits(sectp, WALLFX_DONT_STICK, true, true);
                                sectp.setExtra(sectp.getExtra() | (SECTFX_DYNAMIC_AREA));

                                // don't step on toes of other sector settings
                                if (sectp.getLotag() == 0 && sectp.getHitag() == 0) {
                                    sectp.setLotag(TAG_VATOR);
                                }

                                int type = SP_TAG3(sp);
                                int speed = SP_TAG4(sp);
                                int vel = SP_TAG5(sp);
                                int time = SP_TAG9(sp);
                                boolean start_on = TEST_BOOL1(sp);
                                boolean floor_vator = !TEST(sp.getCstat(), CSTAT_SPRITE_YFLIP);

                                u.jump_speed = (u.vel_tgt = speed);
                                u.vel_rate = vel;
                                u.WaitTics = (time * 15); // 1/8 of a sec
                                u.Tics = 0;

                                u.Flags |= (SPR_ACTIVE);

                                switch (type) {
                                    case 0:
                                    case 1:
                                        u.Flags &= ~(SPR_ACTIVE);
                                        u.ActorActionFunc = DoVator;
                                        break;
                                    case 2:
                                        u.ActorActionFunc = DoVatorAuto;
                                        break;
                                    case 3:
                                        u.Flags &= ~(SPR_ACTIVE);
                                        u.ActorActionFunc = DoVatorAuto;
                                        break;
                                }

                                if (floor_vator) {
                                    // start off
                                    u.sz = sectp.getFloorz();
                                    u.z_tgt = sp.getZ();
                                    if (start_on) {
                                        int amt = sp.getZ() - sectp.getFloorz();

                                        // start in the on position
                                        // sectp.floorz = sp.z;
                                        sectp.setFloorz(sectp.getFloorz() + amt);
                                        u.z_tgt = u.sz;

                                        MoveSpritesWithSector(sp.getSectnum(), amt, false); // floor
                                    }

                                    // set orig z
                                    u.oz = sectp.getFloorz();
                                } else {
                                    // start off
                                    u.sz = sectp.getCeilingz();
                                    u.z_tgt = sp.getZ();
                                    if (start_on) {
                                        int amt = sp.getZ() - sectp.getCeilingz();

                                        // starting in the on position
                                        // sectp.ceilingz = sp.z;
                                        sectp.setCeilingz(sectp.getCeilingz() + amt);
                                        u.z_tgt = u.sz;

                                        MoveSpritesWithSector(sp.getSectnum(), amt, true); // ceiling
                                    }

                                    // set orig z
                                    u.oz = sectp.getCeilingz();
                                }

                                change_sprite_stat(SpriteNum, STAT_VATOR);
                                break;
                            }

                            case SECT_ROTATOR_PIVOT: {
                                change_sprite_stat(SpriteNum, STAT_ROTATOR_PIVOT);
                                break;
                            }

                            case SECT_ROTATOR: {
                                USER u = SpawnUser(SpriteNum, 0, null);

                                SetSectorWallBits(sectp, WALLFX_DONT_STICK, true, true);

                                // need something for this
                                sectp.setLotag(TAG_ROTATOR);
                                sectp.setHitag(sp.getLotag());

                                int type = SP_TAG3(sp);
                                int time = SP_TAG9(sp);

                                u.WaitTics = (time * 15); // 1/8 of a sec
                                u.Tics = 0;

                                int startwall = sectp.getWallptr();
                                int endwall = (startwall + sectp.getWallnum() - 1);
                                int wallcount = 0;

                                // count walls of sector
                                for (int w = startwall; w <= endwall; w++) {
                                    wallcount++;
                                }

                                RotatorStr rotator = new RotatorStr();
                                u.setRotator(rotator);
                                rotator.num_walls = wallcount;
                                rotator.open_dest = SP_TAG5(sp);
                                rotator.speed = SP_TAG7(sp);
                                rotator.vel = SP_TAG8(sp);
                                rotator.pos = 0; // closed
                                rotator.tgt = rotator.open_dest; // closed
                                rotator.orig = new Vector2i[wallcount];
                                for (int a = 0; a < wallcount; a++) {
                                    rotator.orig[a] = new Vector2i();
                                }

                                rotator.orig_speed = rotator.speed;

                                wallcount = 0;
                                for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                                    Wall wal = wn.get();
                                    rotator.orig[wallcount].x = wal.getX();
                                    rotator.orig[wallcount].y = wal.getY();
                                    wallcount++;
                                }

                                u.Flags |= (SPR_ACTIVE);

                                switch (type) {
                                    case 0:
                                    case 1:
                                        u.Flags &= ~(SPR_ACTIVE);
                                        u.ActorActionFunc = DoRotator;
                                        break;
                                }

                                change_sprite_stat(SpriteNum, STAT_ROTATOR);
                                break;
                            }

                            case SECT_SLIDOR: {
                                USER u = SpawnUser(SpriteNum, 0, null);

                                SetSectorWallBits(sectp, WALLFX_DONT_STICK, true, true);

                                // need something for this
                                sectp.setLotag(TAG_SLIDOR);
                                sectp.setHitag(sp.getLotag());

                                int type = SP_TAG3(sp);
                                int time = SP_TAG9(sp);

                                u.WaitTics = (time * 15); // 1/8 of a sec
                                u.Tics = 0;
                                RotatorStr rotator = new RotatorStr();
                                u.setRotator(rotator);
                                rotator.open_dest = SP_TAG5(sp);
                                rotator.speed = SP_TAG7(sp);
                                rotator.vel = SP_TAG8(sp);
                                rotator.pos = 0; // closed
                                rotator.tgt = rotator.open_dest; // closed
                                rotator.num_walls = 0;
                                rotator.orig_speed = rotator.speed;

                                u.Flags |= (SPR_ACTIVE);

                                switch (type) {
                                    case 0:
                                    case 1:
                                        u.Flags &= ~(SPR_ACTIVE);
                                        u.ActorActionFunc = DoSlidor;
                                        break;
                                }

                                if (TEST_BOOL5(sp)) {
                                    DoSlidorInstantClose(SpriteNum);
                                }

                                change_sprite_stat(SpriteNum, STAT_SLIDOR);
                                break;
                            }

                            case SECT_SPIKE: {
                                USER u = SpawnUser(SpriteNum, 0, null);

                                SetSectorWallBits(sectp, WALLFX_DONT_STICK, false, true);
                                sectp.setExtra(sectp.getExtra() | (SECTFX_DYNAMIC_AREA));

                                int type = SP_TAG3(sp);
                                int speed = SP_TAG4(sp);
                                int vel = SP_TAG5(sp);
                                int time = SP_TAG9(sp);
                                boolean start_on = TEST_BOOL1(sp);
                                boolean floor_vator = !TEST(sp.getCstat(), CSTAT_SPRITE_YFLIP);

                                u.jump_speed = (u.vel_tgt = speed);
                                u.vel_rate = vel;
                                u.WaitTics = (time * 15); // 1/8 of a sec
                                u.Tics = 0;

                                u.Flags |= (SPR_ACTIVE);

                                switch (type) {
                                    case 0:
                                    case 1:
                                        u.Flags &= ~(SPR_ACTIVE);
                                        u.ActorActionFunc = DoSpike;
                                        break;
                                    case 2:
                                        u.ActorActionFunc = DoSpikeAuto;
                                        break;
                                    case 3:
                                        u.Flags &= ~(SPR_ACTIVE);
                                        u.ActorActionFunc = DoSpikeAuto;
                                        break;
                                }

                                engine.getzrangepoint(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), tmp_ptr[0], tmp_ptr[1], tmp_ptr[2], tmp_ptr[3]);
                                int ceilingz = tmp_ptr[0].value;
                                int floorz = tmp_ptr[2].value;

                                if (floor_vator) {
                                    u.zclip = floorz;
                                } else {
                                    u.zclip = ceilingz;
                                }

                                // start off
                                u.sz = u.zclip;
                                u.z_tgt = sp.getZ();
                                if (start_on) {
                                    // starting in the on position
                                    u.zclip = sp.getZ();
                                    u.z_tgt = u.sz;
                                    SpikeAlign(SpriteNum);
                                }

                                // set orig z
                                u.oz = u.zclip;

                                change_sprite_stat(SpriteNum, STAT_SPIKE);
                                break;
                            }

                            case LIGHTING: {
                                sp.setZ(0);

                                if (LIGHT_ShadeInc(sp) == 0) {
                                    SET_SP_TAG7(sp, 1);
                                }

                                // save off original floor and ceil shades
                                (sp).setXoffset(sectp.getFloorshade());
                                (sp).setYoffset(sectp.getCeilingshade());

                                // count walls of sector
                                int wallcount = 0;
                                for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                                    Wall wal = wn.get();
                                    wallcount++;
                                    if (TEST_BOOL5(sp)) {
                                        if (wal.getNextwall() >= 0) {
                                            wallcount++;
                                        }
                                    }
                                }

                                USER u = SpawnUser(SpriteNum, 0, null);
                                setUser(SpriteNum, u);
                                u.WallCount = wallcount;
                                byte[] wall_shade = u.WallShade = new byte[u.WallCount];

                                // save off original wall shades
                                wallcount = 0;
                                for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                                    Wall wal = wn.get();
                                    wall_shade[wallcount] = wal.getShade();
                                    wallcount++;
                                    if (TEST_BOOL5(sp)) {
                                        if (wal.getNextwall() >= 0) {
                                            Wall nextw = boardService.getWall(wal.getNextwall());
                                            if (nextw != null) {
                                                wall_shade[wallcount] = nextw.getShade();
                                                wallcount++;
                                            }
                                        }
                                    }
                                }

                                u.spal = (byte) sp.getPal();

                                // DON'T USE COVER function
                                engine.changespritestat(SpriteNum, STAT_LIGHTING);
                                break;
                            }

                            case LIGHTING_DIFFUSE: {
                                sp.setZ(0);

                                // save off original floor and ceil shades
                                sp.setXoffset(sectp.getFloorshade());
                                sp.setYoffset(sectp.getCeilingshade());

                                // count walls of sector
                                int wallcount = 0;
                                for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                                    Wall wal = wn.get();
                                    wallcount++;
                                    if (TEST_BOOL5(sp)) {
                                        if (wal.getNextwall() >= 0) {
                                            wallcount++;
                                        }
                                    }
                                }

                                // !LIGHT
                                // make an wall_shade array and put it in User
                                USER u = SpawnUser(SpriteNum, 0, null);
                                setUser(SpriteNum, u);
                                u.WallCount = wallcount;
                                byte[] wall_shade = u.WallShade = new byte[u.WallCount]; // CallocMem(u.WallCount *
                                // sizeof(*u.WallShade), 1);

                                // save off original wall shades
                                wallcount = 0;
                                for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                                    Wall wal = wn.get();
                                    wall_shade[wallcount] = wal.getShade();
                                    wallcount++;
                                    if (TEST_BOOL5(sp)) {
                                        if (wal.getNextwall() >= 0) {
                                            Wall nextw = boardService.getWall(wal.getNextwall());
                                            if (nextw != null) {
                                                wall_shade[wallcount] = nextw.getShade();
                                                wallcount++;
                                            }
                                        }
                                    }
                                }

                                // DON'T USE COVER function
                                engine.changespritestat(SpriteNum, STAT_LIGHTING_DIFFUSE);
                                break;
                            }

                            case SECT_VATOR_DEST:
                                change_sprite_stat(SpriteNum, STAT_VATOR);
                                break;

                            case SO_WALL_DONT_MOVE_UPPER:
                                change_sprite_stat(SpriteNum, STAT_WALL_DONT_MOVE_UPPER);
                                break;

                            case SO_WALL_DONT_MOVE_LOWER:
                                change_sprite_stat(SpriteNum, STAT_WALL_DONT_MOVE_LOWER);
                                break;

                            case FLOOR_SLOPE_DONT_DRAW:
                                change_sprite_stat(SpriteNum, STAT_FLOOR_SLOPE_DONT_DRAW);
                                break;

                            case DEMO_CAMERA:
                                sp.setYvel(100); // attempt horiz control
                                sp.setZvel(100);
                                change_sprite_stat(SpriteNum, STAT_DEMO_CAMERA);
                                break;

                            case LAVA_ERUPT: {
                                USER u = SpawnUser(SpriteNum, ST1, null);

                                change_sprite_stat(SpriteNum, STAT_NO_STATE);
                                u.ActorActionFunc = DoLavaErupt;

                                // interval between erupts
                                if (SP_TAG10(sp) == 0) {
                                    SET_SP_TAG10(sp, 20);
                                }

                                // interval in seconds
                                u.WaitTics = (RANDOM_RANGE(SP_TAG10(sp)) * 120);

                                // time to erupt
                                if (SP_TAG9(sp) == 0) {
                                    SET_SP_TAG9(sp, 10);
                                }

                                sp.setZ(sp.getZ() + Z(30));

                                break;
                            }

                            case SECT_EXPLODING_CEIL_FLOOR: {
                                SetSectorWallBits(sectp, WALLFX_DONT_STICK, false, true);

                                if (TEST(sectp.getFloorstat(), FLOOR_STAT_SLOPE)) {
                                    sp.setXvel(sectp.getFloorheinum());
                                    sectp.setFloorstat(sectp.getFloorstat() & ~(FLOOR_STAT_SLOPE));
                                    sectp.setFloorheinum(0);
                                }

                                if (TEST(sectp.getCeilingstat(), CEILING_STAT_SLOPE)) {
                                    sp.setYvel(sectp.getCeilingheinum());
                                    sectp.setCeilingstat(sectp.getCeilingstat() & ~(CEILING_STAT_SLOPE));
                                    sectp.setCeilingheinum(0);
                                }

                                sp.setAng((klabs(sectp.getCeilingz() - sectp.getFloorz()) >> 8));

                                sectp.setCeilingz(sectp.getFloorz());

                                change_sprite_stat(SpriteNum, STAT_EXPLODING_CEIL_FLOOR);
                                break;
                            }

                            case SECT_COPY_SOURCE:
                                change_sprite_stat(SpriteNum, STAT_COPY_SOURCE);
                                break;

                            case SECT_COPY_DEST: {
                                SetSectorWallBits(sectp, WALLFX_DONT_STICK, false, true);
                                change_sprite_stat(SpriteNum, STAT_COPY_DEST);
                                break;
                            }

                            case SECT_WALL_MOVE:
                                change_sprite_stat(SpriteNum, STAT_WALL_MOVE);
                                break;
                            case SECT_WALL_MOVE_CANSEE:
                                change_sprite_stat(SpriteNum, STAT_WALL_MOVE_CANSEE);
                                break;

                            case SPRI_CLIMB_MARKER: {
                                // setup climb marker
                                change_sprite_stat(SpriteNum, STAT_CLIMB_MARKER);

                                // make a QUICK_LADDER sprite automatically
                                int ns = COVERinsertsprite(sp.getSectnum(), STAT_QUICK_LADDER);
                                Sprite np = boardService.getSprite(ns);
                                if (np == null) {
                                    break;
                                }

                                np.setCstat(0);
                                np.setExtra(0);
                                np.setX(sp.getX());
                                np.setY(sp.getY());
                                np.setZ(sp.getZ());
                                np.setAng(NORM_ANGLE(sp.getAng() + 1024));
                                np.setPicnum(sp.getPicnum());

                                np.setX(np.getX() + MOVEx(256 + 128, sp.getAng()));
                                np.setY(np.getY() + MOVEy(256 + 128, sp.getAng()));

                                break;
                            }

                            case SO_AUTO_TURRET:
                                change_sprite_stat(SpriteNum, STAT_ST1);
                                break;

                            case SO_DRIVABLE_ATTRIB:
                            case SO_SCALE_XY_MULT:
                            case SO_SCALE_INFO:
                            case SO_SCALE_POINT_INFO:
                            case SO_TORNADO:
                            case SO_FLOOR_MORPH:
                            case SO_AMOEBA:
                            case SO_SET_SPEED:
                            case SO_ANGLE:
                            case SO_SPIN:
                            case SO_SPIN_REVERSE:
                            case SO_BOB_START:
                            case SO_BOB_SPEED:
                            case SO_TURN_SPEED:
                            case SO_SYNC1:
                            case SO_SYNC2:
                            case SO_LIMIT_TURN:
                            case SO_MATCH_EVENT:
                            case SO_MAX_DAMAGE:
                            case SO_RAM_DAMAGE:
                            case SO_SLIDE:
                            case SO_KILLABLE:
                            case SECT_SO_SPRITE_OBJ:
                            case SECT_SO_DONT_ROTATE:
                            case SECT_SO_CLIP_DIST: {
                                // NOTE: These will get deleted by the sector
                                // object
                                // setup code

                                change_sprite_stat(SpriteNum, STAT_ST1);
                                break;
                            }

                            case SOUND_SPOT:
                                SET_SP_TAG13(sp, SP_TAG4(sp));
                                change_sprite_stat(SpriteNum, STAT_SOUND_SPOT);
                                break;

                            case STOP_SOUND_SPOT:
                                change_sprite_stat(SpriteNum, STAT_STOP_SOUND_SPOT);
                                break;

                            case SPAWN_SPOT: {
                                USER u = getUser(SpriteNum);
                                if (u == null) {
                                    SpawnUser(SpriteNum, ST1, null);
                                }

                                if (SP_TAG14(sp) == ((64 << 8) | 64)) {
                                    SET_SP_TAG14(sp, 0);
                                }

                                change_sprite_stat(SpriteNum, STAT_SPAWN_SPOT);
                                break;
                            }
                            case VIEW_THRU_CEILING:
                            case VIEW_THRU_FLOOR: {
                                ListNode<Sprite> nexti;
                                // make sure there is only one set per level of these
                                for (node = boardService.getStatNode(STAT_FAF); node != null; node = nexti) {
                                    nexti = node.getNext();
                                    if (node.get().getHitag() == sp.getHitag() && node.get().getLotag() == sp.getLotag()) {
                                        throw new WarningException("Two VIEW_THRU_ tags with same match found on level\n1: x " + sp.getX() + ", y " + sp.getY() + " \n2: x " + node.get().getX() + ", y " + node.get().getY());
                                    }
                                }
                                change_sprite_stat(SpriteNum, STAT_FAF);
                                break;
                            }

                            case VIEW_LEVEL1:
                            case VIEW_LEVEL2:
                            case VIEW_LEVEL3:
                            case VIEW_LEVEL4:
                            case VIEW_LEVEL5:
                            case VIEW_LEVEL6: {
                                change_sprite_stat(SpriteNum, STAT_FAF);
                                break;
                            }

                            case PLAX_GLOB_Z_ADJUST: {
                                sectp.setExtra(sectp.getExtra() | (SECTFX_Z_ADJUST));
                                PlaxCeilGlobZadjust = SP_TAG2(sp);
                                PlaxFloorGlobZadjust = SP_TAG3(sp);
                                KillSprite(SpriteNum);
                                break;
                            }

                            case CEILING_Z_ADJUST:
                            case FLOOR_Z_ADJUST: {
                                sectp.setExtra(sectp.getExtra() | (SECTFX_Z_ADJUST));
                                change_sprite_stat(SpriteNum, STAT_ST1);
                                break;
                            }

                            case WARP_TELEPORTER: {
                                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
                                sectp.setExtra(sectp.getExtra() | (SECTFX_WARP_SECTOR));
                                change_sprite_stat(SpriteNum, STAT_WARP);

                                // if just a destination teleporter
                                // don't set up flags
                                if (SP_TAG10(sp) == 1) {
                                    break;
                                }

                                // move the the next wall
                                int wall_num = sectp.getWallptr();
                                int start_wall = wall_num;

                                // Travel all the way around loop setting wall bits
                                do {
                                    Wall wal = boardService.getWall(wall_num);
                                    if (wal == null) {
                                        break;
                                    }

                                    // DO NOT TAG WHITE WALLS!
                                    if (wal.getNextwall() >= 0) {
                                        wal.setCstat(wal.getCstat() | (CSTAT_WALL_WARP_HITSCAN));
                                    }

                                    wall_num = wal.getPoint2();
                                } while (wall_num != start_wall);

                                break;
                            }

                            case WARP_CEILING_PLANE:
                            case WARP_FLOOR_PLANE: {
                                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
                                sectp.setExtra(sectp.getExtra() | (SECTFX_WARP_SECTOR));
                                change_sprite_stat(SpriteNum, STAT_WARP);
                                break;
                            }

                            case WARP_COPY_SPRITE1:
                                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
                                sectp.setExtra(sectp.getExtra() | (SECTFX_WARP_SECTOR));
                                change_sprite_stat(SpriteNum, STAT_WARP_COPY_SPRITE1);
                                break;
                            case WARP_COPY_SPRITE2:
                                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
                                sectp.setExtra(sectp.getExtra() | (SECTFX_WARP_SECTOR));
                                change_sprite_stat(SpriteNum, STAT_WARP_COPY_SPRITE2);
                                break;

                            case FIREBALL_TRAP:
                            case BOLT_TRAP:
                            case SPEAR_TRAP: {
                                SpawnUser(SpriteNum, 0, null);
                                sp.setOwner(-1);
                                change_sprite_stat(SpriteNum, STAT_TRAP);
                                break;
                            }

                            case SECT_SO_DONT_BOB: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());
                                sectu.flags |= (SECTFU_SO_DONT_BOB);
                                KillSprite(SpriteNum);
                                break;
                            }

                            case SECT_LOCK_DOOR: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());
                                sectu.number = sp.getLotag();
                                sectu.stag = SECT_LOCK_DOOR;
                                KillSprite(SpriteNum);
                                break;
                            }

                            case SECT_SO_SINK_DEST: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());
                                sectu.flags |= (SECTFU_SO_SINK_DEST);
                                sectu.number = sp.getLotag(); // acually the offset Z
                                // value
                                KillSprite(SpriteNum);
                                break;
                            }

                            case SECT_SO_DONT_SINK: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());
                                sectu.flags |= (SECTFU_SO_DONT_SINK);
                                KillSprite(SpriteNum);
                                break;
                            }

                            case SO_SLOPE_FLOOR_TO_POINT: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());
                                sectu.flags |= (SECTFU_SO_SLOPE_FLOOR_TO_POINT);
                                sectp.setExtra(sectp.getExtra() | (SECTFX_DYNAMIC_AREA));
                                KillSprite(SpriteNum);
                                break;
                            }

                            case SO_SLOPE_CEILING_TO_POINT: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());
                                sectu.flags |= (SECTFU_SO_SLOPE_CEILING_TO_POINT);
                                sectp.setExtra(sectp.getExtra() | (SECTFX_DYNAMIC_AREA));
                                KillSprite(SpriteNum);
                                break;
                            }
                            case SECT_SO_FORM_WHIRLPOOL: {
                                Sect_User sectu = SetupSectUser(sp.getSectnum());
                                sectu.stag = SECT_SO_FORM_WHIRLPOOL;
                                sectu.height = sp.getLotag();
                                KillSprite(SpriteNum);
                                break;
                            }

                            case SECT_ACTOR_BLOCK: {
                                // move the the next wall
                                int wall_num = sectp.getWallptr();
                                int start_wall = wall_num;

                                // Travel all the way around loop setting wall bits
                                do {
                                    Wall wal = boardService.getWall(wall_num);
                                    if (wal == null) {
                                        break;
                                    }

                                    wal.setCstat(wal.getCstat() | (CSTAT_WALL_BLOCK_ACTOR));
                                    if (wal.getNextwall() >= 0) {
                                        Wall wal2 = boardService.getWall(wal.getNextwall());
                                        if (wal2 != null) {
                                            wal2.setCstat(wal2.getCstat() | (CSTAT_WALL_BLOCK_ACTOR));
                                        }
                                    }
                                    wall_num = wal.getPoint2();
                                } while (wall_num != start_wall);

                                KillSprite(SpriteNum);
                                break;
                            }
                        }
                    }
                }
                break;

                case RED_CARD:
                case RED_KEY:
                case BLUE_CARD:
                case BLUE_KEY:
                case GREEN_CARD:
                case GREEN_KEY:
                case YELLOW_CARD:
                case YELLOW_KEY:
                case GOLD_SKELKEY:
                case SILVER_SKELKEY:
                case BRONZE_SKELKEY:
                case RED_SKELKEY: {
                    int num = 0;
                    switch (sp.getPicnum()) {
                        case RED_CARD:
                            num = 4;
                            break;
                        case RED_KEY:
                            break;
                        case BLUE_CARD:
                            num = 5;
                            break;
                        case BLUE_KEY:
                            num = 1;
                            break;
                        case GREEN_CARD:
                            num = 6;
                            break;
                        case GREEN_KEY:
                            num = 2;
                            break;
                        case YELLOW_CARD:
                            num = 7;
                            break;
                        case YELLOW_KEY:
                            num = 3;
                            break;
                        case GOLD_SKELKEY:
                            num = 8;
                            break;
                        case SILVER_SKELKEY:
                            num = 9;
                            break;
                        case BRONZE_SKELKEY:
                            num = 10;
                            break;
                        case RED_SKELKEY:
                            num = 11;
                            break;
                    }

                    if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_AI_BOTS) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    USER u = SpawnUser(SpriteNum, 0, null);

                    sp.setPicnum(u.ID = sp.getPicnum());

                    u.spal = (byte) sp.getPal(); // Set the palette from build

                    ChangeState(SpriteNum, s_Key[num][0]);
                    engine.getTile(sp.getPicnum()).setAnimType(ru.m210projects.Build.Types.AnimType.NONE);
                    engine.getTile(sp.getPicnum() + 1).setAnimType(ru.m210projects.Build.Types.AnimType.NONE);
                    change_sprite_stat(SpriteNum, STAT_ITEM);
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN | CSTAT_SPRITE_ONE_SIDE));
                    u.Radius = 500;
                    sp.setHitag(LUMINOUS); // Set so keys over ride colored lighting

                    DoActorZrange(SpriteNum);
                }
                break;

                // Used for multiplayer locks
                case 1846:
                case 1850:
                case 1852:
                case 2470:
                    if (TEST(sp.getExtra(), SPRX_MULTI_ITEM)) {
                        if ((numplayers <= 1 && !gNet.FakeMultiplayer) || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COOPERATIVE) {
                            KillSprite(SpriteNum);
                        }
                    }
                    break;

                case FIRE_FLY0:
                    break;

                case ICON_REPAIR_KIT: {

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_REPAIR_KIT, s_RepairKit[0]);

                    IconDefault(SpriteNum);
                    break;
                }
                case ICON_STAR: {

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_STAR, s_IconStar[0]);

                    IconDefault(SpriteNum);
                    break;
                }
                case ICON_LG_MINE:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_LG_MINE, s_IconLgMine[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_MICRO_GUN:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_MICRO_GUN, s_IconMicroGun[0]);

                    IconDefault(SpriteNum);
                    break;

                case ICON_MICRO_BATTERY:
                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_MICRO_BATTERY, s_IconMicroBattery[0]);

                    IconDefault(SpriteNum);
                    break;

                case ICON_UZI:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_UZI, s_IconUzi[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_UZIFLOOR:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_UZIFLOOR, s_IconUziFloor[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_LG_UZI_AMMO:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_LG_UZI_AMMO, s_IconLgUziAmmo[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_GRENADE_LAUNCHER:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_GRENADE_LAUNCHER, s_IconGrenadeLauncher[0]);

                    IconDefault(SpriteNum);
                    break;

                case ICON_LG_GRENADE:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_LG_GRENADE, s_IconLgGrenade[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_RAIL_GUN:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_RAIL_GUN, s_IconRailGun[0]);

                    IconDefault(SpriteNum);
                    break;

                case ICON_RAIL_AMMO:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_RAIL_AMMO, s_IconRailAmmo[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_ROCKET:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_ROCKET, s_IconRocket[0]);

                    IconDefault(SpriteNum);
                    break;

                case ICON_LG_ROCKET:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_LG_ROCKET, s_IconLgRocket[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_SHOTGUN: {

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    USER u = SpawnUser(SpriteNum, ICON_SHOTGUN, s_IconShotgun[0]);
                    u.Radius = 350; // Shotgun is hard to pick up for some reason.

                    IconDefault(SpriteNum);
                    break;
                }
                case ICON_LG_SHOTSHELL:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_LG_SHOTSHELL, s_IconLgShotshell[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_AUTORIOT:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_AUTORIOT, s_IconAutoRiot[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_GUARD_HEAD:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_GUARD_HEAD, s_IconGuardHead[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_FIREBALL_LG_AMMO:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_FIREBALL_LG_AMMO, s_IconFireballLgAmmo[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_HEART:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_HEART, s_IconHeart[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_HEART_LG_AMMO:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_HEART_LG_AMMO, s_IconHeartLgAmmo[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_SPELL:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_SPELL, s_IconSpell[0]);
                    IconDefault(SpriteNum);

                    PicAnimOff(sp.getPicnum());
                    break;

                case ICON_ARMOR: {

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    USER u = SpawnUser(SpriteNum, ICON_ARMOR, s_IconArmor[0]);
                    if (sp.getPal() != PALETTE_PLAYER3) {
                        sp.setPal(u.spal = PALETTE_PLAYER1);
                    } else {
                        sp.setPal(u.spal = PALETTE_PLAYER3);
                    }
                    IconDefault(SpriteNum);
                    break;
                }
                case ICON_MEDKIT:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_MEDKIT, s_IconMedkit[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_SM_MEDKIT:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_SM_MEDKIT, s_IconSmMedkit[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_CHEMBOMB:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_CHEMBOMB, s_IconChemBomb[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_FLASHBOMB:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_FLASHBOMB, s_IconFlashBomb[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_NUKE:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    if (gNet.MultiGameType != null) {
                        if (!gNet.Nuke) {
                            SpawnUser(SpriteNum, ICON_MICRO_BATTERY, s_IconMicroBattery[0]);
                            IconDefault(SpriteNum);
                            break;
                        }
                    }

                    SpawnUser(SpriteNum, ICON_NUKE, s_IconNuke[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_CALTROPS:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_CALTROPS, s_IconCaltrops[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_BOOSTER:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_BOOSTER, s_IconBooster[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_HEAT_CARD:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_HEAT_CARD, s_IconHeatCard[0]);
                    IconDefault(SpriteNum);
                    break;

                case ICON_CLOAK:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_CLOAK, s_IconCloak[0]);
                    IconDefault(SpriteNum);
                    PicAnimOff(sp.getPicnum());
                    break;

                case ICON_FLY:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_FLY, s_IconFly[0]);
                    IconDefault(SpriteNum);
                    PicAnimOff(sp.getPicnum());
                    break;

                case ICON_NIGHT_VISION:

                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    SpawnUser(SpriteNum, ICON_NIGHT_VISION, s_IconNightVision[0]);
                    IconDefault(SpriteNum);
                    PicAnimOff(sp.getPicnum());
                    break;

                case ICON_FLAG: {
                    if (!IconSpawn(sp)) {
                        KillSprite(SpriteNum);
                        break;
                    }

                    USER u = SpawnUser(SpriteNum, ICON_FLAG, s_IconFlag[0]);
                    u.spal = (byte) sp.getPal();
                    Sector sectp = boardService.getSector(sp.getSectnum());
                    if (sectp != null) {
                        sectp.setHitag(9000); // Put flag's color in sect containing it
                        sectp.setLotag(u.spal);
                    }
                    IconDefault(SpriteNum);
                    PicAnimOff(sp.getPicnum());
                    break;
                }
                case 3143:
                case 3157: {
                    USER u = SpawnUser(SpriteNum, sp.getPicnum(), null);

                    change_sprite_stat(SpriteNum, STAT_STATIC_FIRE);

                    u.ID = FIREBALL_FLAMES;
                    u.Radius = 200;
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK_HITSCAN));

                    sp.setHitag(LUMINOUS); // Always full brightness
                    sp.setShade(-40);

                    break;
                }

                // blades
                case BLADE1:
                case BLADE2:
                case BLADE3:
                case 5011: {
                    SpawnUser(SpriteNum, sp.getPicnum(), null);

                    change_sprite_stat(SpriteNum, STAT_DEFAULT);

                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK_HITSCAN));
                    sp.setExtra(sp.getExtra() | (SPRX_BLADE));

                    break;
                }

                case BREAK_LIGHT:
                case BREAK_BARREL:
                case BREAK_PEDISTAL:
                case BREAK_BOTTLE1:
                case BREAK_BOTTLE2:
                case BREAK_MUSHROOM:
                    SpawnUser(SpriteNum, sp.getPicnum(), null);

                    sp.setClipdist(SPRITEp_SIZE_X(sp));
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BREAKABLE));
                    sp.setExtra(sp.getExtra() | (SPRX_BREAKABLE));
                    break;

                // switches
                case 581:
                case 582:
                case 558:
                case 559:
                case 560:
                case 561:
                case 562:
                case 563:
                case 564:
                case 565:
                case 566:
                case 567:
                case 568:
                case 569:
                case 570:
                case 571:
                case 572:
                case 573:
                case 574:

                case 551:
                case 552:
                case 575:
                case 576:
                case 577:
                case 578:
                case 579:
                case 589:
                case 583:
                case 584:

                case 553:
                case 554: {
                    if (TEST(sp.getExtra(), SPRX_MULTI_ITEM)) {
                        if ((numplayers <= 1 && !gNet.FakeMultiplayer) || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COOPERATIVE) {
                            KillSprite(SpriteNum);
                            break;
                        }
                    }

                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                    break;
                }

            }
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean ItemSpotClear(Sprite sip, int statnum, int id) {
        boolean found = false;
        if (TEST_BOOL2(sip)) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getSectNode(sip.getSectnum()); node != null; node = nexti) {
                nexti = node.getNext();
                Sprite sp = node.get();
                USER u = getUser(node.getIndex());
                if (u != null && sp.getStatnum() == statnum && u.ID == id) {
                    found = true;
                    break;
                }
            }
        }

        return (!found);
    }    public static final State[] s_GreenCoin = {new State(Green_COIN, GREEN_COIN_RATE, DoCoin), new State(Green_COIN + 1, GREEN_COIN_RATE, DoCoin), new State(Green_COIN + 2, GREEN_COIN_RATE, DoCoin), new State(Green_COIN + 3, GREEN_COIN_RATE, DoCoin), new State(Green_COIN + 4, GREEN_COIN_RATE, DoCoin), new State(Green_COIN + 5, GREEN_COIN_RATE, DoCoin), new State(Green_COIN + 6, GREEN_COIN_RATE, DoCoin), new State(Green_COIN + 7, GREEN_COIN_RATE, DoCoin)};

    public static void SetupItemForJump(Sprite sip, int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        // setup item for jumping
        if (SP_TAG7(sip) != 0) {
            USER u = getUser(SpriteNum);
            if (u == null) {
                return;
            }

            change_sprite_stat(SpriteNum, STAT_SKIP4);
            u.ceiling_dist = Z(6);
            u.floor_dist = Z(0);
            u.Counter = 0;

            sp.setXvel((SP_TAG7(sip) << 2));
            sp.setZvel(-((SP_TAG8(sip)) << 5));

            u.xchange = MOVEx(sp.getXvel(), sp.getAng());
            u.ychange = MOVEy(sp.getXvel(), sp.getAng());
            u.zchange = sp.getZvel();
        }
    }

    public static void ActorCoughItem(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        switch (u.ID) {
            case SAILORGIRL_R0: {
                int newsp = COVERinsertsprite(sp.getSectnum(), STAT_SPAWN_ITEMS);
                Sprite np = boardService.getSprite(newsp);
                if (np == null) {
                    break;
                }

                np.setCstat(0);
                np.setExtra(0);
                np.setX(sp.getX());
                np.setY(sp.getY());
                np.setZ(SPRITEp_MID(sp));
                np.setAng(0);
                np.setExtra(0);

                // vel | zvel
                np.setZvel(40 | (1 << 8));

                int choose = RANDOM_P2(1024);

                if (choose > 854) {
                    np.setClipdist(91); // Match number
                } else if (choose > 684) {
                    np.setClipdist(48); // Match number
                } else if (choose > 514) {
                    np.setClipdist(58); // Match number
                } else if (choose > 344) {
                    np.setClipdist(60); // Match number
                } else if (choose > 174) {
                    np.setClipdist(62); // Match number
                } else {
                    np.setClipdist(68); // Match number
                }

                // match
                np.setLotag(-1);
                // kill
                RESET_BOOL1(np);
                SpawnItemsMatch(-1);
                break;
            }
            case GORO_RUN_R0: {
                if (RANDOM_P2(1024) < 700) {
                    return;
                }

                int newsp = COVERinsertsprite(sp.getSectnum(), STAT_SPAWN_ITEMS);
                Sprite np = boardService.getSprite(newsp);
                if (np == null) {
                    break;
                }

                np.setCstat(0);
                np.setExtra(0);
                np.setX(sp.getX());
                np.setY(sp.getY());
                np.setZ(SPRITEp_MID(sp));
                np.setAng(0);
                np.setExtra(0);

                // vel | zvel
                np.setZvel(40 | (1 << 8));

                np.setClipdist(69); // Match number

                // match
                np.setLotag(-1);
                // kill
                RESET_BOOL1(np);
                SpawnItemsMatch(-1);
                break;
            }
            case RIPPER2_RUN_R0: {
                if (RANDOM_P2(1024) < 700) {
                    return;
                }

                int newsp = COVERinsertsprite(sp.getSectnum(), STAT_SPAWN_ITEMS);
                Sprite np = boardService.getSprite(newsp);
                if (np == null) {
                    break;
                }

                np.setCstat(0);
                np.setExtra(0);
                np.setX(sp.getX());
                np.setY(sp.getY());
                np.setZ(SPRITEp_MID(sp));
                np.setAng(0);
                np.setExtra(0);

                // vel | zvel
                np.setZvel(40 | (1 << 8));

                np.setClipdist(70); // Match number

                // match
                np.setLotag(-1);
                // kill
                RESET_BOOL1(np);
                SpawnItemsMatch(-1);
                break;
            }
            case NINJA_RUN_R0: {

                if (u.PlayerP != -1) {
                    if (RANDOM_P2(1024) > 200) {
                        return;
                    }

                    int newsp = COVERinsertsprite(sp.getSectnum(), STAT_SPAWN_ITEMS);
                    Sprite np = boardService.getSprite(newsp);
                    if (np == null) {
                        break;
                    }

                    np.setCstat(0);
                    np.setExtra(0);
                    np.setX(sp.getX());
                    np.setY(sp.getY());
                    np.setZ(SPRITEp_MID(sp));
                    np.setAng(0);
                    np.setExtra(0);

                    // vel | zvel
                    np.setZvel(40 | (1 << 8));

                    switch (u.WeaponNum) {
                        case WPN_UZI:
                            np.setClipdist(0);
                            break;
                        case WPN_SHOTGUN:
                            np.setClipdist(51);
                            break;
                        case WPN_STAR:
                            if (Player[u.PlayerP].WpnAmmo[WPN_STAR] < 9) {
                                break;
                            }
                            np.setClipdist(41);
                            break;
                        case WPN_MINE:
                            if (Player[u.PlayerP].WpnAmmo[WPN_MINE] < 5) {
                                break;
                            }
                            np.setClipdist(42);
                            break;
                        case WPN_MICRO:
                        case WPN_ROCKET:
                            np.setClipdist(43);
                            break;
                        case WPN_GRENADE:
                            np.setClipdist(45);
                            break;
                        case WPN_RAIL:
                            np.setClipdist(47);
                            break;
                        case WPN_HEART:
                            np.setClipdist(55);
                            break;
                        case WPN_HOTHEAD:
                            np.setClipdist(53);
                            break;
                    }

                    // match
                    np.setLotag(-1);
                    // kill
                    RESET_BOOL1(np);
                    SpawnItemsMatch(-1);
                    break;
                }

                if (RANDOM_P2(1024) < 512) {
                    return;
                }

                int newsp = COVERinsertsprite(sp.getSectnum(), STAT_SPAWN_ITEMS);
                Sprite np = boardService.getSprite(newsp);
                if (np == null) {
                    break;
                }

                np.setCstat(0);
                np.setExtra(0);
                np.setX(sp.getX());
                np.setY(sp.getY());
                np.setZ(SPRITEp_MID(sp));
                np.setAng(0);
                np.setExtra(0);

                // vel | zvel
                np.setZvel(40 | (1 << 8));

                if (u.spal == PAL_XLAT_LT_TAN) {
                    np.setClipdist(44);
                } else if (u.spal == PAL_XLAT_LT_GREY) {
                    np.setClipdist(46);
                } else if (u.spal == PALETTE_PLAYER5) // Green Ninja
                {
                    if (RANDOM_P2(1024) < 700) {
                        np.setClipdist(61);
                    } else {
                        np.setClipdist(60);
                    }
                } else if (u.spal == PALETTE_PLAYER3) // Red Ninja
                {
                    // type
                    if (RANDOM_P2(1024) < 800) {
                        np.setClipdist(68);
                    } else {
                        np.setClipdist(44);
                    }
                } else {
                    if (RANDOM_P2(1024) < 512) {
                        np.setClipdist(41);
                    } else {
                        np.setClipdist(68);
                    }
                }

                // match
                np.setLotag(-1);
                // kill
                RESET_BOOL1(np);
                SpawnItemsMatch(-1);
                break;
            }
            case PACHINKO1:
            case PACHINKO2:
            case PACHINKO3:
            case PACHINKO4: {

                int newsp = COVERinsertsprite(sp.getSectnum(), STAT_SPAWN_ITEMS);
                Sprite np = boardService.getSprite(newsp);
                if (np == null) {
                    break;
                }

                np.setCstat(0);
                np.setExtra(0);
                np.setX(sp.getX());
                np.setY(sp.getY());
                np.setZ(SPRITEp_LOWER(sp) + Z(10));
                np.setAng(sp.getAng());

                // vel | zvel
                np.setZvel(10 | (10 << 8));

                if (u.ID == PACHINKO1) {
                    if (RANDOM_P2(1024) < 600) {
                        np.setClipdist(64); // Small MedKit
                    } else {
                        np.setClipdist(59); // Fortune Cookie
                    }
                } else if (u.ID == PACHINKO2) {
                    if (RANDOM_P2(1024) < 600) {
                        np.setClipdist(52); // Lg Shot Shell
                    } else {
                        np.setClipdist(68); // Uzi clip
                    }
                } else if (u.ID == PACHINKO3) {
                    if (RANDOM_P2(1024) < 600) {
                        np.setClipdist(57);
                    } else {
                        np.setClipdist(63);
                    }
                } else if (u.ID == PACHINKO4) {
                    if (RANDOM_P2(1024) < 600) {
                        np.setClipdist(60);
                    } else {
                        np.setClipdist(61);
                    }
                }

                // match
                np.setLotag(-1);
                // kill
                RESET_BOOL1(np);
                SpawnItemsMatch(-1);
                break;
            }
        }
    }

    public static void SpawnItemsMatch(int match) {
        ListNode<Sprite> nextsi;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPAWN_ITEMS); node != null; node = nextsi) {
            int si = node.getIndex();
            nextsi = node.getNext();
            Sprite sip = node.get();

            if (SP_TAG2(sip) != match) {
                continue;
            }

            switch (SP_TAG3(sip)) {
                case 90: {
                    int SpriteNum = BunnyHatch2(si);
                    Sprite sp = boardService.getSprite(SpriteNum);
                    USER u = getUser(SpriteNum);
                    if (sp == null || u == null) {
                        break;
                    }

                    u.spal = PALETTE_PLAYER8;
                    sp.setPal(PALETTE_PLAYER8); // Boy
                    sp.setAng(sip.getAng());
                    break;
                }
                case 91: {
                    int SpriteNum = BunnyHatch2(si);
                    Sprite sp = boardService.getSprite(SpriteNum);
                    USER u = getUser(SpriteNum);
                    if (sp == null || u == null) {
                        break;
                    }

                    u.spal = PALETTE_PLAYER0;
                    sp.setPal(PALETTE_PLAYER0); // Girl
                    sp.setAng(sip.getAng());
                    break;
                }
                case 92: {
                    int SpriteNum = BunnyHatch2(si);
                    Sprite sp = boardService.getSprite(SpriteNum);
                    if (sp != null) {
                        sp.setAng(sip.getAng());
                    }
                    break;
                }
                case 40: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_REPAIR_KIT)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_REPAIR_KIT, s_RepairKit[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 41: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_STAR)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_STAR, s_IconStar[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 42: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_LG_MINE)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_LG_MINE, s_IconLgMine[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 43: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_MICRO_GUN)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_MICRO_GUN, s_IconMicroGun[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 44: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_MICRO_BATTERY)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_MICRO_BATTERY, s_IconMicroBattery[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 45: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_GRENADE_LAUNCHER)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_GRENADE_LAUNCHER, s_IconGrenadeLauncher[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 46: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_LG_GRENADE)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_LG_GRENADE, s_IconLgGrenade[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 47: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_RAIL_GUN)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_RAIL_GUN, s_IconRailGun[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 48: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_RAIL_AMMO)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_RAIL_AMMO, s_IconRailAmmo[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 49: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_ROCKET)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_ROCKET, s_IconRocket[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 51: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_SHOTGUN)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_SHOTGUN, s_IconShotgun[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 52: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_LG_SHOTSHELL)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_LG_SHOTSHELL, s_IconLgShotshell[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 53: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_GUARD_HEAD)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_GUARD_HEAD, s_IconGuardHead[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 54: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_FIREBALL_LG_AMMO)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_FIREBALL_LG_AMMO, s_IconFireballLgAmmo[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 55: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_HEART)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_HEART, s_IconHeart[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 56: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_HEART_LG_AMMO)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_HEART_LG_AMMO, s_IconHeartLgAmmo[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 57: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_ARMOR)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_ARMOR, s_IconArmor[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    Sprite sp = boardService.getSprite(SpriteNum);
                    USER u = getUser(SpriteNum);
                    if (sp == null || u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);

                    if (sp.getPal() != PALETTE_PLAYER3) {
                        sp.setPal(u.spal = PALETTE_PLAYER1);
                    } else {
                        sp.setPal(u.spal = PALETTE_PLAYER3);
                    }
                    break;
                }

                case 58: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_MEDKIT)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_MEDKIT, s_IconMedkit[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 59: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_SM_MEDKIT)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_SM_MEDKIT, s_IconSmMedkit[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 60: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_CHEMBOMB)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_CHEMBOMB, s_IconChemBomb[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 61: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_FLASHBOMB)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_FLASHBOMB, s_IconFlashBomb[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 62: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_NUKE)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_NUKE, s_IconNuke[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 63: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_CALTROPS)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_CALTROPS, s_IconCaltrops[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 64: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_BOOSTER)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_BOOSTER, s_IconBooster[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 65: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_HEAT_CARD)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_HEAT_CARD, s_IconHeatCard[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 66: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_CLOAK)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_CLOAK, s_IconCloak[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 67: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_NIGHT_VISION)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_NIGHT_VISION, s_IconNightVision[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 68: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_LG_UZI_AMMO)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_LG_UZI_AMMO, s_IconLgUziAmmo[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 69: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_GUARD_HEAD)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_GUARD_HEAD, s_IconGuardHead[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 70: {
                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_HEART)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_HEART, s_IconHeart[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 20: {

                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_UZIFLOOR)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_UZIFLOOR, s_IconUziFloor[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 32:
                case 0: {

                    if (!ItemSpotClear(sip, STAT_ITEM, ICON_UZI)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, ICON_UZI, s_IconUzi[0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    if (u == null) {
                        break;
                    }

                    u.Flags2 |= (SPR2_NEVER_RESPAWN);
                    IconDefault(SpriteNum);

                    SetupItemForJump(sip, SpriteNum);
                    break;
                }
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12: {
                    if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_AI_BOTS) {
                        break;
                    }

                    int num = (SP_TAG3(sip) - 1);

                    if (!ItemSpotClear(sip, STAT_ITEM, s_Key[num][0].Pic)) {
                        break;
                    }

                    int SpriteNum = SpawnSprite(STAT_ITEM, s_Key[num][0].Pic, s_Key[num][0], sip.getSectnum(), sip.getX(), sip.getY(), sip.getZ(), sip.getAng(), 0);
                    USER u = getUser(SpriteNum);
                    Sprite sp = boardService.getSprite(SpriteNum);
                    if (sp == null || u == null) {
                        break;
                    }

                    u.ID = s_Key[num][0].Pic;
                    sp.setPicnum(u.ID);

                    // need to set the palette here - suggest table lookup
                    u.spal = (byte) KeyPal[num];
                    sp.setPal(u.spal); // Set the palette of the flag

                    ChangeState(SpriteNum, s_Key[num][0]);

                    engine.getTile(sp.getPicnum()).setAnimType(ru.m210projects.Build.Types.AnimType.NONE);
                    engine.getTile(sp.getPicnum() + 1).setAnimType(ru.m210projects.Build.Types.AnimType.NONE);

                    SetupItemForJump(sip, SpriteNum);

                    break;
                }
            }

            if (!TEST_BOOL1(sip)) {
                KillSprite(si);
            }
        }
    }    // !JIM! Added rail crap

    public static void NewStateGroup(int SpriteNum, StateGroup StateGroup) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null || StateGroup == null) {
            return;
        }

        // Kind of a goofy check, but it should catch alot of invalid states!
        // BTW, 6144 is the max tile number allowed in editart.
        if (u.State != null && !isValidTile(u.State.Pic)) {
            return;
        }

        u.setRot(StateGroup);
        u.State = u.StateStart = StateGroup.getState(0);
        u.Tics = 0;

        // turn anims off because people keep setting them in the art file
        engine.getTile(sp.getPicnum()).setAnimType(ru.m210projects.Build.Types.AnimType.NONE);
    }    public static final State[] s_IconStar = {new State(ICON_STAR, 100, DoGet).setNext()};

    public static boolean SpriteOverlap(int spritenum_a, int spritenum_b) {
        Sprite spa = boardService.getSprite(spritenum_a);
        Sprite spb = boardService.getSprite(spritenum_b);

        USER ua = getUser(spritenum_a);
        USER ub = getUser(spritenum_b);

        if (spa == null || spb == null || ua == null || ub == null) {
            return false;
        }

        if (Distance(spa.getX(), spa.getY(), spb.getX(), spb.getY()) > ua.Radius + ub.Radius) {
            return (false);
        }

        int spa_tos = SPRITEp_TOS(spa);
        int spa_bos = SPRITEp_BOS(spa);

        int spb_tos = SPRITEp_TOS(spb);
        int spb_bos = SPRITEp_BOS(spb);

        int overlap_z = ua.OverlapZ + ub.OverlapZ;

        // if the top of sprite a is below the bottom of b
        if (spa_tos - overlap_z > spb_bos) {
            return (false);
        }

        // if the top of sprite b is is below the bottom of a
        return spb_tos - overlap_z <= spa_bos;
    }

    public static boolean SpriteOverlapZ(int spritenum_a, int spritenum_b, int z_overlap) {
        Sprite spa = boardService.getSprite(spritenum_a);
        Sprite spb = boardService.getSprite(spritenum_b);
        if (spa == null || spb == null) {
            return false;
        }

        int spa_tos = SPRITEp_TOS(spa);
        int spa_bos = SPRITEp_BOS(spa);
        int spb_tos = SPRITEp_TOS(spb);
        int spb_bos = SPRITEp_BOS(spb);

        // if the top of sprite a is below the bottom of b
        if (spa_tos + z_overlap > spb_bos) {
            return (false);
        }

        // if the top of sprite b is is below the bottom of a
        return spb_tos + z_overlap <= spa_bos;
    }    public static final State[] s_IconUzi = {new State(ICON_UZI, 100, DoGet).setNext()};

    public static void DoActorZrange(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int save_cstat = DTEST(sp.getCstat(), CSTAT_SPRITE_BLOCK);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
        FAFgetzrange(sp.getX(), sp.getY(), sp.getZ() - DIV2(SPRITEp_SIZE_Z(sp)), sp.getSectnum(), tmp_ptr[0].set(0), tmp_ptr[1].set(0), tmp_ptr[2].set(0), tmp_ptr[3].set(0), ((sp.getClipdist()) << 2) - GETZRANGE_CLIP_ADJ, CLIPMASK_ACTOR);
        u.hiz = tmp_ptr[0].value;
        int ceilhit = tmp_ptr[1].value;
        u.loz = tmp_ptr[2].value;
        int florhit = tmp_ptr[3].value;

        sp.setCstat(sp.getCstat() | (save_cstat));

        u.lo_sectp = u.hi_sectp = -1;
        u.lo_sp = u.hi_sp = -1;

        switch (DTEST(ceilhit, HIT_MASK)) {
            case HIT_SPRITE:
                u.hi_sp = NORM_HIT_INDEX(ceilhit);
                break;
            case HIT_SECTOR:
                u.hi_sectp = NORM_HIT_INDEX(ceilhit);
                break;
            default:
                break;
        }

        switch (DTEST(florhit, HIT_MASK)) {
            case HIT_SPRITE:
                u.lo_sp = NORM_HIT_INDEX(florhit);
                break;
            case HIT_SECTOR:
                u.lo_sectp = NORM_HIT_INDEX(florhit);
                break;
            default:
                break;
        }
    }

    public static void DoActorGlobZ(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.loz = globloz;
        u.hiz = globhiz;

        u.lo_sectp = u.hi_sectp = -1;
        u.lo_sp = u.hi_sp = -1;

        if (DTEST(globhihit, HIT_MASK) == HIT_SPRITE) {
            u.hi_sp = NORM_HIT_INDEX(globhihit);
        } else {
            u.hi_sectp = NORM_HIT_INDEX(globhihit);
        }

        if (DTEST(globlohit, HIT_MASK) == HIT_SPRITE) {
            u.lo_sp = NORM_HIT_INDEX(globlohit);
        } else {
            u.lo_sectp = NORM_HIT_INDEX(globlohit);
        }

    }    public static final State[] s_IconLgUziAmmo = {new State(ICON_LG_UZI_AMMO, 100, DoGet).setNext()};

    public static boolean ActorDrop(int SpriteNum, int x, int y, int z, int new_sector, int min_height) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return false;
        }

        // look only at the center point for a floor sprite
        int save_cstat = DTEST(sp.getCstat(), CSTAT_SPRITE_BLOCK);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
        FAFgetzrangepoint(x, y, z - DIV2(SPRITEp_SIZE_Z(sp)), new_sector, tmp_ptr[0].set(0), tmp_ptr[1].set(0), tmp_ptr[2].set(0), tmp_ptr[3].set(0)); // &hiz, &ceilhit, &loz, &florhit);

        int ceilhit = tmp_ptr[1].value;
        int loz = tmp_ptr[2].value;
        int florhit = tmp_ptr[3].value;

        sp.setCstat(sp.getCstat() | (save_cstat));

        if (florhit == -1 || ceilhit == -1) {
            return (true);
        }

        switch (DTEST(florhit, HIT_MASK)) {
            case HIT_SPRITE: {
                Sprite hsp = boardService.getSprite(NORM_HIT_INDEX(florhit));
                // if its a floor sprite and not too far down
                if (hsp != null && TEST(hsp.getCstat(), CSTAT_SPRITE_FLOOR) && (klabs(loz - z) <= min_height)) {
                    return (false);
                }

                break;
            }

            case HIT_SECTOR: {
                if (klabs(loz - z) <= min_height) {
                    return (false);
                }
                break;
            }
            default:
                break;
        }

        return (true);
    }

    // Primarily used in ai.c for now - need to get rid of
    public static boolean DropAhead(int SpriteNum, int min_height) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return false;
        }

        int dax = sp.getX() + MOVEx(256, sp.getAng());
        int day = sp.getY() + MOVEy(256, sp.getAng());
        int newsector = COVERupdatesector(dax, day, sp.getSectnum());

        // look straight down for a drop
        return ActorDrop(SpriteNum, dax, day, sp.getZ(), newsector, min_height);
    }    public static final State[] s_IconUziFloor = {new State(ICON_UZIFLOOR, 100, DoGet).setNext()};

    public static boolean move_actor(int SpriteNum, int xchange, int ychange, int zchange) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        if (TEST(u.Flags, SPR_NO_SCAREDZ)) {
            // For COOLG & HORNETS
            // set to actual z before you move
            sp.setZ(u.sz);
        }

        // save off x,y values
        int x = sp.getX();
        int y = sp.getY();
        int z = sp.getZ();
        int loz = u.loz;
        int hiz = u.hiz;
        int lo_sp = u.lo_sp;
        int hi_sp = u.hi_sp;
        int lo_sectp = u.lo_sectp;
        int hi_sectp = u.hi_sectp;
        int sectnum = sp.getSectnum();

        clipmoveboxtracenum = 1;
        u.moveSpriteReturn = move_sprite(SpriteNum, xchange, ychange, zchange, u.ceiling_dist, u.floor_dist, CLIPMASK_ACTOR, ACTORMOVETICS);
        clipmoveboxtracenum = 3;

        // try and determine whether you moved > lo_step in the z direction
        if (!TEST(u.Flags, SPR_NO_SCAREDZ | SPR_JUMPING | SPR_CLIMBING | SPR_FALLING | SPR_DEAD | SPR_SWIMMING)) {
            if (klabs(sp.getZ() - globloz) > u.lo_step) {
                // cancel move
                sp.setX(x);
                sp.setY(y);
                sp.setZ(z);

                u.loz = loz;
                u.hiz = hiz;
                u.lo_sp = lo_sp;
                u.hi_sp = hi_sp;
                u.lo_sectp = lo_sectp;
                u.hi_sectp = hi_sectp;
                u.moveSpriteReturn = -1;
                engine.changespritesect(SpriteNum, sectnum);
                return (false);
            }

            if (ActorDrop(SpriteNum, sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), u.lo_step)) {
                // cancel move
                sp.setX(x);
                sp.setY(y);
                sp.setZ(z);

                u.loz = loz;
                u.hiz = hiz;
                u.lo_sp = lo_sp;
                u.hi_sp = hi_sp;
                u.lo_sectp = lo_sectp;
                u.hi_sectp = hi_sectp;
                u.moveSpriteReturn = -1;
                engine.changespritesect(SpriteNum, sectnum);
                return (false);
            }
        }

        u.Flags |= (SPR_MOVED);

        if (u.moveSpriteReturn == 0) {
            // Keep track of how far sprite has moved
            int dist = Distance(x, y, sp.getX(), sp.getY());
            u.TargetDist -= dist;
            u.Dist += dist;
            u.DistCheck += (short) dist;
            return (true);
        } else {
            return (false);
        }
    }

    public static void DoGrating(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        game.pInt.setsprinterpolate(SpriteNum, sp);

        // reduce to 0 to 3 value
        int dir = sp.getAng() >> 9;

        if (MOD2(dir) == 0) {
            if (dir == 0) {
                sp.setX(sp.getX() + 2 * GRATE_FACTOR);
            } else {
                sp.setX(sp.getX() - 2 * GRATE_FACTOR);
            }
        } else {
            if (dir == 1) {
                sp.setY(sp.getY() + 2 * GRATE_FACTOR);
            } else {
                sp.setY(sp.getY() - 2 * GRATE_FACTOR);
            }
        }

        sp.setHitag(sp.getHitag() - GRATE_FACTOR);

        if (sp.getHitag() <= 0) {
            change_sprite_stat(SpriteNum, STAT_DEFAULT);
            setUser(SpriteNum, null);
        }

        engine.setspritez(SpriteNum, sp.getX(), sp.getY(), sp.getZ());
    }    public static final State[] s_IconRocket = {new State(ICON_ROCKET, 100, DoGet).setNext()};

    public static void DoSpriteFade(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // adjust Shade based on clock

        for (int i = 0; i < ACTORMOVETICS; i++) {
            if (TEST(u.Flags, SPR_SHADE_DIR)) {
                sp.setShade(sp.getShade() + 1);

                if (sp.getShade() >= 10) {
                    u.Flags &= ~(SPR_SHADE_DIR);
                }
            } else {
                sp.setShade(sp.getShade() - 1);

                if (sp.getShade() <= -40) {
                    u.Flags |= (SPR_SHADE_DIR);
                }
            }
        }
    }

    public static void DoKey(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null) {
            sp.setAng(NORM_ANGLE(sp.getAng() + (14 * ACTORMOVETICS)));
        }
        DoGet(SpriteNum);
    }    public static final State[] s_IconLgRocket = {new State(ICON_LG_ROCKET, 100, DoGet).setNext()};

    public static void DoCoin(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.WaitTics -= ACTORMOVETICS * 2;

        if (u.WaitTics <= 0) {
            KillSprite(SpriteNum);
            return;
        }

        if (u.WaitTics < 10 * 120) {
            if (u.StateStart != s_GreenCoin[0]) {
                int offset = u.State.id - u.StateStart.id;
                ChangeState(SpriteNum, s_GreenCoin[0]);
                u.State = s_GreenCoin[u.StateStart.id + offset];
            }
        } else if (u.WaitTics < 20 * 120) {
            if (u.StateStart != s_YellowCoin[0]) {
                int offset = u.State.id - u.StateStart.id;
                ChangeState(SpriteNum, s_YellowCoin[0]);
                u.State = s_YellowCoin[u.StateStart.id + offset];
            }
        }
    }

    public static void KillGet(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u = getUser(SpriteNum);
        switch (gNet.MultiGameType) {
            case MULTI_GAME_NONE:
                KillSprite(SpriteNum);
                break;
            case MULTI_GAME_COMMBAT:
            case MULTI_GAME_AI_BOTS:
            case MULTI_GAME_COOPERATIVE:
                if (u == null) {
                    break;
                }

                if (TEST(u.Flags2, SPR2_NEVER_RESPAWN)) {
                    KillSprite(SpriteNum);
                    break;
                }

                u.WaitTics = 30 * 120;
                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));

                // respawn markers
                if (!gNet.SpawnMarkers || sp.getHitag() == TAG_NORESPAWN_FLAG) // No coin if it's a special flag
                {
                    break;
                }

                int newsp = SpawnSprite(STAT_ITEM, Red_COIN, s_RedCoin[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), 0, 0);
                Sprite np = boardService.getSprite(newsp);
                USER nu = getUser(newsp);
                if (np != null && nu != null) {
                    np.setShade(-20);
                    nu.WaitTics = (u.WaitTics - 12);
                }

                break;
            default:
                break;
        }
    }    public static final State[] s_IconShotgun = {new State(ICON_SHOTGUN, 100, DoGet).setNext()};

    public static void KillGetAmmo(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        switch (gNet.MultiGameType) {
            default:
                break;
            case MULTI_GAME_NONE:
                KillSprite(SpriteNum);
                break;

            case MULTI_GAME_COMMBAT:
            case MULTI_GAME_AI_BOTS:
            case MULTI_GAME_COOPERATIVE:
                if (u == null) {
                    break;
                }

                if (TEST(u.Flags2, SPR2_NEVER_RESPAWN)) {
                    KillSprite(SpriteNum);
                    break;
                }

                // No Respawn mode - all ammo goes away
                if (gNet.NoRespawn) {
                    KillSprite(SpriteNum);
                    break;
                }

                u.WaitTics = 30 * 120;
                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));

                // respawn markers
                if (!gNet.SpawnMarkers) {
                    break;
                }

                int newsp = SpawnSprite(STAT_ITEM, Red_COIN, s_RedCoin[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), 0, 0);
                Sprite np = boardService.getSprite(newsp);
                USER nu = getUser(newsp);

                if (nu != null && np != null) {
                    np.setShade(-20);
                    nu.WaitTics = (u.WaitTics - 12);
                }

                break;
        }
    }

    public static void KillGetWeapon(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        switch (gNet.MultiGameType) {
            default:
                break;
            case MULTI_GAME_NONE:
                KillSprite(SpriteNum);
                break;

            case MULTI_GAME_COMMBAT:
            case MULTI_GAME_AI_BOTS:
            case MULTI_GAME_COOPERATIVE:
                if (u == null) {
                    break;
                }

                if (TEST(u.Flags2, SPR2_NEVER_RESPAWN)) {
                    KillSprite(SpriteNum);
                    break;
                }

                // No Respawn mode - all weapons stay
                // but can only get once
                if (gNet.NoRespawn) {
                    break;
                }

                u.WaitTics = 30 * 120;
                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));

                // respawn markers
                if (!gNet.SpawnMarkers) {
                    break;
                }

                int newsp = SpawnSprite(STAT_ITEM, Red_COIN, s_RedCoin[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), 0, 0);

                Sprite np = boardService.getSprite(newsp);
                USER nu = getUser(newsp);

                if (np != null && nu != null) {
                    np.setShade(-20);
                    nu.WaitTics = (u.WaitTics - 12);
                }

                break;
        }
    }    public static final State[] s_IconLgShotshell = {new State(ICON_LG_SHOTSHELL, 100, DoGet).setNext()};

    public static void DoSpawnItemTeleporterEffect(Sprite sp) {
        int effect = SpawnSprite(STAT_MISSILE, 0, s_TeleportEffect[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ() - Z(12), sp.getAng(), 0);
        Sprite ep = boardService.getSprite(effect);
        if (ep == null) {
            return;
        }

        ep.setShade(-40);
        ep.setXrepeat(36);
        ep.setYrepeat(36);
        ep.setCstat(ep.getCstat() | (CSTAT_SPRITE_YCENTER));
        ep.setCstat(ep.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
    }

    public static void ChoosePlayerGetSound(PlayerStr pp) {
        if (pp != Player[myconnectindex]) {
            return;
        }

        int choose_snd = STD_RANDOM_RANGE((MAX_GETSOUNDS - 1) << 8) >> 8;

        PlayerSound(PlayerGetItemVocs[choose_snd], v3df_follow | v3df_dontpan, pp);
    }    public static final State[] s_IconAutoRiot = {new State(ICON_AUTORIOT, 100, DoGet).setNext()};

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean CanGetWeapon(PlayerStr pp, int SpriteNum, int WPN) {
        USER u = getUser(SpriteNum);
        if (InfinityAmmo) {
            return true;
        }

        switch (gNet.MultiGameType) {
            default:
                break;
            case MULTI_GAME_NONE:
                return (true);

            case MULTI_GAME_COMMBAT:
            case MULTI_GAME_AI_BOTS:
            case MULTI_GAME_COOPERATIVE:
                if (u != null && TEST(u.Flags2, SPR2_NEVER_RESPAWN)) {
                    return (true);
                }

                // No Respawn - can't get a weapon again if you already got it
                return !gNet.NoRespawn || !TEST(pp.WpnGotOnceFlags, BIT(WPN));
        }

        return (true);
    }

    public static void DoGet(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        // Invisiblility is only used for DeathMatch type games
        // Sprites stays invisible for a period of time and is un-gettable
        // then "Re-Spawns" by becomming visible. Its never actually killed.
        if (u != null && TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
            u.WaitTics -= ACTORMOVETICS * 2;
            if (u.WaitTics <= 0) {
                PlaySound(DIGI_ITEM_SPAWN, sp, v3df_none);
                DoSpawnItemTeleporterEffect(sp);
                sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE));
            }

            return;
        }

        if (sp.getXvel() != 0) {
            if (!DoItemFly(SpriteNum)) {
                sp.setXvel(0);
                change_sprite_stat(SpriteNum, STAT_ITEM);
            }
        }

        if (u == null) {
            return;
        }

        for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            PlayerStr pp = Player[pnum];
            Sprite psp = pp.getSprite();
            USER pu = getUser(pp.PlayerSprite);
            if (psp == null || pu == null) {
                continue;
            }

            if (TEST(pp.Flags, PF_DEAD)) {
                continue;
            }

            int dist = DISTANCE(pp.posx, pp.posy, sp.getX(), sp.getY());
            if (dist > (pu.Radius + u.Radius)) {
                continue;
            }

            if (!SpriteOverlap(SpriteNum, pp.PlayerSprite)) {
                continue;
            }

            int cstat_bak = sp.getCstat();
            sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
            boolean can_see = FAFcansee(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), pp.posx, pp.posy, pp.posz, pp.cursectnum);
            sp.setCstat(cstat_bak);

            if (!can_see) {
                continue;
            }

            switch (u.ID) {
                //
                // Keys
                //
                case RED_CARD:
                case RED_KEY:
                case BLUE_CARD:
                case BLUE_KEY:
                case GREEN_CARD:
                case GREEN_KEY:
                case YELLOW_CARD:
                case YELLOW_KEY:
                case GOLD_SKELKEY:
                case SILVER_SKELKEY:
                case BRONZE_SKELKEY:
                case RED_SKELKEY: {
                    int key_num = 0;
                    switch (u.ID) {
                        case RED_CARD:
                        case RED_KEY:
                            break;
                        case BLUE_CARD:
                        case BLUE_KEY:
                            key_num = 1;
                            break;
                        case GREEN_CARD:
                        case GREEN_KEY:
                            key_num = 2;
                            break;
                        case YELLOW_CARD:
                        case YELLOW_KEY:
                            key_num = 3;
                            break;
                        case GOLD_SKELKEY:
                            key_num = 4;
                            break;
                        case SILVER_SKELKEY:
                            key_num = 5;
                            break;
                        case BRONZE_SKELKEY:
                            key_num = 6;
                            break;
                        case RED_SKELKEY:
                            key_num = 7;
                    }

                    if (pp.HasKey[key_num] != 0) {
                        break;
                    }

                    PutStringInfo(Player[pnum], KeyMsg[key_num]);

                    pp.HasKey[key_num] = 1;
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_KEY, sp, v3df_dontpan);
                    }

                    // don't kill keys in coop
                    if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COOPERATIVE) {
                        break;
                    }

                    KillSprite(SpriteNum);
                    break;
                }
                case ICON_ARMOR:
                    if (pp.Armor < 100) {
                        if (u.spal == PALETTE_PLAYER3) {
                            PlayerUpdateArmor(pp, 1100);
                            PutStringInfo(Player[pnum], "Kevlar Armor Vest +100");
                        } else {
                            if (pp.Armor < 50) {
                                PlayerUpdateArmor(pp, 1050);
                                PutStringInfo(Player[pnum], "Armor Vest +50");
                            } else {
                                break;
                            }
                        }
                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_BIGITEM, sp, v3df_dontpan);
                        }

                        // override for respawn mode
                        if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT && gNet.NoRespawn) {
                            KillSprite(SpriteNum);
                            break;
                        }

                        KillGet(SpriteNum);
                    }
                    break;

                //
                // Health - Instant Use
                //

                case ICON_SM_MEDKIT:
                    if (pu.Health < 100) {
                        boolean putbackmax = false;
                        PutStringInfo(Player[pnum], "MedKit +20");

                        if (pp.MaxHealth == 200) {
                            pp.MaxHealth = 100;
                            putbackmax = true;
                        }
                        PlayerUpdateHealth(pp, 20);

                        if (putbackmax) {
                            pp.MaxHealth = 200;
                        }

                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                        }

                        // override for respawn mode
                        if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT && gNet.NoRespawn) {
                            KillSprite(SpriteNum);
                            break;
                        }

                        KillGet(SpriteNum);
                    }
                    break;

                case ICON_BOOSTER: // Fortune cookie
                    pp.MaxHealth = 200;
                    if (pu.Health < 200) {
                        PutStringInfo(Player[pnum], "Fortune Cookie +50 BOOST");
                        PlayerUpdateHealth(pp, 50); // This is for health
                        // over 100%
                        // Say something witty
                        if (pp == Player[myconnectindex] && cfg.Messages) {
                            if (cfg.ParentalLock || Global_PLock) {
                                adduserquote("Fortune Say: " + ReadFortune[STD_RANDOM_RANGE(10)]);
                            } else {
                                adduserquote("Fortune Say: " + ReadFortune[STD_RANDOM_RANGE(MAX_FORTUNES)]);
                            }
                        }

                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_BIGITEM, sp, v3df_dontpan);
                        }

                        // override for respawn mode
                        if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT && gNet.NoRespawn) {
                            KillSprite(SpriteNum);
                            break;
                        }

                        KillGet(SpriteNum);
                    }
                    break;

                //
                // Inventory
                //
                case ICON_MEDKIT:

                    if (pp.InventoryAmount[INVENTORY_MEDKIT] == 0 || pp.InventoryPercent[INVENTORY_MEDKIT] < 100) {
                        PutStringInfo(Player[pnum], "Portable MedKit");
                        pp.InventoryPercent[INVENTORY_MEDKIT] = 100;
                        pp.InventoryAmount[INVENTORY_MEDKIT] = 1;
                        PlayerUpdateInventory(pp, INVENTORY_MEDKIT);
                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                        }

                        // override for respawn mode
                        if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT && gNet.NoRespawn) {
                            KillSprite(SpriteNum);
                            break;
                        }

                        KillGet(SpriteNum);
                    }
                    break;

                case ICON_CHEMBOMB:

                    if (pp.InventoryAmount[INVENTORY_CHEMBOMB] < 1) {
                        PutStringInfo(Player[pnum], "Gas Bomb");
                        pp.InventoryPercent[INVENTORY_CHEMBOMB] = 0;
                        pp.InventoryAmount[INVENTORY_CHEMBOMB]++;
                        PlayerUpdateInventory(pp, INVENTORY_CHEMBOMB);
                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                        }
                        KillGet(SpriteNum);
                    }
                    break;

                case ICON_FLASHBOMB:

                    if (pp.InventoryAmount[INVENTORY_FLASHBOMB] < 2) {
                        PutStringInfo(Player[pnum], "Flash Bomb");
                        pp.InventoryPercent[INVENTORY_FLASHBOMB] = 0;
                        pp.InventoryAmount[INVENTORY_FLASHBOMB]++;
                        PlayerUpdateInventory(pp, INVENTORY_FLASHBOMB);
                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                        }
                        KillGet(SpriteNum);
                    }
                    break;

                case ICON_CALTROPS:

                    if (pp.InventoryAmount[INVENTORY_CALTROPS] < 3) {
                        PutStringInfo(Player[pnum], "Caltrops");
                        pp.InventoryPercent[INVENTORY_CALTROPS] = 0;
                        pp.InventoryAmount[INVENTORY_CALTROPS] += 3;
                        if (pp.InventoryAmount[INVENTORY_CALTROPS] > 3) {
                            pp.InventoryAmount[INVENTORY_CALTROPS] = 3;
                        }
                        PlayerUpdateInventory(pp, INVENTORY_CALTROPS);
                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                        }
                        KillGet(SpriteNum);
                    }
                    break;

                case ICON_NIGHT_VISION:
                    if (pp.InventoryAmount[INVENTORY_NIGHT_VISION] == 0 || pp.InventoryPercent[INVENTORY_NIGHT_VISION] < 100) {
                        PutStringInfo(Player[pnum], "Night Vision Goggles");
                        pp.InventoryPercent[INVENTORY_NIGHT_VISION] = 100;
                        pp.InventoryAmount[INVENTORY_NIGHT_VISION] = 1;
                        PlayerUpdateInventory(pp, INVENTORY_NIGHT_VISION);
                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                        }
                        KillGet(SpriteNum);
                    }
                    break;
                case ICON_REPAIR_KIT:
                    if (pp.InventoryAmount[INVENTORY_REPAIR_KIT] == 0 || pp.InventoryPercent[INVENTORY_REPAIR_KIT] < 100) {
                        PutStringInfo(Player[pnum], "Repair Kit");
                        pp.InventoryPercent[INVENTORY_REPAIR_KIT] = 100;
                        pp.InventoryAmount[INVENTORY_REPAIR_KIT] = 1;
                        PlayerUpdateInventory(pp, INVENTORY_REPAIR_KIT);
                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                        }

                        // don't kill repair kit in coop
                        if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COOPERATIVE) {
                            break;
                        }

                        KillGet(SpriteNum);
                    }
                    break;

                case ICON_CLOAK:
                    if (pp.InventoryAmount[INVENTORY_CLOAK] == 0 || pp.InventoryPercent[INVENTORY_CLOAK] < 100) {
                        PutStringInfo(Player[pnum], "Smoke Bomb");
                        pp.InventoryPercent[INVENTORY_CLOAK] = 100;
                        pp.InventoryAmount[INVENTORY_CLOAK] = 1;
                        PlayerUpdateInventory(pp, INVENTORY_CLOAK);
                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                        }
                        KillGet(SpriteNum);
                    }
                    break;
                //
                // Weapon
                //
                case ICON_STAR:

                    if (!CanGetWeapon(pp, SpriteNum, WPN_STAR)) {
                        break;
                    }

                    pp.WpnGotOnceFlags |= (BIT(WPN_STAR));

                    if (!InfinityAmmo && pp.WpnAmmo[WPN_STAR] >= DamageData[WPN_STAR].max_ammo) {
                        break;
                    }

                    PutStringInfo(Player[pnum], cfg.UseDarts ? "Darts" : "Shurikens");
                    PlayerUpdateAmmo(pp, WPN_STAR, 9);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    KillGetWeapon(SpriteNum);
                    if (TEST(pp.WpnFlags, BIT(WPN_STAR))) {
                        break;
                    }
                    pp.WpnFlags |= (BIT(WPN_STAR));
                    if (!cfg.WeaponAutoSwitch) {
                        break;
                    }

                    if (pu.WeaponNum <= WPN_STAR) {
                        break;
                    }
                    InitWeaponStar(pp);
                    break;

                case ICON_LG_MINE:

                    if (!CanGetWeapon(pp, SpriteNum, WPN_MINE)) {
                        break;
                    }

                    pp.WpnGotOnceFlags |= (BIT(WPN_MINE));

                    if (!InfinityAmmo && pp.WpnAmmo[WPN_MINE] >= DamageData[WPN_MINE].max_ammo) {
                        break;
                    }

                    PutStringInfo(Player[pnum], "Sticky Bombs");
                    PlayerUpdateAmmo(pp, WPN_MINE, 5);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    ChoosePlayerGetSound(pp);
                    KillGetWeapon(SpriteNum);
                    if (TEST(pp.WpnFlags, BIT(WPN_MINE))) {
                        break;
                    }
                    pp.WpnFlags |= (BIT(WPN_MINE));
                    if (!cfg.WeaponAutoSwitch) {
                        break;
                    }

                    if (pu.WeaponNum > WPN_MINE && pu.WeaponNum != WPN_SWORD) {
                        break;
                    }
                    InitWeaponMine(pp);
                    break;

                case ICON_UZI:
                case ICON_UZIFLOOR:

                    if (!CanGetWeapon(pp, SpriteNum, WPN_UZI)) {
                        break;
                    }

                    pp.WpnGotOnceFlags |= (BIT(WPN_UZI));

                    if (!InfinityAmmo && TEST(pp.Flags, PF_TWO_UZI) && pp.WpnAmmo[WPN_UZI] >= DamageData[WPN_UZI].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "UZI Submachine Gun");
                    PlayerUpdateAmmo(pp, WPN_UZI, 50);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    KillGetWeapon(SpriteNum);

                    if (TEST(pp.WpnFlags, BIT(WPN_UZI)) && TEST(pp.Flags, PF_TWO_UZI)) {
                        break;
                    }
                    // flag to help with double uzi powerup - simpler but kludgy
                    pp.Flags |= (PF_PICKED_UP_AN_UZI);
                    if (TEST(pp.WpnFlags, BIT(WPN_UZI))) {
                        pp.Flags |= (PF_TWO_UZI);
                        pp.WpnUziType = 0; // Let it come up
                        if (pp == Player[myconnectindex]) {
                            PlayerSound(DIGI_DOUBLEUZI, v3df_dontpan | v3df_follow, pp);
                        }
                    } else {
                        pp.WpnFlags |= (BIT(WPN_UZI));
                        ChoosePlayerGetSound(pp);
                    }

                    if (!cfg.WeaponAutoSwitch) {
                        break;
                    }

                    if (pu.WeaponNum > WPN_UZI && pu.WeaponNum != WPN_SWORD) {
                        break;
                    }

                    InitWeaponUzi(pp);
                    break;

                case ICON_LG_UZI_AMMO:
                    if (!InfinityAmmo && pp.WpnAmmo[WPN_UZI] >= DamageData[WPN_UZI].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "UZI Clip");
                    PlayerUpdateAmmo(pp, WPN_UZI, 50);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    KillGetAmmo(SpriteNum);
                    break;

                case ICON_MICRO_GUN:

                    if (!CanGetWeapon(pp, SpriteNum, WPN_MICRO)) {
                        break;
                    }

                    pp.WpnGotOnceFlags |= (BIT(WPN_MICRO));

                    if (!InfinityAmmo && TEST(pp.WpnFlags, BIT(WPN_MICRO)) && pp.WpnAmmo[WPN_MICRO] >= DamageData[WPN_MICRO].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Missile Launcher");

                    PlayerUpdateAmmo(pp, WPN_MICRO, 5);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    ChoosePlayerGetSound(pp);
                    KillGetWeapon(SpriteNum);
                    if (TEST(pp.WpnFlags, BIT(WPN_MICRO))) {
                        break;
                    }
                    pp.WpnFlags |= (BIT(WPN_MICRO));
                    if (!cfg.WeaponAutoSwitch) {
                        break;
                    }

                    if (pu.WeaponNum > WPN_MICRO && pu.WeaponNum != WPN_SWORD) {
                        break;
                    }
                    InitWeaponMicro(pp);
                    break;

                case ICON_MICRO_BATTERY:
                    if (!InfinityAmmo && pp.WpnAmmo[WPN_MICRO] >= DamageData[WPN_MICRO].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Missiles");
                    PlayerUpdateAmmo(pp, WPN_MICRO, 5);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    KillGetAmmo(SpriteNum);
                    break;

                case ICON_NUKE:
                    if (pp.WpnRocketNuke != 1) {
                        PutStringInfo(Player[pnum], "Nuclear Warhead");
                        pp.WpnRocketNuke = 1;
                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                        }
                        if (STD_RANDOM_RANGE(1000) > 800 && pp == Player[myconnectindex]) {
                            PlayerSound(DIGI_ILIKENUKES, v3df_dontpan | v3df_doppler | v3df_follow, pp);
                        }
                        if (pp.CurWpn == pp.Wpn[WPN_MICRO]) {
                            if (pp.WpnRocketType != 2) {
                                pp.CurWpn.over[MICRO_SHOT_NUM].tics = 0;
                                pp.CurWpn.over[MICRO_SHOT_NUM].State = ps_MicroNukeFlash[0];
                                // Play Nuke available sound here!
                            }

                        }

                        KillGetAmmo(SpriteNum);
                    }
                    break;

                case ICON_GRENADE_LAUNCHER:
                    if (!CanGetWeapon(pp, SpriteNum, WPN_GRENADE)) {
                        break;
                    }

                    pp.WpnGotOnceFlags |= (BIT(WPN_GRENADE));

                    if (!InfinityAmmo && TEST(pp.WpnFlags, BIT(WPN_GRENADE)) && pp.WpnAmmo[WPN_GRENADE] >= DamageData[WPN_GRENADE].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Grenade Launcher");

                    PlayerUpdateAmmo(pp, WPN_GRENADE, 6);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    // ChoosePlayerGetSound(pp);
                    if (STD_RANDOM_RANGE(1000) > 800 && pp == Player[myconnectindex]) {
                        PlayerSound(DIGI_LIKEBIGWEAPONS, v3df_dontpan | v3df_doppler | v3df_follow, pp);
                    }
                    KillGetWeapon(SpriteNum);
                    if (TEST(pp.WpnFlags, BIT(WPN_GRENADE))) {
                        break;
                    }
                    pp.WpnFlags |= (BIT(WPN_GRENADE));
                    if (!cfg.WeaponAutoSwitch) {
                        break;
                    }

                    if (pu.WeaponNum > WPN_GRENADE && pu.WeaponNum != WPN_SWORD) {
                        break;
                    }
                    InitWeaponGrenade(pp);
                    break;

                case ICON_LG_GRENADE:
                    if (!InfinityAmmo && pp.WpnAmmo[WPN_GRENADE] >= DamageData[WPN_GRENADE].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Grenade Shells");
                    PlayerUpdateAmmo(pp, WPN_GRENADE, 8);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    KillGetAmmo(SpriteNum);
                    break;

                case ICON_RAIL_GUN:

                    if (!CanGetWeapon(pp, SpriteNum, WPN_RAIL)) {
                        break;
                    }

                    pp.WpnGotOnceFlags |= (BIT(WPN_RAIL));

                    if (!InfinityAmmo && TEST(pp.WpnFlags, BIT(WPN_RAIL)) && pp.WpnAmmo[WPN_RAIL] >= DamageData[WPN_RAIL].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Rail Gun");
                    PlayerUpdateAmmo(pp, WPN_RAIL, 10);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    if (pp == Player[myconnectindex]) {
                        if (STD_RANDOM_RANGE(1000) > 700) {
                            PlayerSound(DIGI_LIKEBIGWEAPONS, v3df_dontpan | v3df_doppler | v3df_follow, pp);
                        } else {
                            PlayerSound(DIGI_GOTRAILGUN, v3df_dontpan | v3df_doppler | v3df_follow, pp);
                        }
                    }

                    KillGetWeapon(SpriteNum);
                    if (TEST(pp.WpnFlags, BIT(WPN_RAIL))) {
                        break;
                    }
                    pp.WpnFlags |= (BIT(WPN_RAIL));
                    if (!cfg.WeaponAutoSwitch) {
                        break;
                    }

                    if (pu.WeaponNum > WPN_RAIL && pu.WeaponNum != WPN_SWORD) {
                        break;
                    }
                    InitWeaponRail(pp);
                    break;

                case ICON_RAIL_AMMO:
                    if (!InfinityAmmo && pp.WpnAmmo[WPN_RAIL] >= DamageData[WPN_RAIL].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Rail Gun Rods");
                    PlayerUpdateAmmo(pp, WPN_RAIL, 10);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    KillGetAmmo(SpriteNum);
                    break;

                case ICON_SHOTGUN:
                    if (!CanGetWeapon(pp, SpriteNum, WPN_SHOTGUN)) {
                        break;
                    }

                    pp.WpnGotOnceFlags |= (BIT(WPN_SHOTGUN));

                    if (!InfinityAmmo && TEST(pp.WpnFlags, BIT(WPN_SHOTGUN)) && pp.WpnAmmo[WPN_SHOTGUN] >= DamageData[WPN_SHOTGUN].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Riot Gun");
                    PlayerUpdateAmmo(pp, WPN_SHOTGUN, 8);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    ChoosePlayerGetSound(pp);
                    KillGetWeapon(SpriteNum);
                    if (TEST(pp.WpnFlags, BIT(WPN_SHOTGUN))) {
                        break;
                    }
                    pp.WpnFlags |= (BIT(WPN_SHOTGUN));
                    if (!cfg.WeaponAutoSwitch) {
                        break;
                    }

                    if (pu.WeaponNum > WPN_SHOTGUN && pu.WeaponNum != WPN_SWORD) {
                        break;
                    }
                    InitWeaponShotgun(pp);
                    break;

                case ICON_LG_SHOTSHELL:
                    if (!InfinityAmmo && pp.WpnAmmo[WPN_SHOTGUN] >= DamageData[WPN_SHOTGUN].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Shotshells");
                    PlayerUpdateAmmo(pp, WPN_SHOTGUN, 24);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    KillGetAmmo(SpriteNum);
                    break;

                case ICON_GUARD_HEAD:

                    if (!CanGetWeapon(pp, SpriteNum, WPN_HOTHEAD)) {
                        break;
                    }

                    pp.WpnGotOnceFlags |= (BIT(WPN_HOTHEAD));

                    if (!InfinityAmmo && TEST(pp.WpnFlags, BIT(WPN_HOTHEAD)) && pp.WpnAmmo[WPN_HOTHEAD] >= DamageData[WPN_HOTHEAD].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Guardian Head");
                    PlayerUpdateAmmo(pp, WPN_HOTHEAD, 30);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }

                    if (STD_RANDOM_RANGE(1000) > 800 && pp == Player[myconnectindex]) {
                        PlayerSound(DIGI_LIKEBIGWEAPONS, v3df_dontpan | v3df_doppler | v3df_follow, pp);
                    }
                    KillGetWeapon(SpriteNum);
                    if (TEST(pp.WpnFlags, BIT(WPN_HOTHEAD))) {
                        break;
                    }
                    pp.WpnFlags |= (BIT(WPN_NAPALM) | BIT(WPN_RING) | BIT(WPN_HOTHEAD));
                    if (!cfg.WeaponAutoSwitch) {
                        break;
                    }

                    if (pu.WeaponNum > WPN_HOTHEAD && pu.WeaponNum != WPN_SWORD) {
                        break;
                    }
                    InitWeaponHothead(pp);
                    break;

                case ICON_FIREBALL_LG_AMMO:
                    if (!InfinityAmmo && pp.WpnAmmo[WPN_HOTHEAD] >= DamageData[WPN_HOTHEAD].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Firebursts");
                    PlayerUpdateAmmo(pp, WPN_HOTHEAD, 60);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    KillGetAmmo(SpriteNum);
                    break;

                case ICON_HEART:

                    if (!CanGetWeapon(pp, SpriteNum, WPN_HEART)) {
                        break;
                    }

                    pp.WpnGotOnceFlags |= (BIT(WPN_HEART));

                    if (!InfinityAmmo && TEST(pp.WpnFlags, BIT(WPN_HEART)) && pp.WpnAmmo[WPN_HEART] >= DamageData[WPN_HEART].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Ripper Heart");
                    PlayerUpdateAmmo(pp, WPN_HEART, 1);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    if (STD_RANDOM_RANGE(1000) > 800 && pp == Player[myconnectindex]) {
                        PlayerSound(DIGI_LIKEBIGWEAPONS, v3df_dontpan | v3df_doppler | v3df_follow, pp);
                    }
                    KillGetWeapon(SpriteNum);
                    if (TEST(pp.WpnFlags, BIT(WPN_HEART))) {
                        break;
                    }
                    pp.WpnFlags |= (BIT(WPN_HEART));
                    if (!cfg.WeaponAutoSwitch) {
                        break;
                    }

                    if (pu.WeaponNum > WPN_HEART && pu.WeaponNum != WPN_SWORD) {
                        break;
                    }

                    InitWeaponHeart(pp);
                    break;

                case ICON_HEART_LG_AMMO:
                    if (!InfinityAmmo && pp.WpnAmmo[WPN_HEART] >= DamageData[WPN_HEART].max_ammo) {
                        break;
                    }
                    PutStringInfo(Player[pnum], "Deathcoils");
                    PlayerUpdateAmmo(pp, WPN_HEART, 6);
                    SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                    if (pp == Player[myconnectindex]) {
                        PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                    }
                    KillGetAmmo(SpriteNum);
                    break;

                case ICON_HEAT_CARD:
                    if (pp.WpnRocketHeat != 5) {
                        PutStringInfo(Player[pnum], "Heat Seeker Card");
                        pp.WpnRocketHeat = 5;
                        SetFadeAmt(pp, ITEMFLASHAMT, ITEMFLASHCLR); // Flash blue on item pickup
                        if (pp == Player[myconnectindex]) {
                            PlaySound(DIGI_ITEM, sp, v3df_dontpan);
                        }
                        KillGet(SpriteNum);

                        if (pp.CurWpn == pp.Wpn[WPN_MICRO]) {
                            if (pp.WpnRocketType == 0) {
                                pp.WpnRocketType = 1;
                            } else if (pp.WpnRocketType == 2) {
                                pp.CurWpn.over[MICRO_HEAT_NUM].tics = 0;
                                pp.CurWpn.over[MICRO_HEAT_NUM].State = ps_MicroHeatFlash[0];
                            }

                        }
                    }
                    break;

                case ICON_FLAG:
                    if (sp.getPal() == psp.getPal()) {
                        break; // Can't pick up your own flag!
                    }

                    PlaySound(DIGI_ITEM, sp, v3df_dontpan);

                    int newsp;
                    if (sp.getHitag() == TAG_NORESPAWN_FLAG) {
                        newsp = SpawnSprite(STAT_ITEM, ICON_FLAG, s_CarryFlagNoDet[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), 0, 0);
                    } else {
                        newsp = SpawnSprite(STAT_ITEM, ICON_FLAG, s_CarryFlag[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), 0, 0);
                    }

                    Sprite np = boardService.getSprite(newsp);
                    USER nu = getUser(newsp);
                    if (np == null || nu == null) {
                        break;
                    }
                    np.setShade(-20);

                    // Attach flag to player
                    nu.Counter = 0;
                    np.setCstat(np.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                    np.setCstat(np.getCstat() | (CSTAT_SPRITE_WALL));
                    SetAttach(pp.PlayerSprite, newsp);
                    nu.sz = SPRITEp_MID(psp); // Set mid way up who it hit
                    nu.spal = (byte) sp.getPal();
                    np.setPal(nu.spal); // Set the palette of the flag

                    SetOwner(pp.PlayerSprite, newsp); // Player now owns the flag
                    nu.FlagOwner = SpriteNum; // Tell carried flag who owns it
                    KillGet(SpriteNum); // Set up for flag respawning
                    break;

                default:
                    KillSprite(SpriteNum);
            }
        }
    }    public static final State[] s_IconGrenadeLauncher = {new State(ICON_GRENADE_LAUNCHER, 100, DoGet).setNext()};

    public static void SetEnemyActive(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u != null) {
            u.Flags |= (SPR_ACTIVE);
            u.inactive_time = 0;
        }
    }

    public static void SetEnemyInactive(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u != null) {
            u.Flags &= ~(SPR_ACTIVE);
        }
    }    public static final State[] s_IconLgGrenade = {new State(ICON_LG_GRENADE, 100, DoGet).setNext()};

    public static void ProcessActiveVars(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (!TEST(u.Flags, SPR_ACTIVE)) {
            // if actor has been unaware for more than a few seconds
            u.inactive_time += ACTORMOVETICS;
            if (u.inactive_time > TIME_TILL_INACTIVE) {
                // reset to min update range
                u.active_range = MIN_ACTIVE_RANGE;
                // keep time low so it doesn't roll over
                u.inactive_time = TIME_TILL_INACTIVE;
            }
        }

        u.wait_active_check += ACTORMOVETICS;
    }

    public static void AdjustActiveRange(PlayerStr pp, int SpriteNum, int dist) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        Sprite psp = pp.getSprite();
        if (u == null || sp == null || psp == null) {
            return;
        }

        // do no FAFcansee before it is time
        if (u.wait_active_check < ACTIVE_CHECK_TIME) {
            return;
        }

        u.wait_active_check = 0;

        // check aboslute max
        if (dist > MAX_ACTIVE_RANGE) {
            return;
        }

        // do not do a FAFcansee if your already active
        // Actor only becomes INACTIVE in DoActorDecision
        if (TEST(u.Flags, SPR_ACTIVE)) {
            return;
        }

        //
        // From this point on Actor is INACTIVE
        //

        // if actor can still see the player
        int look_height = SPRITEp_TOS(sp);
        if (FAFcansee(sp.getX(), sp.getY(), look_height, sp.getSectnum(), psp.getX(), psp.getY(), SPRITEp_UPPER(psp), psp.getSectnum())) {
            // Player is visible
            // adjust update range of this sprite

            // some huge distance
            u.active_range = 75000;
            // sprite is AWARE
            SetEnemyActive(SpriteNum);
        }
    }    public static final State[] s_IconLgMine = {new State(ICON_LG_MINE, 100, DoGet).setNext()};

    private static void StateControl(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        if (u.State == null) {
            u.ActorActionFunc.animatorInvoke(SpriteNum);
            return;
        }

        if (sp.getStatnum() >= STAT_SKIP4_START && sp.getStatnum() <= STAT_SKIP4_END) {
            u.Tics += ACTORMOVETICS * 2;
        } else {
            u.Tics += ACTORMOVETICS;
        }

        // Skip states if too much time has passed
        while (u.Tics >= DTEST(u.State.Tics, SF_TICS_MASK)) {
            int StateTics = DTEST(u.State.Tics, SF_TICS_MASK);

            if (TEST(u.State.Tics, SF_TIC_ADJUST) && u.Attrib != null) {
                StateTics += u.Attrib.TicAdjust[u.speed];
            }

            // Set Tics
            u.Tics -= StateTics;

            // Transition to the next state
            u.State = u.State.getNext();

            // Look for flags embedded into the Tics variable
            while (TEST(u.State.Tics, SF_QUICK_CALL)) {
                // Call it once and go to the next state
                if (u.State.Animator != null) {
                    u.State.Animator.animatorInvoke(SpriteNum);
                }

                // if still on the same QUICK_CALL should you
                // go to the next state.
                if (TEST(u.State.Tics, SF_QUICK_CALL)) {
                    u.State = u.State.getNext();
                }
            }

            if (u.State.Pic == 0) {
                NewStateGroup(SpriteNum, u.State.getNextGroup());
            }
        }

        // Set picnum to the correct pic
        Wall w = boardService.getWall(u.WallP);
        if (TEST(u.State.Tics, SF_WALL_STATE) && w != null) {
            w.setPicnum(u.State.Pic);
        } else {
            if (u.RotNum > 1 && u.getRot() != null && u.getRot().getState(0) != null) {
                sp.setPicnum(u.getRot().getState(0).Pic);
            } else {
                sp.setPicnum(u.State.Pic);
            }
        }

        // Call the correct animator
        if (u.State.Animator != null) {
            u.State.Animator.animatorInvoke(SpriteNum);
        }
    }

    public static void SpriteControl() {
        if (DebugActorFreeze) {
            return;
        }

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_MISC); node != null; node = nexti) {
            nexti = node.getNext();
            StateControl(node.getIndex());
        }

        // Items and skip2 things
        if (!MoveSkip2) {
            for (int stat = STAT_SKIP2_START + 1; stat <= STAT_SKIP2_END; stat++) {
                for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = nexti) {
                    nexti = node.getNext();
                    StateControl(node.getIndex());
                }
            }
        }

        if (!MoveSkip2) // limit to 20 times a second
        {
            // move bad guys around
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_ENEMY); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                USER u = getUser(i);
                Sprite sp = node.get();
                if (u == null) {
                    continue;
                }

                boolean CloseToPlayer = false;

                ProcessActiveVars(i);

                for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                    PlayerStr pp = Player[pnum];

                    // Only update the ones closest
                    int dist = DISTANCE(pp.posx, pp.posy, sp.getX(), sp.getY());
                    AdjustActiveRange(pp, i, dist);

                    if (dist < u.active_range) {
                        CloseToPlayer = true;
                    }
                }

                u.Flags &= ~(SPR_MOVED);

                // Only update the ones close to ANY player
                if (CloseToPlayer) {
                    StateControl(i);
                } else {
                    // to far away to be attacked
                    u.Flags &= ~(SPR_ATTACKED);
                }
            }
        }

        // Skip4 things
        if (MoveSkip4 == 0) // limit to 10 times a second
        {
            for (int stat = STAT_SKIP4_START; stat <= STAT_SKIP4_END; stat++) {
                for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = nexti) {
                    nexti = node.getNext();
                    StateControl(node.getIndex());
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_NO_STATE); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            USER u = getUser(i);

            if (u != null && u.ActorActionFunc != null) {
                u.ActorActionFunc.animatorInvoke(i);
            }
        }

        if (MoveSkip8 == 0) {
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_STATIC_FIRE); node != null; node = nexti) {
                nexti = node.getNext();
                DoStaticFlamesDamage(node.getIndex());
            }
        }

        if (MoveSkip4 == 0) // limit to 10 times a second
        {
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_WALLBLOOD_QUEUE); node != null; node = nexti) {
                nexti = node.getNext();
                StateControl(node.getIndex());
            }
        }

        // vator/rotator/spike/slidor all have some code to
        // prevent calling of the action func()
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_VATOR); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            USER u = getUser(i);
            if (u == null) {
                continue;
            }

            if (u.Tics != 0) {
                if ((u.Tics -= synctics) <= 0) {
                    SetVatorActive(i);
                } else {
                    continue;
                }
            }

            if (!TEST(u.Flags, SPR_ACTIVE)) {
                continue;
            }

            u.ActorActionFunc.animatorInvoke(i);
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPIKE); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            USER u = getUser(i);
            if (u == null) {
                continue;
            }

            if (u.Tics != 0) {
                if ((u.Tics -= synctics) <= 0) {
                    SetSpikeActive(i);
                } else {
                    continue;
                }
            }

            if (!TEST(u.Flags, SPR_ACTIVE)) {
                continue;
            }

            u.ActorActionFunc.animatorInvoke(i);
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ROTATOR); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            USER u = getUser(i);
            if (u == null) {
                continue;
            }

            if (u.Tics != 0) {
                if ((u.Tics -= synctics) <= 0) {
                    SetRotatorActive(i);
                } else {
                    continue;
                }
            }

            if (!TEST(u.Flags, SPR_ACTIVE)) {
                continue;
            }

            u.ActorActionFunc.animatorInvoke(i);
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SLIDOR); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            USER u = getUser(i);
            if (u == null) {
                continue;
            }

            if (u.Tics != 0) {
                if ((u.Tics -= synctics) <= 0) {
                    SetSlidorActive(i);
                } else {
                    continue;
                }
            }

            if (!TEST(u.Flags, SPR_ACTIVE)) {
                continue;
            }

            u.ActorActionFunc.animatorInvoke(i);
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SUICIDE); node != null; node = nexti) {
            nexti = node.getNext();
            KillSprite(node.getIndex());
        }
    }    public static final State[] s_IconGuardHead = {new State(ICON_GUARD_HEAD, 15, DoGet).setNext()};

    public static int move_sprite(int spritenum, int xchange, int ychange, int zchange, int ceildist, int flordist, int cliptype, int numtics) {
        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null) {
            return 0;
        }

        USER u = getUser(spritenum);
        // Can't modify sprite sectors
        // directly becuase of linked lists
        int dasectnum = spr.getSectnum();
        int lastsectnum = spr.getSectnum();


        // Must do this if not using the new
        // centered centering (of course)
        int daz = spr.getZ(), zh = 0;
        if (u != null && !TEST(spr.getCstat(), CSTAT_SPRITE_YCENTER)) {
            // move the center point up for moving
            zh = u.zclip;
            daz -= zh;
        }

        clipmoveboxtracenum = 1;
        int retval = engine.clipmove(spr.getX(), spr.getY(), daz, dasectnum, (((long) xchange * numtics) << 11), (((long) ychange * numtics) << 11), ((spr.getClipdist()) << 2), ceildist, flordist, cliptype);

        spr.setX(clipmove_x);
        spr.setY(clipmove_y);
        dasectnum = clipmove_sectnum;

        clipmoveboxtracenum = 3;

        if (dasectnum == -1) {
            retval = HIT_WALL;
            return (retval);
        }

        if ((dasectnum != spr.getSectnum()) && (dasectnum >= 0)) {
            engine.changespritesect(spritenum, dasectnum);
        }

        // took this out - may not be to relevant anymore
        // ASSERT(inside(spr.x,spr.y,dasectnum));

        // Set the blocking bit to 0 temporarly so FAFgetzrange doesn't pick
        // up its own sprite
        int tempshort = spr.getCstat();
        spr.setCstat(0);

        // I subtracted 8 from the clipdist because actors kept going up on
        // ledges they were not supposed to go up on. Did the same for the
        // player. Seems to work ok!
        FAFgetzrange(spr.getX(), spr.getY(), spr.getZ() - zh - 1, spr.getSectnum(), tmp_ptr[0].set(globhiz), tmp_ptr[1].set(globhihit), tmp_ptr[2].set(globloz), tmp_ptr[3].set(globlohit), ((spr.getClipdist()) << 2) - GETZRANGE_CLIP_ADJ, cliptype);

        globhiz = tmp_ptr[0].value;
        globhihit = tmp_ptr[1].value;
        globloz = tmp_ptr[2].value;
        globlohit = tmp_ptr[3].value;

        spr.setCstat(tempshort);

        // !AIC - puts getzrange results into USER varaible u.loz, u.hiz, u.lo_sectp,
        // u.hi_sectp, etc.
        // Takes info from global variables
        DoActorGlobZ(spritenum);

        daz = spr.getZ() + ((zchange * numtics) >> 3);

        // test for hitting ceiling or floor
        if ((daz - zh <= globhiz) || (daz - zh > globloz)) {
            if (retval == 0) {
                if (u != null && TEST(u.Flags, SPR_CLIMBING)) {
                    spr.setZ(daz);
                    return (0);
                }

                retval = HIT_SECTOR | dasectnum;
            }
        } else {
            spr.setZ(daz);
        }

        // extra processing for Stacks and warping
        if (FAF_ConnectArea(spr.getSectnum())) {
            engine.setspritez(spritenum, spr.getX(), spr.getY(), spr.getZ());
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec != null && TEST(sec.getExtra(), SECTFX_WARP_SECTOR)) {
            Sprite sp_warp;
            if ((sp_warp = WarpPlane(spr.getX(), spr.getY(), spr.getZ(), dasectnum)) != null) {

                spr.setX(warp.x);
                spr.setY(warp.y);
                spr.setZ(warp.z);
                dasectnum = warp.sectnum;

                ActorWarpUpdatePos(spritenum, dasectnum);
                ActorWarpType(spr, sp_warp);
            }

            if (spr.getSectnum() != lastsectnum) {
                if ((sp_warp = WarpM(spr.getX(), spr.getY(), spr.getZ(), dasectnum)) != null) {

                    spr.setX(warp.x);
                    spr.setY(warp.y);
                    spr.setZ(warp.z);
                    dasectnum = warp.sectnum;

                    ActorWarpUpdatePos(spritenum, dasectnum);
                    ActorWarpType(spr, sp_warp);
                }
            }
        }

        return (retval);
    }

    public static void MissileWarpUpdatePos(int SpriteNum, int sectnum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        u.ox = sp.getX();
        u.oy = sp.getY();
        u.oz = sp.getZ();
        engine.changespritesect(SpriteNum, sectnum);
        MissileZrange(SpriteNum);
    }

    public static void ActorWarpUpdatePos(int SpriteNum, int sectnum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        u.ox = sp.getX();
        u.oy = sp.getY();
        u.oz = sp.getZ();
        engine.changespritesect(SpriteNum, sectnum);
        DoActorZrange(SpriteNum);
    }    public static final State[] s_IconFireballLgAmmo = {new State(ICON_FIREBALL_LG_AMMO, FIREBALL_LG_AMMO_RATE, DoGet), new State(ICON_FIREBALL_LG_AMMO + 1, FIREBALL_LG_AMMO_RATE, DoGet), new State(ICON_FIREBALL_LG_AMMO + 2, FIREBALL_LG_AMMO_RATE, DoGet)};

    public static void MissileWarpType(Sprite sp, Sprite sp_warp) {
        switch (SP_TAG1(sp_warp)) {
            case WARP_CEILING_PLANE:
            case WARP_FLOOR_PLANE:
                return;
        }

        if (SP_TAG3(sp_warp) != 1) {
            PlaySound(DIGI_ITEM_SPAWN, sp, v3df_none);
            DoSpawnItemTeleporterEffect(sp);
        }
    }

    public static void ActorWarpType(Sprite sp, Sprite sp_warp) {
        if (SP_TAG3(sp_warp) != 1) {
            PlaySound(DIGI_ITEM_SPAWN, sp, v3df_none);
            DoSpawnTeleporterEffectPlace(sp);
        }
    }    public static final State[] s_IconHeart = {new State(ICON_HEART, 25, DoGet), new State(ICON_HEART + 1, 25, DoGet)};

    public static void MissileWaterAdjust(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u != null && u.lo_sectp != -1) {
            Sect_User sectu = getSectUser(u.lo_sectp);
            if (sectu != null && sectu.depth != 0) {
                u.loz -= Z(sectu.depth);
            }
        }
    }

    public static void MissileZrange(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        // Set the blocking bit to 0 temporarly so FAFgetzrange doesn't pick
        // up its own sprite
        int tempshort = sp.getCstat();
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK));

        FAFgetzrangepoint(sp.getX(), sp.getY(), sp.getZ() - 1, sp.getSectnum(), tmp_ptr[0].set(globhiz), tmp_ptr[1].set(globhihit), tmp_ptr[2].set(globloz), tmp_ptr[3].set(globlohit));

        globhiz = tmp_ptr[0].value;
        globhihit = tmp_ptr[1].value;
        globloz = tmp_ptr[2].value;
        globlohit = tmp_ptr[3].value;

        sp.setCstat(tempshort);

        DoActorGlobZ(SpriteNum);
    }

    public static int move_missile(int spritenum, int xchange, int ychange, int zchange, int ceildist, int flordist, int cliptype, int numtics) {
        Sprite sp = boardService.getSprite(spritenum);
        USER u = getUser(spritenum);
        if (sp == null) {
            return 0;
        }

        // Can't modify sprite sectors
        // directly becuase of linked lists
        int dasectnum = sp.getSectnum();
        int lastsectnum = dasectnum;

        // Can't modify sprite sectors
        // directly becuase of linked lists
        int daz = sp.getZ();
        int zh = 0;

        if (u != null && !TEST(sp.getCstat(), CSTAT_SPRITE_YCENTER)) {
            zh = u.zclip;
            daz -= zh;
        }

        clipmoveboxtracenum = 1;
        int retval = engine.clipmove(sp.getX(), sp.getY(), daz, dasectnum, (((long) xchange * numtics) << 11), (((long) ychange * numtics) << 11), ((sp.getClipdist()) << 2), ceildist, flordist, cliptype);

        sp.setX(clipmove_x);
        sp.setY(clipmove_y);
        dasectnum = clipmove_sectnum;

        clipmoveboxtracenum = 3;

        if (dasectnum == -1) {
            // we've gone beyond a white wall - kill it
            retval = 0;
            retval |= (HIT_PLAX_WALL);
            return (retval);
        }

        // took this out - may not be to relevant anymore

        if ((dasectnum != sp.getSectnum()) && (dasectnum >= 0)) {
            engine.changespritesect(spritenum, dasectnum);
        }

        // Set the blocking bit to 0 temporarly so FAFgetzrange doesn't pick
        // up its own sprite
        int tempshort = sp.getCstat();
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK));

        FAFgetzrangepoint(sp.getX(), sp.getY(), sp.getZ() - 1, sp.getSectnum(), tmp_ptr[0].set(globhiz), tmp_ptr[1].set(globhihit), tmp_ptr[2].set(globloz), tmp_ptr[3].set(globlohit));

        globhiz = tmp_ptr[0].value;
        globhihit = tmp_ptr[1].value;
        globloz = tmp_ptr[2].value;
        globlohit = tmp_ptr[3].value;

        sp.setCstat(tempshort);

        DoActorGlobZ(spritenum);

        // getzrangepoint moves water down
        // missiles don't need the water to be down
        MissileWaterAdjust(spritenum);

        daz = sp.getZ() + ((zchange * numtics) >> 3);
        if (u == null) {
            return 0;
        }

        // NOTE: this does not tell you when you hit a floor sprite
        // this case is currently treated like it hit a sector

        // test for hitting ceiling or floor
        if (daz - zh <= u.hiz + ceildist) {
            // normal code
            sp.setZ(u.hiz + zh + ceildist);
            if (retval == 0) {
                retval = dasectnum | HIT_SECTOR;
            }
        } else if (daz - zh > u.loz - flordist) {
            sp.setZ(u.loz + zh - flordist);
            if (retval == 0) {
                retval = dasectnum | HIT_SECTOR;
            }
        } else {
            sp.setZ(daz);
        }

        if (FAF_ConnectArea(sp.getSectnum())) {
            engine.setspritez(spritenum, sp.getX(), sp.getY(), sp.getZ());
        }

        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec != null && TEST(sec.getExtra(), SECTFX_WARP_SECTOR)) {
            Sprite sp_warp;

            if ((sp_warp = WarpPlane(sp.getX(), sp.getY(), sp.getZ(), dasectnum)) != null) {
                sp.setX(warp.x);
                sp.setY(warp.y);
                sp.setZ(warp.z);
                dasectnum = warp.sectnum;

                MissileWarpUpdatePos(spritenum, dasectnum);
                MissileWarpType(sp, sp_warp);
            }

            if (sp.getSectnum() != lastsectnum) {
                if ((sp_warp = WarpM(sp.getX(), sp.getY(), sp.getZ(), dasectnum)) != null) {
                    sp.setX(warp.x);
                    sp.setY(warp.y);
                    sp.setZ(warp.z);
                    dasectnum = warp.sectnum;

                    MissileWarpUpdatePos(spritenum, dasectnum);
                    MissileWarpType(sp, sp_warp);
                }
            }
        }

        sec = boardService.getSector(sp.getSectnum());
        if (sec != null) {
            if (retval != 0 && TEST(sec.getCeilingstat(), CEILING_STAT_PLAX)) {
                if (sp.getZ() < sec.getCeilingz()) {
                    retval &= ~(HIT_WALL | HIT_SECTOR);
                    retval |= (HIT_PLAX_WALL);
                }
            }

            if (retval != 0 && TEST(sec.getFloorstat(), FLOOR_STAT_PLAX)) {
                if (sp.getZ() > sec.getFloorz()) {
                    retval &= ~(HIT_WALL | HIT_SECTOR);
                    retval |= (HIT_PLAX_WALL);
                }
            }
        }

        return (retval);
    }    public static final State[] s_IconHeartLgAmmo = {new State(ICON_HEART_LG_AMMO, HEART_LG_AMMO_RATE, DoGet), new State(ICON_HEART_LG_AMMO + 1, HEART_LG_AMMO_RATE, DoGet)};

    public static int move_ground_missile(int spritenum, int xchange, int ychange, int ceildist, int flordist, int cliptype, int numtics) {
        USER u = getUser(spritenum);
        Sprite sp = boardService.getSprite(spritenum);
        if (sp == null || u == null) {
            return 0;
        }

        // Can't modify sprite sectors
        // directly becuase of linked lists
        int dasectnum = sp.getSectnum();
        int lastsectnum = dasectnum;

        int daz = sp.getZ();

        // climbing a wall
        if (u.z_tgt != -1) {
            if (klabs(u.z_tgt - sp.getZ()) > Z(40)) {
                if (u.z_tgt > sp.getZ()) {
                    sp.setZ(sp.getZ() + Z(30));
                } else {
                    sp.setZ(sp.getZ() - Z(30));
                }
                return 0;
            } else {
                u.z_tgt = 0;
            }
        }

        int retval = 0;
        int ox = sp.getX();
        int oy = sp.getY();
        sp.setX(sp.getX() + xchange / 2);
        sp.setY(sp.getY() + ychange / 2);

        dasectnum = engine.updatesector(sp.getX(), sp.getY(), dasectnum);
        if (dasectnum == -1) {
            // back up and try again
            dasectnum = lastsectnum = sp.getSectnum();
            sp.setX(ox);
            sp.setY(oy);
            clipmoveboxtracenum = 1;
            retval = engine.clipmove(sp.getX(), sp.getY(), daz, dasectnum, (((long) xchange * numtics) << 11), (((long) ychange * numtics) << 11), ((sp.getClipdist()) << 2), ceildist, flordist, cliptype);

            sp.setX(clipmove_x);
            sp.setY(clipmove_y);
            dasectnum = clipmove_sectnum;

            clipmoveboxtracenum = 3;
        }

        if (dasectnum == -1) {
            // we've gone beyond a white wall - kill it
            retval = 0;
            retval |= (HIT_PLAX_WALL);
            return (retval);
        }

        if (retval != 0) // ran into a white wall
        {
            return (retval);
        }

        u.z_tgt = 0;
        if ((dasectnum != sp.getSectnum()) && (dasectnum >= 0)) {
            engine.getzsofslope(dasectnum, sp.getX(), sp.getY(), fz, cz);
            int new_loz = fz.get();

            sp.setZ(new_loz);

            engine.changespritesect(spritenum, dasectnum);
        }

        engine.getzsofslope(sp.getSectnum(), sp.getX(), sp.getY(), fz, cz);
        u.hiz = cz.get();
        u.loz = fz.get();

        u.hi_sectp = u.lo_sectp = sp.getSectnum();
        u.hi_sp = u.lo_sp = -1;
        sp.setZ(u.loz - Z(8));

        if (klabs(u.hiz - u.loz) < Z(12)) {
            // we've gone into a very small place - kill it
            retval |= (HIT_PLAX_WALL);
            return (retval);
        }

        // getzrangepoint moves water down
        // missiles don't need the water to be down
        // MissileWaterAdjust(spritenum);

        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec != null && TEST(sec.getExtra(), SECTFX_WARP_SECTOR)) {
            Sprite sp_warp;

            if ((sp_warp = WarpPlane(sp.getX(), sp.getY(), sp.getZ(), dasectnum)) != null) {

                sp.setX(warp.x);
                sp.setY(warp.y);
                sp.setZ(warp.z);
                dasectnum = warp.sectnum;

                MissileWarpUpdatePos(spritenum, dasectnum);
                MissileWarpType(sp, sp_warp);
            }

            if (sp.getSectnum() != lastsectnum) {
                if ((sp_warp = WarpM(sp.getX(), sp.getY(), sp.getZ(), dasectnum)) != null) {

                    sp.setX(warp.x);
                    sp.setY(warp.y);
                    sp.setZ(warp.z);
                    dasectnum = warp.sectnum;

                    MissileWarpUpdatePos(spritenum, dasectnum);
                    MissileWarpType(sp, sp_warp);
                }
            }
        }

        return (retval);
    }

    public static void SpritesSaveable() {
        SaveData(DoSpriteFade);
        SaveData(DoFireFly);
        SaveData(DoGrating);
        SaveData(DoKey);
        SaveData(DoCoin);
        SaveData(DoGet);
        SaveData(s_DebrisNinja);
        SaveData(s_DebrisRat);
        SaveData(s_DebrisCrab);
        SaveData(s_DebrisStarFish);
        SaveData(s_RepairKit);
        SaveData(s_GoldSkelKey);
        SaveData(s_BlueKey);
        SaveData(s_BlueCard);
        SaveData(s_SilverSkelKey);
        SaveData(s_RedKey);
        SaveData(s_RedCard);
        SaveData(s_BronzeSkelKey);
        SaveData(s_GreenKey);
        SaveData(s_GreenCard);
        SaveData(s_RedSkelKey);
        SaveData(s_YellowKey);
        SaveData(s_YellowCard);
        SaveData(s_Key);
        SaveData(s_BlueKey);
        SaveData(s_RedKey);
        SaveData(s_GreenKey);
        SaveData(s_YellowKey);
        SaveData(s_Key);
        SaveData(s_RedCoin);
        SaveData(s_YellowCoin);
        SaveData(s_GreenCoin);
        SaveData(s_FireFly);
        SaveData(s_IconStar);
        SaveData(s_IconUzi);
        SaveData(s_IconLgUziAmmo);
        SaveData(s_IconUziFloor);
        SaveData(s_IconRocket);
        SaveData(s_IconLgRocket);
        SaveData(s_IconShotgun);
        SaveData(s_IconLgShotshell);
        SaveData(s_IconAutoRiot);
        SaveData(s_IconGrenadeLauncher);
        SaveData(s_IconLgGrenade);
        SaveData(s_IconLgMine);
        SaveData(s_IconGuardHead);
        SaveData(s_IconFireballLgAmmo);
        SaveData(s_IconHeart);
        SaveData(s_IconHeartLgAmmo);
        SaveData(s_IconMicroGun);
        SaveData(s_IconMicroBattery);
        SaveData(s_IconRailGun);
        SaveData(s_IconRailAmmo);
        SaveData(s_IconElectro);
        SaveData(s_IconSpell);
        SaveData(s_IconArmor);
        SaveData(s_IconMedkit);
        SaveData(s_IconChemBomb);
        SaveData(s_IconFlashBomb);
        SaveData(s_IconNuke);
        SaveData(s_IconCaltrops);
        SaveData(s_IconSmMedkit);
        SaveData(s_IconBooster);
        SaveData(s_IconHeatCard);
        SaveData(s_IconCloak);
        SaveData(s_IconFly);
        SaveData(s_IconNightVision);
        SaveData(s_IconFlag);
    }    public static final State[] s_IconMicroGun = {new State(ICON_MICRO_GUN, 100, DoGet).setNext()};



    public static final State[] s_IconMicroBattery = {new State(ICON_MICRO_BATTERY, 100, DoGet).setNext()};



    public static final State[] s_IconRailGun = {new State(ICON_RAIL_GUN, 100, DoGet).setNext()};



    public static final State[] s_IconRailAmmo = {new State(ICON_RAIL_AMMO, 100, DoGet).setNext()};



    public static final State[] s_IconElectro = {new State(ICON_ELECTRO, 25, DoGet), new State(ICON_ELECTRO + 1, 25, DoGet)};





    public static final State[] s_IconSpell = {new State(ICON_SPELL, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 1, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 2, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 3, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 4, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 5, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 6, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 7, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 8, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 9, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 10, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 11, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 12, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 13, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 14, ICON_SPELL_RATE, DoGet), new State(ICON_SPELL + 15, ICON_SPELL_RATE, DoGet)};



    public static final State[] s_IconArmor = {new State(ICON_ARMOR, 15, DoGet).setNext()};



    public static final State[] s_IconMedkit = {new State(ICON_MEDKIT, 15, DoGet).setNext()};



    public static final State[] s_IconChemBomb = {new State(ICON_CHEMBOMB, 15, DoGet).setNext()};



    public static final State[] s_IconFlashBomb = {new State(ICON_FLASHBOMB, 15, DoGet).setNext()};

    public static final State[] s_IconNuke = {new State(ICON_NUKE, 15, DoGet).setNext()};



    public static final State[] s_IconCaltrops = {new State(ICON_CALTROPS, 15, DoGet).setNext()};



    public static final State[] s_IconSmMedkit = {new State(ICON_SM_MEDKIT, 15, DoGet).setNext()};



    public static final State[] s_IconBooster = {new State(ICON_BOOSTER, 15, DoGet).setNext()};





    public static final State[] s_IconHeatCard = {new State(ICON_HEAT_CARD, 15, DoGet).setNext()};



    public static final State[] s_IconCloak = {new State(ICON_CLOAK, 20, DoGet).setNext()};



    public static final State[] s_IconFly = {new State(ICON_FLY, 20, DoGet), new State(ICON_FLY + 1, 20, DoGet), new State(ICON_FLY + 2, 20, DoGet), new State(ICON_FLY + 3, 20, DoGet), new State(ICON_FLY + 4, 20, DoGet), new State(ICON_FLY + 5, 20, DoGet), new State(ICON_FLY + 6, 20, DoGet), new State(ICON_FLY + 7, 20, DoGet)};



    public static final State[] s_IconNightVision = {new State(ICON_NIGHT_VISION, 20, DoGet).setNext()};



    public static final State[] s_IconFlag = {new State(ICON_FLAG, 32, DoGet), new State(ICON_FLAG + 1, 32, DoGet), new State(ICON_FLAG + 2, 32, DoGet)};




































}
