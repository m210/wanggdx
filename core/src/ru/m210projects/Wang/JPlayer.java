package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Wang.Type.Input;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.USER;

import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Factory.WangProcessor.MAXANGVEL;
import static ru.m210projects.Wang.Game.MessageInputMode;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Inv.INVENTORY_MEDKIT;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Panel.BORDER_BAR;
import static ru.m210projects.Wang.Player.PLAYER_HEIGHT;
import static ru.m210projects.Wang.Player.PLAYER_HORIZ_MAX;
import static ru.m210projects.Wang.Rooms.FAFcansee;
import static ru.m210projects.Wang.Sprites.MoveSkip4;
import static ru.m210projects.Wang.Sprites.MoveSkip8;
import static ru.m210projects.Wang.Type.MyTypes.BIT;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Weapon.DamageData;

public class JPlayer {

    // PLAYER QUOTES TO OTHER PLAYERS
    //////////////////////////////////////////////////////////////

    public static int quotebot, quotebotgoal;
    public static final int[] user_quote_time = new int[MAXUSERQUOTES];
    public static final String[] user_quote = new String[MAXUSERQUOTES];
    public static final int[] goalx = new int[MAX_SW_PLAYERS];
    public static final int[] goaly = new int[MAX_SW_PLAYERS];
    public static final int[] goalz = new int[MAX_SW_PLAYERS];

    //////////// Console Message Queue ////////////////////////////////////
    public static final int[] goalsect = new int[MAX_SW_PLAYERS];
    public static final int[] goalwall = new int[MAX_SW_PLAYERS];
    public static final int[] goalsprite = new int[MAX_SW_PLAYERS];
    public static final int[] goalplayer = new int[MAX_SW_PLAYERS];
    public static final int[] clipmovecount = new int[MAX_SW_PLAYERS];

    public static final short[] searchsect = new short[MAXSECTORS];
    public static final short[] searchparent = new short[MAXSECTORS];
    public static final byte[] dashow2dsector = new byte[(MAXSECTORS + 7) >> 3];

    ////////////////////////////////////////////////
    //Draw a string using a small graphic font
    ////////////////////////////////////////////////

    // BOT STUFF
    // ////////////////////////////////////////////////////////////////////////////////
    public static final int[][] fdmatrix = {
            // SWRD SHUR UZI SHOT RPG 40MM MINE RAIL HEAD HEAD2 HEAD3 HEART
            {128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 0}, // SWRD
            {1024, 512, 128, 128, 2560, 128, 2560, 128, 2560, 2560, 2560, 128, 128, 0}, // SHUR
            {2560, 1024, 512, 512, 2560, 128, 2560, 2560, 1024, 2560, 2560, 2560, 2560, 0}, // UZI
            {512, 512, 512, 512, 2560, 128, 2560, 512, 512, 512, 512, 512, 512, 0}, // SHOT
            {2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 0}, // RPG
            {512, 512, 512, 512, 2048, 512, 2560, 2560, 512, 2560, 2560, 2560, 2560, 0}, // 40MM
            {128, 128, 128, 128, 512, 128, 128, 128, 128, 128, 128, 128, 128, 0}, // MINE
            {1536, 1536, 1536, 1536, 2560, 1536, 1536, 1536, 1536, 1536, 1536, 1536, 1536, 0}, // RAIL
            {2560, 1024, 512, 1024, 1024, 1024, 2560, 512, 1024, 2560, 2560, 512, 512, 0}, // HEAD1
            {128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 0}, // HEAD2
            {512, 512, 512, 512, 512, 512, 512, 512, 512, 512, 512, 512, 512, 0}, // HEAD3
            {1024, 512, 128, 128, 2560, 512, 2560, 1024, 128, 2560, 1024, 1024, 1024, 0}, // HEART
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // NULL
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // NULL
    };
//    public char[][] fta_quotes = new char[NUMOFFIRSTTIMEACTIVE][64];

    public static void gametext(int x, int y, String t, int s) {
        boolean centre = (x == (320 >> 1));
        game.getFont(1).drawTextScaled(game.getRenderer(), x, y, t, 1.0f, s, 0, centre ? TextAlign.Center : TextAlign.Left, Transparent.None, ConvertType.Normal, false);
    }

    public static void minigametext(int x, int y, String t, int s, Transparent transparent, ConvertType convertType) {
        boolean centre = (x == (320 >> 1));
        game.getFont(0).drawTextScaled(game.getRenderer(), x, y, t, 1.0f, s, 4, centre ? TextAlign.Center : TextAlign.Left, transparent, convertType, true);
    }

    public static void adduserquote(String daquote) {
        for (int i = MAXUSERQUOTES - 1; i > 0; i--) {
            user_quote[i] = user_quote[i - 1];
            user_quote_time[i] = user_quote_time[i - 1];
        }
        user_quote[0] = daquote;
        user_quote_time[0] = 180;

        Console.out.println(daquote);
    }

    public static void operatefta() {
        int i, j, k;

        j = MESSAGE_LINE; // Baseline position on screen
        quotebot = Math.min(quotebot, j);
        if (MessageInputMode) {
            j -= 6; // Bump all lines up one to make room for new line
        }
        quotebotgoal = j;
        j = quotebot;

        for (i = 0; i < MAXUSERQUOTES; i++) {
            k = user_quote_time[i];
            if (k <= 0) {
                break;
            }

            if (cfg.BorderNum <= BORDER_BAR + 1) {
                // don't fade out
                if (k > 4) {
                    minigametext(320 >> 1, j, user_quote[i], 0, Transparent.None, ConvertType.Normal);
                } else if (k > 2) {
                    minigametext(320 >> 1, j, user_quote[i], 0, Transparent.Bit1, ConvertType.Normal);
                } else {
                    minigametext(320 >> 1, j, user_quote[i], 0, Transparent.Bit2, ConvertType.Normal);
                }
            } else {
                // don't fade out
                minigametext(320 >> 1, j, user_quote[i], 0, Transparent.None, ConvertType.Normal);
            }

            j -= 6;
        }
    }

    public static void BOT_UseInventory(PlayerStr p, int targetitem, Input syn) {
        // Try to get to item
        // Use whatever you're on too
        if (p.InventoryNum != targetitem) {
            syn.bits |= (1 << SK_INV_LEFT); // Scroll to it
        }
        syn.bits |= (1 << SK_INV_USE);
    }

    public static void BOT_ChooseWeapon(PlayerStr p, USER u, Input syn) {
        // If you have a nuke, fire it
        if (u.WeaponNum == WPN_MICRO && p.WpnRocketNuke != 0 && p.WpnRocketType != 2) {
            syn.bits ^= 15;
            syn.bits |= 4;
        } else {
            for (int weap = 9; weap >= 0; weap--) {
                if (weap <= u.WeaponNum) {
                    break;
                }
                if (TEST(p.WpnFlags, BIT(weap)) && p.WpnAmmo[weap] > DamageData[weap].min_ammo) {
                    syn.bits ^= 15;
                    syn.bits |= weap;
                    break;
                }
            }
        }
    }

    public static int getspritescore(int dapicnum) {

        switch (dapicnum) {
            case ICON_STAR:
                return (5);
            case ICON_UZI:
            case ICON_UZIFLOOR:
                return (20);
            case ICON_LG_UZI_AMMO:
                return (15);
            case ICON_HEART:
                return (160);
            case ICON_HEART_LG_AMMO:
                return (60);
            case ICON_GUARD_HEAD:
                return (170);
            case ICON_FIREBALL_LG_AMMO:
                return (70);
            case ICON_ROCKET:
            case ICON_LG_ROCKET:
            case ICON_MICRO_BATTERY:
                return (100);
            case ICON_SHOTGUN:
                return (130);
            case ICON_LG_SHOTSHELL:
                return (30);
            case ICON_MICRO_GUN:
                return (200);
            case ICON_GRENADE_LAUNCHER:
            case ICON_LG_MINE:
                return (150);
            case ICON_LG_GRENADE:
                return (50);
            case ICON_RAIL_GUN:
                return (180);
            case ICON_RAIL_AMMO:
                return (80);

            case ST_QUICK_EXIT:
            case ST_QUICK_SCAN:
            case ICON_MEDKIT:
            case ICON_CHEMBOMB:
            case ICON_FLASHBOMB:
            case ICON_NUKE:
            case ICON_CALTROPS:
            case TRACK_SPRITE:
            case ST1:
            case ST2:
            case ST_QUICK_JUMP:
            case ST_QUICK_JUMP_DOWN:
            case ST_QUICK_SUPER_JUMP:
                return (120);
        }

        return (0);
    }

    public static void computergetinput(int snum, Input syn) {
        int j, k, l, x1, y1, z1, x2, y2, z2, x3, y3, z3, dx, dy;
        int dist, daang, zang, fightdist, damyang;
        int splc, send;
        int dasect, damysect;
        PlayerStr p;

        if (!NETTEST && MoveSkip4 == 0) {
            return; // Make it so the bots don't slow the game down so bad!
        }

        p = Player[snum];
        USER u = getUser(p.PlayerSprite); // Set user struct
        if (u == null) {
            return;
        }

        int WpnNum = p.getWeapon();

        // Init local position variables
        x1 = p.posx;
        y1 = p.posy;
        z1 = p.posz - PLAYER_HEIGHT / 2;
        damyang = p.getAnglei();
        damysect = p.cursectnum;

        // Reset input bits
        syn.vel = 0;
        syn.svel = 0;
        syn.angvel = 0;
        syn.aimvel = 0;
        syn.bits = 0;

        // Always operate everything
        if (MoveSkip8 == 0) {
            syn.bits |= (1 << SK_OPERATE);
        }

        // If bot can't see the goal enemy, set target to himself so that he
        // will pick a new target
        if (TEST(Player[goalplayer[snum]].Flags, PF_DEAD) || STD_RANDOM_RANGE(1000) > 800) {
            goalplayer[snum] = snum;
        } else {
            x2 = Player[goalplayer[snum]].posx;
            y2 = Player[goalplayer[snum]].posy;
            z2 = Player[goalplayer[snum]].posz;
            Sprite sp = Player[goalplayer[snum]].getSprite();
            if (sp != null && !FAFcansee(x1, y1, z1 - (48 << 8), damysect, x2, y2, z2 - (48 << 8),
                    sp.getSectnum())) {
                goalplayer[snum] = snum;
            }
        }

        // Pick a new target player if goal is dead or target is itself
        if (goalplayer[snum] == snum) {
            j = 0x7fffffff;
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                if (i != snum) {
                    if (TEST(Player[i].Flags, PF_DEAD)) {
                        continue;
                    }

                    x2 = Player[i].posx;
                    y2 = Player[i].posy;
                    z2 = Player[i].posz;
                    Sprite sp = Player[i].getSprite();
                    if (sp == null) {
                        continue;
                    }

                    if (!FAFcansee(x1, y1, z1 - (48 << 8), damysect, x2, y2, z2 - (48 << 8),
                            sp.getSectnum())) {
                        continue;
                    }

                    dist = EngineUtils.sqrt((sp.getX() - x1)
                            * (sp.getX() - x1)
                            + (sp.getY() - y1) * (sp.getY() - y1));

                    if (dist < j) {
                        j = dist;
                        goalplayer[snum] = i;

                        goalx[snum] = sp.getX();
                        goaly[snum] = sp.getY();
                    }
                }
            }
        }

        // Pick a weapon
        BOT_ChooseWeapon(p, u, syn);

        // x2,y2,z2 is the coordinates of the target sprite
        x2 = Player[goalplayer[snum]].posx;
        y2 = Player[goalplayer[snum]].posy;
        z2 = Player[goalplayer[snum]].posz;

        // If bot is dead, either barf or respawn
        if (TEST(p.Flags, PF_DEAD)) {
            if (STD_RANDOM_RANGE(1000) > 990) {
                syn.bits |= (1 << SK_SPACE_BAR); // Respawn
            } else {
                syn.bits |= (1 << SK_SHOOT); // Try to barf
            }
        }

        // Need Health?
        if (u.Health < p.MaxHealth) {
            BOT_UseInventory(p, INVENTORY_MEDKIT, syn);
        }

        // Check the missile stat lists to see what's being fired and
        // take the appropriate action

        ListNode<Sprite> nextj;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_MISSILE); node != null; node = nextj) {
            nextj = node.getNext();
            Sprite sp = node.get();
            if (sp.getPicnum() == BOLT_THINMAN_R0) {
                syn.bits |= (1 << SK_JUMP); // Always jump when rockets being fired!
            }
        }

        Sprite sp = Player[goalplayer[snum]].getSprite();
        if (sp != null && !TEST(Player[goalplayer[snum]].Flags, PF_DEAD) && snum != goalplayer[snum]
                && ((FAFcansee(x1, y1, z1 - (24 << 8), damysect, x2, y2, z2 - (24 << 8),
                sp.getSectnum()))
                || (FAFcansee(x1, y1, z1 - (48 << 8), damysect, x2, y2, z2 - (48 << 8),
                sp.getSectnum())))) {
            // Shoot how often by skill level
            int shootrnd =  STD_RANDOM_RANGE(1000);
            if ((gNet.BotSkill <= 0 && shootrnd > 990) || (gNet.BotSkill == 1 && shootrnd > 550) || (gNet.BotSkill == 2 && shootrnd > 350)
                    || (gNet.BotSkill == 3)) {
                syn.bits |= (1 << SK_SHOOT);
            } else {
                syn.bits &= ~(1 << SK_SHOOT);
            }

            // Jump sometimes, to try to be evasive
            if (STD_RANDOM_RANGE(256) > 252) {
                syn.bits |= (1 << SK_JUMP);
            }

            // Make sure selected weapon is in range

            // Only fire explosive type weaps if you are not too close to the target!
            if (u.WeaponNum == WPN_MICRO || u.WeaponNum == WPN_GRENADE || u.WeaponNum == WPN_RAIL) {
                int x4, y4;
                engine.hitscan(x1, y1, z1 - PLAYER_HEIGHT, damysect, EngineUtils.sin((damyang + 512) & 2047),
                        EngineUtils.sin(damyang & 2047), (100 - p.getHorizi() - (int) p.horizoff) * 32, pHitInfo, CLIPMASK1);

                x4 = pHitInfo.hitx;
                y4 = pHitInfo.hity;

                if ((x4 - x1) * (x4 - x1) + (y4 - y1) * (y4 - y1) < 2560 * 2560) {
                    syn.bits &= ~(1 << SK_SHOOT);
                }
            }

            // Get fighting distance based on you and your opponents current weapons
            fightdist = WpnNum < fdmatrix.length ? fdmatrix[WpnNum][Player[goalplayer[snum]].getWeapon()] : 128;
            if (fightdist < 128) {
                fightdist = 128;
            }

            // Figure out your distance from the enemy target sprite
            dist = EngineUtils.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            if (dist == 0) {
                dist = 1;
            }
            daang = NORM_ANGLE(EngineUtils.getAngle(x2 + (Player[goalplayer[snum]].xvect >> 14) - x1,
                    y2 + (Player[goalplayer[snum]].yvect >> 14) - y1));

            zang = 100 - ((z2 - z1) * 8) / dist;
            fightdist = Math.max(fightdist, (klabs(z2 - z1) >> 4));

            x3 = x2 + ((x1 - x2) * fightdist / dist);
            y3 = y2 + ((y1 - y2) * fightdist / dist);
            syn.vel += (x3 - x1) * 2047 / dist;
            syn.svel += (y3 - y1) * 2047 / dist;

            // Strafe attack
            j = engine.getTotalClock() + snum * 13468;
            int i = EngineUtils.sin((j << 6) & 2047);
            i += EngineUtils.sin(((j + 4245) << 5) & 2047);
            i += EngineUtils.sin(((j + 6745) << 4) & 2047);
            i += EngineUtils.sin(((j + 15685) << 3) & 2047);
            dx = EngineUtils.sin((sp.getAng() + 512) & 2047);
            dy = EngineUtils.sin(sp.getAng() & 2047);
            if ((x1 - x2) * dy > (y1 - y2) * dx) {
                i += 8192;
            } else {
                i -= 8192;
            }
            syn.vel += ((EngineUtils.sin((daang + 1024) & 2047) * i) >> 17);
            syn.svel += ((EngineUtils.sin((daang + 512) & 2047) * i) >> 17);

            // Make aiming and running speed suck by skill level
            if (gNet.BotSkill <= 0) {
                daang = NORM_ANGLE((daang - 256) + STD_RANDOM_RANGE(512));
                syn.vel -= syn.vel / 2;
                syn.svel -= syn.svel / 2;
            } else if (gNet.BotSkill == 1) {
                daang = NORM_ANGLE((daang - 128) + STD_RANDOM_RANGE(256));
                syn.vel -= syn.vel / 8;
                syn.svel -= syn.svel / 8;
            } else if (gNet.BotSkill == 2) {
                daang = NORM_ANGLE((daang - 64) + STD_RANDOM_RANGE(128));
            }

            // Below formula fails in certain cases
            if (NETTEST) {
                syn.angvel = Math.min(Math.max((((daang + 1024 - damyang) & 2047) - 1024) >> 3, -MAXANGVEL), MAXANGVEL);
            } else {
                p.pang =  daang;
            }
            syn.aimvel = Math.min(Math.max((zang - p.getHorizi()) >> 1, -PLAYER_HORIZ_MAX), PLAYER_HORIZ_MAX);
            // Sets type of aiming, auto aim for bots
            syn.bits |= (1 << SK_AUTO_AIM);
            return;
        }

        goalsect[snum] = -1;
        if (goalsect[snum] < 0) {
            goalwall[snum] = -1;
            Sprite psp = p.getSprite();

            if (psp != null && sp != null) {
                int startsect = psp.getSectnum();
                int endsect = sp.getSectnum();

                Arrays.fill(dashow2dsector, (byte) 0);

                searchsect[0] = (short) startsect;
                searchparent[0] = -1;
                dashow2dsector[startsect >> 3] |= (byte) (1 << (startsect & 7));
                for (splc = 0, send = 1; splc < send; splc++) {
                    Sector sec = boardService.getSector(searchsect[splc]);
                    if (sec == null) {
                        continue;
                    }

                    for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        j = wal.getNextsector();
                        Sector nextSec = boardService.getSector(j);
                        if (nextSec == null) {
                            continue;
                        }

                        dx = ((wal.getWall2().getX() + wal.getX()) >> 1);
                        dy = ((wal.getWall2().getY() + wal.getY()) >> 1);
                        if ((engine.getceilzofslope(j, dx, dy) > engine.getflorzofslope(j, dx, dy)
                                - (28 << 8)) && ((nextSec.getLotag() < 15) || (nextSec.getLotag() > 22))) {
                            continue;
                        }
                        if (engine.getceilzofslope(j, dx, dy) < engine.getflorzofslope(searchsect[splc], dx, dy)
                                - (72 << 8)) {
                            continue;
                        }

                        if ((dashow2dsector[j >> 3] & (1 << (j & 7))) == 0) {
                            dashow2dsector[j >> 3] |= (byte) (1 << (j & 7));
                            searchsect[send] = (short) j;
                            searchparent[send] = (short) splc;
                            send++;
                            if (j == endsect) {
                                Arrays.fill(dashow2dsector, (byte) 0);
                                for (k = send - 1; k >= 0; k = searchparent[k]) {
                                    dashow2dsector[searchsect[k] >> 3] |= (byte) (1 << (searchsect[k] & 7));
                                }

                                for (k = send - 1; k >= 0; k = searchparent[k]) {
                                    if (searchparent[k] == 0) {
                                        break;
                                    }
                                }

                                goalsect[snum] = searchsect[k];
                                Sector sec1 = boardService.getSector(goalsect[snum]);

                                if (sec1 != null) {
                                    int startwall = sec1.getWallptr();
                                    int endwall = startwall + sec1.getWallnum();
                                    x3 = y3 = 0;
                                    for (ListNode<Wall> node = sec.getWallNode(); node != null; node = node.getNext()) {
                                        Wall wal1 = node.get();
                                        x3 += wal1.getX();
                                        y3 += wal1.getY();
                                    }
                                    x3 /= (endwall - startwall);
                                    y3 /= (endwall - startwall);


                                    Sector startSec = boardService.getSector(startsect);
                                    if (startSec != null) {
                                        startwall = startSec.getWallptr();

                                        l = 0;
                                        k = startwall;
                                        for (ListNode<Wall> node = sec.getWallNode(); node != null; node = node.getNext()) {
                                            Wall wal1 = node.get();
                                            int i = node.getIndex();
                                            if (wal1.getNextsector() != goalsect[snum]) {
                                                continue;
                                            }
                                            dx = wal1.getWall2().getX() - wal1.getX();
                                            dy = wal1.getWall2().getY() - wal1.getY();

                                            if ((x3 - x1) * (wal1.getY() - y1) <= (y3 - y1) * (wal1.getX() - x1)) {
                                                if ((x3 - x1) * (wal1.getWall2().getY() - y1) >= (y3 - y1)
                                                        * (wal1.getWall2().getX() - x1)) {
                                                    k = i;
                                                    break;
                                                }
                                            }

                                            dist = EngineUtils.sqrt(dx * dx + dy * dy);
                                            if (dist > l) {
                                                l = dist;
                                                k = i;
                                            }
                                        }
                                    }

                                    Wall kwall = boardService.getWall(k);
                                    goalwall[snum] = k;
                                    if (kwall != null) {
                                        daang = ((EngineUtils.getAngle(kwall.getWall2().getX() - kwall.getX(),
                                                kwall.getWall2().getY() - kwall.getY()) + 1536) & 2047);
                                        goalx[snum] = ((kwall.getX() + kwall.getWall2().getX()) >> 1)
                                                + (EngineUtils.sin((daang + 512) & 2047) >> 8);
                                        goaly[snum] = ((kwall.getY() + kwall.getWall2().getY()) >> 1) + (EngineUtils.sin(daang & 2047) >> 8);
                                        goalz[snum] = sec1.getFloorz() - (32 << 8);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if ((goalsect[snum] < 0) || (goalwall[snum] < 0)) {
            if (goalsprite[snum] < 0) {
                for (k = 0; k < 4; k++) {
                    int i = (STD_RANDOM_RANGE(32767) % boardService.getSectorCount());
                    for (ListNode<Sprite> node = boardService.getSectNode(i); node != null; node = node.getNext()) {
                        j = node.getIndex();
                        Sprite spr = node.get();
                        if ((spr.getXrepeat() <= 0) || (spr.getYrepeat() <= 0)) {
                            continue;
                        }
                        if (getspritescore(spr.getPicnum()) <= 0) {
                            continue;
                        }

                        if (FAFcansee(x1, y1, z1 - (32 << 8), damysect, spr.getX(), spr.getY(),
                                spr.getZ() - (4 << 8),  i)) {
                            goalx[snum] = spr.getX();
                            goaly[snum] = spr.getY();
                            goalz[snum] = spr.getZ();
                            goalsprite[snum] = j;
                            break;
                        }
                    }
                }
            }

            x2 = goalx[snum];
            y2 = goaly[snum];
            dist = EngineUtils.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            if (dist <= 164) {
                for (k = 0; k < 8; k++) {
                    int i = (STD_RANDOM_RANGE(32767) % boardService.getSectorCount());
                    for (ListNode<Sprite> node = boardService.getSectNode(i); node != null; node = node.getNext()) {
                        j = node.getIndex();
                        Sprite spr = node.get();
                        if ((spr.getXrepeat() <= 0) || (spr.getYrepeat() <= 0) || j == goalsprite[snum]) {
                            continue;
                        }

                        if (FAFcansee(x1, y1, z1 - (32 << 8), damysect, spr.getX(), spr.getY(), spr.getZ() - (4 << 8),  i)) {
                            dist = EngineUtils.sqrt((spr.getX() - x1) * (spr.getX() - x1) + (spr.getY() - y1) * (spr.getY() - y1));
                            if (dist < 2048 || STD_RANDOM_RANGE(1000) > 500) {
                                continue;
                            }

                            goalx[snum] = spr.getX();
                            goaly[snum] = spr.getY();
                            goalz[snum] = spr.getZ();
                            goalsprite[snum] = j;
                            return;
                        }
                    }
                }
                int daz = ((100 - p.getHorizi()) * 2000);
                engine.hitscan(x1, y1, z1, p.cursectnum, // Start position
                        EngineUtils.sin(NORM_ANGLE(p.getAnglei() + 512)), // X vector of 3D ang
                        EngineUtils.sin(NORM_ANGLE(p.getAnglei())), // Y vector of 3D ang
                        daz, // Z vector of 3D ang
                        pHitInfo, 0xFFFFFFFF);

                dist = EngineUtils.sqrt((pHitInfo.hitx - x1) * (pHitInfo.hitx - x1) + (pHitInfo.hity - y1) * (pHitInfo.hity - y1));
                if (dist < 512) {
                    syn.angvel += STD_RANDOM_RANGE(256);
                } else {
                    goalx[snum] = pHitInfo.hitx;
                    goaly[snum] = pHitInfo.hity;
                    goalsprite[snum] = -1;
                }

                return;
            }

            daang = EngineUtils.getAngle(x2 - x1, y2 - y1);
            syn.vel += (x2 - x1) * 2047 / dist;
            syn.svel += (y2 - y1) * 2047 / dist;
            syn.angvel = Math.min(Math.max((((daang + 1024 - damyang) & 2047) - 1024) >> 3, -MAXANGVEL), MAXANGVEL);
        } else {
            goalsprite[snum] = -1;
        }

        x3 = p.posx;
        y3 = p.posy;
        z3 = p.posz;
        dasect = p.cursectnum;
        int i = engine.clipmove(x3, y3, z3, dasect, p.xvect, p.yvect, 164, 4 << 8, 4 << 8, CLIPMASK0);
        if (i == 0) {
            x3 = p.posx;
            y3 = p.posy;
            z3 = p.posz + (24 << 8);
            dasect = p.cursectnum;
            i = engine.clipmove(x3, y3, z3, dasect, p.xvect, p.yvect, 164, 4 << 8, 4 << 8, CLIPMASK0);
        }

        if (i != 0) {
            clipmovecount[snum]++;

            j = 0;
            if ((i & HIT_MASK) == HIT_WALL) // Hit a wall (49152 for sprite)
            {
                Wall hitWall = boardService.getWall(NORM_HIT_INDEX(i));
                if (hitWall != null && hitWall.getNextsector() >= 0) {
                    if (engine.getflorzofslope(hitWall.getNextsector(), p.posx, p.posy) <= p.posz
                            + (24 << 8)) {
                        j |= 1;
                    }
                    if (engine.getceilzofslope(hitWall.getNextsector(), p.posx, p.posy) >= p.posz
                            - (24 << 8)) {
                        j |= 2;
                    }
                }
            }

            if ((i & HIT_MASK) == HIT_SPRITE) {
                j = 1;
            }
            // Jump
            if ((j & 1) != 0) {
                if (clipmovecount[snum] == 4) {
                    syn.bits |= (1 << SK_JUMP);
                }
            }
            // Crawl
            if ((j & 2) != 0) {
                syn.bits |= (1 << SK_CRAWL);
            }

            // Strafe attack
            daang = EngineUtils.getAngle(x2 - x1, y2 - y1);
            if ((i & HIT_MASK) == HIT_WALL) {
                int wallIndex = NORM_HIT_INDEX(i);
                Wall hitWall = boardService.getWall(wallIndex);
                if (hitWall != null) {
                    daang = EngineUtils.getAngle(hitWall.getWall2().getX() - hitWall.getX(),
                            hitWall.getWall2().getY() - hitWall.getY());
                }
            }

            j = engine.getTotalClock() + snum * 13468;
            i = EngineUtils.sin((j << 6) & 2047);
            i += EngineUtils.sin(((j + 4245) << 5) & 2047);
            i += EngineUtils.sin(((j + 6745) << 4) & 2047);
            i += EngineUtils.sin(((j + 15685) << 3) & 2047);
            syn.vel += ((EngineUtils.sin((daang + 1024) & 2047) * i) >> 17);
            syn.svel += ((EngineUtils.sin((daang + 512) & 2047) * i) >> 17);

            // Try to Open
            if ((clipmovecount[snum] & 31) == 2) {
                syn.bits |= (1 << SK_OPERATE);
            }
            // *TODO: In Duke, this is Kick, but I need to select sword then fire in SW
//	        if ((clipmovecount[snum]&31) == 17) syn.bits |= (1<<22);
            if (clipmovecount[snum] > 32) {
                syn.vel = syn.svel = 0;

                int daz = ((100 - p.getHorizi()) * 2000);
                engine.hitscan(x1, y1, z1, p.cursectnum, // Start position
                        EngineUtils.sin(NORM_ANGLE(p.getAnglei() + 512)), // X vector of 3D ang
                        EngineUtils.sin(NORM_ANGLE(p.getAnglei())), // Y vector of 3D ang
                        daz, // Z vector of 3D ang
                        pHitInfo, 0xFFFFFFFF);

                dist = EngineUtils.sqrt((pHitInfo.hitx - x1) * (pHitInfo.hitx - x1) + (pHitInfo.hity - y1) * (pHitInfo.hity - y1));
                if (dist < 512) {
                    syn.bits |= (1 << SK_TURN_180);
                    syn.angvel += STD_RANDOM_RANGE(256);
                } else {
                    goalx[snum] = pHitInfo.hitx;
                    goaly[snum] = pHitInfo.hity;
                    goalz[snum] = pHitInfo.hitz;

                    goalsect[snum] = -1;
                    goalwall[snum] = -1;
                    clipmovecount[snum] = 0;
                }
            }

            goalsprite[snum] = -1;
        } else {
            clipmovecount[snum] = 0;
        }

        if (p.getHorizi() < 100) {
            syn.aimvel += 1;
        }

        if (p.getHorizi() > 100) {
            syn.aimvel -= 1;
        }

        if ((goalsect[snum] >= 0) && (goalwall[snum] >= 0)) {
            x2 = goalx[snum];
            y2 = goaly[snum];
            dist = EngineUtils.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            if (dist == 0) {
                return;
            }
            daang = EngineUtils.getAngle(x2 - x1, y2 - y1);
            if ((goalwall[snum] >= 0) && (dist < 4096)) {
                Wall w = boardService.getWall(goalwall[snum]);
                if (w != null) {
                    daang = ((EngineUtils.getAngle(w.getWall2().getX() - w.getX(),
                            w.getWall2().getY() - w.getY()) + 1536) & 2047);
                }
            }
            syn.vel += (x2 - x1) * 2047 / dist;
            syn.svel += (y2 - y1) * 2047 / dist;
            syn.angvel = Math.min(Math.max((((daang + 1024 - damyang) & 2047) - 1024) >> 3, -MAXANGVEL), MAXANGVEL);
        }
    }

}
