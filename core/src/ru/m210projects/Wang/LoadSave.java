package ru.m210projects.Wang;

import org.jetbrains.annotations.NotNull;
import ru.m210projects.Build.Board;
import ru.m210projects.Build.Pattern.Tools.SaveManager;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Wang.Menus.MenuCorruptGame;
import ru.m210projects.Wang.Type.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Enemies.Bunny.Bunny_Count;
import static ru.m210projects.Wang.Enemies.Sumo.*;
import static ru.m210projects.Wang.Factory.WangMenuHandler.CORRUPTLOAD;
import static ru.m210projects.Wang.Factory.WangNetwork.CommPlayers;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JSector.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.DoPlayerDivePalette;
import static ru.m210projects.Wang.Palette.DoPlayerNightVisionPalette;
import static ru.m210projects.Wang.Player.NormalVisibility;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Text.PutStringInfo;
import static ru.m210projects.Wang.Track.Track;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.ResourceHandler.*;
import static ru.m210projects.Wang.Weapon.*;

public class LoadSave {

    public static final String savsign = "SHWR";
    public static final int gdxVersionSave = 100;
    public static final int gdxCurrentSave = 102; // 1.16 == 101
    public static final int SAVETIME = 8;
    public static final int SAVENAME = 32;
    public static final int SAVESCREENSHOTSIZE = 160 * 100;
    public static final char[] filenum = new char[4];
    public static boolean gQuickSaving;
    public static boolean gAutosaveRequest;
    public static final LSInfo lsInf = new LSInfo();
    public static final SafeLoader loader = new SafeLoader();
    public static int quickslot = 0;
    public static Entry lastload;

    public static void FindSaves(Directory dir) {
        for (Entry file : dir) {
            if (file.isExtension("sav") && file instanceof FileEntry) {
                try (InputStream is = file.getInputStream()) {
                    String signature = StreamUtils.readString(is, 4);
                    if (signature.isEmpty()) {
                        continue;
                    }

                    if (signature.equals(savsign)) {
                        int nVersion = StreamUtils.readShort(is);
                        if (nVersion >= gdxVersionSave) {
                            long time = StreamUtils.readLong(is);
                            String savname = StreamUtils.readString(is, SAVENAME);
                            game.pSavemgr.add(savname, time, (FileEntry) file);
                        }
                    }
                } catch (Exception ignored) {
                }
            }
        }
        game.pSavemgr.sort();
    }

    public static int lsReadLoadData(FileEntry file) {
        if (file.exists()) {
            ArtEntry pic = engine.getTile(SaveManager.Screenshot);
            if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
                pic = engine.allocatepermanenttile(SaveManager.Screenshot, 160, 100);
            }

            try (InputStream is = file.getInputStream()) {
                int nVersion = checkSave(is) & 0xFFFF;
                lsInf.clear();

                if (nVersion == gdxCurrentSave) {
                    lsInf.date = game.date.getDate(StreamUtils.readLong(is));
                    StreamUtils.skip(is, SAVENAME);

                    lsInf.read(is);
                    if (is.available() <= SAVESCREENSHOTSIZE) {
                        return -1;
                    }

                    ((DynamicArtEntry) pic).copyData(StreamUtils.readBytes(is, SAVESCREENSHOTSIZE));

                    lsInf.addonfile = null;
                    if (StreamUtils.readBoolean(is)) {
                        boolean isPackage = StreamUtils.readBoolean(is);
                        String fullname = StreamUtils.readDataString(is);
                        if (isPackage && !fullname.isEmpty()) {
                            lsInf.addonfile = "File: " + getFilename(fullname);
                        } else {
                            lsInf.addonfile = "Addon: " + getFilename(fullname);
                        }
                    }

                    return 1;
                } else {
                    lsInf.info = "Incompatible ver. " + nVersion + " != " + gdxCurrentSave;
                    return -1;
                }
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }

        lsInf.clear();
        return -1;
    }

    public static String getFilename(String path) {
        if (path != null) {
            path = FileUtils.getPath(path).getFileName().toString();
        }

        return path;
    }

    public static String makeNum(int num) {
        filenum[3] = (char) ((num % 10) + 48);
        filenum[2] = (char) (((num / 10) % 10) + 48);
        filenum[1] = (char) (((num / 100) % 10) + 48);
        filenum[0] = (char) (((num / 1000) % 10) + 48);

        return new String(filenum);
    }

    public static int checkSave(InputStream is) throws IOException {
        String signature = StreamUtils.readString(is, 4);
        if (!signature.equals(savsign)) {
            return 0;
        }

        return StreamUtils.readShort(is);
    }

    public static void savegame(Directory dir, String savename, String filename) {
        FileEntry file = dir.getEntry(filename);
        if (file.exists()) {
            if (!file.delete()) {
                PutStringInfo(Player[myconnectindex], "Game not saved. Access denied!");
                return;
            }
        }

        Path path = dir.getPath().resolve(filename);
        try (OutputStream os = new BufferedOutputStream(Files.newOutputStream(path))) {
            long time = game.date.getCurrentDate();
            save(os, savename, time);

            file = dir.addEntry(path);
            if (file.exists()) {
                game.pSavemgr.add(savename, time, file);
                lastload = file;
                PutStringInfo(Player[myconnectindex], "Game saved");
            } else {
                throw new FileNotFoundException(filename);
            }
        } catch (Exception e) {
            PutStringInfo(Player[myconnectindex], "Game not saved. Access denied!");
        }
    }

    public static void save(OutputStream os, String savename, long time) throws IOException {
        SaveHeader(os, savename, time);
        SaveGDXBlock(os);
        SaveMap(os);
//        SavePanelSprites(os);
        SavePlayers(os);
        SaveSectorUserInfos(os);
        SaveUserInfos(os);
        SaveSectorObjects(os);
        SaveSineSect(os);

        StreamUtils.writeInt(os, x_min_bound);
        StreamUtils.writeInt(os, y_min_bound);
        StreamUtils.writeInt(os, x_max_bound);
        StreamUtils.writeInt(os, y_max_bound);

        SaveTracks(os);

        StreamUtils.writeInt(os, screenpeek);
        StreamUtils.writeInt(os, totalsynctics);

        SaveAnims(os);

        StreamUtils.writeInt(os, engine.getTotalClock());
        StreamUtils.writeInt(os, engine.getrand());

        StreamUtils.writeShort(os, NormalVisibility);
        StreamUtils.writeInt(os, visibility);
        StreamUtils.writeByte(os, parallaxtype);
        StreamUtils.writeInt(os, game.getRenderer().getParallaxOffset());
        writeBytes(os, pskyoff);
        StreamUtils.writeShort(os, pskybits);

        StreamUtils.writeBoolean(os, MoveSkip2);
        StreamUtils.writeInt(os, MoveSkip4);
        StreamUtils.writeInt(os, MoveSkip8);

        SaveMirrors(os);
        SaveQueues(os);
        SaveStuff(os);
    }

    private static void SaveStuff(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, PlayClock);
        StreamUtils.writeInt(os, Kills);
        StreamUtils.writeInt(os, TotalKillable);

        // game settings
        gNet.save(os);

        for (int i = 0; i < MAXTILES; i++) {
            StreamUtils.writeInt(os, engine.getTile(i).getFlags());
        }
        StreamUtils.writeShort(os, LevelSecrets);

        show2dsector.writeObject(os);
        show2dwall.writeObject(os);
        show2dsprite.writeObject(os);
        StreamUtils.writeInt(os, Bunny_Count);

        StreamUtils.writeBoolean(os, GodMode);

        StreamUtils.writeBoolean(os, serpwasseen);
        StreamUtils.writeBoolean(os, sumowasseen);
        StreamUtils.writeBoolean(os, zillawasseen);
        writeBytes(os, BossSpriteNum, 3);
    }

    private static void SaveMirrors(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, mirrorcnt);
        StreamUtils.writeBoolean(os, mirrorinview);
        StreamUtils.writeInt(os, mirror.length);
        for (MirrorType mirrorType : mirror) {
            mirrorType.save(os);
        }
    }

    private static void SaveQueues(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, StarQueueHead);
        writeBytes(os, StarQueue, MAX_STAR_QUEUE);
        StreamUtils.writeInt(os, HoleQueueHead);
        writeBytes(os, HoleQueue, MAX_HOLE_QUEUE);
        StreamUtils.writeInt(os, WallBloodQueueHead);
        writeBytes(os, WallBloodQueue, MAX_WALLBLOOD_QUEUE);
        StreamUtils.writeInt(os, FloorBloodQueueHead);
        writeBytes(os, FloorBloodQueue, MAX_FLOORBLOOD_QUEUE);
        StreamUtils.writeInt(os, GenericQueueHead);
        writeBytes(os, GenericQueue, MAX_GENERIC_QUEUE);
        StreamUtils.writeInt(os, LoWangsQueueHead);
        writeBytes(os, LoWangsQueue, MAX_LOWANGS_QUEUE);
    }

    private static void SaveVersion(OutputStream os) throws IOException {
        StreamUtils.writeString(os, savsign);
        StreamUtils.writeShort(os, gdxCurrentSave);
    }

    private static void SaveScreenshot(OutputStream os) throws IOException {
        StreamUtils.writeBytes(os, gGameScreen.captBuffer, SAVESCREENSHOTSIZE);
        gGameScreen.captBuffer = null;
    }

    private static void SaveGDXBlock(OutputStream os) throws IOException {
        SaveScreenshot(os);

        byte warp_on = 0;
        if (mUserFlag == UserFlag.Addon) {
            warp_on = (byte) ((currentGame != null) ? 1 : 2);
        }
        if (mUserFlag == UserFlag.UserMap) {
            warp_on = 2;
        }

        StreamUtils.writeByte(os, warp_on);
        if (warp_on == 1) {// user episode
            boolean isPacked = currentGame.getEpisodeEntry().isPackageEpisode();
            StreamUtils.writeBoolean(os, isPacked);
            StreamUtils.writeDataString(os, currentGame.getPath());
        }
    }

    private static void SaveHeader(OutputStream os, String savename, long time) throws IOException {
        SaveVersion(os);

        StreamUtils.writeLong(os, time);
        StreamUtils.writeString(os, savename, SAVENAME);

        StreamUtils.writeInt(os, Level);
        StreamUtils.writeInt(os, Skill);
    }

    private static void SavePlayers(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, numplayers);
        StreamUtils.writeShort(os, CommPlayers);
        StreamUtils.writeShort(os, myconnectindex);
        StreamUtils.writeShort(os, connecthead);
        writeBytes(os, connectpoint2);

        for (int i = 0; i < numplayers; i++) {
            Player[i].writeObject(os);
        }
    }

    private static void SaveMap(OutputStream os) throws IOException {
        if (boardfilename != null && boardfilename.exists()) {
            if (boardfilename instanceof FileEntry) {
                StreamUtils.writeString(os, ((FileEntry) boardfilename).getPath().toString(), 144);
            } else {
                StreamUtils.writeString(os, boardfilename.getName(), 144);
            }
        } else {
            StreamUtils.writeString(os, "", 144);
        }

        Board board = boardService.getBoard();
        Sector[] sectors = board.getSectors();
        StreamUtils.writeInt(os, sectors.length);
        for (Sector s : sectors) {
            s.writeObject(os);
        }

        Wall[] walls = board.getWalls();
        StreamUtils.writeInt(os, walls.length);
        for (Wall wal : walls) {
            wal.writeObject(os);
        }

        java.util.List<Sprite> sprites = board.getSprites();
        StreamUtils.writeInt(os, sprites.size());
        for (Sprite s : sprites) {
            s.writeObject(os);
        }

        StreamUtils.writeInt(os, FinishTimer);
        StreamUtils.writeBoolean(os, FinishedLevel);
        StreamUtils.writeInt(os, FinishAnim);
        StreamUtils.writeInt(os, playTrack);
    }

    private static void SaveSectorUserInfos(OutputStream os) throws IOException {
        // Sector User information
        StreamUtils.writeInt(os, boardService.getSectorCount());
        for (int i = 0; i < boardService.getSectorCount(); i++) {
            Sect_User su = getSectUser(i);
            // write trailer
            StreamUtils.writeInt(os, su != null ? i : -1);
            if (su != null) {
                su.writeObject(os);
            }
        }
    }

    private static void SaveUserInfos(OutputStream os) throws IOException {
        int numsprites = boardService.getSpriteCount();
        StreamUtils.writeInt(os, numsprites);
        for (int i = 0; i < numsprites; i++) {
            USER u = getUser(i);
            // write header
            StreamUtils.writeInt(os, u != null ? i : -1);
            if (u != null) {
                u.writeObject(os);
            }
        }
    }

    private static void SaveSectorObjects(OutputStream os) throws IOException {
        // Sector User information
        StreamUtils.writeInt(os, SectorObject.length);
        for (Sector_Object sectorObject : SectorObject) {
            sectorObject.writeObject(os);
        }
    }

    private static void SaveSineSect(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, SineWaveFloor.length);
        StreamUtils.writeInt(os, SineWaveFloor[0].length);
        for (SINE_WAVE_FLOOR[] sineWaveFloors : SineWaveFloor) {
            for (int j = 0; j < SineWaveFloor[0].length; j++) {
                sineWaveFloors[j].save(os);
            }
        }

        StreamUtils.writeInt(os, SineWall.length);
        StreamUtils.writeInt(os, SineWall[0].length);
        for (SINE_WALL[] sineWalls : SineWall) {
            for (int j = 0; j < SineWall[0].length; j++) {
                sineWalls[j].save(os);
            }
        }

        StreamUtils.writeInt(os, SpringBoard.length);
        for (Spring_Board springBoard : SpringBoard) {
            springBoard.save(os);
        }
    }

    private static void SaveTracks(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, Track.length);
        for (TRACK tr : Track) {
            StreamUtils.writeInt(os, tr.ttflags);
            StreamUtils.writeShort(os, tr.flags);
            StreamUtils.writeShort(os, tr.NumPoints);
            if (tr.NumPoints != 0) {
                for (int j = 0; j < tr.NumPoints; j++) {
                    TRACK_POINT tp = tr.TrackPoint[j];
                    StreamUtils.writeInt(os, tp.x);
                    StreamUtils.writeInt(os, tp.y);
                    StreamUtils.writeInt(os, tp.z);

                    StreamUtils.writeShort(os, tp.ang);
                    StreamUtils.writeShort(os, tp.tag_low);
                    StreamUtils.writeShort(os, tp.tag_high);
                }
            }
        }
    }

    private static void SaveAnims(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, AnimCnt);
        for (int i = 0; i < AnimCnt; i++) {
            pAnim[i].save(os);
        }
    }

    public static void quicksave() {
        if (numplayers > 1 || game.net.FakeMultiplayer) {
            return;
        }
        if (!TEST(Player[myconnectindex].Flags, PF_DEAD)) {
            gQuickSaving = true;
        }
    }

    public static void quickload() {
        if (numplayers > 1) {
            return;
        }
        final FileEntry loadFile = game.pSavemgr.getLast();

        if (canLoad(loadFile)) {
            game.changeScreen(gLoadingScreen.setTitle(loadFile.getName()));
            gLoadingScreen.init(() -> {
                if (!loadgame(loadFile)) {
                    game.show();
                }
            });
        }
    }

    public static void checkfile(InputStream is) throws IOException {
        int nVersion = checkSave(is);

        if (nVersion != gdxCurrentSave) {
            return;
        }

        if (!loader.load(is)) {
            throw new RuntimeException("Can't load the file");
        }

    }

    public static boolean canLoad(Entry fil) {
        return fil.load(is -> {
            int nVersion = checkSave(is) & 0xFFFF;
            if (nVersion != gdxCurrentSave) {
                if (nVersion >= gdxVersionSave) {
                    final GameInfo addon = loader.LoadGDXHeader(is);
                    if (loader.Level <= 20 && loader.Skill >= 0 && loader.Skill < 4 && loader.warp_on != 2) {
                        // if not usermap
                        game.menu.mOpen(getMenuCorruptGame(addon), -1);
                    }
                }
                throw new IOException("Wrong file version");
            }
        });
    }

    @NotNull
    private static MenuCorruptGame getMenuCorruptGame(GameInfo addon) {
        MenuCorruptGame menu = (MenuCorruptGame) game.menu.mMenus[CORRUPTLOAD];
        menu.setRunnable(() -> {
            GameInfo game = addon != null ? addon : defGame;
            int nEpisode = game.getNumEpisode(loader.Level);
            int nLevel = game.getNumLevel(loader.Level);
            gGameScreen.newgame(false, game, nEpisode, nLevel, loader.Skill);
        });
        return menu;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean loadgame(Entry file) {
        boolean status = file.load(LoadSave::checkfile);
        if (status) {
            load();
            if (lastload == null || !lastload.exists()) {
                lastload = file;
            }

            if (loader.getMessage() != null) {
                PutStringInfo(Player[myconnectindex], loader.getMessage());
            }

            return true;
        }

        if (file.exists()) {
            PutStringInfo(Player[myconnectindex], "Incompatible version of saved game found!");
        } else {
            PutStringInfo(Player[myconnectindex], "Can't access to file or file not found!");
        }
        return false;
    }

    public static void LoadGDXBlock() {
        if (loader.warp_on == 0) {
            mUserFlag = UserFlag.None;
        }
        if (loader.warp_on == 1) {
            mUserFlag = UserFlag.Addon;
        }
        if (loader.warp_on == 2) {
            mUserFlag = UserFlag.UserMap;
        }

        if (mUserFlag == UserFlag.Addon) {
            GameInfo ini = loader.addon;
            checkEpisodeResources(ini);
        } else {
            resetEpisodeResources();
        }
    }

    public static void load() {
        if (rec != null) {
            rec.close();
        }

        Level = loader.Level;
        Skill = loader.Skill;

        // Clear Sprite Extension structure
        clearSecUser();
        clearUser();

        LoadGDXBlock();
        MapLoad();
        LoadPlayers();
        LoadSectorUserInfos();
        LoadUserInfos();
        LoadSectorObjects();
        LoadSineSect();

        x_min_bound = loader.x_min_bound;
        y_min_bound = loader.y_min_bound;
        x_max_bound = loader.x_max_bound;
        y_max_bound = loader.y_max_bound;

        LoadTracks();

        screenpeek = loader.screenpeek;
        totalsynctics = loader.totalsynctics;

        LoadAnims();

        engine.getTimer().setTotalClock(loader.totalclock);
        engine.srand(loader.randomseed);
        NormalVisibility = loader.NormalVisibility;
        visibility = loader.visibility;
        parallaxtype = loader.parallaxtype;
        game.getRenderer().setParallaxOffset(loader.parallaxyoffs);
        System.arraycopy(loader.pskyoff, 0, pskyoff, 0, MAXPSKYTILES);
        System.arraycopy(pskyoff, 0, zeropskyoff, 0, MAXPSKYTILES);
        pskybits = loader.pskybits;
        MoveSkip2 = loader.MoveSkip2;
        MoveSkip4 = loader.MoveSkip4;
        MoveSkip8 = loader.MoveSkip8;

        LoadMirrors();
        LoadQueues();
        LoadStuff();

        StopFX();
        screenpeek = myconnectindex;
        PlayingLevel = Level;

        for (int i = connecthead; i != -1; i = connectpoint2[i]) {
            Player[i].PlayerTalking = false;
            Player[i].TalkVocnum = -1;
            Player[i].TalkVocHandle = null;
            Player[i].StartColor = 0;
        }

        for (int i = AnimCnt - 1; i >= 0; i--) {
            Anim gAnm = pAnim[i];
            Object object = (gAnm.ptr = GetAnimObject(gAnm.index, gAnm.type));
            switch (gAnm.type) {
                case FloorZ:
                    game.pInt.setfloorinterpolate(gAnm.index, (Sector) object);
                    break;
                case CeilZ:
                    game.pInt.setceilinterpolate(gAnm.index, (Sector) object);
                    break;
                case SpriteZ:
                    game.pInt.setsprinterpolate(gAnm.index, (Sprite) object);
                    break;
                default:
                    break;
            }
        }

        game.doPrecache(() -> {
            InitSpecialTextures();
            DoPlayerDivePalette(Player[myconnectindex]);
            DoPlayerNightVisionPalette(Player[myconnectindex]);

            cfg.AutoAim = (Player[myconnectindex].Flags & Gameutils.PF_AUTO_AIM) != 0;

            Sound.setEcho(false, 0);

            game.gPaused = false;

            game.nNetMode = NetMode.Single;

            game.changeScreen(gGameScreen);
            int SavePlayClock = PlayClock;
            InitTimingVars();
            PlayClock = SavePlayClock;

            game.pNet.WaitForAllPlayers(0);
            game.pNet.ready2send = true;

            triedplay = false;
            if (loader.playTrack != -1) {
                CDAudio_Play(loader.playTrack, true); // track, loop - Level songs are looped
            } else {
                CDAudio_Play(RedBookSong[Level], true);
            }
            StartAmbientSound();

            System.gc();
        });
    }

    private static void MapLoad() {
        boardfilename = loader.boardfilename;
        boardService.setBoard(new Board(null, loader.sector, loader.wall, loader.sprite));

        FinishTimer = loader.FinishTimer;
        FinishedLevel = loader.FinishedLevel;
        FinishAnim = loader.FinishAnim;
        // playTrack = loader.playTrack; don't set it here
    }

    private static void LoadPlayers() {
        numplayers = loader.numplayers;
        CommPlayers = loader.CommPlayers;
        myconnectindex = loader.myconnectindex;
        connecthead = loader.connecthead;
        System.arraycopy(loader.connectpoint2, 0, connectpoint2, 0, MAXPLAYERS);
        for (int i = 0; i < numplayers; i++) {
            Player[i].copy(loader.Player[i]);
        }
    }

    private static void LoadSectorUserInfos() {
        for (int i = 0; i < loader.SectUser.size; i++) {
            Sect_User pSectUser = loader.SectUser.items[i];
            if (pSectUser != null) {
                setSectUser(i, pSectUser);
            }
        }
    }

    private static void LoadUserInfos() {
        for (int i = 0; i < loader.pUser.size; i++) {
            USER user = loader.pUser.items[i];
            if (user != null) {
                setUser(i, user);
            }
        }
    }

    private static void LoadSectorObjects() {
        for (int i = 0; i < loader.SectorObject.length; i++) {
            SectorObject[i] = Sector_Object.copy(loader.SectorObject[i]);
        }
    }

    private static void LoadSineSect() {
        int len = loader.SineWaveFloor.length;
        int len2 = loader.SineWaveFloor[0].length;
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len2; j++) {
                SineWaveFloor[i][j] = SINE_WAVE_FLOOR.copy(loader.SineWaveFloor[i][j]);
            }
        }

        len = loader.SineWall.length;
        len2 = loader.SineWall[0].length;
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len2; j++) {
                SineWall[i][j] = SINE_WALL.copy(loader.SineWall[i][j]);
            }
        }

        for (int i = 0; i < loader.SpringBoard.length; i++) {
            SpringBoard[i] = Spring_Board.copy(loader.SpringBoard[i]);
        }
    }

    private static void LoadTracks() {
        for (int i = 0; i < loader.Track.length; i++) {
            Track[i] = new TRACK();
            Track[i].copy(loader.Track[i]);
        }
    }

    private static void LoadAnims() {
        AnimCnt = loader.AnimCnt;
        for (int i = 0; i < AnimCnt; i++) {
            pAnim[i].copy(loader.pAnim[i]);
        }
    }

    private static void LoadMirrors() {
        mirrorcnt = loader.mirrorcnt;
        mirrorinview = loader.mirrorinview;
        for (int i = 0; i < loader.mirror.length; i++) {
            mirror[i] = new MirrorType();
            mirror[i].copy(loader.mirror[i]);
        }
    }

    private static void LoadQueues() {
        StarQueueHead = loader.StarQueueHead;
        System.arraycopy(loader.StarQueue.items, 0, StarQueue, 0, loader.StarQueue.size);

        HoleQueueHead = loader.HoleQueueHead;
        System.arraycopy(loader.HoleQueue.items, 0, HoleQueue, 0, loader.HoleQueue.size);

        WallBloodQueueHead = loader.WallBloodQueueHead;
        System.arraycopy(loader.WallBloodQueue.items, 0, WallBloodQueue, 0, loader.WallBloodQueue.size);

        FloorBloodQueueHead = loader.FloorBloodQueueHead;
        System.arraycopy(loader.FloorBloodQueue.items, 0, FloorBloodQueue, 0, loader.FloorBloodQueue.size);

        GenericQueueHead = loader.GenericQueueHead;
        System.arraycopy(loader.GenericQueue.items, 0, GenericQueue, 0, loader.GenericQueue.size);

        LoWangsQueueHead = loader.LoWangsQueueHead;
        System.arraycopy(loader.LoWangsQueue.items, 0, LoWangsQueue, 0, loader.LoWangsQueue.size);

        Arrays.fill(HoleQueue, -1);
        int head = HoleQueueHead;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_HOLE_QUEUE); node != null; node = node.getNext()) {
            HoleQueue[head] = node.getIndex();
            head = ((head + 1) & (MAX_HOLE_QUEUE - 1));
        }

        Arrays.fill(WallBloodQueue, -1);
        head = WallBloodQueueHead;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_WALLBLOOD_QUEUE); node != null; node = node.getNext()) {
            WallBloodQueue[head] = node.getIndex();
            head = ((head + 1) & (MAX_WALLBLOOD_QUEUE - 1));
        }

        Arrays.fill(GenericQueue, -1);
        head = GenericQueueHead;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_GENERIC_QUEUE); node != null; node = node.getNext()) {
            GenericQueue[head] = node.getIndex();
            head = ((head + 1) & (MAX_GENERIC_QUEUE - 1));
        }

        Arrays.fill(FloorBloodQueue, -1);
        head = FloorBloodQueueHead;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FLOORBLOOD_QUEUE); node != null; node = node.getNext()) {
            FloorBloodQueue[head] = node.getIndex();
            head = ((head + 1) & (MAX_FLOORBLOOD_QUEUE - 1));
        }
    }

    private static void LoadStuff() {
        PlayClock = loader.PlayClock;
        Kills = loader.Kills;
        TotalKillable = loader.TotalKillable;

        gNet.KillLimit = loader.TotalKillable;
        gNet.TimeLimit = loader.TimeLimit;
        gNet.TimeLimitClock = loader.TimeLimitClock;
        gNet.MultiGameType = loader.MultiGameType;
        gNet.TeamPlay = loader.TeamPlay;
        gNet.HurtTeammate = loader.HurtTeammate;
        gNet.SpawnMarkers = loader.SpawnMarkers;
        gNet.NoRespawn = loader.NoRespawn;
        gNet.Nuke = loader.Nuke;

        for (int i = 0; i < MAXTILES; i++) {
            engine.getTile(i).setFlags(loader.picanm[i]);
        }
        LevelSecrets = loader.LevelSecrets;

        show2dsector.copy(loader.show2dsector);
        show2dwall.copy(loader.show2dwall);
        show2dsprite.copy(loader.show2dsprite);

        Bunny_Count = loader.Bunny_Count;
        GodMode = loader.GodMode;
        serpwasseen = loader.serpwasseen;
        sumowasseen = loader.sumowasseen;
        zillawasseen = loader.zillawasseen;

        System.arraycopy(loader.BossSpriteNum.items, 0, BossSpriteNum, 0, loader.BossSpriteNum.size);
    }

    private static void writeBytes(OutputStream os, short[] array) throws IOException {
        for (short val : array) {
            StreamUtils.writeShort(os, val);
        }
    }

    private static void writeBytes(OutputStream os, int[] array, int len) throws IOException {
        StreamUtils.writeInt(os, len);
        for (int i = 0; i < len; i++) {
            StreamUtils.writeInt(os, array[i]);
        }
    }

}
