package ru.m210projects.Wang;

import org.jetbrains.annotations.Nullable;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.Tools.Interpolation;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;
import ru.m210projects.Wang.Type.Anim.AnimCallback;
import ru.m210projects.Wang.Type.Anim.AnimType;

import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Break.SetupWallForBreak;
import static ru.m210projects.Wang.CopySect.CopySectorMatch;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Factory.WangNetwork.Prediction;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JSector.UnlockKeyLock;
import static ru.m210projects.Wang.JTags.SWITCH_LOCKED;
import static ru.m210projects.Wang.Light.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.MiscActr.*;
import static ru.m210projects.Wang.Morth.DoSOevent;
import static ru.m210projects.Wang.Morth.DoSectorObjectSetScale;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Panel.PlayerUpdateAmmo;
import static ru.m210projects.Wang.Panel.PlayerUpdateHealth;
import static ru.m210projects.Wang.Player.*;
import static ru.m210projects.Wang.Quake.DoQuakeMatch;
import static ru.m210projects.Wang.Rooms.*;
import static ru.m210projects.Wang.Rotator.*;
import static ru.m210projects.Wang.Shrap.SpawnShrap;
import static ru.m210projects.Wang.Slidor.*;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Spike.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Stag.*;
import static ru.m210projects.Wang.Tags.*;
import static ru.m210projects.Wang.Text.PutStringInfo;
import static ru.m210projects.Wang.Track.*;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Vator.*;
import static ru.m210projects.Wang.WallMove.DoWallMoveMatch;
import static ru.m210projects.Wang.Weapon.*;
import static ru.m210projects.Wang.Weapons.Star.InitWeaponStar;

public class Sector {

    public static final int NTAG_SEARCH_LO_HI = 3;

    public static final int MAX_SINE_WAVE = 6;
    public static final int MAX_SINE_WALL = 10;
    public static final int MAX_SINE_WALL_POINTS = 64;
    public static final int SO_SCALE_NONE = 0;
    public static final int SO_SCALE_HOLD = 1;
    public static final int SO_SCALE_DEST = 2;
    public static final int SO_SCALE_RANDOM = 3;
    public static final int SO_SCALE_CYCLE = 4;
    public static final int SO_SCALE_RANDOM_POINT = 5;
    public static final int SCALE_POINT_SPEED = (4 + RANDOM_RANGE(8));
    public static final int SWITCH_LEVER = 581;
    public static final int SWITCH_FUSE = 558;
    public static final int SWITCH_FLIP = 561;
    public static final int SWITCH_RED_CHAIN = 563;
    public static final int SWITCH_GREEN_CHAIN = 565;
    public static final int SWITCH_TOUCH = 567;
    public static final int SWITCH_DRAGON = 569;
    public static final int SWITCH_LIGHT = 551;
    public static final int SWITCH_1 = 575;
    public static final int SWITCH_3 = 579;
    public static final int SWITCH_SHOOTABLE_1 = 577;
    public static final int SWITCH_4 = 571;
    public static final int SWITCH_5 = 573;
    public static final int SWITCH_6 = 583;
    public static final int EXIT_SWITCH = 2470;
    public static final int SWITCH_SKULL = 553;
    public static final int SINE_FLOOR = 1;
    public static final int SINE_CEILING = (1 << 1);
    public static final int SINE_SLOPED = BIT(3);
    // Needed in order to see if Player should grunt if he can't find a wall to
    // operate on
    // If player is too far away, don't grunt
    public static final int PLAYER_SOUNDEVENT_TAG = 900;
    private static final short[] StatList = {STAT_DEFAULT, STAT_VATOR, STAT_SPIKE, STAT_TRAP, STAT_ITEM, STAT_LIGHTING,
            STAT_STATIC_FIRE, STAT_AMBIENT, STAT_FAF};
    public static int LevelSecrets;
    public static PlayerStr GlobPlayerStr;
    private static final Sect_User[] SectUser = new Sect_User[MAXSECTORS];
    public static final Anim[] pAnim = new Anim[MAXANIM];
    public static int AnimCnt = 0;
    public static final Animator DoSpawnSpot = new Animator((Animator.Runnable) Sector::DoSpawnSpot);
    public static final SINE_WAVE_FLOOR[][] SineWaveFloor = new SINE_WAVE_FLOOR[MAX_SINE_WAVE][21];
    public static final SINE_WALL[][] SineWall = new SINE_WALL[MAX_SINE_WALL][MAX_SINE_WALL_POINTS];
    public static final Spring_Board[] SpringBoard = new Spring_Board[20];
    public static int x_min_bound, y_min_bound, x_max_bound, y_max_bound;
    private static final Vector3i midPoint = new Vector3i();
    private static final int[] snd = new int[3];
    private static int nti_cnt;
    private static final NEAR_TAG_INFO[] nti = new NEAR_TAG_INFO[16];
    private static final int[] z = new int[3];

    public static void InitSectorStructs() {
        for (int i = 0; i < MAX_SECTOR_OBJECTS; i++) {
            SectorObject[i] = new Sector_Object();
        }

        for (int i = 0; i < 20; i++) {
            SpringBoard[i] = new Spring_Board();
        }

        for (int i = 0; i < MAX_SINE_WALL; i++) {
            for (int j = 0; j < MAX_SINE_WALL_POINTS; j++) {
                SineWall[i][j] = new SINE_WALL();
            }
        }

        for (int i = 0; i < MAX_SINE_WAVE; i++) {
            for (int j = 0; j < 21; j++) {
                SineWaveFloor[i][j] = new SINE_WAVE_FLOOR();
            }
        }
        for (int i = 0; i < 16; i++) {
            nti[i] = new NEAR_TAG_INFO();
        }
    }

    public static void SetSectorWallBits(ru.m210projects.Build.Types.Sector sec, int bit_mask, boolean set_sectwall, boolean set_nextwall) {
        Wall[] walls = boardService.getBoard().getWalls();
        int wall_num = sec.getWallptr();
        int start_wall = wall_num;

        do {
            Wall w = walls[wall_num];
            if (set_sectwall) {
                w.setExtra(w.getExtra() | bit_mask);
            }

            if (set_nextwall && w.getNextwall() >= 0) {
                Wall nw = walls[w.getNextwall()];
                nw.setExtra(nw.getExtra() | bit_mask);
            }

            wall_num = w.getPoint2();
        } while (wall_num != start_wall);
    }

    public static void WallSetupDontMove() {
        Wall[] walls = boardService.getBoard().getWalls();
        for (ListNode<Sprite> n = boardService.getStatNode(STAT_WALL_DONT_MOVE_UPPER); n != null; n = n.getNext()) {
            Sprite spu = n.get();
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_WALL_DONT_MOVE_LOWER); node != null; node = node.getNext()) {
                Sprite spl = node.get();
                if (spu.getLotag() == spl.getLotag()) {
                    for (Wall wal : walls) {
                        if (wal.getX() < spl.getX() && wal.getX() > spu.getX() && wal.getY() < spl.getY() && wal.getY() > spu.getY()) {
                            wal.setExtra(wal.getExtra() | WALLFX_DONT_MOVE);
                        }
                    }
                }
            }
        }
    }

    public static void WallSetup() {
        int NextSineWall = 0;
        WallSetupDontMove();

        for (int a = 0; a < MAX_SINE_WALL; a++) {
            for (int b = 0; b < MAX_SINE_WALL_POINTS; b++) {
                SineWall[a][b].reset();
            }
        }

        x_min_bound = 999999;
        y_min_bound = 999999;
        x_max_bound = -999999;
        y_max_bound = -999999;
        Wall[] walls = boardService.getBoard().getWalls();
        for (int i = 0; i < walls.length; i++) {
            Wall wp = walls[i];
            if (wp.getPicnum() == FAF_PLACE_MIRROR_PIC) {
                wp.setPicnum(FAF_MIRROR_PIC);
            }

            if (wp.getPicnum() == FAF_PLACE_MIRROR_PIC + 1) {
                wp.setPicnum(FAF_MIRROR_PIC + 1);
            }

            // get map min and max coordinates
            x_min_bound = Math.min(wp.getX(), x_min_bound);
            y_min_bound = Math.min(wp.getY(), y_min_bound);
            x_max_bound = Math.max(wp.getX(), x_max_bound);
            y_max_bound = Math.max(wp.getY(), y_max_bound);

            // this overwrites the lotag so it needs to be called LAST - its down there
            // SetupWallForBreak(wp);

            switch (wp.getLotag()) {
                case TAG_WALL_LOOP_DONT_SPIN: {
                    int wall_num, start_wall;

                    // set first wall
                    wp.setExtra(wp.getExtra() | WALLFX_LOOP_DONT_SPIN);
                    Wall nw = walls[wp.getNextwall()];
                    nw.setExtra(nw.getExtra() | WALLFX_LOOP_DONT_SPIN);

                    // move the the next wall
                    start_wall = wp.getPoint2();

                    // Travel all the way around loop setting wall bits
                    for (wall_num = start_wall; walls[wall_num].getLotag() != TAG_WALL_LOOP_DONT_SPIN; wall_num = walls[wall_num].getPoint2()) {
                        Wall w = walls[wall_num];
                        w.setExtra(w.getExtra() | WALLFX_LOOP_DONT_SPIN);
                        if (w.getNextwall() != -1) {
                            Wall w2 = walls[w.getNextwall()];
                            w2.setExtra(w2.getExtra() | WALLFX_LOOP_DONT_SPIN);
                        }
                    }

                    break;
                }

                case TAG_WALL_LOOP_DONT_SCALE: {
                    int wall_num, start_wall;

                    // set first wall
                    wp.setExtra(wp.getExtra() | (WALLFX_DONT_SCALE));
                    Wall nw = walls[wp.getNextwall()];
                    nw.setExtra(nw.getExtra() | (WALLFX_DONT_SCALE));

                    // move the the next wall
                    start_wall = wp.getPoint2();

                    // Travel all the way around loop setting wall bits
                    for (wall_num = start_wall; walls[wall_num].getLotag() != TAG_WALL_LOOP_DONT_SCALE; wall_num = walls[wall_num].getPoint2()) {
                        Wall w = walls[wall_num];
                        w.setExtra(w.getExtra() | (WALLFX_DONT_SCALE));
                        if (w.getNextwall() >= 0) {
                            Wall w2 = walls[w.getNextwall()];
                            w2.setExtra(w2.getExtra() | (WALLFX_DONT_SCALE));
                        }
                    }

                    wp.setLotag(0);

                    break;
                }

                case TAG_WALL_LOOP_OUTER_SECONDARY: {
                    int wall_num, start_wall;
                    Wall nw = walls[wp.getNextwall()];

                    // set first wall
                    wp.setExtra(wp.getExtra() | (WALLFX_LOOP_OUTER | WALLFX_LOOP_OUTER_SECONDARY));
                    nw.setExtra(nw.getExtra() | (WALLFX_LOOP_OUTER | WALLFX_LOOP_OUTER_SECONDARY));

                    // move the the next wall
                    start_wall = wp.getPoint2();

                    // Travel all the way around loop setting wall bits
                    for (wall_num = start_wall; walls[wall_num].getLotag() != TAG_WALL_LOOP_OUTER_SECONDARY; wall_num = walls[wall_num].getPoint2()) {
                        Wall w = walls[wall_num];
                        w.setExtra(w.getExtra() | (WALLFX_LOOP_OUTER | WALLFX_LOOP_OUTER_SECONDARY));
                        if (w.getNextwall() >= 0) {
                            Wall w2 = walls[w.getNextwall()];
                            w2.setExtra(w2.getExtra() | (WALLFX_LOOP_OUTER | WALLFX_LOOP_OUTER_SECONDARY));
                        }
                    }

                    break;
                }

                case TAG_WALL_LOOP_OUTER: {
                    int wall_num, start_wall;
                    Wall nw = walls[wp.getNextwall()];

                    // set first wall
                    wp.setExtra(wp.getExtra() | (WALLFX_LOOP_OUTER));
                    if (wp.getNextwall() != -1) {
                        nw.setExtra(nw.getExtra() | (WALLFX_LOOP_OUTER));
                    }

                    // move the the next wall
                    start_wall = wp.getPoint2();

                    // Travel all the way around loop setting wall bits
                    for (wall_num = start_wall; walls[wall_num].getLotag() != TAG_WALL_LOOP_OUTER; wall_num = walls[wall_num].getPoint2()) {
                        Wall w = walls[wall_num];
                        w.setExtra(w.getExtra() | (WALLFX_LOOP_OUTER));
                        if (w.getNextwall() != -1) {
                            Wall w2 = walls[w.getNextwall()];
                            w2.setExtra(w2.getExtra() | (WALLFX_LOOP_OUTER));
                        }
                    }

                    wp.setLotag(0);

                    break;
                }

                case TAG_WALL_DONT_MOVE: {
                    // set first wall
                    wp.setExtra(wp.getExtra() | (WALLFX_DONT_MOVE));
                    break;
                }

                case TAG_WALL_LOOP_SPIN_2X: {
                    int wall_num, start_wall;
                    Wall nw = walls[wp.getNextwall()];

                    // set first wall
                    wp.setExtra(wp.getExtra() | (WALLFX_LOOP_SPIN_2X));
                    nw.setExtra(nw.getExtra() | (WALLFX_LOOP_SPIN_2X));

                    // move the the next wall
                    start_wall = wp.getPoint2();

                    // Travel all the way around loop setting wall bits
                    for (wall_num = start_wall; walls[wall_num].getLotag() != TAG_WALL_LOOP_SPIN_2X; wall_num = walls[wall_num].getPoint2()) {
                        Wall w = walls[wall_num];
                        w.setExtra(w.getExtra() | (WALLFX_LOOP_SPIN_2X));
                        if (w.getNextwall() != -1) {
                            Wall w2 = walls[w.getNextwall()];
                            w2.setExtra(w2.getExtra() | (WALLFX_LOOP_SPIN_2X));
                        }
                    }

                    break;
                }

                case TAG_WALL_LOOP_SPIN_4X: {
                    int wall_num, start_wall;
                    Wall nw = walls[wp.getNextwall()];

                    // set first wall
                    wp.setExtra(wp.getExtra() | (WALLFX_LOOP_SPIN_4X));
                    nw.setExtra(nw.getExtra() | (WALLFX_LOOP_SPIN_4X));

                    // move the the next wall
                    start_wall = wp.getPoint2();

                    // Travel all the way around loop setting wall bits
                    for (wall_num = start_wall; walls[wall_num].getLotag() != TAG_WALL_LOOP_SPIN_4X; wall_num = walls[wall_num].getPoint2()) {
                        Wall w = walls[wall_num];
                        w.setExtra(w.getExtra() | (WALLFX_LOOP_SPIN_4X));
                        if (w.getNextwall() != -1) {
                            Wall w2 = walls[w.getNextwall()];
                            w2.setExtra(w2.getExtra() | (WALLFX_LOOP_SPIN_4X));
                        }
                    }

                    break;
                }

                case TAG_WALL_LOOP_REVERSE_SPIN: {
                    int wall_num, start_wall;
                    Wall nw = walls[wp.getNextwall()];

                    // set first wall
                    wp.setExtra(wp.getExtra() | (WALLFX_LOOP_REVERSE_SPIN));
                    nw.setExtra(nw.getExtra() | (WALLFX_LOOP_REVERSE_SPIN));

                    // move the the next wall
                    start_wall = wp.getPoint2();

                    // Travel all the way around loop setting wall bits
                    for (wall_num = start_wall; walls[wall_num].getLotag() != TAG_WALL_LOOP_REVERSE_SPIN; wall_num = walls[wall_num].getPoint2()) {
                        Wall w = walls[wall_num];
                        w.setExtra(w.getExtra() | (WALLFX_LOOP_REVERSE_SPIN));
                        if (w.getNextwall() != -1) {
                            Wall w2 = walls[w.getNextwall()];
                            w2.setExtra(w2.getExtra() | (WALLFX_LOOP_REVERSE_SPIN));
                        }
                    }

                    break;
                }

                case TAG_WALL_SINE_Y_BEGIN:
                case TAG_WALL_SINE_X_BEGIN: {
                    int wall_num, cnt, num_points, type, tag_end;
                    SINE_WALL sw;
                    int range = 250, speed = 3, peak = 0;

                    tag_end =  (wp.getLotag() + 2);

                    type =  (wp.getLotag() - TAG_WALL_SINE_Y_BEGIN);

                    // count up num_points
                    for (wall_num = i, num_points = 0; num_points < MAX_SINE_WALL_POINTS
                            && walls[wall_num].getLotag() != tag_end; wall_num = walls[wall_num].getPoint2(), num_points++) {
                        Wall w = walls[wall_num];
                        if (num_points == 0) {
                            if (w.getHitag() != 0) {
                                range = w.getHitag();
                            }
                        } else if (num_points == 1) {
                            if (w.getHitag() != 0) {
                                speed = w.getHitag();
                            }
                        } else if (num_points == 2) {
                            if (w.getHitag() != 0) {
                                peak = w.getHitag();
                            }
                        }
                    }

                    if (peak != 0) {
                        num_points = peak;
                    }

                    for (wall_num = i, cnt = 0; cnt < MAX_SINE_WALL_POINTS
                            && walls[wall_num].getLotag() != tag_end; wall_num = walls[wall_num].getPoint2(), cnt++) {
                        // set the first on up
                        sw = SineWall[NextSineWall][cnt];
                        Wall w = walls[wall_num];

                        sw.type = type;
                        sw.wall = wall_num;
                        sw.speed_shift = speed;
                        sw.range = range;

                        // don't allow bullet holes/stars
                        w.setExtra(w.getExtra() | (WALLFX_DONT_STICK));

                        if (sw.type == 0) {
                            sw.orig_xy = w.getY() - (sw.range >> 2);
                        } else {
                            sw.orig_xy = w.getX() - (sw.range >> 2);
                        }

                        sw.sintable_ndx =  (cnt * (2048 / num_points));
                    }

                    NextSineWall++;

                }
            }

            // this overwrites the lotag so it needs to be called LAST
            SetupWallForBreak(wp);
        }
    }

    public static void SectorLiquidSet(int i) {
        // ///////////////////////////////////
        //
        // CHECK for pics that mean something
        //
        // ///////////////////////////////////
        ru.m210projects.Build.Types.Sector sec = boardService.getSector(i);
        if (sec == null) {
            return;
        }

        if (sec.getFloorpicnum() >= 300 && sec.getFloorpicnum() <= 307) {

            sec.setExtra(sec.getExtra() | (SECTFX_LIQUID_WATER));
        } else if (sec.getFloorpicnum() >= 320 && sec.getFloorpicnum() <= 343) {

            sec.setExtra(sec.getExtra() | (SECTFX_LIQUID_WATER));
        } else if (sec.getFloorpicnum() >= 780 && sec.getFloorpicnum() <= 794) {

            sec.setExtra(sec.getExtra() | (SECTFX_LIQUID_WATER));
        } else if (sec.getFloorpicnum() >= 890 && sec.getFloorpicnum() <= 897) {
            sec.setExtra(sec.getExtra() | (SECTFX_LIQUID_WATER));
        } else if (sec.getFloorpicnum() >= 175 && sec.getFloorpicnum() <= 182) {
            Sect_User sectu = SetupSectUser(i);
            sec.setExtra(sec.getExtra() | (SECTFX_LIQUID_LAVA));
            if (sectu.damage == 0) {
                sectu.damage = 40;
            }
        }
    }

    public static boolean SectorSetup() {
        int NextSineWave = 0;

        WallSetup();

        for (int ndx = 0; ndx < MAX_SECTOR_OBJECTS; ndx++) {
            SectorObject[ndx].reset();
            // 0 pointers

            SectorObject[ndx].xmid = MAXLONG;
        }

        for (int a = 0; a < MAX_SINE_WAVE; a++) {
            for (int b = 0; b < 21; b++) {
                SineWaveFloor[a][b].reset();
            }
        }

        for (int a = 0; a < 20; a++) {
            SpringBoard[a].reset();
        }

        LevelSecrets = 0;

        ru.m210projects.Build.Types.Sector[] sectors = boardService.getBoard().getSectors();
        for (int i = 0; i < sectors.length; i++) {
            ru.m210projects.Build.Types.Sector sec = sectors[i];
            int tag = sec.getLotag();

            // Stupid Redux's fixes
            if (Level == 14 && swGetAddon() == 2 && i == 750
                    && tag == 216) {
                sec.setLotag(116);
            }

            // ///////////////////////////////////
            //
            // CHECK for pics that mean something
            //
            // ///////////////////////////////////

            // ///////////////////////////////////
            //
            // CHECK for flags
            //
            // ///////////////////////////////////

            if (TEST(sec.getExtra(), SECTFX_SINK)) {
                SectorLiquidSet(i);
            }

            if (TEST(sec.getFloorstat(), FLOOR_STAT_PLAX)) {
                // don't do a z adjust for FAF area
                if (sec.getFloorpicnum() != FAF_PLACE_MIRROR_PIC) {
                    sec.setExtra(sec.getExtra() | (SECTFX_Z_ADJUST));
                }
            }

            if (TEST(sec.getCeilingstat(), CEILING_STAT_PLAX)) {
                // don't do a z adjust for FAF area
                if (sec.getCeilingpicnum() != FAF_PLACE_MIRROR_PIC) {
                    sec.setExtra(sec.getExtra() | (SECTFX_Z_ADJUST));
                }
            }

            // ///////////////////////////////////
            //
            // CHECK for sector/sprite objects
            //
            // ///////////////////////////////////

            if (tag >= TAG_OBJECT_CENTER && tag < TAG_OBJECT_CENTER + 100) {
                if (!SetupSectorObject(i, tag)) {
                    return false;
                }
            }

            // ///////////////////////////////////
            //
            // CHECK lo and hi tags
            //
            // ///////////////////////////////////

            switch (tag) {
                case TAG_SECRET_AREA_TRIGGER:
                    LevelSecrets++;
                    break;

                case TAG_DOOR_SLIDING:
                    SetSectorWallBits(sec, WALLFX_DONT_STICK, true, true);
                    break;

                case TAG_SINE_WAVE_FLOOR:
                case TAG_SINE_WAVE_CEILING:
                case TAG_SINE_WAVE_BOTH: {
                    SINE_WAVE_FLOOR swf;
                    int near_sect = i, base_sect = i;
                    int swf_ndx = 0;
                    int cnt, sector_cnt;
                    int range;
                    int range_diff = 0;
                    int wave_diff = 0;
                    int peak_dist = 0;
                    int speed_shift = 3;
                    int num;

                    num =  ((tag - TAG_SINE_WAVE_FLOOR) / 20);

                    // set the first on up
                    swf = SineWaveFloor[NextSineWave][swf_ndx];

                    swf.flags = 0;

                    switch (num) {
                        case 0:
                            swf.flags |= (SINE_FLOOR);

                            if (TEST(sectors[base_sect].getFloorstat(), FLOOR_STAT_SLOPE)) {
                                swf.flags |= (SINE_SLOPED);
                            }
                            break;
                        case 1:
                            swf.flags |= (SINE_CEILING);
                            break;
                        case 2:
                            swf.flags |= (SINE_FLOOR | SINE_CEILING);
                            break;
                    }

                    swf.sector = near_sect;
                    ru.m210projects.Build.Types.Sector s1 = sectors[swf.sector];

                    swf.range = range = Z(s1.getHitag());
                    swf.floor_origz = s1.getFloorz() - (range >> 2);
                    swf.ceiling_origz = s1.getCeilingz() - (range >> 2);

                    // look for the rest by distance
                    for (swf_ndx = 1, sector_cnt = 1; true; swf_ndx++) {
                        near_sect = FindNextSectorByTag(sectors[base_sect], tag + swf_ndx);
                        ru.m210projects.Build.Types.Sector ns = boardService.getSector(near_sect);
                        if (ns != null) {
                            swf = SineWaveFloor[NextSineWave][swf_ndx];

                            if (swf_ndx == 1 && ns.getHitag() != 0) {
                                range_diff = ns.getHitag();
                            } else if (swf_ndx == 2 && ns.getHitag() != 0) {
                                speed_shift = ns.getHitag();
                            } else if (swf_ndx == 3 && ns.getHitag() != 0) {
                                peak_dist = ns.getHitag();
                            }

                            swf.sector = near_sect;
                            swf.floor_origz = ns.getFloorz() - (range >> 2);
                            swf.ceiling_origz = ns.getCeilingz() - (range >> 2);
                            range -= range_diff;
                            swf.range = range;

                            base_sect = swf.sector;
                            sector_cnt++;
                        } else {
                            break;
                        }
                    }

                    // more than 6 waves and something in high tag - set up wave
                    // dissapate
                    if (sector_cnt > 8 && sectors[base_sect].getHitag() != 0) {
                        wave_diff = sectors[base_sect].getHitag();
                    }

                    // setup the sintable_ndx based on the actual number of
                    // sectors (swf_ndx)
//	                for (swf = SineWaveFloor[NextSineWave][0], cnt = 0; swf.sector >= 0 && cnt < SineWaveFloor.length; swf++, cnt++) XXX
                    for (cnt = 0; swf.sector >= 0 && cnt < SineWaveFloor[NextSineWave].length; cnt++) {
                        swf = SineWaveFloor[NextSineWave][cnt];
                        if (peak_dist != 0) {
                            swf.sintable_ndx =  (cnt * (2048 / peak_dist));
                        } else {
                            swf.sintable_ndx =  (cnt * (2048 / swf_ndx));
                        }

                        swf.speed_shift = speed_shift;
                    }

                    // set up the a real wave that dissapates at the end
                    if (wave_diff != 0) {
                        for (cnt =  (sector_cnt - 1); cnt >= 0; cnt--) {
                            // only do the last (actually the first) few for the
                            // dissapate
                            if (cnt > 8) {
                                continue;
                            }

                            swf = SineWaveFloor[NextSineWave][cnt];

                            swf.range -= wave_diff;

                            wave_diff += wave_diff;

                            if (swf.range < Z(4)) {
                                swf.range = Z(4);
                            }

                            ru.m210projects.Build.Types.Sector s = boardService.getSector(swf.sector);
                            if (s != null) {
                                // reset origz's based on new range
                                swf.floor_origz = s.getFloorz() - (swf.range >> 2);
                                swf.ceiling_origz = s.getCeilingz() - (swf.range >> 2);
                            }
                        }
                    }

                    NextSineWave++;

                    break;
                }
            }
        }

        return true;
    }

    public static Vector3i SectorMidPoint(int sectnum) {
        ru.m210projects.Build.Types.Sector sec = boardService.getSector(sectnum);
        if (sec != null) {
            int startwall = sec.getWallptr();
            int endwall = (startwall + sec.getWallnum() - 1);
            int xsum = 0, ysum = 0;
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wall = wn.get();
                xsum += wall.getX();
                ysum += wall.getY();
            }

            midPoint.x = xsum / (endwall - startwall + 1);
            midPoint.y = ysum / (endwall - startwall + 1);
            midPoint.z = DIV2(sec.getFloorz() + sec.getCeilingz());
        }
        return midPoint;
    }

    public static void DoSpringBoard(PlayerStr pp) {
        ru.m210projects.Build.Types.Sector sec = boardService.getSector(pp.cursectnum);
        if (sec != null) {
            pp.jump_speed = -sec.getHitag();
            DoPlayerBeginForceJump(pp);
        }
    }

    public static void DoSpringBoardDown() {
        for (Spring_Board sbp : SpringBoard) {
            ru.m210projects.Build.Types.Sector sec = boardService.getSector(sbp.Sector);
            // if empty set up an entry to close the sb later
            if (sec != null) {
                if ((sbp.TimeOut -= synctics) <= 0) {
                    int nextsec = engine.nextsectorneighborz(sbp.Sector, sec.getFloorz(), SEARCH_FLOOR, SEARCH_DOWN);
                    ru.m210projects.Build.Types.Sector nsec = boardService.getSector(nextsec);
                    if (nsec != null) {
                        int destz = nsec.getFloorz();
                        AnimSet(sbp.Sector, destz, 256, AnimType.FloorZ);
                        sec.setLotag(TAG_SPRING_BOARD);
                    }
                    sbp.Sector = -1;
                }
            }
        }
    }

    public static int FindNextSectorByTag(ru.m210projects.Build.Types.Sector sec, int tag) {
        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall wall = wn.get();
            int next_sectnum = wall.getNextsector();
            ru.m210projects.Build.Types.Sector nsec = boardService.getSector(next_sectnum);
            if (nsec != null) {
                if (nsec.getLotag() == tag) {
                    return next_sectnum;
                }
            }
        }
        return (-1);
    }

    public static void DoSpawnActorTrigger(int match) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPAWN_TRIGGER); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            if (sp.getHitag() == match) {
                if (ActorSpawn(node.getIndex())) {
                    DoSpawnTeleporterEffectPlace(sp);
                    PlaySound(DIGI_PLAYER_TELEPORT, sp, v3df_none);
                }
            }
        }
    }

    public static boolean OperateSector(int sectnum, boolean player_is_operating) {
        PlayerStr pp = GlobPlayerStr;

        // Don't let actors operate locked or secret doors
        if (!player_is_operating) {
            Sprite fsp;
            Sect_User su = getSectUser(sectnum);

            if (su != null && su.stag == SECT_LOCK_DOOR) {
                return (false);
            }

            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = nexti) {
                nexti = node.getNext();
                fsp = node.get();
                Sect_User su1 = getSectUser(fsp.getSectnum());

                if (su1 != null && su1.stag == SECT_LOCK_DOOR) {
                    return (false);
                }

                if (fsp.getStatnum() == STAT_VATOR && SP_TAG1(fsp) == SECT_VATOR && TEST_BOOL7(fsp)) {
                    return (false);
                }
                if (fsp.getStatnum() == STAT_ROTATOR && SP_TAG1(fsp) == SECT_ROTATOR && TEST_BOOL7(fsp)) {
                    return (false);
                }
                if (fsp.getStatnum() == STAT_SLIDOR && SP_TAG1(fsp) == SECT_SLIDOR && TEST_BOOL7(fsp)) {
                    return (false);
                }

            }
        }

        switch (LOW_TAG(sectnum)) {

            case TAG_VATOR:
                DoVatorOperate(pp, sectnum);
                return (true);

            case TAG_ROTATOR:
                DoRotatorOperate(pp, sectnum);
                return (true);

            case TAG_SLIDOR:
                DoSlidorOperate(pp, sectnum);
                return (true);
        }

        return (false);
    }

    public static int AnimateSwitch(Sprite sp, int tgt_value) {

        // if the value is not ON or OFF
        // then it is a straight toggle

        switch (sp.getPicnum()) {
            // set to TRUE/ON
            case SWITCH_SKULL:
            case SWITCH_LEVER:
            case SWITCH_LIGHT:
            case SWITCH_SHOOTABLE_1:
            case SWITCH_1:
            case SWITCH_3:
            case SWITCH_FLIP:
            case SWITCH_RED_CHAIN:
            case SWITCH_GREEN_CHAIN:
            case SWITCH_TOUCH:
            case SWITCH_DRAGON:
            case SWITCH_4:
            case SWITCH_5:
            case SWITCH_6:
            case EXIT_SWITCH:

                // dont toggle - return the current state
                if (tgt_value == 999) {
                    return (OFF);
                }

                sp.setPicnum(sp.getPicnum() + 1);

                // if the tgt_value should be true
                // flip it again - recursive but only once
                if (tgt_value == OFF) {
                    AnimateSwitch(sp, tgt_value);
                    return (OFF);
                }

                return (ON);

            // set to true
            case SWITCH_SKULL + 1:
            case SWITCH_LEVER + 1:
            case SWITCH_LIGHT + 1:
            case SWITCH_1 + 1:
            case SWITCH_3 + 1:
            case SWITCH_FLIP + 1:
            case SWITCH_RED_CHAIN + 1:
            case SWITCH_GREEN_CHAIN + 1:
            case SWITCH_TOUCH + 1:
            case SWITCH_DRAGON + 1:
            case SWITCH_SHOOTABLE_1 + 1:
            case SWITCH_4 + 1:
            case SWITCH_5 + 1:
            case SWITCH_6 + 1:
            case EXIT_SWITCH + 1:

                // dont toggle - return the current state
                if (tgt_value == 999) {
                    return (ON);
                }

                sp.setPicnum(sp.getPicnum() - 1);

                if (tgt_value == ON) {
                    AnimateSwitch(sp, tgt_value);
                    return (ON);
                }

                return (OFF);
        }
        return (OFF);
    }

    public static void SectorExp(int SpriteNum, int sectnum, int orig_ang, int zh) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_WALL | CSTAT_SPRITE_FLOOR));
        Vector3i p = SectorMidPoint(sectnum);
        int x = p.x;
        int y = p.y;
        int z = p.z;

        sp.setAng( orig_ang);
        sp.setX(x);
        sp.setY(y);
        sp.setZ(z);

        // randomize the explosions
        sp.setAng(sp.getAng() + RANDOM_P2(256) - 128);
        sp.setX(sp.getX() + RANDOM_P2(1024) - 512);
        sp.setY(sp.getY() + RANDOM_P2(1024) - 512);
        sp.setZ(zh);

        // setup vars needed by SectorExp
        engine.changespritesect(SpriteNum, sectnum);
        engine.getzsofslope(sp.getSectnum(), sp.getX(), sp.getY(), fz, cz);

        USER u = getUser(SpriteNum);
        if (u != null) {
            u.hiz = cz.get();
            u.loz = fz.get();
        }

        // spawn explosion
        int explosion =  SpawnSectorExp(SpriteNum);

        Sprite exp = boardService.getSprite(explosion);
        USER eu = getUser(explosion);
        if (exp == null || eu == null) {
            return;
        }

        exp.setXrepeat(exp.getXrepeat() + (RANDOM_P2(32 << 8) >> 8) - 16);
        exp.setYrepeat(exp.getYrepeat() + (RANDOM_P2(32 << 8) >> 8) - 16);
        eu.xchange = MOVEx(92, exp.getAng());
        eu.ychange = MOVEy(92, exp.getAng());
    }

    public static void DoExplodeSector(int match) {
        int orig_ang = 0;// sp.ang;

        ListNode<Sprite> nextcf;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_EXPLODING_CEIL_FLOOR); node != null; node = nextcf) {
            int cf = node.getIndex();
            nextcf = node.getNext();
            Sprite esp = node.get();

            if (match != esp.getLotag()) {
                continue;
            }

            if (getUser(cf) == null) {
                SpawnUser(cf, 0, null);
            }

            ru.m210projects.Build.Types.Sector sectp = boardService.getSector(esp.getSectnum());
            if (sectp == null) {
                continue;
            }

            sectp.setCeilingz(sectp.getCeilingz() - Z(SP_TAG4(esp)));

            if (SP_TAG5(esp) != 0) {
                sectp.setFloorheinum( SP_TAG5(esp));
                sectp.setFloorstat(sectp.getFloorstat() | (FLOOR_STAT_SLOPE));
            }

            if (SP_TAG6(esp) != 0) {
                sectp.setCeilingheinum( SP_TAG6(esp));
                sectp.setCeilingstat(sectp.getCeilingstat() | (CEILING_STAT_SLOPE));
            }

            for (int zh = sectp.getCeilingz(); zh < sectp.getFloorz(); zh += Z(60)) {
                SectorExp(cf, esp.getSectnum(), orig_ang, zh + Z(RANDOM_P2(64)) - Z(32));
            }

            // don't need it any more
            KillSprite(cf);
        }
    }

    public static void DoSpawnSpot(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u != null && (u.WaitTics -= synctics) < 0) {
            change_sprite_stat(SpriteNum, STAT_SPAWN_SPOT);
            SpawnShrap(SpriteNum, -1);

            if (u.LastDamage == 1) {
                KillSprite(SpriteNum);
            }
        }
    }

    // spawns shrap when killing an object
    public static void DoSpawnSpotsForKill(int match) {
        if (match < 0) {
            return;
        }

        ListNode<Sprite> next_sn;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPAWN_SPOT); node != null; node = next_sn) {
            int sn = node.getIndex();
            next_sn = node.getNext();
            Sprite sp = node.get();

            // change the stat num and set the delay correctly to call SpawnShrap
            if (sp.getHitag() == SPAWN_SPOT && sp.getLotag() == match) {
                USER u = getUser(sn);
                if (u == null) {
                    continue;
                }

                change_sprite_stat(sn, STAT_NO_STATE);
                u.ActorActionFunc = DoSpawnSpot;
                u.WaitTics =  (SP_TAG5(sp) * 15);
                engine.setspritez(sn, sp.getX(), sp.getY(), sp.getZ());
                // setting for Killed
                u.LastDamage = 1;
            }
        }
    }

    // spawns shrap when damaging an object
    public static void DoSpawnSpotsForDamage(int match) {
        if (match < 0) {
            return;
        }

        ListNode<Sprite> next_sn;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPAWN_SPOT); node != null; node = next_sn) {
            int sn = node.getIndex();
            next_sn = node.getNext();
            Sprite sp = node.get();

            // change the stat num and set the delay correctly to call SpawnShrap

            if (sp.getHitag() == SPAWN_SPOT && sp.getLotag() == match) {
                USER u = getUser(sn);
                if (u == null) {
                    continue;
                }

                change_sprite_stat(sn, STAT_NO_STATE);
                u.ActorActionFunc = DoSpawnSpot;
                u.WaitTics =  (SP_TAG7(sp) * 15);
                // setting for Damaged
                u.LastDamage = 0;
            }
        }
    }

    public static void DoSoundSpotMatch(int match, int sound_num, SoundType ignored) {
        sound_num--;

        ListNode<Sprite> next_sn;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SOUND_SPOT); node != null; node = next_sn) {
            int sn = node.getIndex();
            next_sn = node.getNext();
            Sprite sp = node.get();

            if (SP_TAG2(sp) == match && !TEST_BOOL6(sp)) {

                snd[0] =  SP_TAG13(sp); // tag4 is copied to tag13
                snd[1] =  SP_TAG5(sp);
                snd[2] =  SP_TAG6(sp);

                int snd2play = 0;
                int flags = 0;

                if (TEST_BOOL2(sp)) {
                    flags = v3df_follow | v3df_nolookup | v3df_init;
                }

                // play once and only once
                if (TEST_BOOL1(sp)) {
                    SET_BOOL6(sp);
                }

                // don't pan
                if (TEST_BOOL4(sp)) {
                    flags |= v3df_dontpan;
                }
                // add doppler
                if (TEST_BOOL5(sp)) {
                    flags |= v3df_doppler;
                }
                // random
                if (TEST_BOOL3(sp)) {
                    if (snd[0] != 0 && snd[1] != 0) {
                        snd2play = snd[RANDOM_RANGE(2)];
                    } else if (snd[0] != 0 || snd[1] != 0 || snd[2] != 0) {
                        snd2play = snd[RANDOM_RANGE(3)];
                    }
                } else if (snd[sound_num] != 0) {
                    snd2play = snd[sound_num];
                }

                if (snd2play <= 0) {
                    continue;
                }

                if (TEST_BOOL7(sp)) {
                    PlayerStr pp = GlobPlayerStr;
                    if (pp != null) {
                        if (pp == Player[myconnectindex]) {
                            PlayerSound(snd2play, v3df_dontpan | v3df_follow, pp);
                        }
                    }
                } else {
                    Set3DSoundOwner(sn, PlaySound(snd2play, sp, flags));
                }
            }
        }
    }

    /**
     * for (MapNode<Sprite> node = boardService.getStatNode(STAT_SOUND_SPOT); node != null; node = next_sn)
     */

    public static void DoSoundSpotStopSound(int match) {
        ListNode<Sprite> next_sn;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SOUND_SPOT); node != null; node = next_sn) {
            next_sn = node.getNext();
            Sprite sp = node.get();

            // found match and is a follow type
            if (SP_TAG2(sp) == match && TEST_BOOL2(sp)) {
                DeleteNoSoundOwner(node.getIndex());
            }
        }
    }

    public static void DoStopSoundSpotMatch(int match) {
        ListNode<Sprite> next_sn;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_STOP_SOUND_SPOT); node != null; node = next_sn) {
            next_sn = node.getNext();
            Sprite sp = node.get();

            if (SP_TAG2(sp) == match) {
                DoSoundSpotStopSound(SP_TAG5(sp));
            }
        }
    }

    public static boolean TestKillSectorObject(Sector_Object sop) {
        if (TEST(sop.flags, SOBJ_KILLABLE)) {
            KillMatchingCrackSprites(sop.match_event);
            // get new sectnums
            CollapseSectorObject(sop, sop.xmid, sop.ymid);
            DoSpawnSpotsForKill(sop.match_event);
            KillSectorObjectSprites(sop);
            return (true);
        }

        return (false);
    }

    public static void DoSectorObjectKillMatch(int match) {
        for (int s = 0; s < MAX_SECTOR_OBJECTS; s++) {
            Sector_Object sop = SectorObject[s];

            if (sop.xmid == MAXLONG) {
                continue;
            }

            if (sop.match_event == match) {
                TestKillSectorObject(sop);
                return;
            }
        }
    }

    public static void SearchExplodeSectorMatch(int match) {
        ListNode<Sprite> next_sn;

        // THIS IS ONLY CALLED FROM DoMatchEverything
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPRITE_HIT_MATCH); node != null; node = next_sn) {
            next_sn = node.getNext();
            Sprite sp = node.get();

            if (sp.getHitag() == match) {
                KillMatchingCrackSprites(match);
                DoExplodeSector(match);
                return;
            }
        }
    }

    public static void KillMatchingCrackSprites(int match) {
        Sprite sp;

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPRITE_HIT_MATCH); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            sp = node.get();

            if (sp.getHitag() == match) {
                if (TEST(SP_TAG8(sp), BIT(2))) {
                    continue;
                }

                KillSprite(i);
            }
        }
    }

    public static void WeaponExplodeSectorInRange(int weapon) {
        Sprite wp = boardService.getSprite(weapon);
        if (wp == null) {
            return;
        }

        USER wu = getUser(weapon);
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SPRITE_HIT_MATCH); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();

            // test to see if explosion is close to crack sprite
            int dist = FindDistance3D(wp.getX() - sp.getX(), wp.getY() - sp.getY(), (wp.getZ() - sp.getZ()) >> 4);

            if (sp.getClipdist() == 0) {
                continue;
            }

            int radius = ((sp.getClipdist()) << 2) * 8;

            if (wu != null && dist > (wu.Radius / 2) + radius) {
                continue;
            }

            if (!FAFcansee(wp.getX(), wp.getY(), wp.getZ(), wp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum())) {
                continue;
            }

            // this and every other crack sprite of this type is now dead
            // don't use them

            // pass in explosion type
            MissileHitMatch(weapon, WPN_ROCKET, i);

            return;
        }
    }

    public static void ShootableSwitch(int SpriteNum, int ignored) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        switch (sp.getPicnum()) {
            case SWITCH_SHOOTABLE_1:
                OperateSprite(SpriteNum, false);
                sp.setPicnum(SWITCH_SHOOTABLE_1 + 1);
                break;
            case SWITCH_FUSE:
            case SWITCH_FUSE + 1:
                sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                OperateSprite(SpriteNum, false);
                sp.setPicnum(SWITCH_FUSE + 2);
                break;
        }
    }

    public static void DoDeleteSpriteMatch(int match) {
        int del_x = 0, del_y = 0;
        int stat;
        int found;

        ListNode<Sprite> nexti;
        while (true) {
            found = -1;

            // search for a DELETE_SPRITE with same match tag
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_DELETE_SPRITE); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite sp = node.get();
                if (sp.getLotag() == match) {
                    found = i;
                    del_x = sp.getX();
                    del_y = sp.getY();
                    break;
                }
            }

            if (found == -1) {
                return;
            }

            for (stat = 0; stat < StatList.length; stat++) {
                for (ListNode<Sprite> node = boardService.getStatNode(StatList[stat]); node != null; node = nexti) {
                    int i = node.getIndex();
                    nexti = node.getNext();
                    Sprite sp = node.get();
                    if (del_x == sp.getX() && del_y == sp.getY()) {
                        // special case lighting delete of Fade On/off after fades
                        if (StatList[stat] == STAT_LIGHTING) {
                            // set shade to darkest and then kill it
                            sp.setShade((byte) SPRITE_TAG6(i));
                            sp.setPal(0);
                            SectorLightShade(i, sp.getShade());
                            DiffuseLighting(sp);
                        }

                        SpriteQueueDelete(i);
                        KillSprite(i);
                    }
                }
            }

            // kill the DELETE_SPRITE
            KillSprite(found);
        }
    }

    public static void DoChangorMatch(int match) {
        ListNode<Sprite> next_sn;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_CHANGOR); node != null; node = next_sn) {
            int sn = node.getIndex();
            next_sn = node.getNext();
            Sprite sp = node.get();
            ru.m210projects.Build.Types.Sector sectp = boardService.getSector(sp.getSectnum());
            if (sectp == null) {
                continue;
            }

            if (SP_TAG2(sp) != match) {
                continue;
            }

            if (TEST_BOOL1(sp)) {
                sectp.setCeilingpicnum( SP_TAG4(sp));
                sectp.setCeilingz(sectp.getCeilingz() + Z(SP_TAG5(sp)));
                sectp.setCeilingheinum(sectp.getCeilingheinum() + SP_TAG6(sp));

                if (sectp.getCeilingheinum() != 0) {
                    sectp.setCeilingstat(sectp.getCeilingstat() | (CEILING_STAT_SLOPE));
                } else {
                    sectp.setCeilingstat(sectp.getCeilingstat() & ~(CEILING_STAT_SLOPE));
                }

                sectp.setCeilingshade(sectp.getCeilingshade() + SP_TAG7(sp));
                sectp.setCeilingpal(sectp.getCeilingpal() + SP_TAG8(sp));
            } else {
                sectp.setFloorpicnum( SP_TAG4(sp));
                sectp.setFloorz(sectp.getFloorz() + Z(SP_TAG5(sp)));
                sectp.setFloorheinum(sectp.getFloorheinum() + SP_TAG6(sp));

                if (sectp.getFloorheinum() != 0) {
                    sectp.setFloorstat(sectp.getFloorstat() | (FLOOR_STAT_SLOPE));
                } else {
                    sectp.setFloorstat(sectp.getFloorstat() & ~(FLOOR_STAT_SLOPE));
                }

                sectp.setFloorshade(sectp.getFloorshade() + SP_TAG7(sp));
                sectp.setFloorpal(sectp.getFloorpal() + SP_TAG8(sp));
            }

            sectp.setVisibility(sectp.getVisibility() + SP_TAG9(sp));

            // if not set then go ahead and kill it
            if (!TEST_BOOL2(sp)) {
                KillSprite(sn);
            }
        }
    }

    public static void DoMatchEverything(PlayerStr pp, int match, int state) {
        PlayerStr bak = GlobPlayerStr;
        GlobPlayerStr = pp;
        // CAREFUL! pp == NULL is a valid case for this routine
        DoStopSoundSpotMatch(match);
        DoSoundSpotMatch(match, 1, SoundType.SOUND_EVERYTHING_TYPE);
        GlobPlayerStr = bak;

        DoLightingMatch(match, state);

        DoQuakeMatch(match);

        // make sure all vators are inactive before allowing
        // to repress switch
        if (!TestVatorMatchActive(match)) {
            DoVatorMatch(pp, match);
        }

        if (!TestSpikeMatchActive(match)) {
            DoSpikeMatch(pp, match);
        }

        if (TestRotatorMatchActive(match)) {
            DoRotatorMatch(pp, match, false);
        }

        if (!TestSlidorMatchActive(match)) {
            DoSlidorMatch(pp, match, false);
        }

        DoSectorObjectKillMatch(match);
        DoSectorObjectSetScale(match);

        DoSOevent(match, state);
        DoSpawnActorTrigger(match);

        // this may or may not find an exploding sector
        SearchExplodeSectorMatch(match);

        CopySectorMatch(match);
        DoWallMoveMatch(match);
        DoSpawnSpotsForKill(match);

        DoTrapReset(match);
        DoTrapMatch(match);

        SpawnItemsMatch(match);
        DoChangorMatch(match);
        DoDeleteSpriteMatch(match);
    }

    public static boolean ComboSwitchTest(int combo_type, int match) {
        int state;

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_DEFAULT); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            if (sp.getLotag() == combo_type && sp.getHitag() == match) {
                // dont toggle - get the current state
                state = AnimateSwitch(sp, 999);

                // if any one is not set correctly then switch is not set
                if (state != SP_TAG3(sp)) {
                    return (false);
                }
            }
        }

        return (true);
    }

    // NOTE: switches are always wall sprites
    public static boolean OperateSprite(int SpriteNum, boolean player_is_operating) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return false;
        }

        PlayerStr pp = null;

        if (Prediction) {
            return (false);
        }

        if (sp.getPicnum() == ST1) {
            return (false);
        }

        if (player_is_operating) {
            pp = GlobPlayerStr;

            if (!FAFcansee(pp.posx, pp.posy, pp.posz, pp.cursectnum, sp.getX(), sp.getY(), sp.getZ() - DIV2(SPRITEp_SIZE_Z(sp)),
                    sp.getSectnum())) {
                return (false);
            }
        }

        switch (sp.getLotag()) {
            case TOILETGIRL_R0:
            case WASHGIRL_R0:
            case CARGIRL_R0:
            case MECHANICGIRL_R0:
            case SAILORGIRL_R0:
            case PRUNEGIRL_R0: {
                USER u = getUser(SpriteNum);
                if (u == null) {
                    break;
                }

                u.FlagOwner = 1;
                u.WaitTics =  SEC(4);

                if (pp != Player[myconnectindex]) {
                    return (true);
                }

                int choose_snd = STD_RANDOM_RANGE(1000);
                if (sp.getLotag() == CARGIRL_R0) {
                    if (choose_snd > 700) {
                        PlayerSound(DIGI_JG44052, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 500) {
                        PlayerSound(DIGI_JG45014, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 250) {
                        PlayerSound(DIGI_JG44068, v3df_dontpan | v3df_follow, pp);
                    } else {
                        PlayerSound(DIGI_JG45010, v3df_dontpan | v3df_follow, pp);
                    }
                } else if (sp.getLotag() == MECHANICGIRL_R0) {
                    if (choose_snd > 700) {
                        PlayerSound(DIGI_JG44027, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 500) {
                        PlayerSound(DIGI_JG44038, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 250) {
                        PlayerSound(DIGI_JG44039, v3df_dontpan | v3df_follow, pp);
                    } else {
                        PlayerSound(DIGI_JG44048, v3df_dontpan | v3df_follow, pp);
                    }
                } else if (sp.getLotag() == SAILORGIRL_R0) {
                    if (choose_snd > 700) {
                        PlayerSound(DIGI_JG45018, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 500) {
                        PlayerSound(DIGI_JG45030, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 250) {
                        PlayerSound(DIGI_JG45033, v3df_dontpan | v3df_follow, pp);
                    } else {
                        PlayerSound(DIGI_JG45043, v3df_dontpan | v3df_follow, pp);
                    }
                } else if (sp.getLotag() == PRUNEGIRL_R0) {
                    if (choose_snd > 700) {
                        PlayerSound(DIGI_JG45053, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 500) {
                        PlayerSound(DIGI_JG45067, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 250) {
                        PlayerSound(DIGI_JG46005, v3df_dontpan | v3df_follow, pp);
                    } else {
                        PlayerSound(DIGI_JG46010, v3df_dontpan | v3df_follow, pp);
                    }
                } else if (sp.getLotag() == TOILETGIRL_R0) {
                    if (choose_snd > 700) {
                        PlayerSound(DIGI_WHATYOUEATBABY, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 500) {
                        PlayerSound(DIGI_WHATDIEDUPTHERE, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 250) {
                        PlayerSound(DIGI_YOUGOPOOPOO, v3df_dontpan | v3df_follow, pp);
                    } else {
                        PlayerSound(DIGI_PULLMYFINGER, v3df_dontpan | v3df_follow, pp);
                    }
                } else {
                    if (choose_snd > 700) {
                        PlayerSound(DIGI_SOAPYOUGOOD, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 500) {
                        PlayerSound(DIGI_WASHWANG, v3df_dontpan | v3df_follow, pp);
                    } else if (choose_snd > 250) {
                        PlayerSound(DIGI_DROPSOAP, v3df_dontpan | v3df_follow, pp);
                    } else {
                        PlayerSound(DIGI_REALTITS, v3df_dontpan | v3df_follow, pp);
                    }
                }
            }
            return (true);

            case PACHINKO1: {
                USER u = getUser(SpriteNum);
                if (u == null) {
                    break;
                }

                // Don't mess with it if it's already going
                if (u.WaitTics > 0) {
                    return (true);
                }

                PlaySound(DIGI_PFLIP, sp, v3df_none);
                u.WaitTics = (SEC(3) + SEC(RANDOM_RANGE(10)));
                ChangeState(SpriteNum, s_Pachinko1Operate[0]);
                return (true);
            }
            case PACHINKO2: {
                USER u = getUser(SpriteNum);
                if (u == null) {
                    break;
                }

                // Don't mess with it if it's already going
                if (u.WaitTics > 0) {
                    return (true);
                }

                PlaySound(DIGI_PFLIP, sp, v3df_none);
                u.WaitTics = (SEC(3) + SEC(RANDOM_RANGE(10)));
                ChangeState(SpriteNum, s_Pachinko2Operate[0]);
                return (true);
            }
            case PACHINKO3: {
                USER u = getUser(SpriteNum);
                if (u == null) {
                    break;
                }

                // Don't mess with it if it's already going
                if (u.WaitTics > 0) {
                    return (true);
                }

                PlaySound(DIGI_PFLIP, sp, v3df_none);
                u.WaitTics = (SEC(3) + SEC(RANDOM_RANGE(10)));
                ChangeState(SpriteNum, s_Pachinko3Operate[0]);
                return (true);
            }
            case PACHINKO4: {
                USER u = getUser(SpriteNum);
                if (u == null) {
                    break;
                }

                // Don't mess with it if it's already going
                if (u.WaitTics > 0) {
                    return (true);
                }

                PlaySound(DIGI_PFLIP, sp, v3df_none);
                u.WaitTics = (SEC(3) + SEC(RANDOM_RANGE(10)));
                ChangeState(SpriteNum, s_Pachinko4Operate[0]);
                return (true);
            }
            case SWITCH_LOCKED: {
                int key_num = sp.getHitag();
                if (key_num > 0 && key_num <= NUM_KEYS && pp != null && pp.HasKey[key_num - 1] != 0) {
                    for (int i = 0; i < boardService.getSectorCount(); i++) {
                        Sect_User su = getSectUser(i);
                        if (su != null && su.stag == SECT_LOCK_DOOR && su.number == key_num) {
                            su.number = 0; // unlock all doors of this type
                        }
                    }
                    UnlockKeyLock(key_num, SpriteNum);
                }
                return (true);
            }
            case TAG_COMBO_SWITCH_EVERYTHING:

                // change the switch state
                AnimateSwitch(sp, -1);
                PlaySound(DIGI_REGULARSWITCH, sp, v3df_none);

                if (ComboSwitchTest(TAG_COMBO_SWITCH_EVERYTHING, sp.getHitag())) {
                    DoMatchEverything(pp, sp.getHitag(), ON);
                }
                return (true);

            case TAG_COMBO_SWITCH_EVERYTHING_ONCE:

                // change the switch state
                AnimateSwitch(sp, -1);
                PlaySound(DIGI_REGULARSWITCH, sp, v3df_none);

                if (ComboSwitchTest(TAG_COMBO_SWITCH_EVERYTHING, sp.getHitag())) {
                    DoMatchEverything(pp, sp.getHitag(), ON);
                }

                sp.setLotag(0);
                sp.setHitag(0);
                return (true);

            case TAG_SWITCH_EVERYTHING:
                DoMatchEverything(pp, sp.getHitag(), AnimateSwitch(sp, -1));
                return (true);

            case TAG_SWITCH_EVERYTHING_ONCE:
                DoMatchEverything(pp, sp.getHitag(), AnimateSwitch(sp, -1));
                sp.setLotag(0);
                sp.setHitag(0);
                return (true);

            case TAG_LIGHT_SWITCH:
                DoLightingMatch(sp.getHitag(), AnimateSwitch(sp, -1));
                return (true);

            case TAG_SPRITE_SWITCH_VATOR: {
                // make sure all vators are inactive before allowing
                // to repress switch
                if (!TestVatorMatchActive(sp.getHitag())) {
                    DoVatorMatch(pp, sp.getHitag());
                }

                if (!TestSpikeMatchActive(sp.getHitag())) {
                    DoSpikeMatch(pp, sp.getHitag());
                }

                if (TestRotatorMatchActive(sp.getHitag())) {
                    DoRotatorMatch(pp, sp.getHitag(), false);
                }

                if (!TestSlidorMatchActive(sp.getHitag())) {
                    DoSlidorMatch(pp, sp.getHitag(), false);
                }
                return (true);
            }

            case TAG_LEVEL_EXIT_SWITCH: {

                AnimateSwitch(sp, -1);

                PlaySound(DIGI_BIGSWITCH, sp, v3df_none);

                if (sp.getHitag() != 0) {
                    Level = sp.getHitag();
                } else {
                    Level++;
                }
                ExitLevel = true;
                FinishedLevel = true;
                return (true);
            }

            case TAG_SPRITE_GRATING: {
                change_sprite_stat(SpriteNum, STAT_NO_STATE);

                USER u = SpawnUser(SpriteNum, 0, null);

                u.ActorActionFunc = DoGrating;

                sp.setLotag(0);
                sp.setHitag(sp.getHitag() / 2);
                return (true);
            }

            case TAG_SO_SCALE_SWITCH:
                AnimateSwitch(sp, -1);
                DoSectorObjectSetScale(sp.getHitag());
                return (true);

            case TAG_SO_SCALE_ONCE_SWITCH:
                AnimateSwitch(sp, -1);
                DoSectorObjectSetScale(sp.getHitag());
                sp.setLotag(0);
                sp.setHitag(0);
                return (true);

            case TAG_SO_EVENT_SWITCH: {
                DoMatchEverything(null, sp.getHitag(), AnimateSwitch(sp, -1));

                sp.setHitag(0);
                sp.setLotag(0);

                PlaySound(DIGI_REGULARSWITCH, sp, v3df_none);
                break;
            }

            case TAG_ROTATE_SO_SWITCH: {
                int so_num;
                Sector_Object sop;

                so_num = sp.getHitag();

                AnimateSwitch(sp, -1);

                sop = SectorObject[so_num];

                sop.ang_tgt = NORM_ANGLE(sop.ang_tgt + 512);

                PlaySound(DIGI_BIGSWITCH, sp, v3df_none);

                return (true);
            }
        }

        return (false);
    }

    public static void DoTrapReset(int match) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_TRAP); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();
            USER u = getUser(i);
            if (u == null) {
                continue;
            }

            if (sp.getLotag() != match) {
                continue;
            }

            // if correct type and matches
            if (sp.getHitag() == FIREBALL_TRAP) {
                u.WaitTics = 0;
            }

            // if correct type and matches
            if (sp.getHitag() == BOLT_TRAP) {
                u.WaitTics = 0;
            }

            // if correct type and matches
            if (sp.getHitag() == SPEAR_TRAP) {
                u.WaitTics = 0;
            }
        }
    }

    public static void DoTrapMatch(int match) {
        // may need to be reset to fire immediately

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_TRAP); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();
            USER u = getUser(i);
            if (u == null) {
                continue;
            }

            if (sp.getLotag() != match) {
                continue;
            }

            // if correct type and matches
            if (sp.getHitag() == FIREBALL_TRAP) {
                u.WaitTics -= synctics;

                if (u.WaitTics <= 0) {
                    u.WaitTics = 120;
                    InitFireballTrap(i);
                }
            }

            // if correct type and matches
            if (sp.getHitag() == BOLT_TRAP) {
                u.WaitTics -= synctics;

                if (u.WaitTics <= 0) {
                    u.WaitTics = 120;
                    InitBoltTrap(i);
                }
            }

            // if correct type and matches
            if (sp.getHitag() == SPEAR_TRAP) {
                u.WaitTics -= synctics;

                if (u.WaitTics <= 0) {
                    u.WaitTics = 120;
                    InitSpearTrap(i);
                }
            }
        }
    }

    public static void OperateTripTrigger(PlayerStr pp) {
        ru.m210projects.Build.Types.Sector sectp = boardService.getSector(pp.cursectnum);
        if (sectp == null) {
            return;
        }

        if (Prediction) {
            return;
        }

        // old method
        switch (LOW_TAG(pp.cursectnum)) {
            // same tag for sector as for switch
            case TAG_LEVEL_EXIT_SWITCH: {
                if (sectp.getHitag() != 0) {
                    Level = sectp.getHitag();
                } else {
                    Level++;
                }
                ExitLevel = true;
                FinishedLevel = true;
                break;
            }

            case TAG_SECRET_AREA_TRIGGER:
                if (pp == Player[myconnectindex]) {
                    PlayerSound(DIGI_ANCIENTSECRET, v3df_dontpan | v3df_doppler | v3df_follow, pp);
                }

                PutStringInfo(pp, "You found a secret area!");
                // always give to the first player
                Player[0].SecretsFound++;
                sectp.setLotag(0);
                sectp.setHitag(0);
                break;

            case TAG_TRIGGER_EVERYTHING:
                DoMatchEverything(pp, sectp.getHitag(), -1);
                break;

            case TAG_TRIGGER_EVERYTHING_ONCE:
                DoMatchEverything(pp, sectp.getHitag(), -1);
                sectp.setLotag(0);
                sectp.setHitag(0);
                break;

            case TAG_SECTOR_TRIGGER_VATOR:
                if (!TestVatorMatchActive(sectp.getHitag())) {
                    DoVatorMatch(pp, sectp.getHitag());
                }
                if (!TestSpikeMatchActive(sectp.getHitag())) {
                    DoSpikeMatch(pp, sectp.getHitag());
                }
                if (TestRotatorMatchActive(sectp.getHitag())) {
                    DoRotatorMatch(pp, sectp.getHitag(), false);
                }
                if (!TestSlidorMatchActive(sectp.getHitag())) {
                    DoSlidorMatch(pp, sectp.getHitag(), false);
                }
                break;

            case TAG_LIGHT_TRIGGER:
                DoLightingMatch(sectp.getHitag(), -1);
                break;

            case TAG_SO_SCALE_TRIGGER:
                DoSectorObjectSetScale(sectp.getHitag());
                break;

            case TAG_SO_SCALE_ONCE_TRIGGER:
                DoSectorObjectSetScale(sectp.getHitag());
                sectp.setLotag(0);
                sectp.setHitag(0);
                break;

            case TAG_TRIGGER_ACTORS: {
                int dist = sectp.getHitag();

                ListNode<Sprite> nexti;
                for (ListNode<Sprite> node = boardService.getStatNode(STAT_ENEMY); node != null; node = nexti) {
                    int i = node.getIndex();
                    nexti = node.getNext();
                    Sprite sp = node.get();
                    USER u = getUser(i);
                    if (u == null) {
                        continue;
                    }

                    if (TEST(u.Flags, SPR_WAIT_FOR_TRIGGER)) {
                        if (Distance(sp.getX(), sp.getY(), pp.posx, pp.posy) < dist) {
                            u.tgt_sp = pp.PlayerSprite;
                            u.Flags &= ~(SPR_WAIT_FOR_TRIGGER);
                        }
                    }
                }
                break;
            }

            case TAG_TRIGGER_MISSILE_TRAP: {
                // reset traps so they fire immediately
                DoTrapReset(sectp.getHitag());
                break;
            }

            case TAG_TRIGGER_EXPLODING_SECTOR: {
                DoMatchEverything(null, sectp.getHitag(), -1);
                break;
            }

            case TAG_SPAWN_ACTOR_TRIGGER: {
                DoMatchEverything(null, sectp.getHitag(), -1);

                sectp.setHitag(0);
                sectp.setLotag(0);
                break;
            }

            case TAG_SO_EVENT_TRIGGER: {
                DoMatchEverything(null, sectp.getHitag(), -1);

                sectp.setHitag(0);
                sectp.setLotag(0);

                PlaySound(DIGI_REGULARSWITCH, pp, v3df_none);
                break;
            }
        }
    }

    public static void OperateContinuousTrigger(PlayerStr pp) {
        ru.m210projects.Build.Types.Sector sectp = boardService.getSector(pp.cursectnum);
        if (sectp == null) {
            return;
        }

        if (Prediction) {
            return;
        }

        if (LOW_TAG(pp.cursectnum) == TAG_TRIGGER_MISSILE_TRAP) {
            DoTrapMatch(sectp.getHitag());

            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_TRAP); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite sp = node.get();
                USER u = getUser(i);
                if (u == null) {
                    continue;
                }

                // if correct type and matches
                if (sp.getHitag() == FIREBALL_TRAP && sp.getLotag() == sectp.getHitag()) {
                    u.WaitTics -= synctics;

                    if (u.WaitTics <= 0) {
                        u.WaitTics = 120;
                        InitFireballTrap(i);
                    }
                }

                // if correct type and matches
                if (sp.getHitag() == BOLT_TRAP && sp.getLotag() == sectp.getHitag()) {
                    u.WaitTics -= synctics;

                    if (u.WaitTics <= 0) {
                        u.WaitTics = 120;
                        InitBoltTrap(i);
                    }
                }

                // if correct type and matches
                if (sp.getHitag() == SPEAR_TRAP && sp.getLotag() == sectp.getHitag()) {
                    u.WaitTics -= synctics;

                    if (u.WaitTics <= 0) {
                        u.WaitTics = 120;
                        InitSpearTrap(i);
                    }
                }
            }
        }
    }

    public static void PlayerTakeSectorDamage(PlayerStr pp) {
        Sect_User sectu = getSectUser(pp.cursectnum);
        USER u = getUser(pp.PlayerSprite);
        if (sectu == null || u == null) {
            return;
        }

        // the calling routine must make sure sectu exists
        if ((u.DamageTics -= synctics) < 0) {
            u.DamageTics = DAMAGE_TIME;

            PlayerUpdateHealth(pp, -sectu.damage);
            PlayerCheckDeath(pp, -1);
        }
    }

    public static void NearThings(PlayerStr pp) {
        ru.m210projects.Build.Types.Sector sectp = boardService.getSector(pp.cursectnum);
        if (sectp == null) {
            return;
        }

        // Check player's current sector for triggered sound
        if (sectp.getHitag() == PLAYER_SOUNDEVENT_TAG) {
            if (pp == Player[myconnectindex]) {
                PlayerSound(sectp.getLotag(), v3df_follow | v3df_dontpan, pp);
            }
            return;
        }

        engine.neartag(pp.posx, pp.posy, pp.posz, pp.cursectnum, pp.getAnglei(), neartag, 1024, NTAG_SEARCH_LO_HI);

        // hit a sprite? Check to see if it has sound info in it!
        // This can work with any sprite!
        Sprite sp = boardService.getSprite(neartag.tagsprite);
        if (sp != null) {

            // Go through list of cases
            if (sp.getHitag() == PLAYER_SOUNDEVENT_TAG) {
                if (pp == Player[myconnectindex]) {
                    PlayerSound(sp.getLotag(), v3df_follow | v3df_dontpan, pp);
                }
            }
            return; // Return false so he doesn't grunt
        }

        Wall nearwall = boardService.getWall(neartag.tagwall);
        if (nearwall != null) {
            // Check player's current sector for triggered sound
            if (nearwall.getHitag() == PLAYER_SOUNDEVENT_TAG) {
                if (pp == Player[myconnectindex]) {
                    PlayerSound(nearwall.getLotag(), v3df_follow | v3df_dontpan, pp);
                }
                return; // We are playing a sound so don't return true
            }
            return;
        }
        // This only gets called if nothing else worked, check for nearness to a wall
        {
            int dang = pp.getAnglei();

            FAFhitscan(pp.posx, pp.posy, pp.posz - Z(30), pp.cursectnum, // Start position
                    EngineUtils.sin(NORM_ANGLE(dang + 512)), // X vector of 3D ang
                    EngineUtils.sin(NORM_ANGLE(dang)), // Y vector of 3D ang
                    0, pHitInfo, CLIPMASK_MISSILE); // Z vector of 3D ang

            if (pHitInfo.hitsect == -1) {
                return;
            }

            if (Distance(pHitInfo.hitx, pHitInfo.hity, pp.posx, pp.posy) > 1024) // was 1500, GDX 19.05.2020
            {
                return;
            }

            // hit a sprite?
            if (pHitInfo.hitsprite != -1) {
                return;
            }

            if (neartag.tagsector >= 0) {
                return;
            }

            Wall wp = boardService.getWall(pHitInfo.hitwall);
            if (wp != null) {
                // Near a plain old vanilla wall. Can't do anything but grunt.
                if (!TEST(wp.getExtra(), WALLFX_DONT_STICK) && pp == Player[myconnectindex]) {
                    if (STD_RANDOM_RANGE(1000) > 970) {
                        PlayerSound(DIGI_HITTINGWALLS, v3df_follow | v3df_dontpan, pp);
                    } else {
                        PlayerSound(DIGI_SEARCHWALL, v3df_follow | v3df_dontpan, pp);
                    }
                }

            }
        }
    }

    public static void NearTagList(NEAR_TAG_INFO[] ntip, int ntipnum, PlayerStr pp, int z, int dist, int type,
                                   int count) {
        engine.neartag(pp.posx, pp.posy, z, pp.cursectnum, pp.getAnglei(), neartag, dist, type);

        int neartagsector = neartag.tagsector;
        int neartaghitdist = neartag.taghitdist;
        int neartagwall = neartag.tagwall;
        int neartagsprite = neartag.tagsprite;

        ru.m210projects.Build.Types.Sector nearsec = boardService.getSector(neartagsector);
        Wall nearwall = boardService.getWall(neartagwall);
        Sprite nearsprite = boardService.getSprite(neartagsprite);
        if (nearsec != null) {
            // save off values
            int save_lotag = nearsec.getLotag();
            int save_hitag = nearsec.getHitag();

            ntip[ntipnum].dist = neartaghitdist;
            ntip[ntipnum].sectnum = neartagsector;
            ntip[ntipnum].spritenum = -1;
            nti_cnt++;
            ntipnum++;

            if (nti_cnt >= count) {
                return;
            }

            // remove them
            nearsec.setLotag(0);
            nearsec.setHitag(0);

            NearTagList(ntip, ntipnum, pp, z, dist, type, count);

            // reset off values
            nearsec.setLotag(save_lotag);
            nearsec.setHitag(save_hitag);
        } else if (nearwall != null) {
            // save off values
            int save_lotag = nearwall.getLotag();
            int save_hitag = nearwall.getHitag();

            ntip[ntipnum].dist = neartaghitdist;
            ntip[ntipnum].sectnum = -1;
            ntip[ntipnum].spritenum = -1;
            nti_cnt++;
            ntipnum++;

            if (nti_cnt >= count) {
                return;
            }

            // remove them
            nearwall.setLotag(0);
            nearwall.setHitag(0);

            NearTagList(ntip, ntipnum, pp, z, dist, type, count);

            // reset off values
            nearwall.setLotag(save_lotag);
            nearwall.setHitag(save_hitag);
        } else if (nearsprite != null) {
            // save off values
            int save_lotag = nearsprite.getLotag();
            int save_hitag = nearsprite.getHitag();

            ntip[ntipnum].dist = neartaghitdist;
            ntip[ntipnum].sectnum = -1;
            ntip[ntipnum].spritenum = neartagsprite;
            nti_cnt++;
            ntipnum++;

            if (nti_cnt >= count) {
                return;
            }

            // remove them
            nearsprite.setLotag(0);
            nearsprite.setHitag(0);

            NearTagList(ntip, ntipnum, pp, z, dist, type, count);

            // reset off values
            nearsprite.setLotag(save_lotag);
            nearsprite.setHitag(save_hitag);
        } else {
            ntip[ntipnum].dist = -1;
            ntip[ntipnum].sectnum = -1;
            ntip[ntipnum].spritenum = -1;
            nti_cnt++;
        }
    }

    public static void BuildNearTagList(NEAR_TAG_INFO[] ntip, int size, PlayerStr pp, int z, int dist, int type,
                                        int count) {
        for (int i = 0; i < size; i++) {
            ntip[i].reset();
        }
        nti_cnt = 0;
        NearTagList(ntip, 0, pp, z, dist, type, count);
    }

    public static boolean DoPlayerGrabStar(PlayerStr pp) {
        Sprite sp = null;
        int i;

        // MUST check exact z's of each star or it will never work
        for (i = 0; i < MAX_STAR_QUEUE; i++) {
            if (StarQueue[i] >= 0) {
                sp = boardService.getSprite(StarQueue[i]);

                if (sp != null && FindDistance3D(sp.getX() - pp.posx, sp.getY() - pp.posy, (sp.getZ() - pp.posz + Z(12)) >> 4) < 500) {
                    break;
                }
            }
        }

        if (i < MAX_STAR_QUEUE) {
            // Pull a star out of wall and up your ammo
            PlayerUpdateAmmo(pp, WPN_STAR, 1);
            PlaySound(DIGI_ITEM, sp, v3df_none);
            KillSprite(StarQueue[i]);
            StarQueue[i] = -1;
            if (TEST(pp.WpnFlags, BIT(WPN_STAR))) {
                return (true);
            }
            pp.WpnFlags |= (BIT(WPN_STAR));
            InitWeaponStar(pp);
            return (true);
        }

        return (false);
    }

    public static void PlayerOperateEnv(PlayerStr pp) {
        ru.m210projects.Build.Types.Sector sectp = boardService.getSector(pp.cursectnum);
        Sprite psp = pp.getSprite();
        if (psp == null || sectp == null) {
            return;
        }

        Sect_User sectu = getSectUser(pp.cursectnum);
        boolean found;

        if (Prediction) {
            return;
        }

        //
        // Switch & door activations
        //

        if (TEST_SYNC_KEY(pp, SK_OPERATE)) {
            if (FLAG_KEY_PRESSED(pp, SK_OPERATE)) {
                // if space bar pressed
                int nt_ndx;

                if (DoPlayerGrabStar(pp)) {
                    FLAG_KEY_RELEASE(pp, SK_OPERATE);
                } else {
                    NearThings(pp); // Check for player sound specified in a level sprite
                }

                BuildNearTagList(nti, 16, pp, pp.posz, 2048, NTAG_SEARCH_LO_HI, 8);

                found = false;

                // try and find a sprite
                for (nt_ndx = 0; nti[nt_ndx].dist >= 0; nt_ndx++) {
                    if (nti[nt_ndx].spritenum >= 0 && nti[nt_ndx].dist < 1024 + 768) {
                        if (OperateSprite(nti[nt_ndx].spritenum, true)) {
                            FLAG_KEY_RELEASE(pp, SK_OPERATE);
                            found = true;
                        }
                    }
                }

                // if not found look at different z positions
                if (!found) {


                    z[0] = psp.getZ() - SPRITEp_SIZE_Z(psp) - Z(10);
                    z[1] = psp.getZ();
                    z[2] = DIV2(z[0] + z[1]);

                    for (int i = 0; i < 3; i++) {
                        BuildNearTagList(nti, 16, pp, z[i], 1024 + 768, NTAG_SEARCH_LO_HI, 8);

                        for (nt_ndx = 0; nti[nt_ndx].dist >= 0; nt_ndx++) {
                            if (nti[nt_ndx].spritenum >= 0 && nti[nt_ndx].dist < 1024 + 768) {
                                if (OperateSprite(nti[nt_ndx].spritenum, true)) {
                                    FLAG_KEY_RELEASE(pp, SK_OPERATE);
                                    break;
                                }
                            }
                        }
                    }
                }

                int neartaghitdist = nti[0].dist;
                int neartagsector = nti[0].sectnum;

                if (neartagsector >= 0 && neartaghitdist < 1024) {
                    if (OperateSector(neartagsector, true)) {
                        // Release the key
                        FLAG_KEY_RELEASE(pp, SK_OPERATE);

                        // Hack fow wd secret area
                        if (pp.cursectnum == 491 && Level == 11 && swGetAddon() == 1) {
                            ru.m210projects.Build.Types.Sector secretSec = boardService.getSector(715);
                            if (secretSec != null && secretSec.getLotag() == TAG_SECRET_AREA_TRIGGER) {
                                if (pp == Player[myconnectindex]) {
                                    PlayerSound(DIGI_ANCIENTSECRET, v3df_dontpan | v3df_doppler | v3df_follow, pp);
                                }
                                PutStringInfo(pp, "You found a secret area!");
                                // always give to the first player
                                Player[0].SecretsFound++;
                                secretSec.setLotag(0);
                            }
                        }
                    }
                }

                //
                // Trigger operations
                //

                switch (LOW_TAG(pp.cursectnum)) {
                    case TAG_VATOR:
                        DoVatorOperate(pp, pp.cursectnum);
                        DoSpikeOperate(pp, pp.cursectnum);
                        DoRotatorOperate(pp, pp.cursectnum);
                        DoSlidorOperate(pp, pp.cursectnum);
                        break;
                    case TAG_SPRING_BOARD:
                        DoSpringBoard(pp);
                        FLAG_KEY_RELEASE(pp, SK_OPERATE);
                        break;
                    case TAG_DOOR_ROTATE:
                        if (OperateSector(pp.cursectnum, true)) {
                            FLAG_KEY_RELEASE(pp, SK_OPERATE);
                        }
                        break;
                }
            }
        } else {
            // Reset the key when syncbit key is not in use
            FLAG_KEY_RESET(pp, SK_OPERATE);
        }

        // ////////////////////////////
        //
        // Sector Damage
        //
        // ////////////////////////////

        if (sectu != null && sectu.damage != 0) {
            if (TEST(sectu.flags, SECTFU_DAMAGE_ABOVE_SECTOR)) {
                PlayerTakeSectorDamage(pp);
            } else if ((SPRITEp_BOS(pp.getSprite()) >= sectp.getFloorz()) && !TEST(pp.Flags, PF_DIVING)) {
                PlayerTakeSectorDamage(pp);
            }
        } else {
            USER u = getUser(pp.PlayerSprite);
            if (u != null) {
                u.DamageTics = 0;
            }
        }

        // ////////////////////////////
        //
        // Trigger stuff
        //
        // ////////////////////////////

        OperateContinuousTrigger(pp);

        // just changed sectors
        if (pp.lastcursectnum != pp.cursectnum) {
            OperateTripTrigger(pp);
            ru.m210projects.Build.Types.Sector sec = boardService.getSector(pp.cursectnum);

            if (sec != null && TEST(sec.getExtra(), SECTFX_WARP_SECTOR)) {
                if (!TEST(pp.Flags2, PF2_TELEPORTED)) {
                    DoPlayerWarpTeleporter(pp);
                }
            }

            pp.Flags2 &= ~(PF2_TELEPORTED);
        }
    }

    public static void DoSineWaveFloor() {
        for (int wave = 0; wave < MAX_SINE_WAVE; wave++) {
            for (int swi = 0, flags = SineWaveFloor[wave][0].flags; swi < SineWaveFloor[wave].length
                    && SineWaveFloor[wave][swi].sector >= 0; swi++) {
                SINE_WAVE_FLOOR swf = SineWaveFloor[wave][swi];
                swf.sintable_ndx = NORM_ANGLE(swf.sintable_ndx + (synctics << swf.speed_shift));
                ru.m210projects.Build.Types.Sector sec = boardService.getSector(swf.sector);
                if (sec == null) {
                    continue;
                }

                if (TEST(flags, SINE_FLOOR)) {
                    int newz = swf.floor_origz + ((swf.range * EngineUtils.sin(swf.sintable_ndx)) >> 14);
                    game.pInt.setfloorinterpolate(swf.sector, sec);
                    sec.setFloorz(newz);
                }

                if (TEST(flags, SINE_CEILING)) {
                    int newz = swf.ceiling_origz + ((swf.range * EngineUtils.sin(swf.sintable_ndx)) >> 14);
                    game.pInt.setceilinterpolate(swf.sector, sec);
                    sec.setCeilingz(newz);
                }

            }
        }

        /*
         * SLOPED SIN-WAVE FLOORS:
         *
         * It's best to program sloped sin-wave floors in 2 steps: 1. First set the
         * floorz of the floor as the sin code normally does it. 2. Adjust the slopes by
         * calling alignflorslope once for each sector.
         *
         * Note: For this to work, the first wall of each sin-wave sector must be
         * aligned on the same side of each sector for the entire strip.
         */

        for (int wave = 0; wave < MAX_SINE_WAVE; wave++) {
            for (int swi = 0, flags = SineWaveFloor[wave][0].flags; swi < SineWaveFloor[wave].length
                    && SineWaveFloor[wave][swi].sector >= 0; swi++) {
                SINE_WAVE_FLOOR swf = SineWaveFloor[wave][swi];
                ru.m210projects.Build.Types.Sector sec = boardService.getSector(swf.sector);
                if (sec == null) {
                    continue;
                }

                if (!TEST(sec.getFloorstat(), FLOOR_STAT_SLOPE)) {
                    continue;
                }

                if (TEST(flags, SINE_SLOPED)) {
                    if (sec.getWallnum() == 4) {
                        // Set wal to the wall on the opposite side of the sector
                        Wall wal = boardService.getWall(sec.getWallptr() + 2);
                        if (wal != null) {
                            // Pass (Sector, x, y, z)
                            game.pInt.setfheinuminterpolate(swf.sector, sec);
                            ru.m210projects.Build.Types.Sector ns = boardService.getSector(wal.getNextsector());
                            if (ns != null) {
                                engine.alignflorslope(swf.sector, wal.getX(), wal.getY(), ns.getFloorz());
                            }
                        }
                    }
                }
            }
        }
    }

    public static void DoSineWaveWall() {
        for (int sw_num = 0; sw_num < MAX_SINE_WAVE; sw_num++) {
            for (int swi = 0; swi < MAX_SINE_WALL_POINTS && SineWall[sw_num][swi].wall >= 0; swi++) {
                SINE_WALL sw = SineWall[sw_num][swi];
                Wall wp = boardService.getWall(sw.wall);
                if (wp == null) {
                    continue;
                }

                // move through the sintable
                sw.sintable_ndx = NORM_ANGLE(sw.sintable_ndx + (synctics << sw.speed_shift));

                int newsp = sw.orig_xy + ((sw.range * EngineUtils.sin(sw.sintable_ndx)) >> 14);
                if (sw.type == 0) {
                    engine.dragpoint(sw.wall, wp.getX(), newsp);
                } else {
                    engine.dragpoint(sw.wall, newsp, wp.getY());
                }
            }
        }
    }

    public static int getAnimValue(Object obj, AnimType type) {
        int j = 0;

        switch (type) {
            case FloorZ:
                j = ((ru.m210projects.Build.Types.Sector) obj).getFloorz();
                break;
            case CeilZ:
                j = ((ru.m210projects.Build.Types.Sector) obj).getCeilingz();
                break;
            case SpriteZ:
                j = ((Sprite) obj).getZ();
                break;
            case SectorObjectZ:
                j = ((Sector_Object) obj).zmid;
                break;
            case UserZ:
                j = ((USER) obj).sz;
                break;
            case SectUserDepth:
                j = ((Sect_User) obj).depth_fract;
                break;
        }

        return j;
    }

    public static void DoAnim(int numtics) {
        for (int i = AnimCnt - 1; i >= 0; i--) {
            Interpolation gInt = game.pInt;
            Anim gAnm = pAnim[i];
            Object obj = gAnm.ptr;
            int animval = getAnimValue(gAnm.ptr, gAnm.type);

            if (animval < gAnm.goal) { // if LESS THAN goal
                // move it
                animval = Math.min(animval + (numtics * PIXZ(gAnm.vel)), gAnm.goal);
            } else { // if GREATER THAN goal
                animval = Math.max(animval - (numtics * PIXZ(gAnm.vel)), gAnm.goal);
            }
            gAnm.vel += gAnm.vel_adj * numtics;

            switch (gAnm.type) {
                case FloorZ:
                    gInt.setfloorinterpolate(gAnm.index, (ru.m210projects.Build.Types.Sector) obj);
                    ((ru.m210projects.Build.Types.Sector) obj).setFloorz(animval);
                    break;
                case CeilZ:
                    gInt.setceilinterpolate(gAnm.index, (ru.m210projects.Build.Types.Sector) obj);
                    ((ru.m210projects.Build.Types.Sector) obj).setCeilingz(animval);
                    break;
                case SpriteZ:
                    gInt.setsprinterpolate(gAnm.index, (Sprite) obj);
                    ((Sprite) obj).setZ(animval);
                    break;
                case SectorObjectZ:
//				System.err.println("DoAnim aaa");
                    ((Sector_Object) obj).zmid = animval;
                    break;
                case UserZ:
                    // System.err.println("DoAnim() bbb");
                    ((USER) obj).sz = animval;
                    break;
                case SectUserDepth:
                    ((Sect_User) obj).depth_fract =  animval;
                    break;
            }

            // EQUAL this entry has finished
            if (animval == gAnm.goal) {
                AnimCallback acp = gAnm.callback;

                // do a callback when done if not NULL
                if (gAnm.callback != null) {
                    gAnm.callback.invoke(gAnm, gAnm.callbackdata);
                }

                // only delete it if the callback has not changed
                // Logic here is that if the callback changed then something
                // else must be happening with it - dont delete it
                if (gAnm.callback == acp) {
                    // decrement the count
                    AnimCnt--;

                    // move the last entry to the current one to free the last entry up
                    gAnm.copy(pAnim[AnimCnt]);
                }
            }
        }
    }

    public static int AnimGetGoal(Object object, AnimType type) {
        int j = -1;
        for (int i = AnimCnt - 1; i >= 0; i--) {
            if (object == pAnim[i].ptr && type == pAnim[i].type) {
                j = i;
                break;
            }
        }
        return (j);
    }

    public static void AnimDelete(Object object, AnimType type) {
        int i, j;

        j = -1;
        for (i = 0; i < AnimCnt; i++) {
            if (object == pAnim[i].ptr && type == pAnim[i].type) {
                j = i;
                break;
            }
        }

        if (j == -1) {
            return;
        }

        // decrement the count
        AnimCnt--;

        // move the last entry to the current one to free the last entry up
        pAnim[j].copy(pAnim[AnimCnt]);
    }

    public static void InitAnim() {
        for (int i = 0; i < MAXANIM; i++) {
            pAnim[i] = new Anim();
        }
    }

    public static Object GetAnimObject(int index, AnimType type) {
        Object object = null;
        switch (type) {
            case FloorZ:
            case CeilZ:
                object = boardService.getSector(index);
                break;
            case SpriteZ:
                object = boardService.getSprite(index);
                break;
            case SectorObjectZ:
                object = SectorObject[index];
                break;
            case UserZ:
                object = getUser(index);
                break;
            case SectUserDepth:
                object = SetupSectUser(index);
                break;
        }

        return object;
    }

    public static int AnimSet(int index, int thegoal, int thevel, AnimType type) {
        if (AnimCnt >= MAXANIM) {
            return -1;
        }

        Object animptr = GetAnimObject(index, type);
        if (animptr == null) {
            return -1;
        }

        // look for existing animation and reset it
        int j = AnimGetGoal(animptr, type);
        if (j == -1) {
            j = AnimCnt;
        }

        pAnim[j].ptr = animptr;
        pAnim[j].index = index;
        pAnim[j].type = type;
        pAnim[j].goal = thegoal;
        pAnim[j].vel = Z(thevel);
        pAnim[j].vel_adj = 0;
        pAnim[j].callback = null;
        pAnim[j].callbackdata = -1;

        if (j == AnimCnt) {
            AnimCnt++;
        }

        return (j);
    }

    public static void AnimSetCallback(int anim_ndx, AnimCallback call, int data) {
        if (anim_ndx >= AnimCnt) {
            return;
        }

        if (anim_ndx == -1) {
            return;
        }

        pAnim[anim_ndx].callback = call;
        pAnim[anim_ndx].callbackdata = data;
    }

    public static void AnimSetVelAdj(int anim_ndx, int vel_adj) {
        if (anim_ndx >= AnimCnt) {
            return;
        }

        if (anim_ndx == -1) {
            return;
        }

        pAnim[anim_ndx].vel_adj =  vel_adj;
    }

    public static void DoPanning() {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FLOOR_PAN); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();
            ru.m210projects.Build.Types.Sector sectp = boardService.getSector(sp.getSectnum());
            if (sectp == null) {
                continue;
            }

            int nx = ((EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512))) * sp.getXvel()) >> 20;
            int ny = ((EngineUtils.sin(NORM_ANGLE(sp.getAng()))) * sp.getXvel()) >> 20;

            sectp.setFloorxpanning(sectp.getFloorxpanning() + nx);
            sectp.setFloorypanning(sectp.getFloorypanning() + ny);

            sectp.setFloorxpanning(sectp.getFloorxpanning() & 255);
            sectp.setFloorypanning(sectp.getFloorypanning() & 255);
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_CEILING_PAN); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();
            ru.m210projects.Build.Types.Sector sectp = boardService.getSector(sp.getSectnum());
            if (sectp == null) {
                continue;
            }

            int nx = ((EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512))) * sp.getXvel()) >> 20;
            int ny = ((EngineUtils.sin(NORM_ANGLE(sp.getAng()))) * sp.getXvel()) >> 20;

            sectp.setCeilingxpanning(sectp.getCeilingxpanning() + nx);
            sectp.setCeilingypanning(sectp.getCeilingypanning() + ny);

            sectp.setCeilingxpanning(sectp.getCeilingxpanning() & 255);
            sectp.setCeilingypanning(sectp.getCeilingypanning() & 255);
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_WALL_PAN); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();
            Wall wp = boardService.getWall(sp.getOwner());
            if (wp == null) {
                continue;
            }

            int nx = ((EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512))) * sp.getXvel()) >> 20;
            int ny = ((EngineUtils.sin(NORM_ANGLE(sp.getAng()))) * sp.getXvel()) >> 20;

            wp.setXpanning(wp.getXpanning() + nx);
            wp.setYpanning(wp.getYpanning() + ny);

            wp.setXpanning(wp.getXpanning() & 255);
            wp.setYpanning(wp.getYpanning() & 255);
        }
    }

    public static void DoSector() {
        if (DebugActorFreeze) {
            return;
        }

        for (int j = 0; j < MAX_SECTOR_OBJECTS; j++) {
            Sector_Object sop = SectorObject[j];

            if (sop.xmid == MAXSO) {
                continue;
            }

            boolean riding = false;
            int min_dist = 999999;

            for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                PlayerStr pp = Player[pnum];

                if (pp.sop_riding == j) {
                    riding = true;
                    pp.sop_riding = -1;
                    break;
                } else {
                    int dist = DISTANCE(pp.posx, pp.posy, sop.xmid, sop.ymid);
                    if (dist < min_dist) {
                        min_dist = dist;
                    }
                }
            }

            if (sop.Animator != null) {
                sop.Animator.invoke(j);
                continue;
            }

            int sync_flag;
            // force sync SOs to be updated regularly
            if ((sync_flag = DTEST(sop.flags, SOBJ_SYNC1 | SOBJ_SYNC2)) != 0) {
                if (sync_flag == SOBJ_SYNC1) {
                    MoveSectorObjects(j, synctics, 1);
                } else {
                    if (!MoveSkip2) {
                        MoveSectorObjects(j, synctics * 2, 2);
                    }
                }

                continue;
            }

            if (riding) {
                // if riding move smoothly
                // update every time
                MoveSectorObjects(j, synctics, 1);
            } else {
                if (min_dist < 15000) {
                    // if close update every other time
                    if (!MoveSkip2) {
                        MoveSectorObjects(j, synctics * 2, 2);
                    }
                } else {
                    // if further update every 4th time
                    if (MoveSkip4 == 0) {
                        MoveSectorObjects(j, synctics * 4, 4);
                    }
                }
            }
        }

        DoPanning();
        DoLighting();
        DoSineWaveFloor();
        DoSineWaveWall();
        DoSpringBoardDown();
    }

    public static void clearSecUser() {
        Arrays.fill(SectUser, null);
    }

    @Nullable
    public static Sect_User getSectUser(int sectnum) {
        if (sectnum >= 0 && sectnum < SectUser.length) {
            return SectUser[sectnum];
        }
        return null;
    }

    public static void setSectUser(int sectnum, Sect_User su) {
        SectUser[sectnum] = su;
    }
}
