package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Enemies.Decision;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Type.Animator;
import ru.m210projects.Wang.Type.LONGp;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.TAG_SWARMSPOT;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Rooms.FAFcansee;
import static ru.m210projects.Wang.Rooms.FAFhitscan;
import static ru.m210projects.Wang.Sector.NTAG_SEARCH_LO_HI;
import static ru.m210projects.Wang.Sector.OperateSector;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Tags.*;
import static ru.m210projects.Wang.Track.ActorFindTrack;
import static ru.m210projects.Wang.Track.Track;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Weapon.PlayerTakeDamage;

public class Ai {

    public static final Animator InitActorAttack = new Animator((Animator.Runnable) Ai::InitActorAttack);
    public static final Animator DoActorDecide = new Animator((Animator.Runnable) Ai::DoActorDecide);
    public static final Animator InitActorRunToward = new Animator((Animator.Runnable) Ai::InitActorRunToward);
    public static final Animator InitActorRunAway = new Animator((Animator.Runnable) Ai::InitActorRunAway);
    public static final Animator InitActorDecide = new Animator((Animator.Runnable) Ai::InitActorDecide);
    public static final Animator InitActorAlertNoise = new Animator((Animator.Runnable) Ai::InitActorAlertNoise);
    public static final Animator InitActorAmbientNoise = new Animator((Animator.Runnable) Ai::InitActorAmbientNoise);
    public static final Animator InitActorAttackNoise = new Animator((Animator.Runnable) Ai::InitActorAttackNoise);
    public static final Animator InitActorDieNoise = new Animator((Animator.Runnable) Ai::InitActorDieNoise);
    public static final Animator InitActorPainNoise = new Animator((Animator.Runnable) Ai::InitActorPainNoise);
    public static final Animator InitActorExtra1Noise = new Animator((Animator.Runnable) Ai::InitActorExtra1Noise);
    public static final Animator InitActorExtra2Noise = new Animator((Animator.Runnable) Ai::InitActorExtra2Noise);
    public static final Animator InitActorExtra3Noise = new Animator((Animator.Runnable) Ai::InitActorExtra3Noise);
    public static final Animator InitActorExtra4Noise = new Animator((Animator.Runnable) Ai::InitActorExtra4Noise);
    public static final Animator InitActorExtra5Noise = new Animator((Animator.Runnable) Ai::InitActorExtra5Noise);
    public static final Animator InitActorExtra6Noise = new Animator((Animator.Runnable) Ai::InitActorExtra6Noise);
    public static final Animator InitActorMoveCloser = new Animator((Animator.Runnable) Ai::InitActorMoveCloser);
    public static final Animator DoActorMoveCloser = new Animator((Animator.Runnable) Ai::DoActorMoveCloser);
    public static final Animator DoActorAttack = new Animator((Animator.Runnable) Ai::DoActorAttack);
    public static final Animator InitActorEvade = new Animator((Animator.Runnable) Ai::InitActorEvade);
    public static final Animator InitActorWanderAround = new Animator((Animator.Runnable) Ai::InitActorWanderAround);
    public static final Animator InitActorFindPlayer = new Animator((Animator.Runnable) Ai::InitActorFindPlayer);
    public static final Animator InitActorDuck = new Animator((Animator.Runnable) Ai::InitActorDuck);
    public static final Animator DoActorDuck = new Animator((Animator.Runnable) Ai::DoActorDuck);
    public static final Animator DoActorMoveJump = new Animator((Animator.Runnable) Ai::DoActorMoveJump);
    public static final Animator InitActorReposition = new Animator((Animator.Runnable) Ai::InitActorReposition);
    public static final Animator DoActorReposition = new Animator((Animator.Runnable) Ai::DoActorReposition);
    public static final Animator DoActorPause = new Animator((Animator.Runnable) Ai::DoActorPause);


    public static void InitActorAttackNoise(int SpriteNum) {
        USER pUser = getUser(SpriteNum);
        if (pUser != null) {
            pUser.ActorActionFunc = DoActorDecide;
        }
    }

    public static void InitActorAmbientNoise(int SpriteNum) {
        USER pUser = getUser(SpriteNum);
        if (pUser != null) {
            pUser.ActorActionFunc = DoActorDecide;
        }
    }

    public static void InitActorAlertNoise(int SpriteNum) {
        USER pUser = getUser(SpriteNum);
        if (pUser != null) {
            pUser.ActorActionFunc = DoActorDecide;
        }
    }

    public static final int SLOW_SPEED = 0, NORM_SPEED = 1, MID_SPEED = 2, FAST_SPEED = 3; //, MAX_SPEED = 4;
//    public static final int MAXATTRIBSNDS = 11;
    public static final int TOWARD = 1;
    public static final int AWAY = -1;
    private static final int[] PlayerAbove = {BIT(TT_LADDER), BIT(TT_STAIRS), BIT(TT_JUMP_UP), BIT(TT_TRAVERSE), BIT(TT_OPERATE), BIT(TT_SCAN)};
    private static final int[] PlayerBelow = {BIT(TT_JUMP_DOWN), BIT(TT_STAIRS), BIT(TT_TRAVERSE), BIT(TT_OPERATE), BIT(TT_SCAN)};
    private static final int[] PlayerOnLevel = {BIT(TT_DUCK_N_SHOOT), BIT(TT_HIDE_N_SHOOT), BIT(TT_TRAVERSE), BIT(TT_EXIT), BIT(TT_OPERATE), BIT(TT_SCAN)};
    private static final int[] RunAwayTracks = {BIT(TT_EXIT), BIT(TT_LADDER), BIT(TT_TRAVERSE), BIT(TT_STAIRS), BIT(TT_JUMP_UP), BIT(TT_JUMP_DOWN), BIT(TT_DUCK_N_SHOOT), BIT(TT_HIDE_N_SHOOT), BIT(TT_OPERATE), BIT(TT_SCAN)};

    /*
     * !AIC - Does a table lookup based on a random value from 0 to 1023. These
     * tables are defined at the top of all actor files such as ninja.c, goro.c etc.
     */
    private static final int[] WanderTracks = {BIT(TT_DUCK_N_SHOOT), BIT(TT_HIDE_N_SHOOT), BIT(TT_WANDER), BIT(TT_JUMP_DOWN), BIT(TT_JUMP_UP), BIT(TT_TRAVERSE), BIT(TT_STAIRS), BIT(TT_LADDER), BIT(TT_EXIT), BIT(TT_OPERATE)};

    /*
     * !AIC - Sometimes just want the offset of the action
     */
    private static final short[][] toward_angle_delta = {{-160, -384, 160, 384, -256, 256, -512, 512, -99}, {-384, -160, 384, 160, -256, 256, -512, 512, -99}, {160, 384, -160, -384, 256, -256, 512, -512, -99}, {384, 160, -384, -160, 256, -256, 512, -512, -99}};
    private static final short[][] away_angle_delta = {{-768, 768, -640, 640, -896, 896, 1024, -99}, {768, -768, 640, -640, -896, 896, 1024, -99}, {896, -896, -768, 768, -640, 640, 1024, -99}, {896, -896, 768, -768, 640, -640, 1024, -99}};
    private static final int[] AwayDist = {17000, 20000, 26000, 26000, 26000, 32000, 32000, 42000};
    private static final int[] TowardDist = {10000, 15000, 20000, 20000, 25000, 30000, 35000, 40000};

    /*
     * !AIC - Pick a nearby player to be the actors target
     */
    private static final int[] PlayerDist = {2000, 3000, 3000, 5000, 5000, 5000, 9000, 9000};

    public static boolean ActorMoveHitReact(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return false;
        }

        // Should only return true if there is a reaction to what was hit that
        // would cause the calling function to abort

        switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
            case HIT_SPRITE: {
                int HitSprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                USER hu = getUser(HitSprite);

                // if you ran into a player - call close range functions
                if (hu != null && hu.PlayerP != -1) {
                    DoActorPickClosePlayer(SpriteNum);
                    Animator action = ChooseAction(u.Personality.TouchTarget);
                    if (action != null) {
                        action.animatorInvoke(SpriteNum);
                        return (true);
                    }
                }
                break;
            }

            case HIT_WALL:
            case HIT_SECTOR:
                break;
        }

        return (false);
    }

    public static int RandomRange(int range) {
        if (range <= 0) {
            return (0);
        }

        int rand_num = RANDOM();
        if (rand_num == 65535) {
            rand_num--;
        }

        // shift values to give more precision
        int value = (rand_num << 14) / ((65535 << 14) / range);
        if (value >= range) {
            value = range - 1;
        }

        return value;
    }

    public static int StdRandomRange(int range) {
        if (range <= 0) {
            return (0);
        }

        int rand_num = STD_RANDOM();
        if (rand_num == RAND_MAX) {
            rand_num--;
        }

        // shift values to give more precision
        int value = (int) ((rand_num << 14) / (((long) (RAND_MAX) << 14) / range));
        if (value >= range) {
            value = range - 1;
        }

        return (value);
    }

    public static boolean ActorFlaming(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        Sprite fp = boardService.getSprite(u.flame);
        if (fp != null) {
            int size = SPRITEp_SIZE_Z(sp) - DIV4(SPRITEp_SIZE_Z(sp));
            return SPRITEp_SIZE_Z(fp) > size;
        }

        return (false);
    }


    public static void DoActorSetSpeed(int SpriteNum, int speed) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
            return;
        }

        u.speed = (byte) speed;
        if (ActorFlaming(SpriteNum)) {
            sp.setXvel( (u.Attrib.Speed[speed] + DIV2(u.Attrib.Speed[speed])));
        } else {
            sp.setXvel(u.Attrib.Speed[speed]);
        }
    }

    public static Animator ChooseAction(Decision[] decision) {
        // !JIM! Here is an opportunity for some AI, instead of randomness!
        int random_value = RANDOM_P2(1024 << 5) >> 5;
        for (int i = 0; i < 10; i++) {
            if (random_value <= decision[i].range) {
                return (decision[i].action);
            }
        }
        return null;
    }


    public static int ChooseActionNumber(short[] decision) {
        int random_value = RANDOM_P2(1024 << 5) >> 5;
        for (int i = 0; true; i++) {
            if (random_value <= decision[i]) {
                return (i);
            }
        }
    }

    public static final Decision[] GenericFlaming = {new Decision(30, InitActorAttack), new Decision(512, InitActorRunToward), new Decision(1024, InitActorRunAway)};

    /*
     * !AIC KEY - This routine decides what the actor will do next. It is not called
     * every time through the loop. This would be too slow. It is only called when
     * the actor needs to know what to do next such as running into something or
     * being targeted. It makes decisions based on the distance and viewablity of
     * its target (u.tgt_sp). When it figures out the situatation with its target it
     * calls ChooseAction which does a random table lookup to decide what action to
     * initialize. Once this action is initialized it will be called until it can't
     * figure out what to do anymore and then this routine is called again.
     */

    public static void DoActorNoise(Animator Action, int SpriteNum) {
        if (Action == InitActorAmbientNoise) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_ambient, v3df_follow);
        } else if (Action == InitActorAlertNoise) {
            USER u = getUser(SpriteNum);
            if (u != null && !u.DidAlert) {// This only allowed once
                PlaySpriteSound(SpriteNum, Attrib_Snds.attr_alert, v3df_follow);
            }
        } else if (Action == InitActorAttackNoise) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_attack, v3df_follow);
        } else if (Action == InitActorPainNoise) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_pain, v3df_follow);
        } else if (Action == InitActorDieNoise) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_die, v3df_none);
        } else if (Action == InitActorExtra1Noise) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_extra1, v3df_follow);
        } else if (Action == InitActorExtra2Noise) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_extra2, v3df_follow);
        } else if (Action == InitActorExtra3Noise) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_extra3, v3df_follow);
        } else if (Action == InitActorExtra4Noise) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_extra4, v3df_follow);
        } else if (Action == InitActorExtra5Noise) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_extra5, v3df_follow);
        } else if (Action == InitActorExtra6Noise) {
            PlaySpriteSound(SpriteNum, Attrib_Snds.attr_extra6, v3df_follow);
        }

    }

    /*
     * !AIC - Setup to do the decision
     */

    public static boolean CanSeePlayer(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        // if actor can still see the player
        int look_height = SPRITEp_TOS(sp);
        Sprite target = boardService.getSprite(u.tgt_sp);
        if (target == null) {
            return false;
        }

        return FAFcansee(sp.getX(), sp.getY(), look_height, sp.getSectnum(), target.getX(), target.getY(), SPRITEp_UPPER(target), target.getSectnum());
    }


    public static boolean CanHitPlayer(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        // if actor can still see the player
        int zhs = sp.getZ() - DIV2(SPRITEp_SIZE_Z(sp));
        Sprite hp = boardService.getSprite(u.tgt_sp);
        if (hp == null) {
            return false;
        }

        // get angle to target
        int ang = EngineUtils.getAngle(hp.getX() - sp.getX(), hp.getY() - sp.getY());

        // get x,yvect
        int xvect = EngineUtils.sin(NORM_ANGLE(ang + 512));
        int yvect = EngineUtils.sin(NORM_ANGLE(ang));

        // get zvect
        int zhh = hp.getZ() - DIV2(SPRITEp_SIZE_Z(hp));
        int zvect;
        if (hp.getX() - sp.getX() != 0) {
            zvect = xvect * ((zhh - zhs) / (hp.getX() - sp.getX()));
        } else if (hp.getY() - sp.getY() != 0) {
            zvect = yvect * ((zhh - zhs) / (hp.getY() - sp.getY()));
        } else {
            return (false);
        }

        FAFhitscan(sp.getX(), sp.getY(), zhs, sp.getSectnum(), xvect, yvect, zvect, pHitInfo, CLIPMASK_MISSILE);

        if (pHitInfo.hitsect == -1) {
            return (false);
        }

        return pHitInfo.hitsprite == u.tgt_sp;
    }

    public static void DoActorPickClosePlayer(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // if actor can still see the player
        int look_height = SPRITEp_TOS(sp);
        boolean found = false;

        if (u.ID != ZOMBIE_RUN_R0 || gNet.MultiGameType != MultiGameTypes.MULTI_GAME_COOPERATIVE) {
            // Set initial target to Player 0
            u.tgt_sp = Player[connecthead].PlayerSprite;

            if (TEST(u.Flags2, SPR2_DONT_TARGET_OWNER)) {
                for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                    PlayerStr pp = Player[pnum];

                    if (sp.getOwner() == pp.PlayerSprite) {
                        continue;
                    }

                    u.tgt_sp = pp.PlayerSprite;
                    break;
                }
            }

            // Set initial target to the closest player
            int near_dist = MAX_ACTIVE_RANGE;
            for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                PlayerStr pp = Player[pnum];

                // Zombies don't target their masters!
                if (TEST(u.Flags2, SPR2_DONT_TARGET_OWNER)) {
                    if (sp.getOwner() == pp.PlayerSprite) {
                        continue;
                    }

                    if (!PlayerTakeDamage(pp, SpriteNum)) {
                        continue;
                    }
                }

                int dist = DISTANCE(sp.getX(), sp.getY(), pp.posx, pp.posy);

                if (dist < near_dist) {
                    near_dist = dist;
                    u.tgt_sp = pp.PlayerSprite;
                }
            }

            // see if you can find someone close that you can SEE
            near_dist = MAX_ACTIVE_RANGE;
            for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                PlayerStr pp = Player[pnum];

                // Zombies don't target their masters!
                if (TEST(u.Flags2, SPR2_DONT_TARGET_OWNER)) {
                    if (sp.getOwner() == pp.PlayerSprite) {
                        continue;
                    }

                    if (!PlayerTakeDamage(pp, SpriteNum)) {
                        continue;
                    }
                }

                int dist = DISTANCE(sp.getX(), sp.getY(), pp.posx, pp.posy);
                Sprite psp = pp.getSprite();
                if (psp != null && dist < near_dist && FAFcansee(sp.getX(), sp.getY(), look_height, sp.getSectnum(), psp.getX(), psp.getY(), SPRITEp_UPPER(psp), psp.getSectnum())) {
                    near_dist = dist;
                    u.tgt_sp = pp.PlayerSprite;
                    found = true;
                }
            }
        }

        // this is only for Zombies right now
        // zombie target other actors
        if (!found && TEST(u.Flags2, SPR2_DONT_TARGET_OWNER)) {
            int near_dist = MAX_ACTIVE_RANGE;

            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_ENEMY); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                if (i == SpriteNum) {
                    continue;
                }

                USER ui = getUser(i);
                if (ui != null && TEST(ui.Flags, SPR_SUICIDE | SPR_DEAD)) {
                    continue;
                }

                Sprite s = node.get();
                int dist = DISTANCE(sp.getX(), sp.getY(), s.getX(), s.getY());
                if (dist < near_dist && FAFcansee(sp.getX(), sp.getY(), look_height, sp.getSectnum(), s.getX(), s.getY(), SPRITEp_UPPER(s), s.getSectnum())) {
                    near_dist = dist;
                    u.tgt_sp = i;
                }
            }
        }
    }


    private static void DoActorDecide(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // See what to do next
        Animator actor_action = DoActorActionDecide(SpriteNum);

        // Fix for the GenericFlaming bug for actors that don't have attack states
        if (actor_action == InitActorAttack && u.WeaponNum == 0) {
            return; // Just let the actor do as it was doing before in this case
        }

        USER tu = getUser(u.tgt_sp);
        // zombie is attacking a player
        if (actor_action == InitActorAttack && u.ID == ZOMBIE_RUN_R0 && u.tgt_sp != -1 && tu != null && tu.PlayerP != -1) {
            // Don't let zombies shoot at master
            if (sp.getOwner() == u.tgt_sp) {
                return;
            }

            // if this player cannot take damage from this zombie(weapon) return out
            if (!PlayerTakeDamage(Player[tu.PlayerP], SpriteNum)) {
                return;
            }
        }

        if (actor_action != InitActorDecide) {
            // NOT staying put
            actor_action.animatorInvoke(SpriteNum);
        } else {
            // Actually staying put
            NewStateGroup(SpriteNum, u.ActorActionSet.Stand);
        }
    }


    public static int GetPlayerSpriteNum(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return 0;
        }

        for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            PlayerStr pp = Player[pnum];
            if (pp.PlayerSprite == u.tgt_sp) {
                return (pp.PlayerSprite);
            }
        }
        return (0);
    }


    public static int CloseRangeDist(Sprite sp1, Sprite sp2) {
        int clip1 = sp1.getClipdist();
        int clip2 = sp2.getClipdist();

        // add clip boxes and a fudge factor
        int DIST_CLOSE_RANGE = 400;

        return ((clip1 << 2) + (clip2 << 2) + DIST_CLOSE_RANGE);
    }


    public static void DoActorOperate(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (u.ID == HORNET_RUN_R0 || u.ID == EEL_RUN_R0 || u.ID == BUNNY_RUN_R0) {
            return;
        }

        if (u.getRot() == u.ActorActionSet.Sit || u.getRot() == u.ActorActionSet.Stand) {
            return;
        }

        if ((u.WaitTics -= ACTORMOVETICS) > 0) {
            return;
        }

        engine.neartag(sp.getX(), sp.getY(), sp.getZ() - DIV2(SPRITEp_SIZE_Z(sp)), sp.getSectnum(), sp.getAng(), neartag, 1024, NTAG_SEARCH_LO_HI);

        if (neartag.tagsector >= 0 && neartag.taghitdist < 1024) {
            if (OperateSector(neartag.tagsector, false)) {
                u.WaitTics = 2 * 120;

                NewStateGroup(SpriteNum, u.ActorActionSet.Sit);
            }
        }
    }

    public static Animator DoActorActionDecide(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        Animator action = InitActorDecide;

        if (u == null || sp == null) {
            return action;
        }

        // REMINDER: This function is not even called if SpriteControl doesn't let
        // it get called

        u.Dist = 0;

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            return (action);
        }

        // everybody on fire acts like this
        if (ActorFlaming(SpriteNum)) {
            action = ChooseAction(GenericFlaming);
            return (action);
        }

        boolean ICanSee = CanSeePlayer(SpriteNum); // Only need to call once
        // But need the result multiple times

        // !AIC KEY - If aware of player - var is changed in SpriteControl
        if (TEST(u.Flags, SPR_ACTIVE)) {
            // Try to operate stuff
            DoActorOperate(SpriteNum);
            Sprite targetSpr = boardService.getSprite(u.tgt_sp);
            if (targetSpr != null) {
                // if far enough away and cannot see the player
                int dist = Distance(sp.getX(), sp.getY(), targetSpr.getX(), targetSpr.getY());

                if (dist > 30000 && !ICanSee) {
                    // Enemy goes inactive - he is still allowed to roam about for about
                    // 5 seconds trying to find another player before his active_range is
                    // bumped down
                    SetEnemyInactive(SpriteNum);

                    // You've lost the player - now decide what to do
                    action = ChooseAction(u.Personality.LostTarget);
                    return (action);
                }

                USER pu = getUser(GetPlayerSpriteNum(SpriteNum));
                // check for short range attack possibility
                if ((dist < CloseRangeDist(sp, targetSpr) && ICanSee) || (pu != null && pu.WeaponNum == WPN_FIST && u.ID != RIPPER2_RUN_R0 && u.ID != RIPPER_RUN_R0)) {
                    if ((u.ID == COOLG_RUN_R0 && TEST(sp.getCstat(), CSTAT_SPRITE_TRANSLUCENT)) || TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                        action = ChooseAction(u.Personality.Evasive);
                    } else {
                        action = ChooseAction(u.Personality.CloseRange);
                    }
                    return (action);
                }

                // if player is facing me and I'm being attacked
                if (FACING(sp, targetSpr) && TEST(u.Flags, SPR_ATTACKED) && ICanSee) {
                    // if I'm a target - at least one missile comming at me
                    if (TEST(u.Flags, SPR_TARGETED)) {
                        // not going to evade, reset the target bit
                        u.Flags &= ~(SPR_TARGETED); // as far as actor knows, it's not a target any more
                        if (u.ActorActionSet.Duck != null && RANDOM_P2(1024 << 8) >> 8 < 100) {
                            action = InitActorDuck;
                        } else {
                            if ((u.ID == COOLG_RUN_R0 && TEST(sp.getCstat(), CSTAT_SPRITE_TRANSLUCENT)) || TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                                action = ChooseAction(u.Personality.Evasive);
                            } else {
                                action = ChooseAction(u.Personality.Battle);
                            }
                        }
                    }
                    // if NOT a target - don't bother with evasive action and start
                    // fighting
                    else {
                        if ((u.ID == COOLG_RUN_R0 && TEST(sp.getCstat(), CSTAT_SPRITE_TRANSLUCENT)) || TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                            action = ChooseAction(u.Personality.Evasive);
                        } else {
                            action = ChooseAction(u.Personality.Battle);
                        }
                    }
                    return (action);

                }
                // if player is NOT facing me he is running or unaware of actor
                else if (ICanSee) {
                    if ((u.ID == COOLG_RUN_R0 && TEST(sp.getCstat(), CSTAT_SPRITE_TRANSLUCENT)) || TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                        action = ChooseAction(u.Personality.Evasive);
                    } else {
                        action = ChooseAction(u.Personality.Offense);
                    }
                    return (action);
                } else {
                    // You've lost the player - now decide what to do
                    action = ChooseAction(u.Personality.LostTarget);
                    return (action);
                }
            }
        }
        // Not active - not aware of player and cannot see him
        else {
            // try and find another player
            // pick a closeby player as the (new) target
            if (sp.getHitag() != TAG_SWARMSPOT) {
                DoActorPickClosePlayer(SpriteNum);
            }

            // if close by
            Sprite targetSpr = boardService.getSprite(u.tgt_sp);
            if (targetSpr != null) {
                int dist = Distance(sp.getX(), sp.getY(), targetSpr.getX(), targetSpr.getY());
                if (dist < 15000 || ICanSee) {
                    if ((FACING(sp, targetSpr) && dist < 10000) || ICanSee) {
                        DoActorOperate(SpriteNum);

                        // Don't let player completely sneek up behind you
                        action = ChooseAction(u.Personality.Surprised);
                        if (!u.DidAlert && ICanSee) {
                            DoActorNoise(InitActorAlertNoise, SpriteNum);
                            u.DidAlert = true;
                        }
                    } else {
                        // Player has not seen actor, to be fair let him know actor
                        // are there
                        DoActorNoise(ChooseAction(u.Personality.Broadcast), SpriteNum);
                    }
                    return (action);
                }
            }
        }

        return (action);
    }

    public static void InitActorPainNoise(int SpriteNum) {
        USER u = getUser(SpriteNum);
        // make some sort of noise here

        if (u != null) {
            u.ActorActionFunc = DoActorDecide;
        }
    }

    public static void InitActorDecide(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        // NOTE: It is possible to overflow the stack with too many calls to this
        // routine
        // Should use:
        // u.ActorActionFunc = DoActorDecide;
        // Instead of calling this function direcly

        u.ActorActionFunc = DoActorDecide;

        DoActorDecide.animatorInvoke(SpriteNum);
    }

    public static void InitActorDieNoise(int SpriteNum) {
        USER u = getUser(SpriteNum);
        // make some sort of noise here

        if (u != null) {
            u.ActorActionFunc = DoActorDecide;
        }
    }

    public static void DoActorCantMoveCloser(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // MONO_PRINT("Can't move closer\n");

        u.track =  FindTrackToPlayer(SpriteNum);

        if (u.track >= 0) {
            sp.setAng(EngineUtils.getAngle((Track[u.track].TrackPoint[u.point]).x - sp.getX(), (Track[u.track].TrackPoint[u.point]).y - sp.getY()));

            DoActorSetSpeed(SpriteNum, MID_SPEED);
            u.Flags |= (SPR_FIND_PLAYER);

            u.ActorActionFunc = DoActorDecide;
            NewStateGroup(SpriteNum, u.ActorActionSet.Run);
            // MONO_PRINT("Trying to get to the track point\n");
        } else {
            // Try to move closer
            InitActorReposition.animatorInvoke(SpriteNum);
        }
    }


    public static void InitActorExtra1Noise(int SpriteNum) {
        USER u = getUser(SpriteNum);
        // make some sort of noise here

        if (u != null) {
            u.ActorActionFunc = DoActorDecide;
        }
    }

    public static int FindTrackToPlayer(int SpriteNum) {
        final USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return -1;
        }

        int[] type;
        int size;

        int zdiff = SPRITEp_UPPER(boardService.getSprite(u.tgt_sp)) - (sp.getZ() - SPRITEp_SIZE_Z(sp) + Z(8));
        if (klabs(zdiff) <= Z(20)) {
            type = PlayerOnLevel;
            size = PlayerOnLevel.length;
        } else {
            if (zdiff < 0) {
                type = PlayerAbove;
                size = PlayerAbove.length;
            } else {
                type = PlayerBelow;
                size = PlayerBelow.length;
            }
        }

        for (int i = 0; i < size; i++) {
            int track = ActorFindTrack(SpriteNum, 1, type[i], tmp_ptr[0], tmp_ptr[1]);
            if (track >= 0) {
                u.point =  tmp_ptr[0].value;
                u.track_dir =  tmp_ptr[1].value;
                Track[track].flags |= (TF_TRACK_OCCUPIED);
                return (track);
            }
        }

        return (-1);
    }

    public static void InitActorExtra2Noise(int SpriteNum) {
        USER u = getUser(SpriteNum);
        // make some sort of noise here

        if (u != null) {
            u.ActorActionFunc = DoActorDecide;
        }
    }

    public static int FindTrackAwayFromPlayer(int SpriteNum) {
        final USER u = getUser(SpriteNum);
        for (int runAwayTrack : RunAwayTracks) {
            int track = ActorFindTrack(SpriteNum, -1, runAwayTrack, tmp_ptr[0], tmp_ptr[1]);

            if (u != null && track >= 0) {
                u.point = tmp_ptr[0].value;
                u.track_dir = tmp_ptr[1].value;
                Track[track].flags |= (TF_TRACK_OCCUPIED);

                return (track);
            }
        }
        return (-1);
    }


    public static void InitActorExtra3Noise(int SpriteNum) {
        USER u = getUser(SpriteNum);
        // make some sort of noise here
        if (u != null) {
            u.ActorActionFunc = DoActorDecide;
        }
    }

    public static int FindWanderTrack(int SpriteNum) {
        USER u = getUser(SpriteNum);
        for (int wanderTrack : WanderTracks) {
            int track = ActorFindTrack(SpriteNum, -1, wanderTrack, tmp_ptr[0], tmp_ptr[1]);
            if (u != null && track >= 0) {
                u.point = tmp_ptr[0].value;
                u.track_dir = tmp_ptr[1].value;
                Track[track].flags |= (TF_TRACK_OCCUPIED);

                return (track);
            }
        }

        return (-1);
    }


    public static void InitActorExtra4Noise(int SpriteNum) {
        USER u = getUser(SpriteNum);
        // make some sort of noise here

        if (u != null) {
            u.ActorActionFunc = DoActorDecide;
        }
    }

    public static void InitActorRunAway(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.ActorActionFunc = DoActorDecide;
        NewStateGroup(SpriteNum, u.ActorActionSet.Run);

        u.track = FindTrackAwayFromPlayer(SpriteNum);

        if (u.track >= 0) {
            sp.setAng(NORM_ANGLE(EngineUtils.getAngle((Track[u.track].TrackPoint[u.point]).x - sp.getX(), (Track[u.track].TrackPoint[u.point]).y - sp.getY())));
            DoActorSetSpeed(SpriteNum, FAST_SPEED);
            u.Flags |= (SPR_RUN_AWAY);
        } else {
            u.Flags |= (SPR_RUN_AWAY);
            InitActorReposition.animatorInvoke(SpriteNum);
        }
    }


    public static void InitActorExtra5Noise(int SpriteNum) {
        USER u = getUser(SpriteNum);
        // make some sort of noise here

        if (u != null) {
            u.ActorActionFunc = DoActorDecide;
        }
    }

    public static void InitActorRunToward(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.ActorActionFunc = DoActorDecide;
        NewStateGroup(SpriteNum, u.ActorActionSet.Run);

        InitActorReposition.animatorInvoke(SpriteNum);
        DoActorSetSpeed(SpriteNum, FAST_SPEED);
    }


    public static void InitActorExtra6Noise(int SpriteNum) {
        USER u = getUser(SpriteNum);
        // make some sort of noise here

        if (u != null) {
            u.ActorActionFunc = DoActorDecide;
        }
    }

    /*
     * !AIC KEY - Routines handle moving toward the player.
     */

    public static boolean CHOOSE2(int value) {
        return (RANDOM_P2(1024) < (value));
    }

    public static void InitActorMoveCloser(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.ActorActionFunc = DoActorMoveCloser;
        if (u.getRot() != u.ActorActionSet.Run) {
            NewStateGroup(SpriteNum, u.ActorActionSet.Run);
        }

        u.ActorActionFunc.animatorInvoke(SpriteNum);
    }

    public static void InitActorAttack(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite tgtSpr = boardService.getSprite(u.tgt_sp);
        if (tgtSpr != null) {
            USER tu = getUser(u.tgt_sp);
            // zombie is attacking a player
            if (tu != null && u.ID == ZOMBIE_RUN_R0 && tu.PlayerP != -1) {
                // Don't let zombies shoot at master
                if (sp.getOwner() == (u.tgt_sp)) {
                    return;
                }

                // if this player cannot take damage from this zombie(weapon) return out
                if (!PlayerTakeDamage(Player[tu.PlayerP], SpriteNum)) {
                    return;
                }
            }

            if (TEST(tgtSpr.getCstat(), CSTAT_SPRITE_TRANSLUCENT)) {
                InitActorRunAway(SpriteNum);
                return;
            }

            if (tu != null && tu.Health <= 0) {
                DoActorPickClosePlayer(SpriteNum);
                InitActorReposition.animatorInvoke(SpriteNum);
                return;
            }

            if (!CanHitPlayer(SpriteNum)) {
                InitActorReposition.animatorInvoke(SpriteNum);
                return;
            }

            // if the guy you are after is dead, look for another and
            // reposition
            if (tu != null && tu.PlayerP != -1 && TEST(Player[tu.PlayerP].Flags, PF_DEAD)) {
                DoActorPickClosePlayer(SpriteNum);
                InitActorReposition.animatorInvoke(SpriteNum);
                return;
            }

            u.ActorActionFunc = DoActorAttack;

            // face player when attacking
            sp.setAng(NORM_ANGLE(EngineUtils.getAngle(tgtSpr.getX() - sp.getX(), tgtSpr.getY() - sp.getY())));

            // If it's your own kind, lay off!
            if (tu != null && u.ID == tu.ID && tu.PlayerP == -1) {
                InitActorRunAway(SpriteNum);
                return;
            }
        }

        // Hari Kari for Ninja's
        if (u.ActorActionSet.Death2 != null) {
            if (u.Health < 38) {
                if (CHOOSE2(100)) {
                    u.ActorActionFunc = DoActorDecide;
                    NewStateGroup(SpriteNum, u.ActorActionSet.Death2);
                    return;
                }
            }
        }

        (u.ActorActionFunc).animatorInvoke(SpriteNum);
    }

    public static void InitActorEvade(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // Evade is same thing as run away except when you get to the end of the
        // track
        // you stop and take up the fight again.

        u.ActorActionFunc = DoActorDecide;
        NewStateGroup(SpriteNum, u.ActorActionSet.Run);

        u.track = FindTrackAwayFromPlayer(SpriteNum);

        if (u.track >= 0) {
            sp.setAng(NORM_ANGLE(EngineUtils.getAngle((Track[u.track].TrackPoint[u.point]).x - sp.getX(), (Track[u.track].TrackPoint[u.point]).y - sp.getY())));
            DoActorSetSpeed(SpriteNum, FAST_SPEED);
            // NOT doing a RUN_AWAY
            u.Flags &= ~(SPR_RUN_AWAY);
        }
    }


    public static void DoActorMoveCloser(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int nx = sp.getXvel() * EngineUtils.cos(NORM_ANGLE(sp.getAng())) >> 14;
        int ny = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng())) >> 14;

        // if cannot move the sprite
        if (!move_actor(SpriteNum, nx, ny, 0)) {
            if (ActorMoveHitReact(SpriteNum)) {
                return;
            }

            DoActorCantMoveCloser(SpriteNum);
            return;
        }

        // Do a noise if ok
        DoActorNoise(ChooseAction(u.Personality.Broadcast), SpriteNum);

        // after moving a ways check to see if player is still in sight
        if (u.DistCheck > 550) {
            u.DistCheck = 0;

            // If player moved out of sight
            if (!CanSeePlayer(SpriteNum)) {
                // stay put and choose another option
                InitActorDecide(SpriteNum);
                return;
            } else {
                Sprite ts = boardService.getSprite(u.tgt_sp);
                if (ts != null) {
                    // turn to face player
                    sp.setAng(EngineUtils.getAngle(ts.getX() - sp.getX(), ts.getY() - sp.getY()));
                }
            }
        }

        // Should be a random value test
        if (u.Dist > 512 * 3) {
            InitActorDecide(SpriteNum);
        }
    }

    /*
     * !AIC - Find tracks of different types. Toward target, away from target, etc.
     */

    private static int move_scan(int SpriteNum, int ang, int dist, LONGp stopx, LONGp stopy) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return 0;
        }

        // moves out a bit but keeps the sprites original postion/sector.

        // save off position info
        int x = sp.getX();
        int y = sp.getY();
        int z = sp.getZ();
        int sang = sp.getAng();
        int loz = u.loz;
        int hiz = u.hiz;
        int lo_sp = u.lo_sp;
        int hi_sp = u.hi_sp;
        int lo_sectp = u.lo_sectp;
        int hi_sectp = u.hi_sectp;
        int ss = sp.getSectnum();

        // do the move
        sp.setAng(NORM_ANGLE(ang));
        int nx = (dist) * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        int ny = (dist) * EngineUtils.sin(sp.getAng()) >> 14;

        int ret = move_sprite(SpriteNum, nx, ny, 0, u.ceiling_dist, u.floor_dist, CLIPMASK_ACTOR, 1);
        // move_sprite DOES do a getzrange point?

        // should I look down with a FAFgetzrange to see where I am?

        // remember where it stopped
        stopx.value = sp.getX();
        stopy.value = sp.getY();

        // reset position information
        sp.setX(x);
        sp.setY(y);
        sp.setZ(z);
        sp.setAng(sang);
        u.loz = loz;
        u.hiz = hiz;
        u.lo_sp = lo_sp;
        u.hi_sp = hi_sp;
        u.lo_sectp = lo_sectp;
        u.hi_sectp = hi_sectp;
        engine.changespritesect(SpriteNum, ss);

        return (ret);
    }

    private static int FindNewAngle(int SpriteNum, int dir, int DistToMove) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return 0;
        }

        // if on fire, run shorter distances
        if (ActorFlaming(SpriteNum)) {
            DistToMove = DIV4(DistToMove) + DIV8(DistToMove);
        }

        int oang = sp.getAng();
        Sprite ts = boardService.getSprite(u.tgt_sp);
        if (ts != null) {
            // Find angle to from the player
            oang = NORM_ANGLE(EngineUtils.getAngle(ts.getX() - sp.getX(), ts.getY() - sp.getY()));
        }

        short[] adp = null;
        // choose a random angle array
        switch (dir) {
            case TOWARD:
                adp = toward_angle_delta[RANDOM_P2(4 << 8) >> 8];
                break;
            case AWAY:
                if (CanHitPlayer(SpriteNum)) {
                    adp = toward_angle_delta[RANDOM_P2(4 << 8) >> 8];
                } else {
                    adp = away_angle_delta[RANDOM_P2(4 << 8) >> 8];
                }
                break;
        }

        if (adp == null) {
            return 0;
        }

        int save_ang = -1;
        // start out with mininum distance that will be accepted as a move
        int save_dist = 500;

        for (int ptr = 0; adp[ptr] != -99; ptr++) {
            int new_ang = NORM_ANGLE(oang + adp[ptr]);

            // look directly ahead for a ledge
            if (!TEST(u.Flags, SPR_NO_SCAREDZ | SPR_JUMPING | SPR_FALLING | SPR_SWIMMING | SPR_DEAD)) {
                sp.setAng(new_ang);
                boolean isDropAhead = DropAhead(SpriteNum, u.lo_step);
                sp.setAng(oang);
                if (isDropAhead) {
                    continue;
                }
            }

            // check to see how far we can move
            int ret = move_scan(SpriteNum, new_ang, DistToMove, tmp_ptr[0], tmp_ptr[1]);

            int stopx = tmp_ptr[0].value;
            int stopy = tmp_ptr[1].value;

            if (ret == 0) {
                // cleanly moved in new direction without hitting something
                u.TargetDist =  Distance(sp.getX(), sp.getY(), stopx, stopy);
                return (new_ang);
            } else {
                // hit something
                int dist = Distance(sp.getX(), sp.getY(), stopx, stopy);

                if (dist > save_dist) {
                    save_ang = new_ang;
                    save_dist = dist;
                }
            }
        }

        if (save_ang != -1) {
            u.TargetDist =  save_dist;

            // If actor moved to the TargetDist it would look like he was running
            // into things.

            // To keep this from happening make the TargetDist is less than the
            // point you would hit something

            if (u.TargetDist > 4000) {
                u.TargetDist -= 3500;
            }

            sp.setAng(save_ang);
            return (save_ang);
        }

        return (-1);
    }

    public static void InitActorPause(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.ActorActionFunc = DoActorPause;
        u.ActorActionFunc.animatorInvoke(SpriteNum);
    }

    public static void AiSaveable() {
        SaveData(InitActorDecide);
        SaveData(DoActorDecide);
        SaveData(InitActorAlertNoise);
        SaveData(InitActorAmbientNoise);
        SaveData(InitActorAttackNoise);
        SaveData(InitActorPainNoise);
        SaveData(InitActorDieNoise);
        SaveData(InitActorExtra1Noise);
        SaveData(InitActorExtra2Noise);
        SaveData(InitActorExtra3Noise);
        SaveData(InitActorExtra4Noise);
        SaveData(InitActorExtra5Noise);
        SaveData(InitActorExtra6Noise);
        SaveData(InitActorMoveCloser);
        SaveData(DoActorMoveCloser);
        SaveData(InitActorRunAway);
        SaveData(InitActorRunToward);
        SaveData(InitActorAttack);
        SaveData(DoActorAttack);
        SaveData(InitActorEvade);
        SaveData(InitActorWanderAround);
        SaveData(InitActorFindPlayer);
        SaveData(InitActorDuck);
        SaveData(DoActorDuck);
        SaveData(DoActorMoveJump);
        SaveData(InitActorReposition);
        SaveData(DoActorReposition);
        SaveData(DoActorPause);
    }

    public enum Attrib_Snds {
        attr_ambient, attr_alert, attr_attack, attr_pain, attr_die, attr_extra1, attr_extra2, attr_extra3, attr_extra4, attr_extra5, attr_extra6
    }

    /*
     * !AIC - Where actors do their attacks. There is some special case code
     * throughout these. Both close and long range attacks are handled here by
     * transitioning to the correct attack state.
     */


    public static void DoActorAttack(int SpriteNum) {
        USER u = getUser(SpriteNum), pu;
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        DoActorNoise(ChooseAction(u.Personality.Broadcast), SpriteNum);

        int dist = Integer.MAX_VALUE;
        Sprite ts = boardService.getSprite(u.tgt_sp);
        if (ts != null) {
            dist = DISTANCE(sp.getX(), sp.getY(), ts.getX(), ts.getY());
        }

        pu = getUser(GetPlayerSpriteNum(SpriteNum));
        if (ts != null && u.ActorActionSet.CloseAttack[0] != null && dist < CloseRangeDist(sp, ts) || (pu != null && pu.WeaponNum == WPN_FIST)) {
            int rand_num = ChooseActionNumber(u.ActorActionSet.CloseAttackPercent);

            NewStateGroup(SpriteNum, u.ActorActionSet.CloseAttack[rand_num]);
        } else {
            int rand_num = ChooseActionNumber(u.ActorActionSet.AttackPercent);

            NewStateGroup(SpriteNum, u.ActorActionSet.Attack[rand_num]);
            u.ActorActionFunc = DoActorDecide;
        }

    }


    public static void InitActorWanderAround(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.ActorActionFunc = DoActorDecide;
        NewStateGroup(SpriteNum, u.ActorActionSet.Run);

        DoActorPickClosePlayer(SpriteNum);
        u.track = FindWanderTrack(SpriteNum);
        if (u.track >= 0) {
            sp.setAng(EngineUtils.getAngle((Track[u.track].TrackPoint[u.point]).x - sp.getX(), (Track[u.track].TrackPoint[u.point]).y - sp.getY()));
            DoActorSetSpeed(SpriteNum, NORM_SPEED);
        }
    }


    public static void InitActorFindPlayer(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.ActorActionFunc = DoActorDecide;
        NewStateGroup(SpriteNum, u.ActorActionSet.Run);

        u.track =  FindTrackToPlayer(SpriteNum);
        if (u.track >= 0) {
            sp.setAng(EngineUtils.getAngle((Track[u.track].TrackPoint[u.point]).x - sp.getX(), (Track[u.track].TrackPoint[u.point]).y - sp.getY()));
            DoActorSetSpeed(SpriteNum, MID_SPEED);
            u.Flags |= (SPR_FIND_PLAYER);

            u.ActorActionFunc = DoActorDecide;
            NewStateGroup(SpriteNum, u.ActorActionSet.Run);
        } else {
            InitActorReposition.animatorInvoke(SpriteNum);
        }
    }


    public static void InitActorDuck(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (u.ActorActionSet.Duck == null) {
            u.ActorActionFunc = DoActorDecide;
            return;
        }

        u.ActorActionFunc = DoActorDuck;
        NewStateGroup(SpriteNum, u.ActorActionSet.Duck);
        Sprite ts = boardService.getSprite(u.tgt_sp);

        if (ts != null) {
            int dist = Distance(sp.getX(), sp.getY(), ts.getX(), ts.getY());
            if (dist > 8000) {
                u.WaitTics = 190;
            } else {
                u.WaitTics = 60;
            }
        }

        u.ActorActionFunc.animatorInvoke(SpriteNum);

    }


    public static void DoActorDuck(int SpriteNum) {
        USER u = getUser(SpriteNum);

        if (u != null && (u.WaitTics -= ACTORMOVETICS) < 0) {
            NewStateGroup(SpriteNum, u.ActorActionSet.Rise);
            u.ActorActionFunc = DoActorDecide;
            // InitActorDecide(SpriteNum);
            u.Flags &= ~(SPR_TARGETED);
        }
    }


    public static void DoActorMoveJump(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // Move while jumping

        int nx = sp.getXvel() * EngineUtils.cos(NORM_ANGLE(sp.getAng())) >> 14;
        int ny = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng())) >> 14;

        move_actor(SpriteNum, nx, ny, 0);

        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            InitActorDecide(SpriteNum);
        }
    }

    /*
     *
     * !AIC KEY - Reposition code is called throughout this file. What this does is
     * pick a new direction close to the target direction (or away from the target
     * direction if running away) and a distance to move in and tries to move there
     * with move_scan(). If it hits something it will try again. No movement is
     * actually acomplished here. This is just testing for clear paths to move in.
     * Location variables that are changed are saved and reset. FindNewAngle() and
     * move_scan() are two routines (above) that go with this. This is definately
     * not called every time through the loop. It would be majorly slow.
     *
     */


    public static void InitActorReposition(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.Dist = 0;
        int rnum = RANDOM_P2(8 << 8) >> 8;
        Sprite ts = boardService.getSprite(u.tgt_sp);

        int dist = Integer.MAX_VALUE;
        if (ts != null) {
            dist = Distance(sp.getX(), sp.getY(), ts.getX(), ts.getY());
        }

        if (dist < PlayerDist[rnum] || TEST(u.Flags, SPR_RUN_AWAY)) {
            rnum = RANDOM_P2(8 << 8) >> 8;
            int ang =  FindNewAngle(SpriteNum, AWAY, AwayDist[rnum]);
            if (ang == -1) {
                u.Vis = 8;
                InitActorPause(SpriteNum);
                return;
            }

            sp.setAng(ang);
            DoActorSetSpeed(SpriteNum, FAST_SPEED);
            u.Flags &= ~(SPR_RUN_AWAY);
        } else {
            // try to move toward player
            rnum = RANDOM_P2(8 << 8) >> 8;
            int ang = FindNewAngle(SpriteNum, TOWARD, TowardDist[rnum]);
            if (ang == -1) {
                // try to move away from player
                rnum = RANDOM_P2(8 << 8) >> 8;
                ang =  FindNewAngle(SpriteNum, AWAY, AwayDist[rnum]);
                if (ang == -1) {
                    u.Vis = 8;
                    InitActorPause(SpriteNum);
                    return;
                }
            } else {
                // pick random speed to move toward the player
                if (RANDOM_P2(1024) < 512) {
                    DoActorSetSpeed(SpriteNum, NORM_SPEED);
                } else {
                    DoActorSetSpeed(SpriteNum, MID_SPEED);
                }
            }

            sp.setAng(ang);
        }

        u.ActorActionFunc = DoActorReposition;
        if (!TEST(u.Flags, SPR_SWIMMING)) {
            NewStateGroup(SpriteNum, u.ActorActionSet.Run);
        }

        (u.ActorActionFunc).animatorInvoke(SpriteNum);
    }

    public static void DoActorReposition(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        int ny = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng())) >> 14;

        // still might hit something and have to handle it.
        if (!move_actor(SpriteNum, nx, ny, 0)) {
            if (ActorMoveHitReact(SpriteNum)) {
                return;
            }

            u.Vis = 6;
            InitActorPause(SpriteNum);
            return;
        }

        // if close to target distance do a Decision again
        if (u.TargetDist < 50) {
            InitActorDecide(SpriteNum);
        }
    }

    public static void DoActorPause(int SpriteNum) {
        USER u = getUser(SpriteNum);

        // Using Vis instead of WaitTics, var name sucks, but it's the same type
        // WaitTics is used by too much other actor code and causes problems here
        if (u != null && (u.Vis -= ACTORMOVETICS) < 0) {
            u.ActorActionFunc = DoActorDecide;
            u.Flags &= ~(SPR_TARGETED);
        }
    }
}
