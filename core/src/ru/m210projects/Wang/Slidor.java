package ru.m210projects.Wang;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Sound.SoundType;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.STAT_SLIDOR;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Sprites.KillSprite;
import static ru.m210projects.Wang.Stag.SECT_LOCK_DOOR;
import static ru.m210projects.Wang.Stag.SECT_SLIDOR;
import static ru.m210projects.Wang.Tags.*;
import static ru.m210projects.Wang.Text.KeyDoorMessage;
import static ru.m210projects.Wang.Text.PutStringInfo;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Vator.VatorSwitch;

public class Slidor {

    public static final Animator DoSlidor = new Animator((Animator.Runnable) Slidor::DoSlidor);

    public static void ReverseSlidor(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        RotatorStr r = u.getRotator();
        // if paused go ahead and start it up again
        if (u.Tics != 0) {
            u.Tics = 0;
            SetSlidorActive(SpriteNum);
            return;
        }

        // moving toward to OFF pos
        if (r.tgt == 0) {
            r.tgt = r.open_dest;
        } else if (r.tgt == r.open_dest) {
            r.tgt = 0;
        }

        r.vel = -r.vel;
    }

    public static void SetSlidorActive(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }
        RotatorStr r = u.getRotator();

        // play activate sound
        DoSoundSpotMatch(SP_TAG2(sp), 1, SoundType.SOUND_OBJECT_TYPE);

        u.Flags |= (SPR_ACTIVE);
        u.Tics = 0;

        // moving to the OFF position
        if (r.tgt == 0) {
            VatorSwitch(SP_TAG2(sp), OFF);
        } else {
            VatorSwitch(SP_TAG2(sp), ON);
        }
    }

    public static void SetSlidorInactive(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // play inactivate sound
        DoSoundSpotMatch(SP_TAG2(sp), 2, SoundType.SOUND_OBJECT_TYPE);

        u.Flags &= ~(SPR_ACTIVE);
    }

    // called for operation from the space bar
    public static void DoSlidorOperate(PlayerStr pp, int sectnum) {
        ru.m210projects.Build.Types.Sector sec = boardService.getSector(sectnum);
        if (sec != null) {
            int match = sec.getHitag();
            if (match > 0) {
                if (!TestSlidorMatchActive(match)) {
                    DoSlidorMatch(pp, match, true);
                }
            }
        }
    }

    // called from switches and triggers
    // returns first vator found
    public static void DoSlidorMatch(PlayerStr pp, int match, boolean manual) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SLIDOR); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite fsp = node.get();

            if (SP_TAG1(fsp) == SECT_SLIDOR && SP_TAG2(fsp) == match) {
                USER fu = getUser(i);

                // single play only vator
                // boolean 8 must be set for message to display
                if (TEST_BOOL4(fsp) && (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT
                        || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_AI_BOTS)) {
                    if (pp != null && TEST_BOOL11(fsp)) {
                        PutStringInfo(pp, "This only opens in single play.");
                    }
                    continue;
                }

                // switch trigger only
                if (SP_TAG3(fsp) == 1) {
                    // tried to manually operat a switch/trigger only
                    if (manual) {
                        continue;
                    }
                }

                int sectnum = fsp.getSectnum();
                Sect_User su = getSectUser(sectnum);

                if (pp != null && su != null && su.stag == SECT_LOCK_DOOR
                        && su.number != 0) {
                    int key_num;

                    key_num = su.number;

                    PutStringInfo(pp, KeyDoorMessage[key_num - 1]);
                    return;

                }

                if (fu != null && TEST(fu.Flags, SPR_ACTIVE)) {
                    ReverseSlidor(i);
                    continue;
                }

                SetSlidorActive(i);
            }
        }

    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean TestSlidorMatchActive(int match) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_SLIDOR); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite fsp = node.get();

            if (SP_TAG1(fsp) == SECT_SLIDOR && SP_TAG2(fsp) == match) {
                USER fu = getUser(i);

                // Does not have to be inactive to be operated
                if (TEST_BOOL6(fsp)) {
                    continue;
                }

                if (fu != null && (TEST(fu.Flags, SPR_ACTIVE) || fu.Tics != 0)) {
                    return (true);
                }
            }
        }

        return (false);
    }

    public static void DoSlidorMoveWalls(int SpriteNum, int amt) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        ru.m210projects.Build.Types.Sector sec = boardService.getSector(sp.getSectnum());
        if (sec == null) {
            return;
        }

        int startwall = sec.getWallptr();
        int endwall = startwall + sec.getWallnum() - 1;

        int pw;
        int w = startwall;
        do {
            Wall wall = boardService.getWall(w);
            if (wall == null) {
                return;
            }

            // prev wall
            pw = w - 1;
            if (w < startwall) {
                pw = endwall;
            }
            Wall pwall = boardService.getWall(pw);

            switch (wall.getLotag()) {
                case TAG_WALL_SLIDOR_LEFT:
                    if (pwall != null && wall.getNextwall() == -1) {
                        // white wall - move 4 points
                        engine.dragpoint(w, wall.getX() - amt, wall.getY());
                        engine.dragpoint(pw, pwall.getX() - amt, pwall.getY());
                        engine.dragpoint(wall.getPoint2(), wall.getWall2().getX() - amt, wall.getWall2().getY());
                        engine.dragpoint(wall.getWall2().getPoint2(), wall.getWall2().getWall2().getX() - amt,
                                wall.getWall2().getWall2().getY());
                    } else {
                        // red wall - move 2 points
                        engine.dragpoint(w, wall.getX() - amt, wall.getY());
                        engine.dragpoint(wall.getPoint2(), wall.getWall2().getX() - amt, wall.getWall2().getY());
                    }
                    break;

                case TAG_WALL_SLIDOR_RIGHT:
                    if (pwall != null && wall.getNextwall() == -1) {
                        // white wall - move 4 points
                        engine.dragpoint(w, wall.getX() + amt, wall.getY());
                        engine.dragpoint( pw, pwall.getX() + amt, pwall.getY());
                        engine.dragpoint(wall.getPoint2(), wall.getWall2().getX() + amt, wall.getWall2().getY());
                        engine.dragpoint(wall.getWall2().getPoint2(), wall.getWall2().getWall2().getX() + amt,
                                wall.getWall2().getWall2().getY());
                    } else {
                        // red wall - move 2 points
                        engine.dragpoint(w, wall.getX() + amt, wall.getY());
                        engine.dragpoint(wall.getPoint2(), wall.getWall2().getX() + amt, wall.getWall2().getY());
                    }

                    break;

                case TAG_WALL_SLIDOR_UP:
                    if (pwall != null && wall.getNextwall() == -1) {
                        engine.dragpoint(w, wall.getX(), wall.getY() - amt);
                        engine.dragpoint( pw, pwall.getX(), pwall.getY() - amt);
                        engine.dragpoint(wall.getPoint2(), wall.getWall2().getX(), wall.getWall2().getY() - amt);
                        engine.dragpoint(wall.getWall2().getPoint2(), wall.getWall2().getWall2().getX(),
                                wall.getWall2().getWall2().getY() - amt);
                    } else {
                        engine.dragpoint(w, wall.getX(), wall.getY() - amt);
                        engine.dragpoint(wall.getPoint2(), wall.getWall2().getX(), wall.getWall2().getY() - amt);
                    }

                    break;

                case TAG_WALL_SLIDOR_DOWN:
                    if (pwall != null && wall.getNextwall() == -1) {
                        engine.dragpoint(w, wall.getX(), wall.getY() + amt);
                        engine.dragpoint( pw, pwall.getX(), pwall.getY() + amt);
                        engine.dragpoint(wall.getPoint2(), wall.getWall2().getX(), wall.getWall2().getY() + amt);
                        engine.dragpoint(wall.getWall2().getPoint2(), wall.getWall2().getWall2().getX(),
                                wall.getWall2().getWall2().getY() + amt);
                    } else {
                        engine.dragpoint(w, wall.getX(), wall.getY() + amt);
                        engine.dragpoint(wall.getPoint2(), wall.getWall2().getX(), wall.getWall2().getY() + amt);
                    }

                    break;
            }

            w = wall.getPoint2();
        } while (w != startwall);

    }

    public static void DoSlidorInstantClose(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        ru.m210projects.Build.Types.Sector sec = boardService.getSector(sp.getSectnum());
        if (sec == null) {
            return;
        }
        int w = sec.getWallptr();
        int startwall = w;

        do {
            Wall wall = boardService.getWall(w);
            if (wall == null) {
                return;
            }

            int diff;
            switch (wall.getLotag()) {
                case TAG_WALL_SLIDOR_LEFT:
                    diff = wall.getX() - sp.getX();
                    DoSlidorMoveWalls(SpriteNum, diff);
                    break;

                case TAG_WALL_SLIDOR_RIGHT:
                    diff = wall.getX() - sp.getX();
                    DoSlidorMoveWalls(SpriteNum, -diff);
                    break;

                case TAG_WALL_SLIDOR_UP:
                    diff = wall.getY() - sp.getY();
                    DoSlidorMoveWalls(SpriteNum, diff);
                    break;

                case TAG_WALL_SLIDOR_DOWN:
                    diff = wall.getY() - sp.getY();
                    DoSlidorMoveWalls(SpriteNum, -diff);
                    break;
            }

            w = wall.getPoint2();
        } while (w != startwall);

    }

    public static void DoSlidorMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        RotatorStr r = u.getRotator();
        boolean kill = false;
        // Example - ang pos moves from 0 to 512 <<OR>> from 0 to -512
        int old_pos = r.pos;

        // control SPEED of swinging
        if (r.pos < r.tgt) {
            // Increment swing angle
            r.pos += r.speed;
            r.speed += r.vel;

            // if the other way make it equal
            if (r.pos > r.tgt) {
                r.pos = r.tgt;
            }
        }

        if (r.pos > r.tgt) {
            // Increment swing angle
            r.pos -= r.speed;
            r.speed += r.vel;

            // if the other way make it equal
            if (r.pos < r.tgt) {
                r.pos = r.tgt;
            }
        }

        if (r.pos == r.tgt) {
            // If ang is OPEN
            if (r.pos == r.open_dest) {
                // new tgt is CLOSED (0)
                r.tgt = 0;
                r.vel = -r.vel;
                SetSlidorInactive(SpriteNum);

                if (SP_TAG6(sp) != 0 && !TEST_BOOL8(sp)) {
                    DoMatchEverything(null, SP_TAG6(sp), -1);
                }

                // wait a bit and close it
                if (u.WaitTics != 0) {
                    u.Tics = u.WaitTics;
                }
            } else
                // If ang is CLOSED then
                if (r.pos == 0) {
                    // new tgt is OPEN (open)
                    r.speed = r.orig_speed;
                    r.vel = klabs(r.vel);

                    r.tgt = r.open_dest;
                    SetSlidorInactive(SpriteNum);

                    RESET_BOOL8(sp);

                    if (SP_TAG6(sp) != 0 && TEST_BOOL8(sp)) {
                        DoMatchEverything(null, SP_TAG6(sp), -1);
                    }
                }

            if (TEST_BOOL2(sp)) {
                kill = true;
            }
        } else {
            // if heading for the OFF (original) position and should NOT CRUSH
            if (TEST_BOOL3(sp) && r.tgt == 0) {

                Sprite bsp;
                USER bu;
                boolean found = false;

                ListNode<Sprite> nexti;
                for (ListNode<Sprite> node = boardService.getSectNode(sp.getSectnum()); node != null; node = nexti) {
                    int i = node.getIndex();
                    nexti = node.getNext();
                    bsp = node.get();
                    bu = getUser(i);

                    if (bu != null && TEST(bsp.getCstat(), CSTAT_SPRITE_BLOCK) && TEST(bsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                        // found something blocking so reverse to ON position
                        ReverseSlidor(SpriteNum);
                        SET_BOOL8(sp); // tell vator that something blocking door
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    int pnum;
                    PlayerStr pp;
                    // go ahead and look for players clip box bounds
                    for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                        pp = Player[pnum];

                        if ((boardService.getSector(pp.lo_sectp) != null && pp.lo_sectp == sp.getSectnum())
                                || (pp.hi_sectp != -1 && pp.hi_sectp == sp.getSectnum())) {
                            ReverseSlidor(SpriteNum);

                            u.vel_rate =  -u.vel_rate;
                        }
                    }
                }
            }
        }

        DoSlidorMoveWalls(SpriteNum, r.pos - old_pos);

        if (kill) {
            SetSlidorInactive(SpriteNum);
            KillSprite(SpriteNum);
        }

    }

    public static void DoSlidor(int SpriteNum) {
        DoSlidorMove(SpriteNum);
    }

}
