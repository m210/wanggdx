package ru.m210projects.Wang;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.utils.Pool;
import org.jetbrains.annotations.Nullable;
import ru.m210projects.Build.Architecture.common.audio.BuildAudio;
import ru.m210projects.Build.Architecture.common.audio.SoundData;
import ru.m210projects.Build.Architecture.common.audio.Source;
import ru.m210projects.Build.Script.CueScript;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Wang.Ai.Attrib_Snds;
import ru.m210projects.Wang.Type.*;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Factory.WangNetwork.Prediction;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.STAT_AMBIENT;
import static ru.m210projects.Wang.Rooms.FAFcansee;
import static ru.m210projects.Wang.Sprites.MoveSkip8;
import static ru.m210projects.Wang.Text.PutStringInfo;
import static ru.m210projects.Wang.Type.MyTypes.TEST;

public class Sound {

    public static final int[] TauntAIVocs = {DIGI_TAUNTAI1, DIGI_TAUNTAI2, DIGI_TAUNTAI3, DIGI_TAUNTAI4, DIGI_TAUNTAI5,
            DIGI_TAUNTAI6, DIGI_TAUNTAI7, DIGI_TAUNTAI8, DIGI_TAUNTAI9, DIGI_TAUNTAI10, DIGI_COWABUNGA, DIGI_NOCHARADE,
            DIGI_TIMETODIE, DIGI_EATTHIS, DIGI_FIRECRACKERUPASS, DIGI_HOLYCOW, DIGI_HAHA2, DIGI_HOLYPEICESOFCOW,
            DIGI_HOLYSHIT, DIGI_HOLYPEICESOFSHIT, DIGI_PAYINGATTENTION, DIGI_EVERYBODYDEAD, DIGI_KUNGFU,
            DIGI_HOWYOULIKEMOVE, DIGI_HAHA3, DIGI_NOMESSWITHWANG, DIGI_RAWREVENGE, DIGI_YOULOOKSTUPID, DIGI_TINYDICK,
            DIGI_NOTOURNAMENT, DIGI_WHOWANTSWANG, DIGI_MOVELIKEYAK, DIGI_ALLINREFLEXES};
    public static final int[] PlayerPainVocs = {DIGI_PLAYERPAIN1, DIGI_PLAYERPAIN2, DIGI_PLAYERPAIN3, DIGI_PLAYERPAIN4,
            DIGI_PLAYERPAIN5};
    // 3D sound engine declarations //////////////////////////////////////////////
    // Flag settings used to turn on and off 3d sound options
    public static final int v3df_none = 0; // Default, take no action, use all defaults
    public static final int v3df_follow = 1; // 1 = Do coordinate updates on sound
    // Use this only if the sprite won't be deleted soon
    public static final int v3df_kill = 2; // 1 = Sound is to be deleted
    public static final int v3df_doppler = 4; // 1 = Don't use doppler pitch variance
    public static final int v3df_dontpan = 8; // 1 = Don't do panning of sound
    public static final int v3df_ambient = 16; // 1 = Sound is ambient, use ambient struct info.
    public static final int v3df_intermit = 32; // 1 = Intermittant sound
    public static final int v3df_init = 64; // 1 = First pass of sound, don't play it.
    // This is mainly used for intermittent sounds
    public static final int v3df_nolookup = 128; // don't use ambient table lookup
    public static final int MAXLEVLDIST = 19000; // The higher the number, the further away you can hear sound
    public static final int DECAY_CONST = 4000;
    public static final int MAX_AMBIENT_SOUNDS = 82;
    // Don't have these sounds yet
    public static final int[] PlayerLowHealthPainVocs = {DIGI_HURTBAD1, DIGI_HURTBAD2, DIGI_HURTBAD3, DIGI_HURTBAD4,
            DIGI_HURTBAD5};
    public static final int[] PlayerGetItemVocs = {DIGI_GOTITEM1, DIGI_HAHA1, DIGI_BANZAI, DIGI_COWABUNGA,
            DIGI_TIMETODIE};
    public static final int[] PlayerYellVocs = {DIGI_PLAYERYELL1, DIGI_PLAYERYELL2, DIGI_PLAYERYELL3};

    // My Stuff ////////////////////////////////////////////////////////////////
    // Never play track 1, that's the game.
    public static final int[] RedBookSong = {2, 4, 9, 12, 10, // Title and ShareWare levels
            5, 6, 8, 11, 12, 5, 10, 4, 6, 9, 7, 10, 8, 7, 9, 10, 11, 5, // Registered levels
            11, 8, 7, 13, 5, 6, // Deathmatch levels
            13 // Fight boss
    };
    public static final String[] cdtracks = {"", "track02.ogg", "track03.ogg", "track04.ogg", "track05.ogg",
            "track06.ogg", "track07.ogg", "track08.ogg", "track09.ogg", "track10.ogg", "track11.ogg", "track12.ogg",
            "track13.ogg", "track14.ogg", "track15.ogg"};
    private static final Map<Integer, Entry> cdTrackEntryMap = new HashMap<>();
    private static final Pool<VOC3D> vocList = new Pool<VOC3D>() {
        @Override
        protected VOC3D newObject() {
            return new VOC3D();
        }
    };
    private static final TVOC_Info[] TmpVocArray = new TVOC_Info[32];

    private static final int[] ambrand = {56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67};
    public static char rtsplaying;
    public static Vector3i sndCoords;
    public static int playTrack = -1;
    public static Music currMusic = null;
    public static BuildAudio audio;
    // Global vars used by ambient sounds to set spritenum of ambient sounds for
    // later lookups in
    // the sprite array so FAFcansee can know the sound sprite's current sector
    // location
    private static boolean Use_SoundSpriteNum = false;
    private static int SoundSpriteNum = -1; // Always set this back to -1 for proper validity checking!
    ///////////////////////////////////////////////
    //
    // 3D sound engine
    // Sound management routines that keep a list of
    // all sounds being played in a level.
    // Doppler and Panning effects are achieved here.
    //
    ///////////////////////////////////////////////
    // Declare and initialize linked list of vocs.
    private static VOC3D voc3dstart = null;
    private static VOC3D voc3dend = null;

    public static void SoundStartup() {
        Console.out.println("Initializing sound system");

        cfg.setAudioDriver(cfg.getAudioDriver());
        cfg.setMidiDevice(cfg.getMidiDevice());
        Sound.audio = cfg.getAudio();
        Sound.audio.registerDecoder("VOC", new VOCDecoder());

        for (int i = 0; i < 32; i++) {
            TmpVocArray[i] = new TVOC_Info();
        }

        // #GDX 21.07.2024 When reinit sound driver
        vocList.clear();
        Terminate3DSounds();

        sndCoords = new Vector3i();
    }

    private static int SoundDist(int x, int y, int z, int basedist) {
        double tx, ty, tz;
        double sqrdist, retval;
        double decay, decayshift;

        tx = klabs(Player[screenpeek].posx - x);
        ty = klabs(Player[screenpeek].posy - y);
        tz = klabs((Player[screenpeek].posz - z) >> 4);

        // Use the Pythagreon Theorem to compute the magnitude of a 3D vector
        sqrdist = Math.abs(tx * tx + ty * ty + tz * tz);
        retval = Math.sqrt(sqrdist);

        if (basedist < 0) // if basedist is negative
        {
            int i;

            decayshift = 2;
            decay = (double) klabs(basedist) / DECAY_CONST;

            for (i = 0; i < decay; i++) {
                decayshift *= 2;
            }

            if (Math.abs(basedist / decayshift) >= retval) {
                retval = 0;
            } else {
                retval *= decay;
            }
        } else {
            if (basedist > retval) {
                retval = 0;
            } else {
                retval -= basedist;
            }
        }

        retval = retval * 256 / MAXLEVLDIST;

        if (retval < 0) {
            retval = 0;
        }
        if (retval > 255) {
            retval = 255;
        }

        return (int) (retval);
    }

    public static boolean CacheSound(int num) {
        if (num < 0 || num >= voc.length || cfg.isNoSound()) {
            return false;
        }

        VOC_INFO vp = voc[num];
        // if no data we need to cache it in
        if (vp.data == null) {
            Entry fp = game.cache.getEntry(vp.name, true);

            String extension = fp.getExtension();
            try(InputStream is = fp.getInputStream()) {
                // Some people love provide in with VOC extension files
                switch (StreamUtils.readString(is, 3)) {
                    case "RIF":
                        extension = "WAV";
                        break;
                    case "Ogg":
                        extension = "OGG";
                        break;
                }
            } catch (Exception ignored) {
            }

            SoundData soundData = audio.getSoundDecoder(extension).decode(fp);
            if (soundData != null) {
                vp.data = soundData;
                return (true);
            }

            Console.out.println("Could not open sound " + vp.name + ", num " + num + ", priority " + vp.priority, OsdColor.RED);
            vp.data = new SoundData(8000, 1, 8, ByteBuffer.allocateDirect(0));
            return false;
        }

        return (true);
    }

    public static void COVER_SetReverb(int amt) {
        if (amt != 0) {
            setEcho(true, amt / 255.0f);
        } else {
            setEcho(false, amt);
        }
    }

    public static void PlayerSound(int num, int flags, PlayerStr pp) {
        if (Prediction) {
            return;
        }

        if (pp.pnum < 0 || pp.pnum >= MAX_SW_PLAYERS) {
            throw new AssertException("Player Sound invalid player");
        }

        if (TEST(pp.Flags, PF_DEAD)) {
            return; // You're dead, no talking!
        }

        // If this is a player voice and he's already yacking, forget it.
        VOC_INFO vp = voc[num];
        if (vp == null) {
            Console.out.println("vp == null in PlayerSound, num = " + num, OsdColor.RED);
            return;
        }

        // Not a player voice, bail.
        if (vp.priority != PRI_PLAYERVOICE && vp.priority != PRI_PLAYERDEATH) {
            return;
        }

        // He wasn't talking, but he will be now.
        if (!pp.PlayerTalking) {
            pp.PlayerTalking = true;
            pp.TalkVocnum = num; // Set the voc number
            pp.TalkVocHandle = PlaySound(num, pp, flags); // Play the sound
            if (pp.TalkVocHandle == null) { // if don't have free sources
                pp.PlayerTalking = false;
                pp.TalkVocnum = -1;
            }
        }
    }

    public static void StartAmbientSound() {
        if (!cfg.Ambient) {
            return;
        }

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_AMBIENT); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            VOC3D voc = PlaySound(sp.getLotag(), sp, v3df_ambient | v3df_init | v3df_doppler | v3df_follow);
            // Ambient sounds need this to get sectnum for later processing
            Set3DSoundOwner(node.getIndex(), voc);
        }
    }

    public static void StopSound() {
        CDAudio_Stop();
        StopFX();
    }

    public static void StopFX() {
        stopAllSounds();
    }

    public static void sndHandlePause(boolean gPaused) {
        if (gPaused) {
            CDAudio_Pause();
            StopFX();
            StopAmbientSound();
        } else {
            CDAudio_Continue();
            StartAmbientSound();
        }
    }

    public static void Set3DSoundOwner(int spritenum, VOC3D p) {
        if (p == null) {
            return;
        }

        // Queue up sounds with ambient flag even if they didn't play right away!
        if (p.handle != null || TEST(p.flags, v3df_ambient)) {
            p.owner =  spritenum;
        } else {
            p.deleted = true;
            p.flags = v3df_kill;
        }
    }

    public static void PlaySpriteSound(int spritenum, Attrib_Snds attrib_ndx, int flags) {
        Sprite sp = boardService.getSprite(spritenum);
        USER u = getUser(spritenum);
        if (sp != null && u != null) {
            PlaySound(u.Attrib.getSound(attrib_ndx), sp, flags);
        }
    }

    // NOTE: If v3df_follow == 1, x,y,z are considered literal coordinates
    public static VOC3D PlaySound(int num, Object obj, int flags) {
        if (Prediction || cfg.isNoSound() || num < 0 || num >= voc.length) {
            return null;
        }

        Source voice = null;
        Sprite sp = null;
        // This is used for updating looping sounds in Update3DSounds
        if (Use_SoundSpriteNum && SoundSpriteNum >= 0) {
            sp = boardService.getSprite(SoundSpriteNum);
        }

        if (cfg.Ambient && TEST(flags, v3df_ambient) && !TEST(flags, v3df_nolookup)) // Look for invalid ambient numbers
        {
            if (num > MAX_AMBIENT_SOUNDS) {
                PutStringInfo(Player[screenpeek], "Invalid or out of range ambient sound number " + num);
                return null;
            }
        }

        // Call queue management to add sound to play list.
        // 3D sound manager will update playing sound 10x per second until
        // the sound ends, at which time it is removed from both the 3D
        // sound list as well as the actual cache.
        VOC3D v3p = Insert3DSound();

        // If the ambient flag is set, do a name conversion to point to actual
        // digital sound entry.
        v3p.num = num;
        v3p.priority = 0;

        if (cfg.Ambient && TEST(flags, v3df_ambient) && !TEST(flags, v3df_nolookup)) {
            v3p.maxtics = STD_RANDOM_RANGE(ambarray[num].maxtics);
            flags |= ambarray[num].ambient_flags; // Add to flags if any
            num = ambarray[num].diginame;
        }

        // Assign voc to voc pointer
        VOC_INFO vp = voc[num];
        if (vp == null || vp.lock <= 0) {
            v3p.flags = v3df_kill;
            v3p.handle = null;
            v3p.deleted = true; // Sound init failed, remove it!
            return null;
        }

        v3p.vp = vp;

        // Assign voc info to 3d struct for future reference
        v3p.obj = obj;
        v3p.fx = v3p.getCoords().x;
        v3p.fy = v3p.getCoords().y;
        v3p.fz = v3p.getCoords().z;
        v3p.flags = flags;

        int tx = v3p.fx, ty = v3p.fy, tz = v3p.fz;
        if (((vp.voc_flags & 1) != 0) && Use_SoundSpriteNum && SoundSpriteNum >= 0 && sp != null) {
            tx = sp.getX();
            ty = sp.getY();
            tz = sp.getZ();
        }

        int sound_dist = 255;
        // Calculate sound distance
        if (tx != 0 || ty != 0 || tz != 0) {
            sound_dist =  SoundDist(tx, ty, tz, vp.voc_distance);
        }

        v3p.doplr_delta = sound_dist; // Save of distance for doppler effect

        // Can the ambient sound see the player? If not, tone it down some.
        if ((vp.voc_flags & 1) != 0 && Use_SoundSpriteNum && SoundSpriteNum >= 0) {
            PlayerStr pp = Player[screenpeek];

            if (sp != null && !FAFcansee(tx, ty, tz, sp.getSectnum(), pp.posx, pp.posy, pp.posz, pp.cursectnum)) {
                sound_dist += ((sound_dist / 2) + (sound_dist / 4)); // Play more quietly
                if (sound_dist > 255) {
                    sound_dist = 255;
                }

                // Special Cases
                if (num == DIGI_WHIPME) {
                    sound_dist = 255;
                }
            }
        }

        // Assign ambient priorities based on distance
        // if (gs.Ambient && TEST(flags, v3df_ambient))
        int priority = v3p.priority = vp.priority - (sound_dist / 26);
        if (game.pMenu.gShowMenu && obj == null) { // Menus sound outdo everything
            priority = 100;
        }

        if (!CacheSound(num)) {
            v3p.flags = v3df_kill;
            v3p.handle = null;
            v3p.deleted = true; // Sound init failed, remove it!
            return null;
        }

        int pitch;
        if (vp.pitch_hi == vp.pitch_lo) {
            pitch = vp.pitch_lo;
        } else {
            pitch = vp.pitch_lo + (STD_RANDOM_RANGE(vp.pitch_hi - vp.pitch_lo));
        }

        float volume = 0.0f;
        // Request playback and play it as a looping sound if flag is set.
        if ((vp.voc_flags & 1) != 0) {
            int loopvol;

            if ((loopvol =  (255 - sound_dist)) <= 0) {
                loopvol = 0;
            }

            if (sound_dist < 255 || (flags & v3df_init) != 0) {
                volume = loopvol / 255.0f;
                voice = newSound(vp.data.getData(), vp.data.getRate() + pitch, vp.data.getBits(), priority);
                if (voice != null) {
                    voice.setPosition(tx, tz >> 4, ty);
                }
            }
        } else if (tx == 0 && ty == 0 && tz == 0) {
            // It's a non-inlevel sound
            voice = newSound(vp.data.getData(), vp.data.getRate() + pitch, vp.data.getBits(), priority);
            volume = 1.0f;
        } else {
            // It's a 3d sound
            if (sound_dist < 255) {
                volume = (255 - sound_dist) / 255.0f;
                voice = newSound(vp.data.getData(), vp.data.getRate() + pitch, vp.data.getBits(), priority);
                if (voice != null) {
                    if ((flags & v3df_dontpan) == 0 /* && sound_dist >= 5 */) {// If true, don't do panning
                        voice.setPosition(tx, tz >> 4, ty);
                    }
                }
            }
        }

        // Assign voc info to 3d struct for future reference
        v3p.handle = voice; // Save the current voc handle in struct
        v3p.tics = 0; // Reset tics
        if ((flags & v3df_init) != 0) {
            v3p.flags ^= v3df_init; // Turn init off now
        }

        if (voice != null) {
            if ((vp.voc_flags & 1) != 0) {
                voice.loop(volume);
            } else {
                voice.play(volume);
            }
            voice.setListener(v3p);
            v3p.vp.lock--;
        }
        return (v3p);
    }

    private static float calcPitch(int pitch) {
        float fp = 1.0f;
        fp += pitch / 2000.0f;

        return fp;
    }

    public static void Terminate3DSounds() {

        VOC3D vp = voc3dstart;

        while (vp != null) {
            vp.stop();
            vp.deleted = true;
            vp = vp.next;
        }

        Delete3DSounds(); // Now delete all remaining sounds
    }

    public static void DoUpdateSounds3D() {
        if (game.pMenu.gShowMenu) {
            return;
        }

        int cx = Player[screenpeek].posx;
        int cy = Player[screenpeek].posy;
        int cz = Player[screenpeek].posz;
        int ca = Player[screenpeek].getAnglei();

        audio.setListener(cx, cz >> 4, cy, ca);

        for (int i = 0; i < 32; i++) {
            TmpVocArray[i].p = null;
            TmpVocArray[i].dist = 0;
            TmpVocArray[i].priority = 0;
        }

        VOC3D p = voc3dstart;
        while (p != null) {
            boolean looping = p.vp != null && (p.vp.voc_flags & 1) != 0;

            // If sprite owner is dead, kill this sound as long as it isn't ambient
            if (looping && p.owner == -1 && !TEST(p.flags, v3df_ambient)) {
                p.flags |= (v3df_kill);
            }

            // Is the sound slated for death? Kill it, otherwise play it.
            if ((p.flags & v3df_kill) != 0) {
                p.stop();// Make sure to stop active sounds
                p.deleted = true;
            } else {
                if (!p.isActive() && !looping) {

                    if ((p.flags & v3df_intermit) != 0) {
                        DoTimedSound(p);
                    } else {
                        p.deleted = true;
                    }
                } else if (p.isActive()) {
                    int x, y, z;
                    int dist;
                    if ((p.flags & v3df_follow) != 0) {
                        Vector3i pos = p.getCoords();

                        dist = SoundDist(pos.x, pos.y, pos.z, p.vp.voc_distance);

                        x = pos.x;
                        y = pos.y;
                        z = pos.z;
                    } else {
                        if (p.fx == 0 && p.fy == 0 && p.fz == 0) {
                            dist = 0;
                        } else {
                            dist = SoundDist(p.fx, p.fy, p.fz, p.vp.voc_distance);
                        }

                        x = p.fx;
                        y = p.fy;
                        z = p.fz;
                    }

                    // Can the ambient sound see the player? If not, tone it down some.
                    if ((p.vp.voc_flags & 1) != 0 && p.owner != -1) {
                        PlayerStr pp = Player[screenpeek];
                        Sprite sp = boardService.getSprite(p.owner);

                        if (sp != null && !FAFcansee(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), pp.posx, pp.posy, pp.posz, pp.cursectnum)) {
                            dist += ((dist / 2) + (dist / 4)); // Play more quietly
                            if (dist > 255) {
                                dist = 255;
                            }

                            // Special cases
                            if (p.num == 76 && TEST(p.flags, v3df_ambient)) {
                                dist = 255; // Cut off whipping sound, it's secret
                            }
                        }
                    }

                    if (dist >= 255 && p.vp.voc_distance == DIST_NORMAL) {
                        p.stop(); // Make sure to stop active sounds
                    } else {
                        if (!p.handle.isGlobal()) {
                            p.handle.setVolume((255 - dist) / 255.0f);
                        }

                        // Handle Doppler Effects
                        if ((p.flags & v3df_doppler) == 0 && p.isActive()) {
                            int pitch = getPitch(dist, p);

                            p.doplr_delta =  dist; // Save new distance to struct
                            p.handle.setPitch(calcPitch(pitch));
                        }

                        // Handle Panning Left and Right
                        if ((p.flags & v3df_dontpan) == 0) {
                            if (!p.handle.isGlobal()) // source can be global if v3df_dontpan != 0 or dist < 5
                            {
                                p.handle.setPosition(x, z >> 4, y);
                            }
                        }
                    }
                } else if (!p.isActive() && looping) {
                    int dist;
                    if ((p.flags & v3df_follow) != 0) {
                        Vector3i pos = p.getCoords();
                        dist = SoundDist(pos.x, pos.y, pos.z, p.vp.voc_distance);
                    } else {
                        dist = SoundDist(p.fx, p.fy, p.fz, p.vp.voc_distance);
                    }

                    // Sound was bumped from active sounds list, try to play
                    // again.
                    // Don't bother if voices are already maxed out.
                    // Sort looping vocs in order of priority and distance
                    if (dist <= 255) {
                        for (int i = 0; i < 32; i++) {
                            if (p.priority >= TmpVocArray[i].priority) {
                                if (TmpVocArray[i].p == null || dist < TmpVocArray[i].dist) {
                                    TmpVocArray[i].p = p;
                                    TmpVocArray[i].dist =  dist;
                                    TmpVocArray[i].priority = p.priority;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            p = p.next;
        }

        // Process all the looping sounds that said they wanted to get back in
        // Only update these sounds 5x per second! Woo hoo!, aren't we optimized now?
        for (int i = 0; i < 32; i++) {
            p = TmpVocArray[i].p;
            if (p == null) {
                break;
            }

            // Transfer the owner
            if ((p.flags & v3df_follow) != 0) {
                if (p.owner == -1) {
                    throw new AssertException("Owner == -1 on looping sound with follow flag set!\r\n p.num = " + p.num);
                }

                Use_SoundSpriteNum = true;
                SoundSpriteNum = p.owner;

                PlaySound(p.num, p.obj, p.flags);
            } else {
                if (p.owner == -1) {
                    throw new AssertException("Owner == -1 on looping sound, no follow flag.\r\n p.num = " + p.num);
                }

                Use_SoundSpriteNum = true;
                SoundSpriteNum = p.owner;

                PlaySound(p.num, p, p.flags);
            }
            voc3dend.owner = p.owner; // Transfer the owner
            p.deleted = true;
            Use_SoundSpriteNum = false;
            SoundSpriteNum = -1;
        }
        // } // MoveSkip8

        // Clean out any deleted sounds now
        Delete3DSounds();
    }

    private static int getPitch(int dist, VOC3D p) {
        int pitch = -(dist - p.doplr_delta);
        int pitchmax;

        if (p.vp.pitch_lo != 0 && p.vp.pitch_hi != 0) {
            pitchmax = Math.max(klabs(p.vp.pitch_lo), klabs(p.vp.pitch_hi));
        } else {
            pitchmax = 400;
        }

        if (pitch > pitchmax) {
            pitch = pitchmax;
        }
        if (pitch < -pitchmax) {
            pitch = -pitchmax;
        }
        return pitch;
    }

    private static int RandomizeAmbientSpecials(int handle) {
        // If ambient sound is found in the array, randomly pick a new sound
        for (int j : ambrand) {
            if (handle == j) {
                return (ambrand[STD_RANDOM_RANGE(ambrand.length - 1)]);
            }
        }

        return (handle); // Give back the sound, no new one was found
    }

    private static void DoTimedSound(VOC3D p) {
        if (MoveSkip8 != 0) { // #GDX 21.07.2024
            return;
        }

        p.tics += synctics;

        if (p.tics >= p.maxtics) {
            if (!p.isActive()) {
                // Check for special case ambient sounds
                p.num = RandomizeAmbientSpecials(p.num);

                // Sound was bumped from active sounds list, try to play again.
                // Don't bother if voices are already maxed out.

                // Mark old sound for deletion
                if ((p.flags & v3df_follow) != 0) {
                    PlaySound(p.num, p.obj, p.flags);
                } else {
                    PlaySound(p.num, p, p.flags);
                }
                p.deleted = true; // Mark old sound for deletion
            }

            p.tics = 0;
        }
    }

    public static void StopAmbientSound() {
        VOC3D p = voc3dstart;

        while (p != null) {
            // kill ambient sounds if Ambient is off
            if (TEST(p.flags, v3df_ambient)) {
                p.flags |= (v3df_kill);
            }

            if ((p.flags & v3df_kill) != 0) {
                p.stop(); // Make sure to stop active sounds

                p.deleted = true;
            }

            p = p.next;
        }

        Delete3DSounds();
    }

    //////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////
    // Initialize new vocs in the 3D sound queue
    ///////////////////////////////////////////////
    private static void InitNew3DSound(VOC3D v3p) {
        v3p.handle = null; // Initialize handle to new sound value
        v3p.owner = -1;
        v3p.deleted = false; // Used for when sound gets deleted
    }

    ///////////////////////////////////////////////
    // Inserts new vocs in the 3D sound queue
    ///////////////////////////////////////////////
    private static VOC3D Insert3DSound() {
        VOC3D vp, old;

        // Allocate memory for new sound
        // You can allocate new sounds as long as memory holds out.
        // If you run out of memory for sounds, you got problems anyway.
        vp = vocList.obtain();

        if (voc3dend == null) // First item in list
        {
            vp.next = vp.prev = null;
            voc3dend = vp;
            voc3dstart = vp;
            InitNew3DSound(vp);
            return (vp);
        }

        old = voc3dend; // Put it on the end
        old.next = vp;
        vp.next = null;
        vp.prev = old;
        voc3dend = vp;

        InitNew3DSound(vp);
        return (vp);
    }

    /////////////////////////////////////////////////////
    // Deletes vocs in the 3D sound queue with no owners
    /////////////////////////////////////////////////////
    public static void DeleteNoSoundOwner(int spritenum) {
        VOC3D vp, dp;

        vp = voc3dstart;

        while (vp != null) {
            dp = null;
            if (vp.owner == spritenum && vp.owner >= 0 && (vp.vp.voc_flags & 1) != 0) {
                // Make sure to stop active sounds
                vp.stop();

                dp = vp; // Point to sound to be deleted

                if (vp.prev != null) {
                    vp.prev.next = vp.next;
                } else {
                    voc3dstart = vp.next; // New first item
                    if (voc3dstart != null) {
                        voc3dstart.prev = null;
                    }
                }

                if (vp.next != null) {
                    vp.next.prev = vp.prev; // Middle element
                } else {
                    voc3dend = vp.prev; // Delete last element
                }
            }

            vp = vp.next;

            if (dp != null) {
                vocList.free(dp); // Return memory to heap
            }
        }
    }

    ///////////////////////////////////////////////
    // This is called from KillSprite to kill a follow sound with no valid sprite
    // owner
    // Stops and active sound with the follow bit set, even play once sounds.
    public static void DeleteNoFollowSoundOwner(Sprite sp) {
        VOC3D vp, dp;
        vp = voc3dstart;

        while (vp != null) {
            dp = null;
            // If the follow flag is set, compare the x and y addresses.
            Vector3i pos = vp.getCoords();
            if ((vp.flags & v3df_follow) != 0 && sp == vp.obj && pos.x == sp.getX() && pos.y == sp.getY()) {
                vp.stop();

                dp = vp; // Point to sound to be deleted

                if (vp.prev != null) {
                    vp.prev.next = vp.next;
                } else {
                    voc3dstart = vp.next; // New first item
                    if (voc3dstart != null) {
                        voc3dstart.prev = null;
                    }
                }

                if (vp.next != null) {
                    vp.next.prev = vp.prev; // Middle element
                } else {
                    voc3dend = vp.prev; // Delete last element
                }
            }

            vp = vp.next;

            if (dp != null) {
                vocList.free(dp); // Return memory to heap
            }
        }
    }

    ///////////////////////////////////////////////
    // Deletes vocs in the 3D sound queue
    ///////////////////////////////////////////////
    private static void Delete3DSounds() {
        VOC3D vp, dp;
        PlayerStr pp;

        vp = voc3dstart;

        while (vp != null) {
            dp = null;
            if (vp.deleted) {
                vp.stop(); // Make sure call callback before it deletes the source
                // Reset Player talking flag if a voice was deleted
                if (vp.vp != null && (vp.vp.priority == PRI_PLAYERVOICE || vp.vp.priority == PRI_PLAYERDEATH)) {
                    for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                        pp = Player[pnum];

                        if (vp.num == pp.TalkVocnum) {
                            pp.PlayerTalking = false;
                            pp.TalkVocnum = -1;
                            pp.TalkVocHandle = null;
                        }
                    }
                }

                dp = vp; // Point to sound to be deleted
                if (vp.prev != null) {
                    vp.prev.next = vp.next;
                } else {
                    voc3dstart = vp.next; // New first item
                    if (voc3dstart != null) {
                        voc3dstart.prev = null;
                    }
                }

                if (vp.next != null) {
                    vp.next.prev = vp.prev; // Middle element
                } else {
                    voc3dend = vp.prev; // Delete last element
                }
            }

            vp = vp.next;

            if (dp != null) {
                vocList.free(dp); // Return memory to heap
            }
        }
    }

    public static boolean PlaySoundRTS(int rts_num) {
        if (cfg.isNoSound() || RTS_File == null || RTS_File.getSize() <= 0
                || rtsplaying != 0) {
            return false;
        }

        Entry entry = RTS_File.getEntry(rts_num);
        if (entry.exists()) {
            SoundData data = Sound.audio.getSoundDecoder("VOC").decode(entry);
            if (data != null) {
                Source voice = Sound.newSound(data.getData(), data.getRate(), data.getBits(), 255);
                if (voice != null) {
                    voice.play(1.0f);
                    rtsplaying = 16;
                    return true;
                }
            }
        }

        return false;
    }

    public static void InitCDtracks() {
        CueScript cueScript;

        cdTrackEntryMap.clear();
        Directory gameDir = game.getCache().getGameDirectory();

        // search cue scripts at first
        for (Entry file : gameDir.stream().filter(e -> e.isExtension("cue")).collect(Collectors.toList())) {
            cueScript = new CueScript(file.getName(), file);
            int trackNum = 0;
            for (String track : cueScript.getTracks()) {
                Entry entry = game.getCache().getEntry(track, true);
                if (entry.exists()) {
                    cdTrackEntryMap.put(trackNum, entry);
                }
                trackNum++;
            }

            if (!cdTrackEntryMap.isEmpty()) {
                break;
            }
        }

        // if not found try to find in music directory
        if (cdTrackEntryMap.isEmpty()) {
            Entry dirEntry = game.cache.getEntry(FileUtils.getPath("music"), true);
            if (!dirEntry.exists()) {
                dirEntry = game.cache.getEntry(FileUtils.getPath("classic", "music"), true);
            }

            int trackNum = 0;
            Group group = game.getCache().newGroup(dirEntry);
            for (String cdTrack : cdtracks) {
                Entry entry = group.getEntry(cdTrack);
                if (entry.exists()) {
                    cdTrackEntryMap.put(trackNum, entry);
                }
                trackNum++;
            }
        }

        if (!cdTrackEntryMap.isEmpty()) {
            Console.out.println(cdTrackEntryMap.size() + " cd tracks found...", OsdColor.YELLOW);
        } else {
            Console.out.println("Cd tracks not found.", OsdColor.YELLOW);
        }
    }

    public static void StartMusic() {
        if (game.isCurrentScreen(gMenuScreen)) {
            CDAudio_Play(RedBookSong[0], true);
        } else {
            if (mUserFlag == UserFlag.UserMap) {
                CDAudio_Play(RedBookSong[4 + STD_RANDOM_RANGE(10)], true); // track, loop - Level songs are looped
            } else {
                CDAudio_Play(RedBookSong[Level], true);
            }
        }
    }

    public static void CDAudio_Pause() {
        if (currMusic != null) {
            currMusic.pause();
        }
    }

    public static void CDAudio_Continue() {
        if (!cfg.isMuteMusic() && currMusic != null) {
            currMusic.play();
        }
    }

    public static boolean CDAudio_Play(int track, boolean looping) {
        if ( /* cfg.musicType == 0 || */ track < 0 || cfg.isMuteMusic()) {
            return false;
        }

        if (CDAudio_Playing() && playTrack == track) {
            return true;
        }

        Music mus;
        CDAudio_Stop();
        if (cdTrackEntryMap.get(track - 1) != null
                && (mus = newMusic(cdTrackEntryMap.get(track - 1))) != null) {
            currMusic = mus;
            playTrack = track;
            currMusic.setLooping(looping);
            currMusic.play();
            return true;
        }

        return false;
    }

    public static void CDAudio_Stop() {
        if (currMusic != null) {
            currMusic.stop();
        }

        playTrack = -1;
        currMusic = null;
    }

    public static boolean CDAudio_Playing() {
        return currMusic != null && currMusic.isPlaying();
    }

    @Nullable
    public static Source newSound(ByteBuffer buffer, int rate, int bits, int priority) {
        return (Source) audio.newSound(buffer, rate, bits, priority);
    }

    public static Music newMusic(Entry entry) {
        return audio.newMusic(entry);
    }

    public static void stopAllSounds() {
        audio.stopAllSounds();
        Terminate3DSounds();
    }

    public static void setEcho(boolean enable, float delay) {
        audio.setEcho(enable, delay);
    }

    public enum SoundType {
        SOUND_OBJECT_TYPE, SOUND_EVERYTHING_TYPE
    }

    private static class TVOC_Info {
        public VOC3D p;
        public int dist;
        public int priority;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TVOC_Info tvocInfo = (TVOC_Info) o;

            boolean equals = dist == tvocInfo.dist && priority == tvocInfo.priority;
            if (!equals) {
                return false;
            }

            if (p != null && tvocInfo.p != null) {
                return p.owner == tvocInfo.p.owner && p.num == tvocInfo.p.num;
            }

            return Objects.equals(p, tvocInfo.p);
        }

        @Override
        public int hashCode() {
            if (p == null) {
                return Objects.hash(dist, priority);
            }

            return Objects.hash(p.owner, p.num, dist, priority);
        }
    }

}
