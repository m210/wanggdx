package ru.m210projects.Wang.Fonts;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Names.MINIFONT;

public class MiniFont extends Font {

    public MiniFont(Engine engine) {
        int nTile = MINIFONT;
        this.size = engine.getTile(MINIFONT).getHeight() + 2;

        this.addCharInfo(' ', new CharInfo(this, -1, 1.0f, 5));
        for (int i = 0; i < 64; i++) {
            char symbol = (char) (i + '!');
            int w = engine.getTile(nTile + i).getWidth();
            if (w != 0) {
                this.addChar(symbol, nTile + i, w + 1);
            }
        }
        for (int i = 64; i < 90; i++) {
            char symbol = (char) (i + '!');
            int w = engine.getTile(nTile + i - 32).getWidth();
            if (w != 0) {
                this.addChar(symbol, nTile + i - 32, w + 1);
            }
        }
        for (int i = 90; i < 95; i++) {
            char symbol = (char) (i + '!');
            int w = engine.getTile(nTile + i).getWidth();
            if (w != 0) {
                this.addChar(symbol, nTile + i, w + 1);
            }
        }
    }

    protected void addChar(char ch, int nTile, int width) {
        this.addCharInfo(ch, new CharInfo(this, nTile, 1.0f, width, width, 0, 0) {
            @Override
            public int getWidth() {
                return engine.getTile(nTile).getWidth() + 1;
            }

            @Override
            public int getHeight() {
                return engine.getTile(nTile).getHeight() + 1;
            }
        });
    }

}
