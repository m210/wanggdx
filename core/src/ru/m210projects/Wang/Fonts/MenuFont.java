package ru.m210projects.Wang.Fonts;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.Wang.Names.FONT_LARGE_ALPHA;
import static ru.m210projects.Wang.Names.FONT_LARGE_DIGIT;

public class MenuFont extends Font {

    public MenuFont(Engine engine) {
        this.size = engine.getTile(FONT_LARGE_ALPHA).getHeight() + 2;

        this.addCharInfo(' ', new CharInfo(this, -1, 1.0f, 10));
        for (int i = 0; i < 26; i++) {
            int nTile = i + FONT_LARGE_ALPHA;
            int w = engine.getTile(nTile).getWidth();
            addChar((char) ('A' + i), nTile, w);
            addChar((char) ('a' + i), nTile, w);
        }

        for (int i = 0; i < 10; i++) {
            int nTile = i + FONT_LARGE_DIGIT;
            int w = engine.getTile(nTile).getWidth();
            addChar((char) ('0' + i), nTile, w);
        }

        addChar('\'', 9230, 6); //I added a font's width manually because this font called before def load
    }

    protected void addChar(char ch, int nTile, int width) {
        this.addCharInfo(ch, new CharInfo(this, nTile, 1.0f, width, width, 0, 0));
    }
}
