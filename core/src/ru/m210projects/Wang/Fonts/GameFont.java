// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Wang.Fonts;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;

import static ru.m210projects.Build.Strhandler.isdigit;
import static ru.m210projects.Wang.Names.STARTALPHANUM;

public class GameFont extends Font {

    public GameFont(Engine engine) {
        int nTile = STARTALPHANUM;
        this.size = engine.getTile(nTile).getHeight() + 2;

        this.addCharInfo(' ', new CharInfo(this, -1, 1.0f, 8));
        for (int i = 0; i < 95; i++) {
            ArtEntry pic = engine.getTile(nTile + i);
            if (pic.getWidth() != 0) {
                char symbol = (char) (i + '!');
                this.addCharInfo(symbol, new CharInfo(this, nTile + i, 1.0f, isdigit(symbol) ? 8 : pic.getWidth()) {
                    @Override
                    public int getWidth() {
                        return pic.getWidth();
                    }

                    @Override
                    public int getHeight() {
                        return pic.getHeight();
                    }
                });
            }
        }
    }
}
