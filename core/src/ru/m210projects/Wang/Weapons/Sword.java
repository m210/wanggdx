package ru.m210projects.Wang.Weapons;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Wang.Break.HitBreakSprite;
import static ru.m210projects.Wang.Break.HitBreakWall;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Factory.WangNetwork.Prediction;
import static ru.m210projects.Wang.Game.Distance;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.LUMINOUS;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.MiscActr.s_TrashCanPain;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.PALETTE_DEFAULT;
import static ru.m210projects.Wang.Panel.*;
import static ru.m210projects.Wang.Player.DoPlayerSpriteThrow;
import static ru.m210projects.Wang.Rooms.FAFcansee;
import static ru.m210projects.Wang.Rooms.FAFhitscan;
import static ru.m210projects.Wang.Sector.ShootableSwitch;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Tags.TAG_SPRITE_HIT_MATCH;
import static ru.m210projects.Wang.Tags.TAG_WALL_BREAK;
import static ru.m210projects.Wang.Type.MyTypes.BIT;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Weapon.*;

public class Sword {

    public static final int SWORD_REST = 2080;
    public static final int SWORD_SWING0 = 2081;
    public static final int SWORD_SWING1 = 2082;
    public static final int SWORD_SWING2 = 2083;

    public static final int BLOODYSWORD_REST = 4090;
    public static final int BLOODYSWORD_SWING0 = 4091;
    public static final int BLOODYSWORD_SWING1 = 4092;
    public static final int BLOODYSWORD_SWING2 = 4093;

    private static final int SWORD_PAUSE_TICS = 10;
    private static final int SWORD_SLIDE_TICS = 10;
    private static final int SWORD_MID_SLIDE_TICS = 14;

    private static final int Sword_BEAT_RATE = 24;
    private static final int SWORD_SWAY_AMT = 12;

    // left swing
    private static final int SWORD_XOFF = (320 + SWORD_SWAY_AMT);
    private static final Panel_Sprite_Func pSwordHide = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            int picnum = psp.picndx;

            psp.y += 3 * synctics;

            if (psp.y >= SWORD_YOFF + engine.getTile(picnum).getHeight()) {
                psp.y = SWORD_YOFF + engine.getTile(picnum).getHeight();
                psp.x = SWORD_XOFF;

                pWeaponUnHideKeys(psp, psp.PresentState);
            }
        }
    };
    private static final Panel_State[] ps_SwordHide = {
            new Panel_State(SWORD_REST, Sword_BEAT_RATE, pSwordHide).setNext()};
    private static final Panel_Sprite_Func pSwordRest = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            if (pWeaponHideKeys(psp, ps_SwordHide[0])) {
                return;
            }

            psp.yorig += synctics;

            if (psp.yorig > SWORD_YOFF) {
                psp.yorig = SWORD_YOFF;
            }

            psp.y = psp.yorig;

            pSwordBobSetup(psp);
            pWeaponBob(psp, PLAYER_MOVING(psp.PlayerP()) != 0);

            boolean force = TEST(psp.flags, PANF_UNHIDE_SHOOT);

            if (TEST_SYNC_KEY(psp.PlayerP(), SK_SHOOT) || force) {
                if (FLAG_KEY_PRESSED(psp.PlayerP(), SK_SHOOT) || force) {
                    psp.flags &= ~(PANF_UNHIDE_SHOOT);

                    pSetState(psp, psp.ActionState);

                    psp.vel = 2500;

                    psp.ang = 1024;
                    psp.PlayerP().SwordAng = SwordAngTable[RANDOM_RANGE(SwordAngTable.length)];
                    DoPlayerSpriteThrow(psp.PlayerP());
                }
            }
        }
    };
    private static final Panel_State[] ps_SwordRest = {
            new Panel_State(SWORD_REST, Sword_BEAT_RATE, pSwordRest).setNext()};
    private static final int SWORD_YOFF = 200;
    // right swing
    private static final int SWORDR_XOFF = (-80);
    private static final short[] SwordAngTable = {82, 168, 256 + 64};
    private static final Panel_Sprite_Func pSwordPresent = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            if (TEST(psp.PlayerP().Flags, PF_WEAPON_RETRACT)) {
                return;
            }

            psp.y -= 3 * synctics;

            if (psp.y < SWORD_YOFF) {
                psp.y = SWORD_YOFF;
                psp.yorig = psp.y;
                pSetState(psp, psp.RestState);
            }
        }
    };
    private static final Panel_State[] ps_PresentSword = {
            new Panel_State(SWORD_REST, Sword_BEAT_RATE, pSwordPresent).setNext()};
    private static final Panel_Sprite_Func pSwordRetract = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            int picnum = psp.picndx;

            psp.y += 3 * synctics;

            if (psp.y >= SWORD_YOFF + engine.getTile(picnum).getHeight()) {
                psp.PlayerP().Flags &= ~(PF_WEAPON_RETRACT);
                psp.PlayerP().Wpn[WPN_SWORD] = null;
                pKillSprite(psp);
            }
        }
    };
    private static final Panel_State[] ps_RetractSword = {
            new Panel_State(SWORD_REST, Sword_BEAT_RATE, pSwordRetract).setNext()};
    private static final Panel_Sprite_Func SwordBlur = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            psp.kill_tics -= synctics;
            if (psp.kill_tics <= 0) {
                pKillSprite(psp);
                return;
            } else if (psp.kill_tics <= 6) {
                psp.flags |= (PANF_TRANS_FLIP);
            }

            psp.shade += 10;
            // change sprites priority

            List.Delete(psp);
            psp.priority--;
            InsertPanelSprite(psp.PlayerP(), psp);
        }
    };
    private static final Panel_Sprite_Func pSwordSlide = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            short vel_adj = 24;

            int x = castLongX(psp);
            int y = castLongY(psp);

            SpawnSwordBlur(psp);

            x += psp.vel * synctics * EngineUtils.cos(psp.ang) >> 6;
            y += psp.vel * synctics * -EngineUtils.sin(psp.ang) >> 6;

            setLong(psp, x, y);

            psp.vel += vel_adj * synctics;
        }
    };
    private static final Panel_State ID_SwordSwing1 = new Panel_State(SWORD_SWING1, SWORD_SLIDE_TICS,
            /* start slide */ pSwordSlide);
    private static final Panel_Sprite_Func pSwordSlideR = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            SpawnSwordBlur(psp);
            short vel_adj = 24;

            int x = castLongX(psp);
            int y = castLongY(psp);

            x += psp.vel * synctics * EngineUtils.cos(psp.ang + 1024) >> 6;
            y += psp.vel * synctics * -EngineUtils.sin(psp.ang + 1024) >> 6;
            setLong(psp, x, y);
            psp.vel += vel_adj * synctics;
        }
    };
    private static final Panel_State ID_SwordSwing5 = new Panel_State(SWORD_SWING1, SWORD_SLIDE_TICS,
            /* start slide */ pSwordSlideR);
    private static final Panel_State ID_SwordSwing0 = new Panel_State(SWORD_SWING0, SWORD_PAUSE_TICS, null);
    private static final Panel_State ID_SwordSwing9 = new Panel_State(SWORD_SWING2, 2, /* end slide */ null);
    private static final short SwordAng = 0;
    private static final Panel_Sprite_Func pSwordSlideDown = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            int vel, vel_adj;

            int x = castLongX(psp);
            int y = castLongY(psp);

            SpawnSwordBlur(psp);
            vel_adj = 20;
            vel = 2500;

            x += psp.vel * synctics * EngineUtils.cos(SwordAng + psp.ang + psp.PlayerP().SwordAng) >> 6;
            y += psp.vel * synctics * -EngineUtils.sin(SwordAng + psp.ang + psp.PlayerP().SwordAng) >> 6;

            setLong(psp, x, y);

            psp.vel += vel_adj * synctics;

            if (psp.x < -40) {
                // if still holding down the fire key - continue swinging
                if (TEST_SYNC_KEY(psp.PlayerP(), SK_SHOOT)) {
                    if (FLAG_KEY_PRESSED(psp.PlayerP(), SK_SHOOT)) {
                        DoPlayerChooseYell(psp.PlayerP());
                        // continue to next state to swing right
                        pStatePlusOne(psp);
                        psp.x = SWORDR_XOFF;
                        psp.y = SWORD_YOFF;
                        psp.yorig = psp.y;
                        psp.ang = 1024;
                        psp.PlayerP().SwordAng = SwordAngTable[RANDOM_RANGE(SwordAngTable.length)];
                        psp.vel = vel;
                        DoPlayerSpriteThrow(psp.PlayerP());
                        return;
                    }
                }

                // NOT still holding down the fire key - stop swinging
                pSetState(psp, psp.PresentState);
                psp.x = SWORD_XOFF;
                psp.y = SWORD_YOFF;
                psp.y += engine.getTile(psp.picndx).getHeight();
                psp.yorig = psp.y;
            }
        }
    };
    private static final Panel_State ID_SwordSwing3 = new Panel_State(SWORD_SWING2, SWORD_MID_SLIDE_TICS,
            /* mid slide */ pSwordSlideDown);
    private static final Panel_State ID_SwordSwing4 = new Panel_State(SWORD_SWING2, 99,
            /* end slide */ pSwordSlideDown);
    private static final Panel_Sprite_Func pSwordSlideDownR = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            int vel, vel_adj;

            int x = castLongX(psp);
            int y = castLongY(psp);

            SpawnSwordBlur(psp);
            vel_adj = 24;
            vel = 2500;

            x += psp.vel * synctics * EngineUtils.cos(SwordAng + psp.ang - psp.PlayerP().SwordAng + 1024) >> 6;
            y += psp.vel * synctics * -EngineUtils.sin(SwordAng + psp.ang - psp.PlayerP().SwordAng + 1024) >> 6;
            setLong(psp, x, y);
            psp.vel += vel_adj * synctics;

            if (psp.x > 350) {
                // if still holding down the fire key - continue swinging
                if (TEST_SYNC_KEY(psp.PlayerP(), SK_SHOOT)) {
                    if (FLAG_KEY_PRESSED(psp.PlayerP(), SK_SHOOT)) {
                        DoPlayerChooseYell(psp.PlayerP());
                        // back to action state
                        pStatePlusOne(psp);
                        psp.x = SWORD_XOFF + 80;
                        psp.y = SWORD_YOFF;
                        psp.yorig = psp.y;
                        psp.PlayerP().SwordAng = SwordAngTable[RANDOM_RANGE(SwordAngTable.length)];
                        psp.ang = 1024;
                        psp.vel = vel;
                        DoPlayerSpriteThrow(psp.PlayerP());
                        return;
                    }
                }

                // NOT still holding down the fire key - stop swinging
                pSetState(psp, psp.PresentState);
                psp.x = SWORD_XOFF;
                psp.y = SWORD_YOFF;
                psp.y += engine.getTile(psp.picndx).getHeight();
                psp.yorig = psp.y;
            }
        }
    };
    private static final Panel_State ID_SwordSwing7 = new Panel_State(SWORD_SWING2, SWORD_MID_SLIDE_TICS,
            /* mid slide */ pSwordSlideDownR);

    private static final Panel_State ID_SwordSwing8 = new Panel_State(SWORD_SWING2, 99,
            /* end slide */ pSwordSlideDownR);
    private static final short[] dangs = {-256, -128, 0, 128, 256};
    private static final Panel_Sprite_Func pSwordAttack = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            InitSwordAttack(psp.PlayerP());
        }
    };
    private static final Panel_State ID_SwordSwing2 = new Panel_State(SWORD_SWING1, 0, /* damage */ pSwordAttack);
    private static final Panel_State ID_SwordSwing6 = new Panel_State(SWORD_SWING2, 0, /* damage */ pSwordAttack);
    private static final Panel_State[] ps_SwordSwing = {ID_SwordSwing0.setNext(ID_SwordSwing1),
            ID_SwordSwing1.setNext(ID_SwordSwing2), ID_SwordSwing2.setNext(ID_SwordSwing3).setFlags(psf_QuickCall),
            ID_SwordSwing3.setNext(ID_SwordSwing4),

            ID_SwordSwing4.setNext().setPlusOne(ID_SwordSwing5),

            ID_SwordSwing5.setNext(ID_SwordSwing6).setFlags(psf_Xflip),
            ID_SwordSwing6.setNext(ID_SwordSwing7).setFlags(psf_QuickCall | psf_Xflip),
            ID_SwordSwing7.setNext(ID_SwordSwing8).setFlags(psf_Xflip),
            ID_SwordSwing8.setNext().setPlusOne(ID_SwordSwing9).setFlags(psf_Xflip),
            ID_SwordSwing9.setNext(ID_SwordSwing1).setPlusOne(ID_SwordSwing0).setFlags(psf_Xflip)};

    public static void InitWeaponSword(PlayerStr pp) {
        Panel_Sprite psp;
        int rnd_num;

        if (Prediction) {
            return;
        }

        if (!TEST(pp.WpnFlags, BIT(WPN_SWORD)) || TEST(pp.Flags, PF_WEAPON_RETRACT)) {
            return;
        }

        // needed for death sequence when the SWORD was your weapon when you died
        if (pp.Wpn[WPN_SWORD] != null && TEST(pp.Wpn[WPN_SWORD].flags, PANF_DEATH_HIDE)) {
            pp.Wpn[WPN_SWORD].flags &= ~(PANF_DEATH_HIDE);
            pp.Flags &= ~(PF_WEAPON_RETRACT | PF_WEAPON_DOWN);
            pSetState(pp.CurWpn, pp.CurWpn.PresentState);
            return;
        }

        if (pp.Wpn[WPN_SWORD] == null) {
            psp = pp.Wpn[WPN_SWORD] = pSpawnSprite(pp, ps_PresentSword[0], PRI_MID, SWORD_XOFF, SWORD_YOFF);
            psp.y += engine.getTile(psp.picndx).getHeight();
        }

        if (pp.CurWpn == pp.Wpn[WPN_SWORD]) {
            return;
        }

        PlayerUpdateWeapon(pp, WPN_SWORD);

        pp.WpnUziType = 2; // Make uzi's go away!
        RetractCurWpn(pp);

        // Set up the new Weapon variables
        psp = pp.CurWpn = pp.Wpn[WPN_SWORD];
        psp.flags |= (PANF_WEAPON_SPRITE);
        psp.ActionState = ps_SwordSwing[0];
        psp.RetractState = ps_RetractSword[0];
        psp.PresentState = ps_PresentSword[0];
        psp.RestState = ps_SwordRest[0];
        pSetState(psp, psp.PresentState);

        PlaySound(DIGI_SWORD_UP, pp, v3df_follow | v3df_dontpan);

        if (pp == Player[myconnectindex]) {
            rnd_num =  STD_RANDOM_RANGE(1024);
            if (rnd_num > 900) {
                PlaySound(DIGI_TAUNTAI2, pp, v3df_follow | v3df_dontpan);
            } else if (rnd_num > 800) {
                PlaySound(DIGI_PLAYERYELL1, pp, v3df_follow | v3df_dontpan);
            } else if (rnd_num > 700) {
                PlaySound(DIGI_PLAYERYELL2, pp, v3df_follow | v3df_dontpan);
            } else if (rnd_num > 600) {
                PlayerSound(DIGI_ILIKESWORD, v3df_follow | v3df_dontpan, pp);
            }
        }

        FLAG_KEY_RELEASE(psp.PlayerP(), SK_SHOOT);
        FLAG_KEY_RESET(psp.PlayerP(), SK_SHOOT);
    }

    private static void InitSwordAttack(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite), tu;
        Sprite sp = null;
        int stat;
        long dist;
        int reach, face;

        PlaySound(DIGI_SWORDSWOOSH, pp, v3df_dontpan | v3df_doppler);

        if (TEST(pp.Flags, PF_DIVING)) {
            int bubble;
            Sprite bp;
            int nx, ny;
            int random_amt;

            for (short dang : dangs) {
                if (RANDOM_RANGE(1000) < 500) {
                    continue; // Don't spawn bubbles every time
                }
                bubble = SpawnBubble(pp.PlayerSprite);
                if (bubble >= 0) {
                    bp = boardService.getSprite(bubble);
                    if (bp != null && u != null) {
                        bp.setAng(pp.getAnglei());

                        random_amt = ((RANDOM_P2(32 << 8) >> 8) - 16);

                        // back it up a bit to get it out of your face
                        nx = MOVEx((1024 + 256) * 3, NORM_ANGLE(bp.getAng() + dang + random_amt));
                        ny = MOVEy((1024 + 256) * 3, NORM_ANGLE(bp.getAng() + dang + random_amt));

                        move_missile(bubble, nx, ny, 0, u.ceiling_dist, u.floor_dist, CLIPMASK_PLAYER, 1);
                    }
                }
            }
        }

        ListNode<Sprite> nexti;
        for (stat = 0; stat < StatDamageList.length; stat++) {
            for (ListNode<Sprite> node = boardService.getStatNode(StatDamageList[stat]); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                sp = node.get();
                USER ui = getUser(i);

                if (ui != null && ui.PlayerP == pp.pnum) {
                    break;
                }

                if (!TEST(sp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    continue;
                }

                dist = Distance(pp.posx, pp.posy, sp.getX(), sp.getY());

                reach = 1000; // !JIM! was 800
                face = 200;
                Sprite psp = pp.getSprite();
                if (psp == null) {
                    continue;
                }

                if (dist < CLOSE_RANGE_DIST_FUDGE(sp, psp, reach) && PLAYER_FACING_RANGE(pp, sp, face)) {
                    if (SpriteOverlapZ(pp.PlayerSprite, i, Z(20))) {
                        if (FAFcansee(sp.getX(), sp.getY(), SPRITEp_MID(sp), sp.getSectnum(), psp.getX(), psp.getY(), SPRITEp_MID(psp),
                                psp.getSectnum())) {
                            DoDamage(i, pp.PlayerSprite);
                        }
                    }
                }
            }
        }

        // all this is to break glass
        {
            int daang = pp.getAnglei();
            int daz = ((100 - pp.getHorizi()) * 2000) + (RANDOM_RANGE(24000) - 12000);

            FAFhitscan(pp.posx, pp.posy, pp.posz, pp.cursectnum, // Start position
                    EngineUtils.cos(daang), // X vector of 3D ang
                    EngineUtils.sin(daang), // Y vector of 3D ang
                    daz, // Z vector of 3D ang
                    pHitInfo, CLIPMASK_MISSILE);

            int hitsect = pHitInfo.hitsect;
            int hitwall = pHitInfo.hitwall;
            int hitsprite = pHitInfo.hitsprite;
            int hitx = pHitInfo.hitx;
            int hity = pHitInfo.hity;
            int hitz = pHitInfo.hitz;

            if (hitsect == -1) {
                return;
            }

            if (FindDistance3D(pp.posx - hitx, pp.posy - hity, (pp.posz - hitz) >> 4) < 700) {

                Sprite hsp = boardService.getSprite(hitsprite);
                if (hsp != null) {
                    tu = getUser(hitsprite);
                    if (tu != null) {
                        switch (tu.ID) {
                            case ZILLA_RUN_R0:
                            case PACHINKO1:
                            case PACHINKO2:
                            case PACHINKO3:
                            case PACHINKO4:
                            case 623:
                                SpawnSwordSparks(pp, hitsect, -1, hitx, hity, hitz, daang);
                                PlaySound(DIGI_SWORDCLANK, sndCoords.set(hitx, hity, hitz), v3df_none);
                                break;
                            case TRASHCAN:
                                if (tu.WaitTics <= 0) {
                                    tu.WaitTics =  SEC(2);
                                    ChangeState(hitsprite, s_TrashCanPain[0]);
                                }
                                SpawnSwordSparks(pp, hitsect, -1, hitx, hity, hitz, daang);
                                PlaySound(DIGI_SWORDCLANK, sndCoords.set(hitx, hity, hitz), v3df_none);
                                PlaySound(DIGI_TRASHLID, sp, v3df_none);
                                break;
                        }
                    }

                    if (hsp.getLotag() == TAG_SPRITE_HIT_MATCH) {
                        if (MissileHitMatch(-1, WPN_STAR, hitsprite)) {
                            return;
                        }
                    }

                    if (TEST(hsp.getExtra(), SPRX_BREAKABLE)) {
                        HitBreakSprite(hitsprite, 0);
                    }

                    // hit a switch?
                    if (TEST(hsp.getCstat(), CSTAT_SPRITE_WALL) && (hsp.getLotag() != 0 || hsp.getHitag() != 0)) {
                        ShootableSwitch(hitsprite, -1);
                    }

                }

                Wall hw = boardService.getWall(hitwall);
                if (hw != null) {
                    if (hw.getNextsector() >= 0) {
                        Sector nexts = boardService.getSector(hw.getNextsector());
                        if (nexts != null && TEST(nexts.getCeilingstat(), CEILING_STAT_PLAX)) {
                            if (hitz < nexts.getCeilingz()) {
                                return;
                            }
                        }
                    }

                    if (u != null && hw.getLotag() == TAG_WALL_BREAK) {
                        HitBreakWall(hitwall, hitx, hity, hitz, daang, u.ID);
                    }
                    // hit non-breakable wall - do sound and puff
                    else {
                        SpawnSwordSparks(pp, hitsect, hitwall, hitx, hity, hitz, daang);
                        PlaySound(DIGI_SWORDCLANK, sndCoords.set(hitx, hity, hitz), v3df_none);
                    }
                }
            }
        }

    }

    public static void SpawnSwordSparks(PlayerStr pp, int hitsect, int hitwall, int hitx, int hity, int hitz, int hitang) {
        USER u = getUser(pp.PlayerSprite);

        int j =  SpawnSprite(STAT_MISSILE, UZI_SMOKE, s_UziSmoke[0], hitsect, hitx, hity, hitz, hitang, 0);
        Sprite wp = boardService.getSprite(j);
        if (wp == null) {
            return;
        }

        wp.setShade(-40);
        wp.setXrepeat(20);
        wp.setYrepeat(20);
        SetOwner(pp.PlayerSprite, j);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_YCENTER));
        wp.setHitag(LUMINOUS); // Always full brightness

        // Sprite starts out with center exactly on wall.
        // This moves it back enough to see it at all angles.

        wp.setClipdist(32 >> 2);

        if (hitwall != -1) {
            HitscanSpriteAdjust(j, hitwall);
        }

        j =  SpawnSprite(STAT_MISSILE, UZI_SPARK, s_UziSpark[0], hitsect, hitx, hity, hitz, hitang, 0);
        wp = boardService.getSprite(j);
        USER wu = getUser(j);
        if (wp == null || wu == null) {
            return;
        }

        wp.setShade(-40);
        wp.setXrepeat(20);
        wp.setYrepeat(20);
        SetOwner(pp.PlayerSprite, j);
        wu.spal = PALETTE_DEFAULT;
        wp.setPal(wu.spal);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        if (u != null && u.WeaponNum == WPN_FIST) {
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
        }

        wp.setClipdist(32 >> 2);

        if (hitwall != -1) {
            HitscanSpriteAdjust(j, hitwall);
        }

    }

    private static void pSwordBobSetup(Panel_Sprite psp) {
        if (TEST(psp.flags, PANF_BOB)) {
            return;
        }

        psp.xorig = psp.x;
        psp.yorig = psp.y;

        psp.sin_amt = SWORD_SWAY_AMT;
        psp.sin_ndx = 0;
        psp.bob_height_shift = 3;
    }

    private static void SpawnSwordBlur(Panel_Sprite psp) {
        Panel_Sprite nsp;

        if (psp.PlayerP().SwordAng > 200) {
            return;
        }

        nsp = pSpawnSprite(psp.PlayerP(), null, PRI_BACK, psp.x, psp.y);

        nsp.flags |= (PANF_WEAPON_SPRITE);
        nsp.xfract = psp.xfract;
        nsp.yfract = psp.yfract;
        nsp.ang = psp.ang;
        nsp.vel = psp.vel;
        nsp.PanelSpriteFunc = SwordBlur;
        nsp.kill_tics = 9;
        nsp.shade =  (psp.shade + 10);

        nsp.picndx = -1;
        nsp.picnum = psp.picndx;

        if (psp.State.testFlag(psf_Xflip)) {
            nsp.flags |= (PANF_XFLIP);
        }

        nsp.rotate_ang = psp.rotate_ang;
        nsp.scale = psp.scale;

        nsp.flags |= (PANF_TRANSLUCENT);
    }

    public static void SwordSaveable() {
        SaveData(ps_PresentSword);
        SaveData(ps_SwordRest);
        SaveData(ps_SwordHide);
        SaveData(ps_SwordSwing);
        SaveData(ps_RetractSword);

        SaveData(pSwordPresent);
        SaveData(pSwordSlide);
        SaveData(pSwordAttack);
        SaveData(pSwordSlideDown);
        SaveData(pSwordSlideR);
        SaveData(pSwordSlideDownR);
        SaveData(pSwordRest);
        SaveData(pSwordHide);
        SaveData(pSwordRetract);
        SaveData(SwordBlur);
    }
}
