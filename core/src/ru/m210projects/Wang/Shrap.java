package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Player.PlayerStateGroup;
import ru.m210projects.Wang.Type.SHRAP;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Wang.Actor.DoBeginJump;
import static ru.m210projects.Wang.Break.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Factory.WangNetwork.Prediction;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JWeapon.InitPhosphorus;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.PALETTE_DEFAULT;
import static ru.m210projects.Wang.Palette.PALETTE_SKEL_GORE;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Stag.SPAWN_SPOT;
import static ru.m210projects.Wang.Type.MyTypes.DIV2;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Weapon.*;

public class Shrap {

    public static final int Z_TOP = 0;
    public static final int Z_MID = 1;
    public static final int Z_BOT = 2;
//    public static final int WALL_FLOOR_SHRAP = 4097;
    private static final SHRAP[] CoinShrap = {
            new SHRAP(s_CoinShrap, COIN_SHRAP, 5, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapA, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapB, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapC, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),

    };
    private static final SHRAP[] GlassShrap = {
            new SHRAP(s_GlassShrapA, GLASS_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_GlassShrapB, GLASS_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_GlassShrapC, GLASS_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),

    };
    private static final SHRAP[] WoodShrap = {
            new SHRAP(s_WoodShrapA, WOOD_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_WoodShrapB, WOOD_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_WoodShrapC, WOOD_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),

    };
    private static final SHRAP[] StoneShrap = {
            new SHRAP(s_StoneShrapA, STONE_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_StoneShrapB, STONE_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_StoneShrapC, STONE_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),

    };
    private static final SHRAP[] PaperShrap = {
            new SHRAP(s_PaperShrapA, PAPER_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_PaperShrapB, PAPER_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_PaperShrapC, PAPER_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),

    };
    private static final SHRAP[] MetalShrap = {
            new SHRAP(s_MetalShrapA, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapB, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapC, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),

    };
    private static final SHRAP[] MetalMix = {
            new SHRAP(s_GlassShrapA, GLASS_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_GlassShrapB, GLASS_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_GlassShrapC, GLASS_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapA, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapB, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapC, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),

    };
    private static final SHRAP[] WoodMix = {
            new SHRAP(s_WoodShrapA, WOOD_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_WoodShrapB, WOOD_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_WoodShrapC, WOOD_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapA, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapB, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_MetalShrapC, METAL_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),

    };
    private static final SHRAP[] PaperMix = {
            new SHRAP(s_WoodShrapA, WOOD_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_WoodShrapB, WOOD_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_WoodShrapC, WOOD_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_PaperShrapA, PAPER_SHRAP_A, 2, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_PaperShrapB, PAPER_SHRAP_A, 2, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_PaperShrapC, PAPER_SHRAP_A, 2, Z_MID, 200, 600, 100, 500, true, 2048),

    };

    ////
    // END - BREAK shrap types
    ////
    private static final SHRAP[] Marbels = {new SHRAP(s_Marbel, MARBEL, 5, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_GlassShrapA, GLASS_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_GlassShrapB, GLASS_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),
            new SHRAP(s_GlassShrapC, GLASS_SHRAP_A, 1, Z_MID, 200, 600, 100, 500, true, 2048),

    };
    private static final SHRAP[] EMPShrap = {new SHRAP(s_EMPShrap, EMP, 1, Z_MID, 500, 1100, 300, 600, false, 128),

    };
    private static final SHRAP[] StdShrap = {new SHRAP(s_GoreHead, GORE_Head, 1, Z_TOP, 400, 700, 20, 40, true, 2048),
            new SHRAP(s_GoreLung, GORE_Lung, 2, Z_TOP, 500, 800, 100, 300, true, 2048),
            new SHRAP(s_GoreLiver, GORE_Liver, 1, Z_MID, 300, 500, 100, 150, true, 2048),
            new SHRAP(s_GoreArm, GORE_Arm, 1, Z_MID, 300, 500, 250, 500, true, 2048),
            new SHRAP(s_GoreSkullCap, GORE_SkullCap, 1, Z_TOP, 300, 500, 250, 500, true, 2048),
            new SHRAP(s_FastGoreDrip, GORE_Drip, 8, Z_BOT, 600, 800, 50, 70, false, 2048),

    };
    private static final SHRAP[] HeartAttackShrap = // fewer gibs because of the plasma fountain sprites
            {new SHRAP(s_GoreLung, GORE_Lung, 2, Z_TOP, 500, 1100, 300, 600, true, 2048),
                    new SHRAP(s_GoreLiver, GORE_Liver, 1, Z_MID, 500, 1100, 300, 500, true, 2048),
                    new SHRAP(s_GoreArm, GORE_Arm, 2, Z_MID, 500, 1100, 350, 600, true, 2048),

            };
    private static final SHRAP[] SkelGore = {new SHRAP(s_GoreHead, GORE_Head, 1, Z_TOP, 400, 700, 20, 40, true, 2048),
            new SHRAP(s_GoreLung, GORE_Lung, 2, Z_TOP, 500, 800, 100, 300, true, 2048),
            new SHRAP(s_GoreLiver, GORE_Liver, 1, Z_MID, 300, 500, 100, 150, true, 2048),
            new SHRAP(s_GoreSkullCap, GORE_SkullCap, 1, Z_TOP, 300, 500, 100, 150, true, 2048),
            new SHRAP(s_GoreArm, GORE_Arm, 1, Z_MID, 300, 500, 250, 500, true, 2048),
            new SHRAP(s_GoreLeg, GORE_Leg, 2, Z_BOT, 200, 400, 250, 500, true, 2048),
            new SHRAP(s_GoreChunkS, GORE_ChunkS, 4, Z_BOT, 200, 400, 250, 400, true, 2048),

    };
    private static final SHRAP[] UpperGore = {new SHRAP(s_GoreHead, GORE_Head, 1, Z_TOP, 400, 700, 20, 40, true, 2048),
            new SHRAP(s_GoreLung, GORE_Lung, 2, Z_TOP, 500, 800, 100, 300, true, 2048),
            new SHRAP(s_GoreLiver, GORE_Liver, 1, Z_MID, 300, 500, 100, 150, true, 2048),
            new SHRAP(s_GoreSkullCap, GORE_SkullCap, 1, Z_TOP, 300, 500, 100, 150, true, 2048),
            new SHRAP(s_GoreArm, GORE_Arm, 1, Z_MID, 300, 500, 250, 500, true, 2048),

    };
    private static final SHRAP[] SmallGore = {
            new SHRAP(s_GoreDrip, GORE_Drip, 3, Z_TOP, 600, 800, 50, 70, false, 2048),
            new SHRAP(s_FastGoreDrip, GORE_Drip, 3, Z_BOT, 600, 800, 70, 100, false, 2048),

    };
    private static final SHRAP[] FlamingGore = {
            new SHRAP(s_GoreFlame, GORE_Drip, 2, Z_TOP, 600, 800, 100, 200, false, 2048),
            new SHRAP(s_GoreFlameChunkB, GORE_Drip, 4, Z_MID, 300, 500, 100, 200, false, 2048),
            new SHRAP(s_GoreFlame, GORE_Drip, 2, Z_BOT, 100, 200, 100, 200, false, 2048),

    };
//    private static final SHRAP[] FireballExpShrap1 = {
//            new SHRAP(s_GoreFlame, GORE_Drip, 1, Z_MID, 100, 300, 100, 200, true, 2048),
//
//    };
//    private static final SHRAP[] FireballExpShrap2 = {
//            new SHRAP(s_GoreFlame, GORE_Drip, 2, Z_MID, 100, 300, 100, 200, true, 2048),
//
//    };
//    static SHRAP[][] FireballExpShrap = {FireballExpShrap1, FireballExpShrap2};
    // state, id, num, zlevel, min_jspeed, max_jspeed, min_vel, max_vel,
    // random_disperse, ang_range;
    private static final SHRAP[] ElectroShrap = {
            new SHRAP(s_ElectroShrap, ELECTRO_SHARD, 12, Z_TOP, 200, 600, 100, 500, true, 2048)};
    // state, id, num, zlevel, min_jspeed, max_jspeed, min_vel, max_vel,
    // random_disperse, ang_range;
    private static final SHRAP[] LavaShrap1 = {
            new SHRAP(s_GoreFlame, GORE_Drip, 1, Z_TOP, 400, 1400, 100, 400, true, 2048),

    };
    private static final SHRAP[] LavaShrap2 = {
            new SHRAP(s_GoreFlameChunkB, GORE_Drip, 1, Z_TOP, 400, 1400, 100, 400, true, 2048),

    };
    private static final SHRAP[][] LavaShrapTable = {LavaShrap1, LavaShrap2};

    //
    // PLAYER SHRAP
    //
    private static final SHRAP[] LavaBoulderShrap = {
            new SHRAP(s_LavaShard, LAVA_SHARD, 16, Z_MID, 400, 900, 200, 600, true, 2048),

    };
    // state, id, num, zlevel, min_jspeed, max_jspeed, min_vel, max_vel,
    // random_disperse, ang_range;
    private static final SHRAP[] PlayerGoreFall = {
            new SHRAP(s_GoreSkullCap, GORE_SkullCap, 1, Z_TOP, 200, 300, 100, 200, true, 2048),
            new SHRAP(s_GoreLiver, GORE_Liver, 1, Z_MID, 200, 300, 100, 200, true, 2048),
            new SHRAP(s_GoreLung, GORE_Lung, 1, Z_MID, 200, 300, 100, 200, true, 2048),
            new SHRAP(s_GoreDrip, GORE_Drip, 10, Z_MID, 200, 300, 100, 200, false, 2048),
            new SHRAP(s_GoreArm, GORE_Arm, 1, Z_MID, 200, 300, 100, 200, true, 2048),
            new SHRAP(s_FastGoreDrip, GORE_Drip, 10, Z_BOT, 200, 300, 100, 200, false, 2048),

    };
    private static final SHRAP[] PlayerGoreFly = {
            new SHRAP(s_GoreSkullCap, GORE_SkullCap, 1, Z_TOP, 500, 1100, 300, 600, true, 2048),
            new SHRAP(s_GoreTorso, GORE_Torso, 1, Z_MID, 500, 1100, 300, 500, true, 2048),
            new SHRAP(s_GoreLiver, GORE_Liver, 1, Z_MID, 200, 300, 100, 200, true, 2048),
            new SHRAP(s_GoreArm, GORE_Arm, 1, Z_MID, 500, 1100, 350, 600, true, 2048),
            new SHRAP(s_FastGoreDrip, GORE_Drip, 16, Z_MID, 500, 1100, 350, 600, false, 2048),
            new SHRAP(s_FastGoreDrip, GORE_Drip, 16, Z_BOT, 500, 1100, 350, 600, false, 2048),

    };
    private static final SHRAP[] PlayerDeadHead = {
            new SHRAP(s_GoreDrip, GORE_Drip, 2, Z_TOP, 150, 400, 40, 80, true, 2048),
            new SHRAP(s_GoreDrip, GORE_Drip, 2, Z_MID, 150, 400, 40, 80, true, 2048),

    };
    // state, id, num, zlevel, min_jspeed, max_jspeed, min_vel, max_vel,
    // random_disperse, ang_range;
    private static final SHRAP[] PlayerHeadHurl1 = {
            new SHRAP(s_Vomit1, Vomit1, 1, Z_BOT, 250, 400, 100, 200, true, 256),

    };
    private static final AutoShrapRet ret = new AutoShrapRet();
    // Individual shraps can be copied to this and then values can be changed
    private static final SHRAP[] CustomShrap = new SHRAP[20];
    private static final int[] hz = new int[3];

    private static AutoShrapRet AutoShrap(int ParentNum, int shrap_type, int shrap_amt, int shrap_delta_size) {
        SHRAP[] p = SmallGore;
        int shrap_xsize;
        boolean shrap_bounce = false;

        Sprite parent = boardService.getSprite(ParentNum);
        switch (shrap_type) {
            case SHRAP_GLASS:
                PlaySound(DIGI_BREAKGLASS, parent, v3df_dontpan | v3df_doppler);
                p = GlassShrap;
                if (shrap_amt != 0) {
                    for (int c = 0; c < GlassShrap.length; c++) {
                        if (CustomShrap[c] == null) {
                            CustomShrap[c] = new SHRAP();
                        }
                        CustomShrap[c].copy(GlassShrap[c]);
                    }

                    CustomShrap[0].num = shrap_amt;
                    p = CustomShrap;
                }

                shrap_xsize =  (16 + shrap_delta_size);
                shrap_bounce = true;
                break;

            case SHRAP_GENERIC:
            case SHRAP_STONE:
                PlaySound(DIGI_BREAKSTONES, parent, v3df_dontpan | v3df_doppler);
                p = StoneShrap;
                if (shrap_amt != 0) {

                    for (int c = 0; c < StoneShrap.length; c++) {
                        if (CustomShrap[c] == null) {
                            CustomShrap[c] = new SHRAP();
                        }
                        CustomShrap[c].copy(StoneShrap[c]);
                    }

                    CustomShrap[0].num = shrap_amt;
                    p = CustomShrap;

                }

                shrap_xsize =  (8 + shrap_delta_size);
                shrap_bounce = true;
                break;

            case SHRAP_WOOD:

            case SHRAP_TREE_BARK:
                PlaySound(DIGI_BREAKINGWOOD, parent, v3df_dontpan | v3df_doppler);
                p = WoodShrap;
                if (shrap_amt != 0) {

                    for (int c = 0; c < WoodShrap.length; c++) {
                        if (CustomShrap[c] == null) {
                            CustomShrap[c] = new SHRAP();
                        }
                        CustomShrap[c].copy(WoodShrap[c]);
                    }

                    CustomShrap[0].num = shrap_amt;
                    p = CustomShrap;

                }

                shrap_xsize =  (16 + shrap_delta_size);
                shrap_bounce = true;
                break;

            case SHRAP_BLOOD:
                shrap_xsize =  (16 + shrap_delta_size);
                break;

            case SHRAP_GIBS:
                PlaySound(DIGI_GIBS1, parent, v3df_dontpan | v3df_doppler);
                shrap_xsize = 34;
                break;

            case SHRAP_PAPER:
                p = PaperShrap;
                if (shrap_amt != 0) {

                    for (int c = 0; c < PaperShrap.length; c++) {
                        if (CustomShrap[c] == null) {
                            CustomShrap[c] = new SHRAP();
                        }
                        CustomShrap[c].copy(PaperShrap[c]);
                    }

                    CustomShrap[0].num = shrap_amt;
                    p = CustomShrap;
                }

                shrap_xsize =  (16 + shrap_delta_size);
                break;

            case SHRAP_METAL:
                PlaySound(DIGI_BREAKMETAL, parent, v3df_dontpan | v3df_doppler);
                p = MetalShrap;
                if (shrap_amt != 0) {

                    for (int c = 0; c < p.length; c++) {
                        if (CustomShrap[c] == null) {
                            CustomShrap[c] = new SHRAP();
                        }
                        CustomShrap[c].copy(p[c]);
                    }

                    CustomShrap[0].num = shrap_amt;
                    p = CustomShrap;
                }

                shrap_xsize =  (16 + shrap_delta_size);
                shrap_bounce = true;
                break;

            case SHRAP_COIN:
                PlaySound(DIGI_COINS, parent, v3df_dontpan | v3df_doppler);
                p = CoinShrap;
                if (shrap_amt != 0) {
                    for (int c = 0; c < p.length; c++) {
                        if (CustomShrap[c] == null) {
                            CustomShrap[c] = new SHRAP();
                        }
                        CustomShrap[c].copy(p[c]);
                    }

                    CustomShrap[0].num = shrap_amt;
                    p = CustomShrap;
                }

                shrap_xsize =  (16 + shrap_delta_size);
                shrap_bounce = true;
                break;

            case SHRAP_METALMIX:
                PlaySound(DIGI_BREAKMETAL, parent, v3df_dontpan | v3df_doppler);
                p = MetalMix;
                if (shrap_amt != 0) {
                    for (int c = 0; c < p.length; c++) {
                        if (CustomShrap[c] == null) {
                            CustomShrap[c] = new SHRAP();
                        }
                        CustomShrap[c].copy(p[c]);
                    }

                    CustomShrap[0].num = shrap_amt;
                    p = CustomShrap;
                }

                shrap_xsize =  (16 + shrap_delta_size);
                shrap_bounce = true;
                break;

            case SHRAP_MARBELS:
                p = Marbels;
                if (shrap_amt != 0) {
                    for (int c = 0; c < p.length; c++) {
                        if (CustomShrap[c] == null) {
                            CustomShrap[c] = new SHRAP();
                        }
                        CustomShrap[c].copy(p[c]);
                    }

                    CustomShrap[0].num = shrap_amt;
                    p = CustomShrap;
                }

                shrap_xsize =  (10 + shrap_delta_size);
                shrap_bounce = true;
                break;
            case SHRAP_WOODMIX:
                PlaySound(DIGI_BREAKINGWOOD, parent, v3df_dontpan | v3df_doppler);
                p = WoodMix;
                if (shrap_amt != 0) {
                    for (int c = 0; c < p.length; c++) {
                        if (CustomShrap[c] == null) {
                            CustomShrap[c] = new SHRAP();
                        }
                        CustomShrap[c].copy(p[c]);
                    }

                    CustomShrap[0].num = shrap_amt;
                    p = CustomShrap;
                }

                shrap_xsize =  (16 + shrap_delta_size);
                shrap_bounce = true;
                break;

            case SHRAP_PAPERMIX:
                PlaySound(DIGI_BREAKINGWOOD, parent, v3df_dontpan | v3df_doppler);
                p = PaperMix;
                if (shrap_amt != 0) {
                    for (int c = 0; c < p.length; c++) {
                        if (CustomShrap[c] == null) {
                            CustomShrap[c] = new SHRAP();
                        }
                        CustomShrap[c].copy(p[c]);
                    }

                    CustomShrap[0].num = shrap_amt;
                    p = CustomShrap;
                }

                shrap_xsize =  (16 + shrap_delta_size);
                break;

            case SHRAP_EXPLOSION: {
                int spnum =  SpawnLargeExp(ParentNum);
                Sprite ep = boardService.getSprite(spnum);
                if (ep != null) {
                    int size = (ep.getXrepeat() + shrap_delta_size);
                    ep.setXrepeat(size);
                    ep.setYrepeat(size);
                }

                return null;
            }

            case SHRAP_LARGE_EXPLOSION:
                int spnum =  SpawnLargeExp(ParentNum);
                Sprite ep = boardService.getSprite(spnum);

                if (ep != null) {
                    int size = (ep.getXrepeat() + shrap_delta_size);
                    ep.setXrepeat(size);
                    ep.setYrepeat(size);
                    InitPhosphorus(spnum);
                }

                return null;
            default:
                return null;
        }

        ret.p = p;
        ret.bounce = shrap_bounce;
        ret.size = shrap_xsize;

        return ret;
    }

    public static boolean SpawnShrap(int ParentNum, int Secondary) {
        Sprite parent = boardService.getSprite(ParentNum);
        if (parent == null) {
            return false;
        }

        USER pu = getUser(ParentNum);
        SHRAP[] p = SmallGore;

        boolean retval = true;
        if (Prediction) {
            return false;
        }

        // Don't spawn shrapnel in invalid sectors gosh dern it!
        Sector parentSec = boardService.getSector(parent.getSectnum());
        if (parentSec == null) {
            return false;
        }

        AutoShrapRet ret = null;
        boolean shrap_bounce = false;
        int shrap_type;
        int shrap_amt;
        int shrap_xsize = 48, shrap_ysize = 48;
        int shrap_rand_zamt = 0;
        int shrap_owner = -1;
        int shrap_pal = PALETTE_DEFAULT;

        if (GlobBreakInfo != null) {
            shrap_type = GlobBreakInfo.shrap_type;
            shrap_amt = GlobBreakInfo.shrap_amt;
            GlobBreakInfo = null;

            ret = AutoShrap(ParentNum, shrap_type, shrap_amt, 0);
            if (ret == null) {
                return false;
            }

            p = ret.p;
            shrap_bounce = ret.bounce;
            shrap_xsize = shrap_ysize =  ret.size;
        } else if (TEST(parent.getExtra(), SPRX_BREAKABLE)) {
            // if no user
            if (getUser(ParentNum) == null) {
                // Jump to shrap type
                shrap_type = SP_TAG8(parent);

                int shrap_delta_size = (byte) SP_TAG10(parent);
                shrap_rand_zamt = SP_TAG9(parent);
                // Hey, better limit this in case mappers go crazy, like I did. :)
                // Kills frame rate!
                shrap_amt =  SP_TAG8(parent);
                if (shrap_amt > 5) {
                    shrap_amt = 5;
                }

                ret = AutoShrap(ParentNum, shrap_type, shrap_amt, shrap_delta_size);
                if (ret == null) {
                    return false;
                }

                p = ret.p;
                shrap_bounce = ret.bounce;
                shrap_xsize = shrap_ysize =  ret.size;

            } else {
                // has a user - is programmed
                change_sprite_stat(ParentNum, STAT_MISC);
                parent.setExtra(parent.getExtra() & ~(SPRX_BREAKABLE));
                parent.setCstat(parent.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
            }
        }

        if (pu != null && ret == null) {
            switch (pu.ID) {
                case ST1:
                    if (parent.getHitag() == SPAWN_SPOT) {
                        shrap_type = SP_TAG6(parent);
                        if (pu.LastDamage != 0) {
                            shrap_type = SP_TAG3(parent);
                        }

                        int shrap_delta_size = (byte) SP_TAG10(parent);
                        shrap_rand_zamt = SP_TAG9(parent);
                        // Hey, better limit this in case mappers go crazy, like I did. :)
                        // Kills frame rate!
                        shrap_amt =  SP_TAG8(parent);
                        if (shrap_amt > 5) {
                            shrap_amt = 5;
                        }

                        ret = AutoShrap(ParentNum, shrap_type, shrap_amt, shrap_delta_size);
                        if (ret == null) {
                            return false;
                        }

                        p = ret.p;
                        shrap_bounce = ret.bounce;
                        shrap_xsize = shrap_ysize =  ret.size;
                    } else {
                        p = LavaShrapTable[RANDOM_P2(2 << 8) >> 8];
                    }
                    break;

                case BREAK_BARREL:
                    PlaySound(DIGI_BREAKDEBRIS, parent, v3df_dontpan | v3df_doppler);
                    p = WoodShrap;
                    shrap_xsize = shrap_ysize = 24;
                    shrap_bounce = true;
                    ChangeState(ParentNum, s_BreakBarrel[0]);
                    break;
                case BREAK_LIGHT:
                    PlaySound(DIGI_BREAKGLASS, parent, v3df_dontpan | v3df_doppler);
                    p = GlassShrap;
                    shrap_xsize = shrap_ysize = 24;
                    shrap_bounce = true;
                    ChangeState(ParentNum, s_BreakLight[0]);
                    break;
                case BREAK_PEDISTAL:
                    PlaySound(DIGI_BREAKSTONES, parent, v3df_dontpan | v3df_doppler);
                    p = StoneShrap;
                    shrap_xsize = shrap_ysize = 24;
                    shrap_bounce = true;
                    ChangeState(ParentNum, s_BreakPedistal[0]);
                    break;
                case BREAK_BOTTLE1:
                    PlaySound(DIGI_BREAKGLASS, parent, v3df_dontpan | v3df_doppler);
                    p = GlassShrap;
                    shrap_xsize = shrap_ysize = 8;
                    shrap_bounce = true;
                    ChangeState(ParentNum, s_BreakBottle1[0]);
                    break;
                case BREAK_BOTTLE2:
                    PlaySound(DIGI_BREAKGLASS, parent, v3df_dontpan | v3df_doppler);
                    p = GlassShrap;
                    shrap_xsize = shrap_ysize = 8;
                    shrap_bounce = true;
                    ChangeState(ParentNum, s_BreakBottle2[0]);
                    break;
                case BREAK_MUSHROOM:
                    PlaySound(DIGI_BREAKDEBRIS, parent, v3df_dontpan | v3df_doppler);
                    p = StoneShrap;
                    shrap_xsize = shrap_ysize = 4;
                    shrap_bounce = true;
                    SetSuicide(ParentNum); // kill next iteration
                    break;
                case BOLT_EXP:
                case TRACER_EXP:
                case TANK_SHELL_EXP:
                case SECTOR_EXP:
                case GRENADE_EXP:
                case FIREBALL_EXP:
                case NINJA_DEAD:
                case SKULL_SERP:
                    return (false);
                case BOLT_THINMAN_R1:
                case ZILLA_RUN_R0:
                    p = MetalShrap;

                    shrap_xsize = shrap_ysize = 10;
                    break;
                case LAVA_BOULDER:
                    PlaySound(DIGI_BREAKSTONES, parent, v3df_dontpan | v3df_doppler);
                    p = LavaBoulderShrap;
                    shrap_owner = parent.getOwner();
                    shrap_xsize = shrap_ysize = 24;
                    shrap_bounce = true;
                    break;
                case ELECTRO_PLAYER:
                case ELECTRO_ENEMY:
                    shrap_owner = parent.getOwner();
                    p = ElectroShrap;
                    shrap_xsize = shrap_ysize = 20;
                    break;
                case COOLIE_RUN_R0:
                    if (Secondary == WPN_NM_SECTOR_SQUISH) {
                        break;
                    }
                    break;
                case NINJA_Head_R0: {
                    if (pu.getRot() == PlayerStateGroup.sg_PlayerHeadHurl) {
                        p = PlayerHeadHurl1;
                    } else {
                        p = PlayerDeadHead;
                        shrap_xsize = shrap_ysize = 16 + 8;
                        shrap_bounce = true;
                    }
                    break;
                }
                case GIRLNINJA_RUN_R0:
                case SERP_RUN_R0:
                case SUMO_RUN_R0:
                    p = StdShrap;
                    break;
                case NINJA_RUN_R0: {
                    p = StdShrap;
                    if (pu.PlayerP != -1) {

                        if (Player[pu.PlayerP].DeathType == PLAYER_DEATH_CRUMBLE) {
                            p = PlayerGoreFall;
                        } else {
                            p = PlayerGoreFly;
                        }
                    }
                    break;
                }
                case GORO_RUN_R0:
                    p = StdShrap;
                    shrap_xsize = shrap_ysize = 64;
                    break;
                case COOLG_RUN_R0:
                    p = UpperGore;
                    break;
                case RIPPER_RUN_R0:
                case RIPPER2_RUN_R0:
                    p = StdShrap;
                    if (pu.spal != 0) {
                        shrap_xsize = shrap_ysize = 64;
                    } else {
                        shrap_xsize = shrap_ysize = 32;
                    }
                    break;
                case SKEL_RUN_R0:
                    p = SkelGore;
                    shrap_pal = PALETTE_SKEL_GORE;
                    break;
                case HORNET_RUN_R0:
                    shrap_pal = PALETTE_SKEL_GORE;
                    break;
                case SKULL_R0:
                case SKULL_R0 + 1:
                    p = FlamingGore;
                    break;
                case BETTY_R0:
                case TRASHCAN:
                case PACHINKO1:
                case PACHINKO2:
                case PACHINKO3:
                case PACHINKO4:
                case 623:
                    PlaySound(DIGI_BREAKGLASS, parent, v3df_dontpan | v3df_doppler);
                    p = MetalShrap;
                    shrap_xsize = shrap_ysize = 10;
                    break;
                case EMP:
                    p = EMPShrap;
                    shrap_xsize = shrap_ysize = 8;
                    break;
            }
        }

        // second sprite involved
        // most of the time is the weapon
        if (Secondary >= 0) {
            USER wu = getUser(Secondary);
            if (wu != null) {
                if (wu.PlayerP != -1 && Player[wu.PlayerP].sop_control != -1) {
                    p = StdShrap;
                } else {
                    if (wu.ID == PLASMA_FOUNTAIN) {
                        p = HeartAttackShrap;
                    }
                }
            }
        }

        int dang = 0;

        hz[Z_TOP] = SPRITEp_TOS(parent); // top
        hz[Z_BOT] = SPRITEp_BOS(parent); // bottom
        hz[Z_MID] = DIV2(hz[0] + hz[2]); // mid

        int shrap_ang = parent.getAng();
        int start_ang = 0;

        for (int ptr = 0; ptr < p.length && p[ptr] != null; ptr++) {
            if (!p[ptr].random_disperse) {
                // dang = (2048 / p.num);


                start_ang = NORM_ANGLE(shrap_ang - DIV2(p[ptr].ang_range));
                dang =  (p[ptr].ang_range / p[ptr].num);
            }

            for (int i = 0; i < p[ptr].num; i++) {
                int SpriteNum =  SpawnSprite(STAT_SKIP4, p[ptr].id, p[ptr].state[0], parent.getSectnum(), parent.getX(),
                        parent.getY(), hz[p[ptr].zlevel], shrap_ang, 512);

                Sprite sp = boardService.getSprite(SpriteNum);
                USER u = getUser(SpriteNum);
                if (sp == null || u == null) {
                    continue;
                }

                if (p[ptr].random_disperse) {
                    sp.setAng( (shrap_ang + (RANDOM_P2(p[ptr].ang_range << 5) >> 5) - DIV2(p[ptr].ang_range)));
                    sp.setAng(NORM_ANGLE(sp.getAng()));
                } else {
                    sp.setAng( (start_ang + (i * dang)));
                    sp.setAng(NORM_ANGLE(sp.getAng()));
                }

                // for FastShrap
                int WaitTics = 64;
                u.zchange = klabs(u.jump_speed * 4) - RANDOM_RANGE(klabs(u.jump_speed) * 8) * 2;
                u.WaitTics =  (WaitTics + RANDOM_RANGE(WaitTics / 2));

                switch (u.ID) {
                    case GORE_Drip:
                        shrap_bounce = false;
                        break;
                    case GORE_Lung:
                    case GORE_Liver:
                        shrap_xsize = 20;
                        shrap_ysize = 20;
                        shrap_bounce = false;
                        break;
                    case GORE_SkullCap:
                        shrap_xsize = 24;
                        shrap_ysize = 24;
                        shrap_bounce = true;
                        break;
                    case GORE_Arm:
                        shrap_xsize = 21;
                        shrap_ysize = 21;
                        shrap_bounce = false;
                        break;
                    case GORE_Head:
                        shrap_xsize = 26;
                        shrap_ysize = 30;
                        shrap_bounce = true;
                        break;
                    case Vomit1:
                        shrap_bounce = false;
                        sp.setZ(sp.getZ() - Z(4));
                        shrap_xsize =  (u.sx = 12 + (RANDOM_P2(32 << 8) >> 8));
                        shrap_ysize =  (u.sy = 12 + (RANDOM_P2(32 << 8) >> 8));
                        u.Counter =  (RANDOM_P2(2048 << 5) >> 5);

                        int nx = EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 6;
                        int ny = EngineUtils.sin(sp.getAng()) >> 6;
                        move_missile(SpriteNum, nx, ny, 0, Z(8), Z(8), CLIPMASK_MISSILE, MISSILEMOVETICS);

                        if (RANDOM_P2(1024) < 700) {
                            u.ID = 0;
                        }

                        break;
                    case EMP:
                        shrap_bounce = false;
                        sp.setZ(sp.getZ() - Z(4));
                        // sp.ang = NORM_ANGLE(sp.ang + 1024);
                        shrap_xsize =  (u.sx = 5 + (RANDOM_P2(4 << 8) >> 8));
                        shrap_ysize =  (u.sy = 5 + (RANDOM_P2(4 << 8) >> 8));
                        break;
                }

                sp.setShade((byte) -15);
                sp.setXrepeat(shrap_xsize);
                sp.setYrepeat(shrap_ysize);
                sp.setClipdist(16 >> 2);

                if (shrap_owner >= 0) {
                    SetOwner(shrap_owner, SpriteNum);
                }

                if (shrap_rand_zamt != 0) {
                    sp.setZ(sp.getZ() + Z(RANDOM_RANGE(shrap_rand_zamt) - (shrap_rand_zamt / 2)));
                }

                sp.setPal(u.spal = (byte) shrap_pal);

                sp.setXvel( (p[ptr].min_vel * 2));
                sp.setXvel(sp.getXvel() + RANDOM_RANGE(p[ptr].max_vel - p[ptr].min_vel));

                int shrap_floor_dist = Z(2);
                int shrap_ceiling_dist = Z(2);

                u.floor_dist =  shrap_floor_dist;
                u.ceiling_dist =  shrap_ceiling_dist;
                u.jump_speed = p[ptr].min_jspeed;
                u.jump_speed += RANDOM_RANGE(p[ptr].max_jspeed - p[ptr].min_jspeed);
                u.jump_speed =  -u.jump_speed;

                DoBeginJump(SpriteNum);
                u.jump_grav = ACTOR_GRAVITY;

                u.xchange = MOVEx(sp.getXvel(), sp.getAng());
                u.ychange = MOVEy(sp.getXvel(), sp.getAng());

                if (!shrap_bounce) {
                    u.Flags |= (SPR_BOUNCE);
                }
            }
        }

        return (retval);
    }

    private static class AutoShrapRet {
        SHRAP[] p;
        int size;
        boolean bounce;
    }

}
