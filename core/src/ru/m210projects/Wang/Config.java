package ru.m210projects.Wang;

import com.badlogic.gdx.Input.Keys;
import ru.m210projects.Build.Gameutils;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.settings.*;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.stream.IntStream;

import static ru.m210projects.Build.input.keymap.Keymap.MOUSE_RBUTTON;

public class Config extends GameConfig {

    public static final int[] defkeys = {
            Keys.W, // Move_Forward 0
            Keys.S, // Move_Backward 1
            Keys.LEFT, // Turn_Left 2
            Keys.RIGHT, // Turn_Right 3
            Keys.BACKSPACE, // Turn_Around 4
            Keys.ALT_LEFT, // Strafe 5
            Keys.A, // Strafe_Left 6
            Keys.D, // Strafe_Right 7
            Keys.SPACE, // Jump 8
            Keys.CONTROL_LEFT, // Crouch 9
            Keys.SHIFT_LEFT, // Run 10
            Keys.E, // Open 12
            Keys.CONTROL_RIGHT, // Weapon_Fire 13
            Keys.APOSTROPHE, // Next_Weapon 44
            Keys.SEMICOLON, // Previous_Weapon 45
            Keys.PAGE_UP, // Look_Up 18
            Keys.PAGE_DOWN, // Look_Down 19
            Keys.TAB, // Map_Toggle 35
            Keys.EQUALS, // Enlarge_Screen 38
            Keys.MINUS, // Shrink_Screen 37
            Keys.T, // Send_Message 39
            Keys.U, // Mouse_Aiming 42
            Keys.ESCAPE, // Open_menu 53
            Keys.GRAVE, // Show_Console 54

            Keys.CAPS_LOCK, // AutoRun 11
            Keys.HOME, // Aim_Up 15
            Keys.END, // Aim_Down 16
            Keys.NUMPAD_5, // Aim_Center 17
            Keys.INSERT, // Tilt_Left 20
            Keys.FORWARD_DEL, // Tilt_Right 21
            Keys.NUM_1, // Weapon_1 22
            Keys.NUM_2, // Weapon_2 23
            Keys.NUM_3, // Weapon_3 24
            Keys.NUM_4, // Weapon_4 25
            Keys.NUM_5, // Weapon_5 26
            Keys.NUM_6, // Weapon_6 27
            Keys.NUM_7, // Weapon_7 28
            Keys.NUM_8, // Weapon_8 29
            Keys.NUM_9, // Weapon_9 30
            Keys.NUM_0, // Weapon_10 31
            Keys.ENTER, // Inventory_Use 32
            Keys.LEFT_BRACKET, // Inventory_Left 33
            Keys.RIGHT_BRACKET, // Inventory_Right 34
            Keys.F, // Map_Follow_Mode 36
            Keys.K, // See_Coop_View 40
            Keys.F7, // See_Chase_View 41
            Keys.I, // Toggle_Crosshair 43
            Keys.BACKSLASH, // Holster_Weapon 46
            Keys.Y, // Show_Opponents_Weapon 47
            Keys.N, // NightVision 48
            Keys.H, // SmokeBomb 49
            Keys.G, // GasBomb 50
            Keys.M, // MedKit 51
            Keys.B, // FlashBomb 52
            Keys.C, // Caltrops
            Keys.F1, // Show_HelpScreen 55
            Keys.F2, // Show_Save 56
            Keys.F3, // Show_Load 57
            Keys.F4, // Show_Sounds 58
            Keys.F5, // Show_Options 59
            Keys.F6, // QuickSave 60
            0, // ToggleMessages 61
            Keys.F9, // QuickLoad 62
            Keys.F10, // Quit 63
            Keys.F11, // Gamma 64
            Keys.F12, // MakeScreenshot 65
            0, // Last_Weapon_Switch
            0, // Crouch_toggle
            0, //Special_Fire
    };
    public static final int[] defclassickeys = {
            Keys.UP, // Move_Forward 0
            Keys.DOWN, // Move_Backward 1
            Keys.LEFT, // Turn_Left 2
            Keys.RIGHT, // Turn_Right 3
            Keys.BACKSPACE, // Turn_Around 4
            Keys.ALT_LEFT, // Strafe 5
            Keys.COMMA, // Strafe_Left 6
            Keys.PERIOD, // Strafe_Right 7
            Keys.A, // Jump 8
            Keys.Z, // Crouch 9
            Keys.SHIFT_LEFT, // Run 10
            Keys.SPACE, // Open 12
            Keys.CONTROL_LEFT, // Weapon_Fire 13
            Keys.APOSTROPHE, // Next_Weapon 44
            Keys.SEMICOLON, // Previous_Weapon 45
            Keys.PAGE_UP, // Look_Up 18
            Keys.PAGE_DOWN, // Look_Down 19
            Keys.TAB, // Map_Toggle 35
            Keys.EQUALS, // Enlarge_Screen 38
            Keys.MINUS, // Shrink_Screen 37
            Keys.T, // Send_Message 39
            Keys.U, // Mouse_Aiming 42
            Keys.ESCAPE, // Open_menu 53
            Keys.GRAVE, // Show_Console 54



            Keys.CAPS_LOCK, // AutoRun 11
            Keys.HOME, // Aim_Up 15
            Keys.END, // Aim_Down 16
            Keys.NUMPAD_5, // Aim_Center 17
            Keys.INSERT, // Tilt_Left 20
            Keys.FORWARD_DEL, // Tilt_Right 21
            Keys.NUM_1, // Weapon_1 22
            Keys.NUM_2, // Weapon_2 23
            Keys.NUM_3, // Weapon_3 24
            Keys.NUM_4, // Weapon_4 25
            Keys.NUM_5, // Weapon_5 26
            Keys.NUM_6, // Weapon_6 27
            Keys.NUM_7, // Weapon_7 28
            Keys.NUM_8, // Weapon_8 29
            Keys.NUM_9, // Weapon_9 30
            Keys.NUM_0, // Weapon_10 31
            Keys.ENTER, // Inventory_Use 32
            Keys.LEFT_BRACKET, // Inventory_Left 33
            Keys.RIGHT_BRACKET, // Inventory_Right 34
            Keys.F, // Map_Follow_Mode 36
            Keys.K, // See_Coop_View 40
            Keys.F7, // See_Chase_View 41
            Keys.I, // Toggle_Crosshair 43
            Keys.SCROLL_LOCK, // Holster_Weapon 46
            Keys.W, // Show_Opponents_Weapon 47
            Keys.N, // NightVision 48
            Keys.S, // SmokeBomb 49
            Keys.G, // GasBomb 50
            Keys.M, // MedKit 51
            Keys.B, // FlashBomb 52 //F original
            Keys.C, // Caltrops
            Keys.F1, // Show_HelpScreen 55
            Keys.F2, // Show_Save 56
            Keys.F3, // Show_Load 57
            Keys.F4, // Show_Sounds 58
            Keys.F5, // Show_Options 59
            Keys.F6, // QuickSave 60
            Keys.F8, // ToggleMessages 61
            Keys.F9, // QuickLoad 62
            Keys.F10, // Quit 63
            Keys.F11, // Gamma 64
            Keys.F12, // MakeScreenshot 65
            0, // Last_Weapon_Switch
            0, // Crouch_toggle
            0, //Special_Fire
    };
    public byte NetColor = 0;
    public boolean AutoAim = false;
    public boolean Ambient = true;
    public int BorderNum = 1;
    public boolean Messages = true;
    public boolean Crosshair = true;
    public int CrosshairSize = 65536;
    public boolean Shadows = true;
    public boolean ParentalLock;
    public boolean AutoRun = true;
    public int Stats = 1;
    public int gStatSize = 65536;
    public boolean SlopeTilting = false;
    public int showMapInfo = 1;
    public int gDemoSeq = 1;
    public boolean WeaponAutoSwitch = true;
    public boolean UseDarts = false;
    public boolean DisableHornets = false;
    public boolean ShowWeapon = false;
    public int weaponIndex = -1;

    public final String[] WangBangMacro = {"Burn baby burn...", "You make another stupid move.",
            "Blocking with your head again?", "You not fight well with hands!", "You so stupid!",
            "Quit jerking off. Come fight me!", "What the matter you scaredy cat?", "Did I break your concentration?",
            "Hope you were paying attention.", "ITTAIIIUUU!!!"};

    public Config(Path path) {
        super(path);
        setpName("Kato");
    }

    @Override
    protected InputContext createDefaultInputContext() {
        return new InputContext(getKeyMap(), defkeys, defclassickeys) {

            @Override
            protected void clearInput() {
                super.clearInput();
                weaponIndex = IntStream.range(0, keymap.length).filter(i -> keymap[i].equals(SwKeys.Weapon_1)).findFirst().orElse(-1);
            }

            @Override
            public void resetInput(boolean classicKeys) {
                super.resetInput(classicKeys);

                primarykeys[MOUSE_KEYS_INDEX][SwKeys.Special_Fire.getNum()] = MOUSE_RBUTTON;
            }
        };
    }

    @Override
    protected ConfigContext createDefaultGameContext() {
        return new ConfigContext() {
            @Override
            public void load(Properties prop) {
                if (prop.setContext("Options")) {
                    NetColor = (byte) prop.getIntValue("NetColor", NetColor);
                    AutoAim = prop.getBooleanValue("AutoAim", AutoAim);
                    Crosshair = prop.getBooleanValue("Crosshair", Crosshair);
                    CrosshairSize = Math.max(prop.getIntValue("CrossSize", CrosshairSize), 16384);
                    Messages = prop.getBooleanValue("MessageState", Messages);
                    Shadows = prop.getBooleanValue("Shadows", Shadows);
                    Ambient = prop.getBooleanValue("Ambient", Ambient);
                    SlopeTilting = prop.getBooleanValue("Tilt", SlopeTilting);
                    Stats = prop.getIntValue("ShowStat", Stats);
                    gStatSize = Math.max(prop.getIntValue("StatSize", gStatSize), 16384);
                    showMapInfo = prop.getIntValue("showMapInfo", showMapInfo);
                    AutoRun = prop.getBooleanValue("AutoRun", AutoRun);
                    BorderNum = Gameutils.BClipRange(prop.getIntValue("Size", BorderNum), 0, 2);
                    WeaponAutoSwitch = prop.getBooleanValue("WeaponAutoSwitch", WeaponAutoSwitch);
                    UseDarts = prop.getBooleanValue("UseDarts", UseDarts);
                    DisableHornets = prop.getBooleanValue("DisableHornets", DisableHornets);
                    ShowWeapon = prop.getBooleanValue("ShowOpponentsWeapon", ShowWeapon);
                    gDemoSeq = prop.getIntValue("DemoSequence", gDemoSeq);
                }
            }

            @Override
            public void save(OutputStream os) throws IOException {
                putString(os, "[Options]\r\n");
                //Options
                putInteger(os, "NetColor", NetColor);
                putBoolean(os, "AutoAim", AutoAim);
                putBoolean(os, "Crosshair", Crosshair);
                putInteger(os, "CrossSize", CrosshairSize);
                putBoolean(os, "MessageState", Messages);
                putBoolean(os, "Shadows", Shadows);
                putBoolean(os, "Ambient", Ambient);
                putBoolean(os, "Tilt", SlopeTilting);
                putInteger(os, "ShowStat", Stats);
                putInteger(os, "StatSize", gStatSize);
                putInteger(os, "showMapInfo", showMapInfo);
                putBoolean(os, "AutoRun", AutoRun);
                putInteger(os, "Size", BorderNum);
                putBoolean(os, "WeaponAutoSwitch", WeaponAutoSwitch);
                putBoolean(os, "UseDarts", UseDarts);
                putBoolean(os, "DisableHornets", DisableHornets);
                putBoolean(os, "ShowOpponentsWeapon", ShowWeapon);
                putInteger(os, "DemoSequence", gDemoSeq);
            }
        };
    }
    
    public GameKey[] getKeyMap() {
         return new GameKey[]{GameKeys.Move_Forward, GameKeys.Move_Backward, GameKeys.Turn_Left, GameKeys.Turn_Right,
                GameKeys.Turn_Around, GameKeys.Strafe, GameKeys.Strafe_Left, GameKeys.Strafe_Right, GameKeys.Jump,
                GameKeys.Crouch, GameKeys.Run, SwKeys.AutoRun, GameKeys.Open, GameKeys.Weapon_Fire, SwKeys.Special_Fire, SwKeys.Aim_Up,
                SwKeys.Aim_Down, SwKeys.Aim_Center, GameKeys.Look_Up, GameKeys.Look_Down, SwKeys.Tilt_Left,
                SwKeys.Tilt_Right, SwKeys.Weapon_1, SwKeys.Weapon_2, SwKeys.Weapon_3, SwKeys.Weapon_4, SwKeys.Weapon_5,
                SwKeys.Weapon_6, SwKeys.Weapon_7, SwKeys.Weapon_8, SwKeys.Weapon_9, SwKeys.Weapon_10,
                SwKeys.Inventory_Use, SwKeys.Inventory_Left, SwKeys.Inventory_Right, GameKeys.Map_Toggle,
                SwKeys.Map_Follow_Mode, GameKeys.Shrink_Screen, GameKeys.Enlarge_Screen, GameKeys.Send_Message,
                SwKeys.See_Coop_View, SwKeys.See_Chase_View, GameKeys.Mouse_Aiming, SwKeys.Toggle_Crosshair,
                GameKeys.Next_Weapon, GameKeys.Previous_Weapon, SwKeys.Holster_Weapon, SwKeys.Show_Opp_Weapon,
                SwKeys.NightVision, SwKeys.SmokeBomb, SwKeys.GasBomb, SwKeys.MedKit, SwKeys.FlashBomb, SwKeys.Caltrops,
                GameKeys.Menu_Toggle, GameKeys.Show_Console, SwKeys.Show_Help, SwKeys.Show_Savemenu,
                SwKeys.Show_Loadmenu, SwKeys.Show_Sounds, SwKeys.Show_Options, SwKeys.Quicksave, SwKeys.Messages,
                SwKeys.Quickload, SwKeys.Quit, SwKeys.Gamma, SwKeys.Screenshot, SwKeys.Last_Weap_Switch,
                SwKeys.Crouch_toggle};
        
    }

    public enum SwKeys implements GameKey {
        AutoRun,
        Aim_Up,
        Aim_Down,
        Aim_Center,
        Tilt_Left,
        Tilt_Right,
        Weapon_1,
        Weapon_2,
        Weapon_3,
        Weapon_4,
        Weapon_5,
        Weapon_6,
        Weapon_7,
        Weapon_8,
        Weapon_9,
        Weapon_10,
        Inventory_Use,
        Inventory_Left,
        Inventory_Right,
        Map_Follow_Mode,
        See_Coop_View,
        See_Chase_View,
        Toggle_Crosshair,
        Holster_Weapon,
        Show_Opp_Weapon,
        NightVision,
        SmokeBomb,
        GasBomb,
        MedKit,
        FlashBomb,
        Caltrops,
        Show_Help,
        Show_Savemenu,
        Show_Loadmenu,
        Show_Sounds,
        Show_Options,
        Quicksave,
        Messages,
        Quickload,
        Quit,
        Gamma,
        Screenshot,
        Last_Weap_Switch,
        Crouch_toggle,
        Special_Fire;

        public int getNum() {
            return GameKeys.values().length + ordinal();
        }

        public String getName() {
            return name();
        }

    }

}
