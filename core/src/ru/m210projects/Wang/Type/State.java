package ru.m210projects.Wang.Type;

public class State extends Saveable {

    public final int Pic;
    public final int Tics;
    public final Animator Animator;
    public int id;

    private State NextState;
    private StateGroup Nextgroup;

    public State(int pic, int tics, Animator anim) {
        this.id = 0;
        this.Pic =  pic;
        this.Tics = tics;
        this.Animator = anim;
    }

    public static void InitState(State[] list) {
        int len = list.length;
        for (int st = 0; st < len; st++) {
            if (list[st].getNext() == null) {
                list[st].setNext(list[(st + 1) % len]);
            }
            list[st].id = st;
        }
    }

    public State setNext() {
        this.NextState = this;
        return this;
    }

    public State getNext() {
        return NextState;
    }

    public State setNext(State pState) {
        this.NextState = pState;
        return this;
    }

    public StateGroup getNextGroup() {
        return Nextgroup;
    }

    public State setNextGroup(StateGroup group) {
        this.Nextgroup = group;
        return this;
    }
}
