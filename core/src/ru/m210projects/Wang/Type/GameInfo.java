package ru.m210projects.Wang.Type;

public class GameInfo {

    public String Title;
    public EpisodeInfo[] episode;
    public final String[] skills = {"Tiny grasshopper", "I Have No Fear", "Who Wants Wang", "No Pain, No Gain"};
    private final EpisodeEntry addonEntry;

    public GameInfo(String title, EpisodeEntry addonEntry, EpisodeInfo... eps) {
        this.Title = title;
        this.addonEntry = addonEntry;
        this.episode = eps;
    }

    public String getPath() {
        return addonEntry.getFileEntry().getRelativePath().toString();
    }

    public long getChecksum() {
        if (addonEntry.isPackageEpisode()) {
            return addonEntry.getFileEntry().getChecksum();
        }

        return 0;
    }

    public EpisodeEntry getEpisodeEntry() {
        return addonEntry;
    }

    public LevelInfo getLevel(int num) {
        if (num < 1) {
            return null;
        }

        if (num < 5) {
            return episode[0] != null ? episode[0].gMapInfo[num - 1] : null;
        }
        return episode[1] != null ? episode[1].gMapInfo[num - 5] : null;
    }

    public int getNumEpisode(int level) {
        if (level < 5) {
            return 0;
        }
        return 1;
    }

    public int getNumLevel(int level) {
        if (level < 5) {
            return level - 1;
        }

        return level - 5;
    }

    public String getMapTitle(int num) {
        LevelInfo info = getLevel(num);
        if (info != null) {
            return info.Description;
        }
        return null;
    }

    public String getMapPath(int num) {
        LevelInfo info = getLevel(num);
        if (info != null) {
            return info.LevelName;
        }
        return "";
    }
}
