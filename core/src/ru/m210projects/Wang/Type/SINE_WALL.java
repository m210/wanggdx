package ru.m210projects.Wang.Type;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SINE_WALL {
    public int orig_xy, range;
    public int wall, sintable_ndx, speed_shift, type;

    public void reset() {
        orig_xy = -1;
        range = -1;
        wall = -1;
        sintable_ndx = -1;
        speed_shift = -1;
        type = -1;
    }

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, orig_xy);
        StreamUtils.writeInt(os, range);
        StreamUtils.writeShort(os, wall);
        StreamUtils.writeShort(os, sintable_ndx);
        StreamUtils.writeShort(os, speed_shift);
        StreamUtils.writeShort(os, type);
    }

    public void load(InputStream is) throws IOException {
        orig_xy = StreamUtils.readInt(is);
        range = StreamUtils.readInt(is);
        wall = StreamUtils.readShort(is);
        sintable_ndx = StreamUtils.readShort(is);
        speed_shift = StreamUtils.readShort(is);
        type = StreamUtils.readShort(is);
    }

    public static SINE_WALL copy(SINE_WALL src) {
        SINE_WALL cp = new SINE_WALL();
        cp.orig_xy = src.orig_xy;
        cp.range = src.range;
        cp.wall = src.wall;
        cp.sintable_ndx = src.sintable_ndx;
        cp.speed_shift = src.speed_shift;
        cp.type = src.type;
        return cp;
    }
}
