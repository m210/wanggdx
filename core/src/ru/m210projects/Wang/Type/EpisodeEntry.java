package ru.m210projects.Wang.Type;

import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;

import static ru.m210projects.Wang.Type.ResourceHandler.episodeManager;

public interface EpisodeEntry {

    FileEntry getFileEntry(); // directory entry file or group (parent group) when map files placed

    Group getGroup();

    boolean isPackageEpisode();

    class Addon extends FileEntry implements EpisodeEntry {

        private final Group group;
        private final FileEntry entry;

        public Addon(Group group, FileEntry groupEntry) {
            super(groupEntry);
            this.group = group;
            this.entry = groupEntry;
        }

        @Override
        public String getName() {
            GameInfo gameInfo = episodeManager.getEpisode(this);
            if (gameInfo != null) {
                return "Addon:" + gameInfo.Title;
            }
            return "Broken entry";
        }

        @Override
        public FileEntry getFileEntry() {
            return entry;
        }

        @Override
        public Group getGroup() {
            return group;
        }

        @Override
        public boolean isPackageEpisode() {
            return !(group instanceof Directory);
        }
    }
}
