package ru.m210projects.Wang.Type;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.BitMap;
import ru.m210projects.Build.Types.collections.DynamicArray;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Factory.WangSprite;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JSector.MAXMIRRORS;
import static ru.m210projects.Wang.LoadSave.*;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Track.MAX_TRACKS;
import static ru.m210projects.Wang.Type.ResourceHandler.episodeManager;
import static ru.m210projects.Wang.Weapon.*;

public class SafeLoader {

    public int Level, Skill;
    public Entry boardfilename;
    public int numsectors;
    public Sector[] sector;
    public Wall[] wall;
    public java.util.List<Sprite> sprite;
    public int playTrack;
    public int FinishTimer;
    public boolean FinishedLevel;
    public int FinishAnim;
    public final PlayerStr[] Player = new PlayerStr[MAX_SW_PLAYERS];
    public short connecthead;
    public final short[] connectpoint2 = new short[MAXPLAYERS];
    public short numplayers;
    public int CommPlayers;
    public short myconnectindex;
    public final Array<Sect_User> SectUser = new DynamicArray<>(MAXSECTORSV7, Sect_User.class, false);
    public final Array<USER> pUser = new DynamicArray<>(MAXSPRITESV7, USER.class, false);
    public final Sector_Object[] SectorObject = new Sector_Object[MAX_SECTOR_OBJECTS];
    public final SINE_WAVE_FLOOR[][] SineWaveFloor = new SINE_WAVE_FLOOR[MAX_SINE_WAVE][21];
    public final SINE_WALL[][] SineWall = new SINE_WALL[MAX_SINE_WALL][MAX_SINE_WALL_POINTS];
    public final Spring_Board[] SpringBoard = new Spring_Board[20];
    public int x_min_bound, y_min_bound, x_max_bound, y_max_bound;
    public final TRACK[] Track = new TRACK[MAX_TRACKS];
    public int screenpeek;
    public int totalsynctics;
    public final Anim[] pAnim = new Anim[MAXANIM];
    public int AnimCnt;
    public final short[] pskyoff = new short[MAXPSKYTILES];
    public short pskybits;
    public int randomseed;
    public int totalclock, visibility;
    public short NormalVisibility;
    public int parallaxyoffs;
    public byte parallaxtype;
    public boolean MoveSkip2;
    public int MoveSkip4, MoveSkip8;
    public final MirrorType[] mirror = new MirrorType[MAXMIRRORS];
    public boolean mirrorinview;
    public int mirrorcnt;
    public int StarQueueHead = 0;
    public final IntArray StarQueue = new IntArray(MAX_STAR_QUEUE);
    public int HoleQueueHead = 0;
    public final IntArray HoleQueue = new IntArray(MAX_HOLE_QUEUE);
    public int WallBloodQueueHead = 0;
    public final IntArray WallBloodQueue = new IntArray(MAX_WALLBLOOD_QUEUE);
    public int FloorBloodQueueHead = 0;
    public final IntArray FloorBloodQueue = new IntArray(MAX_FLOORBLOOD_QUEUE);
    public int GenericQueueHead = 0;
    public final IntArray GenericQueue = new IntArray(MAX_GENERIC_QUEUE);
    public int LoWangsQueueHead = 0;
    public final IntArray LoWangsQueue = new IntArray(MAX_LOWANGS_QUEUE);
    public int PlayClock;
    public int Kills, TotalKillable;
    public short LevelSecrets;
    public final int[] picanm = new int[MAXTILES];
    public final BitMap show2dsector = new BitMap(MAXSECTORSV7);
    public final BitMap show2dwall = new BitMap(MAXWALLSV7);
    public final BitMap show2dsprite = new BitMap(MAXSPRITESV7);
    public int Bunny_Count;
    public boolean GodMode;
    public boolean serpwasseen, sumowasseen, zillawasseen;
    public final IntArray BossSpriteNum = new IntArray(3);
    //Network settings

    public int TimeLimit;
    public int TimeLimitClock;
    public MultiGameTypes MultiGameType = MultiGameTypes.MULTI_GAME_NONE; // used to be a stand alone global
    public boolean TeamPlay;
    public boolean HurtTeammate;
    public boolean SpawnMarkers;
    public boolean NoRespawn;
    public boolean Nuke = true;

    public GameInfo addon;
    public byte warp_on;
    public String addonFileName;
    private String message;

    public SafeLoader() {
        for (int i = 0; i < MAXANIM; i++) {
            pAnim[i] = new Anim();
        }

        for (int i = 0; i < MAX_SW_PLAYERS; i++) {
            Player[i] = new PlayerStr();
        }

        for (int i = 0; i < MAX_SECTOR_OBJECTS; i++) {
            SectorObject[i] = new Sector_Object();
        }

        for (int i = 0; i < 20; i++) {
            SpringBoard[i] = new Spring_Board();
        }

        for (int i = 0; i < MAX_SINE_WALL; i++) {
            for (int j = 0; j < MAX_SINE_WALL_POINTS; j++) {
                SineWall[i][j] = new SINE_WALL();
            }
        }

        for (int i = 0; i < MAX_SINE_WAVE; i++) {
            for (int j = 0; j < 21; j++) {
                SineWaveFloor[i][j] = new SINE_WAVE_FLOOR();
            }
        }
    }

    public boolean load(InputStream is) throws IOException {
        message = null;

        addon = LoadGDXHeader(is);
        pUser.clear();
        SectUser.clear();

        StarQueue.clear();
        HoleQueue.clear();
        WallBloodQueue.clear();
        FloorBloodQueue.clear();
        GenericQueue.clear();
        LoWangsQueue.clear();

        LoadMap(is);
//        LoadPanelSprites(is);
        LoadPlayers(is);
        LoadSectorUserInfos(is);
        LoadUserInfos(is);
        LoadSectorObjects(is);
        LoadSineSect(is);

        x_min_bound = StreamUtils.readInt(is);
        y_min_bound = StreamUtils.readInt(is);
        x_max_bound = StreamUtils.readInt(is);
        y_max_bound = StreamUtils.readInt(is);

        LoadTracks(is);

        screenpeek = StreamUtils.readInt(is);
        totalsynctics = StreamUtils.readInt(is);

        LoadAnims(is);

        totalclock = StreamUtils.readInt(is);
        randomseed = StreamUtils.readInt(is);
        NormalVisibility = StreamUtils.readShort(is);
        visibility = StreamUtils.readInt(is);
        parallaxtype = StreamUtils.readByte(is);
        parallaxyoffs = StreamUtils.readInt(is);
        for (int i = 0; i < pskyoff.length; i++) {
            pskyoff[i] = StreamUtils.readShort(is);
        }
        pskybits = StreamUtils.readShort(is);
        MoveSkip2 = StreamUtils.readBoolean(is);
        MoveSkip4 = StreamUtils.readInt(is);
        MoveSkip8 = StreamUtils.readInt(is);

        LoadMirrors(is);
        LoadQueues(is);
        LoadStuff(is);

        if (warp_on == 1 && addon == null) { // try to find addon
            message = "Can't find user episode file: " + addonFileName;
            warp_on = 2;

            Level = 0;
        }

        System.out.println(is.available());
        return is.available() == 0;
    }

    public String getMessage() {
        return message;
    }

    public GameInfo LoadGDXHeader(InputStream is) {
        Level = -1;
        Skill = -1;
        warp_on = 0;
        addon = null;
        addonFileName = null;

        try {
            StreamUtils.skip(is, SAVETIME + SAVENAME);

            Level = StreamUtils.readInt(is);
            Skill = StreamUtils.readInt(is);

            StreamUtils.skip(is, SAVESCREENSHOTSIZE);

            LoadUserEpisodeInfo(is);
            if (warp_on == 1) {// try to find addon
                addon = findAddon(addonFileName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return addon;
    }

    public void LoadUserEpisodeInfo(InputStream is) throws IOException {
        warp_on = StreamUtils.readByte(is);
        if (warp_on == 1) {
            StreamUtils.readBoolean(is);
            addonFileName = StreamUtils.readDataString(is).toLowerCase();
        }
    }

    private void LoadPlayers(InputStream is) throws IOException {
        numplayers = StreamUtils.readShort(is);
        CommPlayers = StreamUtils.readShort(is);
        myconnectindex = StreamUtils.readShort(is);
        connecthead = StreamUtils.readShort(is);
        for (int i = 0; i < connectpoint2.length; i++) {
            connectpoint2[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < numplayers; i++) {
            Player[i].readObject(is);
        }
    }

    private void LoadMap(InputStream is) throws IOException {
        boardfilename = DUMMY_ENTRY;
        String name = StreamUtils.readString(is, 144);
        if (!name.isEmpty()) {
            boardfilename = game.getCache().getEntry(name, true);
        }

        numsectors = StreamUtils.readInt(is);
        sector = new Sector[numsectors];
        for (int i = 0; i < sector.length; i++) {
            sector[i] = new Sector().readObject(is);
        }

        wall = new Wall[StreamUtils.readInt(is)];
        for (int i = 0; i < wall.length; i++) {
            wall[i] = new Wall().readObject(is);
        }

        int numSprites = StreamUtils.readInt(is);
        sprite = new ArrayList<>(numSprites * 2);

        for (int i = 0; i < numSprites; i++) {
            sprite.add(new WangSprite().readObject(is));
        }

        FinishTimer = StreamUtils.readInt(is);
        FinishedLevel = StreamUtils.readBoolean(is);
        FinishAnim = StreamUtils.readInt(is);
        playTrack = StreamUtils.readInt(is);
    }

    private void LoadSectorUserInfos(InputStream is) throws IOException {
        // Sector User information
        int len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            int sectnum = StreamUtils.readInt(is);
            if (sectnum != -1) {
                Sect_User pSectUser = new Sect_User();
                pSectUser.readObject(is);
                SectUser.set(sectnum, pSectUser);
            }
        }
    }

    private void LoadUserInfos(InputStream is) throws IOException {
        int len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            int snum = StreamUtils.readInt(is);
            if (snum != -1) {
                USER user = new USER();
                user.readObject(is);
                pUser.set(snum, user);
            }
        }
    }

    private void LoadSectorObjects(InputStream is) throws IOException {
        // Sector User information
        int len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            SectorObject[i].readObject(is);
        }
    }

    private void LoadSineSect(InputStream is) throws IOException {
        int len = StreamUtils.readInt(is);
        int len2 = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len2; j++) {
                SineWaveFloor[i][j].load(is);
            }
        }

        len = StreamUtils.readInt(is);
        len2 = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len2; j++) {
                SineWall[i][j].load(is);
            }
        }

        len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            SpringBoard[i].load(is);
        }
    }

    private void LoadTracks(InputStream is) throws IOException {
        int len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            Track[i] = new TRACK();
            TRACK tr = Track[i];

            tr.ttflags = StreamUtils.readInt(is);
            tr.flags = StreamUtils.readShort(is);
            tr.NumPoints = StreamUtils.readShort(is);

            if (tr.NumPoints == 0) {
                tr.TrackPoint = new TRACK_POINT[1];
            } else {
                tr.TrackPoint = new TRACK_POINT[tr.NumPoints];
                for (int j = 0; j < tr.NumPoints; j++) {
                    TRACK_POINT tp = tr.TrackPoint[j] = new TRACK_POINT();

                    tp.x = StreamUtils.readInt(is);
                    tp.y = StreamUtils.readInt(is);
                    tp.z = StreamUtils.readInt(is);

                    tp.ang = StreamUtils.readShort(is);
                    tp.tag_low = StreamUtils.readShort(is);
                    tp.tag_high = StreamUtils.readShort(is);
                }
            }
        }
    }

    private void LoadAnims(InputStream is) throws IOException {
        AnimCnt = StreamUtils.readInt(is);
        for (int i = 0; i < AnimCnt; i++) {
            pAnim[i].load(is);
        }
    }

    private void LoadMirrors(InputStream is) throws IOException {
        mirrorcnt = StreamUtils.readInt(is);
        mirrorinview = StreamUtils.readBoolean(is);
        int len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            mirror[i] = new MirrorType();
            mirror[i].load(is);
        }
    }

    private void LoadQueues(InputStream is) throws IOException {
        StarQueueHead = StreamUtils.readInt(is);
        readBytes(is, StarQueue);
        HoleQueueHead = StreamUtils.readInt(is);
        readBytes(is, HoleQueue);
        WallBloodQueueHead = StreamUtils.readInt(is);
        readBytes(is, WallBloodQueue);
        FloorBloodQueueHead = StreamUtils.readInt(is);
        readBytes(is, FloorBloodQueue);
        GenericQueueHead = StreamUtils.readInt(is);
        readBytes(is, GenericQueue);
        LoWangsQueueHead = StreamUtils.readInt(is);
        readBytes(is, LoWangsQueue);
    }

    private void LoadStuff(InputStream is) throws IOException {
        PlayClock = StreamUtils.readInt(is);
        Kills = StreamUtils.readInt(is);
        TotalKillable = StreamUtils.readInt(is);

        StreamUtils.readInt(is); // KillLimit
        TimeLimit = StreamUtils.readInt(is);
        TimeLimitClock = StreamUtils.readInt(is);

        int i = StreamUtils.readUnsignedByte(is);
        MultiGameType = i != -1 ? MultiGameTypes.values()[i] : null;

        TeamPlay = StreamUtils.readBoolean(is);
        HurtTeammate = StreamUtils.readBoolean(is);
        SpawnMarkers = StreamUtils.readBoolean(is);
        NoRespawn = StreamUtils.readBoolean(is);
        Nuke = StreamUtils.readBoolean(is);

        for (i = 0; i < MAXTILES; i++) {
            picanm[i] = StreamUtils.readInt(is);
        }
        LevelSecrets = StreamUtils.readShort(is);

        show2dsector.readObject(is);
        show2dwall.readObject(is);
        show2dsprite.readObject(is);

        Bunny_Count = StreamUtils.readInt(is);
        GodMode = StreamUtils.readBoolean(is);
        serpwasseen = StreamUtils.readBoolean(is);
        sumowasseen = StreamUtils.readBoolean(is);
        zillawasseen = StreamUtils.readBoolean(is);

        readBytes(is, BossSpriteNum);
    }

    private static void readBytes(InputStream is, IntArray array) throws IOException {
        array.clear();
        int len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            array.add(StreamUtils.readInt(is));
        }
    }

    public static GameInfo findAddon(String addonFileName) {
        try {
            FileEntry addonEntry = game.getCache().getGameDirectory().getEntry(FileUtils.getPath(addonFileName));
            if (addonEntry.exists()) {
                return episodeManager.getEpisode(addonEntry);
            }
        } catch (Exception e) {
            Console.out.println(e.toString(), OsdColor.RED);
        }
        return null;
    }

}
