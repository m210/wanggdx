package ru.m210projects.Wang.Type;

import org.jetbrains.annotations.NotNull;

public class Break_Info implements Comparable<Object> {
    public final int picnum;
    public final int breaknum;
    public final int shrap_type;
    public int flags, shrap_amt;

    public Break_Info(int picnum, int breaknum, int shrap_type) {
        this.picnum =  picnum;
        this.breaknum =  breaknum;
        this.shrap_type =  shrap_type;
    }


    public Break_Info(int picnum, int breaknum, int shrap_type, int flags) {
        this(picnum, breaknum, shrap_type);
        this.flags =  flags;
    }

    public Break_Info(int picnum, int breaknum, int shrap_type, int flags, int shrap_amt) {
        this(picnum, breaknum, shrap_type, flags);
        this.shrap_amt =  shrap_amt;
    }


    @Override
    public int compareTo(@NotNull Object info) {
        if (info instanceof Number) {
            int picInfo = ((Number) info).intValue();
            return ( picInfo - this.picnum);
        } else if (info instanceof Break_Info) {
            return (((Break_Info) info).picnum - this.picnum);
        }
        return 0;
    }
}
