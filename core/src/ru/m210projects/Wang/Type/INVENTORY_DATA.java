package ru.m210projects.Wang.Type;

import ru.m210projects.Wang.Inv.Inventory_Stop_Func;
import ru.m210projects.Wang.Player.Player_Action_Func;

public class INVENTORY_DATA {
    public final String Name;
    public final Player_Action_Func init;
    public final Inventory_Stop_Func stop;
    public final Panel_State State;
    public final int DecPerSec;
    public final int MaxInv;
    public final int Scale;
    public final int Flags;

    public INVENTORY_DATA(String name, Player_Action_Func init, Inventory_Stop_Func stop, Panel_State[] State, int DecPerSec, int MaxInv, int Scale, int Flags) {
        this.Name = name;
        this.init = init;
        this.stop = stop;
        this.State = State[0];
        this.DecPerSec =  DecPerSec;
        this.MaxInv =  MaxInv;
        this.Scale = Scale;
        this.Flags =  Flags;
    }
}
