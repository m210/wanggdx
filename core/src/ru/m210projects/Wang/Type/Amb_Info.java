package ru.m210projects.Wang.Type;

public class Amb_Info {
    public final int diginame;
    public final int ambient_flags;
    public final int maxtics; // When tics reaches this number next

    public Amb_Info(int diginame, int ambient_flags, int maxtics) {
        this.diginame = diginame;
        this.ambient_flags = ambient_flags;
        this.maxtics = maxtics;
    }
}
