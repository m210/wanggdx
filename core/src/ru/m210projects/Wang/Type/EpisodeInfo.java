package ru.m210projects.Wang.Type;

import static ru.m210projects.Wang.Gameutils.MAX_LEVELS;

public class EpisodeInfo {

    public String Title;
    public String Description;
    public int nMaps;
    public final LevelInfo[] gMapInfo;

    public EpisodeInfo() {
        gMapInfo = new LevelInfo[MAX_LEVELS + 1];
    }

    public EpisodeInfo(String title, String Description, LevelInfo[] maps) {
        this.Title = title;
        this.Description = Description;
        this.gMapInfo = new LevelInfo[MAX_LEVELS + 1];
        System.arraycopy(maps, 0, gMapInfo, 0, maps.length);
        nMaps = maps.length;
    }

    public String toString() {
        StringBuilder txt = new StringBuilder("\r\nTitle: " + Title + "\r\n");
        txt.append("Description: ").append(Description).append("\r\n");
        txt.append("nMaps: ").append(nMaps).append("\r\n");
        txt.append("[ \r\n");
        for (int i = 0; i <= nMaps; i++) {
            txt.append("Map ").append(i).append(":\r\n");
            if (gMapInfo[i] != null) {
                txt.append(gMapInfo[i].toString());
            }
            txt.append("\r\n");
        }
        txt.append("] \r\n");
        return txt.toString();
    }
}
