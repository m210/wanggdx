package ru.m210projects.Wang.Type;

import ru.m210projects.Build.Architecture.common.audio.SoundData;

public class VOC_INFO {
    public static final int MAXSOURCEINTICK = 3;

    public final String name; // name of voc file on disk
    public final int pitch_lo; // lo pitch value
    public final int pitch_hi; // hi pitch value
    public final int priority; // priority at which vocs are played
    public final int voc_distance; // Sound's distance effectiveness
    public final int voc_flags; // Various allowable flag settings for voc
    public SoundData data; // pointer to voc data

    public int lock;

    public VOC_INFO(String name, int pri, int pitch_lo, int pitch_hi, int voc_dist,
                    int voc_flags) {
        this.name = name;
        this.data = null;
        this.pitch_lo = pitch_lo;
        this.pitch_hi = pitch_hi;
        this.priority = pri;
        this.voc_distance = voc_dist;
        this.voc_flags = voc_flags;
        this.lock = MAXSOURCEINTICK;
    }
}
