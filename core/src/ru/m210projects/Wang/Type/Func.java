package ru.m210projects.Wang.Type;

public interface Func<T> {
    void invoke(T obj);
}