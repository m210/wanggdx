package ru.m210projects.Wang.Type;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SINE_WAVE_FLOOR {
    public int floor_origz, ceiling_origz, range;
    public int sector, sintable_ndx, speed_shift;
    public int flags;

    public void reset() {
        floor_origz = -1;
        range = -1;
        ceiling_origz = -1;

        sintable_ndx = -1;
        speed_shift = -1;
        sector = -1;

        flags = -1;
    }

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, floor_origz);
        StreamUtils.writeInt(os, ceiling_origz);
        StreamUtils.writeInt(os, range);

        StreamUtils.writeShort(os, sector);
        StreamUtils.writeShort(os, sintable_ndx);
        StreamUtils.writeShort(os, speed_shift);

        StreamUtils.writeInt(os, flags);
    }

    public void load(InputStream is) throws IOException {
        floor_origz = StreamUtils.readInt(is);
        ceiling_origz = StreamUtils.readInt(is);
        range = StreamUtils.readInt(is);

        sector = StreamUtils.readShort(is);
        sintable_ndx = StreamUtils.readShort(is);
        speed_shift = StreamUtils.readShort(is);

        flags = StreamUtils.readInt(is);
    }

    public static SINE_WAVE_FLOOR copy(SINE_WAVE_FLOOR src) {
        SINE_WAVE_FLOOR cp = new SINE_WAVE_FLOOR();
        cp.floor_origz = src.floor_origz;
        cp.ceiling_origz = src.ceiling_origz;
        cp.range = src.range;
        cp.sector = src.sector;
        cp.sintable_ndx = src.sintable_ndx;
        cp.speed_shift = src.speed_shift;
        cp.flags = src.flags;
        return cp;
    }
}
