//-------------------------------------------------------------------------
/*
Copyright (C) 1997, 2005 - 3D Realms Entertainment

This file is part of Shadow Warrior version 1.2

Shadow Warrior is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

Original Source: 1997 - Frank Maddin and Jim Norwood
Prepared for public release: 03/28/2005 - Charlie Wiederhold, 3D Realms
*/
//-------------------------------------------------------------------------

package ru.m210projects.Wang.Type;

public class MyTypes {

    // CTW ADDITION END
    public static final int FALSE = 0;
    public static final int TRUE = 1;

    public static final int OFF = 0;
    public static final int ON = 1;

    public static final int MAXLONG = (2147483647);

    /*
     * =========================== = = FAST calculations =
     * ===========================
     */

    // For fast DIVision of integers
    public static final int C_MOD2 = 1;
    public static final int C_MOD4 = 3;

    // Constants used in fast mods

    public static int DIV2(int value) {
        return value >> 1;
    }

    public static int DIV4(int value) {
        return value >> 2;
    }

    public static int DIV8(int value) {
        return value >> 3;
    }

    public static int DIV16(int value) {
        return value >> 4;
    }

    public static int DIV32(int value) {
        return value >> 5;
    }

    public static int DIV256(int value) {
        return value >> 8;
    }

    // Fast mods of select 2 power numbers

    public static int MOD2(int x) {
        return ((x) & C_MOD2);
    }

    public static int MOD4(int x) {
        return ((x) & C_MOD4);
    }

    // Fast mods of any power of 2

    public static int MOD_P2(int number, int modby) {
        return ((number) & ((modby) - 1));
    }

    /*
     * =========================== = = Bit manipulation =
     * ===========================
     */

    public static boolean TEST(int flags, int mask) {
        return (flags & mask) != 0;
    }

    public static int DTEST(int flags, int mask) {
        return (flags & mask);
    }

    // mask definitions

    public static int BIT(int shift) {
        return (1 << (shift));
    }

    /*
     * =========================== = = Miscellaneous = ===========================
     */

    public static boolean BETWEEN(int x, int low, int high) {
        return ((x >= low) && (x <= high));
    }

}
