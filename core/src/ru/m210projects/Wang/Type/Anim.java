package ru.m210projects.Wang.Type;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Wang.Sprites.SectorObject;
import static ru.m210projects.Wang.Track.CallbackSOsink;

public class Anim {

    public Object ptr;
    public int index; // to set interpolation

    public AnimType type;
    public int goal;
    public int vel;
    public int vel_adj;
    public AnimCallback callback;
    public int callbackdata;

    public Anim copy(Anim src) {
        this.ptr = src.ptr;
        this.index = src.index;
        this.type = src.type;
        this.goal = src.goal;
        this.vel = src.vel;
        this.vel_adj = src.vel_adj;
        this.callback = src.callback;
        this.callbackdata = src.callbackdata;

        return this;
    }

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, index);
        StreamUtils.writeByte(os, type != null ? type.ordinal() : -1);
        StreamUtils.writeInt(os, goal);
        StreamUtils.writeInt(os, vel);
        StreamUtils.writeShort(os, vel_adj);
        StreamUtils.writeInt(os, callback != null ? callback.ordinal() : -1);
        StreamUtils.writeInt(os, callbackdata);
    }

    public void load(InputStream is) throws IOException {
        index = StreamUtils.readShort(is);
        int i = StreamUtils.readByte(is);
        type = (i != -1) ? AnimType.values()[i] : null;
        goal = StreamUtils.readInt(is);
        vel = StreamUtils.readInt(is);
        vel_adj =  StreamUtils.readShort(is);
        i = StreamUtils.readInt(is);
        callback = (i != -1) ? AnimCallback.values()[i] : null;
        callbackdata = StreamUtils.readInt(is);
    }

    public enum AnimCallback {
        SOsink {
            @Override
            public void invoke(Anim ap, int data) {
                CallbackSOsink(ap, SectorObject[data]);
            }
        };

        public abstract void invoke(Anim ap, int data);
    }

    public enum AnimType {
        FloorZ, CeilZ, SpriteZ, SectorObjectZ, UserZ, SectUserDepth
    }

}
