package ru.m210projects.Wang.Type;

import org.jetbrains.annotations.NotNull;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;

import java.util.HashMap;
import java.util.Locale;

import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.MAX_LEVELS;
import static ru.m210projects.Wang.Main.game;

public class EpisodeManager {

    private final HashMap<String, GameInfo> episodeCache = new HashMap<>();

    public void putEpisode(GameInfo gameInfo) {
        episodeCache.put(getHashKey(gameInfo.getEpisodeEntry().getFileEntry()), gameInfo);
    }

    private String getHashKey(FileEntry entry) {
        return entry.getRelativePath().toString().toUpperCase(Locale.ROOT);
    }

    public GameInfo getEpisode(FileEntry entry) {
        GameInfo addon = episodeCache.get(getHashKey(entry));
        if (addon != null) {
            return addon;
        }

        addon = checkOfficialAddon(entry);
        if (addon == null) {
            addon = checkCustomAddon(entry);
        }

        if (addon != null) {
            episodeCache.put(getHashKey(addon.getEpisodeEntry().getFileEntry()), addon);
            if (!defGame.getEpisodeEntry().equals(entry)) {
                Console.out.println("Found addon: " + addon.Title + " ( " + entry.getName() + " )");
            }
        }

        return addon;
    }

    private GameInfo checkCustomAddon(@NotNull FileEntry entry) {
        CustomScript script = null;
        Group group = entry.isDirectory() ? entry.getDirectory() : game.cache.newGroup(entry);

        for (Entry file : group.getEntries()) {
            if (file.getName().toLowerCase(Locale.ROOT).endsWith("custom.txt")) {
                script = new CustomScript(file);
            }
        }

        EpisodeInfo[] UnnamedEpisode = null;
        boolean hasFirstEpisode = group.getEntry("$bullet.map").exists();
        boolean hasSecondEpisode = group.getEntry("$whirl.map").exists();
        if (hasFirstEpisode || hasSecondEpisode) {
            UnnamedEpisode = new EpisodeInfo[hasSecondEpisode ? 2 : 1];
            for (int i = hasFirstEpisode ? 0 : 4; i < MAX_LEVELS; i++) {
                if (i > 3 && !hasSecondEpisode) {
                    break;
                }

                int epnum = i < 4 ? 0 : 1;
                if (UnnamedEpisode[epnum] == null) {
                    UnnamedEpisode[epnum] = new EpisodeInfo();
                    UnnamedEpisode[epnum].Title = "Episode " + epnum;
                    UnnamedEpisode[epnum].Description = "No description";
                }

                EpisodeInfo ep = UnnamedEpisode[epnum];
                LevelInfo map = epnum == 0 ? pSharewareEp.gMapInfo[i] : pOriginalEp.gMapInfo[i - 4];
                if (map != null && group.getEntry(map.LevelName).exists()) {
                    ep.gMapInfo[epnum == 0 ? i : i - 4] = new LevelInfo(map.LevelName, map.SongName, "Map" + (i + 1), map.BestTime, map.ParTime);
                    ep.nMaps++;
                }
            }
        }

        if (script != null || UnnamedEpisode != null) {
            GameInfo addon = new GameInfo(group.getName(), new EpisodeEntry.Addon(group, entry), UnnamedEpisode);
            if (script != null) {
                script.apply(addon);
                // If the directory / group has swcustom.txt, check that it has maps from this script
                if (UnnamedEpisode == null) {
                    for (int i = 0; i < MAX_LEVELS; i++) {
                        Entry mapEntry = group.getEntry(addon.getMapPath(i));
                        if (mapEntry.exists()) {
                            return addon;
                        }
                    }
                    return null;
                }
            }
            return addon;
        }

        return null;
    }

    private GameInfo checkOfficialAddon(@NotNull FileEntry entry) {
        final HashMap<Long, EpisodeInfo> officialAddons = new HashMap<Long, EpisodeInfo>() {
            {
                put(1488316004L, pWantonEp);
                put(1830650101L, pTwinDragonEp);
            }
        };

        Group group = entry.isDirectory() ? entry.getDirectory() : game.cache.newGroup(entry);
        Entry logoFile = group.getEntry("zfcin.anm");
        if (logoFile.exists()) {
            long crc32 = logoFile.getChecksum();
            EpisodeInfo episodeInfo = officialAddons.get(crc32);
            if (episodeInfo != null) {
                return new GameInfo(episodeInfo.Title, new EpisodeEntry.Addon(group, entry), null, episodeInfo);
            }
        }
        return null;
    }

}
