package ru.m210projects.Wang.Type;

import ru.m210projects.Wang.Player.Player_Action_Func;

public class DAMAGE_DATA {

    public final Player_Action_Func init;
    public final int damage_lo;
    public final int damage_hi;
    public final int radius;
    public final int max_ammo;
    public final int min_ammo;
    public final int with_weapon;

    public DAMAGE_DATA(int id, Player_Action_Func init, int damage_lo, int damage_hi, int radius, int max_ammo, int min_ammo, int with_weapon) {
        this.init = init;
        this.damage_lo =  damage_lo;
        this.damage_hi =  damage_hi;
        this.radius = radius;
        this.max_ammo =  max_ammo;
        this.min_ammo =  min_ammo;
        this.with_weapon =  with_weapon;
    }
}
