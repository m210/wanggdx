package ru.m210projects.Wang.Type;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static ru.m210projects.Wang.JSector.MAXMIRRORMONSTERS;

public class MirrorType {

    public int mirrorwall;                   // Wall number containing the mirror

    // tile
    public int mirrorsector;                 // nextsector used internally to draw
    // mirror rooms
    public int camera;                       // Contains number of ST1 sprite used
    // as a camera
    public int camsprite;                    // sprite pointing to campic
    public int campic;                       // Editart tile number to draw a
    // screen to
    public int numspawnspots;                // Number of spawnspots used
    public final short[] spawnspots = new short[MAXMIRRORMONSTERS];// One spot for each possible skill
    // level for a
    // max of up to 4 coolie ghosts to spawn.
    public boolean ismagic;                       // Is this a magic mirror?
    public MirrorState mstate;                 // What state the mirror is currently
    // in
    public int maxtics;                       // Tic count used to time mirror
    // events
    public int tics;                          // How much viewing time has been

    public void reset() {
        mirrorwall = -1;
        mirrorsector = -1;

        camera = -1;
        camsprite = -1;
        campic = -1;
        numspawnspots = -1;
        Arrays.fill(spawnspots, (short) -1);
        ismagic = false;
        mstate = null;
        maxtics = -1;
        tics = -1;
    }
    // used on mirror?

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, mirrorwall);
        StreamUtils.writeShort(os, mirrorsector);

        StreamUtils.writeShort(os, camera);
        StreamUtils.writeShort(os, camsprite);
        StreamUtils.writeShort(os, campic);
        StreamUtils.writeShort(os, numspawnspots);
        for (int v : spawnspots) {
            StreamUtils.writeShort(os, v);
        }
        StreamUtils.writeBoolean(os, ismagic);
        StreamUtils.writeByte(os, mstate != null ? mstate.ordinal() : -1);

        StreamUtils.writeInt(os, maxtics);
        StreamUtils.writeInt(os, tics);
    }

    public void load(InputStream is) throws IOException {
        mirrorwall =  StreamUtils.readShort(is);
        mirrorsector =  StreamUtils.readShort(is);

        camera =  StreamUtils.readShort(is);
        camsprite =  StreamUtils.readShort(is);
        campic =  StreamUtils.readShort(is);
        numspawnspots =  StreamUtils.readShort(is);
        for (int i = 0; i < MAXMIRRORMONSTERS; i++) {
            spawnspots[i] =  StreamUtils.readShort(is);
        }
        ismagic = StreamUtils.readBoolean(is);
        int i = StreamUtils.readByte(is);
        mstate = i != -1 ? MirrorState.values()[i] : null;

        maxtics = StreamUtils.readInt(is);
        tics = StreamUtils.readInt(is);
    }

    public void copy(MirrorType src) {
        this.mirrorwall = src.mirrorwall;
        this.mirrorsector = src.mirrorsector;
        this.camera = src.camera;
        this.camsprite = src.camsprite;
        this.campic = src.campic;
        this.numspawnspots = src.numspawnspots;
        System.arraycopy(src.spawnspots, 0, spawnspots, 0, MAXMIRRORMONSTERS);
        this.ismagic = src.ismagic;
        this.mstate = src.mstate;
        this.maxtics = src.maxtics;
        this.tics = src.tics;
    }


    public enum MirrorState {
        m_normal, m_viewon, m_pissed
    }
}
