package ru.m210projects.Wang.Type;

import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.filehandle.grp.GrpFile;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.HIGHEST;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_DIRECTORY;
import static ru.m210projects.Wang.Digi.voc;
import static ru.m210projects.Wang.Game.currentGame;
import static ru.m210projects.Wang.Game.defGame;
import static ru.m210projects.Wang.JSector.*;
import static ru.m210projects.Wang.Main.*;

public class ResourceHandler {

    public static final EpisodeManager episodeManager = new EpisodeManager();

    public static boolean usecustomarts;
    private static GrpFile usergroup;

    public static void resetEpisodeResources() {
        Console.out.println("Resetting custom resources", OsdColor.GREEN);
        if (usergroup != null) {
            game.getCache().removeGroup(usergroup);
        }
        usergroup = null;
        currentGame = defGame;

        for (VOC_INFO vocInfo : voc) {
            vocInfo.data = null;
        }

        if (!usecustomarts) {
            game.setDefs(game.baseDef);
            return;
        }

        System.err.println("Reset to default resources");
        if (engine.loadpics() == 0) {
            throw new AssertException("ART files not found " + game.getCache().getGameDirectory().getPath().resolve(engine.getTileManager().getTilesPath()));
        }

        if(!game.setDefs(game.baseDef)) {
            game.baseDef.apply();
        }

        InitSpecialTextures();

        usecustomarts = false;
    }

    public static void InitSpecialTextures() {
        engine.allocatepermanenttile(MIRROR, 0, 0);
        for (int i = 0; i < MAXMIRRORS; i++) {
            engine.allocatepermanenttile(i + MIRRORLABEL, 0, 0);
        }
    }

    public static void InitGroupResources(DefScript addonScript, java.util.List<Entry> list) {
        for (Entry res : list) {
            switch (res.getExtension()) {
                case "ART":
                    engine.loadpic(res);
                    usecustomarts = true;
                    break;
                case "DEF":
                    if (!res.getName().equalsIgnoreCase(appdef)) {
                        addonScript.loadScript(res.getName() + " script", res);
                        Console.out.println("Found def-script. Loading " + res.getName());
                    }
                    break;
                case "TXT":
                    if (res.getName().equalsIgnoreCase("swvoxfil.txt")) {
                        VoxelScript vox = new VoxelScript(res);
                        vox.apply(addonScript);
                        Console.out.println("Found swvoxfil.txt. Loading... ");
                    }
                    break;
            }
        }
    }

    private static void searchEpisodeResources(Group container, GrpFile resourceHolder) {
        for (Entry file : container.getEntries()) {
            Group subContainer = DUMMY_DIRECTORY;
            if (file.isDirectory() && file instanceof FileEntry) {
                subContainer = ((FileEntry) file).getDirectory();
            } else if (file.isExtension("zip")
                    || file.isExtension("pk3")
                    || file.isExtension("grp")
                    || file.isExtension("rff")) {
                subContainer = game.getCache().newGroup(file);
            }

            if (!subContainer.equals(DUMMY_DIRECTORY)) {
                searchEpisodeResources(subContainer, resourceHolder);
            } else {
                resourceHolder.addEntry(file);
            }
        }
    }

    public static void checkEpisodeResources(GameInfo addon) {
        if (addon == null) {
            return;
        }

        if (addon.equals(currentGame)) {
            return;
        }

        resetEpisodeResources();

        usergroup = new GrpFile("RemovableGroup");
        EpisodeEntry addonEntry = addon.getEpisodeEntry(); // sw.grp
        DefScript addonScript;
        Group parent = addonEntry.getGroup();

        if (addonEntry.isPackageEpisode()) { // is group entry
            addonScript = new DefScript(game.baseDef, addonEntry.getFileEntry());
            try {
                Entry res = parent.getEntry(appdef); // load def scripts
                if (res.exists()) {
                    addonScript.loadScript(parent.getName() + " script", res);
                }
                searchEpisodeResources(parent, usergroup);
            } catch (Exception e) {
                throw new WarningException("Error found in " + ((EpisodeEntry.Addon) addonEntry).getName() + "\r\n" + e.getMessage());
            }
        } else {
            addonScript = new DefScript(game.baseDef, addonEntry.getFileEntry());
            if (!game.getCache().isGameDirectory(parent)) {
                searchEpisodeResources(parent, usergroup);
                Entry def = parent.getEntry(appdef);
                if (def.exists()) {
                    addonScript.loadScript(def);
                }
            }
        }

        // Loading user package files
        game.getCache().addGroup(usergroup, HIGHEST);
        InitGroupResources(addonScript, usergroup.getEntries());

        currentGame = addon;
        game.setDefs(addonScript);
    }

    public static GameInfo levelGetEpisode(Entry entry) {
        if (entry == null || !entry.exists()) {
            return null;
        }

        if (entry instanceof FileEntry) {
            return episodeManager.getEpisode((FileEntry) entry);
        }
        return null;
    }
}
