package ru.m210projects.Wang.Type;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Spring_Board {
    public short Sector, TimeOut;

    public void reset() {
        Sector = -1;
        TimeOut = -1;
    }

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, Sector);
        StreamUtils.writeShort(os, TimeOut);
    }

    public void load(InputStream is) throws IOException {
        Sector = StreamUtils.readShort(is);
        TimeOut = StreamUtils.readShort(is);
    }

    public static Spring_Board copy(Spring_Board src) {
        Spring_Board cp = new Spring_Board();
        cp.Sector = src.Sector;
        cp.TimeOut = src.TimeOut;
        return cp;
    }
}
