package ru.m210projects.Wang.Type;

import java.util.Objects;

import static ru.m210projects.Wang.Type.MyTypes.TEST;

public class Panel_State extends Saveable {

    public final int id;
    public final int picndx; // for pip stuff in conpic.h
    public final int tics;
    public final Panel_Sprite_Func Animator;
    private Panel_State NextState, PlusOne;
    private int flags;

    public Panel_State(int id, int picndx, int tics, Panel_Sprite_Func Animator) {
        this.picndx = picndx;
        this.tics = tics;
        this.Animator = Animator;
        this.id = id;
    }

    public Panel_State(int picndx, int tics, Panel_Sprite_Func Animator) {
        this.id = 0;
        this.picndx = picndx;
        this.tics = tics;
        this.Animator = Animator;
    }

    public boolean testFlag(int val) {
        return TEST(this.flags, val);
    }

    public Panel_State setFlags(int flags) {
        this.flags = flags;
        return this;
    }

    public Panel_State setNext() {
        this.NextState = this;
        return this;
    }

    public Panel_State getNext() {
        return NextState;
    }

    public Panel_State setNext(Panel_State next) {
        this.NextState = next;
        if (this.PlusOne == null) {
            this.PlusOne = next;
        }
        return this;
    }

    public Panel_State getPlusOne() {
        return PlusOne;
    }

    public Panel_State setPlusOne(Panel_State PlusOne) {
        this.PlusOne = PlusOne;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Panel_State that = (Panel_State) o;
        return picndx == that.picndx;
    }

    @Override
    public int hashCode() {
        return Objects.hash(picndx);
    }
}
