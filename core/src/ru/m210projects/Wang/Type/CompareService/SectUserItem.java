package ru.m210projects.Wang.Type.CompareService;

import ru.m210projects.Wang.Type.Sect_User;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SectUserItem extends StructItem<Sect_User> {

    public SectUserItem(Sect_User objectStruct, int index) {
        super(objectStruct, index);
    }

    @Override
    public boolean compare(InputStream is) throws IOException {
        Sect_User readObject = readObject(is);
        if (!isEquals(readObject, objectStruct)) {
            System.out.println("Unsync in " + getName());
            return false;
        }
        return true;
    }

    @Override
    protected Sect_User readObject(InputStream is) throws IOException {
        Sect_User u = new Sect_User();
        u.readObject(is);
        return u;
    }

    @Override
    public void write(OutputStream os) throws IOException {
        objectStruct.writeObject(os);
    }

}
