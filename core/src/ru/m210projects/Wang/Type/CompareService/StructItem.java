package ru.m210projects.Wang.Type.CompareService;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Objects;

@SuppressWarnings("unused")
public abstract class StructItem<T> implements CompareItem {

    protected final T objectStruct;
    protected final int index;

    public StructItem(T objectStruct, int index) {
        this.objectStruct = objectStruct;
        this.index = index;
    }

    @Override
    public boolean compare(InputStream is) throws IOException {
        T readObject = readObject(is);
        if (!isEquals(readObject, objectStruct)) {
            System.out.println("Unsync in " + getName());
            return false;
        }
        return true;
    }

    protected abstract T readObject(InputStream is) throws IOException;

    protected boolean isEquals(Object that, Object our) {
        if (that == our) {
            return true;
        }

        if (our == null || that.getClass() != our.getClass()) {
            return false;
        }

        Field[] fields = our.getClass().getDeclaredFields();
        boolean result = true;
        for (Field field : fields) {
            if (Modifier.isTransient(field.getModifiers()) || Modifier.isStatic(field.getModifiers())) {
                continue;
            }

            if (Modifier.isPrivate(field.getModifiers())) {
                field.setAccessible(true);
            }

            try {
                Object valueThat = field.get(that);
                Object valueOur = field.get(our);

                if (!Objects.deepEquals(valueThat, valueOur)) {
                    System.err.println("Unsync in " + getName() + "." + field.getName() + ": " + valueThat + "(read) != " + valueOur + "(this)");
                    result = false;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public String getName() {
        return objectStruct.getClass().getSimpleName() + "[" + index + "]";
    }
}
