package ru.m210projects.Wang.Type.CompareService;

import ru.m210projects.Wang.Type.Sector_Object;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SectorObjectItem extends StructItem<Sector_Object> {

    public SectorObjectItem(Sector_Object objectStruct, int index) {
        super(objectStruct, index);
    }

    @Override
    public void write(OutputStream os) throws IOException {
        objectStruct.writeObject(os);
    }

    @Override
    protected Sector_Object readObject(InputStream is) throws IOException {
        Sector_Object sop = new Sector_Object();
        sop.readObject(is);
        return sop;
    }
}
