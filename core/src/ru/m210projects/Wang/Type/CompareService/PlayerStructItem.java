package ru.m210projects.Wang.Type.CompareService;

import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.Remote_Control;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Wang.Game.Player;

@SuppressWarnings("unused")
public class PlayerStructItem extends StructItem<PlayerStr> {

    public PlayerStructItem(PlayerStr objectStruct, int index) {
        super(objectStruct, index);

        if (objectStruct.remote == null) {
            objectStruct.remote = new Remote_Control();
        }
        objectStruct.PlayerTalking = false;
        objectStruct.TalkVocnum = -1;
    }

    @Override
    protected PlayerStr readObject(InputStream is) throws IOException {
        return new PlayerStr().readObject(is);
    }

    @Override
    public void write(OutputStream os) throws IOException {
        Player[index].writeObject(os);
    }
}
