package ru.m210projects.Wang.Type.CompareService;

import ru.m210projects.Build.Types.Sector;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@SuppressWarnings("unused")
public class SectorItem extends StructItem<Sector> {

    public SectorItem(Sector objectStruct, int index) {
        super(objectStruct, index);

        objectStruct.setFiller((byte) objectStruct.getFiller());
    }

    @Override
    protected Sector readObject(InputStream is) throws IOException {
        return new Sector().readObject(is);
    }

    @Override
    public void write(OutputStream os) throws IOException {
        objectStruct.writeObject(os);
    }
}
