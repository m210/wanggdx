package ru.m210projects.Wang.Type.CompareService;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Wang.Main.boardService;

@SuppressWarnings("unused")
public class SpriteListItem implements CompareItem {

    private final List<SpriteItem> list = new ArrayList<>();

    public SpriteListItem() {
        int size = -1;
        for (int i = 0; i < boardService.getSectorCount(); i++) {
            for (ListNode<Sprite> node = boardService.getSectNode(i); node != null; node = node.getNext()) {
                if (size < node.getIndex()) {
                    size = node.getIndex();
                }
            }
        }

        List<Sprite> spriteList = boardService.getBoard().getSprites();
        for (int i = 0; i < size; i++) {
            Sprite sprite = new Sprite();
            sprite.set(spriteList.get(i));
            list.add(new SpriteItem(sprite, i));
        }
    }

    @Override
    public boolean compare(InputStream is) throws IOException {
        int spriteNum = StreamUtils.readInt(is);
        boolean result = spriteNum == list.size();

        while (list.size() < spriteNum) {
            list.add(new SpriteItem(new Sprite(), -1));
        }

        for (int i = 0; i < spriteNum; i++) {
            if (!list.get(i).compare(is)) {
                result = false;
            }
        }

        return result;
    }

    @Override
    public void write(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, list.size());
        for (SpriteItem item : list) {
            item.write(os);
        }
    }

    @Override
    public String getName() {
        return "SpriteList";
    }
}
