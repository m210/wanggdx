package ru.m210projects.Wang.Type.CompareService;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@SuppressWarnings("unused")
public class ArrayItem implements CompareItem {

    private final Number[] array;
    private final String name;

    public ArrayItem(Number[] array, String name) {
        this.array = array;
        this.name = name;
    }

    @Override
    public boolean compare(InputStream is) throws IOException {
        boolean equals = true;
        if (array instanceof Long[]) {
            for (int i = 0; i < array.length; i++) {
                long val = (long) array[i];
                int compValue = StreamUtils.readInt(is);
                if (val != compValue) {
                    System.out.println("Unsync in " + getName() + ": " + i);
                    equals = false;
                }
            }
        } else {
            throw new UnsupportedOperationException();
        }
        return equals;
    }

    @Override
    public void write(OutputStream os) throws IOException {
        if (array instanceof Long[]) {
            for (Number number : array) {
                long val = (long) number;
                StreamUtils.writeInt(os, val);
            }
        } else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
