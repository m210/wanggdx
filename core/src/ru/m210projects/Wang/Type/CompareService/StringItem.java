package ru.m210projects.Wang.Type.CompareService;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StringItem implements CompareItem {
    final String value;
    final String name;

    public StringItem(String value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public boolean compare(InputStream is) throws IOException {
        String compValue = StreamUtils.readDataString(is);
        if (!value.equals(compValue)) {
            System.out.println(name + ": " + value + " != " + compValue);
            return false;
        }
        return true;
    }

    @Override
    public void write(OutputStream os) throws IOException {
        StreamUtils.writeDataString(os, value);
    }

    @Override
    public String getName() {
        return name;
    }
}
