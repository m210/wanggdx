package ru.m210projects.Wang.Type.CompareService;

import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Wang.Type.Sect_User;
import ru.m210projects.Wang.Type.USER;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.Engine.MAXSECTORS;
import static ru.m210projects.Build.Engine.MAXSPRITES;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.boardfilename;
import static ru.m210projects.Wang.Gameutils.getUser;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Sector.getSectUser;

public class CompareService {

    private static OutputStream writer;
    private static InputStream reader;
    private static Type type;

    public static void prepare(Path path, Type t) {
        type = t;
        try {
            if (type == Type.Write) {
                if (writer != null) {
                    writer.close();
                }
                writer = new BufferedOutputStream(Files.newOutputStream(path));
            } else {
                if (reader != null) {
                    reader.close();
                }
                reader = new BufferedInputStream(Files.newInputStream(path));
            }
        } catch (FileNotFoundException | NoSuchFileException fnf) {
            reader = null;
            writer = null;

            Console.out.println(path + " is not found!", OsdColor.RED);
        } catch (IOException e) {
            reader = null;
            writer = null;
            e.printStackTrace();
        }
    }

    @Deprecated
    public static boolean conditionCheck() {
        return gDemoScreen.demfile.rcnt >= 11377 && gDemoScreen.demfile.rcnt <= 11500;
    }

    @Deprecated
    public static boolean conditionUpdate() {
        if (conditionCheck()) {
            // System.out.println("Condition update " + gDemoScreen.demfile.rcnt);
            CompareService.update(gDemoScreen.demfile.rcnt);
            return true;
        }
        return false;
    }

    public static void update(int cnt) {
        if (type == null) {
            return;
        }

        switch (type) {
            case Read:
                if (reader != null) {
                    try {
                        Packet packet = new Packet(cnt);
                        if (!packet.read(reader)) {
//                            throw new Error("Unsync at " + packet.rcnt + " gFrameClock = " + engine.getTotalClock());
                            System.err.println("Unsync at " + packet.rcnt + " in map " + boardfilename);
                        } else {
                            System.out.println("\033[32mDemo file " + boardfilename + " at " + packet.rcnt + " is OK\033[0m");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        reader = null;
                    }
                }
                break;
            case Write:
                if (writer != null) {
                    try {
                        Packet packet = new Packet(cnt);
                        packet.write(writer);
                    } catch (IOException e) {
                        e.printStackTrace();
                        writer = null;
                    }
                }
                break;
        }
    }

    public static void close() {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public enum Type {
        Read, Write
    }

    public static class Packet {
        private final int rcnt;
        private final CompareItem[] items;

        public Packet(int rcnt) {
            this.rcnt = rcnt;

            List<CompareItem> list = new ArrayList<>();
            list.add(new IntegerItem(engine.getrand(), "bseed"));

//            if (conditionCheck()) {
//                list.add(new PlayerStructItem(Player[0], 0));
//                list.add(new SpriteListItem());
////                list.add(new StringItem(WangEngine.randomStack, "randomStack"));
////                WangEngine.randomStack = "";
//
//                for (int i = 0; i < MAXSPRITES; i++) {
//                    USER u = getUser(i);
//                    if (u != null) {
//                        USER newu = new USER();
//                        newu.copy(u);
//                        list.add(new UserItem(newu, i));
//                    }
//                }
//
//                for (int i = 0; i < MAXSECTORS; i++) {
//                    Sect_User su = getSectUser(i);
//                    if (su != null) {
//                        Sect_User newsu = new Sect_User();
//                        newsu.set(su);
//                        list.add(new SectUserItem(newsu, i));
//                    }
//                }
//
//                for (int i = 0; i < boardService.getWallCount(); i++) {
//                    Wall wall = new Wall();
//                    wall.set(boardService.getWall(i));
//                    list.add(new WallItem(wall, i));
//                }
//
//                for (int i = 0; i < boardService.getSectorCount(); i++) {
//                    Sector sector = new Sector();
//                    sector.set(boardService.getSector(i));
//                    list.add(new SectorItem(sector, i));
//                }
//                for (int i = 0; i < MAX_SECTOR_OBJECTS; i++) {
//                    Sector_Object sop = SectorObject[i];
//                    if (sop != null) {
//                        list.add(new SectorObjectItem(sop, i));
//                    }
//                }
//                list.add(new SpriteLinkedListItem());
//            }
            this.items = new CompareItem[list.size()];
            list.toArray(items);
        }

        public boolean read(InputStream is) throws IOException {
            boolean[] equals = new boolean[items.length + 1];
            equals[0] = rcnt == StreamUtils.readInt(is);
            for (int i = 0; i < items.length; i++) {
                equals[i + 1] = items[i].compare(is);
            }

            for (boolean equal : equals) {
                if (!equal) {
                    return false;
                }
            }
            return true;
        }

        public void write(OutputStream os) throws IOException {
            StreamUtils.writeInt(os, rcnt);
            for (CompareItem item : items) {
                item.write(os);
            }
        }
    }
}
