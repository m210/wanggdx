package ru.m210projects.Wang.Type.CompareService;

import ru.m210projects.Wang.Type.StateGroup;
import ru.m210projects.Wang.Type.USER;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Engine.*;

@SuppressWarnings("unused")
public class UserItem extends StructItem<USER> {

    public UserItem(USER objectStruct, int index) {
        super(objectStruct, index);
        objectStruct.moveSpriteReturn = CONVERT_HIT(objectStruct.moveSpriteReturn);
        StateGroup stateGroup = objectStruct.getRot();
        if (stateGroup != null && stateGroup.index() == -1) {
            objectStruct.setRot(null);
        }
    }

    @Override
    public boolean compare(InputStream is) throws IOException {
        USER readObject = readObject(is);
        if (!isEquals(readObject, objectStruct)) {
            System.out.println("Unsync in " + getName());
            return false;
        }
        return true;
    }

    @Override
    protected USER readObject(InputStream is) throws IOException {
        USER u = new USER();
        u.readObject(is);
        return u;
    }

    @Override
    public void write(OutputStream os) throws IOException {
        objectStruct.writeObject(os);
    }

    /**
     * @param hit variable returned by getzrange or clipmove
     *       Sprites 0xC000_0000
     *       Walls 0x8000_0000
     *       Sectors 0x4000_0000
     * @return converted hit variable to old format
     *      Sprites 0xC000 (14 and 15 bit)
     *      Walls 0x8000 (15 bit)
     *      Sectors 0x4000 (14 bit)
     */
    public static int CONVERT_HIT(int hit) {
        if (hit == -1) {
            return -1;
        }

        int type = hit & HIT_TYPE_MASK;
        int index = hit & HIT_INDEX_MASK;
        if (hit != 0) {
            switch (type) {
                case HIT_SPRITE:
                    return index | 0xC000;
                case HIT_WALL:
                    return index | 0x8000;
                case HIT_SECTOR:
                    return index | 0x4000;
            }

            return 0;
        }

        return 0;
    }
}
