package ru.m210projects.Wang.Type.CompareService;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@SuppressWarnings("unused")
public class IntegerItem implements CompareItem {

    final int value;
    final String name;

    public IntegerItem(int value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public boolean compare(InputStream is) throws IOException {
        int compValue = StreamUtils.readInt(is);
        if (value != compValue) {
            System.out.println(name + ": " + compValue + "(read) != " + value + "(this)");
            return false;
        }
        return true;
    }

    @Override
    public void write(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, value);
    }

    @Override
    public String getName() {
        return name;
    }
}
