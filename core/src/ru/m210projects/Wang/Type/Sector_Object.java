package ru.m210projects.Wang.Type;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Morth.*;
import static ru.m210projects.Wang.Track.DoAutoTurretObject;
import static ru.m210projects.Wang.Track.DoTornadoObject;

public class Sector_Object implements Serializable<Sector_Object> {

    public final int[] zorig_floor = new int[MAX_SO_SECTOR]; // original z values for all sectors
    public final int[] zorig_ceiling = new int[MAX_SO_SECTOR]; // original z values for all sectors
    public final int[] sector = new int[MAX_SO_SECTOR]; // hold the sector numbers of the sector object
    public final int[] sp_num = new int[MAX_SO_SPRITE]; // hold the sprite numbers of the object
    public final int[] xorig = new int[MAX_SO_POINTS]; // save the original x & y location of each wall so it can be
    public final int[] yorig = new int[MAX_SO_POINTS]; // refreshed
    public final int[] clipbox_dist = new int[MAX_CLIPBOX]; // mult-clip box variables
    public final int[] clipbox_xoff = new int[MAX_CLIPBOX]; // mult-clip box variables
    public final int[] clipbox_yoff = new int[MAX_CLIPBOX]; // mult-clip box variables
    public final int[] clipbox_ang = new int[MAX_CLIPBOX]; // mult-clip box variables
    public final int[] clipbox_vdist = new int[MAX_CLIPBOX]; // mult-clip box variables
    public final int[]// values for single point scaling
            scale_point_dist = new int[MAX_SO_POINTS]; // distance from center
    public final int[] scale_point_speed = new int[MAX_SO_POINTS]; // speed of scaling
    public SOAnimator PreMoveAnimator;
    public SOAnimator PostMoveAnimator;
    public SOAnimator Animator;
    public int controller = -1;
    public int sp_child; // child sprite that holds info for the sector object
    public int xmid;
    public int ymid;
    public int zmid; // midpoints of the sector object
    public int vel; // velocity
    public int vel_tgt; // target velocity
    public int player_xoff; // player x offset from the xmid
    public int player_yoff; // player y offset from the ymid
    public int zdelta; // z delta from original
    public int z_tgt; // target z delta
    public int z_rate; // rate at which z aproaches target
    public int update; // Distance from player at which you continue updating
    public int// only works for single player.
            bob_diff; // bobbing difference for the frame
    public int target_dist; // distance to next point
    public int floor_loz; // floor low z
    public int floor_hiz; // floor hi z
    public int morph_z; // morphing point z
    public int morph_z_min; // morphing point z min
    public int morph_z_max;
    public int bob_amt; // bob amount max in z coord
    public int// variables set by mappers for drivables
            drive_angspeed;
    public int drive_angslide;
    public int drive_speed;
    public int drive_slide;
    public int crush_z;
    public int flags;
    public int sectnum; // current secnum of midpoint
    public int mid_sector; // middle sector
    public short max_damage; // max damage SIGNED short!!
    public int ram_damage; // damage taken by ramming
    public int wait_tics; //
    public int num_sectors; // number of sectors
    public int num_walls; // number of sectors
    public int track; // the track # 0 to 20
    public int point; // the point on the track that the sector object is headed toward
    public int vel_rate; // rate at which velocity aproaches target
    public int dir; // direction traveling on the track
    public int ang; // angle facing
    public int ang_moving; // angle the SO is facing
    public int clipdist; // cliping distance for operational sector objects
    public int clipbox_num;
    public int ang_tgt; // target angle
    public int ang_orig; // original angle
    public int last_ang; // last angle before started spinning
    public int old_ang; // holding variable for the old angle
    public int spin_speed; // spin_speed
    public int spin_ang; // spin angle
    public int turn_speed; // shift value determines how fast SO turns to match new angle
    public int bob_sine_ndx; // index into sine table
    public int bob_speed; // shift value for speed
    public int op_main_sector; // main sector operational SO moves in - for speed purposes
    public int save_vel; // save velocity
    public int save_spin_speed; // save spin speed
    public int match_event; // match number
    public int match_event_sprite; // spritenum of the match event sprite
    public int// SO Scaling Vector Info
            scale_type; // type of scaling - enum controled
    public int scale_active_type; // activated by a switch or trigger
    public int// values for whole SO
            scale_dist; // distance from center
    public int scale_speed; // speed of scaling
    public int scale_dist_min; // absolute min
    public int scale_dist_max; // absolute max
    public int scale_rand_freq; // freqency of direction change - based on rand(1024)
    public int scale_point_base_speed; // base speed of scaling
    public int scale_point_dist_min; // absolute min
    public int scale_point_dist_max; // absolute max
    public int scale_point_rand_freq; // freqency of direction change - based on rand(1024)

    public int scale_x_mult; // x multiplyer for scaling
    public int scale_y_mult; // y multiplyer for scaling

    public int// Used for center point movement
            morph_wall_point; // actual wall point to drag
    public int morph_ang; // angle moving from CENTER
    public int morph_speed; // speed of movement
    public int morph_dist_max; // radius boundry
    public int morph_rand_freq; // freq of dir change
    public int morph_dist; // dist from CENTER
    public int morph_z_speed; // z speed for morph point
    public int morph_xoff; // save xoff from center
    public int morph_yoff; // save yoff from center

    public int// scale_rand_reverse, // random at random interval
            // limit rotation angle
            limit_ang_center; // for limiting the angle of turning - turrets etc
    public int limit_ang_delta; //

    public Sector_Object() {
        Arrays.fill(sector, -1);
    }

    public static Sector_Object copy(Sector_Object src) {
        Sector_Object sectorObject = new Sector_Object();

        sectorObject.PreMoveAnimator = src.PreMoveAnimator;
        sectorObject.PostMoveAnimator = src.PostMoveAnimator;
        sectorObject.Animator = src.Animator;

        sectorObject.controller = src.controller;
        sectorObject.sp_child = src.sp_child;

        sectorObject.xmid = src.xmid;
        sectorObject.ymid = src.ymid;
        sectorObject.zmid = src.zmid;
        sectorObject.vel = src.vel;
        sectorObject.vel_tgt = src.vel_tgt;
        sectorObject.player_xoff = src.player_xoff;
        sectorObject.player_yoff = src.player_yoff;

        System.arraycopy(src.zorig_floor, 0, sectorObject.zorig_floor, 0, MAX_SO_SECTOR);
        System.arraycopy(src.zorig_ceiling, 0, sectorObject.zorig_ceiling, 0, MAX_SO_SECTOR);

        sectorObject.zdelta = src.zdelta;
        sectorObject.z_tgt = src.z_tgt;
        sectorObject.z_rate = src.z_rate;
        sectorObject.update = src.update;

        sectorObject.bob_diff = src.bob_diff;
        sectorObject.target_dist = src.target_dist;
        sectorObject.floor_loz = src.floor_loz;
        sectorObject.floor_hiz = src.floor_hiz;
        sectorObject.morph_z = src.morph_z;
        sectorObject.morph_z_min = src.morph_z_min;
        sectorObject.morph_z_max = src.morph_z_max;
        sectorObject.bob_amt = src.bob_amt;
        sectorObject.drive_angspeed = src.drive_angspeed;
        sectorObject.drive_angslide = src.drive_angslide;
        sectorObject.drive_speed = src.drive_speed;
        sectorObject.drive_slide = src.drive_slide;
        sectorObject.crush_z = src.crush_z;
        sectorObject.flags = src.flags;

        System.arraycopy(src.sector, 0, sectorObject.sector, 0, MAX_SO_SECTOR);
        System.arraycopy(src.sp_num, 0, sectorObject.sp_num, 0, MAX_SO_SPRITE);
        System.arraycopy(src.xorig, 0, sectorObject.xorig, 0, MAX_SO_POINTS);
        System.arraycopy(src.yorig, 0, sectorObject.yorig, 0, MAX_SO_POINTS);

        sectorObject.sectnum = src.sectnum;
        sectorObject.mid_sector = src.mid_sector;
        sectorObject.max_damage = src.max_damage;
        sectorObject.ram_damage = src.ram_damage;
        sectorObject.wait_tics = src.wait_tics;
        sectorObject.num_sectors = src.num_sectors;
        sectorObject.num_walls = src.num_walls;
        sectorObject.track = src.track;
        sectorObject.point = src.point;
        sectorObject.vel_rate = src.vel_rate;
        sectorObject.dir = src.dir;
        sectorObject.ang = src.ang;
        sectorObject.ang_moving = src.ang_moving;
        sectorObject.clipdist = src.clipdist;

        System.arraycopy(src.clipbox_dist, 0, sectorObject.clipbox_dist, 0, MAX_CLIPBOX);
        System.arraycopy(src.clipbox_xoff, 0, sectorObject.clipbox_xoff, 0, MAX_CLIPBOX);
        System.arraycopy(src.clipbox_yoff, 0, sectorObject.clipbox_yoff, 0, MAX_CLIPBOX);
        System.arraycopy(src.clipbox_ang, 0, sectorObject.clipbox_ang, 0, MAX_CLIPBOX);
        System.arraycopy(src.clipbox_vdist, 0, sectorObject.clipbox_vdist, 0, MAX_CLIPBOX);

        sectorObject.clipbox_num = src.clipbox_num;
        sectorObject.ang_tgt = src.ang_tgt;
        sectorObject.ang_orig = src.ang_orig;
        sectorObject.last_ang = src.last_ang;
        sectorObject.old_ang = src.old_ang;
        sectorObject.spin_speed = src.spin_speed;
        sectorObject.spin_ang = src.spin_ang;
        sectorObject.turn_speed = src.turn_speed;
        sectorObject.bob_sine_ndx = src.bob_sine_ndx;
        sectorObject.bob_speed = src.bob_speed;
        sectorObject.op_main_sector = src.op_main_sector;
        sectorObject.save_vel = src.save_vel;
        sectorObject.save_spin_speed = src.save_spin_speed;
        sectorObject.match_event = src.match_event;
        sectorObject.match_event_sprite = src.match_event_sprite;
        sectorObject.scale_type = src.scale_type;
        sectorObject.scale_active_type = src.scale_active_type;

        sectorObject.scale_dist = src.scale_dist;
        sectorObject.scale_speed = src.scale_speed;
        sectorObject.scale_dist_min = src.scale_dist_min;
        sectorObject.scale_dist_max = src.scale_dist_max;
        sectorObject.scale_rand_freq = src.scale_rand_freq;

        System.arraycopy(src.scale_point_dist, 0, sectorObject.scale_point_dist, 0, MAX_SO_POINTS);
        System.arraycopy(src.scale_point_speed, 0, sectorObject.scale_point_speed, 0, MAX_SO_POINTS);

        sectorObject.scale_point_base_speed = src.scale_point_base_speed;
        sectorObject.scale_point_dist_min = src.scale_point_dist_min;
        sectorObject.scale_point_dist_max = src.scale_point_dist_max;
        sectorObject.scale_point_rand_freq = src.scale_point_rand_freq;

        sectorObject.scale_x_mult = src.scale_x_mult;
        sectorObject.scale_y_mult = src.scale_y_mult;

        sectorObject.morph_wall_point = src.morph_wall_point;
        sectorObject.morph_ang = src.morph_ang;
        sectorObject.morph_speed = src.morph_speed;
        sectorObject.morph_dist_max = src.morph_dist_max;
        sectorObject.morph_rand_freq = src.morph_rand_freq;
        sectorObject.morph_dist = src.morph_dist;
        sectorObject.morph_z_speed = src.morph_z_speed;
        sectorObject.morph_xoff = src.morph_xoff;
        sectorObject.morph_yoff = src.morph_yoff;

        sectorObject.limit_ang_center = src.limit_ang_center;
        sectorObject.limit_ang_delta = src.limit_ang_delta;

        return sectorObject;
    }

    public void reset() {
        Arrays.fill(sector, -1);
        PreMoveAnimator = null;
        PostMoveAnimator = null;
        Animator = null;
        controller = -1;
        sp_child = -1;

        xmid = -1;
        ymid = -1;
        zmid = -1;
        vel = -1; // velocity
        vel_tgt = -1;
        player_xoff = -1; // player x offset from the xmid
        player_yoff = -1; // player y offset from the ymid
        Arrays.fill(zorig_floor, -1);
        Arrays.fill(zorig_ceiling, -1);

        zdelta = -1; // z delta from original
        z_tgt = -1; // target z delta
        z_rate = -1; // rate at which z aproaches target
        update = -1; // Distance from player at which you continue updating
        // only works for single player.
        bob_diff = -1; // bobbing difference for the frame
        target_dist = -1; // distance to next point
        floor_loz = -1; // floor low z
        floor_hiz = -1; // floor hi z
        morph_z = -1; // morphing point z
        morph_z_min = -1; // morphing point z min
        morph_z_max = -1;
        bob_amt = -1; // bob amount max in z coord
        // variables set by mappers for drivables
        drive_angspeed = -1;
        drive_angslide = -1;
        drive_speed = -1;
        drive_slide = -1;
        crush_z = -1;
        flags = -1;
        Arrays.fill(sector, -1);

        Arrays.fill(sp_num, -1);
        Arrays.fill(xorig, -1);
        Arrays.fill(yorig, -1);

        sectnum = -1; // current secnum of midpoint
        mid_sector = -1; // middle sector
        max_damage = -1; // max damage
        ram_damage = -1; // damage taken by ramming
        wait_tics = -1; //
        num_sectors = -1; // number of sectors
        num_walls = -1; // number of sectors
        track = -1; // the track # 0 to 20
        point = -1; // the point on the track that the sector object is headed toward
        vel_rate = -1; // rate at which velocity aproaches target
        dir = -1; // direction traveling on the track
        ang = -1; // angle facing
        ang_moving = -1; // angle the SO is facing
        clipdist = -1; // cliping distance for operational sector objects

        Arrays.fill(clipbox_dist, -1);
        Arrays.fill(clipbox_xoff, -1);
        Arrays.fill(clipbox_yoff, -1);

        Arrays.fill(clipbox_ang, -1);
        Arrays.fill(clipbox_vdist, -1);

        clipbox_num = -1;
        ang_tgt = -1; // target angle
        ang_orig = -1; // original angle
        last_ang = -1; // last angle before started spinning
        old_ang = -1; // holding variable for the old angle
        spin_speed = -1; // spin_speed
        spin_ang = -1; // spin angle
        turn_speed = -1; // shift value determines how fast SO turns to match new angle
        bob_sine_ndx = -1; // index into sine table
        bob_speed = -1; // shift value for speed
        op_main_sector = -1; // main sector operational SO moves in - for speed purposes
        save_vel = -1; // save velocity
        save_spin_speed = -1; // save spin speed
        match_event = -1; // match number
        match_event_sprite = -1; // spritenum of the match event sprite
        // SO Scaling Vector Info
        scale_type = -1; // type of scaling - enum controled
        scale_active_type = -1; // activated by a switch or trigger

        // values for whole SO
        scale_dist = -1; // distance from center
        scale_speed = -1; // speed of scaling
        scale_dist_min = -1; // absolute min
        scale_dist_max = -1; // absolute max
        scale_rand_freq = -1; // freqency of direction change - based on rand(1024)

        // values for single point scaling
        Arrays.fill(scale_point_dist, -1);
        Arrays.fill(scale_point_speed, -1);
        scale_point_base_speed = -1; // base speed of scaling
        scale_point_dist_min = -1; // absolute min
        scale_point_dist_max = -1; // absolute max
        scale_point_rand_freq = -1; // freqency of direction change - based on rand(1024)

        scale_x_mult = -1; // x multiplyer for scaling
        scale_y_mult = -1; // y multiplyer for scaling

        // Used for center point movement
        morph_wall_point = -1; // actual wall point to drag
        morph_ang = -1; // angle moving from CENTER
        morph_speed = -1; // speed of movement
        morph_dist_max = -1; // radius boundry
        morph_rand_freq = -1; // freq of dir change
        morph_dist = -1; // dist from CENTER
        morph_z_speed = -1; // z speed for morph point
        morph_xoff = -1; // save xoff from center
        morph_yoff = -1; // save yoff from center

        // scale_rand_reverse = -1; // random at random interval
        // limit rotation angle
        limit_ang_center = -1; // for limiting the angle of turning - turrets etc
        limit_ang_delta = -1; //
    }

    @Override
    public Sector_Object writeObject(OutputStream os) throws IOException {
        if (PreMoveAnimator != null) {
            StreamUtils.writeInt(os, PreMoveAnimator.ordinal());
        } else {
            StreamUtils.writeInt(os, -1);
        }

        if (PostMoveAnimator != null) {
            StreamUtils.writeInt(os, PostMoveAnimator.ordinal());
        } else {
            StreamUtils.writeInt(os, -1);
        }

        if (Animator != null) {
            StreamUtils.writeInt(os, Animator.ordinal());
        } else {
            StreamUtils.writeInt(os, -1);
        }

        StreamUtils.writeInt(os, controller);
        StreamUtils.writeInt(os, sp_child);

        StreamUtils.writeInt(os, xmid);
        StreamUtils.writeInt(os, ymid);
        StreamUtils.writeInt(os, zmid);
        StreamUtils.writeInt(os, vel); // velocity
        StreamUtils.writeInt(os, vel_tgt);
        StreamUtils.writeInt(os, player_xoff); // player x offset from the xmid
        StreamUtils.writeInt(os, player_yoff); // player y offset from the ymid

        for (int i = 0; i < MAX_SO_SECTOR; i++) {
            StreamUtils.writeInt(os, zorig_floor[i]);
            StreamUtils.writeInt(os, zorig_ceiling[i]);
        }

        StreamUtils.writeInt(os, zdelta); // z delta from original
        StreamUtils.writeInt(os, z_tgt); // target z delta
        StreamUtils.writeInt(os, z_rate); // rate at which z aproaches target
        StreamUtils.writeInt(os, update); // Distance from player at which you continue updating
        // only works for single player.
        StreamUtils.writeInt(os, bob_diff); // bobbing difference for the frame
        StreamUtils.writeInt(os, target_dist); // distance to next point
        StreamUtils.writeInt(os, floor_loz); // floor low z
        StreamUtils.writeInt(os, floor_hiz); // floor hi z
        StreamUtils.writeInt(os, morph_z); // morphing point z
        StreamUtils.writeInt(os, morph_z_min); // morphing point z min
        StreamUtils.writeInt(os, morph_z_max);
        StreamUtils.writeInt(os, bob_amt); // bob amount max in z coord
        // variables set by mappers for drivables
        StreamUtils.writeInt(os, drive_angspeed);
        StreamUtils.writeInt(os, drive_angslide);
        StreamUtils.writeInt(os, drive_speed);
        StreamUtils.writeInt(os, drive_slide);
        StreamUtils.writeInt(os, crush_z);
        StreamUtils.writeInt(os, flags);

        for (int i = 0; i < MAX_SO_SECTOR; i++) {
            StreamUtils.writeShort(os, (short) sector[i]);
        }
        for (int i = 0; i < MAX_SO_SPRITE; i++) {
            StreamUtils.writeShort(os, (short) sp_num[i]);
        }
        for (int i = 0; i < MAX_SO_POINTS; i++) {
            StreamUtils.writeShort(os, (short) xorig[i]);
            StreamUtils.writeShort(os, (short) yorig[i]);
        }

        StreamUtils.writeShort(os, (short) sectnum); // current secnum of midpoint
        StreamUtils.writeShort(os, (short) mid_sector); // middle sector
        StreamUtils.writeShort(os, max_damage); // max damage
        StreamUtils.writeShort(os, (short) ram_damage); // damage taken by ramming
        StreamUtils.writeShort(os, (short) wait_tics); //
        StreamUtils.writeShort(os, (short) num_sectors); // number of sectors
        StreamUtils.writeShort(os, (short) num_walls); // number of sectors
        StreamUtils.writeShort(os, (short) track); // the track # 0 to 20
        StreamUtils.writeShort(os, (short) point); // the point on the track that the sector object is headed toward
        StreamUtils.writeShort(os, (short) vel_rate); // rate at which velocity aproaches target
        StreamUtils.writeShort(os, (short) dir); // direction traveling on the track
        StreamUtils.writeShort(os, (short) ang); // angle facing
        StreamUtils.writeShort(os, (short) ang_moving); // angle the SO is facing
        StreamUtils.writeShort(os, (short) clipdist); // cliping distance for operational sector objects

        for (int i = 0; i < MAX_CLIPBOX; i++) {
            StreamUtils.writeShort(os, (short) clipbox_dist[i]);
            StreamUtils.writeShort(os, (short) clipbox_xoff[i]);
            StreamUtils.writeShort(os, (short) clipbox_yoff[i]);
            StreamUtils.writeShort(os, (short) clipbox_ang[i]);
            StreamUtils.writeShort(os, (short) clipbox_vdist[i]);
        }

        StreamUtils.writeShort(os, (short) clipbox_num);
        StreamUtils.writeShort(os, (short) ang_tgt); // target angle
        StreamUtils.writeShort(os, (short) ang_orig); // original angle
        StreamUtils.writeShort(os, (short) last_ang); // last angle before started spinning
        StreamUtils.writeShort(os, (short) old_ang); // holding variable for the old angle
        StreamUtils.writeShort(os, (short) spin_speed); // spin_speed
        StreamUtils.writeShort(os, (short) spin_ang); // spin angle
        StreamUtils.writeShort(os, (short) turn_speed); // shift value determines how fast SO turns to match new angle
        StreamUtils.writeShort(os, (short) bob_sine_ndx); // index into sine table
        StreamUtils.writeShort(os, (short) bob_speed); // shift value for speed
        StreamUtils.writeShort(os, (short) op_main_sector); // main sector operational SO moves in - for speed purposes
        StreamUtils.writeShort(os, (short) save_vel); // save velocity
        StreamUtils.writeShort(os, (short) save_spin_speed); // save spin speed
        StreamUtils.writeShort(os, (short) match_event); // match number
        StreamUtils.writeShort(os, (short) match_event_sprite); // spritenum of the match event sprite
        // SO Scaling Vector Info
        StreamUtils.writeShort(os, (short) scale_type); // type of scaling - enum controled
        StreamUtils.writeShort(os, (short) scale_active_type); // activated by a switch or trigger

        // values for whole SO
        StreamUtils.writeShort(os, (short) scale_dist); // distance from center
        StreamUtils.writeShort(os, (short) scale_speed); // speed of scaling
        StreamUtils.writeShort(os, (short) scale_dist_min); // absolute min
        StreamUtils.writeShort(os, (short) scale_dist_max); // absolute max
        StreamUtils.writeShort(os, (short) scale_rand_freq); // freqency of direction change - based on rand(1024)

        // values for single point scaling
        for (int i = 0; i < MAX_SO_POINTS; i++) {
            StreamUtils.writeShort(os, (short) scale_point_dist[i]);
            StreamUtils.writeShort(os, (short) scale_point_speed[i]);
        }

        StreamUtils.writeShort(os, (short) scale_point_base_speed); // base speed of scaling
        StreamUtils.writeShort(os, (short) scale_point_dist_min); // absolute min
        StreamUtils.writeShort(os, (short) scale_point_dist_max); // absolute max
        StreamUtils.writeShort(os, (short) scale_point_rand_freq); // freqency of direction change - based on rand(1024)
        StreamUtils.writeShort(os, (short) scale_x_mult); // x multiplyer for scaling
        StreamUtils.writeShort(os, (short) scale_y_mult); // y multiplyer for scaling

        // Used for center point movement
        StreamUtils.writeShort(os, (short) morph_wall_point); // actual wall point to drag
        StreamUtils.writeShort(os, (short) morph_ang); // angle moving from CENTER
        StreamUtils.writeShort(os, (short) morph_speed); // speed of movement
        StreamUtils.writeShort(os, (short) morph_dist_max); // radius boundry
        StreamUtils.writeShort(os, (short) morph_rand_freq); // freq of dir change
        StreamUtils.writeShort(os, (short) morph_dist); // dist from CENTER
        StreamUtils.writeShort(os, (short) morph_z_speed); // z speed for morph point
        StreamUtils.writeShort(os, (short) morph_xoff); // save xoff from center
        StreamUtils.writeShort(os, (short) morph_yoff); // save yoff from center

        // StreamUtils.writeShort(os, scale_rand_reverse); // random at random interval
        // limit rotation angle
        StreamUtils.writeShort(os, (short) limit_ang_center); // for limiting the angle of turning - turrets etc
        StreamUtils.writeShort(os, (short) limit_ang_delta); //

        return this;
    }

    public Sector_Object readObject(InputStream is) throws IOException {
        int ndx = StreamUtils.readInt(is);
        if (ndx != -1) {
            PreMoveAnimator = SOAnimator.values()[ndx];
        } else {
            PreMoveAnimator = null;
        }

        ndx = StreamUtils.readInt(is);
        if (ndx != -1) {
            PostMoveAnimator = SOAnimator.values()[ndx];
        } else {
            PostMoveAnimator = null;
        }

        ndx = StreamUtils.readInt(is);
        if (ndx != -1) {
            Animator = SOAnimator.values()[ndx];
        } else {
            Animator = null;
        }

        controller = StreamUtils.readInt(is);
        sp_child = StreamUtils.readInt(is);

        xmid = StreamUtils.readInt(is);
        ymid = StreamUtils.readInt(is);
        zmid = StreamUtils.readInt(is);
        vel = StreamUtils.readInt(is); // velocity
        vel_tgt = StreamUtils.readInt(is);
        player_xoff = StreamUtils.readInt(is); // player x offset from the xmid
        player_yoff = StreamUtils.readInt(is); // player y offset from the ymid

        for (int i = 0; i < MAX_SO_SECTOR; i++) {
            zorig_floor[i] = StreamUtils.readInt(is);
            zorig_ceiling[i] = StreamUtils.readInt(is);
        }

        zdelta = StreamUtils.readInt(is); // z delta from original
        z_tgt = StreamUtils.readInt(is); // target z delta
        z_rate = StreamUtils.readInt(is); // rate at which z aproaches target
        update = StreamUtils.readInt(is); // Distance from player at which you continue updating
        // only works for single player.
        bob_diff = StreamUtils.readInt(is); // bobbing difference for the frame
        target_dist = StreamUtils.readInt(is); // distance to next point
        floor_loz = StreamUtils.readInt(is); // floor low z
        floor_hiz = StreamUtils.readInt(is); // floor hi z
        morph_z = StreamUtils.readInt(is); // morphing point z
        morph_z_min = StreamUtils.readInt(is); // morphing point z min
        morph_z_max = StreamUtils.readInt(is);
        bob_amt = StreamUtils.readInt(is); // bob amount max in z coord
        // variables set by mappers for drivables
        drive_angspeed = StreamUtils.readInt(is);
        drive_angslide = StreamUtils.readInt(is);
        drive_speed = StreamUtils.readInt(is);
        drive_slide = StreamUtils.readInt(is);
        crush_z = StreamUtils.readInt(is);
        flags = StreamUtils.readInt(is);

        for (int i = 0; i < MAX_SO_SECTOR; i++) {
            sector[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < MAX_SO_SPRITE; i++) {
            sp_num[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < MAX_SO_POINTS; i++) {
            xorig[i] = StreamUtils.readShort(is);
            yorig[i] = StreamUtils.readShort(is);
        }

        sectnum = StreamUtils.readShort(is); // current secnum of midpoint
        mid_sector = StreamUtils.readShort(is); // middle sector
        max_damage = StreamUtils.readShort(is); // max damage
        ram_damage = StreamUtils.readShort(is); // damage taken by ramming
        wait_tics = StreamUtils.readShort(is); //
        num_sectors = StreamUtils.readShort(is); // number of sectors
        num_walls = StreamUtils.readShort(is); // number of sectors
        track = StreamUtils.readShort(is); // the track # 0 to 20
        point = StreamUtils.readShort(is); // the point on the track that the sector object is headed toward
        vel_rate = StreamUtils.readShort(is); // rate at which velocity aproaches target
        dir = StreamUtils.readShort(is); // direction traveling on the track
        ang = StreamUtils.readShort(is); // angle facing
        ang_moving = StreamUtils.readShort(is); // angle the SO is facing
        clipdist = StreamUtils.readShort(is); // cliping distance for operational sector objects

        for (int i = 0; i < MAX_CLIPBOX; i++) {
            clipbox_dist[i] = StreamUtils.readShort(is);
            clipbox_xoff[i] = StreamUtils.readShort(is);
            clipbox_yoff[i] = StreamUtils.readShort(is);
            clipbox_ang[i] = StreamUtils.readShort(is);
            clipbox_vdist[i] = StreamUtils.readShort(is);
        }

        clipbox_num = StreamUtils.readShort(is);
        ang_tgt = StreamUtils.readShort(is); // target angle
        ang_orig = StreamUtils.readShort(is); // original angle
        last_ang = StreamUtils.readShort(is); // last angle before started spinning
        old_ang = StreamUtils.readShort(is); // holding variable for the old angle
        spin_speed = StreamUtils.readShort(is); // spin_speed
        spin_ang = StreamUtils.readShort(is); // spin angle
        turn_speed = StreamUtils.readShort(is); // shift value determines how fast SO turns to match new angle
        bob_sine_ndx = StreamUtils.readShort(is); // index into sine table
        bob_speed = StreamUtils.readShort(is); // shift value for speed
        op_main_sector = StreamUtils.readShort(is); // main sector operational SO moves in - for speed purposes
        save_vel = StreamUtils.readShort(is); // save velocity
        save_spin_speed = StreamUtils.readShort(is); // save spin speed
        match_event = StreamUtils.readShort(is); // match number
        match_event_sprite = StreamUtils.readShort(is); // spritenum of the match event sprite
        // SO Scaling Vector Info
        scale_type = StreamUtils.readShort(is); // type of scaling - enum controled
        scale_active_type = StreamUtils.readShort(is); // activated by a switch or trigger

        // values for whole SO
        scale_dist = StreamUtils.readShort(is); // distance from center
        scale_speed = StreamUtils.readShort(is); // speed of scaling
        scale_dist_min = StreamUtils.readShort(is); // absolute min
        scale_dist_max = StreamUtils.readShort(is); // absolute max
        scale_rand_freq = StreamUtils.readShort(is); // freqency of direction change - based on rand(1024)

        // values for single point scaling
        for (int i = 0; i < MAX_SO_POINTS; i++) {
            scale_point_dist[i] = StreamUtils.readShort(is);
            scale_point_speed[i] = StreamUtils.readShort(is);
        }

        scale_point_base_speed = StreamUtils.readShort(is); // base speed of scaling
        scale_point_dist_min = StreamUtils.readShort(is); // absolute min
        scale_point_dist_max = StreamUtils.readShort(is); // absolute max
        scale_point_rand_freq = StreamUtils.readShort(is); // freqency of direction change - based on rand(1024)

        scale_x_mult = StreamUtils.readShort(is); // x multiplyer for scaling
        scale_y_mult = StreamUtils.readShort(is); // y multiplyer for scaling

        // Used for center point movement
        morph_wall_point = StreamUtils.readShort(is); // actual wall point to drag
        morph_ang = StreamUtils.readShort(is); // angle moving from CENTER
        morph_speed = StreamUtils.readShort(is); // speed of movement
        morph_dist_max = StreamUtils.readShort(is); // radius boundry
        morph_rand_freq = StreamUtils.readShort(is); // freq of dir change
        morph_dist = StreamUtils.readShort(is); // dist from CENTER
        morph_z_speed = StreamUtils.readShort(is); // z speed for morph point
        morph_xoff = StreamUtils.readShort(is); // save xoff from center
        morph_yoff = StreamUtils.readShort(is); // save yoff from center

        // scale_rand_reverse = StreamUtils.readShort(is); // random at random interval
        // limit rotation angle
        limit_ang_center = StreamUtils.readShort(is); // for limiting the angle of turning - turrets etc
        limit_ang_delta = StreamUtils.readShort(is); //

        return this;
    }

    public enum SOAnimator {
        DoTornadoObject {
            @Override
            public void invoke(int sop) {
                DoTornadoObject(sop);
            }
        },

        // SCALING - PreAnimator
        ScaleSectorObject {
            @Override
            public void invoke(int sop) {
                ScaleSectorObject(sop);
            }
        },

        // Morph point - move point around
        MorphTornado {
            @Override
            public void invoke(int sop) {
                MorphTornado(sop);
            }
        },

        MorphFloor {
            @Override
            public void invoke(int sop) {
                MorphFloor(sop);
            }
        },

        DoAutoTurretObject {
            @Override
            public void invoke(int sopi) {
                DoAutoTurretObject(sopi);
            }
        };

        public abstract void invoke(int sop);
    }
}
