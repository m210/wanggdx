package ru.m210projects.Wang.Type;

import ru.m210projects.Build.Render.Types.DefaultScreenFade;

import static ru.m210projects.Build.Pragmas.klabs;

public class WangScreenFade extends DefaultScreenFade {

    // Fades from 100% to 62.5% somewhat quickly,
    // then from 62.5% to 37.5% slowly,
    // then from 37.5% to 0% quickly.
    // This seems to capture the pain caused by enemy shots, plus the extreme
    // fade caused by being blinded or intense pain.
    // Perhaps the next step would be to apply a gentle smoothing to the
    // intersections of these lines.
    public static final int[] faderamp = {64, 60, 56, 52, 48, 44, // y=64-4x

            40, 39, 38, 38, 37, // y=44.8-(16/20)x
            36, 35, 34, 34, 33, 32, 31, 30, 30, 29, 28, 27, 26, 26, 25,

            24, 20, 16, 12, 8, 4 // y=128-4x
    };

    private final byte[] palette = new byte[768];

    public WangScreenFade(String name) {
        super(name);
    }

    @Override
    public DefaultScreenFade set(int r, int g, int b, int intensive) {
        int amt = faderamp[Math.min(31, Math.max(0, 32 - klabs(intensive)))];
        float k = amt / 48.0f;
        r = (int) (r * k);
        g = (int) (g * k);
        b = (int) (b * k);
        return super.set(r, g, b, amt | 128);
    }

    public byte[] getPalette() {
        return palette;
    }

    public void setPalette(byte[] palette) {
        System.arraycopy(palette, 0, this.palette, 0, 768);
    }
}
