package ru.m210projects.Wang.Type;

public class Animator extends Saveable {

    public static final Runnable NULL = spr -> {
    };

    private final Callback callback;

    public Animator(Callback callback) {
        this.callback = callback;
    }

    public boolean animatorInvoke(int spr) {
        return callback.invoke(spr);
    }

    public interface Callback {
        boolean invoke(int spr);
    }

    public interface Runnable extends Callback {
        void run(int spr);

        default boolean invoke(int spr) {
            run(spr);
            return false;
        }
    }

}
