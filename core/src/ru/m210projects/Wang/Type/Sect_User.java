package ru.m210projects.Wang.Type;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Sect_User {

    public int dist, flags;
    public int depth_fract, depth; // do NOT change this, doubles as a long FIXED point number
    public int stag, // ST? tag number - for certain things it helps to know it
            ang, height, speed, damage, number; // usually used for matching number
    public byte flags2;

    public Sect_User set(Sect_User src) {
        this.dist = src.dist;
        this.flags = src.flags;
        this.depth_fract = src.depth_fract;
        this.depth = src.depth;
        this.stag = src.stag;
        this.ang = src.ang;
        this.height = src.height;
        this.speed = src.speed;
        this.damage = src.damage;
        this.number = src.number;
        this.flags2 = src.flags2;

        return this;
    }

    public void reset() {
        this.dist = 0;
        this.flags = 0;
        this.depth_fract = 0;
        this.depth = 0;
        this.stag = 0;
        this.ang = 0;
        this.height = 0;
        this.speed = 0;
        this.damage = 0;
        this.number = 0;
        this.flags2 = 0;
    }

    public void writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, dist);
        StreamUtils.writeInt(os, flags);
        StreamUtils.writeShort(os, (short) depth_fract);
        StreamUtils.writeShort(os, (short) depth);
        StreamUtils.writeShort(os, (short) stag);
        StreamUtils.writeShort(os, (short) ang);
        StreamUtils.writeShort(os, (short) height);
        StreamUtils.writeShort(os, (short) speed);
        StreamUtils.writeShort(os, (short) damage);
        StreamUtils.writeShort(os, (short) number);
        StreamUtils.writeByte(os, flags2);
    }

    public void readObject(InputStream is) throws IOException {
        dist = StreamUtils.readInt(is);
        flags = StreamUtils.readInt(is);
        depth_fract = StreamUtils.readShort(is);
        depth = StreamUtils.readShort(is);

        stag = StreamUtils.readShort(is);
        ang = StreamUtils.readShort(is);
        height = StreamUtils.readShort(is);
        speed = StreamUtils.readShort(is);
        damage = StreamUtils.readShort(is);
        number = StreamUtils.readShort(is);

        flags2 = StreamUtils.readByte(is);
    }
}
