package ru.m210projects.Wang.Type;

import com.badlogic.gdx.utils.Pool.Poolable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import static ru.m210projects.Wang.Game.Player;

public class Panel_Sprite implements Poolable {

    public Panel_Sprite Prev, Next;

    public int siblingNdx = -1; // temporary variable to help loading the game
    public Panel_Sprite sibling;
    public Panel_State State, RetractState, PresentState, ActionState, RestState;

    public int PlayerP = -1;
    // Do not change the order of this line
    public int xfract;
    public int x;
    public int yfract;
    public int y; // Do not change the order of this
    // line

    public final PANEL_SPRITE_OVERLAY[] over = new PANEL_SPRITE_OVERLAY[8];
    public Panel_Sprite_Func PanelSpriteFunc;
    public int ID; // id for finding sprite types on the list
    public int picndx; // for pip stuff in conpic.h
    public int picnum; // bypass pip stuff in conpic.h
    public int x1, y1, x2, y2; // for rotatesprites box cliping
    public int vel, vel_adj;
    public int numpages;
    public int xorig, yorig, flags, priority;
    public int scale;
    public int jump_speed, jump_grav; // jumping vars
    public int xspeed;
    public int tics, delay; // time vars
    public int ang, rotate_ang;
    public int sin_ndx, sin_amt, sin_arc_speed;
    public int bob_height_shift;
    public int shade, pal;
    public int kill_tics;
    public int WeaponType; // remember my own weapon type for weapons with secondary function

    public Panel_Sprite() {
        for (int i = 0; i < 8; i++) {
            over[i] = new PANEL_SPRITE_OVERLAY();
        }
    }

    public PlayerStr PlayerP() {
        return PlayerP != -1 ? Player[PlayerP] : null;
    }

    public Panel_Sprite InitList() {
        Next = this;
        return this;
    }

    public void save(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, flags);

        StreamUtils.writeInt(os, State != null ? State.ordinal() : -1);
        StreamUtils.writeInt(os, RetractState != null ? RetractState.ordinal() : -1);
        StreamUtils.writeInt(os, PresentState != null ? PresentState.ordinal() : -1);
        StreamUtils.writeInt(os, ActionState != null ? ActionState.ordinal() : -1);
        StreamUtils.writeInt(os, RestState != null ? RestState.ordinal() : -1);

        StreamUtils.writeInt(os, PlayerP);
        StreamUtils.writeInt(os, xfract);
        StreamUtils.writeInt(os, x);
        StreamUtils.writeInt(os, yfract);
        StreamUtils.writeInt(os, y);

        for (int i = 0; i < 8; i++) {
            over[i].save(os);
        }

        StreamUtils.writeInt(os, PanelSpriteFunc != null ? PanelSpriteFunc.ordinal() : -1);

        StreamUtils.writeShort(os, ID);
        StreamUtils.writeShort(os, picndx);
        StreamUtils.writeShort(os, picnum);
        StreamUtils.writeShort(os, x1);
        StreamUtils.writeShort(os, y1);
        StreamUtils.writeShort(os, x2);
        StreamUtils.writeShort(os, y2);
        StreamUtils.writeShort(os, vel);
        StreamUtils.writeShort(os, vel_adj);
        StreamUtils.writeShort(os, numpages);
        StreamUtils.writeInt(os, xorig);
        StreamUtils.writeInt(os, yorig);
        StreamUtils.writeInt(os, flags);
        StreamUtils.writeInt(os, yfract);
        StreamUtils.writeInt(os, priority);
        StreamUtils.writeInt(os, scale);
        StreamUtils.writeInt(os, jump_speed);
        StreamUtils.writeInt(os, jump_grav);
        StreamUtils.writeInt(os, xspeed);
        StreamUtils.writeShort(os, tics);
        StreamUtils.writeShort(os, delay);
        StreamUtils.writeShort(os, ang);
        StreamUtils.writeShort(os, rotate_ang);
        StreamUtils.writeShort(os, sin_ndx);
        StreamUtils.writeShort(os, sin_amt);
        StreamUtils.writeShort(os, sin_arc_speed);
        StreamUtils.writeShort(os, bob_height_shift);
        StreamUtils.writeShort(os, shade);
        StreamUtils.writeShort(os, pal);
        StreamUtils.writeShort(os, kill_tics);
        StreamUtils.writeShort(os, WeaponType);
    }

    public void load(InputStream is) throws IOException {
        flags = StreamUtils.readInt(is);

        State = (Panel_State) Saveable.valueOf(StreamUtils.readInt(is));
        RetractState = (Panel_State) Saveable.valueOf(StreamUtils.readInt(is));
        PresentState = (Panel_State) Saveable.valueOf(StreamUtils.readInt(is));
        ActionState = (Panel_State) Saveable.valueOf(StreamUtils.readInt(is));
        RestState = (Panel_State) Saveable.valueOf(StreamUtils.readInt(is));

        PlayerP = StreamUtils.readInt(is);
        xfract = StreamUtils.readInt(is);
        x = StreamUtils.readInt(is);
        yfract = StreamUtils.readInt(is);
        y = StreamUtils.readInt(is);

        for (int i = 0; i < 8; i++) {
            over[i].load(is);
        }

        PanelSpriteFunc = (Panel_Sprite_Func) Saveable.valueOf(StreamUtils.readInt(is));

        ID =  StreamUtils.readShort(is);
        picndx =  StreamUtils.readShort(is);
        picnum =  StreamUtils.readShort(is);
        x1 =  StreamUtils.readShort(is);
        y1 =  StreamUtils.readShort(is);
        x2 =  StreamUtils.readShort(is);
        y2 =  StreamUtils.readShort(is);
        vel =  StreamUtils.readShort(is);
        vel_adj =  StreamUtils.readShort(is);
        numpages =  StreamUtils.readShort(is);
        xorig = StreamUtils.readInt(is);
        yorig = StreamUtils.readInt(is);
        flags = StreamUtils.readInt(is);
        yfract = StreamUtils.readInt(is);
        priority = StreamUtils.readInt(is);
        scale = StreamUtils.readInt(is);
        jump_speed = StreamUtils.readInt(is);
        jump_grav = StreamUtils.readInt(is);
        xspeed = StreamUtils.readInt(is);
        tics =  StreamUtils.readShort(is);
        delay =  StreamUtils.readShort(is);
        ang =  StreamUtils.readShort(is);
        rotate_ang =  StreamUtils.readShort(is);
        sin_ndx =  StreamUtils.readShort(is);
        sin_amt =  StreamUtils.readShort(is);
        sin_arc_speed =  StreamUtils.readShort(is);
        bob_height_shift =  StreamUtils.readShort(is);
        shade =  StreamUtils.readShort(is);
        pal =  StreamUtils.readShort(is);
        kill_tics =  StreamUtils.readShort(is);
        WeaponType =  StreamUtils.readShort(is);
    }

    @Override
    public void reset() {
        Prev = Next = null;
        siblingNdx = -1;
        sibling = null;
        State = RetractState = PresentState = ActionState = RestState = null;

        PlayerP = -1;
        xfract = 0;
        x = 0;
        yfract = 0;
        y = 0;

        for (PANEL_SPRITE_OVERLAY panelSpriteOverlay : over) {
            panelSpriteOverlay.State = null;
            panelSpriteOverlay.flags = 0;
            panelSpriteOverlay.tics = 0;
            panelSpriteOverlay.pic = -1;
            panelSpriteOverlay.xoff = -1;
            panelSpriteOverlay.yoff = -1;
        }

        PanelSpriteFunc = null;
        ID = 0;
        picndx = 0;
        picnum = 0;
        x1 = y1 = x2 = y2 = 0;
        vel = vel_adj = 0;

        xorig = yorig = flags = priority = 0;
        scale = 0;
        jump_speed = jump_grav = 0;
        xspeed = 0;
        tics = delay = 0;
        ang = rotate_ang = 0;
        sin_ndx = sin_amt = sin_arc_speed = 0;
        bob_height_shift = 0;
        shade = pal = 0;
        kill_tics = 0;
        WeaponType = 0;
    }

    public static class PANEL_SPRITE_OVERLAY {
        public Panel_State State;
        public int flags;
        public int tics;
        public int pic;
        public int xoff; // from panel sprite center x
        public int yoff; // from panel sprite center y

        public void copy(PANEL_SPRITE_OVERLAY src) {
            this.State = src.State;
            this.flags = src.flags;

            this.tics = src.tics;
            this.pic = src.pic;
            this.xoff = src.xoff;
            this.yoff = src.yoff;
        }

        public void save(OutputStream os) throws IOException {
            StreamUtils.writeInt(os, State != null ? State.ordinal() : -1);
            StreamUtils.writeInt(os, flags);
            StreamUtils.writeShort(os, tics);
            StreamUtils.writeShort(os, pic);
            StreamUtils.writeShort(os, xoff);
            StreamUtils.writeShort(os, yoff);
        }

        public void load(InputStream is) throws IOException {
            State = (Panel_State) Saveable.valueOf(StreamUtils.readInt(is));
            flags = StreamUtils.readInt(is);

            tics =  StreamUtils.readShort(is);
            pic =  StreamUtils.readShort(is);
            xoff =  StreamUtils.readShort(is);
            yoff =  StreamUtils.readShort(is);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Panel_Sprite that = (Panel_Sprite) o;
        return picndx == that.picndx && picnum == that.picnum;
    }

    @Override
    public int hashCode() {
        return Objects.hash(picndx, picnum);
    }
}
