package ru.m210projects.Wang.Type;

import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static ru.m210projects.Wang.Gameutils.MAX_WEAPONS;
import static ru.m210projects.Wang.Inv.MAX_INVENTORY;
import static ru.m210projects.Wang.Main.UserFlag;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Type.ResourceHandler.episodeManager;

public class DemoFile {

    public static final String header = "SWDEM";

    public int rcnt;
    public int reccnt;
    public Input[][] recsync;

    public UserFlag userFlag;
    public String mapname;
    public int numplayers;
    public int Episode;
    public int Level;
    public int Skill;
    public String LevelSong;
    public int[] Flags;
    public int[] Armors;
    public int[] Health;
    public int[] MaxHealth;
    public int[] WpnFlags;
    public byte[] WpnRocketHeats;
    public byte[] WpnRocketNukes;
    public int[][] WpnAmmo;
    public int[][] InventoryPercent;
    public int[][] InventoryAmount;
    public int[] InventoryNums;

    public GameInfo addon;

    // gNet
    public int KillLimit;
    public int TimeLimit;
    public int TimeLimitClock;
    public MultiGameTypes MultiGameType;
    public boolean TeamPlay;
    public boolean HurtTeammate;
    public boolean SpawnMarkers;
    public boolean AutoAim;
    public boolean NoRespawn;
    public boolean Nuke;
    public int nVersion;

    public DemoFile(InputStream is) throws Exception {
        nVersion = 1;
        mapname = StreamUtils.readString(is,5);
        if (mapname.equalsIgnoreCase(header)) {
            nVersion = StreamUtils.readInt(is);
            reccnt = StreamUtils.readInt(is);
            userFlag = UserFlag.parseValue(StreamUtils.readByte(is));
            if (userFlag == UserFlag.Addon) {
                String addonFileName = StreamUtils.readDataString(is).toLowerCase();
                addon = findAddon(addonFileName);
            }

            mapname = StreamUtils.readDataString(is);
            numplayers = StreamUtils.readUnsignedByte(is);
            Episode = StreamUtils.readUnsignedByte(is);
            Level = StreamUtils.readUnsignedByte(is);

            Flags = new int[numplayers];
            Armors = new int[numplayers];
            Health = new int[numplayers];
            MaxHealth = new int[numplayers];

            WpnFlags = new int[numplayers];
            WpnRocketHeats = new byte[numplayers];
            WpnRocketNukes = new byte[numplayers];
            WpnAmmo = new int[numplayers][MAX_WEAPONS];
            InventoryPercent = new int[numplayers][MAX_INVENTORY];
            InventoryAmount = new int[numplayers][MAX_INVENTORY];
            InventoryNums = new int[numplayers];

            for (int p = 0; p < numplayers; p++) {
                Flags[p] = StreamUtils.readInt(is);

                Armors[p] = StreamUtils.readInt(is);
                Health[p] = StreamUtils.readInt(is);
                MaxHealth[p] = StreamUtils.readInt(is);
                WpnFlags[p] = StreamUtils.readInt(is);
                WpnRocketHeats[p] = StreamUtils.readByte(is);
                WpnRocketNukes[p] = StreamUtils.readByte(is);
                for (int i = 0; i < MAX_WEAPONS; i++) {
                    WpnAmmo[p][i] = StreamUtils.readInt(is);
                }

                for (int i = 0; i < MAX_INVENTORY; i++) {
                    InventoryPercent[p][i] = StreamUtils.readInt(is);
                    InventoryAmount[p][i] = StreamUtils.readInt(is);
                }
                InventoryNums[p] = StreamUtils.readInt(is);
            }
        } else {
            mapname += StreamUtils.readString(is,11);
            numplayers = StreamUtils.readUnsignedByte(is);
            Episode = StreamUtils.readUnsignedByte(is);
            Level = StreamUtils.readUnsignedByte(is);
            LevelSong = StreamUtils.readString(is,16);

            Flags = new int[numplayers];

            for (int p = 0; p < numplayers; p++) {
                StreamUtils.readInt(is); // x
                StreamUtils.readInt(is); // y
                StreamUtils.readInt(is); // z
                Flags[p] = StreamUtils.readInt(is);
                StreamUtils.readShort(is); // pang
            }
        }
        Skill = StreamUtils.readShort(is);

        // gNet
        KillLimit = StreamUtils.readInt(is);
        TimeLimit = StreamUtils.readInt(is);
        TimeLimitClock = StreamUtils.readInt(is);
        int i = StreamUtils.readShort(is);
        MultiGameType = i != -1 ? MultiGameTypes.values()[i] : null;
        TeamPlay = StreamUtils.readBoolean(is);
        HurtTeammate = StreamUtils.readBoolean(is);
        SpawnMarkers = StreamUtils.readBoolean(is);
        AutoAim = StreamUtils.readBoolean(is);
        NoRespawn = StreamUtils.readBoolean(is);
        Nuke = StreamUtils.readBoolean(is);

        int inputsize = getInputSize(nVersion);
        int swpackets = is.available() / (numplayers * inputsize);

        recsync = new Input[numplayers][swpackets];
        int rcnt = 0;
        while (is.available() > 0) {
            for (int p = 0; p < numplayers; p++) {
                recsync[p][rcnt] = new Input().readObject(is, nVersion);
            }
            rcnt++;
        }
        reccnt = rcnt;
    }

    @SuppressWarnings("unused")
    public static void convertDemo(Path path, InputStream is) throws IOException {
        try (OutputStream os = Files.newOutputStream(path)) {
            StreamUtils.writeString(os, StreamUtils.readString(is, 5));
            int ver = StreamUtils.readInt(is);
            StreamUtils.writeInt(os, ver); // ver
            int rcnt = StreamUtils.readInt(is);
            StreamUtils.writeInt(os, rcnt);

            StreamUtils.writeByte(os, 2);
            boolean hasAddon = StreamUtils.readBoolean(is);
//            StreamUtils.writeBoolean(os, hasAddon);
//            if (hasAddon) {
                StreamUtils.readDataString(is); //addon path
//            }
            StreamUtils.writeDataString(os, StreamUtils.readDataString(is)); // mapname
            int numplayers = StreamUtils.readByte(is);
            StreamUtils.writeByte(os, numplayers);
            StreamUtils.writeByte(os, StreamUtils.readByte(is)); // episode
            StreamUtils.writeByte(os, StreamUtils.readByte(is)); // level

            for (int p = 0; p < numplayers; p++) {
                StreamUtils.writeInt(os, StreamUtils.readInt(is)); // flags

                StreamUtils.writeInt(os, StreamUtils.readInt(is)); // armor
                StreamUtils.writeInt(os, StreamUtils.readInt(is)); //StreamUtils.writeInt(os, 100); // health
                StreamUtils.writeInt(os, StreamUtils.readInt(is)); //StreamUtils.writeInt(os, 200); // maxhealth

                StreamUtils.writeInt(os, StreamUtils.readInt(is)); // wpnflags
                StreamUtils.writeByte(os, StreamUtils.readByte(is)); //StreamUtils.writeByte(os, 0); // WpnRocketHeats
                StreamUtils.writeByte(os, StreamUtils.readByte(is)); //StreamUtils.writeByte(os, 0); // WpnRocketNukes
                for (int i = 0; i < MAX_WEAPONS; i++) {
                    StreamUtils.writeInt(os, StreamUtils.readInt(is)); // ammo
                }

                for (int i = 0; i < MAX_INVENTORY; i++) {
                    StreamUtils.writeInt(os, StreamUtils.readInt(is));
                    StreamUtils.writeInt(os, StreamUtils.readInt(is));
                }
                StreamUtils.writeInt(os, StreamUtils.readInt(is)); // StreamUtils.writeInt(os, 0); // InventoryNums
            }

            StreamUtils.writeShort(os, StreamUtils.readShort(is)); //Game.Skill);
            StreamUtils.writeInt(os, StreamUtils.readInt(is)); //gNet.KillLimit);
            StreamUtils.writeInt(os, StreamUtils.readInt(is)); //gNet.TimeLimit);
            StreamUtils.writeInt(os, StreamUtils.readInt(is)); //gNet.TimeLimitClock);
            StreamUtils.writeShort(os, StreamUtils.readShort(is)); //gNet.MultiGameType
            StreamUtils.writeByte(os, StreamUtils.readByte(is)); //gNet.TeamPlay
            StreamUtils.writeByte(os, StreamUtils.readByte(is)); //gNet.HurtTeammate
            StreamUtils.writeByte(os, StreamUtils.readByte(is)); //gNet.SpawnMarkers
            StreamUtils.writeByte(os, StreamUtils.readByte(is)); //cfg.AutoAim
            StreamUtils.writeByte(os, StreamUtils.readByte(is)); //gNet.NoRespawn
            StreamUtils.writeByte(os, StreamUtils.readByte(is)); //gNet.Nuke

            int r = 0;
            while (is.available() > 0) {
                for (int p = 0; p < numplayers; p++) {
                    Input input = new Input().readObject(is, ver);
                    input.writeObject(os, ver);
                }
                r++;
            }

            System.out.println(r + " " + rcnt);
        }
    }

    public static int getInputSize(int nVersion) {
        return (nVersion == 1 ? 12 : 16);
    }

    private GameInfo findAddon(String addonFileName) {
        try {
            FileEntry addonEntry = game.getCache().getGameDirectory().getEntry(FileUtils.getPath(addonFileName));
            if (addonEntry.exists()) {
                return episodeManager.getEpisode(addonEntry);
            }
        } catch (Exception e) {
            Console.out.println(e.toString(), OsdColor.RED);
        }
        return null;
    }
}
