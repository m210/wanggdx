package ru.m210projects.Wang.Type;

import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Wang.Enemies.Personality;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class USER {

    //
    // Variables that can be used by actors and Player
    //
    private transient RotatorStr rotator;

    // wall vars for lighting
    public int WallCount;
    public byte[] WallShade; // malloced - save off wall shades for lighting

    public int WallP = -1; // operate on wall instead of sprite
    public State State;
    private StateGroup Rot;
    public State StateStart;
    public State StateEnd;
    public StateGroup StateFallOverride; // a bit kludgy - override std fall state

    public Animator ActorActionFunc;
    public Actor_Action_Set ActorActionSet;
    public Personality Personality;
    public ATTRIBUTE Attrib;
    public int sop_parent = -1; // denotes that this sprite is a part of the
    // sector object - contains info for the SO

    public int ox, oy, oz;

    public int Flags;
    public int Flags2;
    public int Tics;

    public int RotNum;
    public int ID;

    // Health/Pain related
    public int Health;
    public int MaxHealth;

    public int LastDamage; // last damage amount taken
    public int PainThreshold; // amount of damage that can be taken before
    // going into pain frames.

    // jump & fall
    public int jump_speed;
    public int jump_grav;

    // clipmove
    public int ceiling_dist;
    public int floor_dist;
    public int lo_step;
    public int hiz, loz;
    public int zclip; // z height to move up for clipmove
    public int hi_sectp = -1, lo_sectp = -1;
    public int hi_sp = -1, lo_sp = -1;

    public int active_range;
    public int Attach; // attach to sprite if needed - electro snake
    // if a player's sprite points to player structure
    public int PlayerP = -1;
    public int Sibling;
    // precalculated vectors
    public int xchange, ychange, zchange;

    //
    // Possibly used by both.
    //
    public int z_tgt;
    // velocity
    public int vel_tgt;
    public int vel_rate;
    public byte speed; // Ordinal Speed Range 0-3 from slow to fast
    public int Counter;
    public int Counter2;
    public int Counter3;
    public int DamageTics;
    public int BladeDamageTics;
    public int WpnGoal;
    public int Radius; // for distance checking
    public int OverlapZ; // for z overlap variable
    // For actors on fire
    public int flame;

    //
    // Only have a place for actors
    //
    // target player for the enemy - can only handle one player at time
    // PlayerStr tgt_player;
    public int tgt_sp = -1;
    // scaling
    public int scale_speed;
    public int scale_value;
    public int scale_tgt;
    // zig zagging
    public short DistCheck; // signed short!!!
    public int Dist;
    public int TargetDist;
    public int WaitTics;
    // track
    public int track;
    public int point;
    public int track_dir;
    public int track_vel;
    // sliding variables - slide backwards etc
    public int slide_ang;
    public int slide_vel;
    public int slide_dec;
    public int motion_blur_dist;
    public int motion_blur_num;
    public int wait_active_check; // for enemy checking of player
    public int inactive_time; // length of time actor has been unaware of his tgt
    public int sx, sy, sz;
    public int sang;
    public byte spal; // save off default palette number
    public int moveSpriteReturn; // holder for move_sprite return value
    // Need to get rid of these flags
    public int Flag1;
    public int LastWeaponNum;
    public int WeaponNum;
    public int bounce; // count bounces off wall for killing shrap stuff
    // !JIM! my extensions
    public int ShellNum; // This is shell no. 0 to whatever
    // Shell gets deleted when ShellNum < (ShellCount - MAXSHELLS)
    public int FlagOwner; // The spritenum of the original flag
    public int Vis; // Shading upgrade, for shooting, etc...
    public boolean DidAlert; // Has actor done his alert noise before?

    public void writeObject(OutputStream os) throws IOException {
        StreamUtils.writeBoolean(os, rotator != null);
        if (rotator != null) {
            StreamUtils.writeInt(os, rotator.pos);
            StreamUtils.writeInt(os, rotator.open_dest);
            StreamUtils.writeInt(os, rotator.tgt);
            StreamUtils.writeInt(os, rotator.speed);
            StreamUtils.writeInt(os, rotator.orig_speed);
            StreamUtils.writeInt(os, rotator.vel);
            StreamUtils.writeInt(os, rotator.num_walls);

            StreamUtils.writeInt(os, rotator.orig != null ? rotator.orig.length : -1);
            if (rotator.orig != null) {
                for (int i = 0; i < rotator.orig.length; i++) {
                    StreamUtils.writeInt(os, rotator.orig[i].x);
                    StreamUtils.writeInt(os, rotator.orig[i].y);
                }
            }
        }

        StreamUtils.writeInt(os, WallCount);

        StreamUtils.writeInt(os, WallShade != null ? WallShade.length : -1);
        if (WallShade != null) {
            StreamUtils.writeBytes(os, WallShade);
        }

        StreamUtils.writeInt(os, WallP);
        StreamUtils.writeInt(os, State != null ? State.ordinal() : -1);
        StreamUtils.writeInt(os, Rot != null ? Rot.index() : -1);
        StreamUtils.writeInt(os, StateStart != null ? StateStart.ordinal() : -1);
        StreamUtils.writeInt(os, StateEnd != null ? StateEnd.ordinal() : -1);
        StreamUtils.writeInt(os, StateFallOverride != null ? StateFallOverride.index() : -1);
        StreamUtils.writeInt(os, ActorActionFunc != null ? ActorActionFunc.ordinal() : -1);
        StreamUtils.writeInt(os, ActorActionSet != null ? ActorActionSet.ordinal() : -1);
        StreamUtils.writeInt(os, Personality != null ? Personality.ordinal() : -1);
        StreamUtils.writeInt(os, Attrib != null ? Attrib.ordinal() : -1);
        StreamUtils.writeInt(os, sop_parent);
        StreamUtils.writeInt(os, ox);
        StreamUtils.writeInt(os, oy);
        StreamUtils.writeInt(os, oz);
        StreamUtils.writeInt(os, Flags);
        StreamUtils.writeInt(os, Flags2);
        StreamUtils.writeInt(os, Tics);
        StreamUtils.writeShort(os, RotNum);
        StreamUtils.writeShort(os, ID);

        // Health/Pain related
        StreamUtils.writeShort(os, Health);
        StreamUtils.writeShort(os, MaxHealth);
        StreamUtils.writeShort(os, LastDamage);
        StreamUtils.writeShort(os, PainThreshold);
        StreamUtils.writeShort(os, jump_speed);
        StreamUtils.writeShort(os, jump_grav);
        StreamUtils.writeShort(os, ceiling_dist);
        StreamUtils.writeShort(os, floor_dist);
        StreamUtils.writeShort(os, lo_step);
        StreamUtils.writeInt(os, hiz);
        StreamUtils.writeInt(os, loz);
        StreamUtils.writeInt(os, zclip);
        StreamUtils.writeShort(os, hi_sectp);
        StreamUtils.writeShort(os, lo_sectp);
        StreamUtils.writeInt(os, hi_sp);
        StreamUtils.writeInt(os, lo_sp);
        StreamUtils.writeInt(os, active_range);
        StreamUtils.writeShort(os, 0);
        StreamUtils.writeShort(os, Attach);
        StreamUtils.writeInt(os, PlayerP);
        StreamUtils.writeShort(os, Sibling);
        StreamUtils.writeInt(os, xchange);
        StreamUtils.writeInt(os, ychange);
        StreamUtils.writeInt(os, zchange);
        StreamUtils.writeInt(os, z_tgt);
        StreamUtils.writeInt(os, vel_tgt);
        StreamUtils.writeShort(os, vel_rate);
        StreamUtils.writeByte(os, speed);
        StreamUtils.writeShort(os, Counter);
        StreamUtils.writeShort(os, Counter2);
        StreamUtils.writeShort(os, Counter3);
        StreamUtils.writeShort(os, DamageTics);
        StreamUtils.writeShort(os, BladeDamageTics);
        StreamUtils.writeShort(os, WpnGoal);
        StreamUtils.writeInt(os, Radius);
        StreamUtils.writeInt(os, OverlapZ);
        StreamUtils.writeShort(os, flame);
        StreamUtils.writeInt(os, tgt_sp);
        StreamUtils.writeShort(os, scale_speed);
        StreamUtils.writeInt(os, scale_value);
        StreamUtils.writeShort(os, scale_tgt);
        StreamUtils.writeShort(os, DistCheck);
        StreamUtils.writeShort(os, Dist);
        StreamUtils.writeShort(os, TargetDist);
        StreamUtils.writeShort(os, WaitTics);
        StreamUtils.writeShort(os, track);
        StreamUtils.writeShort(os, point);
        StreamUtils.writeShort(os, track_dir);
        StreamUtils.writeInt(os, track_vel);
        StreamUtils.writeShort(os, slide_ang);
        StreamUtils.writeInt(os, slide_vel);
        StreamUtils.writeShort(os, slide_dec);
        StreamUtils.writeShort(os, motion_blur_dist);
        StreamUtils.writeShort(os, motion_blur_num);
        StreamUtils.writeShort(os, wait_active_check);
        StreamUtils.writeShort(os, inactive_time);
        StreamUtils.writeInt(os, sx);
        StreamUtils.writeInt(os, sy);
        StreamUtils.writeInt(os, sz);
        StreamUtils.writeShort(os, sang);
        StreamUtils.writeByte(os, spal);
        StreamUtils.writeInt(os, moveSpriteReturn);
        StreamUtils.writeInt(os, Flag1);
        StreamUtils.writeShort(os, LastWeaponNum);
        StreamUtils.writeShort(os, WeaponNum);
        StreamUtils.writeShort(os, bounce);
        StreamUtils.writeInt(os, ShellNum);
        StreamUtils.writeShort(os, FlagOwner);
        StreamUtils.writeShort(os, Vis);
        StreamUtils.writeBoolean(os, DidAlert);
    }

    public void readObject(InputStream is) throws IOException {
        boolean hasRotator = StreamUtils.readBoolean(is);
        if (hasRotator) {
            if (rotator == null) {
                setRotator(new RotatorStr());
            }
            rotator.pos = StreamUtils.readInt(is);
            rotator.open_dest = StreamUtils.readInt(is);
            rotator.tgt = StreamUtils.readInt(is);
            rotator.speed = StreamUtils.readInt(is);
            rotator.orig_speed = StreamUtils.readInt(is);
            rotator.vel = StreamUtils.readInt(is);
            rotator.num_walls = StreamUtils.readInt(is);

            int len = StreamUtils.readInt(is);
            if (len != -1) {
                rotator.orig = new Vector2i[len];
                for (int i = 0; i < len; i++) {
                    rotator.orig[i] = new Vector2i();
                    rotator.orig[i].x = StreamUtils.readInt(is);
                    rotator.orig[i].y = StreamUtils.readInt(is);
                }
            }
        }

        WallCount = StreamUtils.readInt(is);
        int ShadeLen = StreamUtils.readInt(is);
        if (ShadeLen != -1) {
            WallShade = StreamUtils.readBytes(is, new byte[ShadeLen]);
        }

        WallP = StreamUtils.readInt(is);
        State = (State) Saveable.valueOf(StreamUtils.readInt(is));
        setRot(Saveable.getGroup(StreamUtils.readInt(is)));
        StateStart = (State) Saveable.valueOf(StreamUtils.readInt(is));
        StateEnd = (State) Saveable.valueOf(StreamUtils.readInt(is));
        StateFallOverride = Saveable.getGroup(StreamUtils.readInt(is));
        ActorActionFunc = (Animator) Saveable.valueOf(StreamUtils.readInt(is));
        ActorActionSet = (Actor_Action_Set) Saveable.valueOf(StreamUtils.readInt(is));
        Personality = (Personality) Saveable.valueOf(StreamUtils.readInt(is));
        Attrib = (ATTRIBUTE) Saveable.valueOf(StreamUtils.readInt(is));
        sop_parent = StreamUtils.readInt(is);
        ox = StreamUtils.readInt(is);
        oy = StreamUtils.readInt(is);
        oz = StreamUtils.readInt(is);
        Flags = StreamUtils.readInt(is);
        Flags2 = StreamUtils.readInt(is);
        Tics = StreamUtils.readInt(is);
        RotNum =  StreamUtils.readShort(is);
        ID =  StreamUtils.readShort(is);

        // Health/Pain related
        Health =  StreamUtils.readShort(is);
        MaxHealth =  StreamUtils.readShort(is);
        LastDamage =  StreamUtils.readShort(is);
        PainThreshold =  StreamUtils.readShort(is);
        jump_speed =  StreamUtils.readShort(is);
        jump_grav =  StreamUtils.readShort(is);
        ceiling_dist =  StreamUtils.readShort(is);
        floor_dist =  StreamUtils.readShort(is);
        lo_step =  StreamUtils.readShort(is);
        hiz = StreamUtils.readInt(is);
        loz = StreamUtils.readInt(is);
        zclip = StreamUtils.readInt(is);
        hi_sectp =  StreamUtils.readShort(is);
        lo_sectp =  StreamUtils.readShort(is);
        hi_sp = StreamUtils.readInt(is);
        lo_sp = StreamUtils.readInt(is);
        active_range = StreamUtils.readInt(is);
        StreamUtils.readShort(is);
        Attach = StreamUtils.readShort(is);
        PlayerP = StreamUtils.readInt(is);
        Sibling =  StreamUtils.readShort(is);
        xchange = StreamUtils.readInt(is);
        ychange = StreamUtils.readInt(is);
        zchange = StreamUtils.readInt(is);
        z_tgt = StreamUtils.readInt(is);
        vel_tgt = StreamUtils.readInt(is);
        vel_rate =  StreamUtils.readShort(is);
        speed = StreamUtils.readByte(is);
        Counter =  StreamUtils.readShort(is);
        Counter2 =  StreamUtils.readShort(is);
        Counter3 =  StreamUtils.readShort(is);
        DamageTics =  StreamUtils.readShort(is);
        BladeDamageTics =  StreamUtils.readShort(is);
        WpnGoal =  StreamUtils.readShort(is);
        Radius = StreamUtils.readInt(is);
        OverlapZ = StreamUtils.readInt(is);
        flame =  StreamUtils.readShort(is);
        tgt_sp = StreamUtils.readInt(is);
        scale_speed =  StreamUtils.readShort(is);
        scale_value = StreamUtils.readInt(is);
        scale_tgt =  StreamUtils.readShort(is);
        DistCheck =  StreamUtils.readShort(is);
        Dist =  StreamUtils.readShort(is);
        TargetDist =  StreamUtils.readShort(is);
        WaitTics =  StreamUtils.readShort(is);
        track =  StreamUtils.readShort(is);
        point =  StreamUtils.readShort(is);
        track_dir =  StreamUtils.readShort(is);
        track_vel = StreamUtils.readInt(is);
        slide_ang = StreamUtils.readShort(is);
        slide_vel = StreamUtils.readInt(is);
        slide_dec =  StreamUtils.readShort(is);
        motion_blur_dist =  StreamUtils.readShort(is);
        motion_blur_num =  StreamUtils.readShort(is);
        wait_active_check =  StreamUtils.readShort(is);
        inactive_time =  StreamUtils.readShort(is);
        sx = StreamUtils.readInt(is);
        sy = StreamUtils.readInt(is);
        sz = StreamUtils.readInt(is);
        sang =  StreamUtils.readShort(is);
        spal = StreamUtils.readByte(is);
        moveSpriteReturn = StreamUtils.readInt(is);
        Flag1 = StreamUtils.readInt(is);
        LastWeaponNum =  StreamUtils.readShort(is);
        WeaponNum =  StreamUtils.readShort(is);
        bounce =  StreamUtils.readShort(is);
        ShellNum = StreamUtils.readInt(is);
        FlagOwner =  StreamUtils.readShort(is);
        Vis =  StreamUtils.readShort(is);
        DidAlert = StreamUtils.readBoolean(is);
    }

    public void copy(USER src) {
        if (src.rotator != null) {
            if (rotator == null) {
                setRotator(new RotatorStr());
            }
            rotator.copy(src.rotator);
        } else {
            this.setRotator(null);
        }

        this.WallCount = src.WallCount;
        if (src.WallShade != null) {
            if (this.WallShade == null) {
                this.WallShade = new byte[src.WallShade.length];
            }
            System.arraycopy(src.WallShade, 0, this.WallShade, 0, src.WallShade.length);
        } else {
            this.WallShade = null;
        }

        this.WallP = src.WallP;
        this.State = src.State;
        this.setRot(src.Rot);
        this.StateStart = src.StateStart;
        this.StateEnd = src.StateEnd;
        this.StateFallOverride = src.StateFallOverride;
        this.ActorActionFunc = src.ActorActionFunc;
        this.ActorActionSet = src.ActorActionSet;
        this.Personality = src.Personality;
        this.Attrib = src.Attrib;

        this.sop_parent = src.sop_parent;
        this.ox = src.ox;
        this.oy = src.oy;
        this.oz = src.oz;
        this.Flags = src.Flags;
        this.Flags2 = src.Flags2;
        this.Tics = src.Tics;
        this.RotNum = src.RotNum;
        this.ID = src.ID;

        // Health/Pain related
        this.Health = src.Health;
        this.MaxHealth = src.MaxHealth;
        this.LastDamage = src.LastDamage;
        this.PainThreshold = src.PainThreshold;
        this.jump_speed = src.jump_speed;
        this.jump_grav = src.jump_grav;
        this.ceiling_dist = src.ceiling_dist;
        this.floor_dist = src.floor_dist;
        this.lo_step = src.lo_step;
        this.hiz = src.hiz;
        this.loz = src.loz;
        this.zclip = src.zclip;
        this.hi_sectp = src.hi_sectp;
        this.lo_sectp = src.lo_sectp;
        this.hi_sp = src.hi_sp;
        this.lo_sp = src.lo_sp;
        this.active_range = src.active_range;
        this.Attach = src.Attach;
        this.PlayerP = src.PlayerP;
        this.Sibling = src.Sibling;
        this.xchange = src.xchange;
        this.ychange = src.ychange;
        this.zchange = src.zchange;
        this.z_tgt = src.z_tgt;
        this.vel_tgt = src.vel_tgt;
        this.vel_rate = src.vel_rate;
        this.speed = src.speed;
        this.Counter = src.Counter;
        this.Counter2 = src.Counter2;
        this.Counter3 = src.Counter3;
        this.DamageTics = src.DamageTics;
        this.BladeDamageTics = src.BladeDamageTics;
        this.WpnGoal = src.WpnGoal;
        this.Radius = src.Radius;
        this.OverlapZ = src.OverlapZ;
        this.flame = src.flame;
        this.tgt_sp = src.tgt_sp;
        this.scale_speed = src.scale_speed;
        this.scale_value = src.scale_value;
        this.scale_tgt = src.scale_tgt;
        this.DistCheck = src.DistCheck;
        this.Dist = src.Dist;
        this.TargetDist = src.TargetDist;
        this.WaitTics = src.WaitTics;
        this.track = src.track;
        this.point = src.point;
        this.track_dir = src.track_dir;
        this.track_vel = src.track_vel;
        this.slide_ang = src.slide_ang;
        this.slide_vel = src.slide_vel;
        this.slide_dec = src.slide_dec;
        this.motion_blur_dist = src.motion_blur_dist;
        this.motion_blur_num = src.motion_blur_num;
        this.wait_active_check = src.wait_active_check;
        this.inactive_time = src.inactive_time;
        this.sx = src.sx;
        this.sy = src.sy;
        this.sz = src.sz;
        this.sang = src.sang;
        this.spal = src.spal;
        this.moveSpriteReturn = src.moveSpriteReturn;
        this.Flag1 = src.Flag1;
        this.LastWeaponNum = src.LastWeaponNum;
        this.WeaponNum = src.WeaponNum;
        this.bounce = src.bounce;
        this.ShellNum = src.ShellNum;
        this.FlagOwner = src.FlagOwner;
        this.Vis = src.Vis;
        this.DidAlert = src.DidAlert;
    }

    public StateGroup getRot() {
        return Rot;
    }

    public void setRot(StateGroup rot) {
        Rot = rot;
    }

    public RotatorStr getRotator() {
        return rotator;
    }

    public void setRotator(RotatorStr rotator) {
        this.rotator = rotator;
    }
}
