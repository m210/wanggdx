package ru.m210projects.Wang.Type;

public interface StateGroup {

    State getState(int rotation, int offset);

    State getState(int rotation);

    int index();

    void setIndex(int index);

}

