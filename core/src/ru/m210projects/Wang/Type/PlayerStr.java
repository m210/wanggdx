package ru.m210projects.Wang.Type;

import org.jetbrains.annotations.Nullable;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Wang.Player;
import ru.m210projects.Wang.Player.Player_Action_Func;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static ru.m210projects.Wang.Game.isOriginal;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Inv.MAX_INVENTORY;
import static ru.m210projects.Wang.Main.boardService;

public class PlayerStr {
    // variable that fit in the sprite or user structure
    public int posx, posy, posz;
    // interpolation
    public int oposx, oposy, oposz;
    public int obob_z, obob_amt;
    public float oang, ohoriz;
    // holds last valid move position
    public int lv_sectnum;
    public int lv_x, lv_y, lv_z;
    public int remote_sprite;
    public Remote_Control remote;
    public int sop_remote;
    public int sop; // will either be sop_remote or sop_control
    public int jump_count, jump_speed; // jumping
    public int down_speed, up_speed; // diving
    public int z_speed, oz_speed; // used for diving and flying instead of down_speed, up_speed
    public int climb_ndx;
    public int hiz, loz;
    public int ceiling_dist, floor_dist;
    public int hi_sectp = -1, lo_sectp = -1; // sector
    public int hi_sp = -1, lo_sp = -1; // sprite
    public transient int last_camera_sp;
    public transient int camera_dist; // view mode dist
    public transient int circle_camera_dist;
    public transient int six, siy, siz; // save player interp position for PlayerSprite
    public transient int siang;
    public int xvect, yvect;
    public int oxvect, oyvect;
    public int friction;
    public int slide_xvect, slide_yvect;
    public int slide_ang;
    public int slide_dec;
    public float drive_angvel;
    public float drive_oangvel;
    // scroll 2D mode stuff
    public int scr_x, scr_y, oscr_x, oscr_y;
    public int scr_xvect, scr_yvect;
    public int scr_ang, oscr_ang, scr_sectnum;
    public int view_outside_dang; // outside view delta ang
    public transient int circle_camera_ang;
    public transient int camera_check_time_delay;
    public int cursectnum, lastcursectnum;
    public int turn180_target; // 180 degree turn
    public float pang, horiz, horizbase, horizoff;
    // variables that do not fit into sprite structure
    public int hvel, tilt, tilt_dest;
    public int recoil_amt;
    public int recoil_speed;
    public int recoil_ndx;
    public int recoil_horizoff;
    public int oldposx, oldposy, oldposz;
    public int RevolveX, RevolveY;
    public int RevolveDeltaAng, RevolveAng;
    // under vars are for wading and swimming
    public int PlayerSprite, PlayerUnderSprite;
    public int pnum; // carry aint the player number
    public int LadderSector, LadderAngle;
    public int lx, ly; // ladder x and y
    public int JumpDuration;
    public int WadeDepth;
    public int bob_amt;
    public int bob_ndx;
    public int bcnt; // bob count
    public int bob_z;
    // Multiplayer variables
    public final transient Input input = new Input();
    // must start out as 0
    public int playerreadyflag;
    public Player_Action_Func DoPlayerAction;
    public int Flags, Flags2;
    public int KeyPressFlags;
    public int sop_control;
    public int sop_riding;
    public final byte[] HasKey = new byte[NUM_KEYS];
    // Weapon stuff
    public int SwordAng;
    public int WpnGotOnceFlags; // for no respawn mode where weapons are allowed grabbed only once
    public int WpnFlags;
    public short[] WpnAmmo = new short[MAX_WEAPONS];
    public Panel_Sprite PanelSpriteList = new Panel_Sprite().InitList();
    public Panel_Sprite CurWpn;
    public Panel_Sprite[] Wpn = new Panel_Sprite[MAX_WEAPONS];
    public Panel_Sprite Chops;
    public byte WpnRocketType; // rocket type
    public byte WpnRocketHeat; // 5 to 0 range
    public byte WpnRocketNuke; // 1, you have it, or you don't
    public byte WpnFlameType; // Guardian weapons fire
    public byte WpnFirstType; // First weapon type - Sword/Shuriken
    public byte WeaponType; // for weapons with secondary functions
    public int FirePause; // for sector objects - limits rapid firing
    //
    // Inventory Vars
    //
    public int InventoryNum;
    public int InventoryBarTics;
    public Panel_Sprite[] InventorySprite = new Panel_Sprite[MAX_INVENTORY];
    public Panel_Sprite InventorySelectionBox;
    public final short[] InventoryTics = new short[MAX_INVENTORY];
    public short[] InventoryPercent = new short[MAX_INVENTORY];
    public short[] InventoryAmount = new short[MAX_INVENTORY];
    public final boolean[] InventoryActive = new boolean[MAX_INVENTORY];
    public int DiveTics;
    public int DiveDamageTics;
    // Death stuff
    public int DeathType;
    public int Kills;
    public int Killer; // who killed me
    public final int[] KilledPlayer = new int[MAX_SW_PLAYERS];
    public int SecretsFound;
    // Health
    public int Armor;
    public int MaxHealth;
    public String PlayerName; // 32 characters
    public boolean UziShellLeftAlt;
    public boolean UziShellRightAlt;
    public byte TeamColor; // used in team play and also used in regular mulit-play for show
    // palette fading up and down for player hit and get items
    public int FadeTics; // Tics between each fade cycle
    public int FadeAmt; // Current intensity of fade
    public boolean NightVision; // Is player's night vision active?
    public byte StartColor; // Darkest color in color range being used
    //    public byte[] temp_pal = new byte[768]; // temporary working palette
    public int fta, ftq; // First time active and first time quote, for talking in multiplayer games
    public int NumFootPrints; // Number of foot prints left to lay down
    public boolean PlayerTalking; // Is player currently talking
    public int TalkVocnum; // Number of sound that player is using
    public transient VOC3D TalkVocHandle; // Handle of sound in sound queue, to access in Dose's code
    public byte WpnUziType; // Toggle between single or double uzi's if you own 2.
    public byte WpnShotgunType; // Shotgun has normal or fully automatic fire
    public byte WpnShotgunAuto; // 50-0 automatic shotgun rounds
    public byte WpnShotgunLastShell; // Number of last shell fired
    public byte WpnRailType; // Normal Rail Gun or EMP Burst Mode
    public boolean Bloody; // Is player gooey from the slaughter?
    public transient VOC3D nukevochandle; // Stuff for the Nuke
    public boolean InitingNuke;
    public boolean TestNukeInit;
    public boolean NukeInitialized; // Nuke already has counted down
    public int FistAng; // KungFu attack angle
    public byte WpnKungFuMove; // KungFu special moves
    public boolean BunnyMode; // Can shoot Bunnies out of rocket launcher
    public int HitBy; // SpriteNum of whatever player was last hit by
    public int Reverb; // Player's current reverb setting
    public int Heads; // Number of Accursed Heads orbiting player
    public byte last_used_weapon; //GDX last choosed weapon
    public int lookang;

    @Nullable
    public Sprite getSprite() {
        return PlayerSprite != -1 ? boardService.getSprite(PlayerSprite) : null;
    }

    public int getWeapon() {
        USER pu = getUser(PlayerSprite);
        return (PlayerSprite != -1 && pu != null) ? pu.WeaponNum : 0;
    }

    public String getName() {
        return PlayerName == null || PlayerName.isEmpty() ? "Player" + (pnum + 1) : PlayerName;
    }

    public int getAnglei() {
        return ((int) pang & 0x7FF);
    }

    public float getAnglef() {
        if (isOriginal()) {
            return getAnglei();
        }
        return pang;
    }

    public float getHorizf() {
        if (isOriginal()) {
            return getHorizi();
        }
        return horiz;
    }

    public int getHorizi() {
        return (int) horiz;
    }

    public void reset() {
        this.posx = 0;
        this.posy = 0;
        this.posz = 0;
        this.oposx = 0;
        this.oposy = 0;
        this.oposz = 0;
        this.oang = 0;
        this.ohoriz = 0;
        this.obob_z = 0;
        this.obob_amt = 0;
        this.lv_sectnum = 0;
        this.lv_x = 0;
        this.lv_y = 0;
        this.lv_z = 0;
        this.remote_sprite = -1;
        this.remote = null;
        this.sop_remote = -1;
        this.sop = -1;
        this.jump_count = 0;
        this.jump_speed = 0;
        this.down_speed = 0;
        this.up_speed = 0;
        this.z_speed = 0;
        this.oz_speed = 0;
        this.climb_ndx = 0;
        this.hiz = 0;
        this.loz = 0;
        this.ceiling_dist = 0;
        this.floor_dist = 0;
        this.hi_sectp = -1;
        this.lo_sectp = -1;
        this.hi_sp = -1;
        this.lo_sp = -1;
        this.last_camera_sp = -1;
        this.camera_dist = 0;
        this.circle_camera_dist = 0;
        this.six = 0;
        this.siy = 0;
        this.siz = 0;
        this.siang = 0;
        this.xvect = 0;
        this.yvect = 0;
        this.oxvect = 0;
        this.oyvect = 0;
        this.friction = 0;
        this.slide_xvect = 0;
        this.slide_yvect = 0;
        this.slide_ang = 0;
        this.slide_dec = 0;
        this.drive_angvel = 0;
        this.drive_oangvel = 0;
        this.scr_x = 0;
        this.scr_y = 0;
        this.oscr_x = 0;
        this.oscr_y = 0;
        this.scr_xvect = 0;
        this.scr_yvect = 0;
        this.scr_ang = 0;
        this.oscr_ang = 0;
        this.scr_sectnum = 0;
        this.view_outside_dang = 0;
        this.circle_camera_ang = 0;
        this.camera_check_time_delay = 0;
        this.pang = 0;
        this.cursectnum = 0;
        this.lastcursectnum = 0;
        this.turn180_target = 0;
        this.horizbase = 0;
        this.horiz = 0;
        this.horizoff = 0;
        this.hvel = 0;
        this.tilt = 0;
        this.tilt_dest = 0;
        this.recoil_amt = 0;
        this.recoil_speed = 0;
        this.recoil_ndx = 0;
        this.recoil_horizoff = 0;
        this.oldposx = 0;
        this.oldposy = 0;
        this.oldposz = 0;
        this.RevolveX = 0;
        this.RevolveY = 0;
        this.RevolveDeltaAng = 0;
        this.RevolveAng = 0;
        this.PlayerSprite = -1;
        this.PlayerUnderSprite = -1;
        this.pnum = 0;
        this.LadderSector = 0;
        this.LadderAngle = 0;
        this.lx = 0;
        this.ly = 0;
        this.JumpDuration = 0;
        this.WadeDepth = 0;
        this.bob_amt = 0;
        this.bob_ndx = 0;
        this.bcnt = 0;
        this.bob_z = 0;
        this.input.reset();
        this.playerreadyflag = 0;
        this.DoPlayerAction = null;
        this.Flags = 0;
        this.Flags2 = 0;
        this.KeyPressFlags = 0;
        this.sop_control = -1;
        this.sop_riding = -1;
        List.Init(PanelSpriteList);
        Arrays.fill(HasKey, (byte) 0);
        this.SwordAng = 0;
        this.WpnGotOnceFlags = 0;
        this.WpnFlags = 0;
        Arrays.fill(WpnAmmo, (short) 0);
        this.CurWpn = null;
        Arrays.fill(Wpn, null);
        this.Chops = null;
        this.WpnRocketType = 0;
        this.WpnRocketHeat = 0;
        this.WpnRocketNuke = 0;
        this.WpnFlameType = 0;
        this.WpnFirstType = 0;
        this.WeaponType = 0;
        this.FirePause = 0;
        this.InventoryNum = 0;
        this.InventoryBarTics = 0;
        Arrays.fill(InventorySprite, null);
        this.InventorySelectionBox = null;
        Arrays.fill(InventoryTics, (short) 0);
        Arrays.fill(InventoryPercent, (short) 0);
        Arrays.fill(InventoryAmount, (short) 0);
        Arrays.fill(InventoryActive, false);
        this.DiveTics = 0;
        this.DiveDamageTics = 0;
        this.DeathType = 0;
        this.Kills = 0;
        this.Killer = 0;
        Arrays.fill(KilledPlayer, 0);
        this.SecretsFound = 0;
        this.Armor = 0;
        this.MaxHealth = 0;
        this.PlayerName = null;
        this.UziShellLeftAlt = false;
        this.UziShellRightAlt = false;
        this.TeamColor = 0;
        this.FadeTics = 0;
        this.FadeAmt = 0;
        this.NightVision = false;
        this.StartColor = 0;
//        Arrays.fill(temp_pal, (byte) 0);
        this.fta = 0;
        this.ftq = 0;
        this.NumFootPrints = 0;
        this.PlayerTalking = false;
        this.TalkVocnum = -1;
        this.TalkVocHandle = null;
        this.WpnUziType = 0;
        this.WpnShotgunType = 0;
        this.WpnShotgunAuto = 0;
        this.WpnShotgunLastShell = 0;
        this.WpnRailType = 0;
        this.Bloody = false;
        this.nukevochandle = null;
        this.InitingNuke = false;
        this.TestNukeInit = false;
        this.NukeInitialized = false;
        this.FistAng = 0;
        this.WpnKungFuMove = 0;
        this.BunnyMode = false;
        this.HitBy = 0;
        this.Reverb = 0;
        this.Heads = 0;

        this.last_used_weapon = 0;
        this.lookang = 0;
    }

    public void writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, posx);
        StreamUtils.writeInt(os, posy);
        StreamUtils.writeInt(os, posz);
        StreamUtils.writeInt(os, oposx);
        StreamUtils.writeInt(os, oposy);
        StreamUtils.writeInt(os, oposz);
        StreamUtils.writeFloat(os, oang);
        StreamUtils.writeFloat(os, ohoriz);
        StreamUtils.writeInt(os, obob_z);
        StreamUtils.writeInt(os, obob_amt);
        StreamUtils.writeShort(os, lv_sectnum);
        StreamUtils.writeInt(os, lv_x);
        StreamUtils.writeInt(os, lv_y);
        StreamUtils.writeInt(os, lv_z);
        StreamUtils.writeInt(os, remote_sprite);

        if (remote == null) {
            remote = new Remote_Control();
        }
        remote.writeObject(os);

        StreamUtils.writeInt(os, sop_remote);
        StreamUtils.writeInt(os, sop);
        StreamUtils.writeInt(os, jump_count);
        StreamUtils.writeInt(os, jump_speed);
        StreamUtils.writeShort(os, down_speed);
        StreamUtils.writeShort(os, up_speed);
        StreamUtils.writeInt(os, z_speed);
        StreamUtils.writeInt(os, oz_speed);
        StreamUtils.writeInt(os, climb_ndx);
        StreamUtils.writeInt(os, hiz);
        StreamUtils.writeInt(os, loz);
        StreamUtils.writeInt(os, ceiling_dist);
        StreamUtils.writeInt(os, floor_dist);
        StreamUtils.writeInt(os, hi_sectp);
        StreamUtils.writeInt(os, lo_sectp);
        StreamUtils.writeInt(os, hi_sp);
        StreamUtils.writeInt(os, lo_sp);
        StreamUtils.writeInt(os, last_camera_sp);
        StreamUtils.writeInt(os, camera_dist);
        StreamUtils.writeInt(os, circle_camera_dist);
        StreamUtils.writeInt(os, xvect);
        StreamUtils.writeInt(os, yvect);
        StreamUtils.writeInt(os, oxvect);
        StreamUtils.writeInt(os, oyvect);
        StreamUtils.writeInt(os, friction);
        StreamUtils.writeInt(os, slide_xvect);
        StreamUtils.writeInt(os, slide_yvect);
        StreamUtils.writeShort(os, slide_ang);
        StreamUtils.writeInt(os, slide_dec);
        StreamUtils.writeFloat(os, drive_angvel);
        StreamUtils.writeFloat(os, drive_oangvel);
        StreamUtils.writeInt(os, scr_x);
        StreamUtils.writeInt(os, scr_y);
        StreamUtils.writeInt(os, oscr_x);
        StreamUtils.writeInt(os, oscr_y);
        StreamUtils.writeInt(os, scr_xvect);
        StreamUtils.writeInt(os, scr_yvect);
        StreamUtils.writeShort(os, scr_ang);
        StreamUtils.writeShort(os, oscr_ang);
        StreamUtils.writeShort(os, scr_sectnum);
        StreamUtils.writeShort(os, view_outside_dang);
        StreamUtils.writeShort(os, circle_camera_ang);
        StreamUtils.writeShort(os, camera_check_time_delay);
        StreamUtils.writeFloat(os, pang);
        StreamUtils.writeShort(os, cursectnum);
        StreamUtils.writeShort(os, lastcursectnum);
        StreamUtils.writeShort(os, turn180_target);
        StreamUtils.writeFloat(os, horizbase);
        StreamUtils.writeFloat(os, horiz);
        StreamUtils.writeFloat(os, horizoff);
        StreamUtils.writeInt(os, hvel);
        StreamUtils.writeInt(os, tilt);
        StreamUtils.writeInt(os, tilt_dest);
        StreamUtils.writeShort(os, recoil_amt);
        StreamUtils.writeShort(os, recoil_speed);
        StreamUtils.writeShort(os, recoil_ndx);
        StreamUtils.writeShort(os, recoil_horizoff);
        StreamUtils.writeInt(os, oldposx);
        StreamUtils.writeInt(os, oldposy);
        StreamUtils.writeInt(os, oldposz);
        StreamUtils.writeInt(os, RevolveX);
        StreamUtils.writeInt(os, RevolveY);
        StreamUtils.writeShort(os, RevolveDeltaAng);
        StreamUtils.writeShort(os, RevolveAng);
        StreamUtils.writeShort(os, PlayerSprite);
        StreamUtils.writeShort(os, PlayerUnderSprite);

        StreamUtils.writeShort(os, pnum);
        StreamUtils.writeShort(os, LadderSector);
        StreamUtils.writeShort(os, LadderAngle);
        StreamUtils.writeInt(os, lx);
        StreamUtils.writeInt(os, ly);
        StreamUtils.writeShort(os, JumpDuration);
        StreamUtils.writeShort(os, WadeDepth);
        StreamUtils.writeShort(os, bob_amt);
        StreamUtils.writeShort(os, bob_ndx);
        StreamUtils.writeShort(os, bcnt);
        StreamUtils.writeInt(os, bob_z);
        StreamUtils.writeInt(os, playerreadyflag);
        StreamUtils.writeInt(os, DoPlayerAction != null ? DoPlayerAction.ordinal() : -1);
        StreamUtils.writeInt(os, Flags);
        StreamUtils.writeInt(os, Flags2);
        StreamUtils.writeInt(os, KeyPressFlags);
        StreamUtils.writeInt(os, sop_control);
        StreamUtils.writeInt(os, sop_riding);

        StreamUtils.writeBytes(os, HasKey);
        StreamUtils.writeShort(os, SwordAng);
        StreamUtils.writeInt(os, WpnGotOnceFlags);
        StreamUtils.writeInt(os, WpnFlags);

        for (int i = 0; i < MAX_WEAPONS; i++) {
            StreamUtils.writeShort(os, WpnAmmo[i]);
        }

        writeSpriteList(os);
        StreamUtils.writeInt(os, List.PanelSpriteToNdx(PanelSpriteList, CurWpn));
        for (int ndx = 0; ndx < MAX_WEAPONS; ndx++) {
            StreamUtils.writeInt(os, List.PanelSpriteToNdx(PanelSpriteList, Wpn[ndx]));
        }
        StreamUtils.writeInt(os, List.PanelSpriteToNdx(PanelSpriteList, Chops));

        StreamUtils.writeByte(os, WpnRocketType);
        StreamUtils.writeByte(os, WpnRocketHeat);
        StreamUtils.writeByte(os, WpnRocketNuke);
        StreamUtils.writeByte(os, WpnFlameType);
        StreamUtils.writeByte(os, WpnFirstType);
        StreamUtils.writeByte(os, WeaponType);
        StreamUtils.writeShort(os, FirePause);
        StreamUtils.writeShort(os, InventoryNum);
        StreamUtils.writeShort(os, InventoryBarTics);

        for (int ndx = 0; ndx < MAX_INVENTORY; ndx++) {
            StreamUtils.writeInt(os, List.PanelSpriteToNdx(PanelSpriteList, InventorySprite[ndx]));
        }
        StreamUtils.writeInt(os, List.PanelSpriteToNdx(PanelSpriteList, InventorySelectionBox));

        for (int i = 0; i < MAX_INVENTORY; i++) {
            StreamUtils.writeShort(os, InventoryTics[i]);
            StreamUtils.writeShort(os, InventoryPercent[i]);
            StreamUtils.writeShort(os, InventoryAmount[i]);
            StreamUtils.writeByte(os, InventoryActive[i] ? (byte) 1 : 0);
        }
        StreamUtils.writeShort(os, DiveTics);
        StreamUtils.writeShort(os, DiveDamageTics);
        StreamUtils.writeShort(os, DeathType);
        StreamUtils.writeShort(os, Kills);
        StreamUtils.writeShort(os, Killer);
        for (int i = 0; i < MAX_SW_PLAYERS; i++) {
            StreamUtils.writeShort(os, KilledPlayer[i]);
        }

        StreamUtils.writeShort(os, SecretsFound);
        StreamUtils.writeShort(os, Armor);
        StreamUtils.writeShort(os, MaxHealth);

        StreamUtils.writeByte(os, UziShellLeftAlt ? (byte) 1 : 0);
        StreamUtils.writeByte(os, UziShellRightAlt ? (byte) 1 : 0);
        StreamUtils.writeByte(os, TeamColor);
        StreamUtils.writeShort(os, FadeTics);
        StreamUtils.writeShort(os, FadeAmt);
        StreamUtils.writeByte(os, NightVision ? (byte) 1 : 0);
        StreamUtils.writeByte(os, StartColor);
        StreamUtils.writeShort(os, fta);
        StreamUtils.writeShort(os, ftq);
        StreamUtils.writeShort(os, NumFootPrints);

        StreamUtils.writeByte(os, WpnUziType);
        StreamUtils.writeByte(os, WpnShotgunType);
        StreamUtils.writeByte(os, WpnShotgunAuto);
        StreamUtils.writeByte(os, WpnShotgunLastShell);
        StreamUtils.writeByte(os, WpnRailType);
        StreamUtils.writeByte(os, Bloody ? (byte) 1 : 0);

        StreamUtils.writeByte(os, InitingNuke ? (byte) 1 : 0);
        StreamUtils.writeByte(os, TestNukeInit ? (byte) 1 : 0);
        StreamUtils.writeByte(os, NukeInitialized ? (byte) 1 : 0);
        StreamUtils.writeShort(os, FistAng);
        StreamUtils.writeByte(os, WpnKungFuMove);
        StreamUtils.writeByte(os, BunnyMode ? (byte) 1 : 0);
        StreamUtils.writeShort(os, HitBy);
        StreamUtils.writeShort(os, Reverb);
        StreamUtils.writeShort(os, Heads);
        StreamUtils.writeByte(os, last_used_weapon);
    }

    public PlayerStr readObject(InputStream is) throws IOException {
        posx = StreamUtils.readInt(is);
        posy = StreamUtils.readInt(is);
        posz = StreamUtils.readInt(is);
        oposx = StreamUtils.readInt(is);
        oposy = StreamUtils.readInt(is);
        oposz = StreamUtils.readInt(is);
        oang = StreamUtils.readFloat(is);
        ohoriz = StreamUtils.readFloat(is);
        obob_z = StreamUtils.readInt(is);
        obob_amt = StreamUtils.readInt(is);
        lv_sectnum = StreamUtils.readShort(is);
        lv_x = StreamUtils.readInt(is);
        lv_y = StreamUtils.readInt(is);
        lv_z = StreamUtils.readInt(is);
        remote_sprite = StreamUtils.readInt(is);

        remote = new Remote_Control();
        remote.readObject(is);

        sop_remote = StreamUtils.readInt(is);
        sop = StreamUtils.readInt(is);
        jump_count = StreamUtils.readInt(is);
        jump_speed = StreamUtils.readInt(is);
        down_speed = StreamUtils.readShort(is);
        up_speed = StreamUtils.readShort(is);
        z_speed = StreamUtils.readInt(is);
        oz_speed = StreamUtils.readInt(is);
        climb_ndx = StreamUtils.readInt(is);
        hiz = StreamUtils.readInt(is);
        loz = StreamUtils.readInt(is);
        ceiling_dist = StreamUtils.readInt(is);
        floor_dist = StreamUtils.readInt(is);
        hi_sectp = StreamUtils.readInt(is);
        lo_sectp = StreamUtils.readInt(is);
        hi_sp = StreamUtils.readInt(is);
        lo_sp = StreamUtils.readInt(is);
        last_camera_sp = StreamUtils.readInt(is);
        camera_dist = StreamUtils.readInt(is);
        circle_camera_dist = StreamUtils.readInt(is);
        xvect = StreamUtils.readInt(is);
        yvect = StreamUtils.readInt(is);
        oxvect = StreamUtils.readInt(is);
        oyvect = StreamUtils.readInt(is);
        friction = StreamUtils.readInt(is);
        slide_xvect = StreamUtils.readInt(is);
        slide_yvect = StreamUtils.readInt(is);
        slide_ang = StreamUtils.readShort(is);
        slide_dec = StreamUtils.readInt(is);
        drive_angvel = StreamUtils.readFloat(is);
        drive_oangvel = StreamUtils.readFloat(is);
        scr_x = StreamUtils.readInt(is);
        scr_y = StreamUtils.readInt(is);
        oscr_x = StreamUtils.readInt(is);
        oscr_y = StreamUtils.readInt(is);
        scr_xvect = StreamUtils.readInt(is);
        scr_yvect = StreamUtils.readInt(is);
        scr_ang = StreamUtils.readShort(is);
        oscr_ang = StreamUtils.readShort(is);
        scr_sectnum = StreamUtils.readShort(is);
        view_outside_dang = StreamUtils.readShort(is);
        circle_camera_ang = StreamUtils.readShort(is);
        camera_check_time_delay = StreamUtils.readShort(is);
        pang = StreamUtils.readFloat(is);
        cursectnum = StreamUtils.readShort(is);
        lastcursectnum = StreamUtils.readShort(is);
        turn180_target = StreamUtils.readShort(is);
        horizbase = StreamUtils.readFloat(is);
        horiz = StreamUtils.readFloat(is);
        horizoff = StreamUtils.readFloat(is);
        hvel = StreamUtils.readInt(is);
        tilt = StreamUtils.readInt(is);
        tilt_dest = StreamUtils.readInt(is);
        recoil_amt = StreamUtils.readShort(is);
        recoil_speed = StreamUtils.readShort(is);
        recoil_ndx = StreamUtils.readShort(is);
        recoil_horizoff = StreamUtils.readShort(is);
        oldposx = StreamUtils.readInt(is);
        oldposy = StreamUtils.readInt(is);
        oldposz = StreamUtils.readInt(is);
        RevolveX = StreamUtils.readInt(is);
        RevolveY = StreamUtils.readInt(is);
        RevolveDeltaAng = StreamUtils.readShort(is);
        RevolveAng = StreamUtils.readShort(is);
        PlayerSprite = StreamUtils.readShort(is);
        PlayerUnderSprite = StreamUtils.readShort(is);

        pnum = StreamUtils.readShort(is);
        LadderSector = StreamUtils.readShort(is);
        LadderAngle = StreamUtils.readShort(is);
        lx = StreamUtils.readInt(is);
        ly = StreamUtils.readInt(is);
        JumpDuration = StreamUtils.readShort(is);
        WadeDepth = StreamUtils.readShort(is);
        bob_amt = StreamUtils.readShort(is);
        bob_ndx = StreamUtils.readShort(is);
        bcnt = StreamUtils.readShort(is);
        bob_z = StreamUtils.readInt(is);
        playerreadyflag = StreamUtils.readInt(is);
        int func = StreamUtils.readInt(is);
        DoPlayerAction = func != -1 ? Player.Player_Action_Func.values()[func] : null;
        Flags = StreamUtils.readInt(is);
        Flags2 = StreamUtils.readInt(is);
        KeyPressFlags = StreamUtils.readInt(is);
        sop_control = StreamUtils.readInt(is);
        sop_riding = StreamUtils.readInt(is);

        StreamUtils.readBytes(is, HasKey);
        SwordAng = StreamUtils.readShort(is);
        WpnGotOnceFlags = StreamUtils.readInt(is);
        WpnFlags = StreamUtils.readInt(is);

        for (int i = 0; i < MAX_WEAPONS; i++) {
            WpnAmmo[i] = StreamUtils.readShort(is);
        }

        readSpriteList(is);
        CurWpn = List.PanelNdxToSprite(PanelSpriteList, StreamUtils.readInt(is));
        for (int ndx = 0; ndx < MAX_WEAPONS; ndx++) {
            Wpn[ndx] = List.PanelNdxToSprite(PanelSpriteList, StreamUtils.readInt(is));
        }
        Chops = List.PanelNdxToSprite(PanelSpriteList, StreamUtils.readInt(is));

        WpnRocketType = StreamUtils.readByte(is);
        WpnRocketHeat = StreamUtils.readByte(is);
        WpnRocketNuke = StreamUtils.readByte(is);
        WpnFlameType = StreamUtils.readByte(is);
        WpnFirstType = StreamUtils.readByte(is);
        WeaponType = StreamUtils.readByte(is);
        FirePause = StreamUtils.readShort(is);
        InventoryNum = StreamUtils.readShort(is);
        InventoryBarTics = StreamUtils.readShort(is);

        for (int ndx = 0; ndx < MAX_INVENTORY; ndx++) {
            InventorySprite[ndx] = List.PanelNdxToSprite(PanelSpriteList, StreamUtils.readInt(is));
        }
        InventorySelectionBox = List.PanelNdxToSprite(PanelSpriteList, StreamUtils.readInt(is));

        for (int i = 0; i < MAX_INVENTORY; i++) {
            InventoryTics[i] = StreamUtils.readShort(is);
            InventoryPercent[i] = StreamUtils.readShort(is);
            InventoryAmount[i] = StreamUtils.readShort(is);
            InventoryActive[i] = StreamUtils.readBoolean(is);
        }

        DiveTics = StreamUtils.readShort(is);
        DiveDamageTics = StreamUtils.readShort(is);
        DeathType = StreamUtils.readShort(is);
        Kills = StreamUtils.readShort(is);
        Killer = StreamUtils.readShort(is);
        for (int i = 0; i < MAX_SW_PLAYERS; i++) {
            KilledPlayer[i] = StreamUtils.readShort(is);
        }

        SecretsFound = StreamUtils.readShort(is);
        Armor = StreamUtils.readShort(is);
        MaxHealth = StreamUtils.readShort(is);

        UziShellLeftAlt = StreamUtils.readBoolean(is);
        UziShellRightAlt = StreamUtils.readBoolean(is);
        TeamColor = StreamUtils.readByte(is);
        FadeTics = StreamUtils.readShort(is);
        FadeAmt = StreamUtils.readShort(is);
        NightVision = StreamUtils.readBoolean(is);
        StartColor = StreamUtils.readByte(is);
        fta = StreamUtils.readShort(is);
        ftq = StreamUtils.readShort(is);
        NumFootPrints = StreamUtils.readShort(is);

        PlayerTalking = false;
        TalkVocnum = -1;
        nukevochandle = null;
        TalkVocHandle = null;

        WpnUziType = StreamUtils.readByte(is);
        WpnShotgunType = StreamUtils.readByte(is);
        WpnShotgunAuto = StreamUtils.readByte(is);
        WpnShotgunLastShell = StreamUtils.readByte(is);
        WpnRailType = StreamUtils.readByte(is);
        Bloody = StreamUtils.readBoolean(is);

        InitingNuke = StreamUtils.readBoolean(is);
        TestNukeInit = StreamUtils.readBoolean(is);
        NukeInitialized = StreamUtils.readBoolean(is);
        FistAng = StreamUtils.readShort(is);
        WpnKungFuMove = StreamUtils.readByte(is);
        BunnyMode = StreamUtils.readBoolean(is);
        HitBy = StreamUtils.readShort(is);
        Reverb = StreamUtils.readShort(is);
        Heads = StreamUtils.readShort(is);
        last_used_weapon = StreamUtils.readByte(is);

        return this;
    }

    public String toString() {
        StringBuilder out = new StringBuilder("posx " + posx + " \r\n");
        out.append("posy ").append(posy).append(" \r\n");
        out.append("posz ").append(posz).append(" \r\n");
        out.append("oposx ").append(oposx).append(" \r\n");
        out.append("oposy ").append(oposy).append(" \r\n");
        out.append("oposz ").append(oposz).append(" \r\n");
        out.append("oang ").append(oang).append(" \r\n");
        out.append("ohoriz ").append(ohoriz).append(" \r\n");
        out.append("obob_z ").append(obob_z).append(" \r\n");
        out.append("obob_amt ").append(obob_amt).append(" \r\n");
        out.append("lv_sectnum ").append(lv_sectnum).append(" \r\n");
        out.append("lv_x ").append(lv_x).append(" \r\n");
        out.append("lv_y ").append(lv_y).append(" \r\n");
        out.append("lv_z ").append(lv_z).append(" \r\n");
        out.append("remote_sprite ").append(remote_sprite).append(" \r\n");
        out.append("Class remote ").append(remote).append(" \r\n");
        out.append("sop_remote ").append(sop_remote).append(" \r\n");
        out.append("sop ").append(sop).append(" \r\n");
        out.append("jump_count ").append(jump_count).append(" \r\n");
        out.append("jump_speed ").append(jump_speed).append(" \r\n");
        out.append("down_speed ").append(down_speed).append(" \r\n");
        out.append("up_speed ").append(up_speed).append(" \r\n");
        out.append("z_speed ").append(z_speed).append(" \r\n");
        out.append("oz_speed ").append(oz_speed).append(" \r\n");
        out.append("climb_ndx ").append(climb_ndx).append(" \r\n");
        out.append("hiz ").append(hiz).append(" \r\n");
        out.append("loz ").append(loz).append(" \r\n");
        out.append("ceiling_dist ").append(ceiling_dist).append(" \r\n");
        out.append("floor_dist ").append(floor_dist).append(" \r\n");
        out.append("hi_sectp ").append(hi_sectp).append(" \r\n");
        out.append("lo_sectp ").append(lo_sectp).append(" \r\n");
        out.append("hi_sp ").append(hi_sp).append(" \r\n");
        out.append("lo_sp ").append(lo_sp).append(" \r\n");
        out.append("last_camera_sp ").append(last_camera_sp).append(" \r\n");
        out.append("camera_dist ").append(camera_dist).append(" \r\n");
        out.append("circle_camera_dist ").append(circle_camera_dist).append(" \r\n");
        out.append("six ").append(six).append(" \r\n");
        out.append("siy ").append(siy).append(" \r\n");
        out.append("siz ").append(siz).append(" \r\n");
        out.append("siang ").append(siang).append(" \r\n");
        out.append("xvect ").append(xvect).append(" \r\n");
        out.append("yvect ").append(yvect).append(" \r\n");
        out.append("oxvect ").append(oxvect).append(" \r\n");
        out.append("oyvect ").append(oyvect).append(" \r\n");
        out.append("friction ").append(friction).append(" \r\n");
        out.append("slide_xvect ").append(slide_xvect).append(" \r\n");
        out.append("slide_yvect ").append(slide_yvect).append(" \r\n");
        out.append("slide_ang ").append(slide_ang).append(" \r\n");
        out.append("slide_dec ").append(slide_dec).append(" \r\n");
        out.append("drive_angvel ").append(drive_angvel).append(" \r\n");
        out.append("drive_oangvel ").append(drive_oangvel).append(" \r\n");
        out.append("scr_x ").append(scr_x).append(" \r\n");
        out.append("scr_y ").append(scr_y).append(" \r\n");
        out.append("oscr_x ").append(oscr_x).append(" \r\n");
        out.append("oscr_y ").append(oscr_y).append(" \r\n");
        out.append("scr_xvect ").append(scr_xvect).append(" \r\n");
        out.append("scr_yvect ").append(scr_yvect).append(" \r\n");
        out.append("scr_ang ").append(scr_ang).append(" \r\n");
        out.append("oscr_ang ").append(oscr_ang).append(" \r\n");
        out.append("scr_sectnum ").append(scr_sectnum).append(" \r\n");
        out.append("view_outside_dang ").append(view_outside_dang).append(" \r\n");
        out.append("circle_camera_ang ").append(circle_camera_ang).append(" \r\n");
        out.append("camera_check_time_delay ").append(camera_check_time_delay).append(" \r\n");
        out.append("pang ").append(pang).append(" \r\n");
        out.append("cursectnum ").append(cursectnum).append(" \r\n");
        out.append("lastcursectnum ").append(lastcursectnum).append(" \r\n");
        out.append("turn180_target ").append(turn180_target).append(" \r\n");
        out.append("horizbase ").append(horizbase).append(" \r\n");
        out.append("horiz ").append(horiz).append(" \r\n");
        out.append("horizoff ").append(horizoff).append(" \r\n");
        out.append("hvel ").append(hvel).append(" \r\n");
        out.append("tilt ").append(tilt).append(" \r\n");
        out.append("tilt_dest ").append(tilt_dest).append(" \r\n");
        out.append("recoil_amt ").append(recoil_amt).append(" \r\n");
        out.append("recoil_speed ").append(recoil_speed).append(" \r\n");
        out.append("recoil_ndx ").append(recoil_ndx).append(" \r\n");
        out.append("recoil_horizoff ").append(recoil_horizoff).append(" \r\n");
        out.append("oldposx ").append(oldposx).append(" \r\n");
        out.append("oldposy ").append(oldposy).append(" \r\n");
        out.append("oldposz ").append(oldposz).append(" \r\n");
        out.append("RevolveX ").append(RevolveX).append(" \r\n");
        out.append("RevolveY ").append(RevolveY).append(" \r\n");
        out.append("RevolveDeltaAng ").append(RevolveDeltaAng).append(" \r\n");
        out.append("RevolveAng ").append(RevolveAng).append(" \r\n");
        out.append("PlayerSprite ").append(PlayerSprite).append(" \r\n");
        out.append("PlayerUnderSprite ").append(PlayerUnderSprite).append(" \r\n");
        out.append("pnum ").append(pnum).append(" \r\n");
        out.append("LadderSector ").append(LadderSector).append(" \r\n");
        out.append("LadderAngle ").append(LadderAngle).append(" \r\n");
        out.append("lx ").append(lx).append(" \r\n");
        out.append("ly ").append(ly).append(" \r\n");
        out.append("JumpDuration ").append(JumpDuration).append(" \r\n");
        out.append("WadeDepth ").append(WadeDepth).append(" \r\n");
        out.append("bob_amt ").append(bob_amt).append(" \r\n");
        out.append("bob_ndx ").append(bob_ndx).append(" \r\n");
        out.append("bcnt ").append(bcnt).append(" \r\n");
        out.append("bob_z ").append(bob_z).append(" \r\n");

        out.append("playerreadyflag ").append(playerreadyflag).append(" \r\n");
        out.append("DoPlayerAction ").append(DoPlayerAction).append(" \r\n");
        out.append("Flags ").append(Flags).append(" \r\n");
        out.append("Flags2 ").append(Flags2).append(" \r\n");
        out.append("KeyPressFlags ").append(KeyPressFlags).append(" \r\n");
        out.append("sop_control ").append(sop_control).append(" \r\n");
        out.append("sop_riding ").append(sop_riding).append(" \r\n");
        for (int i = 0; i < NUM_KEYS; i++) {
            out.append("HasKey[").append(i).append("] ").append(HasKey[i]).append(" \r\n");
        }
        out.append("SwordAng ").append(SwordAng).append(" \r\n");
        out.append("WpnGotOnceFlags ").append(WpnGotOnceFlags).append(" \r\n");
        out.append("WpnFlags ").append(WpnFlags).append(" \r\n");
        for (int i = 0; i < MAX_WEAPONS; i++) {
            out.append("WpnAmmo[").append(i).append("] ").append(WpnAmmo[i]).append(" \r\n");
        }
        out.append("Class CurWpn ").append(CurWpn).append(" \r\n");
//		Arrays.fill(Wpn, null); XXX
        out.append("Class Chops ").append(Chops).append(" \r\n");
        out.append("WpnRocketType ").append(WpnRocketType).append(" \r\n");
        out.append("WpnRocketHeat ").append(WpnRocketHeat).append(" \r\n");
        out.append("WpnRocketNuke ").append(WpnRocketNuke).append(" \r\n");
        out.append("WpnFlameType ").append(WpnFlameType).append(" \r\n");
        out.append("WpnFirstType ").append(WpnFirstType).append(" \r\n");
        out.append("WeaponType ").append(WeaponType).append(" \r\n");
        out.append("FirePause ").append(FirePause).append(" \r\n");
        out.append("InventoryNum ").append(InventoryNum).append(" \r\n");
        out.append("InventoryBarTics ").append(InventoryBarTics).append(" \r\n");
//		Arrays.fill(InventorySprite, null); XXX
        out.append("Class InventorySelectionBox ").append(InventorySelectionBox).append(" \r\n");
        for (int i = 0; i < MAX_INVENTORY; i++) {
            out.append("InventoryTics[").append(i).append("] ").append(InventoryTics[i]).append(" \r\n");
        }
        for (int i = 0; i < MAX_INVENTORY; i++) {
            out.append("InventoryPercent[").append(i).append("] ").append(InventoryPercent[i]).append(" \r\n");
        }
        for (int i = 0; i < MAX_INVENTORY; i++) {
            out.append("InventoryAmount[").append(i).append("] ").append(InventoryAmount[i]).append(" \r\n");
        }
        for (int i = 0; i < MAX_INVENTORY; i++) {
            out.append("InventoryActive[").append(i).append("] ").append(InventoryActive[i]).append(" \r\n");
        }
        out.append("DiveTics ").append(DiveTics).append(" \r\n");
        out.append("DiveDamageTics ").append(DiveDamageTics).append(" \r\n");
        out.append("DeathType ").append(DeathType).append(" \r\n");
        out.append("Kills ").append(Kills).append(" \r\n");
        out.append("Killer ").append(Killer).append(" \r\n");
        for (int i = 0; i < MAX_SW_PLAYERS; i++) {
            out.append("KilledPlayer[").append(i).append("] ").append(KilledPlayer[i]).append(" \r\n");
        }
        out.append("SecretsFound ").append(SecretsFound).append(" \r\n");
        out.append("Armor ").append(Armor).append(" \r\n");
        out.append("MaxHealth ").append(MaxHealth).append(" \r\n");
        out.append("PlayerName ").append(PlayerName).append(" \r\n");
        out.append("UziShellLeftAlt ").append(UziShellLeftAlt).append(" \r\n");
        out.append("UziShellRightAlt ").append(UziShellRightAlt).append(" \r\n");
        out.append("TeamColor ").append(TeamColor).append(" \r\n");
        out.append("FadeTics ").append(FadeTics).append(" \r\n");
        out.append("FadeAmt ").append(FadeAmt).append(" \r\n");
        out.append("NightVision ").append(NightVision).append(" \r\n");
        out.append("StartColor ").append(StartColor).append(" \r\n");
//        out += "Pal CRC32 " + CRC32.getChecksum(temp_pal) + " \r\n";
        out.append("fta ").append(fta).append(" \r\n");
        out.append("ftq ").append(ftq).append(" \r\n");
        out.append("NumFootPrints ").append(NumFootPrints).append(" \r\n");
        out.append("PlayerTalking ").append(PlayerTalking).append(" \r\n");
        out.append("TalkVocnum ").append(TalkVocnum).append(" \r\n");
        out.append("WpnUziType ").append(WpnUziType).append(" \r\n");
        out.append("WpnShotgunType ").append(WpnShotgunType).append(" \r\n");
        out.append("WpnShotgunAuto ").append(WpnShotgunAuto).append(" \r\n");
        out.append("WpnShotgunLastShell ").append(WpnShotgunLastShell).append(" \r\n");
        out.append("WpnRailType ").append(WpnRailType).append(" \r\n");
        out.append("Bloody ").append(Bloody).append(" \r\n");
        out.append("InitingNuke ").append(InitingNuke).append(" \r\n");
        out.append("TestNukeInit ").append(TestNukeInit).append(" \r\n");
        out.append("NukeInitialized ").append(NukeInitialized).append(" \r\n");
        out.append("FistAng ").append(FistAng).append(" \r\n");
        out.append("WpnKungFuMove ").append(WpnKungFuMove).append(" \r\n");
        out.append("BunnyMode ").append(BunnyMode).append(" \r\n");
        out.append("HitBy ").append(HitBy).append(" \r\n");
        out.append("Reverb ").append(Reverb).append(" \r\n");
        out.append("Heads ").append(Heads).append(" \r\n");
        out.append("last_used_weapon ").append(last_used_weapon).append(" \r\n");
        out.append("lookang ").append(lookang).append(" \r\n");

        return out.toString();
    }

    public void compare(PlayerStr src) {
        Console.out.println("Comparing...", OsdColor.GREEN);
        if (this.posx != src.posx) {
            Console.out.println("Not match posx: " + this.posx + " != " + src.posx, OsdColor.RED);
        }
        if (this.posy != src.posy) {
            Console.out.println("Not match posy: " + this.posy + " != " + src.posy, OsdColor.RED);
        }
        if (this.posz != src.posz) {
            Console.out.println("Not match posz: " + this.posz + " != " + src.posz, OsdColor.RED);
        }
        if (this.oposx != src.oposx) {
            Console.out.println("Not match oposx: " + this.oposx + " != " + src.oposx, OsdColor.RED);
        }
        if (this.oposy != src.oposy) {
            Console.out.println("Not match oposy: " + this.oposy + " != " + src.oposy, OsdColor.RED);
        }
        if (this.oposz != src.oposz) {
            Console.out.println("Not match oposz: " + this.oposz + " != " + src.oposz, OsdColor.RED);
        }
        if (this.oang != src.oang) {
            Console.out.println("Not match oang: " + this.oang + " != " + src.oang, OsdColor.RED);
        }
        if (this.ohoriz != src.ohoriz) {
            Console.out.println("Not match ohoriz: " + this.ohoriz + " != " + src.ohoriz, OsdColor.RED);
        }
        if (this.obob_z != src.obob_z) {
            Console.out.println("Not match oang: " + this.obob_z + " != " + src.obob_z, OsdColor.RED);
        }
        if (this.obob_amt != src.obob_amt) {
            Console.out.println("Not match ohoriz: " + this.obob_amt + " != " + src.obob_amt, OsdColor.RED);
        }
        if (this.lv_sectnum != src.lv_sectnum) {
            Console.out.println("Not match lv_sectnum: " + this.lv_sectnum + " != " + src.lv_sectnum, OsdColor.RED);
        }
        if (this.lv_x != src.lv_x) {
            Console.out.println("Not match lv_x: " + this.lv_x + " != " + src.lv_x, OsdColor.RED);
        }
        if (this.lv_y != src.lv_y) {
            Console.out.println("Not match lv_y: " + this.lv_y + " != " + src.lv_y, OsdColor.RED);
        }
        if (this.lv_z != src.lv_z) {
            Console.out.println("Not match lv_z: " + this.lv_z + " != " + src.lv_z, OsdColor.RED);
        }
        if (this.remote_sprite != src.remote_sprite) {
            Console.out.println("Not match remote_sprite: " + this.remote_sprite + " != " + src.remote_sprite,
                    OsdColor.RED);
        }

//		if (this.remote != src.remote) XXX
//			Console.out.println("Not match remote: " + this.remote + " != " + src.remote, OsdColor.RED);

        if (this.sop_remote != src.sop_remote) {
            Console.out.println("Not match sop_remote: " + this.sop_remote + " != " + src.sop_remote, OsdColor.RED);
        }
        if (this.sop != src.sop) {
            Console.out.println("Not match sop: " + this.sop + " != " + src.sop, OsdColor.RED);
        }
        if (this.jump_count != src.jump_count) {
            Console.out.println("Not match jump_count: " + this.jump_count + " != " + src.jump_count, OsdColor.RED);
        }
        if (this.jump_speed != src.jump_speed) {
            Console.out.println("Not match jump_speed: " + this.jump_speed + " != " + src.jump_speed, OsdColor.RED);
        }
        if (this.down_speed != src.down_speed) {
            Console.out.println("Not match down_speed: " + this.down_speed + " != " + src.down_speed, OsdColor.RED);
        }
        if (this.up_speed != src.up_speed) {
            Console.out.println("Not match up_speed: " + this.up_speed + " != " + src.up_speed, OsdColor.RED);
        }
        if (this.z_speed != src.z_speed) {
            Console.out.println("Not match z_speed: " + this.z_speed + " != " + src.z_speed, OsdColor.RED);
        }
        if (this.oz_speed != src.oz_speed) {
            Console.out.println("Not match oz_speed: " + this.oz_speed + " != " + src.oz_speed, OsdColor.RED);
        }
        if (this.climb_ndx != src.climb_ndx) {
            Console.out.println("Not match climb_ndx: " + this.climb_ndx + " != " + src.climb_ndx, OsdColor.RED);
        }
        if (this.hiz != src.hiz) {
            Console.out.println("Not match hiz: " + this.hiz + " != " + src.hiz, OsdColor.RED);
        }
        if (this.loz != src.loz) {
            Console.out.println("Not match loz: " + this.loz + " != " + src.loz, OsdColor.RED);
        }
        if (this.ceiling_dist != src.ceiling_dist) {
            Console.out.println("Not match ceiling_dist: " + this.ceiling_dist + " != " + src.ceiling_dist,
                    OsdColor.RED);
        }
        if (this.floor_dist != src.floor_dist) {
            Console.out.println("Not match floor_dist: " + this.floor_dist + " != " + src.floor_dist, OsdColor.RED);
        }
        if (this.hi_sectp != src.hi_sectp) {
            Console.out.println("Not match hi_sectp: " + this.hi_sectp + " != " + src.hi_sectp, OsdColor.RED);
        }
        if (this.lo_sectp != src.lo_sectp) {
            Console.out.println("Not match lo_sectp: " + this.lo_sectp + " != " + src.lo_sectp, OsdColor.RED);
        }
        if (this.hi_sp != src.hi_sp) {
            Console.out.println("Not match hi_sp: " + this.hi_sp + " != " + src.hi_sp, OsdColor.RED);
        }
        if (this.lo_sp != src.lo_sp) {
            Console.out.println("Not match lo_sp: " + this.lo_sp + " != " + src.lo_sp, OsdColor.RED);
        }
        if (this.last_camera_sp != src.last_camera_sp) {
            Console.out.println("Not match last_camera_sp: " + this.last_camera_sp + " != " + src.last_camera_sp,
                    OsdColor.RED);
        }
        if (this.camera_dist != src.camera_dist) {
            Console.out.println("Not match camera_dist: " + this.camera_dist + " != " + src.camera_dist,
                    OsdColor.RED);
        }
        if (this.circle_camera_dist != src.circle_camera_dist) {
            Console.out.println(
                    "Not match circle_camera_dist: " + this.circle_camera_dist + " != " + src.circle_camera_dist,
                    OsdColor.RED);
        }
        if (this.xvect != src.xvect) {
            Console.out.println("Not match xvect: " + this.xvect + " != " + src.xvect, OsdColor.RED);
        }
        if (this.yvect != src.yvect) {
            Console.out.println("Not match yvect: " + this.yvect + " != " + src.yvect, OsdColor.RED);
        }
        if (this.oxvect != src.oxvect) {
            Console.out.println("Not match oxvect: " + this.oxvect + " != " + src.oxvect, OsdColor.RED);
        }
        if (this.oyvect != src.oyvect) {
            Console.out.println("Not match oyvect: " + this.oyvect + " != " + src.oyvect, OsdColor.RED);
        }
        if (this.friction != src.friction) {
            Console.out.println("Not match friction: " + this.friction + " != " + src.friction, OsdColor.RED);
        }
        if (this.slide_xvect != src.slide_xvect) {
            Console.out.println("Not match slide_xvect: " + this.slide_xvect + " != " + src.slide_xvect,
                    OsdColor.RED);
        }
        if (this.slide_yvect != src.slide_yvect) {
            Console.out.println("Not match slide_yvect: " + this.slide_yvect + " != " + src.slide_yvect,
                    OsdColor.RED);
        }
        if (this.slide_ang != src.slide_ang) {
            Console.out.println("Not match slide_ang: " + this.slide_ang + " != " + src.slide_ang, OsdColor.RED);
        }
        if (this.slide_dec != src.slide_dec) {
            Console.out.println("Not match slide_dec: " + this.slide_dec + " != " + src.slide_dec, OsdColor.RED);
        }
        if (this.drive_angvel != src.drive_angvel) {
            Console.out.println("Not match drive_angvel: " + this.drive_angvel + " != " + src.drive_angvel,
                    OsdColor.RED);
        }
        if (this.drive_oangvel != src.drive_oangvel) {
            Console.out.println("Not match drive_oangvel: " + this.drive_oangvel + " != " + src.drive_oangvel,
                    OsdColor.RED);
        }
        if (this.scr_x != src.scr_x) {
            Console.out.println("Not match scr_x: " + this.scr_x + " != " + src.scr_x, OsdColor.RED);
        }
        if (this.scr_y != src.scr_y) {
            Console.out.println("Not match scr_y: " + this.scr_y + " != " + src.scr_y, OsdColor.RED);
        }
        if (this.oscr_x != src.oscr_x) {
            Console.out.println("Not match oscr_x: " + this.oscr_x + " != " + src.oscr_x, OsdColor.RED);
        }
        if (this.oscr_y != src.oscr_y) {
            Console.out.println("Not match oscr_y: " + this.oscr_y + " != " + src.oscr_y, OsdColor.RED);
        }
        if (this.scr_xvect != src.scr_xvect) {
            Console.out.println("Not match scr_xvect: " + this.scr_xvect + " != " + src.scr_xvect, OsdColor.RED);
        }
        if (this.scr_yvect != src.scr_yvect) {
            Console.out.println("Not match scr_yvect: " + this.scr_yvect + " != " + src.scr_yvect, OsdColor.RED);
        }
        if (this.scr_ang != src.scr_ang) {
            Console.out.println("Not match scr_ang: " + this.scr_ang + " != " + src.scr_ang, OsdColor.RED);
        }
        if (this.oscr_ang != src.oscr_ang) {
            Console.out.println("Not match oscr_ang: " + this.oscr_ang + " != " + src.oscr_ang, OsdColor.RED);
        }
        if (this.scr_sectnum != src.scr_sectnum) {
            Console.out.println("Not match scr_sectnum: " + this.scr_sectnum + " != " + src.scr_sectnum,
                    OsdColor.RED);
        }
        if (this.view_outside_dang != src.view_outside_dang) {
            Console.out.println("Not match view_outside_dang: " + this.view_outside_dang + " != " + src.view_outside_dang,
                    OsdColor.RED);
        }
        if (this.circle_camera_ang != src.circle_camera_ang) {
            Console.out.println("Not match circle_camera_ang: " + this.circle_camera_ang + " != " + src.circle_camera_ang,
                    OsdColor.RED);
        }
        if (this.camera_check_time_delay != src.camera_check_time_delay) {
            Console.out.println("Not match camera_check_time_delay: " + this.camera_check_time_delay + " != "
                    + src.camera_check_time_delay, OsdColor.RED);
        }
        if (this.pang != src.pang) {
            Console.out.println("Not match pang: " + this.pang + " != " + src.pang, OsdColor.RED);
        }
        if (this.cursectnum != src.cursectnum) {
            Console.out.println("Not match cursectnum: " + this.cursectnum + " != " + src.cursectnum, OsdColor.RED);
        }
        if (this.lastcursectnum != src.lastcursectnum) {
            Console.out.println("Not match lastcursectnum: " + this.lastcursectnum + " != " + src.lastcursectnum,
                    OsdColor.RED);
        }
        if (this.turn180_target != src.turn180_target) {
            Console.out.println("Not match turn180_target: " + this.turn180_target + " != " + src.turn180_target,
                    OsdColor.RED);
        }
        if (this.horizbase != src.horizbase) {
            Console.out.println("Not match horizbase: " + this.horizbase + " != " + src.horizbase, OsdColor.RED);
        }
        if (this.horiz != src.horiz) {
            Console.out.println("Not match horiz: " + this.horiz + " != " + src.horiz, OsdColor.RED);
        }
        if (this.horizoff != src.horizoff) {
            Console.out.println("Not match horizoff: " + this.horizoff + " != " + src.horizoff, OsdColor.RED);
        }
        if (this.hvel != src.hvel) {
            Console.out.println("Not match hvel: " + this.hvel + " != " + src.hvel, OsdColor.RED);
        }
        if (this.tilt != src.tilt) {
            Console.out.println("Not match tilt: " + this.tilt + " != " + src.tilt, OsdColor.RED);
        }
        if (this.tilt_dest != src.tilt_dest) {
            Console.out.println("Not match tilt_dest: " + this.tilt_dest + " != " + src.tilt_dest, OsdColor.RED);
        }
        if (this.recoil_amt != src.recoil_amt) {
            Console.out.println("Not match recoil_amt: " + this.recoil_amt + " != " + src.recoil_amt, OsdColor.RED);
        }
        if (this.recoil_speed != src.recoil_speed) {
            Console.out.println("Not match recoil_speed: " + this.recoil_speed + " != " + src.recoil_speed,
                    OsdColor.RED);
        }
        if (this.recoil_ndx != src.recoil_ndx) {
            Console.out.println("Not match recoil_ndx: " + this.recoil_ndx + " != " + src.recoil_ndx, OsdColor.RED);
        }
        if (this.recoil_horizoff != src.recoil_horizoff) {
            Console.out.println("Not match recoil_horizoff: " + this.recoil_horizoff + " != " + src.recoil_horizoff,
                    OsdColor.RED);
        }
        if (this.oldposx != src.oldposx) {
            Console.out.println("Not match oldposx: " + this.oldposx + " != " + src.oldposx, OsdColor.RED);
        }
        if (this.oldposy != src.oldposy) {
            Console.out.println("Not match oldposy: " + this.oldposy + " != " + src.oldposy, OsdColor.RED);
        }
        if (this.oldposz != src.oldposz) {
            Console.out.println("Not match oldposz: " + this.oldposz + " != " + src.oldposz, OsdColor.RED);
        }
        if (this.RevolveX != src.RevolveX) {
            Console.out.println("Not match RevolveX: " + this.RevolveX + " != " + src.RevolveX, OsdColor.RED);
        }
        if (this.RevolveY != src.RevolveY) {
            Console.out.println("Not match RevolveY: " + this.RevolveY + " != " + src.RevolveY, OsdColor.RED);
        }
        if (this.RevolveDeltaAng != src.RevolveDeltaAng) {
            Console.out.println("Not match RevolveDeltaAng: " + this.RevolveDeltaAng + " != " + src.RevolveDeltaAng,
                    OsdColor.RED);
        }
        if (this.RevolveAng != src.RevolveAng) {
            Console.out.println("Not match RevolveAng: " + this.RevolveAng + " != " + src.RevolveAng, OsdColor.RED);
        }
        if (this.PlayerSprite != src.PlayerSprite) {
            Console.out.println("Not match PlayerSprite: " + this.PlayerSprite + " != " + src.PlayerSprite,
                    OsdColor.RED);
        }
        if (this.PlayerUnderSprite != src.PlayerUnderSprite) {
            Console.out.println("Not match PlayerUnderSprite: " + this.PlayerUnderSprite + " != " + src.PlayerUnderSprite,
                    OsdColor.RED);
        }
        if (this.pnum != src.pnum) {
            Console.out.println("Not match pnum: " + this.pnum + " != " + src.pnum, OsdColor.RED);
        }
        if (this.LadderSector != src.LadderSector) {
            Console.out.println("Not match LadderSector: " + this.LadderSector + " != " + src.LadderSector,
                    OsdColor.RED);
        }
        if (this.LadderAngle != src.LadderAngle) {
            Console.out.println("Not match LadderAngle: " + this.LadderAngle + " != " + src.LadderAngle,
                    OsdColor.RED);
        }
        if (this.lx != src.lx) {
            Console.out.println("Not match lx: " + this.lx + " != " + src.lx, OsdColor.RED);
        }
        if (this.ly != src.ly) {
            Console.out.println("Not match ly: " + this.ly + " != " + src.ly, OsdColor.RED);
        }
        if (this.JumpDuration != src.JumpDuration) {
            Console.out.println("Not match JumpDuration: " + this.JumpDuration + " != " + src.JumpDuration,
                    OsdColor.RED);
        }
        if (this.WadeDepth != src.WadeDepth) {
            Console.out.println("Not match WadeDepth: " + this.WadeDepth + " != " + src.WadeDepth, OsdColor.RED);
        }
        if (this.bob_amt != src.bob_amt) {
            Console.out.println("Not match bob_amt: " + this.bob_amt + " != " + src.bob_amt, OsdColor.RED);
        }
        if (this.bob_ndx != src.bob_ndx) {
            Console.out.println("Not match bob_ndx: " + this.bob_ndx + " != " + src.bob_ndx, OsdColor.RED);
        }
        if (this.bcnt != src.bcnt) {
            Console.out.println("Not match bcnt: " + this.bcnt + " != " + src.bcnt, OsdColor.RED);
        }
        if (this.bob_z != src.bob_z) {
            Console.out.println("Not match bob_z: " + this.bob_z + " != " + src.bob_z, OsdColor.RED);
        }

        if (this.playerreadyflag != src.playerreadyflag) {
            Console.out.println("Not match playerreadyflag: " + this.playerreadyflag + " != " + src.playerreadyflag,
                    OsdColor.RED);
        }
        if (this.DoPlayerAction != src.DoPlayerAction) {
            Console.out.println("Not match DoPlayerAction: " + this.DoPlayerAction + " != " + src.DoPlayerAction,
                    OsdColor.RED);
        }
        if (this.Flags != src.Flags) {
            Console.out.println("Not match Flags: " + this.Flags + " != " + src.Flags, OsdColor.RED);
        }
        if (this.Flags2 != src.Flags2) {
            Console.out.println("Not match Flags2: " + this.Flags2 + " != " + src.Flags2, OsdColor.RED);
        }
        if (this.KeyPressFlags != src.KeyPressFlags) {
            Console.out.println("Not match KeyPressFlags: " + this.KeyPressFlags + " != " + src.KeyPressFlags,
                    OsdColor.RED);
        }
        if (this.sop_control != src.sop_control) {
            Console.out.println("Not match sop_control: " + this.sop_control + " != " + src.sop_control,
                    OsdColor.RED);
        }
        if (this.sop_riding != src.sop_riding) {
            Console.out.println("Not match sop_riding: " + this.sop_riding + " != " + src.sop_riding, OsdColor.RED);
        }

        for (int i = 0; i < NUM_KEYS; i++) {
            if (this.HasKey[i] != src.HasKey[i]) {
                Console.out.println("Not match HasKey[" + i + "] " + this.HasKey[i] + " != " + src.HasKey[i],
                        OsdColor.RED);
            }
        }

        if (this.SwordAng != src.SwordAng) {
            Console.out.println("Not match SwordAng: " + this.SwordAng + " != " + src.SwordAng, OsdColor.RED);
        }
        if (this.WpnGotOnceFlags != src.WpnGotOnceFlags) {
            Console.out.println("Not match WpnGotOnceFlags: " + this.WpnGotOnceFlags + " != " + src.WpnGotOnceFlags,
                    OsdColor.RED);
        }
        if (this.WpnFlags != src.WpnFlags) {
            Console.out.println("Not match WpnFlags: " + this.WpnFlags + " != " + src.WpnFlags, OsdColor.RED);
        }
        for (int i = 0; i < MAX_WEAPONS; i++) {
            if (this.WpnAmmo[i] != src.WpnAmmo[i]) {
                Console.out.println("Not match WpnAmmo[" + i + "] " + this.WpnAmmo[i] + " != " + src.WpnAmmo[i],
                        OsdColor.RED);
            }
        }
        if (this.WpnRocketType != src.WpnRocketType) {
            Console.out.println("Not match WpnRocketType: " + this.WpnRocketType + " != " + src.WpnRocketType,
                    OsdColor.RED);
        }
        if (this.WpnRocketHeat != src.WpnRocketHeat) {
            Console.out.println("Not match WpnRocketHeat: " + this.WpnRocketHeat + " != " + src.WpnRocketHeat,
                    OsdColor.RED);
        }
        if (this.WpnRocketNuke != src.WpnRocketNuke) {
            Console.out.println("Not match WpnRocketNuke: " + this.WpnRocketNuke + " != " + src.WpnRocketNuke,
                    OsdColor.RED);
        }
        if (this.WpnFlameType != src.WpnFlameType) {
            Console.out.println("Not match WpnFlameType: " + this.WpnFlameType + " != " + src.WpnFlameType,
                    OsdColor.RED);
        }
        if (this.WpnFirstType != src.WpnFirstType) {
            Console.out.println("Not match WpnFirstType: " + this.WpnFirstType + " != " + src.WpnFirstType,
                    OsdColor.RED);
        }
        if (this.WeaponType != src.WeaponType) {
            Console.out.println("Not match WeaponType: " + this.WeaponType + " != " + src.WeaponType, OsdColor.RED);
        }
        if (this.FirePause != src.FirePause) {
            Console.out.println("Not match FirePause: " + this.FirePause + " != " + src.FirePause, OsdColor.RED);
        }
        if (this.InventoryNum != src.InventoryNum) {
            Console.out.println("Not match InventoryNum: " + this.InventoryNum + " != " + src.InventoryNum,
                    OsdColor.RED);
        }
        if (this.InventoryBarTics != src.InventoryBarTics) {
            Console.out.println("Not match InventoryBarTics: " + this.InventoryBarTics + " != " + src.InventoryBarTics,
                    OsdColor.RED);
        }

        for (int i = 0; i < MAX_INVENTORY; i++) {
            if (this.InventoryTics[i] != src.InventoryTics[i]) {
                Console.out.println(
                        "Not match InventoryTics[" + i + "] " + this.InventoryTics[i] + " != " + src.InventoryTics[i],
                        OsdColor.RED);
            }
            if (this.InventoryPercent[i] != src.InventoryPercent[i]) {
                Console.out.println("Not match InventoryPercent[" + i + "] " + this.InventoryPercent[i] + " != "
                        + src.InventoryPercent[i], OsdColor.RED);
            }
            if (this.InventoryAmount[i] != src.InventoryAmount[i]) {
                Console.out.println("Not match InventoryAmount[" + i + "] " + this.InventoryAmount[i] + " != "
                        + src.InventoryAmount[i], OsdColor.RED);
            }
            if (this.InventoryActive[i] != src.InventoryActive[i]) {
                Console.out.println("Not match InventoryActive[" + i + "] " + this.InventoryActive[i] + " != "
                        + src.InventoryActive[i], OsdColor.RED);
            }
        }

        if (this.DiveTics != src.DiveTics) {
            Console.out.println("Not match DiveTics: " + this.DiveTics + " != " + src.DiveTics, OsdColor.RED);
        }
        if (this.DiveDamageTics != src.DiveDamageTics) {
            Console.out.println("Not match DiveDamageTics: " + this.DiveDamageTics + " != " + src.DiveDamageTics,
                    OsdColor.RED);
        }
        if (this.DeathType != src.DeathType) {
            Console.out.println("Not match DeathType: " + this.DeathType + " != " + src.DeathType, OsdColor.RED);
        }
        if (this.Kills != src.Kills) {
            Console.out.println("Not match Kills: " + this.Kills + " != " + src.Kills, OsdColor.RED);
        }
        if (this.Killer != src.Killer) {
            Console.out.println("Not match Killer: " + this.Killer + " != " + src.Killer, OsdColor.RED);
        }

        for (int i = 0; i < MAX_SW_PLAYERS; i++) {
            if (this.KilledPlayer[i] != src.KilledPlayer[i]) {
                Console.out.println(
                        "Not match KilledPlayer[" + i + "] " + this.KilledPlayer[i] + " != " + src.KilledPlayer[i],
                        OsdColor.RED);
            }
        }

        if (this.SecretsFound != src.SecretsFound) {
            Console.out.println("Not match SecretsFound: " + this.SecretsFound + " != " + src.SecretsFound,
                    OsdColor.RED);
        }
        if (this.Armor != src.Armor) {
            Console.out.println("Not match Armor: " + this.Armor + " != " + src.Armor, OsdColor.RED);
        }
        if (this.MaxHealth != src.MaxHealth) {
            Console.out.println("Not match MaxHealth: " + this.MaxHealth + " != " + src.MaxHealth, OsdColor.RED);
        }
        if (this.UziShellLeftAlt != src.UziShellLeftAlt) {
            Console.out.println("Not match UziShellLeftAlt: " + this.UziShellLeftAlt + " != " + src.UziShellLeftAlt,
                    OsdColor.RED);
        }
        if (this.UziShellRightAlt != src.UziShellRightAlt) {
            Console.out.println("Not match UziShellRightAlt: " + this.UziShellRightAlt + " != " + src.UziShellRightAlt,
                    OsdColor.RED);
        }
        if (this.TeamColor != src.TeamColor) {
            Console.out.println("Not match TeamColor: " + this.TeamColor + " != " + src.TeamColor, OsdColor.RED);
        }
        if (this.FadeTics != src.FadeTics) {
            Console.out.println("Not match FadeTics: " + this.FadeTics + " != " + src.FadeTics, OsdColor.RED);
        }
        if (this.FadeAmt != src.FadeAmt) {
            Console.out.println("Not match FadeAmt: " + this.FadeAmt + " != " + src.FadeAmt, OsdColor.RED);
        }
        if (this.NightVision != src.NightVision) {
            Console.out.println("Not match NightVision: " + this.NightVision + " != " + src.NightVision,
                    OsdColor.RED);
        }
        if (this.StartColor != src.StartColor) {
            Console.out.println("Not match StartColor: " + this.StartColor + " != " + src.StartColor, OsdColor.RED);
        }

        if (this.fta != src.fta) {
            Console.out.println("Not match fta: " + this.fta + " != " + src.fta, OsdColor.RED);
        }
        if (this.ftq != src.ftq) {
            Console.out.println("Not match ftq: " + this.ftq + " != " + src.ftq, OsdColor.RED);
        }
        if (this.NumFootPrints != src.NumFootPrints) {
            Console.out.println("Not match NumFootPrints: " + this.NumFootPrints + " != " + src.NumFootPrints,
                    OsdColor.RED);
        }
        if (this.PlayerTalking != src.PlayerTalking) {
            Console.out.println("Not match PlayerTalking: " + this.PlayerTalking + " != " + src.PlayerTalking,
                    OsdColor.RED);
        }
        if (this.TalkVocnum != src.TalkVocnum) {
            Console.out.println("Not match TalkVocnum: " + this.TalkVocnum + " != " + src.TalkVocnum, OsdColor.RED);
        }
        if (this.WpnUziType != src.WpnUziType) {
            Console.out.println("Not match WpnUziType: " + this.WpnUziType + " != " + src.WpnUziType, OsdColor.RED);
        }
        if (this.WpnShotgunType != src.WpnShotgunType) {
            Console.out.println("Not match WpnShotgunType: " + this.WpnShotgunType + " != " + src.WpnShotgunType,
                    OsdColor.RED);
        }
        if (this.WpnShotgunAuto != src.WpnShotgunAuto) {
            Console.out.println("Not match WpnShotgunAuto: " + this.WpnShotgunAuto + " != " + src.WpnShotgunAuto,
                    OsdColor.RED);
        }
        if (this.WpnShotgunLastShell != src.WpnShotgunLastShell) {
            Console.out.println(
                    "Not match WpnShotgunLastShell: " + this.WpnShotgunLastShell + " != " + src.WpnShotgunLastShell,
                    OsdColor.RED);
        }
        if (this.WpnRailType != src.WpnRailType) {
            Console.out.println("Not match WpnRailType: " + this.WpnRailType + " != " + src.WpnRailType,
                    OsdColor.RED);
        }
        if (this.Bloody != src.Bloody) {
            Console.out.println("Not match Bloody: " + this.Bloody + " != " + src.Bloody, OsdColor.RED);
        }
        if (this.InitingNuke != src.InitingNuke) {
            Console.out.println("Not match InitingNuke: " + this.InitingNuke + " != " + src.InitingNuke,
                    OsdColor.RED);
        }
        if (this.TestNukeInit != src.TestNukeInit) {
            Console.out.println("Not match TestNukeInit: " + this.TestNukeInit + " != " + src.TestNukeInit,
                    OsdColor.RED);
        }
        if (this.NukeInitialized != src.NukeInitialized) {
            Console.out.println("Not match NukeInitialized: " + this.NukeInitialized + " != " + src.NukeInitialized,
                    OsdColor.RED);
        }
        if (this.FistAng != src.FistAng) {
            Console.out.println("Not match FistAng: " + this.FistAng + " != " + src.FistAng, OsdColor.RED);
        }
        if (this.WpnKungFuMove != src.WpnKungFuMove) {
            Console.out.println("Not match WpnKungFuMove: " + this.WpnKungFuMove + " != " + src.WpnKungFuMove,
                    OsdColor.RED);
        }
        if (this.BunnyMode != src.BunnyMode) {
            Console.out.println("Not match BunnyMode: " + this.BunnyMode + " != " + src.BunnyMode, OsdColor.RED);
        }
        if (this.HitBy != src.HitBy) {
            Console.out.println("Not match HitBy: " + this.HitBy + " != " + src.HitBy, OsdColor.RED);
        }
        if (this.Reverb != src.Reverb) {
            Console.out.println("Not match Reverb: " + this.Reverb + " != " + src.Reverb, OsdColor.RED);
        }
        if (this.Heads != src.Heads) {
            Console.out.println("Not match Heads: " + this.Heads + " != " + src.Heads, OsdColor.RED);
        }
        if (this.last_used_weapon != src.last_used_weapon) {
            Console.out.println("Not match last_used_weapon: " + this.last_used_weapon + " != " + src.last_used_weapon, OsdColor.RED);
        }
        if (this.lookang != src.lookang) {
            Console.out.println("Not match lookang: " + this.lookang + " != " + src.lookang, OsdColor.RED);
        }

        Console.out.println("Compare completed", OsdColor.GREEN);
    }

    public void readSpriteList(InputStream is) throws IOException {
        List.Init(PanelSpriteList);

        while (true) {
            int ndx = StreamUtils.readInt(is);
            if (ndx == -1) {
                break;
            }

            Panel_Sprite psp = new Panel_Sprite();
            psp.siblingNdx = StreamUtils.readInt(is);
            psp.load(is);

            List.InsertTrail(PanelSpriteList, psp);
        }

        Panel_Sprite next;
        for (Panel_Sprite psp = PanelSpriteList.Next; psp != PanelSpriteList; psp = next) {
            next = psp.Next;
            // sibling is the only PanelSprite (malloced ptr) in the PanelSprite struct
            psp.sibling = List.PanelNdxToSprite(PanelSpriteList, psp.siblingNdx);
        }
    }

    public void writeSpriteList(OutputStream os) throws IOException {
        int ndx = 0;
        for (Panel_Sprite cur = this.PanelSpriteList.Next, n; cur != this.PanelSpriteList; cur = n) {
            n = cur.Next;

            StreamUtils.writeInt(os, ndx);
            StreamUtils.writeInt(os, List.PanelSpriteToNdx(this.PanelSpriteList, cur.sibling));
            cur.save(os);
            ndx++;
        }
        StreamUtils.writeInt(os, -1);
    }

    public void copy(PlayerStr src) {
        this.posx = src.posx;
        this.posy = src.posy;
        this.posz = src.posz;
        this.oposx = src.oposx;
        this.oposy = src.oposy;
        this.oposz = src.oposz;
        this.oang = src.oang;
        this.ohoriz = src.ohoriz;
        this.obob_z = src.obob_z;
        this.obob_amt = src.obob_amt;
        this.lv_sectnum = src.lv_sectnum;
        this.lv_x = src.lv_x;
        this.lv_y = src.lv_y;
        this.lv_z = src.lv_z;
        this.remote_sprite = src.remote_sprite;
        if (src.remote != null) {
            if (this.remote == null) {
                this.remote = new Remote_Control();
            }
            this.remote.copy(src.remote);
        } else {
            this.remote = null;
        }
        this.sop_remote = src.sop_remote;
        this.sop = src.sop;
        this.jump_count = src.jump_count;
        this.jump_speed = src.jump_speed;
        this.down_speed = src.down_speed;
        this.up_speed = src.up_speed;
        this.z_speed = src.z_speed;
        this.oz_speed = src.oz_speed;
        this.climb_ndx = src.climb_ndx;
        this.hiz = src.hiz;
        this.loz = src.loz;
        this.ceiling_dist = src.ceiling_dist;
        this.floor_dist = src.floor_dist;
        this.hi_sectp = src.hi_sectp;
        this.lo_sectp = src.lo_sectp;
        this.hi_sp = src.hi_sp;
        this.lo_sp = src.lo_sp;
        this.last_camera_sp = src.last_camera_sp;
        this.camera_dist = src.camera_dist;
        this.circle_camera_dist = src.circle_camera_dist;
        this.xvect = src.xvect;
        this.yvect = src.yvect;
        this.oxvect = src.oxvect;
        this.oyvect = src.oyvect;
        this.friction = src.friction;
        this.slide_xvect = src.slide_xvect;
        this.slide_yvect = src.slide_yvect;
        this.slide_ang = src.slide_ang;
        this.slide_dec = src.slide_dec;
        this.drive_angvel = src.drive_angvel;
        this.drive_oangvel = src.drive_oangvel;
        this.scr_x = src.scr_x;
        this.scr_y = src.scr_y;
        this.oscr_x = src.oscr_x;
        this.oscr_y = src.oscr_y;
        this.scr_xvect = src.scr_xvect;
        this.scr_yvect = src.scr_yvect;
        this.scr_ang = src.scr_ang;
        this.oscr_ang = src.oscr_ang;
        this.scr_sectnum = src.scr_sectnum;
        this.view_outside_dang = src.view_outside_dang;
        this.circle_camera_ang = src.circle_camera_ang;
        this.camera_check_time_delay = src.camera_check_time_delay;
        this.pang = src.pang;
        this.cursectnum = src.cursectnum;
        this.lastcursectnum = src.lastcursectnum;
        this.turn180_target = src.turn180_target;
        this.horizbase = src.horizbase;
        this.horiz = src.horiz;
        this.horizoff = src.horizoff;
        this.hvel = src.hvel;
        this.tilt = src.tilt;
        this.tilt_dest = src.tilt_dest;
        this.recoil_amt = src.recoil_amt;
        this.recoil_speed = src.recoil_speed;
        this.recoil_ndx = src.recoil_ndx;
        this.recoil_horizoff = src.recoil_horizoff;
        this.oldposx = src.oldposx;
        this.oldposy = src.oldposy;
        this.oldposz = src.oldposz;
        this.RevolveX = src.RevolveX;
        this.RevolveY = src.RevolveY;
        this.RevolveDeltaAng = src.RevolveDeltaAng;
        this.RevolveAng = src.RevolveAng;
        this.PlayerSprite = src.PlayerSprite;
        this.PlayerUnderSprite = src.PlayerUnderSprite;
        this.pnum = src.pnum;
        this.LadderSector = src.LadderSector;
        this.LadderAngle = src.LadderAngle;
        this.lx = src.lx;
        this.ly = src.ly;
        this.JumpDuration = src.JumpDuration;
        this.WadeDepth = src.WadeDepth;
        this.bob_amt = src.bob_amt;
        this.bob_ndx = src.bob_ndx;
        this.bcnt = src.bcnt;
        this.bob_z = src.bob_z;
        this.input.Copy(src.input);
        this.playerreadyflag = src.playerreadyflag;
        this.DoPlayerAction = src.DoPlayerAction;
        this.Flags = src.Flags;
        this.Flags2 = src.Flags2;
        this.KeyPressFlags = src.KeyPressFlags;
        this.sop_control = src.sop_control;
        this.sop_riding = src.sop_riding;
        System.arraycopy(src.HasKey, 0, HasKey, 0, NUM_KEYS);
        this.SwordAng = src.SwordAng;
        this.WpnGotOnceFlags = src.WpnGotOnceFlags;
        this.WpnFlags = src.WpnFlags;
        System.arraycopy(src.WpnAmmo, 0, WpnAmmo, 0, MAX_WEAPONS);

        this.PanelSpriteList = src.PanelSpriteList;
//		this.PanelSpriteList.copy(src.PanelSpriteList);
        this.CurWpn = src.CurWpn;
//		this.CurWpn.copy(src.CurWpn);
        this.Wpn = src.Wpn;
        this.Chops = src.Chops;
//		this.Chops.copy(src.Chops);
        this.WpnRocketType = src.WpnRocketType;
        this.WpnRocketHeat = src.WpnRocketHeat;
        this.WpnRocketNuke = src.WpnRocketNuke;
        this.WpnFlameType = src.WpnFlameType;
        this.WpnFirstType = src.WpnFirstType;
        this.WeaponType = src.WeaponType;
        this.FirePause = src.FirePause;
        this.InventoryNum = src.InventoryNum;
        this.InventoryBarTics = src.InventoryBarTics;

        this.InventorySprite = src.InventorySprite;
        this.InventorySelectionBox = src.InventorySelectionBox;

        System.arraycopy(src.InventoryTics, 0, InventoryTics, 0, MAX_INVENTORY);
        System.arraycopy(src.InventoryPercent, 0, InventoryPercent, 0, MAX_INVENTORY);
        System.arraycopy(src.InventoryAmount, 0, InventoryAmount, 0, MAX_INVENTORY);
        System.arraycopy(src.InventoryActive, 0, InventoryActive, 0, MAX_INVENTORY);

        this.DiveTics = src.DiveTics;
        this.DiveDamageTics = src.DiveDamageTics;
        this.DeathType = src.DeathType;
        this.Kills = src.Kills;
        this.Killer = src.Killer;
        System.arraycopy(src.KilledPlayer, 0, KilledPlayer, 0, MAX_SW_PLAYERS);

        this.SecretsFound = src.SecretsFound;
        this.Armor = src.Armor;
        this.MaxHealth = src.MaxHealth;
        this.UziShellLeftAlt = src.UziShellLeftAlt;
        this.UziShellRightAlt = src.UziShellRightAlt;
        this.TeamColor = src.TeamColor;
        this.FadeTics = src.FadeTics;
        this.FadeAmt = src.FadeAmt;
        this.NightVision = src.NightVision;
        this.StartColor = src.StartColor;

//        System.arraycopy(src.temp_pal, 0, temp_pal, 0, 768);
        this.fta = src.fta;
        this.ftq = src.ftq;
        this.NumFootPrints = src.NumFootPrints;

        this.PlayerTalking = src.PlayerTalking;
        this.TalkVocnum = src.TalkVocnum;
        this.nukevochandle = src.nukevochandle;
        this.TalkVocHandle = src.TalkVocHandle;

        this.WpnUziType = src.WpnUziType;
        this.WpnShotgunType = src.WpnShotgunType;
        this.WpnShotgunAuto = src.WpnShotgunAuto;
        this.WpnShotgunLastShell = src.WpnShotgunLastShell;
        this.WpnRailType = src.WpnRailType;
        this.Bloody = src.Bloody;
        this.InitingNuke = src.InitingNuke;
        this.TestNukeInit = src.TestNukeInit;
        this.NukeInitialized = src.NukeInitialized;
        this.FistAng = src.FistAng;
        this.WpnKungFuMove = src.WpnKungFuMove;
        this.BunnyMode = src.BunnyMode;
        this.HitBy = src.HitBy;
        this.Reverb = src.Reverb;
        this.Heads = src.Heads;
        this.last_used_weapon = src.last_used_weapon;
        this.lookang = src.lookang;
    }
}
