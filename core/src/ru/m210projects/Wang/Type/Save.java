package ru.m210projects.Wang.Type;

import static ru.m210projects.Wang.Rooms.ZMAX;

public class Save {
    public final int[] zval = new int[ZMAX];
    public final short[] sectnum = new short[ZMAX];
    public final short[] pic = new short[ZMAX];
    public short zcount;
    public final short[] slope = new short[ZMAX];
}
