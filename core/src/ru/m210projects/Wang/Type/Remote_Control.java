package ru.m210projects.Wang.Type;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

public class Remote_Control {

    public int cursectnum, lastcursectnum, pang;
    public int xvect, yvect, oxvect, oyvect, slide_xvect, slide_yvect;
    public int posx, posy, posz;
    public int oposx, oposy, oposz;

    public void copy(Remote_Control src) {
        this.cursectnum = src.cursectnum;
        this.lastcursectnum = src.lastcursectnum;
        this.pang = src.pang;

        this.xvect = src.xvect;
        this.yvect = src.yvect;
        this.oxvect = src.oxvect;
        this.oyvect = src.oyvect;
        this.slide_xvect = src.slide_xvect;
        this.slide_yvect = src.slide_yvect;

        this.posx = src.posx;
        this.posy = src.posy;
        this.posz = src.posz;

        this.oposx = src.oposx;
        this.oposy = src.oposy;
        this.oposz = src.oposz;
    }

    public Remote_Control readObject(InputStream is) throws IOException {
        cursectnum = StreamUtils.readShort(is);
        lastcursectnum = StreamUtils.readShort(is);
        pang = StreamUtils.readShort(is);

        xvect = StreamUtils.readInt(is);
        yvect = StreamUtils.readInt(is);
        oxvect = StreamUtils.readInt(is);
        oyvect = StreamUtils.readInt(is);
        slide_xvect = StreamUtils.readInt(is);
        slide_yvect = StreamUtils.readInt(is);

        posx = StreamUtils.readInt(is);
        posy = StreamUtils.readInt(is);
        posz = StreamUtils.readInt(is);

        oposx = StreamUtils.readInt(is);
        oposy = StreamUtils.readInt(is);
        oposz = StreamUtils.readInt(is);

        return this;
    }

    public void writeObject(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, cursectnum);
        StreamUtils.writeShort(os, lastcursectnum);
        StreamUtils.writeShort(os, pang);

        StreamUtils.writeInt(os, xvect);
        StreamUtils.writeInt(os, yvect);
        StreamUtils.writeInt(os, oxvect);
        StreamUtils.writeInt(os, oyvect);
        StreamUtils.writeInt(os, slide_xvect);
        StreamUtils.writeInt(os, slide_yvect);

        StreamUtils.writeInt(os, posx);
        StreamUtils.writeInt(os, posy);
        StreamUtils.writeInt(os, posz);

        StreamUtils.writeInt(os, oposx);
        StreamUtils.writeInt(os, oposy);
        StreamUtils.writeInt(os, oposz);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Remote_Control that = (Remote_Control) o;
        return cursectnum == that.cursectnum && lastcursectnum == that.lastcursectnum && pang == that.pang && xvect == that.xvect && yvect == that.yvect && oxvect == that.oxvect && oyvect == that.oyvect && slide_xvect == that.slide_xvect && slide_yvect == that.slide_yvect && posx == that.posx && posy == that.posy && posz == that.posz && oposx == that.oposx && oposy == that.oposy && oposz == that.oposz;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cursectnum, lastcursectnum, pang, xvect, yvect, oxvect, oyvect, slide_xvect, slide_yvect, posx, posy, posz, oposx, oposy, oposz);
    }
}
