package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Wang.Factory.WangMenuHandler;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Type.GameInfo;

import java.nio.file.Path;

import static ru.m210projects.Wang.Factory.WangMenuHandler.DIFFICULTY;
import static ru.m210projects.Wang.Factory.WangMenuHandler.USERCONTENT;
import static ru.m210projects.Wang.Game.defGame;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Type.ResourceHandler.levelGetEpisode;

public class MenuNewGame extends BuildMenu {

    private final MenuProc newEpProc = (handler, pItem) -> {
        final WangMenuHandler menu = (WangMenuHandler) handler;

        WangEpisodeButton but = (WangEpisodeButton) pItem;
        MenuDifficulty next = (MenuDifficulty) menu.mMenus[DIFFICULTY];
        next.setEpisode(but.game, but.specialOpt);
        menu.mOpen(next, but.nItem);
    };

    public MenuNewGame(final Main app) {
        super(app.pMenu);
        int posy = 45;
        final WangMenuHandler menu = app.menu;

        final MenuUserContent usercont = (MenuUserContent) (menu.mMenus[USERCONTENT] = new MenuUserContent(app));

        GameInfo wt = searchAddon(FileUtils.getPath("wt.grp"), FileUtils.getPath("addons", "wt.grp"));
        GameInfo td = searchAddon(FileUtils.getPath("td.grp"), FileUtils.getPath("addons", "td.grp"), FileUtils.getPath("dragon"));

        addItem(new WangTitle("Episode"), false);
        addItem(new WangEpisodeButton(app, 35, posy, defGame, newEpProc, 0), true);
        addItem(new WangEpisodeButton(app, 35, posy += 32, defGame, newEpProc, 1), false);
        if (wt != null || td != null) {
            addItem(new MenuButton("Official addons", app.getFont(2), 35, posy += 32, 320, 0, 0,
                    new AddonMenu(app, td, wt), -1, null, -1) {
            }, false);
        } else {
            addItem(new MenuButton("Official addons", app.getFont(2), 35, posy += 32, 320, 0, 0, null, -1, null, 0) {
                @Override
                public void draw(MenuHandler handler) {
                    this.mCheckEnableItem(false);
                    super.draw(handler);
                }
            }, false);
        }

        addItem(new MenuButton("User Content", app.getFont(2), 35, posy + 20, 320, 0, 0, null, -1,
                (handler, pItem) -> {
                    if (usercont.showmain) {
                        usercont.setShowMain(false);
                    }
                    handler.mOpen(usercont, -1);
                }, 0), false);
    }

    private GameInfo searchAddon(Path... paths) {
        Directory dir = game.cache.getGameDirectory();
        for (Path path : paths) {
            FileEntry entry = dir.getEntry(path);
            GameInfo addon = levelGetEpisode(entry);
            if (addon != null) {
                return addon;
            }
        }
        return null;
    }

    private class AddonMenu extends BuildMenu {
        public AddonMenu(final Main app, GameInfo... addons) {
            super(app.menu);
            addItem(new WangTitle("ADDON"), false);
            int pos = 15;
            int add = 0;
            for (GameInfo addon : addons) {
                if (addon != null) {
                    addItem(new WangEpisodeButton(app, 35, pos += 32, addon, newEpProc, 1), add == 0);
                    add++;
                }
            }
        }
    }
}
