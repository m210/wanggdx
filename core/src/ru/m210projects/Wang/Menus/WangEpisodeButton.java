package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.MenuItems.MenuButton;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuProc;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Type.GameInfo;

public class WangEpisodeButton extends MenuButton {

    private final Main app;
    private final String description;
    public final GameInfo game;

    public WangEpisodeButton(Main app, int x, int y, GameInfo info, MenuProc newEpProc, int epnum) {
        super(info.episode[epnum].Title, app.getFont(2), x, y, 320, 0, 0, null, -1, newEpProc, epnum);

        this.app = app;
        this.description = info.episode[epnum].Description;
        game = info;
    }

    @Override
    public void draw(MenuHandler handler) {
        super.draw(handler);

        Renderer renderer = handler.getRenderer();
        app.getFont(1).drawTextScaled(renderer, x, y + 17, description, 1.0f, 0, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
    }
}
