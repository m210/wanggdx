package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuLoadSave;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Wang.Screens.PrecacheScreen;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Build.Gameutils.coordsConvertXScaled;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Wang.Enemies.Ninja.PlayerDeathReset;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.getUser;
import static ru.m210projects.Wang.LoadSave.*;
import static ru.m210projects.Wang.Main.gLoadingScreen;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Names.LOADSCREEN;

public class MenuLoad extends MenuLoadSave {

    public MenuLoad(final BuildGame app) {
        super(app, app.getFont(0), 75, 50, 185, 240, 12, 16, 19, LOADSCREEN, (handler, pItem) -> {
            final MenuSlotList item = (MenuSlotList) pItem;
            if (canLoad(item.getFileEntry()) && (!app.isCurrentScreen(gLoadingScreen)
                    && !(app.getScreen() instanceof PrecacheScreen))) {
                app.changeScreen(gLoadingScreen);
                gLoadingScreen.init(() -> {
                    if (!loadgame(item.getFileEntry())) {
                        game.show();
                    } else {
                        if (item.getFileEntry().getName().equalsIgnoreCase("autosave.sav") && isDemoRecording) {
                            PlayerStr pp = Player[myconnectindex];
                            USER u = getUser(pp.PlayerSprite);
                            int Flags = pp.Flags;
                            int WpnFlags = pp.WpnFlags;
                            int Armor = pp.Armor;
                            int Health = 100;
                            int MaxHealth = pp.MaxHealth;
                            if (u != null) {
                                Health = u.Health;
                            }
                            short[] WpnAmmo = pp.WpnAmmo.clone();
                            short[] InventoryPercent = pp.InventoryPercent.clone();
                            short[] InventoryAmount = pp.InventoryAmount.clone();
                            int InventoryNum = pp.InventoryNum;
                            int WpnRocketHeat = pp.WpnRocketHeat;
                            int WpnRocketNuke = pp.WpnRocketNuke;

                            PlayerDeathReset(pp);

                            pp.Flags = Flags;
                            pp.WpnFlags = WpnFlags;
                            pp.Armor = Armor;
                            pp.WpnAmmo = WpnAmmo;
                            pp.InventoryPercent = InventoryPercent;
                            pp.InventoryAmount = InventoryAmount;

                            pp.MaxHealth = MaxHealth;
                            if (u != null) {
                                u.Health = Health;
                            }
                            pp.InventoryNum = InventoryNum;
                            pp.WpnRocketHeat = (byte) WpnRocketHeat;
                            pp.WpnRocketNuke = (byte) WpnRocketNuke;

                            ExitLevel = true;
                        }
                    }
                });
            }
        }, false);

        list.pal = 16;
        list.helpPal = 16;
        list.questionFont = app.getFont(1);
        list.nListOffset = 15;
        list.backgroundPal = 4;
    }

    @Override
    public boolean loadData(FileEntry filename) {
        return lsReadLoadData(filename) != -1;
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        return new WangTitle(text);
    }

    @Override
    public MenuPicnum getPicnum(Engine draw, int x, int y) {
        return new MenuPicnum(draw, x - 70, y - 3, LOADSCREEN, LOADSCREEN, 0) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = game.getRenderer();
                int ydim = renderer.getHeight();

                if (nTile != defTile) {
                    renderer.rotatesprite((x + 3) << 16, y << 16, 0x12400, 0, nTile, 0, 0, 10 | 16);
                } else {
                    renderer.rotatesprite((x + 94) << 16, y + 57 << 16, 0x9200, 0, nTile, 0, 0, 10, coordsConvertXScaled(x, ConvertType.Normal), 0, coordsConvertXScaled(x + 186, ConvertType.Normal), ydim - 1);
                }
            }
        };
    }

    @Override
    public MenuText getInfo(BuildGame app, int x, int y) {
        return new MenuText(lsInf.info, app.getFont(0), x - 58, y + 100, 0) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = handler.getRenderer();
                int ty = y;
                if (lsInf.addonfile != null && !lsInf.addonfile.isEmpty()) {
                    font.drawTextScaled(renderer, x, ty, lsInf.addonfile, 1.0f, -128, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                    ty -= 10;
                }
                if (lsInf.date != null && !lsInf.date.isEmpty()) {
                    font.drawTextScaled(renderer, x, ty, lsInf.date, 1.0f, -128, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                    ty -= 10;
                }
                if (lsInf.info != null) {
                    font.drawTextScaled(renderer, x, ty, lsInf.info, 1.0f, -128, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                }
            }
        };
    }

}
