package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuLoadSave;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;

import static ru.m210projects.Build.Gameutils.coordsConvertXScaled;
import static ru.m210projects.Wang.Gameutils.ROTATE_SPRITE_SCREEN_CLIP;
import static ru.m210projects.Wang.LoadSave.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.COINCURSOR;
import static ru.m210projects.Wang.Names.LOADSCREEN;

public class MenuSave extends MenuLoadSave {

    public MenuSave(final BuildGame app) {
        super(app, app.getFont(0), 75, 50, 185, 240, 12, 16, 19, LOADSCREEN, (handler, pItem) -> {
            MenuSlotList item = (MenuSlotList) pItem;
            String filename;
            Directory userDir = app.getUserDirectory();
            if (item.l_nFocus == 0) {
                int num = 0;
                do {
                    if (num > 9999) {
                        return;
                    }
                    filename = "game" + makeNum(num) + ".sav";
                    if (!userDir.getEntry(filename).exists()) {
                        break;
                    }

                    num++;
                } while (true);
            } else {
                filename = item.getFileEntry().getName();
            }
            if (item.typed == null || item.typed.isEmpty()) {
                item.typed = filename;
            }

            savegame(userDir, item.typed, filename);
            handler.mClose();
        }, true);

        list.owncursor = true;
        list.pal = 16;
        list.helpPal = 16;
        list.questionFont = app.getFont(1);
        list.nListOffset = 15;
        list.backgroundPal = 4;
    }

    @Override
    protected void drawOwnCursor(int x, int y) {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(x + 4 << 16, y + 1 << 16, 20000, 0, COINCURSOR + ((engine.getTotalClock() >> 3) % 7), 0,
                0, ROTATE_SPRITE_SCREEN_CLIP);
    }

    @Override
    public boolean loadData(FileEntry filename) {
        return lsReadLoadData(filename) != -1;
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        return new WangTitle(text);
    }

    @Override
    public MenuPicnum getPicnum(Engine draw, int x, int y) {
        return new MenuPicnum(draw, x - 70, y - 3, LOADSCREEN, LOADSCREEN, 0) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = game.getRenderer();
                int ydim = renderer.getHeight();

                if (nTile != defTile) {
                    renderer.rotatesprite(x + 3 << 16, y << 16, 0x12400, 0, nTile, 0, 0, 10 | 16);
                } else {
                    renderer.rotatesprite((x + 94) << 16, y + 57 << 16, 0x9200, 0, nTile, 0, 0, 10, coordsConvertXScaled(x, ConvertType.Normal), 0, coordsConvertXScaled(x + 186, ConvertType.Normal), ydim - 1);
                }
            }

            @Override
            public void close() {
                gGameScreen.captBuffer = null;
            }
        };
    }

    @Override
    public MenuText getInfo(BuildGame app, int x, int y) {
        return new MenuText(lsInf.info, app.getFont(0), x - 58, y + 100, 0) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = handler.getRenderer();
                int ty = y;
                if (lsInf.addonfile != null && !lsInf.addonfile.isEmpty()) {
                    font.drawTextScaled(renderer, x, ty, lsInf.addonfile, 1.0f, -128, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                    ty -= 10;
                }
                if (lsInf.date != null && !lsInf.date.isEmpty()) {
                    font.drawTextScaled(renderer, x, ty, lsInf.date, 1.0f, -128, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                    ty -= 10;
                }
                if (lsInf.info != null) {
                    font.drawTextScaled(renderer, x, ty, lsInf.info, 1.0f, -128, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                }
            }
        };
    }

}
