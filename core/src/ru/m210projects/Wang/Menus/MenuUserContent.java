package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuFileBrowser;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Menus.Network.MenuNetwork;
import ru.m210projects.Wang.Type.EpisodeEntry;
import ru.m210projects.Wang.Type.GameInfo;
import ru.m210projects.Wang.Type.LevelInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static ru.m210projects.Wang.Digi.DIGI_SWORDSWOOSH;
import static ru.m210projects.Wang.Factory.WangMenuHandler.*;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Main.gDemoScreen;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Screens.DemoScreen.checkDemoEntry;
import static ru.m210projects.Wang.Sound.PlaySound;
import static ru.m210projects.Wang.Sound.v3df_dontpan;
import static ru.m210projects.Wang.Type.ResourceHandler.episodeManager;

public class MenuUserContent extends BuildMenu {

    private final Main app;
    private final MenuFileBrowser list;

    private final HashMap<String, LevelInfo> originalMaps = new HashMap<String, LevelInfo>() {
        private static final long serialVersionUID = 1L;

        {
            put("$bullet.map", pSharewareEp.gMapInfo[1]);
            put("$dozer.map", pSharewareEp.gMapInfo[2]);
            put("$shrine.map", pSharewareEp.gMapInfo[3]);
            put("$woods.map", pSharewareEp.gMapInfo[4]);

            put("$whirl.map", pOriginalEp.gMapInfo[0]);
            put("$tank.map", pOriginalEp.gMapInfo[1]);
            put("$boat.map", pOriginalEp.gMapInfo[2]);
            put("$garden.map", pOriginalEp.gMapInfo[3]);
            put("$outpost.map", pOriginalEp.gMapInfo[4]);
            put("$hidtemp.map", pOriginalEp.gMapInfo[5]);
            put("$plax1.map", pOriginalEp.gMapInfo[6]);
            put("$bath.map", pOriginalEp.gMapInfo[7]);
            put("$airport.map", pOriginalEp.gMapInfo[8]);
            put("$refiner.map", pOriginalEp.gMapInfo[9]);
            put("$newmine.map", pOriginalEp.gMapInfo[10]);
            put("$subbase.map", pOriginalEp.gMapInfo[11]);
            put("$rock.map", pOriginalEp.gMapInfo[12]);
            put("$yamato.map", pOriginalEp.gMapInfo[13]);
            put("$seabase.map", pOriginalEp.gMapInfo[14]);
            put("$volcano.map", pOriginalEp.gMapInfo[15]);
            // Secret levels:
            put("$shore.map", pOriginalEp.gMapInfo[16]);
            put("$auto.map", pOriginalEp.gMapInfo[17]);
        }
    };
    public boolean showmain;

    public MenuUserContent(final Main app) {
        super(app.pMenu);
        this.app = app;
        addItem(new WangTitle("User content"), false);

        int width = 240;
        list = new MenuFileBrowser(app, app.getFont(0), app.getFont(1), app.getFont(0), 40, 40, width, 1, 14, 2324) {
            @Override
            protected void drawHeader(Renderer renderer, int x1, int x2, int y) {
                /* directories */
                app.getFont(1).drawTextScaled(renderer, x1, y, dirs, 1.0f, -32, topPal, TextAlign.Left, Transparent.None, ConvertType.Normal, fontShadow);
                /* files */
                app.getFont(1).drawTextScaled(renderer, x2, y, ffs, 1.0f, -32, topPal, TextAlign.Left, Transparent.None, ConvertType.Normal, fontShadow);
            }

            @Override
            public void init() {
                registerExtension("map", 0, 0);
                registerExtension("grp", 19, 1);
                registerExtension("zip", 19, 1);
                registerExtension("pk3", 19, 1);
                registerExtension("dmo", 1, -1);
                registerClass(EpisodeEntry.Addon.class, 19, 2);
            }

            @Override
            public void handleDirectory(Directory dir) {
                if (app.pMenu.gShowMenu) {
                    PlaySound(DIGI_SWORDSWOOSH, null, v3df_dontpan);
                }

                GameInfo addon = episodeManager.getEpisode(dir.getDirectoryEntry());
                if (addon != null) {
                    addFile((FileEntry) addon.getEpisodeEntry());
                }
            }

            @Override
            public void handleFile(FileEntry fil) {

                switch (fil.getExtension()) {
                    case "MAP": {
                        Directory dir = fil.getParent();
                        GameInfo addon = episodeManager.getEpisode(dir.getDirectoryEntry());
                        if (addon != null) {
                            LevelInfo map = originalMaps.get(fil.getName().toLowerCase(Locale.ROOT));
                            if (map != null) { // This file has added to addon
                                return;
                            }
                        }
                        addFile(fil);
                        break;
                    }
                    case "DMO":
                        if (showmain) {
                            break; // multiplayer menu
                        }

                        if (checkDemoEntry(fil)) {
                            addFile(fil);
                        }
                        break;
                    case "GRP":
                    case "PK3":
                    case "ZIP": {
                        GameInfo addon = episodeManager.getEpisode(fil);
                        if (addon != null) {
                            if (showmain || !addon.equals(defGame)) {
                                addFile(fil);
                            }
                        }
                        break;
                    }
                }
            }

            @Override
            public void invoke(FileEntry fil) {
                switch (fil.getExtension()) {
                    case "MAP":
                        launchMap(fil);
                        break;
                    case "GRP":
                    case "ZIP":
                    case "PK3":
                        launchEpisode(episodeManager.getEpisode(fil));
                        break;
                    case "DMO":
//                        Entry episode = null;
                        Directory parent = fil.getParent();

                        List<Entry> demoList = gDemoScreen.checkDemoEntry(parent);
                        gDemoScreen.nDemonum = demoList.indexOf(fil);
                        gDemoScreen.showDemo(fil, null);
                        app.pMenu.mClose();
                        break;
                    default:
                        if (fil instanceof EpisodeEntry) {
                            launchEpisode(episodeManager.getEpisode(fil));
                        }
                        break;
                }
            }
        };

        list.topPal = 20;
        list.pathPal = 20;
        list.listPal = 4;
        addItem(list, true);
    }

    private void launchMap(FileEntry file) {
        if (file == null) {
            return;
        }

        if (mFromNetworkMenu()) {
            MenuNetwork network = (MenuNetwork) app.menu.mMenus[NETWORKGAME];
            network.setMap(file);
            app.menu.mMenuBack();
            return;
        }

        MenuDifficulty next = (MenuDifficulty) app.menu.mMenus[DIFFICULTY];
        next.setMap(file);
        app.menu.mOpen(next, -1);
    }

    private void launchEpisode(GameInfo game) {
        if (game == null) {
            return;
        }

        if (mFromNetworkMenu()) {
            MenuNetwork network = (MenuNetwork) app.menu.mMenus[NETWORKGAME];
            network.setEpisode(game);
            app.menu.mMenuBack();
            return;
        }

        MenuNewAddon next = (MenuNewAddon) app.menu.mMenus[NEWADDON];
        next.setEpisode(game);
        app.menu.mOpen(next, -1);
    }

    public void setShowMain(boolean show) {
        this.showmain = show;
        if (game.getCache().isGameDirectory(list.getDirectory())) {
            list.refreshList();
        }
    }

    public boolean mFromNetworkMenu() {
        return app.menu.getLastMenu() == app.menu.mMenus[NETWORKGAME];
    }
}
