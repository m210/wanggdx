package ru.m210projects.Wang.Menus;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Type.GameInfo;

import static ru.m210projects.Wang.Main.gGameScreen;

public class MenuDifficulty extends BuildMenu {

    private GameInfo game;
    private int currentEp;
    private FileEntry map;

    public MenuDifficulty(final Main app) {
        super(app.pMenu);
        MenuProc newGameProc = (handler, pItem) -> {
            MenuButton button = (MenuButton) pItem;
            final int skill = button.specialOpt;
            Gdx.app.postRunnable(() -> gGameScreen.newgame(false, map != null ? map : game, currentEp, 0, skill));
        };

        int posy = 25;
        addItem(new WangTitle("Skill"), false);

        addItem(new MenuButton("Tiny grasshopper", app.getFont(2), 35, posy += 16, 320, 0, 0, null, -1, newGameProc, 0), false);
        addItem(new MenuButton("I Have No Fear", app.getFont(2), 35, posy += 16, 320, 0, 0, null, -1, newGameProc, 1), false);
        addItem(new MenuButton("Who Wants Wang", app.getFont(2), 35, posy += 16, 320, 0, 0, null, -1, newGameProc, 2), true);
        addItem(new MenuButton("No Pain, No Gain", app.getFont(2), 35, posy + 16, 320, 0, 0, null, -1, newGameProc, 3), false);
    }

    public void setEpisode(GameInfo game, int episodeNum) {
        this.map = null;
        this.game = game;
        this.currentEp = episodeNum;
    }

    public void setMap(FileEntry map) {
        this.map = map;
        this.game = null;
        this.currentEp = -1;
    }

}
