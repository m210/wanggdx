package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuButton;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuPicnum;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Wang.Factory.WangMenuHandler;
import ru.m210projects.Wang.Main;

import static ru.m210projects.Build.Strhandler.toCharArray;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Wang.Factory.WangMenuHandler.*;
import static ru.m210projects.Wang.Main.gDemoScreen;
import static ru.m210projects.Wang.Main.game;

public class MenuMain extends BuildMenu {

    public MenuMain(final Main app) {
        super(app.pMenu);
        final WangMenuHandler menu = app.menu;

        MenuPicnum bLogo = new MenuPicnum(app.pEngine, 160, 15, 2366, 2366, 65536) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = game.getRenderer();
                renderer.rotatesprite(x << 16, y << 16, 65536, 0, nTile, 0, 0, 10);
            }
        };

        int posy = 30;

        MenuButton NewGame = new MenuButton("New Game", app.getFont(2), 55, posy += 16, 320, 0, 0, menu.mMenus[NEWGAME], -1, null, 0);
        MenuButton Multiplayer = new MenuButton("Multiplayer", app.getFont(2), 55, posy += 16, 320, 0, 0, null, -1, null, 0) {
            @Override
            public void draw(MenuHandler handler) {
                if (!app.isCurrentScreen(gDemoScreen) && (numplayers > 1 || game.net.FakeMultiplayer)) {
                    nextMenu = menu.mMenus[NETWORKGAME];
                } else {
                    nextMenu = menu.mMenus[MULTIPLAYER];
                }
                super.draw(handler);
            }
        };
        MenuButton Load = new MenuButton("Load Game", app.getFont(2), 55, posy += 16, 320, 0, 0, menu.mMenus[LOADGAME], -1, null, 0) {
            @Override
            public void draw(MenuHandler handler) {
                mCheckEnableItem(app.isCurrentScreen(gDemoScreen) || numplayers < 2 && !game.net.FakeMultiplayer);
                super.draw(handler);
            }
        };
        MenuButton Options = new MenuButton("Options", app.getFont(2), 55, posy += 16, 320, 0, 0, menu.mMenus[OPTIONS], -1, null, 0);
        MenuButton Credits = new MenuButton("Cool Stuff", app.getFont(2), 55, posy += 16, 320, 0, 0, new MenuCredits(app), -1, null, 0);
        MenuButton Quit = new MenuButton("Quit", app.getFont(2), 55, posy + 16, 320, 0, 0, menu.mMenus[QUIT], -1, null, 0) {
            @Override
            public void draw(MenuHandler handler) {
                if (!app.isCurrentScreen(gDemoScreen) && (numplayers > 1 || game.net.FakeMultiplayer)) {
                    text = toCharArray("Disconnect");
                    nextMenu = menu.mMenus[QUITTITLE];
                } else {
                    text = toCharArray("Quit");
                    nextMenu = menu.mMenus[QUIT];
                }
                super.draw(handler);
            }
        };

        addItem(bLogo, false);
        addItem(NewGame, true);
        addItem(Multiplayer, false);
        addItem(Load, false);
        addItem(Options, false);
        addItem(Credits, false);
        addItem(Quit, false);
    }
}
