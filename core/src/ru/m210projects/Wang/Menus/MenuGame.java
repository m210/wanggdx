package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Wang.Factory.WangMenuHandler;
import ru.m210projects.Wang.Main;

import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Wang.Factory.WangMenuHandler.*;
import static ru.m210projects.Wang.Main.*;

public class MenuGame extends BuildMenu {

    public MenuGame(final Main app) {
        super(app.pMenu);
        final WangMenuHandler menu = app.menu;

        MenuPicnum bLogo = new MenuPicnum(app.pEngine, 160, 15, 2366, 2366, 65536) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = game.getRenderer();
                renderer.rotatesprite(x << 16, y << 16, 65536, 0, nTile, 0, 0, 10);
            }
        };

        int posy = 30;

        MenuButton NewGame = new MenuButton("New Game", app.getFont(2), 55, posy += 16, 320, 0, 0, menu.mMenus[NEWGAME], -1, null, 0) {
            @Override
            public void open() {
                nextMenu = menu.mMenus[NEWGAME];
                if (numplayers > 1 || game.net.FakeMultiplayer) {
                    nextMenu = menu.mMenus[NETWORKGAME];
                }
            }
        };
        MenuButton Load = new MenuButton("Load Game", app.getFont(2), 55, posy += 16, 320, 0, 0, menu.mMenus[LOADGAME], -1, null, 0) {
            @Override
            public void draw(MenuHandler handler) {
                mCheckEnableItem(app.isCurrentScreen(gDemoScreen) || numplayers < 2 && !game.net.FakeMultiplayer);
                super.draw(handler);
            }
        };

        MenuProc mScreenCapture = (handler, pItem) -> gGameScreen.capture(160, 100);
        MenuButton Save = new MenuButton("Save Game", app.getFont(2), 55, posy += 16, 320, 0, 0, menu.mMenus[SAVEGAME], -1, mScreenCapture, 0) {
            @Override
            public void draw(MenuHandler handler) {
                mCheckEnableItem(app.isCurrentScreen(gDemoScreen) || numplayers < 2 && !game.net.FakeMultiplayer);
                super.draw(handler);
            }
        };

        MenuButton Options = new MenuButton("Options", app.getFont(2), 55, posy += 16, 320, 0, 0, menu.mMenus[OPTIONS], -1, null, 0);

        MenuButton End = new MenuButton("End game", app.getFont(2), 55, posy += 16, 320, 0, 0, menu.mMenus[QUITTITLE], -1, null, 0);

        MenuButton Quit = new MenuButton("Quit", app.getFont(2), 55, posy + 16, 320, 0, 0, menu.mMenus[QUIT], -1, null, 0);

        addItem(bLogo, false);
        addItem(NewGame, true);
        addItem(Load, false);
        addItem(Save, false);
        addItem(Options, false);
        addItem(End, false);
        addItem(Quit, false);
    }

}
