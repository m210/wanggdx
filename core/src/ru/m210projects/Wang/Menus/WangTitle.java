package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Main.game;

public class WangTitle extends MenuTitle {

    public WangTitle(Object text) {
        super(engine, text, game.getFont(2), 160, 10, 2427);
    }

    @Override
    public void draw(MenuHandler handler) {
        if (text != null) {
            Renderer renderer = game.getRenderer();
            renderer.rotatesprite(x << 16, y << 16, 65536, 0, nTile, 0, 0, 10);
            font.drawTextScaled(renderer, x, y - (renderer.getTile(2427).getHeight() - font.getSize()) / 2 - 1, text, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, fontShadow);
        }
    }

}
