package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Pattern.ScreenAdapters.GameAdapter;
import ru.m210projects.Wang.Main;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.isDemoRecording;
import static ru.m210projects.Wang.Gameutils.PF_AUTO_AIM;
import static ru.m210projects.Wang.Gameutils.synctics;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Text.PutStringInfo;

public class MenuGameSetup extends BuildMenu {

    public MenuGameSetup(final Main app) {
        super(app.pMenu);
        WangTitle mTitle = new WangTitle("Game Setup");
        int pos = 40;

        MenuSwitch sAutoload = new MenuSwitch("Autoload folder", app.getFont(1), 35, pos += 10, 240, cfg.isAutoloadFolder(), (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.setAutoloadFolder(sw.value);
        }, "Enabled", "Disabled");

        WangSwitch sSlopeTilt = new WangSwitch("Screen tilting", app.getFont(1), 35, pos += 10, 240, cfg.SlopeTilting,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.SlopeTilting = sw.value;
                });

        WangSwitch sAutoAim = new WangSwitch("AutoAim", app.getFont(1), 35, pos += 10, 240, cfg.AutoAim, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.AutoAim = sw.value;

            if (game.nNetMode == NetMode.Single && game.getScreen() != gDemoScreen) {
                if (cfg.AutoAim) {
                    Player[myconnectindex].Flags |= (PF_AUTO_AIM);
                } else {
                    Player[myconnectindex].Flags &= ~(PF_AUTO_AIM);
                }
            }
        }) {
            @Override
            public void draw(MenuHandler handler) {
                super.draw(handler);
                value = cfg.AutoAim;
            }
        };

        WangSwitch sWeaponAutoSwitch = new WangSwitch("Weapon Auto Switch", app.getFont(1), 35, pos += 10, 240, cfg.WeaponAutoSwitch, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.WeaponAutoSwitch = sw.value;
        }) {
            @Override
            public void draw(MenuHandler handler) {
                super.draw(handler);
                value = cfg.WeaponAutoSwitch;
            }
        };

        MenuSwitch sUseDarts = new WangSwitch("Use Darts", app.getFont(1), 35, pos += 10, 240,
                cfg.UseDarts, (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.UseDarts = sw.value;
                });

        MenuSwitch sDisableHornets = new WangSwitch("Disable Hornets", app.getFont(1), 35, pos += 10, 240,
                cfg.DisableHornets, (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.DisableHornets = sw.value;
                    if (game.getScreen() instanceof GameAdapter) {
                        PutStringInfo(Player[myconnectindex], "You have to restart the level");
                    }
                });

        MenuConteiner mPlayingDemo = new MenuConteiner("Demos playback", app.getFont(1), 35, pos += 10, 240, null, 0,
                (handler, pItem) -> {
                    MenuConteiner item = (MenuConteiner) pItem;
                    cfg.gDemoSeq = item.num;
                }) {
            @Override
            public void open() {
                if (this.list == null) {
                    this.list = new char[3][];
                    this.list[0] = "Off".toCharArray();
                    this.list[1] = "In order".toCharArray();
                    this.list[2] = "Randomly".toCharArray();
                }
                num = cfg.gDemoSeq;
            }
        };

        MenuSwitch sRecord = new WangSwitch("Record demo", app.getFont(1), 35, pos += 10, 240, isDemoRecording,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    isDemoRecording = sw.value;
                }) {

            @Override
            public void open() {
                value = isDemoRecording;
                mCheckEnableItem(!app.isCurrentScreen(gGameScreen));
            }
        };

        MenuSwitch mTimer = new MenuSwitch("Game loop timer:", app.getFont(1), 35, pos + 10, 240, cfg.isLegacyTimer(),
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.setLegacyTimer(sw.value);
                    engine.inittimer(cfg.isLegacyTimer(), 120, synctics);
                }, "Legacy", "Gdx") {
        };

        addItem(mTitle, false);
        addItem(sAutoload, true);
        addItem(sSlopeTilt, false);
        addItem(sAutoAim, false);
        addItem(sWeaponAutoSwitch, false);
        addItem(sUseDarts, false);
        addItem(sDisableHornets, false);
		addItem(mPlayingDemo, false);
		addItem(sRecord, false);
        addItem(mTimer, false);
    }
}
