package ru.m210projects.Wang.Menus.Network;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class NetInfo implements Serializable<NetInfo> {

    public int nGameType;
    public int nEpisode;
    public int nLevel;
    public int nDifficulty;
    public int nMonsters;
    public int nFriendlyFire;

    public boolean SpawnMarkers = true;
    public boolean TeamPlay = false;
    public boolean NetNuke = true;
    public int TimeLimit = 0;
    public int KillLimit = 0;

    @Override
    public NetInfo readObject(InputStream is) throws IOException {
        nGameType = StreamUtils.readByte(is);
        nEpisode = StreamUtils.readUnsignedByte(is);
        nLevel = StreamUtils.readUnsignedByte(is);
        nDifficulty = StreamUtils.readByte(is);
        nMonsters = StreamUtils.readByte(is);
        nFriendlyFire = StreamUtils.readByte(is);

        SpawnMarkers = StreamUtils.readBoolean(is);
        TeamPlay = StreamUtils.readBoolean(is);
        NetNuke = StreamUtils.readBoolean(is);
        TimeLimit = StreamUtils.readUnsignedByte(is);
        KillLimit = StreamUtils.readUnsignedByte(is);

        return this;
    }

    @Override
    public NetInfo writeObject(OutputStream os) throws IOException {
        StreamUtils.writeByte(os, (byte) nGameType);
        StreamUtils.writeByte(os, (byte) nEpisode);
        StreamUtils.writeByte(os, (byte) nLevel);
        StreamUtils.writeByte(os, (byte) nDifficulty);
        StreamUtils.writeByte(os, (byte) nMonsters);
        StreamUtils.writeByte(os, (byte) nFriendlyFire);

        StreamUtils.writeBoolean(os, SpawnMarkers);
        StreamUtils.writeBoolean(os, TeamPlay);
        StreamUtils.writeBoolean(os, NetNuke);
        StreamUtils.writeByte(os, (byte) TimeLimit);
        StreamUtils.writeByte(os, (byte) KillLimit);

        return this;
    }
}
