package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Wang.Main;

public class MenuControls extends BuildMenu {

    public MenuControls(final Main app) {
        super(app.pMenu);
        addItem(new WangTitle("Controls Setup"), false);
        int pos = 39;

        MenuButton mMouseSet = new MenuButton("Mouse setup", app.getFont(1), 35, pos += 10, 320, 0, 0, new MenuMouse(app), -1, null, 0);
        MenuButton mJoySet = new MenuButton("Joystick setup", app.getFont(1), 35, pos += 10, 320, 0, 0, new MenuJoystick(app), -1, null, 0);
        MenuButton mKeySet = new MenuButton("Keyboard Setup", app.getFont(1), 35, pos += 10, 320, 0, 0, new MenuMenuKeyboard(app), -1, null, 0);
        MenuButton mKeyReset = new MenuButton("Reset to default", app.getFont(1), 35, pos += 15, 320, 0, 0, getResetDefaultMenu(app, app.getFont(1), 90, 16), -1, null, 0);
        MenuButton mKeyClassic = new MenuButton("Reset to classic", app.getFont(1), 35, pos + 10, 320, 0, 0, getResetClassicMenu(app, app.getFont(1), 90, 16), -1, null, 0);

        addItem(mMouseSet, true);
        addItem(mJoySet, false);
        addItem(mKeySet, false);
        addItem(mKeyReset, false);
        addItem(mKeyClassic, false);
    }

    private void mResetDefault(GameConfig cfg, MenuHandler menu) {
        cfg.resetInput(false);

        menu.mMenuBack();
    }

    private void mResetClassic(GameConfig cfg, MenuHandler menu) {
        cfg.resetInput(true);
        menu.mMenuBack();
    }

    protected BuildMenu getResetDefaultMenu(final BuildGame app, Font style, int posy, int pal) {
        BuildMenu menu = new BuildMenu(app.pMenu);

        MenuText QuitQuestion = new MenuText("Do you really want to reset keys?", style, 160, posy, 1);
        QuitQuestion.pal = pal;
        MenuVariants QuitVariants = new MenuVariants(app.pEngine, "[Y/N]", style, 160, posy + 2 * style.getSize()) {
            @Override
            public void positive(MenuHandler menu) {
                mResetDefault(app.pCfg, menu);
            }
        };
        QuitVariants.pal = pal;

        menu.addItem(QuitQuestion, false);
        menu.addItem(QuitVariants, true);

        return menu;
    }

    protected BuildMenu getResetClassicMenu(final BuildGame app, Font style, int posy, int pal) {
        BuildMenu menu = new BuildMenu(app.pMenu);

        MenuText QuitQuestion = new MenuText("Do you really want reset to classic keys?", style, 160, posy, 1);
        QuitQuestion.pal = pal;
        MenuVariants QuitVariants = new MenuVariants(app.pEngine, "[Y/N]", style, 160, posy + 2 * style.getSize()) {
            @Override
            public void positive(MenuHandler menu) {
                mResetClassic(app.pCfg, menu);
            }
        };
        QuitVariants.pal = pal;

        menu.addItem(QuitQuestion, false);
        menu.addItem(QuitVariants, true);

        return menu;
    }
}
