package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuProc;
import ru.m210projects.Build.Pattern.MenuItems.MenuSwitch;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.art.ArtEntry;

import static ru.m210projects.Wang.Main.game;

public class WangSwitch extends MenuSwitch {

    public WangSwitch(Object text, Font font, int x, int y, int width, boolean value, MenuProc callback) {
        super(text, font, x, y, width, value, callback, null, null);
    }

    @Override
    public void draw(MenuHandler handler) {
        int shade = handler.getShade(this);
        Renderer renderer = game.getRenderer();
        if (text != null) {
            font.drawTextScaled(renderer, x, y, text, 1.0f, shade, 16, TextAlign.Left, Transparent.None, ConvertType.Normal, fontShadow);
        }

        ArtEntry pic = renderer.getTile(2849);
        renderer.rotatesprite((x + width - 1 - pic.getWidth()) << 16, (y + (font.getSize() - pic.getHeight()) / 2) << 16, 65536, 0, 2849, value ? 0 : 20, 0, 10);
        handler.mPostDraw(this);
    }

}
