package ru.m210projects.Wang.Menus;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuText;
import ru.m210projects.Build.Pattern.MenuItems.MenuVariants;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Screens.DemoScreen;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Wang.Main.gDemoScreen;

public class MenuQuit extends BuildMenu {

    public MenuQuit(final Main game, final boolean toTitle) {
        super(game.pMenu);
        if (toTitle) {
            MenuText QuitQuestion = new MenuText("QUIT TO TITLE?", game.getFont(1), 160, 90, 1);
            QuitQuestion.pal = 4;
            addItem(QuitQuestion, false);
        }

        MenuVariants question = new MenuVariants(game.pEngine, toTitle ? "[Y/N]" : "PRESS [Y] TO QUIT, [N] TO FIGHT ON.",
                game.getFont(1), 160, 105) {
            @Override
            public void positive(MenuHandler menu) {
                if (!toTitle) {
                    if (numplayers > 1) {
                        Gdx.app.postRunnable(() -> game.pNet.NetDisconnect(myconnectindex));
                    }
                    game.gExit = true;
                } else {
                    if (!DemoScreen.isDemoPlaying() && (numplayers > 1 || game.net.FakeMultiplayer)) {
                        Gdx.app.postRunnable(() -> game.net.NetDisconnect(myconnectindex));
                    } else {
                        game.show();
                    }
                }
                menu.mClose();
            }
        };
        question.pal = 4;

        addItem(question, true);
    }
}
