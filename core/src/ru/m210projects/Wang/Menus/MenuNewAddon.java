package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Wang.Factory.WangMenuHandler;
import ru.m210projects.Wang.Main;
import ru.m210projects.Wang.Type.EpisodeInfo;
import ru.m210projects.Wang.Type.GameInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Wang.Factory.WangMenuHandler.DIFFICULTY;

public class MenuNewAddon extends BuildMenu {

    private static final int nMaxEpisodes = 2;
    private final List<char[]> mEpisodelist;
    private final List<char[]> mEpisodeDescr;
    private GameInfo game;
    private final MenuList mSlot;
    private final int[] episodeNum;

    public MenuNewAddon(final Main app) {
        super(app.pMenu);
        final WangMenuHandler menu = app.menu;
        addItem(new WangTitle("USER EPISODE"), false);

        mEpisodelist = new ArrayList<>();
        mEpisodeDescr = new ArrayList<>();
        episodeNum = new int[nMaxEpisodes];
        MenuProc newEpProc = (handler, pItem) -> {
            MenuList button = (MenuList) pItem;
            MenuDifficulty next = (MenuDifficulty) menu.mMenus[DIFFICULTY];
            next.setEpisode(game, episodeNum[button.l_nFocus]);
            menu.mOpen(next, -1);
        };

        mSlot = new MenuList(mEpisodelist, app.getFont(2), 35, 45, 320, 0, newEpProc, nMaxEpisodes) {
            @Override
            public int mFontOffset() {
                return font.getSize() + 17;
            }

            @Override
            public void draw(MenuHandler handler) {
                super.draw(handler);

                Renderer renderer = handler.getRenderer();
                if (!text.isEmpty()) {
                    int py = y;
                    for (int i = l_nMin; i >= 0 && i < l_nMin + rowCount && i < len; i++) {
                        app.getFont(1).drawTextScaled(renderer, x, py + 17, mEpisodeDescr.get(i), 1.0f, 0, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                        py += mFontOffset();
                    }
                }
            }
        };
        addItem(mSlot, true);
    }

    public void setEpisode(GameInfo game) {
        this.game = game;
        mEpisodelist.clear();
        mEpisodeDescr.clear();

        Arrays.fill(episodeNum, -1);

        for (int i = 0; i < nMaxEpisodes; i++) {
            EpisodeInfo episode = game.episode[i];
            if (episode != null && !episode.Title.isEmpty() && episode.nMaps != 0) {
                episodeNum[mEpisodelist.size()] = i;
                mEpisodelist.add(episode.Title.toCharArray());
                mEpisodeDescr.add(episode.Description.toCharArray());
            }
        }
        mSlot.len = mEpisodelist.size();
        mSlot.l_nFocus = mSlot.l_nMin = 0;

    }
}
