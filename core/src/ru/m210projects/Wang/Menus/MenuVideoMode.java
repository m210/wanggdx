package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuRendererSettings;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Wang.Factory.WangMenuHandler;
import ru.m210projects.Wang.Main;

import static ru.m210projects.Wang.Factory.WangMenuHandler.COLORCORR;
import static ru.m210projects.Wang.Names.TITLE_PIC;

public class MenuVideoMode extends ru.m210projects.Build.Pattern.CommonMenus.MenuVideoMode {

    public MenuVideoMode(BuildGame app) {
        super(app, 35, 40, 240, 10, app.getFont(1), 10, 240, TITLE_PIC);

        for (int i = 0; i < m_nItems; i++) {
            if (m_pItems[i] instanceof MenuSwitch) {
                final MenuSwitch oldSwitch = (MenuSwitch) m_pItems[i];
                m_pItems[i] = new WangSwitch(oldSwitch.text, oldSwitch.font, oldSwitch.x, oldSwitch.y, oldSwitch.width, oldSwitch.value, oldSwitch.callback) {
                    @Override
                    public void open() {
                        oldSwitch.open();
                    }
                };
                m_pItems[i].m_pMenu = this;
            }
        }

        mApplyChanges.font = app.getFont(1);
        mApplyChanges.x = 35;
        mApplyChanges.y -= 5;
        mApplyChanges.align = 0;
        mSlot.backgroundPal = 4;
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        return new WangTitle(text);
    }

    @Override
    public MenuRendererSettings getRenSettingsMenu(BuildGame app, int posx, int posy, int width,
                                                   int nHeight, Font style) {

        final WangMenuHandler menu = ((Main) app).menu;

        MenuRendererSettings rmenu = new MenuRendererSettings(app, posx, posy, width, nHeight, style) {
            @Override
            public MenuTitle getTitle(BuildGame app, String text) {
                return new WangTitle(text);
            }

            @Override
            protected MenuSwitch BuildSwitch(String text, SettingsProvider<Boolean> var) {
                return new WangSwitch(text, style, 0, 0, width, false, (handler, pItem) -> {
                    MenuSwitch ss = (MenuSwitch) pItem;
                    var.setValue(ss.value);
                }) {
                    @Override
                    public void draw(MenuHandler handler) {
                        this.value = var.getValue();
                        super.draw(handler);
                    }
                };
            }
        };

        menu.mMenus[COLORCORR] = rmenu;
        return rmenu;
    }

}
