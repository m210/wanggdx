package ru.m210projects.Wang.Menus;

import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Wang.Main;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Wang.Border.SetBorder;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Main.cfg;

public class MenuInterface extends BuildMenu {

    public MenuInterface(final Main app) {
        super(app.pMenu);
        WangTitle mTitle = new WangTitle("Interface Setup");
        int pos = 30;

        MenuSwitch messages = new WangSwitch("Messages", app.getFont(1), 35, pos += 10, 240, cfg.Messages,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.Messages = sw.value;
                }) {
            @Override
            public void open() {
                value = cfg.Messages;
            }
        };

        MenuSlider sScreenSize = new MenuSlider(app.pSlider, "Screen size", app.getFont(1), 35, pos += 10, 240,
                cfg.BorderNum, 0, 3, 1, (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    SetBorder(Player[myconnectindex], slider.value);
                }, false);

        MenuSlider mCurSize = new MenuSlider(app.pSlider, "Mouse cursor size", app.getFont(1), 35, pos += 10, 240,
                cfg.getgMouseCursorSize(), 0x1000, 0x28000, 4096, (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.setgMouseCursorSize(slider.value);
                }, false);

        pos += 5;
        MenuSwitch sCrosshair = new WangSwitch("Crosshair", app.getFont(1), 35, pos += 10, 240, cfg.Crosshair,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.Crosshair = sw.value;
                }) {
            @Override
            public void open() {
                value = cfg.Crosshair;
            }
        };

        MenuSlider sCrossSize = new MenuSlider(app.pSlider, "Crosshair size", app.getFont(1), 35, pos += 10, 240,
                cfg.CrosshairSize, 16384, 65536, 8192, (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.CrosshairSize = slider.value;
                }, false);
        pos += 5;
        MenuConteiner sShowStat = new MenuConteiner("Statistics:", app.getFont(1), 35, pos += 10, 240, null, 0,
                (handler, pItem) -> {
                    MenuConteiner item = (MenuConteiner) pItem;
                    cfg.Stats = item.num;
                }) {
            @Override
            public void open() {
                if (this.list == null) {
                    this.list = new char[3][];
                    this.list[0] = "Off".toCharArray();
                    this.list[1] = "Always show".toCharArray();
                    this.list[2] = "Only on a minimap".toCharArray();
                }
                num = cfg.Stats;
            }
        };

        MenuSlider sStatSize = new MenuSlider(app.pSlider, "Statistics size", app.getFont(1), 35, pos += 10, 240,
                cfg.gStatSize, 16384, 2 * 65536, 4096, (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.gStatSize = slider.value;
                }, true);
        sStatSize.digitalMax = 65536.0f;
        pos += 5;

        WangSwitch sShowMapName = new WangSwitch("Info at level startup", app.getFont(1), 35, pos += 10, 240,
                cfg.showMapInfo == 1, (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.showMapInfo = sw.value ? 1 : 0;
                });

        MenuSwitch sShowFPS = new WangSwitch("Fps counter", app.getFont(1), 35, pos += 10, 240, cfg.isgShowFPS(),
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.setgShowFPS(sw.value);
                });

        MenuSlider sFpsSize = new MenuSlider(app.pSlider, "Fps size", app.getFont(1), 35, pos + 10, 240,
                (int) (cfg.getgFpsScale() * 65536), 32768, 3 * 65536, 8192, (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.setgFpsScale(slider.value / 65536.0f);
                }, true);
        sFpsSize.pal = 16;
        sFpsSize.digitalMax = 65536f;

        addItem(mTitle, false);
        addItem(messages, true);
        addItem(sScreenSize, false);
        addItem(mCurSize, false);
        addItem(sCrosshair, false);
        addItem(sCrossSize, false);
        addItem(sShowStat, false);
        addItem(sStatSize, false);
        addItem(sShowMapName, false);
        addItem(sShowFPS, false);
        addItem(sFpsSize, false);
    }
}
