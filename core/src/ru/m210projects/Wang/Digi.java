package ru.m210projects.Wang;

import ru.m210projects.Wang.Type.Amb_Info;
import ru.m210projects.Wang.Type.VOC_INFO;

public class Digi {

    public static final int DIGI_NULL = 0;
    public static final int DIGI_SWORDSWOOSH = 1;
    public static final int DIGI_STAR = 2;
    public static final int DIGI_STARCLINK = 3;
    public static final int DIGI_UZIFIRE = 5;
    public static final int DIGI_RICHOCHET1 = 6;
    public static final int DIGI_RICHOCHET2 = 7;
    public static final int DIGI_REMOVECLIP = 8;
    public static final int DIGI_REPLACECLIP = 9;
    public static final int DIGI_SHELL = 10;
    public static final int DIGI_RIOTFIRE = 11;
    public static final int DIGI_RIOTFIRE2 = 12;
    public static final int DIGI_BOLTEXPLODE = 14;
    public static final int DIGI_30MMFIRE = 16;
    public static final int DIGI_30MMEXPLODE = 18;
    public static final int DIGI_HEADFIRE = 20;
    public static final int DIGI_HEADSHOTHIT = 22;
    public static final int DIGI_MINETHROW = 23;
    public static final int DIGI_MINEBLOW = 25;
    public static final int DIGI_MINEBEEP = 26;
    public static final int DIGI_HEARTBEAT = 27;
    public static final int DIGI_HEARTFIRE = 28;
    public static final int DIGI_RFWIZ = 32;
    public static final int DIGI_NAPFIRE = 33;
    public static final int DIGI_NAPWIZ = 34;
    public static final int DIGI_NAPPUFF = 35;
    public static final int DIGI_MIRVFIRE = 36;
    public static final int DIGI_MIRVWIZ = 37;
    public static final int DIGI_MAGIC1 = 40;
    public static final int DIGI_DHVOMIT = 48;
    public static final int DIGI_PROJECTILEWATERHIT = 52;
    public static final int DIGI_KEY = 53;
    public static final int DIGI_ITEM = 54;
    public static final int DIGI_BIGITEM = 55;
    public static final int DIGI_BODYFALL1 = 56;
    public static final int DIGI_HITGROUND = 57;
    public static final int DIGI_BODYSQUISH1 = 58;
    public static final int DIGI_BODYCRUSHED1 = 61;
    public static final int DIGI_ACTORBODYFALL1 = 73;
    public static final int DIGI_ACTORHITGROUND = 74;
    public static final int DIGI_COOLIEEXPLODE = 75;
    public static final int DIGI_COOLIESCREAM = 76;
    public static final int DIGI_COOLIEALERT = 77;
    public static final int DIGI_COOLIEAMBIENT = 78;
    public static final int DIGI_COOLIEPAIN = 79;
    public static final int DIGI_CGMATERIALIZE = 80;
    public static final int DIGI_CGALERT = 81;
    public static final int DIGI_CGTHIGHBONE = 82;
    public static final int DIGI_CGAMBIENT = 83;
    public static final int DIGI_CGPAIN = 84;
    public static final int DIGI_CGMAGIC = 85;
    public static final int DIGI_CGMAGICHIT = 86;
    public static final int DIGI_CGSCREAM = 87;
    public static final int DIGI_NINJAAMBIENT = 88;
    public static final int DIGI_NINJAPAIN = 90;
    public static final int DIGI_NINJASCREAM = 91;
    public static final int DIGI_NINJAALERT = 92;
    public static final int DIGI_NINJAUZIATTACK = 93;
    public static final int DIGI_NINJARIOTATTACK = 94;
    public static final int DIGI_RIPPERAMBIENT = 95;
    public static final int DIGI_RIPPERALERT = 96;
    public static final int DIGI_RIPPERATTACK = 97;
    public static final int DIGI_RIPPERPAIN = 98;
    public static final int DIGI_RIPPERSCREAM = 99;
    public static final int DIGI_RIPPERHEARTOUT = 100;
    public static final int DIGI_GRDAMBIENT = 101;
    public static final int DIGI_GRDALERT = 102;
    public static final int DIGI_GRDPAIN = 103;
    public static final int DIGI_GRDSCREAM = 104;
    public static final int DIGI_GRDFIREBALL = 105;
    public static final int DIGI_GRDSWINGAXE = 106;
    public static final int DIGI_GRDAXEHIT = 107;
    public static final int DIGI_SPAMBIENT = 108;
    public static final int DIGI_SPALERT = 109;
    public static final int DIGI_SPPAIN = 110;
    public static final int DIGI_SPSCREAM = 111;
    public static final int DIGI_SPBLADE = 112;
    public static final int DIGI_SPELEC = 113;
    public static final int DIGI_SPTELEPORT = 114;
    public static final int DIGI_AHAMBIENT = 115;
    public static final int DIGI_AHSCREAM = 116;
    public static final int DIGI_AHEXPLODE = 117;
    public static final int DIGI_HORNETBUZZ = 119;
    public static final int DIGI_HORNETSTING = 120;
    public static final int DIGI_HORNETDEATH = 122;
    public static final int DIGI_SERPAMBIENT = 123;
    public static final int DIGI_SERPALERT = 124;
    public static final int DIGI_SERPPAIN = 125;
    public static final int DIGI_SERPSCREAM = 126;
    public static final int DIGI_SERPDEATHEXPLODE = 127;
    public static final int DIGI_SERPSWORDATTACK = 128;
    public static final int DIGI_SERPMAGICLAUNCH = 129;
    public static final int DIGI_SERPSUMMONHEADS = 130;
    public static final int DIGI_SERPTAUNTYOU = 131;
    public static final int DIGI_LAVABOSSAMBIENT = 132;
    public static final int DIGI_LAVABOSSSWIM = 133;
    public static final int DIGI_LAVABOSSRISE = 134;
    public static final int DIGI_LAVABOSSALERT = 135;
    public static final int DIGI_LAVABOSSFLAME = 136;
    public static final int DIGI_LAVABOSSMETEOR = 137;
    public static final int DIGI_LAVABOSSMETEXP = 138;
    public static final int DIGI_LAVABOSSPAIN = 139;
    public static final int DIGI_LAVABOSSSIZZLE = 140;
    public static final int DIGI_LAVABOSSEXPLODE = 141;
    public static final int DIGI_BOATFIRE = 145;
    public static final int DIGI_BOMBRFLYING = 156;
    public static final int DIGI_BOMBRDROPBOMB = 157;
    public static final int DIGI_BUBBLES = 158;
    public static final int DIGI_CRICKETS = 161;
    public static final int DIGI_DRILL = 172;
    public static final int DIGI_CAVEDRIP1 = 173;
    public static final int DIGI_CAVEDRIP2 = 174;
    public static final int DIGI_DRIP = 175;
    public static final int DIGI_WATERFALL1 = 176;
    public static final int DIGI_WATERFALL2 = 177;
    public static final int DIGI_WATERFLOW1 = 178;
    public static final int DIGI_WATERFLOW2 = 179;
    public static final int DIGI_SMALLEXP = 181;
    public static final int DIGI_MEDIUMEXP = 182;
    public static final int DIGI_FIRE1 = 185;
    public static final int DIGI_FIRE2 = 186;
    public static final int DIGI_FIREBALL1 = 187;
    public static final int DIGI_GEAR1 = 189;
    public static final int DIGI_GONG = 190;
    public static final int DIGI_LAVAFLOW1 = 191;
    public static final int DIGI_MACHINE1 = 192;
    public static final int DIGI_MUBBUBBLES1 = 193;
    public static final int DIGI_EARTHQUAKE = 194;
    public static final int DIGI_SEWERFLOW1 = 195;
    public static final int DIGI_SPLASH1 = 196;
    public static final int DIGI_STEAM1 = 197;
    public static final int DIGI_VOLCANOSTEAM1 = 198;
    public static final int DIGI_SWAMP = 200;
    public static final int DIGI_REGULARSWITCH = 201;
    public static final int DIGI_BIGSWITCH = 202;
    public static final int DIGI_THUNDER = 206;
    public static final int DIGI_TELEPORT = 207;
    public static final int DIGI_UNDERWATER = 208;
    public static final int DIGI_UNLOCK = 209;
    public static final int DIGI_VOID1 = 211;
    public static final int DIGI_VOID2 = 212;
    public static final int DIGI_VOID3 = 213;
    public static final int DIGI_VOID4 = 214;
    public static final int DIGI_VOID5 = 215;
    public static final int DIGI_ERUPTION = 216;
    public static final int DIGI_VOLCANOPROJECTILE = 217;
    public static final int DIGI_LIGHTWIND = 218;
    public static final int DIGI_STRONGWIND = 219;
    public static final int DIGI_BREAKINGWOOD = 220;
    public static final int DIGI_BREAKSTONES = 221;
    public static final int DIGI_ENGROOM1 = 222;
    public static final int DIGI_ENGROOM2 = 223;
    public static final int DIGI_ENGROOM3 = 224;
    public static final int DIGI_ENGROOM4 = 225;
    public static final int DIGI_ENGROOM5 = 226;
    public static final int DIGI_BREAKGLASS = 227;
    public static final int DIGI_HELI = 229;
    public static final int DIGI_BIGHART = 230;
    public static final int DIGI_WIND4 = 231;
    public static final int DIGI_SPOOKY1 = 232;
    public static final int DIGI_JET = 234;
    public static final int DIGI_DRUMCHANT = 235;
    public static final int DIGI_BUZZZ = 236;
    public static final int DIGI_CHOP_CLICK = 237;
    public static final int DIGI_SWORD_UP = 238;
    public static final int DIGI_UZI_UP = 239;
    public static final int DIGI_SHOTGUN_UP = 240;
    public static final int DIGI_ROCKET_UP = 241;
    public static final int DIGI_GRENADE_UP = 242;
    public static final int DIGI_RAIL_UP = 243;
    public static final int DIGI_MINE_UP = 244;
    public static final int DIGI_TAUNTAI1 = 246;
    public static final int DIGI_TAUNTAI2 = 247;
    public static final int DIGI_TAUNTAI3 = 248;
    public static final int DIGI_TAUNTAI4 = 249;
    public static final int DIGI_TAUNTAI5 = 250;
    public static final int DIGI_TAUNTAI6 = 251;
    public static final int DIGI_TAUNTAI7 = 252;
    public static final int DIGI_TAUNTAI8 = 253;
    public static final int DIGI_TAUNTAI9 = 254;
    public static final int DIGI_TAUNTAI10 = 255;
    public static final int DIGI_PLAYERPAIN1 = 256;
    public static final int DIGI_PLAYERPAIN2 = 257;
    public static final int DIGI_PLAYERPAIN3 = 258;
    public static final int DIGI_PLAYERPAIN4 = 259;
    public static final int DIGI_PLAYERPAIN5 = 260;
    public static final int DIGI_PLAYERYELL1 = 261;
    public static final int DIGI_PLAYERYELL2 = 262;
    public static final int DIGI_PLAYERYELL3 = 263;
    public static final int DIGI_SEARCHWALL = 264;
    public static final int DIGI_FALLSCREAM = 266;
    public static final int DIGI_GOTITEM1 = 267;
    public static final int DIGI_RAILFIRE = 269;
    public static final int DIGI_RAILREADY = 270;
    public static final int DIGI_NUCLEAREXP = 272;
    public static final int DIGI_NUKESTDBY = 273;
    public static final int DIGI_NUKECDOWN = 274;
    public static final int DIGI_NUKEREADY = 275;
    public static final int DIGI_CHEMGAS = 276;
    public static final int DIGI_CHEMBOUNCE = 277;
    public static final int DIGI_THROW = 278;
    public static final int DIGI_PULL = 279;
    public static final int DIGI_GASPOP = 283;
    public static final int DIGI_40MMBNCE = 284;
    public static final int DIGI_CALTROPS = 288;
    public static final int DIGI_NIGHTON = 289;
    public static final int DIGI_NIGHTOFF = 290;
    public static final int DIGI_SHOTSHELLSPENT = 291;
    public static final int DIGI_ARMORHIT = 295;
    public static final int DIGI_ASIREN1 = 296;
    public static final int DIGI_FIRETRK1 = 297;
    public static final int DIGI_TRAFFIC1 = 298;
    public static final int DIGI_TRAFFIC2 = 299;
    public static final int DIGI_TRAFFIC3 = 300;
    public static final int DIGI_TRAFFIC4 = 301;
    public static final int DIGI_TRAFFIC5 = 302;
    public static final int DIGI_TRAFFIC6 = 303;
    public static final int DIGI_HELI1 = 304;
    public static final int DIGI_JET1 = 305;
    public static final int DIGI_MOTO1 = 306;
    public static final int DIGI_MOTO2 = 307;
    public static final int DIGI_NEON1 = 308;
    public static final int DIGI_SUBWAY = 309;
    public static final int DIGI_TRAIN1 = 310;
    public static final int DIGI_COINS = 311;
    public static final int DIGI_SWORDCLANK = 312;
    public static final int DIGI_RIPPER2AMBIENT = 313;
    public static final int DIGI_RIPPER2ALERT = 314;
    public static final int DIGI_RIPPER2ATTACK = 315;
    public static final int DIGI_RIPPER2PAIN = 316;
    public static final int DIGI_RIPPER2SCREAM = 317;
    public static final int DIGI_RIPPER2HEARTOUT = 318;
    public static final int DIGI_M60 = 319;
    public static final int DIGI_SUMOSCREAM = 320;
    public static final int DIGI_SUMOALERT = 321;
    public static final int DIGI_SUMOAMBIENT = 322;
    public static final int DIGI_SUMOPAIN = 323;
    public static final int DIGI_RAMUNLOCK = 324;
    public static final int DIGI_CARDUNLOCK = 325;
    public static final int DIGI_ANCIENTSECRET = 326;
    public static final int DIGI_LIKEBIGWEAPONS = 330;
    public static final int DIGI_COWABUNGA = 331;
    public static final int DIGI_NOCHARADE = 332;
    public static final int DIGI_TIMETODIE = 333;
    public static final int DIGI_EATTHIS = 334;
    public static final int DIGI_FIRECRACKERUPASS = 335;
    public static final int DIGI_HOLYCOW = 336;
    public static final int DIGI_HOLYPEICESOFCOW = 337;
    public static final int DIGI_HOLYSHIT = 338;
    public static final int DIGI_HOLYPEICESOFSHIT = 339;
    public static final int DIGI_PAYINGATTENTION = 340;
    public static final int DIGI_EVERYBODYDEAD = 341;
    public static final int DIGI_KUNGFU = 342;
    public static final int DIGI_HOWYOULIKEMOVE = 343;
    public static final int DIGI_NOMESSWITHWANG = 344;
    public static final int DIGI_RAWREVENGE = 345;
    public static final int DIGI_YOULOOKSTUPID = 346;
    public static final int DIGI_TINYDICK = 347;
    public static final int DIGI_NOTOURNAMENT = 348;
    public static final int DIGI_WHOWANTSWANG = 349;
    public static final int DIGI_MOVELIKEYAK = 350;
    public static final int DIGI_ALLINREFLEXES = 351;
    public static final int DIGI_EVADEFOREVER = 352;
    public static final int DIGI_MRFLY = 353;
    public static final int DIGI_SHISEISI = 354;
    public static final int DIGI_LIKEFIREWORKS = 355;
    public static final int DIGI_LIKEHIROSHIMA = 356;
    public static final int DIGI_LIKENAGASAKI = 357;
    public static final int DIGI_LIKEPEARL = 358;
    public static final int DIGI_IAMSHADOW = 359;
    public static final int DIGI_ILIKENUKES = 360;
    public static final int DIGI_ILIKESWORD = 361;
    public static final int DIGI_ILIKESHURIKEN = 362;
    public static final int DIGI_NOLIKEMUSIC = 366;
    public static final int DIGI_NOFEAR = 368;
    public static final int DIGI_NOPAIN = 369;
    public static final int DIGI_NOREPAIRMAN = 370;
    public static final int DIGI_SONOFABITCH = 371;
    public static final int DIGI_PAINFORWEAK = 372;
    public static final int DIGI_GETTINGSTIFF = 374;
    public static final int DIGI_STICKYGOTU1 = 376;
    public static final int DIGI_STICKYGOTU2 = 377;
    public static final int DIGI_STICKYGOTU3 = 378;
    public static final int DIGI_STICKYGOTU4 = 379;
    public static final int DIGI_SWORDGOTU1 = 380;
    public static final int DIGI_SWORDGOTU2 = 381;
    public static final int DIGI_SWORDGOTU3 = 382;
    public static final int DIGI_HURTBAD1 = 383;
    public static final int DIGI_HURTBAD2 = 384;
    public static final int DIGI_HURTBAD3 = 385;
    public static final int DIGI_HURTBAD4 = 386;
    public static final int DIGI_HURTBAD5 = 387;
    public static final int DIGI_TOILETGIRLSCREAM = 388;
    public static final int DIGI_TOILETGIRLPAIN = 391;
    public static final int DIGI_SUMOFART = 394;
    public static final int DIGI_GIBS1 = 395;
    public static final int DIGI_GIBS2 = 396;
    public static final int DIGI_BIRDS1 = 397;
    public static final int DIGI_BIRDS2 = 398;
    public static final int DIGI_AMOEBA = 409;
    public static final int DIGI_BODYFALL2 = 410;
    public static final int DIGI_GIBS3 = 411;
    public static final int DIGI_NINJACHOKE = 412;
    public static final int DIGI_TRAIN3 = 413;
    public static final int DIGI_TRAIN8 = 415;
    public static final int DIGI_TRASHLID = 416;
    public static final int DIGI_GETMEDKIT = 417;
    public static final int DIGI_AHH = 418;
    public static final int DIGI_PALARM = 419;
    public static final int DIGI_PFLIP = 420;
    public static final int DIGI_PROLL1 = 421;
    public static final int DIGI_PROLL2 = 422;
    public static final int DIGI_PROLL3 = 423;
    public static final int DIGI_BUNNYATTACK = 424;
    public static final int DIGI_BUNNYDIE2 = 426;
    public static final int DIGI_BUNNYDIE3 = 427;
    public static final int DIGI_BUNNYAMBIENT = 428;
    public static final int DIGI_NINJAINHALF = 430;
    public static final int DIGI_RIPPER2CHEST = 431;
    public static final int DIGI_WHIPME = 432;
    public static final int DIGI_ENDLEV = 433;
    public static final int DIGI_BREAKMETAL = 435;
    public static final int DIGI_BREAKDEBRIS = 436;
    public static final int DIGI_BANZAI = 438;
    public static final int DIGI_HAHA1 = 439;
    public static final int DIGI_HAHA2 = 440;
    public static final int DIGI_HAHA3 = 441;
    public static final int DIGI_ITEM_SPAWN = 442;
    public static final int DIGI_NOREPAIRMAN2 = 443;
    public static final int DIGI_DOUBLEUZI = 445;
    public static final int DIGI_CANBEONLYONE = 447;
    public static final int DIGI_HITTINGWALLS = 450;
    public static final int DIGI_GOTRAILGUN = 451;
    public static final int DIGI_RABBITHUMP1 = 452;
    public static final int DIGI_RABBITHUMP2 = 453;
    public static final int DIGI_RABBITHUMP3 = 454;
    public static final int DIGI_RABBITHUMP4 = 455;
    public static final int DIGI_FAGRABBIT1 = 456;
    public static final int DIGI_FAGRABBIT2 = 457;
    public static final int DIGI_FAGRABBIT3 = 458;
    public static final int DIGI_WHATYOUEATBABY = 460;
    public static final int DIGI_WHATDIEDUPTHERE = 461;
    public static final int DIGI_YOUGOPOOPOO = 462;
    public static final int DIGI_PULLMYFINGER = 463;
    public static final int DIGI_SOAPYOUGOOD = 464;
    public static final int DIGI_WASHWANG = 465;
    public static final int DIGI_DROPSOAP = 466;
    public static final int DIGI_REALTITS = 467;
    public static final int DIGI_FLAGWAVE = 475;
    public static final int DIGI_SURFACE = 476;
    public static final int DIGI_GASHURT = 477;
    public static final int DIGI_BONUS_GRAB = 478;
    public static final int DIGI_ANIMECRY = 479;
    public static final int DIGI_ANIMESING1 = 480;
    public static final int DIGI_ANIMEMAD1 = 481;
    public static final int DIGI_ANIMESING2 = 482;
    public static final int DIGI_ANIMEMAD2 = 483;
    public static final int DIGI_PLAYER_TELEPORT = 484;
    public static final int DIGI_INTRO_SLASH = 485;
    public static final int DIGI_WARNING = 486;
    public static final int DIGI_INTRO_WHIRL = 487;
    public static final int DIGI_TOILETGIRLFART1 = 488;
    public static final int DIGI_TOILETGIRLFART2 = 489;
    public static final int DIGI_TOILETGIRLFART3 = 490;
    public static final int DIGI_WINDCHIMES = 491;
    public static final int DIGI_USEBROKENVEHICLE = 494;
    public static final int DIGI_STEPONCALTROPS = 495;
    public static final int DIGI_SERPTAUNTWANG = 497;
    public static final int DIGI_WANGTAUNTSERP1 = 498;
    public static final int DIGI_WANGDROWNING = 502;
    public static final int DIGI_PMESSAGE = 504;
    public static final int DIGI_SHAREND_UGLY1 = 505;
    public static final int DIGI_SHAREND_UGLY2 = 506;
    public static final int DIGI_SHAREND_TELEPORT = 507;
    public static final int DIGI_HOTHEADSWITCH = 508;
    public static final int DIGI_BOATCREAK = 509;
    public static final int DIGI_SHIPBELL = 512;
    public static final int DIGI_FOGHORN = 513;
    public static final int DIGI_CANNON = 514;
    public static final int DIGI_JG41012 = 516;
    public static final int DIGI_JG41028 = 518;
    public static final int DIGI_JG44027 = 539;
    public static final int DIGI_JG44038 = 540;
    public static final int DIGI_JG44039 = 541;
    public static final int DIGI_JG44048 = 542;
    public static final int DIGI_JG44052 = 543;
    public static final int DIGI_JG45014 = 544;
    public static final int DIGI_JG44068 = 545;
    public static final int DIGI_JG45010 = 546;
    public static final int DIGI_JG45018 = 547;
    public static final int DIGI_JG45030 = 548;
    public static final int DIGI_JG45033 = 549;
    public static final int DIGI_JG45043 = 550;
    public static final int DIGI_JG45053 = 551;
    public static final int DIGI_JG45067 = 552;
    public static final int DIGI_JG46005 = 553;
    public static final int DIGI_JG46010 = 554;
    public static final int DIGI_LANI049 = 555;
    public static final int DIGI_LANI051 = 556;
    public static final int DIGI_LANI052 = 557;
    public static final int DIGI_LANI054 = 558;
    public static final int DIGI_LANI060 = 559;
    public static final int DIGI_LANI063 = 560;
    public static final int DIGI_LANI065 = 561;
    public static final int DIGI_LANI066 = 562;
    public static final int DIGI_LANI073 = 563;
    public static final int DIGI_LANI075 = 564;
    public static final int DIGI_LANI077 = 565;
    public static final int DIGI_LANI079 = 566;
    public static final int DIGI_LANI089 = 567;
    public static final int DIGI_LANI091 = 568;
    public static final int DIGI_LANI093 = 569;
    public static final int DIGI_LANI095 = 570;

    public static final int DIGI_GIRLNINJAALERTT = 575;
    public static final int DIGI_GIRLNINJASCREAM = 576;
    public static final int DIGI_GIRLNINJAALERT = 577;
    public static final int DIGI_PRUNECACKLE = 578;
    public static final int DIGI_PRUNECACKLE2 = 579;
    public static final int DIGI_PRUNECACKLE3 = 580;
    public static final int DIGI_SUMOSTOMP = 581;
    public static final int DIGI_JG9009 = 583;
    public static final int DIGI_Z16004 = 584;
    public static final int DIGI_Z17010 = 590;
    public static final int DIGI_Z17052 = 591;
    public static final int DIGI_Z17025 = 592;
    public static final int DIGI_JG94024 = 612;
    public static final int DIGI_JG94039 = 613;
    public static final int DIGI_JG95012 = 614;
    public static final int DIGI_ZILLASTOMP = 615;
    public static final int DIGI_ZC1 = 616;
    public static final int DIGI_ZC2 = 617;
    public static final int DIGI_ZC3 = 618;
    public static final int DIGI_ZC4 = 619;
    public static final int DIGI_ZC5 = 620;
    public static final int DIGI_ZC6 = 621;
    public static final int DIGI_ZC7 = 622;
    public static final int DIGI_ZC8 = 623;
    public static final int DIGI_ZC9 = 624;
    public static final int DIGI_Z16043 = 625;
    // NOTE: HIGHER priority numbers have the highest precedence in the play list.
    public static final int PRI_PLAYERDEATH = 51;
    public static final int PRI_PLAYERVOICE = 50;
    public static final int DIST_NORMAL = 0;
    private static final int PRI_MAX = 100;
    private static final int PRI_HI_PLAYERWEAP = 49;
    private static final int PRI_LOW_PLAYERWEAP = 48;
    private static final int PRI_PLAYERAMBIENT = 40;
    private static final int PRI_NPCDEATH = 49;
    private static final int PRI_NPCWEAP = 47;
    private static final int PRI_NPCATTACK = 42;
    private static final int PRI_NPCAMBIENT = 39;
    private static final int PRI_ITEM = 41;
    private static final int PRI_SECTOROBJ = 30;
    private static final int PRI_ENVIRONMENT = 20;
    private static final int PRI_AMBIENT = 10;
    private static final int DIST_MAXNORMAL = 16384; // This is max distance constant for normal sounds
    // This is the limiting constant in Sound_Dist function.
    private static final int DIST_WIDE = 65536; // Half Level at full volume before sound begins to fade.
    private static final int DIST_LEVELWIDE = 131072; // Full Level

    private static final int VF_NORMAL = 0;
    private static final int VF_LOOP = 1;
    // Ambient Flags, flags can be added whenever needed, up to 16 bits
    private static final int AMB_NONE = 0;
    private static final int AMB_PAN = 8; // 1 = Don't do panning of sound
    private static final int AMB_INTERMIT = 32; // 1 = This is a non-looping intermittant sound
    // Tic counts used for intermittent sounds
    private static final int AMB_TICRATE = 12; // 120/10 since it's only called 10x per second
    private static final int AMB_NOTICS = 0;
    private static final int AMB_5 = 5 * AMB_TICRATE;
    private static final int AMB_10 = 10 * AMB_TICRATE; // AMB_TICRATE is the game's tic rate
    private static final int AMB_20 = 20 * AMB_TICRATE;
    private static final int AMB_30 = 30 * AMB_TICRATE;
    private static final int AMB_45 = 45 * AMB_TICRATE;
    private static final int AMB_60 = 60 * AMB_TICRATE;
    private static final int AMB_120 = 120 * AMB_TICRATE;
    public static final Amb_Info[] ambarray = {
            // BUBBLES
            new Amb_Info(DIGI_BUBBLES, AMB_PAN, AMB_NOTICS),

            // CRICKETS
            new Amb_Info(DIGI_CRICKETS, AMB_PAN, AMB_NOTICS),

            // AMBIENT WATER DRIPPING IN CAVE
            new Amb_Info(DIGI_CAVEDRIP1, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_CAVEDRIP2, AMB_INTERMIT, AMB_10),
            new Amb_Info(DIGI_DRIP, AMB_PAN | AMB_INTERMIT, AMB_20),

            // WATER FALL
            new Amb_Info(DIGI_WATERFALL1, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_WATERFALL2, AMB_NONE, AMB_NOTICS),

            // WATER FLOWING
            new Amb_Info(DIGI_WATERFLOW1, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_WATERFLOW2, AMB_NONE, AMB_NOTICS),

            // CRACKLING FIRE FOR CONTINUOUS BURN
            new Amb_Info(DIGI_FIRE1, AMB_NONE, AMB_NOTICS),

            // POWERFULL HIGH HEAT CONTINUOUS BURN
            new Amb_Info(DIGI_FIRE2, AMB_NONE, AMB_NOTICS),

            // AMBIENT GONG FOR USE IN TEMPLE/PALACE LEVELS
            new Amb_Info(DIGI_GONG, AMB_INTERMIT, AMB_120),

            // AMBIENT LAVA FLOW
            new Amb_Info(DIGI_LAVAFLOW1, AMB_NONE, AMB_NOTICS),

            // AMBIENT MUD BUBBLES
            new Amb_Info(DIGI_MUBBUBBLES1, AMB_NONE, AMB_NOTICS),

            // AMBIENT EARTH QUAKE
            new Amb_Info(DIGI_EARTHQUAKE, AMB_NONE, AMB_NOTICS),

            // YUCKY SEWER FLOW
            new Amb_Info(DIGI_SEWERFLOW1, AMB_NONE, AMB_NOTICS),

            // STEAM FLOW
            new Amb_Info(DIGI_STEAM1, AMB_NONE, AMB_NOTICS),

            // VOLCANIC STEAM VENT
            new Amb_Info(DIGI_VOLCANOSTEAM1, AMB_NONE, AMB_NOTICS),

            // SCARY AMBIENT SWAMP SOUNDS
            new Amb_Info(DIGI_SWAMP, AMB_NONE, AMB_NOTICS),

            // AMBIENT ROLLING THUNDER
            new Amb_Info(DIGI_THUNDER, AMB_PAN | AMB_INTERMIT, AMB_60),

            // UNDERWATER AMBIENCE
            new Amb_Info(DIGI_UNDERWATER, AMB_PAN, AMB_NOTICS),

            // SPOOKY ETHERAL VOID AMBIENCE (NETHERWORLDLY SOUNDS),
            new Amb_Info(DIGI_VOID1, AMB_NONE, AMB_NOTICS), new Amb_Info(DIGI_VOID2, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_VOID3, AMB_NONE, AMB_NOTICS), new Amb_Info(DIGI_VOID4, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_VOID5, AMB_NONE, AMB_NOTICS),

            // VOLCANIC ERUPTION
            new Amb_Info(DIGI_ERUPTION, AMB_NONE, AMB_NOTICS),

            // VOLCANIC SIZZLING PROJECTILES FLYING THROUGH AIR
            new Amb_Info(DIGI_VOLCANOPROJECTILE, AMB_NONE, AMB_NOTICS),

            // LIGHT WIND AMBIENCE
            new Amb_Info(DIGI_LIGHTWIND, AMB_NONE, AMB_NOTICS),

            // STRONG BLOWING WIND AMBIENCE
            new Amb_Info(DIGI_STRONGWIND, AMB_PAN | AMB_INTERMIT, AMB_20),

            // BREAKING WOOD AMBIENCE
            new Amb_Info(DIGI_BREAKINGWOOD, AMB_INTERMIT, AMB_120),

            // BREAKING, TUMBLING STONES FALLING AMBIENCE
            new Amb_Info(DIGI_BREAKSTONES, AMB_NONE, AMB_NOTICS),

            // MOTOR BOAT
            new Amb_Info(DIGI_NULL, AMB_NONE, AMB_NOTICS), new Amb_Info(DIGI_NULL, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_NULL, AMB_NONE, AMB_NOTICS),

            // WWII JAP ARMY TANK
            new Amb_Info(DIGI_NULL, AMB_NONE, AMB_NOTICS), new Amb_Info(DIGI_NULL, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_NULL, AMB_NONE, AMB_NOTICS), new Amb_Info(DIGI_NULL, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_NULL, AMB_NONE, AMB_NOTICS),

            // WWII JAP BOMBER PLANE
            new Amb_Info(DIGI_BOMBRFLYING, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_BOMBRDROPBOMB, AMB_NONE, AMB_NOTICS),

            // GIANT DRILL MACHINE
            new Amb_Info(DIGI_DRILL, AMB_NONE, AMB_NOTICS),

            // SECTOR GEAR COG TURNING
            new Amb_Info(DIGI_GEAR1, AMB_NONE, AMB_NOTICS),

            // GENERIC SECTOR OBJECT MACHINE RUNNING
            new Amb_Info(DIGI_MACHINE1, AMB_NONE, AMB_NOTICS),

            // ENGINE ROOM
            new Amb_Info(DIGI_ENGROOM1, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_ENGROOM2, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_ENGROOM3, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_ENGROOM4, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_ENGROOM5, AMB_NONE, AMB_NOTICS),

            // HELICOPTER, SPINNING BLADE SOUND
            new Amb_Info(DIGI_HELI, AMB_NONE, AMB_NOTICS),

            // ECHOING HEART
            new Amb_Info(DIGI_BIGHART, AMB_NONE, AMB_NOTICS),

            // ETHERAL WIND
            new Amb_Info(DIGI_WIND4, AMB_NONE, AMB_NOTICS),

            // SPOOKY SINE WAVE
            new Amb_Info(DIGI_SPOOKY1, AMB_NONE, AMB_NOTICS),

            // JET ENGINE
            new Amb_Info(DIGI_JET, AMB_NONE, AMB_NOTICS),

            // CEREMONIAL DRUM CHANT
            new Amb_Info(DIGI_DRUMCHANT, AMB_NONE, AMB_NOTICS),

            new Amb_Info(DIGI_ASIREN1, AMB_INTERMIT, AMB_45), new Amb_Info(DIGI_FIRETRK1, AMB_INTERMIT, AMB_60),
            new Amb_Info(DIGI_TRAFFIC1, AMB_INTERMIT, AMB_60),
            new Amb_Info(DIGI_TRAFFIC2, AMB_INTERMIT, AMB_60),
            new Amb_Info(DIGI_TRAFFIC3, AMB_INTERMIT, AMB_60),
            new Amb_Info(DIGI_TRAFFIC4, AMB_INTERMIT, AMB_60),
            new Amb_Info(DIGI_TRAFFIC5, AMB_INTERMIT, AMB_30),
            new Amb_Info(DIGI_TRAFFIC6, AMB_INTERMIT, AMB_60), new Amb_Info(DIGI_HELI1, AMB_INTERMIT, AMB_120),
            new Amb_Info(DIGI_JET1, AMB_INTERMIT, AMB_120), new Amb_Info(DIGI_MOTO1, AMB_INTERMIT, AMB_45),
            new Amb_Info(DIGI_MOTO2, AMB_INTERMIT, AMB_60), new Amb_Info(DIGI_NEON1, AMB_INTERMIT, AMB_5),
            new Amb_Info(DIGI_SUBWAY, AMB_INTERMIT, AMB_30), new Amb_Info(DIGI_TRAIN1, AMB_INTERMIT, AMB_120),
            new Amb_Info(DIGI_BIRDS1, AMB_INTERMIT, AMB_10), new Amb_Info(DIGI_BIRDS2, AMB_INTERMIT, AMB_10),
            new Amb_Info(DIGI_AMOEBA, AMB_NONE, AMB_NOTICS), new Amb_Info(DIGI_TRAIN3, AMB_INTERMIT, AMB_120),
            new Amb_Info(DIGI_TRAIN8, AMB_INTERMIT, AMB_120), new Amb_Info(DIGI_WHIPME, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_FLAGWAVE, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_ANIMECRY, AMB_NONE, AMB_NOTICS),
            new Amb_Info(DIGI_WINDCHIMES, AMB_INTERMIT, AMB_10),
            new Amb_Info(DIGI_BOATCREAK, AMB_INTERMIT, AMB_10),
            new Amb_Info(DIGI_SHIPBELL, AMB_INTERMIT, AMB_30),
            new Amb_Info(DIGI_FOGHORN, AMB_INTERMIT, AMB_120)};
    public static final VOC_INFO[] voc = {
            // NULL Entry used to detect a sound's presence in sprite attrib structs.
            new VOC_INFO("NULL.VOC", 0, 0, 0, DIST_NORMAL, VF_NORMAL),

            // SWORD
            new VOC_INFO("SWRDSTR1.VOC", PRI_HI_PLAYERWEAP, -200, 200, DIST_NORMAL, VF_NORMAL),

            // SHURIKEN
            new VOC_INFO("THROW.VOC", PRI_HI_PLAYERWEAP, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("STRCLNK.VOC", PRI_PLAYERAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NULL.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),

            // UZI
            new VOC_INFO("UZIFIRE1.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RICH1.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RICH2.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RMVCLIP.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RPLCLIP.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            // SPENT SHELL HITTING FLOOR
            new VOC_INFO("SHELL.VOC", PRI_NPCATTACK, -200, 200, DIST_NORMAL, VF_NORMAL),

            // CROSSRIOT
            new VOC_INFO("RIOTFIR1.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SHOTGUN.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIOTRLD.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("EXPMED.VOC", PRI_HI_PLAYERWEAP, -100, 100, DIST_MAXNORMAL,
                    VF_NORMAL),
            new VOC_INFO("RIOTWIZ.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),

            // 30MM GRENADE LAUNCHER
            new VOC_INFO("40MMFIR2.VOC", PRI_HI_PLAYERWEAP, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIOTRLD.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("40MMEXP2.VOC", PRI_HI_PLAYERWEAP, -100, 100, DIST_WIDE, VF_NORMAL),
            new VOC_INFO("RIOTWIZ.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),

            // GORO HEAD
            new VOC_INFO("GHFIR1.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GHWIZ.VOC", PRI_PLAYERAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("EXPSML.VOC", PRI_LOW_PLAYERWEAP, -100, 100, DIST_NORMAL, VF_NORMAL),

            // MINES
            new VOC_INFO("THROW.VOC", PRI_HI_PLAYERWEAP, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("PHITGRND.VOC", PRI_LOW_PLAYERWEAP, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("EXPLRG.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_WIDE, VF_NORMAL),
            new VOC_INFO("STSCAN2.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            // HEART ATTACK
            new VOC_INFO("HBLOOP1.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HSQUEEZ1.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HRTWIZ.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),

            // MISSILE BATTERY
            new VOC_INFO("RIOTFIR1.VOC", PRI_HI_PLAYERWEAP, -75, 75, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("EXPMED.VOC", PRI_HI_PLAYERWEAP, -100, 100, DIST_WIDE, VF_NORMAL),

            // RING OF FIRE SPELL
            new VOC_INFO("RFWIZ.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            // NAPALM SPELL
            new VOC_INFO("NAPFIRE.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NAPTWIZ.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("NAPPUFF.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            // MAGIC MIRV SPELL
            new VOC_INFO("MMFIRE.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MMWIZ.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),

            // SPIRAL SPELL
            new VOC_INFO("SPRLFIRE.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SPRLWIZ.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),

            // MAGIC SOUNDS, GENERIC
            // (USED FOR MAGIC CARPET RIDES,ETC.),
            new VOC_INFO("MAGIC1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MAGIC2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MAGIC3.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MAGIC4.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MAGIC5.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MAGIC6.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MAGIC7.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // SHADOW WARRIOR SPELL
            // #ifndef SW_SHAREWARE
            new VOC_INFO("SWSPELL.VOC", PRI_LOW_PLAYERWEAP, -100, 100, DIST_NORMAL,
                    VF_NORMAL),
            // #else
            // new VOC_INFO("NULL.VOC", DIGI_NULL_SWCLOAK, 47, PRI_LOW_PLAYERWEAP,-100, 100,
            // 0, DIST_NORMAL, VF_NORMAL ),
            // #endif

            // PLAYER DEAD HEAD
            new VOC_INFO("DHVOMIT.VOC", PRI_PLAYERAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("DHCLUNK.VOC", PRI_PLAYERAMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            // new VOC_INFO("DHSQSH.VOC", DIGI_DHSQUISH, 50, PRI_PLAYERAMBIENT, 0, 0, 0,
            // DIST_NORMAL, VF_NORMAL ),
            new VOC_INFO("NULL.VOC", PRI_PLAYERAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // WEAPON RELATED
            new VOC_INFO("LAVAHIT.VOC", PRI_PLAYERAMBIENT, -100, 100, DIST_NORMAL,
                    VF_NORMAL),
            new VOC_INFO("STSPL01.VOC", PRI_PLAYERAMBIENT, -100, 100, DIST_NORMAL,
                    VF_NORMAL),

            // ITEMS
            new VOC_INFO("KEY.VOC", PRI_ITEM, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ITEM5A.VOC", PRI_ITEM, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ITEMBIG2.VOC", PRI_ITEM, -100, 100, DIST_NORMAL, VF_NORMAL),

            // DEATH/HURT
            new VOC_INFO("BODY9.VOC", PRI_PLAYERDEATH, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("PHITGRND.VOC", PRI_PLAYERAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BODY2.VOC", PRI_PLAYERAMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BURN1.VOC", PRI_PLAYERAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BURNSCRM.VOC", PRI_PLAYERDEATH, -200, 200, DIST_NORMAL,
                    VF_NORMAL),
            new VOC_INFO("BODY3.VOC", PRI_PLAYERAMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BODY4.VOC", PRI_PLAYERAMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BURN2.VOC", PRI_PLAYERAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("DROWN1.VOC", PRI_PLAYERDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SCREAM1.VOC", PRI_PLAYERDEATH, -200, 400, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SCREAM2.VOC", PRI_PLAYERDEATH, -200, 400, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SCREAM3.VOC", PRI_PLAYERDEATH, -200, 400, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HIT1.VOC", PRI_LOW_PLAYERWEAP, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ELECTRC1.VOC", PRI_PLAYERAMBIENT, -200, 200, DIST_NORMAL,
                    VF_NORMAL),
            new VOC_INFO("SWDIE02.VOC", PRI_PLAYERDEATH, -200, 500, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("IMPALE1.VOC", PRI_PLAYERDEATH, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("OOF1.VOC", PRI_PLAYERAMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),

            // ACTOR SOUNDS THAT USER
            // PLAYER SOUNDS AT A LOWER PRIORITY
            new VOC_INFO("BODY1.VOC", PRI_NPCWEAP, 0, 0, DIST_NORMAL,
                    VF_NORMAL),
            new VOC_INFO("HITGRND.VOC", PRI_NPCWEAP, 0, 0, DIST_NORMAL,
                    VF_NORMAL),

            // NPC'S //////////////////////////////////////////////////////////////////////

            // COOLIE
            new VOC_INFO("COLEXP.VOC", PRI_NPCDEATH, -100, 100, DIST_MAXNORMAL, VF_NORMAL),
            new VOC_INFO("COLSCRM.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("COLALRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("COLAMB.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("COLPAIN.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),

            // COOLIE GHOST
            new VOC_INFO("CGMAT.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CGALRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CGWHACK.VOC", PRI_NPCWEAP, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CGAMB.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CGPAIN.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CGSHOOT.VOC", PRI_NPCWEAP, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CGHIT.VOC", PRI_NPCWEAP, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CGSCRM.VOC", PRI_NPCDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),

            // NINJA
            new VOC_INFO("NINAMB.VOC", PRI_NPCAMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NINSTAR.VOC", PRI_NPCWEAP, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NINPAIN.VOC", PRI_NPCWEAP, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NINSCRM.VOC", PRI_NPCDEATH, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NINALRT.VOC", PRI_NPCATTACK, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NINSHOOT.VOC", PRI_NPCWEAP, 0, 0, DIST_NORMAL,
                    VF_NORMAL),
            new VOC_INFO("RIOTFIR1.VOC", PRI_NPCWEAP, 0, 0, DIST_NORMAL,
                    VF_NORMAL),

            // RIPPER
            new VOC_INFO("RIPAMB.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIPALRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIPATCK.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIPPAIN.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIPSCRM.VOC", PRI_NPCDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIPHRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),

            // GUARDIAN
            new VOC_INFO("GRDAMB.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GRDALRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GRDPAIN.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GRDSCRM.VOC", PRI_NPCDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GRDFIR.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GRDAXE.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GRDAXHT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),

            // SKELETOR PRIEST
            new VOC_INFO("SPAMB.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SPALRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SPPAIN.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SPSCRM.VOC", PRI_NPCDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SPBLADE.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SPELEC.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SPTLPRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),

            // ACCURSED HEAD
            new VOC_INFO("AHAMB.VOC", PRI_NPCAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("AHSCRM.VOC", PRI_NPCDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("AHEXP.VOC", PRI_NPCDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("AHSHWSH.VOC", PRI_NPCATTACK, 0, 0, DIST_NORMAL, VF_LOOP),

            // HORNET
            new VOC_INFO("HBUZZ.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("HSTING.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HPAIN.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HDEATH.VOC", PRI_NPCDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),

            // SERPENT GOD BOSS
            new VOC_INFO("SGAMB.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SGALRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SGPAIN.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SGSCRM.VOC", PRI_MAX, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SGDETH.VOC", PRI_NPCDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SGSWORD.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SGMAGIC.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SGHEADS.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SGTAUNT.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),

            // LAVA BOSS
            new VOC_INFO("LVAMB.VOC", PRI_NPCAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LVSWIM.VOC", PRI_NPCAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LVRISE.VOC", PRI_NPCAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LVALRT.VOC", PRI_NPCATTACK, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LVFLAME.VOC", PRI_NPCATTACK, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LVMETEOR.VOC", PRI_NPCATTACK, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LVMETEXP.VOC", PRI_NPCATTACK, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LVPAIN.VOC", PRI_NPCATTACK, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LVSIZZLE.VOC", PRI_NPCAMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LVEXPL.VOC", PRI_NPCDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),

            // LEVEL AND SECTOR OBJECT SOUNDS ///////////

            // MOTOR BOAT
            new VOC_INFO("BTSTRT.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BTRUN01.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("BTSTOP.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BTFIRE.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            // ARMY TANK
            new VOC_INFO("TNKSTRT.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TNKRUN.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("TNKSTOP.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TNKIDLE.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("TNKFIRE.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            // ARMY TRUCK
            new VOC_INFO("TRUKRUN.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("TRUKIDLE.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),

            // SUBMARINE
            new VOC_INFO("SUBRUN.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("SUBIDLE.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("SUBDOOR.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),

            // WWII JAP BOMBER PLANE
            new VOC_INFO("BMBFLY.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("BMBDROP.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),

            // BUBBLES
            new VOC_INFO("BUBBLE.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),

            // CHAIN MOVING
            // SOUND NOT AVAILABLE -- DELTED
            new VOC_INFO("CHAIN.VOC", PRI_ENVIRONMENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // CHAIN DOOR
            new VOC_INFO("CHNDOOR.VOC", PRI_ENVIRONMENT, 0, 0, 8000, VF_NORMAL),

            // CRICKETS
            new VOC_INFO("CRCKT2.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),

            // WOOD DOOR OPEN/CLOSE
            new VOC_INFO("DRWOODO.VOC", PRI_ENVIRONMENT, -100, 100, 8000, VF_NORMAL),
            new VOC_INFO("DRWOODC.VOC", PRI_ENVIRONMENT, -100, 100, 8000, VF_NORMAL),

            // METAL DOOR OPEN/CLOSE
            new VOC_INFO("DRMETO.VOC", PRI_ENVIRONMENT, -100, 100, 8000, VF_NORMAL),
            new VOC_INFO("DRMETC.VOC", PRI_ENVIRONMENT, -100, 100, 8000, VF_NORMAL),

            // SLIDING DOOR OPEN/CLOSE
            new VOC_INFO("DRSLDO.VOC", PRI_ENVIRONMENT, -100, 100, 8000, VF_NORMAL),
            new VOC_INFO("DRSLDC.VOC", PRI_ENVIRONMENT, -100, 100, 8000, VF_NORMAL),

            // STONE SLIDING DOOR OPEN/CLOSE
            new VOC_INFO("DRSTNO.VOC", PRI_ENVIRONMENT, -100, 100, 8000, VF_NORMAL),
            new VOC_INFO("DRSTNC.VOC", PRI_ENVIRONMENT, -100, 100, 8000, VF_NORMAL),

            // SQUEAKY DOOR OPEN/CLOSE
            new VOC_INFO("DRSQKO.VOC", PRI_ENVIRONMENT, -100, 100, 8000, VF_NORMAL),
            new VOC_INFO("DRSQKC.VOC", PRI_ENVIRONMENT, -100, 100, 8000, VF_NORMAL),

            // GIANT DRILL MACHINE
            new VOC_INFO("DRILL.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),

            // AMBIENT WATER DRIPPING IN CAVE
            new VOC_INFO("CAVE1.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("CAVE2.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),

            new VOC_INFO("DRIP.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),

            // WATER FALL
            new VOC_INFO("WTRFAL1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),
            // THERE IS NO WTRFAL2 -- DELETED!!!
            new VOC_INFO("WTRFAL2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // WATER FLOWING
            new VOC_INFO("WTRFLW1.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),
            // THERE IS NO WTRFLW2 -- DELETED!!!
            new VOC_INFO("WTRFLW2.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),

            // ELEVATOR START/STOP
            new VOC_INFO("ELEV1.VOC", PRI_ENVIRONMENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // SMALL EXP
            new VOC_INFO("EXPSML.VOC", PRI_ENVIRONMENT, -200, 200, DIST_NORMAL, VF_NORMAL),

            // MEDIUM EXP
            new VOC_INFO("EXPMED.VOC", PRI_ENVIRONMENT, -200, 200, DIST_WIDE, VF_NORMAL),

            // LARGE EXP
            new VOC_INFO("EXPLRG.VOC", PRI_ENVIRONMENT, -200, 200, DIST_WIDE, VF_NORMAL),

            // HUGE EXP
            // new VOC_INFO("BIGEXP.VOC", DIGI_HUGEEXP, 184, PRI_ENVIRONMENT, -200, 200, 0,
            // DIST_WIDE, VF_NORMAL ),
            new VOC_INFO("NULL.VOC", PRI_ENVIRONMENT, -200, 200, DIST_WIDE, VF_NORMAL),

            // CRACKLING FIRE FOR CONTINUOUS BURN
            new VOC_INFO("FIRE1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // POWERFULL HIGH HEAT CONTINUOUS BURN
            new VOC_INFO("FIRE2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // SHOOTING FIREBALL FOR FIREBALL TRAP
            new VOC_INFO("FBALL1.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),

            // SHOOTING FIREBALL FOR FIREBALL TRAP
            new VOC_INFO("FIREBALL1.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),

            // SECTOR GEAR COG TURNING
            new VOC_INFO("GEAR1.VOC", PRI_ENVIRONMENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // AMBIENT GONG FOR USE IN TEMPLE/PALACE LEVELS
            new VOC_INFO("GONG.VOC", PRI_HI_PLAYERWEAP, -100, 100, 32336, VF_NORMAL),

            // AMBIENT LAVA FLOW
            new VOC_INFO("LAVAFLW1.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),

            // GENERIC SECTOR OBJECT MACHINE RUNNING
            new VOC_INFO("MACHN1.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),

            // AMBIENT MUD BUBBLES
            new VOC_INFO("MUD1.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),

            // AMBIENT EARTH QUAKE
            new VOC_INFO("QUAKE1.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),

            // YUCKY SEWER FLOW
            new VOC_INFO("SEWER1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // WATER SPLASHING
            // (USE FOR PLAYER/NPC'S JUMPING AROUND IN WATER),
            new VOC_INFO("SPLASH1.VOC", PRI_ENVIRONMENT, -100, 100, DIST_NORMAL, VF_NORMAL),

            // STEAM FLOW
            new VOC_INFO("STEAM1.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),

            // VOLCANIC STEAM VENT
            new VOC_INFO("VOLSTM1.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),

            // STOMPER THUD SECTOR OBJECT
            new VOC_INFO("STMPR.VOC", PRI_SECTOROBJ, -100, 100, DIST_NORMAL, VF_NORMAL),

            // SCARY AMBIENT SWAMP SOUNDS
            new VOC_INFO("SWAMP1.VOC", PRI_AMBIENT, -100, 100, DIST_NORMAL, VF_LOOP),

            // FLIP SWITCH
            new VOC_INFO("SWITCH1.VOC", PRI_ENVIRONMENT, -100, 100, DIST_NORMAL, VF_NORMAL),

            // FLIP LARGE SWITCH
            new VOC_INFO("SWITCH2.VOC", PRI_ENVIRONMENT, -100, 100, DIST_NORMAL, VF_NORMAL),

            // STONE SWITCH
            new VOC_INFO("SWITCH3.VOC", PRI_ENVIRONMENT, -100, 100, DIST_NORMAL, VF_NORMAL),

            // BREAKABLE GLASS SWITCH
            new VOC_INFO("SWITCH4.VOC", PRI_ENVIRONMENT, -100, 100, DIST_NORMAL, VF_NORMAL),

            // HUGE ECHOING SWITCH
            new VOC_INFO("SWITCH5.VOC", PRI_ENVIRONMENT, -100, 100, DIST_NORMAL, VF_NORMAL),

            // AMBIENT ROLLING THUNDER
            new VOC_INFO("THUNDR.VOC", PRI_AMBIENT, -200, 200, DIST_LEVELWIDE, VF_NORMAL),

            // TELEPORTER
            new VOC_INFO("TELPORT.VOC", PRI_ENVIRONMENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // UNDERWATER AMBIENCE
            new VOC_INFO("UNDRWTR.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // UNLOCK BIG LOCKED DOOR
            new VOC_INFO("UNLOCK.VOC", PRI_ENVIRONMENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // SQUEAKY VALVE TURNING
            new VOC_INFO("VALVE.VOC", PRI_ENVIRONMENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // SPOOKY ETHERAL VOID AMBIENCE
            // (NETHERWORLDLY SOUNDS),
            new VOC_INFO("VOID1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("VOID2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("VOID3.VOC", PRI_NPCWEAP, 0, 0, -8000, VF_NORMAL),
            new VOC_INFO("VOID4.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("VOID5.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // VOLCANIC ERUPTION
            new VOC_INFO("ERUPT.VOC", PRI_AMBIENT, 0, 0, DIST_MAXNORMAL, VF_LOOP),

            // VOLCANIC SIZZLING PROJECTILES FLYING THROUGH AIR
            new VOC_INFO("VOLPRJCT.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // LIGHT WIND AMBIENCE
            new VOC_INFO("WIND1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // STRONG BLOWING WIND AMBIENCE
            new VOC_INFO("WIND2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // BREAKING WOOD AMBIENCE
            new VOC_INFO("WOODBRK.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // BREAKING, TUMBLING STONES FALLING AMBIENCE
            new VOC_INFO("STONEBRK.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // ENGINE ROOM SOUND
            new VOC_INFO("ENGROOM1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("ENGROOM2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("ENGROOM3.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("ENGROOM4.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("ENGROOM5.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // BREAKING GLASS, LARGE WINDOW PANE
            new VOC_INFO("GLASS3.VOC", PRI_NPCDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),

            // MUSICAL STINGER
            new VOC_INFO("MUSSTING.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // HELICOPTER LIKE SOUND
            new VOC_INFO("HELI.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),

            // HUGE ECHOING HEART LIKE AMBIENCE
            new VOC_INFO("BIGHART.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // EERIE ETHERAL TYPE WIND
            new VOC_INFO("WIND4.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // SPOOKY SINE WAVE SOUND
            new VOC_INFO("SPOOKY1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            // SPOOKY SINE WAVE SOUND
            new VOC_INFO("DRILL1.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),

            // JET ENGINE
            new VOC_INFO("JET.VOC", PRI_AMBIENT, 0, 0, DIST_MAXNORMAL, VF_LOOP),

            // CERIMONIAL DRUM CHANT
            new VOC_INFO("DRUMCHNT.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),

            new VOC_INFO("FLY.VOC", PRI_MAX, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("UZICLK.VOC", PRI_MAX, 0, 0, DIST_NORMAL, VF_NORMAL),

            // !IMPORTANT! Make sure all player voices stay together
            new VOC_INFO("STICKY2R.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("UZI1R.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SHOTG1R.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BOLT1R.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BOLT1R.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BOLT1R.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("STICKY1R.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("", 0, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BADMAN04.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("COMEGET2.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GHOP07.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GOODDAY4.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("KILLU05.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NATURAL4.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NOHONOR6.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SAYON09.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TAKSAN1.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SNATCH01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("CHOTO7.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWPAIN05.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWPAIN03.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWPAIN07.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWPAIN22.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("SWYELL03.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWYELL05.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWYELL06.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("GRUNT06.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("NOWAY1.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("SWDIE02.VOC", PRI_PLAYERDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("AHSO5.VOC", PRI_PLAYERDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("", 0, 0, 0, DIST_NORMAL, VF_NORMAL),

            // #ifndef SW_SHAREWARE
            // was RAILB10.VOC
            new VOC_INFO("HSHOT1.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            // #else
            // new VOC_INFO("NULL.VOC", DIGI_NULL_RAILFIRE, 269, PRI_HI_PLAYERWEAP, 0, 0, 0,
            // DIST_NORMAL, VF_NORMAL ),
            // #endif
            new VOC_INFO("RAIL2.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("RAILUP09.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("HBOMB2.VOC", PRI_MAX, 0, 0, DIST_LEVELWIDE, VF_NORMAL),
            new VOC_INFO("STANDBY.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CDOWN.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SYSREAD.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("HISS1.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("CHBNCE1.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("THROW.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("PULL.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("STSCAN2.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HBDOWN1.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            // new VOC_INFO("TOOLUSE1.VOC", DIGI_TOOLBOX, 282, PRI_LOW_PLAYERWEAP, 0, 0, 0,
            // DIST_NORMAL, VF_NORMAL ),
            new VOC_INFO("NULL.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GASPOP.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("40MMBNCE.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BURGALRM.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("CARALRM2.VOC", PRI_LOW_PLAYERWEAP, 0, 0, 25000, VF_NORMAL),
            new VOC_INFO("CAOFF1.VOC", PRI_LOW_PLAYERWEAP, 0, 0, 25000, VF_NORMAL),
            new VOC_INFO("TACK1.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NVON3.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NVOFF2.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SGSH01.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SKID3.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CRASH4.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BUS1.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BIMP01.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("ASIREN1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("FIRETRK1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TRAFFIC1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TRAFFIC2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TRAFFIC3.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TRAFFIC4.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TRAFFIC5.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TRAFFIC6.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HELI1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JET1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MOTO1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MOTO2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NEON1.VOC", PRI_AMBIENT, 0, 0, -8000, VF_NORMAL),
            new VOC_INFO("SUBWAY1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TRAINS01.VOC", PRI_PLAYERDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("COIN.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWRDSMK1.VOC", PRI_HI_PLAYERWEAP, -200, 200, DIST_NORMAL, VF_NORMAL),
            // RIPPER2
            new VOC_INFO("RIP2AMB.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIP2ALRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIP2ATCK.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIP2PAIN.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIP2SCRM.VOC", PRI_NPCDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIP2HRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("M60.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),

            // SUMO
            new VOC_INFO("SUMSCRM.VOC", PRI_MAX, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SUMALRT.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SUMAMB.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SUMPAIN.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),

            // UNLOCK RAM LOCKED DOOR
            new VOC_INFO("RAMLOCK.VOC", PRI_ENVIRONMENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            // UNLOCK CARD LOCKED DOOR
            new VOC_INFO("CARDLOCK.VOC", PRI_ENVIRONMENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // More player voices
            new VOC_INFO("ACS10.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("AMDRIV01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BABOON03.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BBURN04.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BIGWPN01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CBUNG01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CHARAD09.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("DTIME.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("EAT02.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("FCRACK01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HCOW03.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HCOW06.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HSHIT03.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HSHIT04.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("IHOPE01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ILIKE01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("KUNGFU06.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LMOVE01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LWANG05.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RAW01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("STUPID01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TDICK02.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TOURN01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("WWANG11.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("YAK02.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("REFLEX08.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("EVADE01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MFLY03.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SHISEI03.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("FWORKS01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HIRO03.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NAGA06.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("PEARL03.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("IAM01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LIKNUK01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LIKSRD01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LIKSHK02.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LUCK06.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MCHAN01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RCHAN13.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MUSIC03.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NODIFF07.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NOFEAR01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("NOPAIN.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("REPMAN15.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SOB15.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("WEAK03.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SPEED04.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("STIFF01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TOMB05.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TSTICK01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TSTICK05.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TSTICK07.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TSTICK10.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TSWORD05.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TSWORD08.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TSWORD01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWYELL22.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWYELL14.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWYELL23.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWYELL16.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SWYELL13.VOC", PRI_PLAYERVOICE, -200, 200, DIST_NORMAL, VF_NORMAL),

            // TOILETGIRL
            new VOC_INFO("TGSCRM.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TGALRT.VOC", PRI_NPCATTACK, -300, 300, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TGAMB.VOC", PRI_NPCAMBIENT, -300, 300, DIST_NORMAL,
                    VF_NORMAL),
            new VOC_INFO("TGPAIN.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TGTNT1.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TGTNT2.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            // MORE SUMO
            new VOC_INFO("SUMOFART.VOC", PRI_PLAYERVOICE, -100, 100, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("GIBS04.VOC", PRI_LOW_PLAYERWEAP, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GIBS05.VOC", PRI_LOW_PLAYERWEAP, -200, 200, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("BIRDS01.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BIRDS02.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TOILET01.VOC", PRI_AMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("FLIDLE.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("FLRUN01.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TOYCAR03.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("PRESS03.VOC", PRI_AMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("COMPON01.VOC", PRI_LOW_PLAYERWEAP, -200, 200, DIST_NORMAL,
                    VF_NORMAL),
            new VOC_INFO("TURBON01.VOC", PRI_LOW_PLAYERWEAP, -200, 200, DIST_NORMAL,
                    VF_NORMAL),
            new VOC_INFO("TURBRN01.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("BIGDRL03.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("FLUOR01.VOC", PRI_AMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("AMOEBA03.VOC", PRI_SECTOROBJ, -200, 200, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("BODY6.VOC", PRI_PLAYERDEATH, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("GIBS01.VOC", PRI_LOW_PLAYERWEAP, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("CHOK01.VOC", PRI_NPCDEATH, -200, 200, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("TRAIN3.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TRAINR02.VOC", PRI_PLAYERDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TRAIN8.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TCLID01.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            // player voices
            new VOC_INFO("ACCU01.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("AHH03.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),

            // Pachinko
            new VOC_INFO("PALARM1.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("PFLIP4.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("PROLL1.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("PROLL2.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("PROLL3.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),

            // BUNNY
            new VOC_INFO("RABATK1.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RABDIE1.VOC", PRI_NPCDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RABDIE2.VOC", PRI_NPCDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RABDIE3.VOC", PRI_NPCDEATH, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RABAMB.VOC", PRI_NPCAMBIENT, -100, 100, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("STONE2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("NINCUT3.VOC", PRI_NPCDEATH, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("RIPCHST1.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("WHIPIN2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("ENDLEV3.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MDALARM1.VOC", PRI_PLAYERDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("METALBRK.VOC", PRI_AMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("DEBRIBRK.VOC", PRI_AMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MARBELS.VOC", PRI_AMBIENT, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BANZAI1.VOC", PRI_PLAYERDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HAHA19.VOC", PRI_PLAYERDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HAHA11.VOC", PRI_PLAYERDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("HAHA15.VOC", PRI_PLAYERDEATH, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TELEPT02.VOC", PRI_ITEM, 0, 0, DIST_NORMAL, VF_NORMAL),

            // More Player Voices
            new VOC_INFO("JG1075.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG1082.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG1087.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG1088.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG1103.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2000.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2005.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2020.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2032.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2053.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2054.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2045.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2087.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2074.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2075.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2078.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG3005.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG3017.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG3047.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG3022.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG6053.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG3059.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG4012.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG3070.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG6051.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG4002.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG4024.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG5042.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG5049A.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("WDOOR02.VOC", PRI_SECTOROBJ, -200, 200, 8000, VF_NORMAL),
            new VOC_INFO("MDOOR03.VOC", PRI_SECTOROBJ, -200, 200, 8000, VF_NORMAL),
            new VOC_INFO("603981_1.VOC", PRI_SECTOROBJ, -200, 200, 8000, VF_NORMAL),
            new VOC_INFO("FLAG03.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),

            new VOC_INFO("JG7009.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG7001.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG2001.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("ACHCRY04.VOC", PRI_SECTOROBJ, 0, 0, -8000, VF_LOOP),
            new VOC_INFO("ACHS010.VOC", PRI_SECTOROBJ, 0, 0, -8000, VF_NORMAL),
            new VOC_INFO("ACHT1006.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ACHS016.VOC", PRI_SECTOROBJ, 0, 0, -8000, VF_NORMAL),
            new VOC_INFO("ACHT120A.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("TELEPT02.VOC", PRI_LOW_PLAYERWEAP, 0, 0, DIST_NORMAL,
                    VF_NORMAL),
            new VOC_INFO("SLASH1.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("WARNING.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("WHIRL1.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("ACHF1003.VOC", PRI_NPCAMBIENT, -300, 300, -16000, VF_NORMAL),
            new VOC_INFO("ACHF1002.VOC", PRI_NPCAMBIENT, -300, 300, -16000, VF_NORMAL),
            new VOC_INFO("ACHF1016.VOC", PRI_NPCAMBIENT, -300, 300, -16000, VF_NORMAL),

            new VOC_INFO("CHIMES4.VOC", PRI_SECTOROBJ, 0, 0, -8000, VF_LOOP),

            // Yet more LoWang voices
            new VOC_INFO("JGB023.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JGB020.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JGB080.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JGB106.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JGB130.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JGSB4.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JGB166.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JGB156.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JGB193.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JGB202.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JGB340A.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("JGEN06.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("MSG9.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("UGLY1A.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("UGLY1B.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("TELEPT07.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("GOROSW1.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BTCREAK2.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("BTRUN05.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("BTIDLE4.VOC", PRI_SECTOROBJ, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("SHIPBELL.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("SHIPHRN1.VOC", PRI_AMBIENT, 0, 0, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("EXP3.VOC", PRI_HI_PLAYERWEAP, 0, 0, DIST_WIDE, VF_NORMAL),

            // Yet even more player voices!
            new VOC_INFO("JG41001.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG41012.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG41018.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG41028.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG41048.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG41052.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG41058.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG41060.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG41075.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG42004.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG42019.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG42021.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG42028.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG42033.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG42034.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG42050.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG42056.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG42061.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG43004.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG43015.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG43019.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG43021.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG44011.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG44014.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG44027.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG44038.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG44039.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG44048.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG44052.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG45014.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG44068.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG45010.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG45018.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG45030.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG45033.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG45043.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG45053.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG45067.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG46005.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG46010.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),

            // And the girls talk back to Wang
            // Car Girl
            new VOC_INFO("LANI049.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI051.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI052.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI054.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),

            // Sailor Moon
            new VOC_INFO("LANI060.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI063.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI065.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI066.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),

            // Mechanic
            new VOC_INFO("LANI073.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI075.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI077.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI079.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),

            // Prune
            new VOC_INFO("LANI089.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI091.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI093.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("LANI095.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),

            new VOC_INFO("AD5.VOC", PRI_ENVIRONMENT, -300, 300, 8000, VF_NORMAL),
            new VOC_INFO("AD6.VOC", PRI_ENVIRONMENT, -300, 300, 8000, VF_NORMAL),
            new VOC_INFO("JET05.VOC", PRI_SECTOROBJ, -100, 100, 8000, VF_NORMAL),
            new VOC_INFO("VC04.VOC", PRI_SECTOROBJ, -100, 100, DIST_NORMAL, VF_LOOP),

            // GIRL NINJA
            new VOC_INFO("LANI017.VOC", PRI_NPCATTACK, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LANI033.VOC", PRI_NPCDEATH, -200, 200, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("LANI001.VOC", PRI_NPCATTACK, -200, 200, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("CACKLE.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("CACKLE2.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),
            new VOC_INFO("CACKLE3.VOC", PRI_NPCAMBIENT, -200, 200, -16000, VF_NORMAL),

            new VOC_INFO("SUMO058.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ELEV01.VOC", PRI_SECTOROBJ, -100, 100, DIST_NORMAL, VF_NORMAL),

            new VOC_INFO("JG9009.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("Z16004.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("Z16012.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("Z16022.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("Z16027.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG93030.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG94002.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("Z17010.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("Z17052.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("Z17025.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ML25014.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ML250101.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG9022.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG9032.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG9038.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG9055.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG9060.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG92055.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ML25032.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG92036.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG92042.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ML26001.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG93000.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG93011.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG93018.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG93023.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ML26008.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ML26011.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG94007.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG94024.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG94039.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("JG95012.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ZWALK5.VOC", PRI_NPCATTACK, -100, 100, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ZC1.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ZC2.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ZC3.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ZC4.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ZC5.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ZC6.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ZC7.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_LOOP),
            new VOC_INFO("ZC8.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("ZC9.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL),
            new VOC_INFO("Z16043.VOC", PRI_PLAYERVOICE, 0, 0, DIST_NORMAL, VF_NORMAL)};
}
