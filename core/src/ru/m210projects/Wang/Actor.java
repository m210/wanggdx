package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Type.Animator;
import ru.m210projects.Wang.Type.Sect_User;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Ninja.s_NinjaDieSliced;
import static ru.m210projects.Wang.Game.tmp_ptr;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Rooms.*;
import static ru.m210projects.Wang.Sector.getSectUser;
import static ru.m210projects.Wang.Shrap.SpawnShrap;
import static ru.m210projects.Wang.Sound.PlaySound;
import static ru.m210projects.Wang.Sound.v3df_none;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Weapon.*;

public class Actor {

    public static final Animator DoActorDebris = new Animator((Animator.Runnable) Actor::DoActorDebris);

    public static void DoScaleSprite(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (u.scale_speed != 0) {
            u.scale_value += u.scale_speed * ACTORMOVETICS;

            int scale_value = u.scale_value >> 8;
            if (u.scale_speed > 0) {
                if (scale_value > u.scale_tgt) {
                    u.scale_speed = 0;
                } else {
                    sp.setXrepeat(scale_value);
                    sp.setYrepeat(scale_value);
                }
            } else {
                if (scale_value < u.scale_tgt) {
                    u.scale_speed = 0;
                } else {
                    sp.setXrepeat(scale_value);
                    sp.setYrepeat(scale_value);
                }
            }

        }

    }

    public static void DoActorDie(final int SpriteNum, final int weapon) {
        final USER u = getUser(SpriteNum);
        final Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        change_sprite_stat(SpriteNum, STAT_DEAD_ACTOR);
        u.Flags |= (SPR_DEAD);
        u.Flags &= ~(SPR_FALLING | SPR_JUMPING);
        u.floor_dist =  Z(40);

        // test for gibable dead bodies
        sp.setExtra(sp.getExtra() | (SPRX_BREAKABLE));
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BREAKABLE));

        if (weapon < 0) {
            // killed by one of these non-sprites
            switch (weapon) {
                case WPN_NM_LAVA:
                case WPN_NM_SECTOR_SQUISH:
                    ChangeState(SpriteNum, u.StateEnd);
                    u.RotNum = 0;
                    break;
            }

            return;
        }

        Sprite wp = boardService.getSprite(weapon);
        USER wu = getUser(weapon);
        if (wp == null || wu == null) {
            return;
        }

        Sprite targetSpr = boardService.getSprite(u.tgt_sp);

        // killed by one of these sprites
        switch (wu.ID) {
            // Coolie actually explodes himself
            // he is the Sprite AND Weapon
            case COOLIE_RUN_R0:
                ChangeState(SpriteNum, u.StateEnd);
                u.RotNum = 0;
                sp.setXvel(sp.getXvel() << 1);
                u.ActorActionFunc = null;
                sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
                break;

            case NINJA_RUN_R0:
                if (u.ID == NINJA_RUN_R0) // Cut in half!
                {
                    if (wu.WeaponNum != WPN_FIST) {
                        InitPlasmaFountain(wp, SpriteNum);
                        InitPlasmaFountain(wp, SpriteNum);
                        PlaySound(DIGI_NINJAINHALF, sp, v3df_none);
                        ChangeState(SpriteNum, s_NinjaDieSliced[0]);
                    } else {
                        if (RANDOM_RANGE(1000) > 500) {
                            InitPlasmaFountain(boardService.getSprite(weapon), SpriteNum);
                        }

                        ChangeState(SpriteNum, u.StateEnd);
                        u.RotNum = 0;
                        u.ActorActionFunc = null;
                        sp.setXvel( (200 + RANDOM_RANGE(200)));
                        u.jump_speed =  (-200 - RANDOM_RANGE(250));
                        DoActorBeginJump(SpriteNum);
                        sp.setAng(wp.getAng());
                    }
                } else {
                    // test for gibable dead bodies
                    if (RANDOM_RANGE(1000) > 500) {
                        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YFLIP));
                    }
                    ChangeState(SpriteNum, u.StateEnd);
                    sp.setXvel(0);
                    u.jump_speed = 0;
                    DoActorBeginJump(SpriteNum);
                }

                u.RotNum = 0;
                u.ActorActionFunc = null;
                sp.setAng(wp.getAng());
                break;

            case COOLG_RUN_R0:
            case SKEL_RUN_R0:
            case RIPPER_RUN_R0:
            case RIPPER2_RUN_R0:
            case EEL_RUN_R0:
            case STAR1:
            case SUMO_RUN_R0:
                ChangeState(SpriteNum, u.StateEnd);
                u.RotNum = 0;
                break;

            case UZI_SMOKE:
                if (RANDOM_RANGE(1000) > 500) {
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YFLIP));
                }
                ChangeState(SpriteNum, u.StateEnd);
                u.RotNum = 0;
                // Rippers still gotta jump or they fall off walls weird
                if (u.ID == RIPPER_RUN_R0 || u.ID == RIPPER2_RUN_R0) {
                    sp.setXvel(sp.getXvel() << 1);
                    u.jump_speed =  (-100 - RANDOM_RANGE(250));
                    DoActorBeginJump(SpriteNum);
                } else {
                    sp.setXvel(0);
                    u.jump_speed =  (-10 - RANDOM_RANGE(25));
                    DoActorBeginJump(SpriteNum);
                }
                u.ActorActionFunc = null;

                if (targetSpr != null) {
                    // Get angle to player
                    sp.setAng(NORM_ANGLE(EngineUtils.getAngle(targetSpr.getX() - sp.getX(), targetSpr.getY() - sp.getY()) + 1024));
                }
                break;

            case UZI_SMOKE + 1: // Shotgun
                if (RANDOM_RANGE(1000) > 500) {
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YFLIP));
                }
                ChangeState(SpriteNum, u.StateEnd);
                u.RotNum = 0;

                // Rippers still gotta jump or they fall off walls weird
                if (u.ID == RIPPER_RUN_R0 || u.ID == RIPPER2_RUN_R0) {
                    sp.setXvel( (75 + RANDOM_RANGE(100)));
                    u.jump_speed =  (-100 - RANDOM_RANGE(150));
                } else {
                    sp.setXvel( (100 + RANDOM_RANGE(200)));
                    u.jump_speed =  (-100 - RANDOM_RANGE(250));
                }
                DoActorBeginJump(SpriteNum);
                u.ActorActionFunc = null;

                if (targetSpr != null) {
                    // Get angle to player
                    sp.setAng(NORM_ANGLE(EngineUtils.getAngle(targetSpr.getX() - sp.getX(), targetSpr.getY() - sp.getY()) + 1024));

                }
                break;
            default:
                switch (u.ID) {
                    case SKULL_R0:
                    case BETTY_R0:
                        ChangeState(SpriteNum, u.StateEnd);
                        break;

                    default:
                        if (RANDOM_RANGE(1000) > 700) {
                            InitPlasmaFountain(boardService.getSprite(weapon), SpriteNum);
                        }

                        if (RANDOM_RANGE(1000) > 500) {
                            sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YFLIP));
                        }
                        ChangeState(SpriteNum, u.StateEnd);
                        u.RotNum = 0;
                        u.ActorActionFunc = null;
                        sp.setXvel( (300 + RANDOM_RANGE(400)));
                        u.jump_speed =  (-300 - RANDOM_RANGE(350));
                        DoActorBeginJump(SpriteNum);
                        sp.setAng(wp.getAng());
                        break;
                }
                break;
        }

        // These are too big to flip upside down
        switch (u.ID) {
            case RIPPER2_RUN_R0:
            case COOLIE_RUN_R0:
            case SUMO_RUN_R0:
            case ZILLA_RUN_R0:
                sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YFLIP));
                break;
        }

        u.ID = 0;
    }

    public static void DoDebrisCurrent(int spnum) {
        Sprite sp = boardService.getSprite(spnum);
        USER u = getUser(spnum);
        if (sp == null || u == null) {
            return;
        }

        Sect_User sectu = getSectUser(sp.getSectnum());
        if (sectu == null) {
            return;
        }

        int nx = (int) (DIV4(sectu.speed) * (long) EngineUtils.sin(NORM_ANGLE(sectu.ang + 512)) >> 14);
        int ny = (int) (DIV4(sectu.speed) * (long) EngineUtils.sin(sectu.ang) >> 14);

        // faster than move_sprite
        // move_missile(sp-sprite, nx, ny, 0, Z(2), Z(0), 0, ACTORMOVETICS);
        int ret = move_sprite(spnum, nx, ny, 0, u.ceiling_dist, u.floor_dist, 0, ACTORMOVETICS);

        // attempt to move away from wall
        if (ret != 0) {
            int rang =  RANDOM_P2(2048);

            nx = (int) (DIV4(sectu.speed) * (long) EngineUtils.sin(NORM_ANGLE(sectu.ang + rang + 512)) >> 14);
            ny = (int) (DIV4(sectu.speed) * (long) EngineUtils.sin(NORM_ANGLE(sectu.ang + rang)) >> 14);

            move_sprite(spnum, nx, ny, 0, u.ceiling_dist, u.floor_dist, 0, ACTORMOVETICS);
        }

        sp.setZ(u.loz);
    }

    public static boolean DoActorSectorDamage(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        Sector sectp = boardService.getSector(sp.getSectnum());
        Sect_User sectu = getSectUser(sp.getSectnum());

        if (u.Health <= 0) {
            return (false);
        }

        if (sectu != null && sectu.damage != 0) {
            if (TEST(sectu.flags, SECTFU_DAMAGE_ABOVE_SECTOR)) {
                if ((u.DamageTics -= synctics) < 0) {
                    u.DamageTics = 60;
                    u.Health -= sectu.damage;

                    if (u.Health <= 0) {
                        UpdateSinglePlayKills(SpriteNum, null);
                        DoActorDie(SpriteNum, WPN_NM_LAVA);
                        return (true);
                    }
                }
            } else if (sectp != null && SPRITEp_BOS(sp) >= sectp.getFloorz()) {
                if ((u.DamageTics -= synctics) < 0) {
                    u.DamageTics = 60;
                    u.Health -= sectu.damage;

                    if (u.Health <= 0) {
                        UpdateSinglePlayKills(SpriteNum, null);
                        DoActorDie(SpriteNum, WPN_NM_LAVA);
                        return (true);
                    }
                }
            }
        }

        // note that most squishing is done in vator.c
        if (u.lo_sectp != -1 && u.hi_sectp != -1 && klabs(u.loz - u.hiz) < DIV2(SPRITE_SIZE_Z(SpriteNum))) {
            u.Health = 0;
            if (SpawnShrap(SpriteNum, WPN_NM_SECTOR_SQUISH)) {
                UpdateSinglePlayKills(SpriteNum, null);
                SetSuicide(SpriteNum);
            }

            return (true);
        }

        return (false);
    }

    // !AIC - Supposed to allow floating of DEBRIS (dead bodies, flotsam, jetsam).
    // Or if water has
    // current move with the current.

    private static boolean move_debris(int SpriteNum, int xchange, int ychange) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return false;
        }

        u.moveSpriteReturn = move_sprite(SpriteNum, xchange, ychange, 0, u.ceiling_dist, u.floor_dist, 0, ACTORMOVETICS);
        return (u.moveSpriteReturn == 0);
    }

    public static void DoActorDebris(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sector sectp = boardService.getSector(sp.getSectnum());

        // This was move from DoActorDie so actor's can't be walked through until they
        // are on the floor
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        // Don't let some actors float
        switch (u.ID) {
            case HORNET_RUN_R0:
            case BUNNY_RUN_R0:
                KillSprite(SpriteNum);
                return;
            case ZILLA_RUN_R0:
                engine.getzsofslope(sp.getSectnum(), sp.getX(), sp.getY(), fz, cz);
                u.hiz = cz.get();
                u.loz = fz.get();

                u.lo_sectp = sp.getSectnum();
                u.hi_sectp = sp.getSectnum();
                u.lo_sp = -1;
                u.hi_sp = -1;
                break;
        }

        if (sectp != null && TEST(sectp.getExtra(), SECTFX_SINK)) {
            if (TEST(sectp.getExtra(), SECTFX_CURRENT)) {
                DoDebrisCurrent(SpriteNum);
            } else {
                int nx = ACTORMOVETICS * EngineUtils.cos(sp.getAng()) >> 14;
                int ny = ACTORMOVETICS * EngineUtils.sin(sp.getAng()) >> 14;

                if (!move_debris(SpriteNum, nx, ny)) {
                    sp.setAng( RANDOM_P2(2048));
                }
            }

            Sect_User su = getSectUser(sp.getSectnum());
            if (su != null && su.depth > 10) {
                u.WaitTics =  ((u.WaitTics + (ACTORMOVETICS << 3)) & 1023);
                sp.setZ(u.loz - ((Z(2) * EngineUtils.sin(u.WaitTics)) >> 14));
            }
        } else {
            sp.setZ(u.loz);
        }
    }

    public static void DoFireFly(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int nx = 4 * ACTORMOVETICS * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        int ny = 4 * ACTORMOVETICS * EngineUtils.sin(sp.getAng()) >> 14;

        sp.setClipdist(256 >> 2);
        if (!move_actor(SpriteNum, nx, ny, 0)) {
            sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
        }

        u.WaitTics =  ((u.WaitTics + (ACTORMOVETICS << 1)) & 2047);

        sp.setZ(u.sz + ((Z(32) * EngineUtils.sin(u.WaitTics)) >> 14));
    }

    // !AIC - Tries to keep actors correctly on the floor. More that a bit messy.

    public static void KeepActorOnFloor(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sector sectp = boardService.getSector(sp.getSectnum());

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YFLIP)); // If upside down, reset it

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            return;
        }

        int depth = 0;
        Sect_User losu = getSectUser(u.lo_sectp);
        if (u.lo_sectp != -1 && losu != null) {
            depth = losu.depth;
        }

        if (sectp != null && TEST(sectp.getExtra(), SECTFX_SINK) && depth > 35 && u.ActorActionSet != null && u.ActorActionSet.Swim != null) {
            if (TEST(u.Flags, SPR_SWIMMING)) {
                if (u.getRot() != u.ActorActionSet.Run && u.getRot() != u.ActorActionSet.Swim && u.getRot() != u.ActorActionSet.Stand) {
                    // was swimming but have now stopped
                    u.Flags &= ~(SPR_SWIMMING);
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
                    u.oz = u.loz;
                    sp.setZ(u.loz);
                    return;
                }

                if (u.getRot() == u.ActorActionSet.Run) {
                    NewStateGroup(SpriteNum, u.ActorActionSet.Swim);
                }

                // are swimming
                u.oz = (u.loz - Z(depth));
                sp.setZ(u.oz);
            } else {
                // only start swimming if you are running
                if (u.getRot() == u.ActorActionSet.Run || u.getRot() == u.ActorActionSet.Swim) {
                    NewStateGroup(SpriteNum, u.ActorActionSet.Swim);
                    u.oz = u.loz - Z(depth);
                    sp.setZ(u.oz);
                    u.Flags |= (SPR_SWIMMING);
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YCENTER));
                } else {
                    u.Flags &= ~(SPR_SWIMMING);
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
                    u.oz = u.loz;
                    sp.setZ(u.oz);
                }
            }

            return;
        }

        // NOT in a swimming situation
        u.Flags &= ~(SPR_SWIMMING);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));

        if (TEST(u.Flags, SPR_MOVED)) {
            u.oz = (u.loz);
        } else {
            FAFgetzrangepoint(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(), tmp_ptr[0], tmp_ptr[1], tmp_ptr[2].set(0), tmp_ptr[3]);
            u.oz = (tmp_ptr[2].value);
        }
        sp.setZ(u.oz);
    }

    public static void DoActorBeginSlide(int SpriteNum, int ang, int vel, int dec) {
        USER u = getUser(SpriteNum);
        if (u != null) {
            u.Flags |= (SPR_SLIDING);
            u.slide_ang = ang;
            u.slide_vel = vel;
            u.slide_dec = dec;
        }

    }

    // !AIC - Sliding can occur in different directions from movement of the actor.
    // Has its own set of variables

    public static boolean DoActorSlide(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return false;
        }

        int nx = u.slide_vel * EngineUtils.sin(NORM_ANGLE(u.slide_ang + 512)) >> 14;
        int ny = u.slide_vel * EngineUtils.sin(u.slide_ang) >> 14;

        if (!move_actor(SpriteNum, nx, ny, 0)) {
            u.Flags &= ~(SPR_SLIDING);
            return (false);
        }

        u.slide_vel -= u.slide_dec * ACTORMOVETICS;

        if (u.slide_vel < 20) {
            u.Flags &= ~(SPR_SLIDING);
        }

        return (true);
    }

    // !AIC - Actor jumping and falling

    public static void DoActorBeginJump(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.Flags |= (SPR_JUMPING);
        u.Flags &= ~(SPR_FALLING);

        // u.jump_speed = should be set before calling

        // set up individual actor jump gravity
        u.jump_grav = ACTOR_GRAVITY;

        // Change sprites state to jumping
        if (u.ActorActionSet != null) {
            if (TEST(u.Flags, SPR_DEAD)) {
                NewStateGroup(SpriteNum, u.ActorActionSet.DeathJump);
            } else {
                NewStateGroup(SpriteNum, u.ActorActionSet.Jump);
            }
        }
        u.StateFallOverride = null;

        // DO NOT CALL DoActorJump! DoActorStopFall can cause an infinite loop and
        // stack overflow if it is called.
        // DoActorJump(SpriteNum);
    }

    public static void DoActorJump(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // precalculate jump value to adjust jump speed by
        int jump_adj = u.jump_grav * ACTORMOVETICS;

        // adjust jump speed by gravity - if jump speed greater than 0 player
        // have started falling
        if ((u.jump_speed += jump_adj) > 0) {
            // Start falling
            DoActorBeginFall(SpriteNum);
            return;
        }

        // adjust height by jump speed
        sp.setZ(sp.getZ() + u.jump_speed * ACTORMOVETICS);

        // if player gets to close the ceiling while jumping
        if (sp.getZ() < u.hiz + Z(PIC_SIZY(SpriteNum))) {
            // put player at the ceiling
            sp.setZ(u.hiz + Z(PIC_SIZY(SpriteNum)));

            // reverse your speed to falling
            u.jump_speed = -u.jump_speed;

            // Change sprites state to falling
            DoActorBeginFall(SpriteNum);
        }
    }

    public static void DoActorBeginFall(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.Flags |= (SPR_FALLING);
        u.Flags &= ~(SPR_JUMPING);

        u.jump_grav = ACTOR_GRAVITY;

        // Change sprites state to falling
        if (u.ActorActionSet != null) {
            if (TEST(u.Flags, SPR_DEAD)) {
                NewStateGroup(SpriteNum, u.ActorActionSet.DeathFall);
            } else {
                NewStateGroup(SpriteNum, u.ActorActionSet.Fall);
            }

            if (u.StateFallOverride != null) {
                NewStateGroup(SpriteNum, u.StateFallOverride);
            }
        }

        DoActorFall(SpriteNum);
    }

    public static void DoActorFall(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // adjust jump speed by gravity
        u.jump_speed += u.jump_grav * ACTORMOVETICS;

        // adjust player height by jump speed
        sp.setZ(sp.getZ() + u.jump_speed * ACTORMOVETICS);

        // Stick like glue when you hit the ground
        if (sp.getZ() > u.loz) {
            DoActorStopFall(SpriteNum);
        }
    }

    public static void DoActorStopFall(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setZ(u.loz);
        u.Flags &= ~(SPR_FALLING | SPR_JUMPING);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YFLIP));

        Sprite loSpr = boardService.getSprite(u.lo_sp);
        // don't stand on face or wall sprites - jump again
        if (loSpr != null && !TEST(loSpr.getCstat(), CSTAT_SPRITE_FLOOR)) {
            // sp.ang = NORM_ANGLE(sp.ang + (RANDOM_P2(64<<8)>>8) - 32);
            sp.setAng(NORM_ANGLE(sp.getAng() + 1024 + (RANDOM_P2(512 << 8) >> 8)));
            u.jump_speed = -350;

            DoActorBeginJump(SpriteNum);
            return;
        }

        // Change sprites state to running
        if (u.ActorActionSet != null) {
            if (TEST(u.Flags, SPR_DEAD)) {
                NewStateGroup(SpriteNum, u.ActorActionSet.Dead);
                PlaySound(DIGI_ACTORBODYFALL1, sp, v3df_none);
            } else {
                PlaySound(DIGI_ACTORHITGROUND, sp, v3df_none);

                NewStateGroup(SpriteNum, u.ActorActionSet.Run);

                if ((u.track >= 0) && (u.jump_speed) > 800 && (u.ActorActionSet.Sit != null)) {
                    u.WaitTics = 80;
                    NewStateGroup(SpriteNum, u.ActorActionSet.Sit);
                }
            }
        }
    }

    public static void DoActorDeathMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoActorJump(SpriteNum);
            } else {
                DoActorFall(SpriteNum);
            }
        }

        int nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        int ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

        sp.setClipdist((128 + 64) >> 2);
        move_actor(SpriteNum, nx, ny, 0);

        // only fall on top of floor sprite or sector
        DoFindGroundPoint(SpriteNum);
    }

    // !AIC - Jumping a falling for shrapnel and other stuff, not actors.

    public static void DoBeginJump(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.Flags |= (SPR_JUMPING);
        u.Flags &= ~(SPR_FALLING);

        // set up individual actor jump gravity
        u.jump_grav = ACTOR_GRAVITY;

        DoJump(SpriteNum);
    }

    public static void DoJump(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // precalculate jump value to adjust jump speed by
        int jump_adj = u.jump_grav * ACTORMOVETICS;

        // adjust jump speed by gravity - if jump speed greater than 0 player
        // have started falling
        if ((u.jump_speed += jump_adj) > 0) {
            // Start falling
            DoBeginFall(SpriteNum);
            return;
        }

        // adjust height by jump speed
        sp.setZ(sp.getZ() + u.jump_speed * ACTORMOVETICS);

        // if player gets to close the ceiling while jumping
        if (sp.getZ() < u.hiz + Z(PIC_SIZY(SpriteNum))) {
            // put player at the ceiling
            sp.setZ(u.hiz + Z(PIC_SIZY(SpriteNum)));

            // reverse your speed to falling
            u.jump_speed =  -u.jump_speed;

            // Change sprites state to falling
            DoBeginFall(SpriteNum);
        }
    }

    public static void DoBeginFall(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.Flags |= (SPR_FALLING);
        u.Flags &= ~(SPR_JUMPING);

        u.jump_grav = ACTOR_GRAVITY;

        DoFall(SpriteNum);
    }

    public static void DoFall(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // adjust jump speed by gravity
        u.jump_speed += u.jump_grav * ACTORMOVETICS;

        // adjust player height by jump speed
        sp.setZ(sp.getZ() + u.jump_speed * ACTORMOVETICS);

        // Stick like glue when you hit the ground
        if (sp.getZ() > u.loz - u.floor_dist) {
            sp.setZ(u.loz - u.floor_dist);
            u.Flags &= ~(SPR_FALLING);
        }
    }

}
