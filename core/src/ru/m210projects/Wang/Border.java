package ru.m210projects.Wang;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Wang.Type.PlayerStr;

import static ru.m210projects.Build.Pragmas.scale;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Main.cfg;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Panel.BORDER_BAR;
import static ru.m210projects.Wang.Panel.BORDER_NONE;

public class Border {

    public static void SetBorder(PlayerStr pp, int value) {
        if (pp != Player[myconnectindex]) {
            return;
        }

        if (value >= 0) // just refresh
        {
            cfg.BorderNum = value;
        }

        if (cfg.BorderNum < BORDER_NONE) {
            cfg.BorderNum = BORDER_NONE;
        }

        if (cfg.BorderNum > BORDER_BAR) {
            cfg.BorderNum = BORDER_BAR;
            return;
        }

        BorderSetView(cfg.BorderNum);
    }

    public static void BorderSetView(int ignoredSize) {
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        int x1 = 0; //scale(ss, xdim, 160);
        int x2 = xdim - x1;
        int y1 = 0; //3 * ss;
        int y2 = 200;

        y1 = scale(y1, ydim, 200);
        y2 = scale(y2, ydim, 200);

        renderer.setview(x1, y1, x2 - 1, y2 - 1);
    }
}
