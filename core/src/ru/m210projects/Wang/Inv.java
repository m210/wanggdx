package ru.m210projects.Wang;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Strhandler.Bitoa;
import static ru.m210projects.Build.Strhandler.buildString;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JWeapon.*;
import static ru.m210projects.Wang.Main.cfg;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Palette.DoPlayerDivePalette;
import static ru.m210projects.Wang.Palette.DoPlayerNightVisionPalette;
import static ru.m210projects.Wang.Panel.*;
import static ru.m210projects.Wang.Player.Player_Action_Func.*;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Text.*;
import static ru.m210projects.Wang.Type.MyTypes.*;

public class Inv {

    public static final int INVENTORY_MEDKIT = 0, INVENTORY_REPAIR_KIT = 1, INVENTORY_CLOAK = 2, // de-cloak when firing
            INVENTORY_NIGHT_VISION = 3, INVENTORY_CHEMBOMB = 4, INVENTORY_FLASHBOMB = 5, INVENTORY_CALTROPS = 6,
            MAX_INVENTORY = 7;
    public static final int INVENTORY_BOX_X = 231;
    public static final int INVENTORY_BOX_Y = (176 - 8);
    private static final int INVF_AUTO_USE = (BIT(0));
    private static final int INVF_TIMED = (BIT(1));
    private static final int INVF_COUNT = (BIT(2));
    // indexed by gs.BorderNum 130,172
    private static final short[] InventoryBarXpos = {110, 130, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80,
            80};
    private static final short[] InventoryBarYpos = {172, 172, 130, 130, 130, 130, 130, 130, 130, 130, 130, 130, 130,
            130, 130, 130, 130};
    private static final int INVENTORY_ICON_WIDTH = 28;
    private static final Inventory_Stop_Func StopInventoryCloak = Inv::StopInventoryCloak;
    private static final Inventory_Stop_Func StopInventoryNightVision = Inv::StopInventoryNightVision;
    public static final INVENTORY_DATA[] InventoryData = {
            new INVENTORY_DATA("PORTABLE MEDKIT", UseInventoryMedkit, null, ps_PanelMedkit, 0, 1, (1 << 16), 0),
            new INVENTORY_DATA("REPAIR KIT", null, null, ps_PanelRepairKit, 100, 1, (1 << 16), INVF_AUTO_USE),
            new INVENTORY_DATA("SMOKE BOMB", UseInventoryCloak, StopInventoryCloak, ps_PanelCloak, 4, 1, (1 << 16),
                    INVF_TIMED),
            new INVENTORY_DATA("NIGHT VISION", UseInventoryNightVision, StopInventoryNightVision, ps_PanelNightVision,
                    3, 1, (1 << 16), INVF_TIMED),
            new INVENTORY_DATA("GAS BOMB", UseInventoryChemBomb, null, ps_PanelChemBomb, 0, 1, (1 << 16), INVF_COUNT),
            new INVENTORY_DATA("FLASH BOMB", UseInventoryFlashBomb, null, ps_PanelFlashBomb, 0, 2, (1 << 16),
                    INVF_COUNT),
            new INVENTORY_DATA("CALTROPS", UseInventoryCaltrops, null, ps_PanelCaltrops, 0, 3, (1 << 16),
                    INVF_COUNT)};
    private static final int INVENTORY_PIC_XOFF = 1;
    private static final int INVENTORY_PIC_YOFF = 1;
    private static final int INVENTORY_PERCENT_XOFF = 19;
    private static final int INVENTORY_PERCENT_YOFF = 13;

    //////////////////////////////////////////////////////////////////////
    //
    // MEDKIT
    //
    //////////////////////////////////////////////////////////////////////
    private static final int INVENTORY_STATE_XOFF = 19;
    private static final int INVENTORY_STATE_YOFF = 1;

    //////////////////////////////////////////////////////////////////////
    //
    // CHEMICAL WARFARE CANISTERS
    //
    //////////////////////////////////////////////////////////////////////
    private static final char[] buffer = new char[32];

    //////////////////////////////////////////////////////////////////////
    //
    // FLASH BOMBS
    //
    //////////////////////////////////////////////////////////////////////
    private static int InventoryBoxX;

    //////////////////////////////////////////////////////////////////////
    //
    // CALTROPS
    //
    //////////////////////////////////////////////////////////////////////
    private static int InventoryBoxY;

    //////////////////////////////////////////////////////////////////////
    //
    // REPAIR KIT
    //
    //////////////////////////////////////////////////////////////////////

    public static void PanelInvTestSuicide(Panel_Sprite psp) {
        if (TEST(psp.flags, PANF_SUICIDE)) {
            pKillSprite(psp);
        }
    }

    //////////////////////////////////////////////////////////////////////
    //
    // CLOAK
    //
    //////////////////////////////////////////////////////////////////////

    public static Panel_Sprite SpawnInventoryIcon(PlayerStr pp, int InventoryNum) {
        // check to see if its already spawned
        if (pp.InventorySprite[InventoryNum] != null) {
            return (null);
        }

        // check for Icon panel state
        if (InventoryData[InventoryNum].State == null) {
            return (null);
        }

        Renderer renderer = game.getRenderer();
        int x = InventoryBarXpos[cfg.BorderNum] + (InventoryNum * INVENTORY_ICON_WIDTH);
        int y = InventoryBarYpos[cfg.BorderNum];
        Panel_Sprite psp = pSpawnSprite(pp, InventoryData[InventoryNum].State, PRI_FRONT, x, y);
        pp.InventorySprite[InventoryNum] = psp;

        psp.x1 = 0;
        psp.y1 = 0;
        psp.x2 =  (renderer.getWidth() - 1);
        psp.y2 =  (renderer.getHeight() - 1);
        psp.scale = InventoryData[InventoryNum].Scale;
        psp.flags |= (PANF_STATUS_AREA | PANF_SCREEN_CLIP);

        return (psp);
    }

    private static void KillPlayerIcon(PlayerStr ignored, Panel_Sprite pspp) {
        if (pspp != null) {
            pspp.flags |= (PANF_SUICIDE);
        }
    }

    //////////////////////////////////////////////////////////////////////
    //
    // NIGHT VISION
    //
    //////////////////////////////////////////////////////////////////////

    private static void KillAllPanelInv(PlayerStr pp) {
        pp.InventoryBarTics = 0;
        for (int i = 0; i < MAX_INVENTORY; i++) {
            if (pp.InventorySprite[i] == null) {
                continue;
            }

            pp.InventoryTics[i] = 0;
            pp.InventorySprite[i].flags |= (PANF_SUICIDE);
            pp.InventorySprite[i].numpages = 0;
            pp.InventorySprite[i] = null;
        }
    }

    public static Panel_Sprite SpawnIcon(PlayerStr pp, Panel_State state) {
        Renderer renderer = game.getRenderer();
        Panel_Sprite psp = pSpawnSprite(pp, state, PRI_FRONT, 0, 0);

        psp.x1 = 0;
        psp.y1 = 0;
        psp.x2 =  (renderer.getWidth() - 1);
        psp.y2 =  (renderer.getHeight() - 1);
        psp.flags |= (PANF_STATUS_AREA | PANF_SCREEN_CLIP);
        return (psp);
    }

    //////////////////////////////////////////////////////////////////////
    //
    // INVENTORY KEYS
    //
    //////////////////////////////////////////////////////////////////////

    private static void AutoPickInventory(PlayerStr pp) {
        // auto pick only if run out of currently selected one

        if (pp.InventoryAmount[pp.InventoryNum] <= 0) {
            for (int i = 0; i < MAX_INVENTORY; i++) {
                if (i == INVENTORY_REPAIR_KIT) {
                    continue;
                }

                if (pp.InventoryAmount[i] != 0) {
                    pp.InventoryNum =  i;
                    return;
                }
            }

            // only take this if there is nothing else
            if (pp.InventoryAmount[INVENTORY_REPAIR_KIT] != 0) {
                pp.InventoryNum = INVENTORY_REPAIR_KIT;
            }
        }
    }

    public static void UseInventoryMedkit(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        int inv = INVENTORY_MEDKIT;

        if (pp.InventoryAmount[inv] == 0) {
            return;
        }

        int diff =  (100 - u.Health);
        if (diff <= 0) {
            return;
        }

        int amt;
        if (diff > pp.InventoryPercent[inv]) {// If not enough to get to 100, use what's left
            amt = pp.InventoryPercent[inv];
        } else {
            amt = diff;
        }

        PlayerUpdateHealth(pp, amt);

        pp.InventoryPercent[inv] -= (short) diff;
        if (pp.InventoryPercent[inv] < 0) {
            pp.InventoryPercent[inv] = 0;
            pp.InventoryAmount[inv]--;
        }

        AutoPickInventory(pp);

        // percent
        PlayerUpdateInventory(pp, pp.InventoryNum);

        if (pp == Player[myconnectindex]) {
            if (amt >= 30) {
                PlayerSound(DIGI_GETMEDKIT, v3df_follow | v3df_dontpan, pp);
            } else {
                PlayerSound(DIGI_AHH, v3df_follow | v3df_dontpan, pp);
            }
        }
    }

    public static void UseInventoryChemBomb(PlayerStr pp) {
        int inv = INVENTORY_CHEMBOMB;
        if (pp.InventoryAmount[inv] == 0) {
            return;
        }

        PlayerInitChemBomb(pp); // Throw a chemical bomb out there
        pp.InventoryPercent[inv] = 0;
        if (--pp.InventoryAmount[inv] < 0) {
            pp.InventoryAmount[inv] = 0;
        }

        AutoPickInventory(pp);

        PlayerUpdateInventory(pp, pp.InventoryNum);
    }

    //////////////////////////////////////////////////////////////////////
    //
    // INVENTORY BAR
    //
    //////////////////////////////////////////////////////////////////////

    public static void UseInventoryFlashBomb(PlayerStr pp) {
        int inv = INVENTORY_FLASHBOMB;
        if (pp.InventoryAmount[inv] == 0) {
            return;
        }

        PlayerInitFlashBomb(pp);
        pp.InventoryPercent[inv] = 0;
        if (--pp.InventoryAmount[inv] < 0) {
            pp.InventoryAmount[inv] = 0;
        }

        AutoPickInventory(pp);

        PlayerUpdateInventory(pp, pp.InventoryNum);
    }

    public static void UseInventoryCaltrops(PlayerStr pp) {
        int inv = INVENTORY_CALTROPS;
        if (pp.InventoryAmount[inv] == 0) {
            return;
        }

        PlayerInitCaltrops(pp);
        pp.InventoryPercent[inv] = 0;
        if (--pp.InventoryAmount[inv] < 0) {
            pp.InventoryAmount[inv] = 0;
        }

        AutoPickInventory(pp);

        PlayerUpdateInventory(pp, pp.InventoryNum);
    }

    public static void UseInventoryRepairKit(PlayerStr pp) {
        int inv = INVENTORY_REPAIR_KIT;
        if (pp == Player[myconnectindex]) {
            if (STD_RANDOM_RANGE(1000) > 500) {
                PlayerSound(DIGI_NOREPAIRMAN, v3df_follow | v3df_dontpan, pp);
            } else {
                PlayerSound(DIGI_NOREPAIRMAN2, v3df_follow | v3df_dontpan, pp);
            }
        }

        pp.InventoryPercent[inv] = 0;
        pp.InventoryAmount[inv] = 0;

        AutoPickInventory(pp);

        // percent
        PlayerUpdateInventory(pp, pp.InventoryNum);
    }

    public static void UseInventoryCloak(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        if (sp == null || pp.InventoryActive[pp.InventoryNum]) {
            return;
        }

        pp.InventoryActive[pp.InventoryNum] = true;
        AutoPickInventory(pp);

        // on/off
        PlayerUpdateInventory(pp, pp.InventoryNum);

        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT));
        sp.setShade(100);

        PlaySound(DIGI_GASPOP, pp, v3df_none);
        if (pp == Player[myconnectindex]) {
            PlayerSound(DIGI_IAMSHADOW, v3df_follow | v3df_dontpan, pp);
        }
    }

    private static void StopInventoryCloak(PlayerStr pp, int InventoryNum) {
        Sprite sp = pp.getSprite();
        if (sp == null) {
            return;
        }

        pp.InventoryActive[InventoryNum] = false;
        if (pp.InventoryPercent[InventoryNum] <= 0) {
            pp.InventoryPercent[InventoryNum] = 0;
            if (--pp.InventoryAmount[InventoryNum] < 0) {
                pp.InventoryAmount[InventoryNum] = 0;
            }
        }

        // on/off
        PlayerUpdateInventory(pp, InventoryNum);

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));
        sp.setShade(0);

        PlaySound(DIGI_GASPOP, pp, v3df_none);
    }

    /////////////////////////////////////////////////////////////////
    //
    // Inventory Console Area
    //
    /////////////////////////////////////////////////////////////////

    public static void UseInventoryNightVision(PlayerStr pp) {
        if (pp.InventoryActive[pp.InventoryNum]) {
            StopInventoryNightVision(pp, pp.InventoryNum);
            return;
        }

        pp.InventoryActive[pp.InventoryNum] = true;

        // on/off
        PlayerUpdateInventory(pp, pp.InventoryNum);

        DoPlayerNightVisionPalette(pp);
        PlaySound(DIGI_NIGHTON, pp, v3df_dontpan | v3df_follow);
    }

    private static void StopInventoryNightVision(PlayerStr pp, int InventoryNum) {
        pp.InventoryActive[InventoryNum] = false;

        if (pp.InventoryPercent[InventoryNum] <= 0) {
            pp.InventoryPercent[InventoryNum] = 0;
            if (--pp.InventoryAmount[InventoryNum] < 0) {
                pp.InventoryAmount[InventoryNum] = 0;
            }
        }

        AutoPickInventory(pp);

        // on/off
        PlayerUpdateInventory(pp, pp.InventoryNum);

        DoPlayerNightVisionPalette(pp);
        DoPlayerDivePalette(pp);
        PlaySound(DIGI_NIGHTOFF, pp, v3df_dontpan | v3df_follow);
    }

    public static void InventoryKeys(PlayerStr pp) {
        // scroll SPELLs left
        if (TEST_SYNC_KEY(pp, SK_INV_LEFT)) {
            if (FLAG_KEY_PRESSED(pp, SK_INV_LEFT)) {
                FLAG_KEY_RELEASE(pp, SK_INV_LEFT);
                SpawnInventoryBar(pp);
                PlayerUpdateInventory(pp, pp.InventoryNum - 1);
                PutStringInfo(pp, InventoryData[pp.InventoryNum].Name);
                InventoryBarUpdatePosition(pp);
            }
        } else {
            FLAG_KEY_RESET(pp, SK_INV_LEFT);
        }

        // scroll SPELLs right
        if (TEST_SYNC_KEY(pp, SK_INV_RIGHT)) {
            if (FLAG_KEY_PRESSED(pp, SK_INV_RIGHT)) {
                FLAG_KEY_RELEASE(pp, SK_INV_RIGHT);
                SpawnInventoryBar(pp);
                PlayerUpdateInventory(pp, pp.InventoryNum + 1);
                PutStringInfo(pp, InventoryData[pp.InventoryNum].Name);
                InventoryBarUpdatePosition(pp);
            }
        } else {
            FLAG_KEY_RESET(pp, SK_INV_RIGHT);
        }

        if (TEST_SYNC_KEY(pp, SK_INV_USE)) {
            if (FLAG_KEY_PRESSED(pp, SK_INV_USE)) {
                FLAG_KEY_RELEASE(pp, SK_INV_USE);

                if (InventoryData[pp.InventoryNum].init != null) {
                    if (pp.InventoryAmount[pp.InventoryNum] != 0) {
                        InventoryUse(pp);
                    } else {
                        PutStringInfo(pp, "No " + InventoryData[pp.InventoryNum].Name); // DONT have message
                    }
                }
            }
        } else {
            FLAG_KEY_RESET(pp, SK_INV_USE);
        }

        // get hotkey number out of input bits
        int inv_hotkey = (DTEST(pp.input.bits, SK_INV_HOTKEY_MASK) >> SK_INV_HOTKEY_BIT0);
        if (inv_hotkey != 0) {
            if (FLAG_KEY_PRESSED(pp, SK_INV_HOTKEY_BIT0)) {
                FLAG_KEY_RELEASE(pp, SK_INV_HOTKEY_BIT0);

                inv_hotkey -= 1;

                pp.InventoryNum = inv_hotkey;

                if (InventoryData[pp.InventoryNum].init != null && !TEST(pp.Flags, PF_CLIMBING)) {
                    if (pp.InventoryAmount[pp.InventoryNum] != 0) {
                        InventoryUse(pp);
                    }
                }

            }
        } else {
            FLAG_KEY_RESET(pp, SK_INV_HOTKEY_BIT0);
        }
    }

    public static void InventoryTimer(PlayerStr pp) {
        // called every time through loop

        // if bar is up
        if (pp.InventoryBarTics != 0) {
            InventoryBarUpdatePosition(pp);

            pp.InventoryBarTics -= synctics;
            // if bar time has elapsed
            if (pp.InventoryBarTics <= 0) {
                // get rid of the bar
                KillInventoryBar(pp);
                // don't update bar anymore
                pp.InventoryBarTics = 0;

                // BorderRefresh(pp);
            }
        }

        int inv = 0;
        for (int i = 0; i < InventoryData.length; i++, inv++) {
            INVENTORY_DATA id = InventoryData[i];
            // if timed and active
            if (TEST(id.Flags, INVF_TIMED) && pp.InventoryActive[inv]) {
                // dec tics
                pp.InventoryTics[inv] -= synctics;
                if (pp.InventoryTics[inv] <= 0) {
                    // take off a percentage
                    pp.InventoryPercent[inv] -= (short) id.DecPerSec;
                    if (pp.InventoryPercent[inv] <= 0) {
                        // ALL USED UP
                        pp.InventoryPercent[inv] = 0;
                        InventoryStop(pp, inv);
                        pp.InventoryActive[inv] = false;
                    } else {
                        // reset 1 sec tic clock
                        pp.InventoryTics[inv] = (short) SEC(1);
                    }

                    // PlayerUpdateInventoryPercent(pp);
                    PlayerUpdateInventory(pp, pp.InventoryNum);
                }
            } else
                // the idea behind this is that the USE function will get called
                // every time the player is in an AUTO_USE situation.
                // This code will decrement the timer and set the Item to InActive
                // EVERY SINGLE TIME. Relies on the USE function getting called!
                if (TEST(id.Flags, INVF_AUTO_USE) && pp.InventoryActive[inv]) {
                    pp.InventoryTics[inv] -= synctics;
                    if (pp.InventoryTics[inv] <= 0) {
                        // take off a percentage
                        pp.InventoryPercent[inv] -= (short) id.DecPerSec;
                        if (pp.InventoryPercent[inv] <= 0) {
                            // ALL USED UP
                            pp.InventoryPercent[inv] = 0;
                            // should get rid if Amount - stop it for good
                            InventoryStop(pp, inv);
                        } else {
                            // reset 1 sec tic clock
                            pp.InventoryTics[inv] = (short) SEC(1);
                            // set to InActive EVERY TIME THROUGH THE LOOP!
                            pp.InventoryActive[inv] = false;
                        }

                        // PlayerUpdateInventoryPercent(pp);
                        PlayerUpdateInventory(pp, pp.InventoryNum);
                    }
                }
        }
    }

    private static void SpawnInventoryBar(PlayerStr pp) {
        int inv = 0;
        INVENTORY_DATA id = InventoryData[0];

        // its already up
        if (pp.InventoryBarTics != 0) {
            pp.InventoryBarTics =  SEC(2);
            return;
        }

        pp.InventorySelectionBox = SpawnIcon(pp, ps_PanelSelectionBox[0]);

        for (int i = 0; i < InventoryData.length && id.Name != null; i++, inv++) {
            id = InventoryData[i];
            Panel_Sprite psp = SpawnInventoryIcon(pp, inv);

            if (psp != null && pp.InventoryAmount[inv] == 0) {
                psp.shade = 100; // Darken it
            }
        }

        pp.InventoryBarTics =  SEC(2);
    }

    public static void KillInventoryBar(PlayerStr pp) {
        KillAllPanelInv(pp);
        KillPlayerIcon(pp, pp.InventorySelectionBox);
        pp.InventorySelectionBox = null;
    }

    // In case the BorderNum changes - move the postions
    private static void InventoryBarUpdatePosition(PlayerStr pp) {
        int inv = 0;
        INVENTORY_DATA id = InventoryData[0];

        int x =  (InventoryBarXpos[cfg.BorderNum] + (pp.InventoryNum * INVENTORY_ICON_WIDTH));
        int y = InventoryBarYpos[cfg.BorderNum];

        if (pp.InventorySelectionBox == null) {
            return;
        }

        pp.InventorySelectionBox.x = x - 5;
        pp.InventorySelectionBox.y = y - 5;

        for (int i = 0; i < InventoryData.length && id.Name != null; i++, inv++) {
            id = InventoryData[i];
            x =  (InventoryBarXpos[cfg.BorderNum] + (inv * INVENTORY_ICON_WIDTH));
            y = InventoryBarYpos[cfg.BorderNum];
            if (pp.InventorySprite[inv] != null) {
                pp.InventorySprite[inv].x = x;
                pp.InventorySprite[inv].y = y;
            }
        }
    }

    private static void InventoryUse(PlayerStr pp) {
        INVENTORY_DATA id = InventoryData[pp.InventoryNum];

        if (id.init != null) {
            (id.init).invoke(pp);
        }
    }

    private static void InventoryStop(PlayerStr pp, int InventoryNum) {
        INVENTORY_DATA id = InventoryData[InventoryNum];

        if (id.stop != null) {
            (id.stop).invoke(pp, InventoryNum);
        }
    }

    public static void DrawInventory(PlayerStr pp, int x, int y, int flags) {
        // Check for items that need to go translucent from use
        if (pp.InventoryBarTics != 0) {
            int inv = 0;
            INVENTORY_DATA id = InventoryData[0];
            Panel_Sprite psp;

            // Go translucent if used
            for (int i = 0; i < InventoryData.length && id.Name != null; i++, inv++) {
                id = InventoryData[i];
                psp = pp.InventorySprite[inv];
                if (psp != null) {
                    if (pp.InventoryAmount[inv] == 0) {
                        psp.shade = 100; // Darken it
                    } else {
                        psp.shade = 0;
                    }
                }
            }
        }

        InventoryBoxX =  x;
        InventoryBoxY =  y;

        // put pic
        if (cfg.BorderNum == BORDER_BAR) {
            if (pp.InventoryAmount[pp.InventoryNum] != 0) {
                PlayerUpdateInventoryPic(pp, flags);
            }
        }

        if (pp.InventoryAmount[pp.InventoryNum] != 0) {
            // Auto/On/Off
            PlayerUpdateInventoryState(pp, flags);
            // Percent count/Item count
            PlayerUpdateInventoryPercent(pp, flags);
        }
    }

    public static void PlayerUpdateInventory(PlayerStr pp, int InventoryNum) {
        pp.InventoryNum =  InventoryNum;

        if (pp.InventoryNum < 0) {
            pp.InventoryNum = MAX_INVENTORY - 1;
        }

        if (pp.InventoryNum >= MAX_INVENTORY) {
            pp.InventoryNum = 0;
        }
    }

    private static void PlayerUpdateInventoryPercent(PlayerStr pp, int flags) {
        INVENTORY_DATA id = InventoryData[pp.InventoryNum];

        int x = InventoryBoxX + INVENTORY_PERCENT_XOFF;
        int y = InventoryBoxY + INVENTORY_PERCENT_YOFF;

        if (TEST(id.Flags, INVF_COUNT)) {
            Bitoa(pp.InventoryAmount[pp.InventoryNum], buffer);
            InventoryDisplayString(pp, x, y, flags);
        } else {
            int offs = Bitoa(pp.InventoryPercent[pp.InventoryNum], buffer);
            buildString(buffer, offs, "%");
            InventoryDisplayString(pp, x, y, flags);
        }
    }

    private static void PlayerUpdateInventoryPic(PlayerStr pp, int flags) {
        Renderer renderer = game.getRenderer();
        INVENTORY_DATA id = InventoryData[pp.InventoryNum];

        int x = InventoryBoxX + INVENTORY_PIC_XOFF;
        int y = InventoryBoxY + INVENTORY_PIC_YOFF;

        renderer.rotatesprite(x << 16, y << 16, id.Scale, 0, id.State.picndx, 0, 0, ROTATE_SPRITE_SCREEN_CLIP | ROTATE_SPRITE_CORNER | flags);
    }

    private static void PlayerUpdateInventoryState(PlayerStr pp, int flags) {
        INVENTORY_DATA id = InventoryData[pp.InventoryNum];

        int x = InventoryBoxX + INVENTORY_STATE_XOFF;
        int y = InventoryBoxY + INVENTORY_STATE_YOFF;

        if (TEST(id.Flags, INVF_AUTO_USE)) {
            buildString(buffer, 0, "AUTO");
            InventoryDisplayString(pp, x, y, flags);
        } else if (TEST(id.Flags, INVF_TIMED)) {
            buildString(buffer, 0, pp.InventoryActive[pp.InventoryNum] ? "ON" : "OFF");
            InventoryDisplayString(pp, x, y, flags);
        }
    }

    private static void InventoryDisplayString(PlayerStr pp, int x, int y, int flags) {
        if (cfg.BorderNum != BORDER_BAR) {
            DisplayMiniBarSmString(pp, x, y, 0, buffer, flags);
        } else {
            DisplaySmString(pp, x, y, 0, buffer);
        }
    }

    public interface Inventory_Stop_Func {
        void invoke(PlayerStr pp, int num);
    }

}
