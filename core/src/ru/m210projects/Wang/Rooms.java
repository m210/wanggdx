package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Hitscan;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Wang.Type.LONGp;
import ru.m210projects.Wang.Type.Save;
import ru.m210projects.Wang.Type.Sect_User;

import java.util.concurrent.atomic.AtomicInteger;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Pragmas.scale;
import static ru.m210projects.Wang.Draw.analyzesprites;
import static ru.m210projects.Wang.Draw.post_analyzesprites;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.STAT_FAF;
import static ru.m210projects.Wang.Names.STAT_ST1;
import static ru.m210projects.Wang.Sector.getSectUser;
import static ru.m210projects.Wang.Stag.*;
import static ru.m210projects.Wang.Type.MyTypes.DTEST;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Warp.*;

public class Rooms {

    public static final int ZMAX = 400;
    private static final int[] sectorlist = new int[16];
    private static final LONGp upper = new LONGp();
    private static final LONGp lower = new LONGp();
    public static final Save save = new Save();
    public static final AtomicInteger fz = new AtomicInteger();
    public static final AtomicInteger cz = new AtomicInteger();
    public static final int[] GlobStackSect = new int[2];

    // Game loop
    public static int COVERupdatesector(int x, int y, int newsector) {
        if (boardService.isValidSector(newsector)) {
            return engine.updatesector(x, y, newsector);
        }
        return -1;
    }

    // Game loop
    public static int COVERinsertsprite(int sectnum, int stat) {
        int spnum = engine.insertsprite(sectnum, stat);
        Sprite sp = boardService.getSprite(spnum);
        if (sp == null) {
            return -1;
        }

        sp.setX(0);
        sp.setY(0);
        sp.setZ(0);
        sp.setCstat(0);
        sp.setPicnum(0);
        sp.setShade(0);
        sp.setPal(0);
        sp.setClipdist(0);
        sp.setXrepeat(0);
        sp.setYrepeat(0);
        sp.setXoffset(0);
        sp.setYoffset(0);
        sp.setAng(0);
        sp.setOwner(-1);
        sp.setXvel(0);
        sp.setYvel(0);
        sp.setZvel(0);
        sp.setLotag(0);
        sp.setHitag(0);
        sp.setExtra(0);

        return (spnum);
    }

    // Game loop
    public static boolean FAF_Sector(int sectnum) {
        if (!boardService.isValidSector(sectnum)) {
            return false;
        }

        for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = node.getNext()) {
            Sprite sp = node.get();
            if (sp.getStatnum() == STAT_FAF && (sp.getHitag() >= VIEW_LEVEL1 && sp.getHitag() <= VIEW_LEVEL6)) {
                return true;
            }
        }

        return false;
    }

    private static void SetWallWarpHitscan(int sectnum) {
        Sprite sp_warp = WarpSectorInfo(sectnum);
        Sector sec = boardService.getSector(sectnum);
        if (sec == null || sp_warp == null) {
            return;
        }

        // move the next wall
        int wall_num = sec.getWallptr();
        int start_wall = wall_num;

        // Travel all the way around loop setting wall bits
        do {
            Wall wal = boardService.getWall(wall_num);
            if (wal == null) {
                break;
            }

            if (wal.getNextwall() >= 0) {
                wal.setCstat(wal.getCstat() | (CSTAT_WALL_WARP_HITSCAN));
            }
            wall_num = wal.getPoint2();
        } while (wall_num != start_wall);
    }

    private static void ResetWallWarpHitscan(int sectnum) {
        Sector sec = boardService.getSector(sectnum);
        if (sec == null) {
            return;
        }

        // move the next wall
        int wall_num = sec.getWallptr();
        int start_wall = wall_num;

        // Travel all the way around loop setting wall bits
        do {
            Wall wal = boardService.getWall(wall_num);
            if (wal == null) {
                break;
            }

            wal.setCstat(wal.getCstat() & ~(CSTAT_WALL_WARP_HITSCAN));
            wall_num = wal.getPoint2();
        } while (wall_num != start_wall);
    }

    // Game loop
    public static void FAFhitscan(int x, int y, int z, int sectnum, int xvect, int yvect, int zvect, Hitscan hit,
                                  int clipmask) {
        int newsectnum = sectnum;
        int startclipmask = 0;
        boolean plax_found = false;

        if (clipmask == CLIPMASK_MISSILE) {
            startclipmask = CLIPMASK_WARP_HITSCAN;
        }

        engine.hitscan(x, y, z, sectnum, xvect, yvect, zvect, hit, startclipmask);

        if (hit.hitsect == -1) {
            return;
        }

        Wall hitWall = boardService.getWall(hit.hitwall);
        if (hitWall != null) {
            // hitscan warping
            if (TEST(hitWall.getCstat(), CSTAT_WALL_WARP_HITSCAN)) {
                // back it up a bit to get a correct warp location
                hit.hitx -= xvect >> 9;
                hit.hity -= yvect >> 9;

                // warp to new x,y,z, sectnum
                if (WarpM(hit.hitx, hit.hity, hit.hitz, hit.hitsect) != null) {
                    hit.hitx = warp.x;
                    hit.hity = warp.y;
                    hit.hitz = warp.z;
                    hit.hitsect = warp.sectnum;

                    int dest_sect = hit.hitsect;

                    // hitscan needs to pass through dest sect
                    ResetWallWarpHitscan(dest_sect);

                    // NOTE: This could be recursive I think if need be
                    engine.hitscan(hit.hitx, hit.hity, hit.hitz, hit.hitsect, xvect, yvect, zvect, hit, startclipmask);

                    // reset hitscan block for dest sect
                    SetWallWarpHitscan(dest_sect);

                    return;
                }
            }
        }

        // make sure it hit JUST a sector before doing a check
        if (hit.hitwall == -1 && hit.hitsprite == -1) {
            Sector hitSec = boardService.getSector(hit.hitsect);
            if (hitSec != null && TEST(hitSec.getExtra(), SECTFX_WARP_SECTOR)) {
                Wall wal = boardService.getWall(hitSec.getWallptr());
                if (wal != null && TEST(wal.getCstat(), CSTAT_WALL_WARP_HITSCAN)) {
                    // hit the floor of a sector that is a warping sector
                    if (WarpM(hit.hitx, hit.hity, hit.hitz, hit.hitsect) != null) {
                        hit.hitx = warp.x;
                        hit.hity = warp.y;
                        hit.hitz = warp.z;
                        hit.hitsect = warp.sectnum;

                        engine.hitscan(hit.hitx, hit.hity, hit.hitz, hit.hitsect, xvect, yvect, zvect, hit, clipmask);
                        return;
                    }
                } else {
                    if (WarpPlane(hit.hitx, hit.hity, hit.hitz, hit.hitsect) != null) {
                        hit.hitx = warp.x;
                        hit.hity = warp.y;
                        hit.hitz = warp.z;
                        hit.hitsect = warp.sectnum;

                        engine.hitscan(hit.hitx, hit.hity, hit.hitz, hit.hitsect, xvect, yvect, zvect, hit, clipmask);

                        return;
                    }
                }
            }

            engine.getzsofslope(hit.hitsect, hit.hitx, hit.hity, fz, cz);
            if (klabs(hit.hitz - fz.get()) < Z(4)) {
                if (FAF_ConnectFloor(hitSec)
                        && !TEST(hitSec.getFloorstat(), FLOOR_STAT_FAF_BLOCK_HITSCAN)) {
                    newsectnum = engine.updatesectorz(hit.hitx, hit.hity, hit.hitz + Z(12), newsectnum);
                    plax_found = true;
                }
            } else if (klabs(hit.hitz - cz.get()) < Z(4)) {
                if (FAF_ConnectCeiling(hitSec)
                        && !TEST(hitSec.getFloorstat(), CEILING_STAT_FAF_BLOCK_HITSCAN)) {
                    newsectnum = engine.updatesectorz(hit.hitx, hit.hity, hit.hitz - Z(12), newsectnum);
                    plax_found = true;
                }
            }
        }

        if (plax_found) {
            engine.hitscan(hit.hitx, hit.hity, hit.hitz, newsectnum, xvect, yvect, zvect, hit, clipmask);
        }
    }

    // Game loop
    public static boolean FAFcansee(int xs, int ys, int zs, int sects, int xe, int ye, int ze, int secte) {
        int newsectnum = sects;


        // early out to regular routine
        if (!FAF_Sector(sects) && !FAF_Sector(secte)) {
            return (engine.cansee(xs, ys, zs, sects, xe, ye, ze, secte));
        }

        // get angle
        int ang = EngineUtils.getAngle(xe - xs, ye - ys);

        // get x,y,z, vectors
        int xvect = EngineUtils.sin(NORM_ANGLE(ang + 512));
        int yvect = EngineUtils.sin(NORM_ANGLE(ang));
        int zvect = 0;

        // find the distance to the target
        int dist = EngineUtils.sqrt(SQ(xe - xs) + SQ(ye - ys));

        if (dist != 0) {
            if (xe - xs != 0) {
                zvect = scale(xvect, ze - zs, xe - xs);
            } else if (ye - ys != 0) {
                zvect = scale(yvect, ze - zs, ye - ys);
            }
        }

        engine.hitscan(xs, ys, zs, sects, xvect, yvect, zvect, pHitInfo, CLIPMASK_MISSILE);

        if (pHitInfo.hitsect == -1) {
            return false;
        }

        boolean plax_found = false;
        // make sure it hit JUST a sector before doing a check
        if (pHitInfo.hitwall == -1 && pHitInfo.hitsprite == -1) {
            engine.getzsofslope(pHitInfo.hitsect, pHitInfo.hitx, pHitInfo.hity, fz, cz);
            if (klabs(pHitInfo.hitz - fz.get()) < Z(4)) {
                if (FAF_ConnectFloor(boardService.getSector(pHitInfo.hitsect))) {
                    newsectnum = engine.updatesectorz(pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz + Z(12), newsectnum);
                    plax_found = true;
                }
            } else if (klabs(pHitInfo.hitz - cz.get()) < Z(4)) {
                if (FAF_ConnectCeiling(boardService.getSector(pHitInfo.hitsect))) {
                    newsectnum = engine.updatesectorz(pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz - Z(12), newsectnum);
                    plax_found = true;
                }
            }
        } else {
            return (engine.cansee(xs, ys, zs, sects, xe, ye, ze, secte));
        }

        if (plax_found) {
            return (engine.cansee(pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz, newsectnum, xe, ye, ze, secte));
        }

        return false;
    }

    // Game loop
    public static int GetZadjustment(int sectnum, int hitag) {
        Sector sec = boardService.getSector(sectnum);
        if (sec == null) {
            return 0;
        }

        if (!TEST(sec.getExtra(), SECTFX_Z_ADJUST)) {
            return (0);
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ST1); node != null; node = node.getNext()) {
            Sprite sp = node.get();

            if (sp.getHitag() == hitag && sp.getSectnum() == sectnum) {
                return (Z(sp.getLotag()));
            }
        }

        return (0);
    }

    private static boolean SectorZadjust(int ceilhit, LONGp hiz, int florhit, LONGp loz) {
        // extern long PlaxCeilGlobZadjust, PlaxFloorGlobZadjust;
        boolean SkipFAFcheck = false;

        if (florhit != -1) {
            switch (DTEST(florhit, HIT_MASK)) {
                case HIT_SPRITE:
                case HIT_WALL:
                    break;
                case HIT_SECTOR: {
                    final int hitsector = NORM_HIT_INDEX(florhit);
                    Sector hitSec = boardService.getSector(hitsector);
                    if (hitSec == null) {
                        break;
                    }

                    // don't jack with connect sectors
                    if (FAF_ConnectFloor(boardService.getSector(hitsector))) {
                        // rippers were dying through the floor in $rock
                        if (TEST(hitSec.getFloorstat(), CEILING_STAT_FAF_BLOCK_HITSCAN)) {
                            break;
                        }

                        if (TEST(hitSec.getExtra(), SECTFX_Z_ADJUST)) {
                            // see if a z adjust ST1 is around
                            int z_amt = GetZadjustment(hitsector, FLOOR_Z_ADJUST);

                            if (z_amt != 0) {
                                // explicit z adjust overrides Connect Floor
                                loz.value += z_amt;
                                SkipFAFcheck = true;
                            }
                        }
                        break;
                    }

                    if (!TEST(hitSec.getExtra(), SECTFX_Z_ADJUST)) {
                        break;
                    }

                    // see if a z adjust ST1 is around
                    int z_amt = GetZadjustment(hitsector, FLOOR_Z_ADJUST);

                    if (z_amt != 0) {
                        // explicit z adjust overrides plax default
                        loz.value += z_amt;
                    } else
                        // default adjustment for plax
                        if (TEST(hitSec.getFloorstat(), FLOOR_STAT_PLAX)) {
                            loz.value += PlaxFloorGlobZadjust;
                        }

                    break;
                }
            }
        }

        if (ceilhit != -1) {
            if (DTEST(ceilhit, HIT_MASK) == HIT_SECTOR) {
                final int hitsector = NORM_HIT_INDEX(ceilhit);
                Sector hitSec = boardService.getSector(hitsector);
                if (hitSec == null) {
                    return (SkipFAFcheck);
                }

                // don't jack with connect sectors
                if (FAF_ConnectCeiling(hitSec)) {
                    if (TEST(hitSec.getExtra(), SECTFX_Z_ADJUST)) {
                        // see if a z adjust ST1 is around
                        int z_amt = GetZadjustment(hitsector, CEILING_Z_ADJUST);

                        if (z_amt != 0) {
                            // explicit z adjust overrides Connect Floor
                            if (loz != null) {
                                loz.value += z_amt;
                            }
                            SkipFAFcheck = true;
                        }
                    }

                    return (SkipFAFcheck);
                }

                if (!TEST(hitSec.getExtra(), SECTFX_Z_ADJUST)) {
                    return (SkipFAFcheck);
                }

                // see if a z adjust ST1 is around
                int z_amt = GetZadjustment(hitsector, CEILING_Z_ADJUST);

                if (z_amt != 0) {
                    // explicit z adjust overrides plax default
                    hiz.value -= z_amt;
                } else
                    // default adjustment for plax
                    if (TEST(hitSec.getCeilingstat(), CEILING_STAT_PLAX)) {
                        hiz.value -= PlaxCeilGlobZadjust;
                    }
            }
        }

        return (SkipFAFcheck);
    }

    private static void WaterAdjust(int florhit, LONGp loz) {
        switch (DTEST(florhit, HIT_MASK)) {
            case HIT_SECTOR: {
                Sect_User sectu = getSectUser(NORM_HIT_INDEX(florhit));

                if (sectu != null && sectu.depth != 0) {
                    loz.value += Z(sectu.depth);
                }
            }
            break;
            case HIT_SPRITE:
                break;
        }
    }

    // Game loop
    public static void FAFgetzrange(int x, int y, int z, final int sectnum, LONGp hiz, LONGp ceilhit, LONGp loz,
                                    LONGp florhit, int clipdist, int clipmask) {
        // IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // This will return invalid FAF ceiling and floor heights inside of analyzesprite
        // because the ceiling and floors get moved out of the way for drawing.

        // early out to regular routine
        if (!FAF_ConnectArea(sectnum)) {
            engine.getzrange(x, y, z, sectnum, hiz, ceilhit, loz, florhit, clipdist, clipmask);
            SectorZadjust(ceilhit.value, hiz, florhit.value, loz);
            WaterAdjust(florhit.value, loz);
            return;
        }

        engine.getzrange(x, y, z, sectnum, hiz, ceilhit, loz, florhit, clipdist, clipmask);
        boolean SkipFAFcheck = SectorZadjust(ceilhit.value, hiz, florhit.value, loz);
        WaterAdjust(florhit.value, loz);

        if (SkipFAFcheck) {
            return;
        }

        Sector sec = boardService.getSector(sectnum);
        if (sec == null) {
            return;
        }

        if (FAF_ConnectCeiling(sec)) {
            int uppersect = sectnum;
            int newz = hiz.value - Z(2);

            if (DTEST(ceilhit.value, HIT_MASK) == HIT_SPRITE) {
                return;
            }

            uppersect = engine.updatesectorz(x, y, newz, uppersect);
            if (uppersect == -1) {
                Console.out.println("Did not find a sector at " + x + " ," + y + " ," + newz);
            }

            engine.getzrange(x, y, newz, uppersect, hiz, ceilhit, null, null, clipdist, clipmask);
            SectorZadjust(ceilhit.value, hiz, -1, null);
        } else if (FAF_ConnectFloor(sec) && !TEST(sec.getFloorstat(), FLOOR_STAT_FAF_BLOCK_HITSCAN)) {
            int lowersect = sectnum;
            int newz = loz.value + Z(2);

            if (DTEST(florhit.value, HIT_MASK) == HIT_SPRITE) {
                return;
            }

            lowersect = engine.updatesectorz(x, y, newz, lowersect);
            if (lowersect == -1) {
                Console.out.println("Did not find a sector at " + x + " ," + y + " ," + newz);
            }

            engine.getzrange(x, y, newz, lowersect, null, null, loz, florhit, clipdist, clipmask);
            SectorZadjust(-1, null, florhit.value, loz);
            WaterAdjust(florhit.value, loz);
        }
    }

    // Game loop
    public static void FAFgetzrangepoint(int x, int y, int z, final int sectnum, LONGp hiz, LONGp ceilhit, LONGp loz,
                                         LONGp florhit) {
        // IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // This will return invalid FAF ceiling and floor heights inside of analyzesprite
        // because the ceiling and floors get moved out of the way for drawing.

        // early out to regular routine
        if (!FAF_ConnectArea(sectnum)) {
            engine.getzrangepoint(x, y, z, sectnum, hiz, ceilhit, loz, florhit);
            SectorZadjust(ceilhit.value, hiz, florhit.value, loz);
            WaterAdjust(florhit.value, loz);
            return;
        }

        engine.getzrangepoint(x, y, z, sectnum, hiz, ceilhit, loz, florhit);
        boolean SkipFAFcheck = SectorZadjust(ceilhit.value, hiz, florhit.value, loz);
        WaterAdjust(florhit.value, loz);

        if (SkipFAFcheck) {
            return;
        }

        Sector sec = boardService.getSector(sectnum);
        if (sec == null) {
            return;
        }

        if (FAF_ConnectCeiling(sec)) {
            int uppersect = sectnum;
            int newz = hiz.value - Z(2);

            if (DTEST(ceilhit.value, HIT_MASK) == HIT_SPRITE) {
                return;
            }

            uppersect = engine.updatesectorz(x, y, newz, uppersect);
            if (uppersect == -1) {
                Console.out.println("Did not find a sector at " + x + ", " + y + ", " + newz + ", sectnum " + sectnum);
            }

            engine.getzrangepoint(x, y, newz, uppersect, hiz, ceilhit, null, null);
            SectorZadjust(ceilhit.value, hiz, -1, null);
        } else if (FAF_ConnectFloor(sec) && !TEST(sec.getFloorstat(), FLOOR_STAT_FAF_BLOCK_HITSCAN)) {
            int lowersect = sectnum;
            int newz = loz.value + Z(2);

            if (DTEST(florhit.value, HIT_MASK) == HIT_SPRITE) {
                return;
            }

            lowersect = engine.updatesectorz(x, y, newz, lowersect);
            if (lowersect == -1) {
                Console.out.println("Did not find a sector at " + x + ", " + y + ", " + newz + ", sectnum " + sectnum);
            }

            engine.getzrangepoint(x, y, newz, lowersect, null, null, loz, florhit);
            SectorZadjust(-1, null, florhit.value, loz);
            WaterAdjust(florhit.value, loz);
        }
    }

    // For renderer (Draw.java)
    // doesn't work for blank pics
    public static boolean PicInView(short tile_num, boolean reset) {
        Renderer renderer = game.getRenderer();
        byte[] gotpic = renderer.getRenderedPics();
        if (TEST(gotpic[tile_num >> 3], 1 << (tile_num & 7))) {
            if (reset) {
                gotpic[tile_num >> 3] &= (byte) ~(1 << (tile_num & 7));
            }

            return true;
        }

        return false;
    }

    // InitLevel
    public static void SetupMirrorTiles() {
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FAF); node != null; node = node.getNext()) {
            Sprite sp = node.get();
            Sector sector = boardService.getSector(sp.getSectnum());
            if (sector == null) {
                continue;
            }

            if (sector.getCeilingpicnum() == FAF_PLACE_MIRROR_PIC) {
                sector.setCeilingpicnum(FAF_MIRROR_PIC);
                sector.setCeilingstat(sector.getCeilingstat() | (CEILING_STAT_PLAX));
            }

            if (sector.getFloorpicnum() == FAF_PLACE_MIRROR_PIC) {
                sector.setFloorpicnum(FAF_MIRROR_PIC);
                sector.setFloorstat(sector.getFloorstat() | (FLOOR_STAT_PLAX));
            }

            if (sector.getCeilingpicnum() == FAF_PLACE_MIRROR_PIC + 1) {
                sector.setCeilingpicnum(FAF_MIRROR_PIC + 1);
            }

            if (sector.getFloorpicnum() == FAF_PLACE_MIRROR_PIC + 1) {
                sector.setFloorpicnum(FAF_MIRROR_PIC + 1);
            }
        }
    }

    private static void GetUpperLowerSector(int match, int x, int y, LONGp upper, LONGp lower) {
        // keep a list of the last stacked sectors the view was in and
        // check those fisrt
        short sln = 0;
        for (int j : GlobStackSect) {
            // will not hurt if GlobStackSect is invalid - inside checks for this
            if (engine.inside(x, y, j) == 1) {
                boolean found = false;

                for (ListNode<Sprite> node = boardService.getSectNode(j); node != null; node = node.getNext()) {
                    Sprite sp = node.get();

                    if (sp.getStatnum() == STAT_FAF && (sp.getHitag() >= VIEW_LEVEL1 && sp.getHitag() <= VIEW_LEVEL6)
                            && sp.getLotag() == match) {
                        found = true;
                    }
                }

                if (!found) {
                    continue;
                }

                sectorlist[sln] = j;
                sln++;
            }
        }

        // didn't find it yet so test ALL sectors
        if (sln < 2) {
            sln = 0;
            for (int i = (boardService.getSectorCount() - 1); i >= 0; i--) {
                Sector sec = boardService.getSector(i);
                if (sec != null && sec.inside(x, y)) {
                    boolean found = false;

                    for (ListNode<Sprite> node = boardService.getSectNode(i); node != null; node = node.getNext()) {
                        Sprite sp = node.get();

                        if (sp.getStatnum() == STAT_FAF && (sp.getHitag() >= VIEW_LEVEL1 && sp.getHitag() <= VIEW_LEVEL6)
                                && sp.getLotag() == match) {
                            found = true;
                        }
                    }

                    if (!found) {
                        continue;
                    }

                    sectorlist[sln] = i;
                    if (sln < GlobStackSect.length) {
                        GlobStackSect[sln] = i;
                    }
                    sln++;
                }
            }
        }

        // might not find ANYTHING if not tagged right
        if (sln == 0) {
            upper.value = -1;
            lower.value = -1;
            return;
        } else
            // inside will somtimes find that you are in two different sectors if the x,y
            // is exactly on a sector line.
            if (sln > 2) {
                // try again moving the x,y pos around until you only get two sectors
                GetUpperLowerSector(match, x - 1, y, upper, lower);
            }

        if (sln == 2) {
            Sector sec0 = boardService.getSector(sectorlist[0]);
            Sector sec1 = boardService.getSector(sectorlist[1]);
            if (sec0 != null && sec1 != null && sec0.getFloorz() < sec1.getFloorz()) {
                // swap
                // make sectorlist[0] the LOW sector
                int hold = sectorlist[0];
                sectorlist[0] = sectorlist[1];
                sectorlist[1] = hold;
            }

            lower.value = sectorlist[0];
            upper.value = sectorlist[1];
        }
    }

    private static void FindCeilingView(int match, LONGp x, LONGp y, int z, LONGp sectnum) {
        int xoff = 0;
        int yoff = 0;
        Sprite sp = null;

        save.zcount = 0;

        // Search Stat List For closest ceiling view sprite
        // Get the match, xoff, yoff from this point
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FAF); node != null; node = node.getNext()) {
            sp = node.get();

            if (sp.getHitag() == VIEW_THRU_CEILING && sp.getLotag() == match) {
                xoff = x.value - sp.getX();
                yoff = y.value - sp.getY();
                break;
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FAF); node != null; node = node.getNext()) {
            sp = node.get();

            if (sp.getLotag() == match) {
                // determine x,y position
                if (sp.getHitag() == VIEW_THRU_FLOOR) {
                    x.value = sp.getX() + xoff;
                    y.value = sp.getY() + yoff;

                    // get new sector
                    GetUpperLowerSector(match, x.value, y.value, sectnum, lower);
                    break;
                }
            }
        }

        if (sectnum.value == -1) {
            return;
        }

        if (sp == null || sp.getHitag() != VIEW_THRU_FLOOR) {
            return;
        }

        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec == null) {
            return;
        }

        int pix_diff = klabs(z - sec.getFloorz()) >> 8;
        int newz = sec.getFloorz() + ((pix_diff / 128) + 1) * Z(128);

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FAF); node != null; node = node.getNext()) {
            Sprite sp2 = node.get();
            Sector sec2 = boardService.getSector(sp2.getSectnum());
            if (sec2 == null) {
                continue;
            }

            if (sp2.getLotag() == match) {
                // move lower levels ceilings up for the correct view
                if (sp2.getHitag() == VIEW_LEVEL2) {
                    // save it off
                    save.sectnum[save.zcount] = sp2.getSectnum();
                    save.zval[save.zcount] = sec2.getFloorz();
                    save.pic[save.zcount] = sec2.getFloorpicnum();
                    save.slope[save.zcount] = sec2.getFloorheinum();

                    sec2.setFloorz(newz);
                    // don't change FAF_MIRROR_PIC - ConnectArea
                    if (sec2.getFloorpicnum() != FAF_MIRROR_PIC) {
                        sec2.setFloorpicnum(FAF_MIRROR_PIC + 1);
                    }
                    sec2.setFloorheinum(0);

                    save.zcount++;
                }
            }
        }
    }

    private static void FindFloorView(int match, LONGp x, LONGp y, int z, LONGp sectnum) {
        int xoff = 0;
        int yoff = 0;
        Sprite sp = null;

        save.zcount = 0;

        // Search Stat List For closest ceiling view sprite
        // Get the match, xoff, yoff from this point
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FAF); node != null; node = node.getNext()) {
            sp = node.get();

            if (sp.getHitag() == VIEW_THRU_FLOOR && sp.getLotag() == match) {
                xoff = x.value - sp.getX();
                yoff = y.value - sp.getY();
                break;
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FAF); node != null; node = node.getNext()) {
            sp = node.get();

            if (sp.getLotag() == match) {
                // determine x,y position
                if (sp.getHitag() == VIEW_THRU_CEILING) {
                    x.value = sp.getX() + xoff;
                    y.value = sp.getY() + yoff;

                    // get new sector
                    GetUpperLowerSector(match, x.value, y.value, upper, sectnum);
                    break;
                }
            }
        }

        if (sectnum.value < 0) {
            return;
        }

        if (sp == null || sp.getHitag() != VIEW_THRU_CEILING) {
            return;
        }

        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec == null) {
            return;
        }

        // move ceiling multiple of 128 so that the wall tile will line up
        int pix_diff = klabs(z - sec.getCeilingz()) >> 8;
        int newz = sec.getCeilingz() - ((pix_diff / 128) + 1) * Z(128);

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FAF); node != null; node = node.getNext()) {
            Sprite sp2 = node.get();
            Sector sec2 = boardService.getSector(sp2.getSectnum());
            if (sec2 == null) {
                continue;
            }

            if (sp2.getLotag() == match) {
                // move upper levels floors down for the correct view
                if (sp2.getHitag() == VIEW_LEVEL1) {
                    // save it off
                    save.sectnum[save.zcount] = sp2.getSectnum();
                    save.zval[save.zcount] = sec2.getCeilingz();
                    save.pic[save.zcount] = sec2.getCeilingpicnum();
                    save.slope[save.zcount] = sec2.getCeilingheinum();

                    sec2.setCeilingz(newz);
                    // don't change FAF_MIRROR_PIC - ConnectArea
                    if (sec2.getCeilingpicnum() != FAF_MIRROR_PIC) {
                        sec2.setCeilingpicnum(FAF_MIRROR_PIC + 1);
                    }
                    sec2.setCeilingheinum(0);

                    save.zcount++;
                }
            }
        }

    }

    private static short ViewSectorInScene(int cursectnum, int level) {
        short match;

        for (ListNode<Sprite> node = boardService.getStatNode(STAT_FAF); node != null; node = node.getNext()) {
            Sprite sp = node.get();

            if (sp.getHitag() == level) {
                if (cursectnum == sp.getSectnum()) {
                    // ignore case if sprite is pointing up
                    if (sp.getAng() == 1536) {
                        continue;
                    }

                    // only gets to here is sprite is pointing down

                    // found a potential match
                    match = sp.getLotag();

                    if (!PicInView(FAF_MIRROR_PIC, true)) {
                        return (-1);
                    }

                    return (match);
                }
            }
        }

        return (-1);
    }

    // For renderer (Draw.java)
    public static void DrawOverlapRoom(int tx, int ty, int tz, float tang, float thoriz, int tsectnum, int smoothratio) {
        save.zcount = 0;
        Renderer renderer = game.getRenderer();
        int match = ViewSectorInScene(tsectnum, VIEW_LEVEL1);
        if (match != -1) {
            FindCeilingView(match, tmp_ptr[0].set(tx), tmp_ptr[1].set(ty), tz, tmp_ptr[2].set(tsectnum));
            tx = tmp_ptr[0].value;
            ty = tmp_ptr[1].value;
            tsectnum = tmp_ptr[2].value;

            if (tsectnum == -1) {
                return;
            }

            renderer.drawrooms(tx, ty, tz, tang, thoriz, tsectnum);

            // reset Z's
            for (int i = 0; i < save.zcount; i++) {
                Sector sec = boardService.getSector(save.sectnum[i]);
                if (sec != null) {
                    sec.setFloorz(save.zval[i]);
                    sec.setFloorpicnum(save.pic[i]);
                    sec.setFloorheinum(save.slope[i]);
                }
            }

            analyzesprites(tx, ty, tz, false, smoothratio);
            post_analyzesprites(smoothratio);
            renderer.drawmasks();

        } else {
            match = ViewSectorInScene(tsectnum, VIEW_LEVEL2);
            if (match != -1) {
                FindFloorView(match, tmp_ptr[0].set(tx), tmp_ptr[1].set(ty), tz, tmp_ptr[2].set(tsectnum));
                tx = tmp_ptr[0].value;
                ty = tmp_ptr[1].value;
                tsectnum = tmp_ptr[2].value;

                if (tsectnum == -1) {
                    return;
                }

                renderer.drawrooms(tx, ty, tz, tang, thoriz, tsectnum);

                // reset Z's
                for (int i = 0; i < save.zcount; i++) {
                    Sector sec = boardService.getSector(save.sectnum[i]);
                    if (sec != null) {
                        sec.setCeilingz(save.zval[i]);
                        sec.setCeilingpicnum(save.pic[i]);
                        sec.setCeilingheinum(save.slope[i]);
                    }
                }

                analyzesprites(tx, ty, tz, false, smoothratio);
                post_analyzesprites(smoothratio);
                renderer.drawmasks();
            }
        }
    }

}
