package ru.m210projects.Wang;

import com.badlogic.gdx.Gdx;
import org.jetbrains.annotations.NotNull;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Type.*;
import ru.m210projects.Wang.Weapons.*;

import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Break.HitBreakSprite;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Draw.*;
import static ru.m210projects.Wang.Enemies.Ninja.PlayerDeathReset;
import static ru.m210projects.Wang.Enemies.Ninja.SpawnPlayerUnderSprite;
import static ru.m210projects.Wang.Factory.WangMenuHandler.LASTSAVE;
import static ru.m210projects.Wang.Factory.WangNetwork.CommPlayers;
import static ru.m210projects.Wang.Factory.WangNetwork.Prediction;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Inv.*;
import static ru.m210projects.Wang.JPlayer.*;
import static ru.m210projects.Wang.JSector.JS_ProcessEchoSpot;
import static ru.m210projects.Wang.JWeapon.InitBloodSpray;
import static ru.m210projects.Wang.LoadSave.lastload;
import static ru.m210projects.Wang.MClip.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.*;
import static ru.m210projects.Wang.Panel.*;
import static ru.m210projects.Wang.Player.Player_Action_Func.*;
import static ru.m210projects.Wang.Quake.*;
import static ru.m210projects.Wang.Rooms.*;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Shrap.SpawnShrap;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Stag.*;
import static ru.m210projects.Wang.Tags.TAG_WALL_CLIMB;
import static ru.m210projects.Wang.Text.*;
import static ru.m210projects.Wang.Track.*;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Vis.ProcessVisOn;
import static ru.m210projects.Wang.Warp.*;
import static ru.m210projects.Wang.Weapon.*;
import static ru.m210projects.Wang.Weapons.Chops.ChopsSetRetract;
import static ru.m210projects.Wang.Weapons.Chops.InitChops;

public class Player {

    public static final Animator QueueFloorBlood = new Animator(Weapon::QueueFloorBlood);
    private static final Animator DoFootPrints = new Animator((Animator.Runnable) ru.m210projects.Wang.Player::DoFootPrints);
    private static final Animator DoPlayerSpriteReset = new Animator((Animator.Runnable) ru.m210projects.Wang.Player::DoPlayerSpriteReset);

    public static final int SO_DRIVE_SOUND = 2;
    public static final int SO_IDLE_SOUND = 1;
    public static final int PLAYER_HORIZ_MAX = 299; // !JIM! was 199 and 5
    public static final int PLAYER_HORIZ_MIN = -99; // Had to make plax sky pan up/down like in Duke
//    public static final int MIN_SWIM_DEPTH = 15;
    // Player view height
    public static final int PLAYER_HEIGHT = Z(58);
    // But this is MUCH better!
    public static final int PLAYER_CRAWL_HEIGHT = Z(36);
    public static final int PLAYER_SWIM_HEIGHT = Z(26);
    public static final int PLAYER_DIVE_HEIGHT = Z(26);
//    public static final int PLAYER_DIE_DOWN_HEIGHT = Z(4);
//    public static final int PLAYER_DIE_UP_HEIGHT = Z(8);
    // step heights - effects floor_dist's
    public static final int PLAYER_STEP_HEIGHT = Z(30);
    public static final int PLAYER_CRAWL_STEP_HEIGHT = Z(8);
//    public static final int PLAYER_SWIM_STEP_HEIGHT = Z(8);
    public static final int PLAYER_DIVE_STEP_HEIGHT = Z(8);
    public static final int PLAYER_JUMP_STEP_HEIGHT = Z(48);
    public static final int PLAYER_FALL_STEP_HEIGHT = Z(24);
    // FLOOR_DIST variables are the difference in the Players view and the sector
    // floor.
    // Must be at LEAST this distance or you cannot move onto sector.
    public static final int PLAYER_RUN_FLOOR_DIST = (PLAYER_HEIGHT - PLAYER_STEP_HEIGHT);
    public static final int PLAYER_CRAWL_FLOOR_DIST = (PLAYER_CRAWL_HEIGHT - PLAYER_CRAWL_STEP_HEIGHT);
    public static final int PLAYER_WADE_FLOOR_DIST = (PLAYER_HEIGHT - PLAYER_STEP_HEIGHT);
    public static final int PLAYER_JUMP_FLOOR_DIST = (PLAYER_HEIGHT - PLAYER_JUMP_STEP_HEIGHT);
    public static final int PLAYER_FALL_FLOOR_DIST = (PLAYER_HEIGHT - PLAYER_FALL_STEP_HEIGHT);
//    public static final int PLAYER_SWIM_FLOOR_DIST = (PLAYER_SWIM_HEIGHT - PLAYER_SWIM_STEP_HEIGHT);
    public static final int PLAYER_DIVE_FLOOR_DIST = (PLAYER_DIVE_HEIGHT - PLAYER_DIVE_STEP_HEIGHT);
    // FLOOR_DIST variables are the difference in the Players view and the sector
    // floor.
    // Must be at LEAST this distance or you cannot move onto sector.
    public static final int PLAYER_RUN_CEILING_DIST = Z(10);
//    public static final int PLAYER_SWIM_CEILING_DIST = (Z(12));
    public static final int PLAYER_DIVE_CEILING_DIST = (Z(22));
    public static final int PLAYER_CRAWL_CEILING_DIST = (Z(12));
    public static final int PLAYER_JUMP_CEILING_DIST = Z(4);
    public static final int PLAYER_FALL_CEILING_DIST = Z(4);
    public static final int PLAYER_WADE_CEILING_DIST = Z(4);
    public static final int PLAYER_DIVE_MAX_SPEED = (1700);
    public static final int PLAYER_DIVE_INC = (600);

    //
    // DIVE
    //
    public static final int PLAYER_DIVE_BOB_AMT = (Z(8));
    public static final int PLAYER_DIVE_TIME = (12 * 120); // time before damage is taken
//    public static final int PLAYER_DIVE_DAMAGE_AMOUNT = (-1); // amount of damage accessed
    public static final int PLAYER_DIVE_DAMAGE_TIME = (50); // time between damage accessment
    public static final int PLAYER_FLY_MAX_SPEED = (2560);
    public static final int PLAYER_FLY_INC = (1000);

    //
    // FLY
    //
    public static final int PLAYER_FLY_BOB_AMT = (Z(12));
    // Height from which Player will actually call DoPlayerBeginFall()
    public static final int PLAYER_FALL_HEIGHT = Z(28);
//    public static final int PLAYER_FALL_DAMAGE_AMOUNT = (10);
    // dead head height - used in DeathFall
    public static final int PLAYER_DEATH_HEIGHT = (Z(16));
    public static final int PLAYER_DEAD_HEAD_FLOORZ_OFFSET = (Z(7));

    //
    // DEATH
    //
    public static final int PLAYER_NINJA_XREPEAT = (47);
    public static final int PLAYER_NINJA_YREPEAT = (33);
    public static final int PLAYER_MIN_HEIGHT = (Z(20));
    public static final int PLAYER_CRAWL_WADE_DEPTH = (30);
    // public static final int PLAYER_TURN_SCALE (8)
    public static final int PLAYER_TURN_SCALE = (12);
    // the smaller the number the slower the going
    public static final int PLAYER_RUN_FRICTION = (50000);
    // public static final int PLAYER_RUN_FRICTION 0xcb00
    public static final int PLAYER_JUMP_FRICTION = PLAYER_RUN_FRICTION;
    public static final int PLAYER_FALL_FRICTION = PLAYER_RUN_FRICTION;
    public static final int PLAYER_WADE_FRICTION = PLAYER_RUN_FRICTION;
    public static final int PLAYER_FLY_FRICTION = (55808);
    public static final int PLAYER_CRAWL_FRICTION = (45056);
//    public static final int PLAYER_SWIM_FRICTION = (49152);
    public static final int PLAYER_DIVE_FRICTION = (49152);
    // only for z direction climbing
    public static final int PLAYER_CLIMB_FRICTION = (45056);
    // public static final int BOAT_FRICTION 0xd000
    public static final int BOAT_FRICTION = 0xcb00;
    // public static final int TANK_FRICTION 0xcb00
    public static final int TANK_FRICTION = (53248);
    public static final int PLAYER_SLIDE_FRICTION = (53248);
//    public static final int JUMP_STUFF = 4;
    public static final int PLAYER_JUMP_GRAV = 24;
    public static final int PLAYER_JUMP_AMT = (-650);
    public static final int PLAYER_CLIMB_JUMP_AMT = (-1100);
    public static final int MAX_JUMP_DURATION = 12;
    public static final int PlayerGravity = PLAYER_JUMP_GRAV;
    // Current Z position, adjust down to the floor, adjust to player height,
    // adjust for "bump head"
    public static final int PLAYER_STANDING_ROOM = Z(68);

    public static final int PLAYER_NINJA_RATE = 14;
    public static final int PLAYER_NINJA_STAND_RATE = 10;
//    public static final int NINJA_STAR_RATE = 12;
    public static final int PLAYER_NINJA_JUMP_RATE = 24;
    public static final int PLAYER_NINJA_FALL_RATE = 16;
    public static final int PLAYER_NINJA_CLIMB_RATE = 20;
    public static final int PLAYER_NINJA_CRAWL_RATE = 14;

    //////////////////////
    //
    // PLAYER SPECIFIC
    //
    //////////////////////
    public static final int PLAYER_NINJA_SWIM_RATE = 22;
    public static final int NINJA_HeadHurl_RATE = 16;

    //////////////////////
    //
    // PLAYER_NINJA STAND
    //
    //////////////////////
    public static final int NINJA_Head_RATE = 16;
    public static final int NINJA_HeadFly = 1134;
    public static final int NINJA_HeadFly_RATE = 16;

    //////////////////////
    //
    // PLAYER_NINJA JUMP
    //
    //////////////////////
    public static final int NINJA_HeadHurl_FRAMES = 1;
    public static final int NINJA_HeadHurl_R0 = 1147;

    //////////////////////
    //
    // PLAYER_NINJA FALL
    //
    //////////////////////
    public static final int NINJA_HeadHurl_R1 = NINJA_HeadHurl_R0 + (NINJA_HeadHurl_FRAMES);
    public static final int NINJA_HeadHurl_R2 = NINJA_HeadHurl_R0 + (NINJA_HeadHurl_FRAMES * 2);

    //////////////////////
    //
    // PLAYER_NINJA CLIMB
    //
    //////////////////////
    public static final int NINJA_HeadHurl_R3 = NINJA_HeadHurl_R0 + (NINJA_HeadHurl_FRAMES * 3);
    public static final int NINJA_HeadHurl_R4 = NINJA_HeadHurl_R0 + (NINJA_HeadHurl_FRAMES * 4);

    //////////////////////
    //
    // PLAYER_NINJA CRAWL
    //
    //////////////////////
    private static final State[][] s_PlayerHeadHurl = new State[][]{
            {new State(NINJA_HeadHurl_R0, NINJA_HeadHurl_RATE, null).setNext(),},
            {new State(NINJA_HeadHurl_R1, NINJA_HeadHurl_RATE, null).setNext(),},
            {new State(NINJA_HeadHurl_R2, NINJA_HeadHurl_RATE, null).setNext(),},
            {new State(NINJA_HeadHurl_R3, NINJA_HeadHurl_RATE, null).setNext(),},
            {new State(NINJA_HeadHurl_R4, NINJA_HeadHurl_RATE, null).setNext(),}};
    public static final int NINJA_DIE_RATE = 22;

    //////////////////////
    //
    // PLAYER NINJA SWIM
    //
    //////////////////////
    public static final int PLAYER_NINJA_FLY_RATE = 15;
    public static final int PLAYER_NINJA_FLY_R0 = 1200;
    public static final int PLAYER_NINJA_FLY_R1 = 1200;
    public static final int PLAYER_NINJA_FLY_R2 = 1200;
    public static final int PLAYER_NINJA_FLY_R3 = 1200;
    public static final int PLAYER_NINJA_FLY_R4 = 1200;
    public static final Panel_Sprite_Func pSetVisNorm = new Panel_Sprite_Func() {
        @Override
        public void invoke(Panel_Sprite psp) {
            SetVisNorm();
        }
    };
    public static final int PICK_DIST = 40000;
    public static final int HORIZ_SPEED = (16);
    public static final int MAX_SUICIDE = 11;
    public static final int MAX_KILL_NOTES = 16;
//    public static final int PLAYER_DEATH_TILT_VALUE = (32);
    public static final int PLAYER_DEATH_HORIZ_UP_VALUE = (165);
    public static final int PLAYER_DEATH_HORIZ_JUMP_VALUE = (150);
    public static final int PLAYER_DEATH_HORIZ_FALL_VALUE = (50);
    public static final String MSG_GAME_PAUSED = "Game Paused";

    //////////////////////
    //
    // PLAYER NINJA SWORD
    //
    //////////////////////

    private static final int PLAYER_NINJA_SWORD_RATE = 12;

    //////////////////////
    //
    // PLAYER NINJA PUNCH
    //
    //////////////////////
    private static final int PLAYER_NINJA_PUNCH_RATE = 15;
    private static final int TURN_SHIFT = 2;

    //////////////////////
    //
    // PLAYER NINJA FLY
    //
    //////////////////////
    private static final int ADJ_AMT = 8;
    private static final short[] angles = {30, -30};
    private static final int PLAYER_DEATH_GRAV = 8;
    private static final String[] SuicideNote = {"decided to do the graveyard tour.", "had enough and checked out.",
            "didn't fear the Reaper.", "dialed the 1-800-CYANIDE line.", "wasted himself.", "kicked his own ass.",
            "went out in blaze of his own glory.", "killed himself before anyone else could.",
            "needs shooting lessons.", "blew his head off.", "did everyone a favor and offed himself."};
    private static final Player_Action_Func[] PlayerDeathFunc = {DoPlayerDeathFlip,
            DoPlayerDeathCrumble, DoPlayerDeathExplode,
            DoPlayerDeathFlip, DoPlayerDeathExplode,
            DoPlayerDeathDrown};
    private static final int ChopTimer = 30 * 120;
    public static int NormalVisibility;
    public static final Target_Sort[] TargetSort = new Target_Sort[MAX_TARGET_SORT];

    /////////////////////////////////////////////////////////////////////////////
    public static int TargetSortCount;
    private static final State[][] s_PlayerNinjaRun = new State[][]{
            {new State(PLAYER_NINJA_RUN_R0, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R0 + 1, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R0 + 1, SF_QUICK_CALL, DoFootPrints),
                    new State(PLAYER_NINJA_RUN_R0 + 2, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R0 + 3, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R0 + 3, SF_QUICK_CALL, DoFootPrints),},
            {new State(PLAYER_NINJA_RUN_R1, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R1 + 1, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R1 + 1, SF_QUICK_CALL, DoFootPrints),
                    new State(PLAYER_NINJA_RUN_R1 + 2, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R1 + 3, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R1 + 3, SF_QUICK_CALL, DoFootPrints),},
            {new State(PLAYER_NINJA_RUN_R2, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R2 + 1, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R2 + 1, SF_QUICK_CALL, DoFootPrints),
                    new State(PLAYER_NINJA_RUN_R2 + 2, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R2 + 3, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R2 + 3, SF_QUICK_CALL, DoFootPrints),},
            {new State(PLAYER_NINJA_RUN_R3, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R3 + 1, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R3 + 1, SF_QUICK_CALL, DoFootPrints),
                    new State(PLAYER_NINJA_RUN_R3 + 2, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R3 + 3, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R3 + 3, SF_QUICK_CALL, DoFootPrints),},
            {new State(PLAYER_NINJA_RUN_R4, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R4 + 1, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R4 + 1, SF_QUICK_CALL, DoFootPrints),
                    new State(PLAYER_NINJA_RUN_R4 + 2, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R4 + 3, PLAYER_NINJA_RATE | SF_TIC_ADJUST, null),
                    new State(PLAYER_NINJA_RUN_R4 + 3, SF_QUICK_CALL, DoFootPrints),}};
    private static final State[][] s_PlayerNinjaStand = new State[][]{
            {new State(PLAYER_NINJA_STAND_R0, PLAYER_NINJA_STAND_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_STAND_R1, PLAYER_NINJA_STAND_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_STAND_R2, PLAYER_NINJA_STAND_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_STAND_R3, PLAYER_NINJA_STAND_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_STAND_R4, PLAYER_NINJA_STAND_RATE, null).setNext(),}};
    private static final State[][] s_PlayerNinjaJump = new State[][]{
            {new State(PLAYER_NINJA_JUMP_R0, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R0 + 1, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R0 + 2, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R0 + 3, PLAYER_NINJA_JUMP_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_JUMP_R1, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R1 + 1, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R1 + 2, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R1 + 3, PLAYER_NINJA_JUMP_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_JUMP_R2, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R2 + 1, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R2 + 2, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R2 + 3, PLAYER_NINJA_JUMP_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_JUMP_R3, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R3 + 1, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R3 + 2, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R3 + 3, PLAYER_NINJA_JUMP_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_JUMP_R4, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R4 + 1, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R4 + 2, PLAYER_NINJA_JUMP_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R4 + 3, PLAYER_NINJA_JUMP_RATE, null).setNext(),}};
    private static final State[][] s_PlayerNinjaFall = new State[][]{
            {new State(PLAYER_NINJA_JUMP_R0 + 1, PLAYER_NINJA_FALL_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R0 + 2, PLAYER_NINJA_FALL_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_JUMP_R1 + 1, PLAYER_NINJA_FALL_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R1 + 2, PLAYER_NINJA_FALL_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_JUMP_R2 + 1, PLAYER_NINJA_FALL_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R2 + 2, PLAYER_NINJA_FALL_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_JUMP_R3 + 1, PLAYER_NINJA_FALL_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R3 + 2, PLAYER_NINJA_FALL_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_JUMP_R4 + 1, PLAYER_NINJA_FALL_RATE, null),
                    new State(PLAYER_NINJA_JUMP_R4 + 2, PLAYER_NINJA_FALL_RATE, null).setNext(),}};
    private static final State[][] s_PlayerNinjaClimb = new State[][]{
            {new State(PLAYER_NINJA_CLIMB_R0, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R0 + 1, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R0 + 2, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R0 + 3, PLAYER_NINJA_CLIMB_RATE, null),},
            {new State(PLAYER_NINJA_CLIMB_R1, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R1 + 1, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R1 + 2, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R1 + 3, PLAYER_NINJA_CLIMB_RATE, null),},
            {new State(PLAYER_NINJA_CLIMB_R2, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R2 + 1, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R2 + 2, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R2 + 3, PLAYER_NINJA_CLIMB_RATE, null),},
            {new State(PLAYER_NINJA_CLIMB_R3, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R3 + 1, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R3 + 2, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R3 + 3, PLAYER_NINJA_CLIMB_RATE, null),},
            {new State(PLAYER_NINJA_CLIMB_R4, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R4 + 1, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R4 + 2, PLAYER_NINJA_CLIMB_RATE, null),
                    new State(PLAYER_NINJA_CLIMB_R4 + 3, PLAYER_NINJA_CLIMB_RATE, null),}};
    private static final State[][] s_PlayerNinjaCrawl = new State[][]{
            {new State(PLAYER_NINJA_CRAWL_R0, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R0 + 1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R0 + 1, SF_QUICK_CALL, DoFootPrints),
                    new State(PLAYER_NINJA_CRAWL_R0 + 2, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R0 + 1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R0 + 1, SF_QUICK_CALL, DoFootPrints),},
            {new State(PLAYER_NINJA_CRAWL_R1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R1 + 1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R1 + 1, SF_QUICK_CALL, DoFootPrints),
                    new State(PLAYER_NINJA_CRAWL_R1 + 2, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R1 + 1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R1 + 1, SF_QUICK_CALL, DoFootPrints),},
            {new State(PLAYER_NINJA_CRAWL_R2, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R2 + 1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R2 + 1, SF_QUICK_CALL, DoFootPrints),
                    new State(PLAYER_NINJA_CRAWL_R2 + 2, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R2 + 1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R2 + 1, SF_QUICK_CALL, DoFootPrints),},
            {new State(PLAYER_NINJA_CRAWL_R3, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R3 + 1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R3 + 1, SF_QUICK_CALL, DoFootPrints),
                    new State(PLAYER_NINJA_CRAWL_R3 + 2, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R3 + 1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R3 + 1, SF_QUICK_CALL, DoFootPrints),},
            {new State(PLAYER_NINJA_CRAWL_R4, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R4 + 1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R4 + 1, SF_QUICK_CALL, DoFootPrints),
                    new State(PLAYER_NINJA_CRAWL_R4 + 2, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R4 + 1, PLAYER_NINJA_CRAWL_RATE, null),
                    new State(PLAYER_NINJA_CRAWL_R4 + 1, SF_QUICK_CALL, DoFootPrints),}};
    private static final State[][] s_PlayerNinjaSwim = new State[][]{
            {new State(PLAYER_NINJA_SWIM_R0, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R0 + 1, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R0 + 2, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R0 + 3, PLAYER_NINJA_SWIM_RATE, null),},
            {new State(PLAYER_NINJA_SWIM_R1, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R1 + 1, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R1 + 2, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R1 + 3, PLAYER_NINJA_SWIM_RATE, null),},
            {new State(PLAYER_NINJA_SWIM_R2, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R2 + 1, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R2 + 2, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R2 + 3, PLAYER_NINJA_SWIM_RATE, null),},
            {new State(PLAYER_NINJA_SWIM_R3, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R3 + 1, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R3 + 2, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R3 + 3, PLAYER_NINJA_SWIM_RATE, null),},
            {new State(PLAYER_NINJA_SWIM_R4, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R4 + 1, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R4 + 2, PLAYER_NINJA_SWIM_RATE, null),
                    new State(PLAYER_NINJA_SWIM_R4 + 3, PLAYER_NINJA_SWIM_RATE, null),}};
    private static final State[][] s_PlayerHeadFly = new State[][]{
            {new State(NINJA_HeadFly, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 1, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 2, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 3, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 4, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 5, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 6, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 7, NINJA_HeadFly_RATE, null),},
            {new State(NINJA_HeadFly, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 1, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 2, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 3, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 4, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 5, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 6, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 7, NINJA_HeadFly_RATE, null),},
            {new State(NINJA_HeadFly, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 1, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 2, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 3, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 4, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 5, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 6, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 7, NINJA_HeadFly_RATE, null),},
            {new State(NINJA_HeadFly, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 1, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 2, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 3, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 4, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 5, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 6, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 7, NINJA_HeadFly_RATE, null),},
            {new State(NINJA_HeadFly, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 1, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 2, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 3, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 4, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 5, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 6, NINJA_HeadFly_RATE, null),
                    new State(NINJA_HeadFly + 7, NINJA_HeadFly_RATE, null),}};
    private static final State[][] s_PlayerHead = new State[][]{
            {new State(NINJA_Head_R0, NINJA_Head_RATE, null).setNext(),},
            {new State(NINJA_Head_R1, NINJA_Head_RATE, null).setNext(),},
            {new State(NINJA_Head_R2, NINJA_Head_RATE, null).setNext(),},
            {new State(NINJA_Head_R3, NINJA_Head_RATE, null).setNext(),},
            {new State(NINJA_Head_R4, NINJA_Head_RATE, null).setNext(),}};
    private static final State[][] s_PlayerDeath = new State[][]{
            {new State(PLAYER_NINJA_DIE, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 1, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 2, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 3, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 4, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 5, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 6, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 7, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 8, SF_QUICK_CALL, QueueFloorBlood),
                    new State(PLAYER_NINJA_DIE + 8, NINJA_DIE_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_DIE, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 1, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 2, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 3, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 4, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 5, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 6, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 7, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 8, SF_QUICK_CALL, QueueFloorBlood),
                    new State(PLAYER_NINJA_DIE + 8, NINJA_DIE_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_DIE, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 1, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 2, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 3, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 4, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 5, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 6, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 7, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 8, SF_QUICK_CALL, QueueFloorBlood),
                    new State(PLAYER_NINJA_DIE + 8, NINJA_DIE_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_DIE, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 1, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 2, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 3, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 4, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 5, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 6, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 7, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 8, SF_QUICK_CALL, QueueFloorBlood),
                    new State(PLAYER_NINJA_DIE + 8, NINJA_DIE_RATE, null).setNext(),},
            {new State(PLAYER_NINJA_DIE, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 1, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 2, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 3, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 4, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 5, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 6, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 7, NINJA_DIE_RATE, null),
                    new State(PLAYER_NINJA_DIE + 8, SF_QUICK_CALL, QueueFloorBlood),
                    new State(PLAYER_NINJA_DIE + 8, NINJA_DIE_RATE, null).setNext(),}};
    private static final State[][] s_PlayerNinjaSword = new State[][]{{ // 0 - Front view
            new State(PLAYER_NINJA_SWORD_R0, PLAYER_NINJA_SWORD_RATE, null),
            new State(PLAYER_NINJA_SWORD_R0 + 1, PLAYER_NINJA_SWORD_RATE, null),
            new State(PLAYER_NINJA_SWORD_R0 + 2, PLAYER_NINJA_SWORD_RATE, null),
            new State(PLAYER_NINJA_SWORD_R0 + 2, PLAYER_NINJA_SWORD_RATE | SF_PLAYER_FUNC, DoPlayerSpriteReset)},

            { // 1
                    new State(PLAYER_NINJA_SWORD_R1, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R1 + 1, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R1 + 2, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R1 + 2, PLAYER_NINJA_SWORD_RATE | SF_PLAYER_FUNC,
                            DoPlayerSpriteReset),},

            { // 2
                    new State(PLAYER_NINJA_SWORD_R2, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R2 + 1, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R2 + 2, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R2 + 2, PLAYER_NINJA_SWORD_RATE | SF_PLAYER_FUNC,
                            DoPlayerSpriteReset),},

            { // 3
                    new State(PLAYER_NINJA_SWORD_R3, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R3 + 1, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R3 + 2, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R3 + 2, PLAYER_NINJA_SWORD_RATE | SF_PLAYER_FUNC,
                            DoPlayerSpriteReset),},

            { // 4
                    new State(PLAYER_NINJA_SWORD_R4, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R4 + 1, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R4 + 2, PLAYER_NINJA_SWORD_RATE, null),
                    new State(PLAYER_NINJA_SWORD_R4 + 2, PLAYER_NINJA_SWORD_RATE | SF_PLAYER_FUNC,
                            DoPlayerSpriteReset),}};
    private static final State[][] s_PlayerNinjaPunch = new State[][]{{ // 0 - Front view
            new State(PLAYER_NINJA_PUNCH_R0, PLAYER_NINJA_PUNCH_RATE, null),
            new State(PLAYER_NINJA_PUNCH_R0 + 1, PLAYER_NINJA_PUNCH_RATE, null),
            new State(PLAYER_NINJA_PUNCH_R0 + 1, PLAYER_NINJA_PUNCH_RATE | SF_PLAYER_FUNC, DoPlayerSpriteReset)
                    .setNext(),},

            { // 1
                    new State(PLAYER_NINJA_PUNCH_R1, PLAYER_NINJA_PUNCH_RATE, null),
                    new State(PLAYER_NINJA_PUNCH_R1 + 1, PLAYER_NINJA_PUNCH_RATE, null),
                    new State(PLAYER_NINJA_PUNCH_R1 + 1, PLAYER_NINJA_PUNCH_RATE | SF_PLAYER_FUNC, DoPlayerSpriteReset)
                            .setNext(),},

            { // 2
                    new State(PLAYER_NINJA_PUNCH_R2, PLAYER_NINJA_PUNCH_RATE, null),
                    new State(PLAYER_NINJA_PUNCH_R2 + 1, PLAYER_NINJA_PUNCH_RATE, null),
                    new State(PLAYER_NINJA_PUNCH_R2 + 1, PLAYER_NINJA_PUNCH_RATE | SF_PLAYER_FUNC, DoPlayerSpriteReset)
                            .setNext(),},

            { // 3
                    new State(PLAYER_NINJA_PUNCH_R3, PLAYER_NINJA_PUNCH_RATE, null),
                    new State(PLAYER_NINJA_PUNCH_R3 + 1, PLAYER_NINJA_PUNCH_RATE, null),
                    new State(PLAYER_NINJA_PUNCH_R3 + 1, PLAYER_NINJA_PUNCH_RATE | SF_PLAYER_FUNC, DoPlayerSpriteReset)
                            .setNext(),},

            { // 4
                    new State(PLAYER_NINJA_PUNCH_R4, PLAYER_NINJA_PUNCH_RATE, null),
                    new State(PLAYER_NINJA_PUNCH_R4 + 1, PLAYER_NINJA_PUNCH_RATE, null),
                    new State(PLAYER_NINJA_PUNCH_R4 + 1, PLAYER_NINJA_PUNCH_RATE | SF_PLAYER_FUNC, DoPlayerSpriteReset)
                            .setNext(),}};
    private static final State[][] s_PlayerNinjaFly = new State[][]{
            {new State(PLAYER_NINJA_FLY_R0, PLAYER_NINJA_FLY_RATE, null).setNext()},
            {new State(PLAYER_NINJA_FLY_R1, PLAYER_NINJA_FLY_RATE, null).setNext()},
            {new State(PLAYER_NINJA_FLY_R2, PLAYER_NINJA_FLY_RATE, null).setNext()},
            {new State(PLAYER_NINJA_FLY_R3, PLAYER_NINJA_FLY_RATE, null).setNext()},
            {new State(PLAYER_NINJA_FLY_R4, PLAYER_NINJA_FLY_RATE, null).setNext()}};
    private static int count = 0;
    private static final int[] x = new int[4];
    private static final int[] y = new int[4];
    private static final int[] ox = new int[4];
    private static final int[] oy = new int[4];
    private static VOC3D handle = null;
    private static final int[] angs = {0, 0, 0};
    private static final int[] sf = {0, 0}; // sectors found
    private static final int[] UnderStatList = {STAT_UNDERWATER, STAT_UNDERWATER2};
    private static final boolean[] SpawnPositionUsed = new boolean[MAX_SW_PLAYERS + 1];
    private static final int[] MultiStatList = {STAT_MULTI_START, STAT_CO_OP_START};

    public static void InitPlayerStates() {
        for (PlayerStateGroup sg : PlayerStateGroup.values()) {
            for (int rot = 0; rot < 5; rot++) {
                State.InitState(sg.group[rot]);
            }
        }
    }

    public static void DoPlayerSpriteThrow(PlayerStr pp) {
        if (!TEST(pp.Flags, PF_DIVING | PF_FLYING | PF_CRAWLING)) {
            USER pu = getUser(pp.PlayerSprite);
            if (pp.CurWpn == pp.Wpn[WPN_SWORD] && pu != null && pu.getRot() != PlayerStateGroup.sg_PlayerNinjaSword) {
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerNinjaSword);
            } else {
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerNinjaPunch);
            }
        }
    }

    public static void DoPlayerSpriteReset(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null || u.PlayerP == -1) {
            return;
        }

        PlayerStr pp = Player[u.PlayerP];

        // need to figure out what frames to put sprite into
        if (pp.DoPlayerAction == DoPlayerCrawl) {
            NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Crawl);
        } else {
            if (TEST(pp.Flags, PF_PLAYER_MOVED)) {
                NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Run);
            } else {
                NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Stand);
            }
        }
    }

    public static void SetVisHigh() {
//	    visibility = NormalVisibility>>1;
    }

    public static void SetVisNorm() {
//	    visibility = NormalVisibility;
    }

    public static int GetDeltaAngle(int ang1, int ang2) {
        // Look at the smaller angle if > 1024 (180 degrees)
        if (klabs(ang1 - ang2) > 1024) {
            if (ang1 <= 1024) {
                ang1 += 2048;
            }

            if (ang2 <= 1024) {
                ang2 += 2048;
            }
        }
        return  (ang1 - ang2);
    }

    public static int DoPickTarget(int spnum, int max_delta_ang, int skip_targets) {
        Sprite sp = boardService.getSprite(spnum);
        if (sp == null) {
            return 0;
        }

        USER u = getUser(spnum);
        // !JIM! Watch out for max_delta_ang of zero!
        if (max_delta_ang == 0) {
            max_delta_ang = 1;
        }

        TargetSortCount = 0;
        if (TargetSort[0] == null) {
            TargetSort[0] = new Target_Sort();
        }
        TargetSort[0].sprite_num = -1;

        ListNode<Sprite> nexti;
        for (int j : StatDamageList) {
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite ep = node.get();
                USER eu = getUser(i);
                if (eu == null) {
                    continue;
                }

                // don't pick yourself
                if (i == spnum) {
                    continue;
                }

                if (skip_targets != 2) // Used for spriteinfo mode
                {
                    if (skip_targets != 0 && TEST(eu.Flags, SPR_TARGETED)) {
                        continue;
                    }

                    // don't pick a dead player
                    if (eu.PlayerP != -1 && TEST(Player[eu.PlayerP].Flags, PF_DEAD)) {
                        continue;
                    }
                }

                int dist = FindDistance3D(sp.getX() - ep.getX(), sp.getY() - ep.getY(), (sp.getZ() - ep.getZ()) >> 4);
                // Only look at closest ones
                if (dist > PICK_DIST) {
                    continue;
                }

                if (skip_targets != 2) // Used for spriteinfo mode
                {
                    // don't set off mine
                    if (!TEST(ep.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                        continue;
                    }
                }

                // Get the angle to the player
                int angle2 = NORM_ANGLE(EngineUtils.getAngle(ep.getX() - sp.getX(), ep.getY() - sp.getY()));

                // Get the angle difference
                // delta_ang = labs(pp.pang - angle2);

                int delta_ang = klabs(GetDeltaAngle(sp.getAng(), angle2));
                // If delta_ang not in the range skip this one
                if (delta_ang > max_delta_ang) {
                    continue;
                }

                int zh;
                if (u != null && u.PlayerP != -1) {
                    zh = Player[u.PlayerP].posz;
                } else {
                    zh = SPRITEp_TOS(sp) + DIV4(SPRITEp_SIZE_Z(sp));
                }

                int ezh = SPRITEp_TOS(ep) + DIV4(SPRITEp_SIZE_Z(ep));
                int ezhm = SPRITEp_TOS(ep) + DIV2(SPRITEp_SIZE_Z(ep));
                int ezhl = SPRITEp_BOS(ep) - DIV4(SPRITEp_SIZE_Z(ep));

                // If you can't see 'em you can't shoot 'em
                if (!FAFcansee(sp.getX(), sp.getY(), zh, sp.getSectnum(), ep.getX(), ep.getY(), ezh, ep.getSectnum())
                        && !FAFcansee(sp.getX(), sp.getY(), zh, sp.getSectnum(), ep.getX(), ep.getY(), ezhm, ep.getSectnum())
                        && !FAFcansee(sp.getX(), sp.getY(), zh, sp.getSectnum(), ep.getX(), ep.getY(), ezhl, ep.getSectnum())) {
                    continue;
                }

                // get ndx - there is only room for 15
                int ndx;
                if (TargetSortCount > TargetSort.length - 1) {
                    for (ndx = 0; ndx < TargetSort.length; ndx++) {
                        Target_Sort ts = TargetSort[ndx];
                        if (ts != null && dist < ts.dist) {
                            break;
                        }
                    }

                    if (ndx == TargetSort.length) {
                        continue;
                    }
                } else {
                    ndx = TargetSortCount;
                }

                if (TargetSort[ndx] == null) {
                    TargetSort[ndx] = new Target_Sort();
                }

                Target_Sort ts = TargetSort[ndx];
                ts.sprite_num = i;
                ts.dang = delta_ang;
                ts.dist = dist;
                // gives a value between 0 and 65535
                int ang_weight = ((max_delta_ang - ts.dang) << 16) / max_delta_ang;
                // gives a value between 0 and 65535
                int dist_weight = ((DIV2(PICK_DIST) - DIV2(ts.dist)) << 16) / DIV2(PICK_DIST);
                // weighted average
                ts.weight = (ang_weight + dist_weight * 4) / 5;

                TargetSortCount++;
                if (TargetSortCount >= TargetSort.length) {
                    TargetSortCount = TargetSort.length;
                }
            }
        }

        if (TargetSortCount > 1) {
            Arrays.sort(TargetSort, 0, TargetSortCount); // qsort(TargetSort, TargetSortCount);
        }

        if (TargetSort[0] != null) {
            return (TargetSort[0].sprite_num);
        }
        return -1;
    }

    public static void DoPlayerResetMovement(PlayerStr pp) {
        pp.xvect = 0;
        pp.yvect = 0;
        pp.oxvect = 0;
        pp.slide_xvect = 0;
        pp.slide_yvect = 0;
        pp.drive_angvel = 0;
        pp.drive_oangvel = 0;
        pp.Flags &= ~(PF_PLAYER_MOVED);
    }

    public static void DoPlayerTeleportPause(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);

        // set this so we don't get stuck in teleporting loop
        pp.lastcursectnum = pp.cursectnum;

        if (u != null && (u.WaitTics -= synctics) <= 0) {
            pp.Flags2 &= ~(PF2_TELEPORTED);
            DoPlayerResetMovement(pp);
            DoPlayerBeginRun(pp);
        }
    }

    public static void DoPlayerTeleportToSprite(PlayerStr pp, Sprite sp) {
        pp.pang = pp.oang = sp.getAng();
        pp.posx = pp.oposx = pp.oldposx = sp.getX();
        pp.posy = pp.oposy = pp.oldposy = sp.getY();
        pp.posz = pp.oposz = sp.getZ() - PLAYER_HEIGHT;

        pp.cursectnum = COVERupdatesector(pp.posx, pp.posy, pp.cursectnum);
        pp.Flags2 |= (PF2_TELEPORTED);
    }

    public static void DoPlayerTeleportToOffset(PlayerStr pp) {

        pp.oposx = pp.oldposx = pp.posx;
        pp.oposy = pp.oldposy = pp.posy;

        pp.cursectnum = COVERupdatesector(pp.posx, pp.posy, pp.cursectnum);
        // pp.lastcursectnum = pp.cursectnum;
        pp.Flags2 |= (PF2_TELEPORTED);
    }

    public static void DoSpawnTeleporterEffect(Sprite sp) {
        int nx = MOVEx(512, sp.getAng());
        int ny = MOVEy(512, sp.getAng());

        nx += sp.getX();
        ny += sp.getY();

        int effect = SpawnSprite(STAT_MISSILE, 0, s_TeleportEffect[0], sp.getSectnum(), nx, ny, SPRITEp_TOS(sp) + Z(16), sp.getAng(),
                0);

        Sprite ep = boardService.getSprite(effect);
        if (ep == null) {
            return;
        }

        engine.setspritez(effect, ep.getX(), ep.getY(), ep.getZ());

        ep.setShade(-40);
        ep.setXrepeat(42);
        ep.setYrepeat(42);
        ep.setCstat(ep.getCstat() | (CSTAT_SPRITE_YCENTER));
        ep.setCstat(ep.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        ep.setCstat(ep.getCstat() | (CSTAT_SPRITE_WALL));
    }

    public static void DoSpawnTeleporterEffectPlace(Sprite sp) {
        int effect = SpawnSprite(STAT_MISSILE, 0, s_TeleportEffect[0], sp.getSectnum(), sp.getX(), sp.getY(), SPRITEp_TOS(sp) + Z(16),
                sp.getAng(), 0);

        Sprite ep = boardService.getSprite(effect);
        if (ep == null) {
            return;
        }

        engine.setspritez(effect, ep.getX(), ep.getY(), ep.getZ());

        ep.setShade(-40);
        ep.setXrepeat(42);
        ep.setYrepeat(42);
        ep.setCstat(ep.getCstat() | (CSTAT_SPRITE_YCENTER));
        ep.setCstat(ep.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        ep.setCstat(ep.getCstat() | (CSTAT_SPRITE_WALL));
    }

    public static void DoPlayerWarpTeleporter(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sprite sp = pp.getSprite();
        Sprite sp_warp = WarpM(pp.posx, pp.posy, pp.posz, pp.cursectnum);
        if (sp == null || u == null || sp_warp == null) {
            return;
        }

        pp.posx = warp.x;
        pp.posy = warp.y;
        pp.posz = warp.z;
        pp.cursectnum = warp.sectnum;

        if (SP_TAG3(sp_warp) == 1) {
            DoPlayerTeleportToOffset(pp);
            UpdatePlayerSprite(pp);
        } else {
            DoPlayerTeleportToSprite(pp, sp_warp);

            PlaySound(DIGI_TELEPORT, pp, v3df_none);

            DoPlayerResetMovement(pp);

            u.WaitTics = 30;
            DoPlayerBeginRun(pp);
            pp.DoPlayerAction = DoPlayerTeleportPause;

            NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Stand);

            UpdatePlayerSprite(pp);
            DoSpawnTeleporterEffect(sp);

            for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                if (Player[pnum] != pp) {
                    PlayerStr npp = Player[pnum];

                    // if someone already standing there
                    if (npp.cursectnum == pp.cursectnum) {
                        PlayerUpdateHealth(npp, -u.Health); // Make sure he dies!
                        // telefraged by teleporting player
                        // PlayerCheckDeath(npp, npp.PlayerSprite);
                        PlayerCheckDeath(npp, pp.PlayerSprite);
                    }
                }
            }
        }

        u.ox = sp.getX();
        u.oy = sp.getY();
        u.oz = sp.getZ();
    }

    public static void DoPlayerSetWadeDepth(PlayerStr pp) {
        Sector sectp = boardService.getSector(pp.lo_sectp);
        Sect_User su = getSectUser(pp.lo_sectp);
        pp.WadeDepth = 0;
        if (su == null || sectp == null) {
            return;
        }

        if (TEST(sectp.getExtra(), SECTFX_SINK)) {
            // make sure your even in the water
            if (pp.posz + PLAYER_HEIGHT > sectp.getFloorz() - Z(su.depth)) {
                pp.WadeDepth = su.depth;
            }
        }
    }

    public static void DoPlayerHeight(PlayerStr pp) {
        int diff = pp.posz - (pp.loz - PLAYER_HEIGHT);

        pp.posz = pp.posz - (DIV4(diff) + DIV8(diff));
    }

    public static void DoPlayerJumpHeight(PlayerStr pp) {
        Sector sectp = boardService.getSector(pp.lo_sectp);
        if (sectp != null && TEST(sectp.getExtra(), SECTFX_DYNAMIC_AREA)) {
            if (pp.posz + PLAYER_HEIGHT > pp.loz) {
                pp.posz = pp.loz - PLAYER_HEIGHT;
                DoPlayerBeginRun(pp);
            }
        }
    }

    public static void DoPlayerCrawlHeight(PlayerStr pp) {
        int diff = pp.posz - (pp.loz - PLAYER_CRAWL_HEIGHT);
        pp.posz = pp.posz - (DIV4(diff) + DIV8(diff));
    }

    public static void DoPlayerTurn(PlayerStr pp) {
        Sprite ppSpr = pp.getSprite();
        if (ppSpr == null) {
            return;
        }

        if (!TEST(pp.Flags, PF_TURN_180)) {
            if (TEST_SYNC_KEY(pp, SK_TURN_180)) {
                if (FLAG_KEY_PRESSED(pp, SK_TURN_180)) {
                    int delta_ang;

                    FLAG_KEY_RELEASE(pp, SK_TURN_180);

                    pp.turn180_target = NORM_ANGLE(pp.getAnglei() + 1024);

                    // make the first turn in the clockwise direction
                    // the rest will follow
                    delta_ang = GetDeltaAngle(pp.turn180_target, pp.getAnglei());
                    pp.pang = NORM_ANGLE(pp.getAnglei() + (klabs(delta_ang) >> TURN_SHIFT));

                    pp.Flags |= (PF_TURN_180);
                }
            } else {
                FLAG_KEY_RESET(pp, SK_TURN_180);
            }
        }

        if (TEST(pp.Flags, PF_TURN_180)) {
            int delta_ang = GetDeltaAngle(pp.turn180_target, pp.getAnglei());
            if (!isOriginal()) {
                pp.pang += (delta_ang >> TURN_SHIFT);
                pp.pang = BClampAngle(pp.pang);
            } else {
                pp.pang = NORM_ANGLE(pp.getAnglei() + (delta_ang >> TURN_SHIFT));
            }

            ppSpr.setAng(pp.getAnglei());
            if (!Prediction) {
                Sprite underwaterSpr = boardService.getSprite(pp.PlayerUnderSprite);
                if (underwaterSpr != null) {
                    underwaterSpr.setAng(pp.getAnglei());
                }
            }

            // get new delta to see how close we are
            delta_ang = GetDeltaAngle(pp.turn180_target, pp.getAnglei());

            if (klabs(delta_ang) < (3 << TURN_SHIFT)) {
                pp.pang = pp.turn180_target;
                pp.Flags &= ~(PF_TURN_180);
            } else {
                return;
            }
        }

        float angvel = (pp.input.angvel * PLAYER_TURN_SCALE);

        if (angvel != 0) {
            // running is not handled here now
            if (!isOriginal()) {
                angvel += angvel / 4.0f;

                pp.pang += (angvel * synctics) / 32.0f;
                pp.pang = BClampAngle(pp.pang);
            } else {
                angvel += DIV4((int) angvel);

                pp.pang += DIV32((int) angvel * synctics);
                pp.pang = NORM_ANGLE(pp.getAnglei());
            }

            // update players sprite angle
            // NOTE: It's also updated in UpdatePlayerSprite, but needs to be
            // here to cover
            // all cases.
            ppSpr.setAng(pp.getAnglei());
            if (!Prediction) {
                Sprite underwaterSpr = boardService.getSprite(pp.PlayerUnderSprite);
                if (underwaterSpr != null) {
                    underwaterSpr.setAng(pp.getAnglei());
                }
            }
        }
    }

    public static void DoPlayerTurnBoat(PlayerStr pp) {
        float angvel;
        Sector_Object sop = SectorObject[pp.sop];

        if (sop.drive_angspeed != 0) {
            pp.drive_oangvel = pp.drive_angvel;
            pp.drive_angvel = mulscale((int) pp.input.angvel, sop.drive_angspeed, 16);

            int angslide = sop.drive_angslide;
            pp.drive_angvel = (int) ((pp.drive_angvel + (pp.drive_oangvel * (angslide - 1))) / angslide);

            angvel = pp.drive_angvel;
        } else {
            angvel = pp.input.angvel * PLAYER_TURN_SCALE;
            if (!isOriginal()) {
                angvel += angvel - angvel / 4.0f;
                angvel = (angvel * synctics) / 32.0f;
            } else {
                angvel += angvel - DIV4((int) angvel);
                angvel = DIV32((int) angvel * synctics);
            }
        }

        if (angvel != 0) {
            if (!isOriginal()) {
                pp.pang += angvel;
                pp.pang = BClampAngle(pp.pang);
            } else {
                pp.pang = NORM_ANGLE((int) (pp.getAnglei() + angvel));
            }

            Sprite s = pp.getSprite();
            if (s != null) {
                s.setAng(pp.getAnglei());
            }
        }
    }

    public static void DoPlayerTurnTank(PlayerStr pp, int z, int floor_dist) {
        float angvel;
        Sector_Object sop = SectorObject[pp.sop];

        if (sop.drive_angspeed != 0) {
            long angslide;

            pp.drive_oangvel = pp.drive_angvel;
            pp.drive_angvel = mulscale((int) pp.input.angvel, sop.drive_angspeed, 16);

            angslide = sop.drive_angslide;
            pp.drive_angvel = (int) ((pp.drive_angvel + (pp.drive_oangvel * (angslide - 1))) / angslide);

            angvel = pp.drive_angvel;
        } else {
            angvel = pp.input.angvel * synctics / 8.0f;
        }

        if (angvel != 0) {
            if (MultiClipTurn(pp, NORM_ANGLE((int) (pp.getAnglef() + angvel)), z, floor_dist)) {
                if (!isOriginal()) {
                    pp.pang += angvel;
                    pp.pang = BClampAngle(pp.pang);
                } else {
                    pp.pang = NORM_ANGLE((int) (pp.getAnglei() + angvel));
                }
                Sprite s = pp.getSprite();
                if (s != null) {
                    s.setAng(pp.getAnglei());
                }
            }
        }
    }

    public static void DoPlayerTurnTankRect(PlayerStr pp, int[] x, int[] y, int[] ox, int[] oy) {
        float angvel;
        Sector_Object sop = SectorObject[pp.sop];

        if (sop.drive_angspeed != 0) {
            int angslide;

            pp.drive_oangvel = pp.drive_angvel;
            pp.drive_angvel = mulscale((int) pp.input.angvel, sop.drive_angspeed, 16);

            angslide = sop.drive_angslide;
            pp.drive_angvel = (pp.drive_angvel + (pp.drive_oangvel * (angslide - 1))) / angslide;

            angvel = pp.drive_angvel;
        } else {
            angvel = (pp.input.angvel * synctics) / 8.0f;
        }

        if (angvel != 0) {
            if (RectClipTurn(pp, NORM_ANGLE((int) (pp.getAnglef() + angvel)), x, y, ox, oy)) {
                if (!isOriginal()) {
                    pp.pang += angvel;
                    pp.pang = BClampAngle(pp.pang);
                } else {
                    pp.pang = NORM_ANGLE((int) (pp.getAnglei() + angvel));
                }
                Sprite s = pp.getSprite();
                if (s != null) {
                    s.setAng(pp.getAnglei());
                }
            }
        }
    }

    public static void DoPlayerTurnTurret(PlayerStr pp) {
        float angvel;
        int diff;
        Sector_Object sop = SectorObject[pp.sop];
        Input last_input;
        int fifo_ndx;

        if (!Prediction) {
            // this code looks at the fifo to get the last value for comparison

            fifo_ndx = (game.pNet.gNetFifoTail - 2) & (MOVEFIFOSIZ - 1);
            last_input = (Input) game.pNet.gFifoInput[fifo_ndx][pp.pnum];

            if (pp.input.angvel != 0 && last_input.angvel == 0) {
                PlaySOsound(sop.mid_sector, SO_DRIVE_SOUND);
            } else if (pp.input.angvel == 0 && last_input.angvel != 0) {
                PlaySOsound(sop.mid_sector, SO_IDLE_SOUND);
            }
        }

        if (sop.drive_angspeed != 0) {
            int angslide;

            pp.drive_oangvel = pp.drive_angvel;
            pp.drive_angvel = pp.input.angvel * sop.drive_angspeed / 65536.0f; //mulscale((int)pp.input.angvel, sop.drive_angspeed, 16);

            angslide = sop.drive_angslide;
            pp.drive_angvel = (pp.drive_angvel + (pp.drive_oangvel * (angslide - 1))) / angslide;

            angvel = pp.drive_angvel;
        } else {
            angvel = (pp.input.angvel * synctics) / 4.0f;
        }

        if (angvel != 0) {
            float new_ang = pp.getAnglef() + angvel;
            if (isOriginal()) {
                new_ang = NORM_ANGLE((int) (pp.getAnglei() + angvel));
            } else {
                new_ang = BClampAngle(new_ang);
            }

            if (sop.limit_ang_center >= 0) {
                diff = GetDeltaAngle((int) new_ang, sop.limit_ang_center);

                if (klabs(diff) >= sop.limit_ang_delta) {
                    if (diff < 0) {
                        new_ang =  (sop.limit_ang_center - sop.limit_ang_delta);
                    } else {
                        new_ang =  (sop.limit_ang_center + sop.limit_ang_delta);
                    }

                }
            }

            pp.pang = new_ang;
            Sprite s = pp.getSprite();
            if (s != null) {
                s.setAng(pp.getAnglei());
            }
        }
    }

    public static void SlipSlope(PlayerStr pp) {
        Sector sec = boardService.getSector(pp.cursectnum);
        if (sec == null) {
            return;
        }

        Sect_User sectu = getSectUser(pp.cursectnum);
        if (sectu == null || !TEST(sectu.flags, SECTFU_SLIDE_SECTOR)
                || !TEST(sec.getFloorstat(), FLOOR_STAT_SLOPE)) {
            return;
        }

        if (sec.getWallNode() == null) {
            return;
        }

        Wall wal = sec.getWallNode().get();
        int ang = NORM_ANGLE(wal.getWallAngle() + 512);

        pp.xvect += mulscale(EngineUtils.cos(ang), sec.getFloorheinum(), sectu.speed);
        pp.yvect += mulscale(EngineUtils.sin(ang), sec.getFloorheinum(), sectu.speed);
    }

    private static void PlayerAutoLook(PlayerStr pp) {
        Sector sec = boardService.getSector(pp.cursectnum);
        if (sec == null) {
            return;
        }

        if (!TEST(pp.Flags, PF_FLYING | PF_SWIMMING | PF_DIVING | PF_CLIMBING | PF_JUMPING | PF_FALLING)) {
            if (TEST(sec.getFloorstat(), FLOOR_STAT_SLOPE)) // If the floor is sloped
            {
                int x, y;
                // Get a point, 512 units ahead of player's position
                if (isOriginal()) {
                    x = pp.posx + (EngineUtils.sin((pp.getAnglei() + 512) & 2047) >> 5);
                    y = pp.posy + (EngineUtils.sin(pp.getAnglei() & 2047) >> 5);
                } else {
                    x = (int) (pp.posx + (BCosAngle(pp.getAnglef()) / 32.0f));
                    y = (int) (pp.posy + (BSinAngle(pp.getAnglef()) / 32.0f));
                }
                int tempsect = COVERupdatesector(x, y, pp.cursectnum);

                if (tempsect >= 0) // If the new point is inside a valid
                // sector...
                {
                    // Get the floorz as if the new (x,y) point was still in
                    // your sector
                    int j = engine.getflorzofslope(pp.cursectnum, pp.posx, pp.posy);
                    int k = engine.getflorzofslope(pp.cursectnum, x, y);

                    // If extended point is in same sector as you or the slopes
                    // of the sector of the extended point and your sector match
                    // closely (to avoid accidently looking straight out when
                    // you're at the edge of a sector line) then adjust horizon
                    // accordingly
                    if ((pp.cursectnum == tempsect)
                            || (klabs(engine.getflorzofslope(tempsect, x, y) - k) <= (4 << 8))) {
                        pp.horizoff += (((j - k) * 160) >> 16);
                    }
                }
            }
        }

        if (TEST(pp.Flags, PF_CLIMBING)) {
            // tilt when climbing but you can't even really tell it
            if (pp.horizoff < 100) {
                if (isOriginal()) {
                    pp.horizoff += (((100 - (int) pp.horizoff) >> 3) + 1);
                } else {
                    pp.horizoff += (((100 - pp.horizoff) / 8.0f) + 1);
                }
            }
        } else {
            // Make horizoff grow towards 0 since horizoff is not modified when
            // you're not on a slope
            if (pp.horizoff > 0) {
                if (isOriginal()) {
                    pp.horizoff -= (((int) pp.horizoff >> 3) + 1);
                } else {
                    pp.horizoff -= ((pp.horizoff / 8.0f) + 1);
                }
            }
            if (pp.horizoff < 0) {
                if (isOriginal()) {
                    pp.horizoff += (((int) (-pp.horizoff) >> 3) + 1);
                } else {
                    pp.horizoff += (((-pp.horizoff) / 8.0f) + 1);
                }
            }
        }
    }

    public static void DoPlayerHorizon(PlayerStr pp) {
        if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_NONE) {
            if (cfg.SlopeTilting || !cfg.isgMouseAim()) {
                PlayerAutoLook(pp);
            }
        }

        if (pp.input.aimvel != 0) {
            pp.horizbase += pp.input.aimvel;
            pp.Flags |= (PF_LOCK_HORIZ | PF_LOOKING);
        }

        if (TEST_SYNC_KEY(pp, SK_CENTER_VIEW)) {
            {
                pp.horiz = pp.horizbase = 100;
                pp.horizoff = 0;
            }
        }

        // this is the locked type
        if (TEST_SYNC_KEY(pp, SK_SNAP_UP) || TEST_SYNC_KEY(pp, SK_SNAP_DOWN)) {
            // set looking because player is manually looking
            pp.Flags |= (PF_LOCK_HORIZ | PF_LOOKING);

            // adjust pp.horizon negative
            if (TEST_SYNC_KEY(pp, SK_SNAP_DOWN)) {
                pp.horizbase -= ((float) HORIZ_SPEED / 2);
            }

            // adjust pp.horizon positive
            if (TEST_SYNC_KEY(pp, SK_SNAP_UP)) {
                pp.horizbase += ((float) HORIZ_SPEED / 2);
            }
        }

        // this is the unlocked type
        if (TEST_SYNC_KEY(pp, SK_LOOK_UP) || TEST_SYNC_KEY(pp, SK_LOOK_DOWN)) {
            pp.Flags &= ~(PF_LOCK_HORIZ);
            pp.Flags |= (PF_LOOKING);

            // adjust pp.horizon negative
            if (TEST_SYNC_KEY(pp, SK_LOOK_DOWN)) {
                pp.horizbase -= HORIZ_SPEED;
            }

            // adjust pp.horizon positive
            if (TEST_SYNC_KEY(pp, SK_LOOK_UP)) {
                pp.horizbase += HORIZ_SPEED;
            }
        }

        if (!TEST(pp.Flags, PF_LOCK_HORIZ)) {
            if (!(TEST_SYNC_KEY(pp, SK_LOOK_UP) || TEST_SYNC_KEY(pp, SK_LOOK_DOWN))) {
                // not pressing the pp.horiz keys
                if (pp.horizbase != 100) {

                    // move pp.horiz back to 100
                    for (int i = 1; i != 0; i--) {
                        // this formula does not work for pp.horiz = 101-103
                        if (isOriginal()) {
                            pp.horizbase += 25 - ((int) pp.horizbase >> 2);
                        } else {
                            pp.horizbase += 25 - pp.horizbase / 4.0f;
                        }
                    }
                } else {
                    // not looking anymore because pp.horiz is back at 100
                    pp.Flags &= ~(PF_LOOKING);
                }
            }
        }

        // bound the base
        pp.horizbase = Math.max(pp.horizbase, PLAYER_HORIZ_MIN);
        pp.horizbase = Math.min(pp.horizbase, PLAYER_HORIZ_MAX);

        // bound adjust horizoff
        if (pp.horizbase + pp.horizoff < PLAYER_HORIZ_MIN) {
            pp.horizoff = PLAYER_HORIZ_MIN - pp.horizbase;
        } else if (pp.horizbase + pp.horizoff > PLAYER_HORIZ_MAX) {
            pp.horizoff = PLAYER_HORIZ_MAX - pp.horizbase;
        }

        // add base and offsets
        pp.horiz = pp.horizbase + pp.horizoff;
    }

    public static void DoPlayerBob(PlayerStr pp) {
        int dist = Distance(pp.posx, pp.posy, pp.oldposx, pp.oldposy);
        if (dist > 512) {
            dist = 0;
        }

        // if running make a longer stride
        int amt;
        if (TEST_SYNC_KEY(pp, SK_RUN) || TEST(pp.Flags, PF_LOCK_RUN)) {
            amt = mulscale(12, (long) dist << 8, 16);
            dist = mulscale(dist, 26000, 16);
        } else {
            amt = mulscale(5, (long) dist << 9, 16);
            dist = mulscale(dist, 32000, 16);
        }

        // controls how fast you move through the sin table
        pp.bcnt += dist;

        // wrap bcnt
        pp.bcnt &= 2047;

        // move pp.horiz up and down from 100 using sintable
        pp.bob_z = mulscale(Z(amt), EngineUtils.sin(pp.bcnt), 14);
    }

    public static void DoPlayerBeginRecoil(PlayerStr pp, int pix_amt) {
        pp.Flags |= (PF_RECOIL);

        pp.recoil_amt =  pix_amt;
        pp.recoil_speed = 80;
        pp.recoil_ndx = 0;
        pp.recoil_horizoff = 0;
    }

    public static void DoPlayerRecoil(PlayerStr pp) {
        // controls how fast you move through the sin table
        pp.recoil_ndx += pp.recoil_speed;

        if (EngineUtils.sin(pp.recoil_ndx) < 0) {
            pp.Flags &= ~(PF_RECOIL);
            pp.recoil_horizoff = 0;
            return;
        }

        // move pp.horiz up and down
        pp.recoil_horizoff =  ((pp.recoil_amt * EngineUtils.sin(pp.recoil_ndx)) >> 14);
    }

    // for wading
    public static void DoPlayerSpriteBob(PlayerStr pp, int player_height, int bob_amt, int bob_speed) {
        Sprite sp = pp.getSprite();
        if (sp == null) {
            return;
        }

        pp.bob_ndx = ((pp.bob_ndx + (synctics << bob_speed)) & 2047);
        pp.bob_amt = ((bob_amt * EngineUtils.sin(pp.bob_ndx)) >> 14);
        sp.setZ((pp.posz + player_height) + pp.bob_amt);
    }

    public static void UpdatePlayerUnderSprite(PlayerStr pp) {
        Sprite over_sp = pp.getSprite();
        USER over_u = getUser(pp.PlayerSprite);
        if (over_sp == null || over_u == null) {
            return;
        }

        Sector over_sec = boardService.getSector(over_sp.getSectnum());
        if (over_sec == null) {
            return;
        }

        if (Prediction) {
            return;
        }

        // dont bother spawning if you ain't really in the water
        int water_level_z = over_sec.getFloorz();

        // if not below water
        boolean above_water = (SPRITEp_BOS(over_sp) <= water_level_z);
        boolean in_dive_area = SpriteInDiveArea(over_sp);

        // if not in dive area OR (in dive area AND above the water) - Kill it
        if (!in_dive_area || above_water) {
            // if under sprite exists and not in a dive area - Kill it
            if (pp.PlayerUnderSprite >= 0) {
                KillSprite(pp.PlayerUnderSprite);
                pp.PlayerUnderSprite = -1;
            }
            return;
        } else {
            // if in a dive area and a under sprite does not exist - create it
            if (pp.PlayerUnderSprite < 0) {
                SpawnPlayerUnderSprite(pp.pnum);
            }
        }

        int SpriteNum = pp.PlayerUnderSprite;
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setX(over_sp.getX());
        sp.setY(over_sp.getY());
        sp.setZ(over_sp.getZ());
        engine.changespritesect(SpriteNum, over_sp.getSectnum());

        SpriteWarpToUnderwater(SpriteNum);

        // find z water level of the top sector
        // diff between the bottom of the upper sprite and the water level
        int zdiff = SPRITEp_BOS(over_sp) - water_level_z;

        // add diff to ceiling
        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec != null) {
            sp.setZ(sec.getCeilingz() + zdiff);
        }

        u.State = over_u.State;
        u.setRot(over_u.getRot());
        u.StateStart = over_u.StateStart;
        sp.setPicnum(over_sp.getPicnum());
    }

    public static void UpdatePlayerSprite(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        Sector sec = boardService.getSector(pp.cursectnum);
        if (sec == null || sp == null) {
            return;
        }

        // Update sprite representation of player
        game.pInt.setsprinterpolate(pp.PlayerSprite, sp);

        sp.setX(pp.posx);
        sp.setY(pp.posy);

        // there are multiple death functions
        if (TEST(pp.Flags, PF_DEAD)) {
            engine.changespritesect(pp.PlayerSprite, pp.cursectnum);
            sp.setAng(pp.getAnglei());
            UpdatePlayerUnderSprite(pp);
            return;
        }

        if (pp.sop_control != -1) {
            sp.setZ(sec.getFloorz());
            engine.changespritesect(pp.PlayerSprite, pp.cursectnum);
        } else if (pp.DoPlayerAction == DoPlayerCrawl) {
            sp.setZ(pp.posz + PLAYER_CRAWL_HEIGHT);
            engine.changespritesect(pp.PlayerSprite, pp.cursectnum);
        } else if (pp.DoPlayerAction == DoPlayerWade) {
            sp.setZ(pp.posz + PLAYER_HEIGHT);
            engine.changespritesect(pp.PlayerSprite, pp.cursectnum);

            if (pp.WadeDepth > Z(29)) {
                DoPlayerSpriteBob(pp, PLAYER_HEIGHT, Z(3), 3);
            }
        } else if (pp.DoPlayerAction == DoPlayerDive) {
            // bobbing and sprite position taken care of in DoPlayerDive
            sp.setZ(pp.posz + Z(10));
            engine.changespritesect(pp.PlayerSprite, pp.cursectnum);
        } else if (pp.DoPlayerAction == DoPlayerClimb) {
            sp.setZ(pp.posz + Z(17));

            engine.changespritesect(pp.PlayerSprite, pp.cursectnum);
        } else if (pp.DoPlayerAction == DoPlayerFly) {
            DoPlayerSpriteBob(pp, PLAYER_HEIGHT, Z(6), 3);
            engine.changespritesect(pp.PlayerSprite, pp.cursectnum);
        } else if (pp.DoPlayerAction == DoPlayerJump
                || pp.DoPlayerAction == DoPlayerFall
                || pp.DoPlayerAction == DoPlayerForceJump) {
            sp.setZ(pp.posz + PLAYER_HEIGHT);
            engine.changespritesect(pp.PlayerSprite, pp.cursectnum);
        } else if (pp.DoPlayerAction == DoPlayerTeleportPause) {
            sp.setZ(pp.posz + PLAYER_HEIGHT);
            engine.changespritesect(pp.PlayerSprite, pp.cursectnum);
        } else {
            sp.setZ(pp.loz);
            engine.changespritesect(pp.PlayerSprite, pp.cursectnum);
        }

        UpdatePlayerUnderSprite(pp);

        sp.setAng((int) pp.pang);
    }

    public static void DoPlayerZrange(PlayerStr pp) {
        // Don't let you fall if you're just slightly over a cliff
        // This function returns the highest and lowest z's
        // for an entire box, NOT just a point. -Useful for clipping
        Sprite psp = pp.getSprite();
        if (psp == null) {
            return;
        }

        int bakcstat = psp.getCstat();
        psp.setCstat(psp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
        FAFgetzrange(pp.posx, pp.posy, pp.posz + Z(8), pp.cursectnum, tmp_ptr[0].set(0), tmp_ptr[1].set(0), tmp_ptr[2].set(0), tmp_ptr[3].set(0),
                (psp.getClipdist() << 2) - GETZRANGE_CLIP_ADJ, CLIPMASK_PLAYER);
        psp.setCstat(bakcstat);

        pp.hiz = tmp_ptr[0].value;
        int ceilhit = tmp_ptr[1].value;
        pp.loz = tmp_ptr[2].value;
        int florhit = tmp_ptr[3].value;

        // 16384+sector (sector first touched) or
        // 49152+spritenum (sprite first touched)

        pp.lo_sectp = pp.hi_sectp = -1;
        pp.lo_sp = pp.hi_sp = -1;

        if (DTEST(ceilhit, HIT_TYPE_MASK) == HIT_SPRITE) {
            pp.hi_sp = NORM_HIT_INDEX(ceilhit);
        } else {
            pp.hi_sectp = NORM_HIT_INDEX(ceilhit);
        }

        if (DTEST(florhit, HIT_TYPE_MASK) == HIT_SPRITE) {
            pp.lo_sp = NORM_HIT_INDEX(florhit);
            Sprite loSp = boardService.getSprite(pp.lo_sp);
            USER lou = getUser(pp.lo_sp);

            // prevent player from standing on Zombies
            if (loSp != null && lou != null && loSp.getStatnum() == STAT_ENEMY && lou.ID == ZOMBIE_RUN_R0) {
                pp.lo_sectp = loSp.getSectnum();
                pp.loz = loSp.getZ();
                pp.lo_sp = -1;
            }
        } else {
            pp.lo_sectp = NORM_HIT_INDEX(florhit);
        }
    }

    private static void DoPlayerSlide(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sprite psp = pp.getSprite();
        if (psp == null || (pp.slide_xvect | pp.slide_yvect) == 0) {
            return;
        }

        if (pp.sop != -1) {
            return;
        }

        pp.slide_xvect = mulscale(pp.slide_xvect, PLAYER_SLIDE_FRICTION, 16);
        pp.slide_yvect = mulscale(pp.slide_yvect, PLAYER_SLIDE_FRICTION, 16);

        if (klabs(pp.slide_xvect) < 12800 && klabs(pp.slide_yvect) < 12800) {
            pp.slide_xvect = pp.slide_yvect = 0;
        }

        int push_ret = engine.pushmove(pp.posx, pp.posy, pp.posz, pp.cursectnum, (psp.getClipdist() << 2),
                pp.ceiling_dist, pp.floor_dist, CLIPMASK_PLAYER);

        if (pushmove_sectnum != -1) {
            pp.posx = pushmove_x;
            pp.posy = pushmove_y;
            pp.posz = pushmove_z;
            pp.cursectnum = pushmove_sectnum;
        }

        if (push_ret < 0) {
            if (u != null && !TEST(pp.Flags, PF_DEAD)) {
                PlayerUpdateHealth(pp, -u.Health); // Make sure he dies!
                PlayerCheckDeath(pp, -1);

                if (TEST(pp.Flags, PF_DEAD)) {
                    return;
                }
            }
            return;
        }

        engine.clipmove(pp.posx, pp.posy, pp.posz, pp.cursectnum, pp.slide_xvect, pp.slide_yvect,
                (psp.getClipdist() << 2), pp.ceiling_dist, pp.floor_dist, CLIPMASK_PLAYER);

        if (clipmove_sectnum != -1) {
            pp.posx = clipmove_x;
            pp.posy = clipmove_y;
            pp.posz = clipmove_z;
            pp.cursectnum = clipmove_sectnum;
        }

        PlayerCheckValidMove(pp);
        push_ret = engine.pushmove(pp.posx, pp.posy, pp.posz, pp.cursectnum, (psp.getClipdist() << 2),
                pp.ceiling_dist, pp.floor_dist, CLIPMASK_PLAYER);

        if (pushmove_sectnum != -1) {
            pp.posx = pushmove_x;
            pp.posy = pushmove_y;
            pp.posz = pushmove_z;
            pp.cursectnum = pushmove_sectnum;
        }

        if (push_ret < 0) {
            if (u != null && !TEST(pp.Flags, PF_DEAD)) {
                PlayerUpdateHealth(pp, -u.Health); // Make sure he dies!
                PlayerCheckDeath(pp, -1);
            }
        }
    }

    public static void PlayerCheckValidMove(PlayerStr pp) {
        if (pp.cursectnum == -1) {
            pp.posx = pp.oldposx;
            pp.posy = pp.oldposy;
            pp.posz = pp.oldposz;
            pp.cursectnum = pp.lastcursectnum;

            // if stuck here for more than 10 seconds
            if (count++ > 40 * 10) {
                count = 0;
                throw new WarningException("Player stuck");
            }
        }
    }

    public static void DoPlayerMenuKeys(PlayerStr pp) {
        if (game.nNetMode == NetMode.Single) {
            if (TEST_SYNC_KEY((pp), SK_AUTO_AIM)) {
                if (FLAG_KEY_PRESSED(pp, SK_AUTO_AIM)) {
                    FLAG_KEY_RELEASE(pp, SK_AUTO_AIM);
                    pp.Flags ^= (PF_AUTO_AIM);
                }
            } else {
                FLAG_KEY_RESET(pp, SK_AUTO_AIM);
            }
        }
    }

    public static void PlayerSectorBound(PlayerStr pp, int amt) {
        // player should never go into a sector

        // was getting some problems with this
        // when jumping onto hight sloped sectors

        // call this routine to make sure he doesn't
        // called from DoPlayerMove() but can be called
        // from anywhere it is needed

        engine.getzsofslope(pp.cursectnum, pp.posx, pp.posy, fz, cz);

        if (pp.posz > fz.get() - amt) {
            pp.posz = fz.get() - amt;
        }

        if (pp.posz < cz.get() + amt) {
            pp.posz = cz.get() + amt;
        }
    }

    public static void PLAYER_RUN_LOCK(PlayerStr pp) {
        if (TEST_SYNC_KEY((pp), SK_RUN_LOCK)) {
            if (FLAG_KEY_PRESSED((pp), SK_RUN_LOCK)) {
                FLAG_KEY_RELEASE((pp), SK_RUN_LOCK);
                (pp).Flags ^= (PF_LOCK_RUN);
                cfg.AutoRun = TEST((pp).Flags, PF_LOCK_RUN);
                PutStringInfo(pp, "Run mode " + (TEST((pp).Flags, PF_LOCK_RUN) ? "ON" : "OFF"));
            }
        } else {
            FLAG_KEY_RESET((pp), SK_RUN_LOCK);
        }
    }

    public static void DoPlayerMove(PlayerStr pp) {
        Sprite psp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (psp == null || u == null) {
            return;
        }

        SlipSlope(pp);
        PLAYER_RUN_LOCK(pp);
        DoPlayerTurn(pp);

        pp.oldposx = pp.posx;
        pp.oldposy = pp.posy;
        pp.oldposz = pp.posz;
        pp.lastcursectnum = pp.cursectnum;

        if (PLAYER_MOVING(pp) == 0) {
            pp.Flags &= ~(PF_PLAYER_MOVED);
        } else {
            pp.Flags |= (PF_PLAYER_MOVED);
        }

        DoPlayerSlide(pp);

        pp.oxvect = pp.xvect;
        pp.oyvect = pp.yvect;

        pp.xvect += ((pp.input.vel * synctics * 2) << 6);
        pp.yvect += ((pp.input.svel * synctics * 2) << 6);

        int friction = pp.friction;
        if (!TEST(pp.Flags, PF_SWIMMING) && pp.WadeDepth != 0) {
            friction -= pp.WadeDepth * 100;
        }

        pp.xvect = mulscale(pp.xvect, friction, 16);
        pp.yvect = mulscale(pp.yvect, friction, 16);

        if (TEST(pp.Flags, PF_FLYING)) {
            // do a bit of weighted averaging
            pp.xvect = (pp.xvect + (pp.oxvect)) / 2;
            pp.yvect = (pp.yvect + (pp.oyvect)) / 2;
        } else if (TEST(pp.Flags, PF_DIVING)) {
            // do a bit of weighted averaging
            pp.xvect = (pp.xvect + (pp.oxvect * 2)) / 3;
            pp.yvect = (pp.yvect + (pp.oyvect * 2)) / 3;
        }

        if (klabs(pp.xvect) < 12800 && klabs(pp.yvect) < 12800) {
            pp.xvect = pp.yvect = 0;
        }

        psp.setXvel( (FindDistance2D(pp.xvect, pp.yvect) >> 14));

        if (TEST(pp.Flags, PF_CLIP_CHEAT)) {
            int sectnum = pp.cursectnum;
            pp.posx += pp.xvect >> 14;
            pp.posy += pp.yvect >> 14;
            sectnum = COVERupdatesector(pp.posx, pp.posy, sectnum);
            if (sectnum != -1) {
                pp.cursectnum = sectnum;
            }
        } else {
            int push_ret = engine.pushmove(pp.posx, pp.posy, pp.posz, pp.cursectnum, (psp.getClipdist() << 2),
                    pp.ceiling_dist, pp.floor_dist - Z(16), CLIPMASK_PLAYER);

            if (pushmove_sectnum != -1) {
                pp.posx = pushmove_x;
                pp.posy = pushmove_y;
                pp.posz = pushmove_z;
                pp.cursectnum = pushmove_sectnum;
            }

            if (push_ret < 0) {
                if (!TEST(pp.Flags, PF_DEAD)) {
                    PlayerUpdateHealth(pp, -u.Health); // Make sure he dies!
                    PlayerCheckDeath(pp, -1);

                    if (TEST(pp.Flags, PF_DEAD)) {
                        return;
                    }
                }
            }

            int save_cstat = psp.getCstat();
            psp.setCstat(psp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
            pp.cursectnum = COVERupdatesector(pp.posx, pp.posy, pp.cursectnum);
            engine.clipmove(pp.posx, pp.posy, pp.posz, pp.cursectnum, pp.xvect, pp.yvect, (psp.getClipdist() << 2),
                    pp.ceiling_dist, pp.floor_dist, CLIPMASK_PLAYER);

            if (clipmove_sectnum != -1) {
                pp.posx = clipmove_x;
                pp.posy = clipmove_y;
                pp.posz = clipmove_z;
                pp.cursectnum = clipmove_sectnum;
            }

            psp.setCstat( save_cstat);
            PlayerCheckValidMove(pp);

            push_ret = engine.pushmove(pp.posx, pp.posy, pp.posz, pp.cursectnum, (psp.getClipdist() << 2),
                    pp.ceiling_dist, pp.floor_dist - Z(16), CLIPMASK_PLAYER);

            if (pushmove_sectnum != -1) {
                pp.posx = pushmove_x;
                pp.posy = pushmove_y;
                pp.posz = pushmove_z;
                pp.cursectnum = pushmove_sectnum;
            }

            if (push_ret < 0) {
                if (!TEST(pp.Flags, PF_DEAD)) {
                    PlayerUpdateHealth(pp, -u.Health); // Make sure he dies!
                    PlayerCheckDeath(pp, -1);

                    if (TEST(pp.Flags, PF_DEAD)) {
                        return;
                    }
                }
            }
        }

        // check for warp - probably can remove from CeilingHit
        if (WarpPlane(pp.posx, pp.posy, pp.posz, pp.cursectnum) != null) {
            pp.posx = warp.x;
            pp.posy = warp.y;
            pp.posz = warp.z;
            pp.cursectnum = warp.sectnum;
            PlayerWarpUpdatePos(pp);
        }

        DoPlayerZrange(pp);

        DoPlayerSetWadeDepth(pp);

        DoPlayerHorizon(pp);
        Sector sec = boardService.getSector(pp.cursectnum);
        if (sec != null && TEST(sec.getExtra(), SECTFX_DYNAMIC_AREA)) {
            if (TEST(pp.Flags, PF_FLYING | PF_JUMPING | PF_FALLING)) {
                if (pp.posz > pp.loz) {
                    pp.posz = pp.loz - PLAYER_HEIGHT;
                }

                if (pp.posz < pp.hiz) {
                    pp.posz = pp.hiz + PLAYER_HEIGHT;
                }
            } else if (TEST(pp.Flags, PF_SWIMMING | PF_DIVING)) {
                if (pp.posz > pp.loz) {
                    pp.posz = pp.loz - PLAYER_SWIM_HEIGHT;
                }

                if (pp.posz < pp.hiz) {
                    pp.posz = pp.hiz + PLAYER_SWIM_HEIGHT;
                }
            }
        }
    }

    public static void DoPlayerSectorUpdatePreMove(PlayerStr pp) {
        int sectnum = pp.cursectnum;
        Sector sec = boardService.getSector(pp.cursectnum);
        if (sec == null) {
            return;
        }

        if (TEST(sec.getExtra(), SECTFX_DYNAMIC_AREA)) {
            sectnum = engine.updatesectorz(pp.posx, pp.posy, pp.posz, sectnum);
            if (sectnum < 0) {
                sectnum = pp.cursectnum;
                sectnum = COVERupdatesector(pp.posx, pp.posy, sectnum);
            }
        } else if (FAF_ConnectArea(sectnum)) {
            sectnum = engine.updatesectorz(pp.posx, pp.posy, pp.posz, sectnum);
            if (sectnum < 0) {
                sectnum = pp.cursectnum;
                sectnum = COVERupdatesector(pp.posx, pp.posy, sectnum);
            }
        }

        pp.cursectnum =  sectnum;
    }

    public static void DoPlayerSectorUpdatePostMove(PlayerStr pp) {
        // need to do updatesectorz if in connect area
        if (FAF_ConnectArea(pp.cursectnum)) {
            int sectnum = pp.cursectnum;
            pp.cursectnum =  engine.updatesectorz(pp.posx, pp.posy, pp.posz, sectnum);

            // can mess up if below
            if (pp.cursectnum < 0) {
                pp.cursectnum = sectnum;

                // adjust the posz to be in a sector
                engine.getzsofslope(pp.cursectnum, pp.posx, pp.posy, fz, cz);
                if (pp.posz > fz.get()) {
                    pp.posz = fz.get();
                }

                if (pp.posz < cz.get()) {
                    pp.posz = cz.get();
                }

                // try again
                pp.cursectnum =  engine.updatesectorz(pp.posx, pp.posy, pp.posz, pp.cursectnum);
            }
        } else {
            PlayerSectorBound(pp, Z(1));
        }
    }

    public static void PlaySOsound(int sectnum, int sound_num) {
        ListNode<Sprite> nexti;
        // play idle sound - sound 1
        for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();
            if (sp.getStatnum() == STAT_SOUND_SPOT) {
                DoSoundSpotStopSound(sp.getLotag());
                DoSoundSpotMatch(sp.getLotag(), sound_num, SoundType.SOUND_OBJECT_TYPE);
            }
        }
    }

    public static void StopSOsound(int sectnum) {
        // play idle sound - sound 1
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = nexti) {
            Sprite sp = node.get();
            nexti = node.getNext();
            if (sp.getStatnum() == STAT_SOUND_SPOT) {
                DoSoundSpotStopSound(sp.getLotag());
            }
        }
    }

    public static void DoPlayerMoveBoat(PlayerStr pp) {
        Sector_Object sop = SectorObject[pp.sop];

        if (Prediction) {
            return;
        }

        // this code looks at the fifo to get the last value for comparison
        int fifo_ndx = (game.pNet.gNetFifoTail - 2) & (MOVEFIFOSIZ - 1);
        Input last_input = (Input) game.pNet.gFifoInput[fifo_ndx][pp.pnum];

        if (klabs(pp.input.vel | pp.input.svel) != 0 && klabs(last_input.vel | last_input.svel) == 0) {
            PlaySOsound(sop.mid_sector, SO_DRIVE_SOUND);
        } else if (klabs(pp.input.vel | pp.input.svel) == 0 && klabs(last_input.vel | last_input.svel) != 0) {
            PlaySOsound(sop.mid_sector, SO_IDLE_SOUND);
        }

        PLAYER_RUN_LOCK(pp);
        DoPlayerTurnBoat(pp);

        if (PLAYER_MOVING(pp) == 0) {
            pp.Flags &= ~(PF_PLAYER_MOVED);
        } else {
            pp.Flags |= (PF_PLAYER_MOVED);
        }

        pp.oxvect = pp.xvect;
        pp.oyvect = pp.yvect;

        if (sop.drive_speed != 0) {
            pp.xvect = mulscale(pp.input.vel, sop.drive_speed, 6);
            pp.yvect = mulscale(pp.input.svel, sop.drive_speed, 6);

            // does sliding/momentum
            pp.xvect = (pp.xvect + (pp.oxvect * (sop.drive_slide - 1))) / sop.drive_slide;
            pp.yvect = (pp.yvect + (pp.oyvect * (sop.drive_slide - 1))) / sop.drive_slide;
        } else {
            pp.xvect += ((pp.input.vel * synctics * 2) << 6);
            pp.yvect += ((pp.input.svel * synctics * 2) << 6);

            pp.xvect = mulscale(pp.xvect, BOAT_FRICTION, 16);
            pp.yvect = mulscale(pp.yvect, BOAT_FRICTION, 16);

            // does sliding/momentum
            pp.xvect = (pp.xvect + (pp.oxvect * 5)) / 6;
            pp.yvect = (pp.yvect + (pp.oyvect * 5)) / 6;
        }

        if (klabs(pp.xvect) < 12800 && klabs(pp.yvect) < 12800) {
            pp.xvect = pp.yvect = 0;
        }

        pp.lastcursectnum = pp.cursectnum;
        int z = pp.posz + Z(10);

        int save_sectnum = pp.cursectnum;
        OperateSectorObject(pp.sop, pp.getAnglef(), MAXSO, MAXSO, 1);
        pp.cursectnum = sop.op_main_sector; // for speed

        int floor_dist = klabs(z - sop.floor_loz);
        engine.clipmove(pp.posx, pp.posy, z, pp.cursectnum, pp.xvect, pp.yvect, sop.clipdist, Z(4), floor_dist,
                CLIPMASK_PLAYER);
        if (clipmove_sectnum != -1) {
            pp.posx = clipmove_x;
            pp.posy = clipmove_y;
            pp.cursectnum = clipmove_sectnum;
        }

        OperateSectorObject(pp.sop, pp.getAnglef(), pp.posx, pp.posy, 1);
        pp.cursectnum = save_sectnum; // for speed

        DoPlayerHorizon(pp);
    }

    public static void DoTankTreads(PlayerStr pp) {
        Sprite sp;
        int vel;
        int j;
        int dot;
        boolean reverse = false;

        if (Prediction) {
            return;
        }

        vel = FindDistance2D(pp.xvect >> 8, pp.yvect >> 8);
        if (isOriginal()) {
            dot = DOT_PRODUCT_2D(pp.xvect, pp.yvect, EngineUtils.sin(NORM_ANGLE(pp.getAnglei() + 512)), EngineUtils.sin(pp.getAnglei()));
        } else {
            dot = DOT_PRODUCT_2D(pp.xvect, pp.yvect, (int) BCosAngle(pp.getAnglef()), (int) BSinAngle(pp.getAnglef()));
        }
        if (dot < 0) {
            reverse = true;
        }

        Sector_Object sop = SectorObject[pp.sop];

        ListNode<Sprite> nexti;
        for (j = 0; sop.sector[j] != -1; j++) {
            for (ListNode<Sprite> node = boardService.getSectNode(sop.sector[j]); node != null; node = nexti) {
                nexti = node.getNext();
                sp = node.get();

                // BOOL1 is set only if pans with SO
                if (!TEST_BOOL1(sp)) {
                    continue;
                }

                if (sp.getStatnum() == STAT_WALL_PAN) {
                    if (reverse) {
                        if (!TEST_BOOL2(sp)) {
                            SET_BOOL2(sp);
                            sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
                        }
                    } else {
                        if (TEST_BOOL2(sp)) {
                            RESET_BOOL2(sp);
                            sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
                        }
                    }

                    sp.setXvel( vel);
                } else if (sp.getStatnum() == STAT_FLOOR_PAN) {
                    if (reverse) {
                        if (!TEST_BOOL2(sp)) {
                            SET_BOOL2(sp);
                            sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
                        }
                    } else {
                        if (TEST_BOOL2(sp)) {
                            RESET_BOOL2(sp);
                            sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
                        }
                    }

                    sp.setXvel( vel);
                } else if (sp.getStatnum() == STAT_CEILING_PAN) {
                    if (reverse) {
                        if (!TEST_BOOL2(sp)) {
                            SET_BOOL2(sp);
                            sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
                        }
                    } else {
                        if (TEST_BOOL2(sp)) {
                            RESET_BOOL2(sp);
                            sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
                        }
                    }

                    sp.setXvel( vel);
                }
            }
        }
    }

    public static void DriveCrush(PlayerStr pp, int[] x, int[] y) {
        if (MoveSkip4 == 0) {
            return;
        }

        // not moving - don't crush
        if ((pp.xvect | pp.yvect) == 0 && pp.input.angvel == 0) {
            return;
        }

        Sector_Object sop = SectorObject[pp.sop_control];

        // main sector
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getSectNode(sop.op_main_sector); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();
            USER u = getUser(i);

            if (testpointinquad(sp.getX(), sp.getY(), x, y) != 0) {
                if (TEST(sp.getExtra(), SPRX_BREAKABLE) && HitBreakSprite(i, 0)) {
                    continue;
                }

                if (sp.getStatnum() == STAT_MISSILE) {
                    continue;
                }

                if (sp.getPicnum() == ST1) {
                    continue;
                }

                if (TEST(sp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    if (u != null && !TEST(u.Flags, SPR_DEAD) && !TEST(sp.getExtra(), SPRX_BREAKABLE)) {
                        continue;
                    }
                }

                if (TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
                    continue;
                }

                if (sp.getStatnum() > STAT_DONT_DRAW) {
                    continue;
                }

                if (sp.getZ() < sop.crush_z) {
                    continue;
                }

                SpriteQueueDelete(i);
                KillSprite(i);
            }
        }

        // all enemys
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ENEMY); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();

            if (testpointinquad(sp.getX(), sp.getY(), x, y) != 0) {
                // if (sp.z < pp.posz)
                if (sp.getZ() < sop.crush_z) {
                    continue;
                }

                int vel = FindDistance2D(pp.xvect >> 8, pp.yvect >> 8);
                if (vel < 9000) {
                    DoActorBeginSlide(i, EngineUtils.getAngle(pp.xvect, pp.yvect), vel / 8, 5);
                    if (DoActorSlide(i)) {
                        continue;
                    }
                }

                UpdateSinglePlayKills(i, pp);

                if (SpawnShrap(i, -99)) {
                    SetSuicide(i);
                } else {
                    KillSprite(i);
                }
            }
        }

        // all dead actors
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_DEAD_ACTOR); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();

            if (testpointinquad(sp.getX(), sp.getY(), x, y) != 0) {
                if (sp.getZ() < sop.crush_z) {
                    continue;
                }

                SpriteQueueDelete(i);
                KillSprite(i);
            }
        }

        // all players
        for (int stat = 0; stat < MAX_SW_PLAYERS; stat++) {
            ListNode<Sprite> node = boardService.getStatNode(STAT_PLAYER0 + stat);
            if (node == null) {
                continue;
            }
            int i = node.getIndex();
            Sprite sp = node.get();
            USER u = getUser(i);

            if (u == null || u.PlayerP == pp.pnum) {
                continue;
            }

            if (testpointinquad(sp.getX(), sp.getY(), x, y) != 0) {
                int damage;

                // if (sp.z < pp.posz)
                if (sp.getZ() < sop.crush_z) {
                    continue;
                }

                damage = -(u.Health + 100);
                PlayerDamageSlide(Player[u.PlayerP], damage, pp.getAnglei());
                PlayerUpdateHealth(Player[u.PlayerP], damage);
                PlayerCheckDeath(Player[u.PlayerP], pp.PlayerSprite);
            }
        }

        // if it ends up actually in the drivable sector kill it
        for (int s = 0; sop.sector[s] != -1; s++) {
            for (ListNode<Sprite> node = boardService.getSectNode(sop.sector[s]); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite sp = node.get();

                // give some extra buffer
                if (sp.getZ() < sop.crush_z + Z(40)) {
                    continue;
                }

                if (TEST(sp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    if (sp.getStatnum() == STAT_ENEMY) {
                        if (SpawnShrap(i, -99)) {
                            SetSuicide(i);
                        } else {
                            KillSprite(i);
                        }
                    }
                }
            }
        }
    }

    public static void DoPlayerMoveTank(PlayerStr pp) {
        Sprite psp = pp.getSprite();
        Sector_Object sop = SectorObject[pp.sop];
        final int spnum = sop.sp_child;
        final boolean RectClip = TEST(sop.flags, SOBJ_RECT_CLIP);
        USER u = getUser(spnum);

        if (psp == null || Prediction) {
            return;
        }

        // this code looks at the fifo to get the last value for comparison
        int fifo_ndx = (game.pNet.gNetFifoTail - 2) & (MOVEFIFOSIZ - 1);
        Input last_input = (Input) game.pNet.gFifoInput[fifo_ndx][pp.pnum];

        if (klabs(pp.input.vel | pp.input.svel) != 0 && klabs(last_input.vel | last_input.svel) == 0) {
            PlaySOsound(sop.mid_sector, SO_DRIVE_SOUND);
        } else if (klabs(pp.input.vel | pp.input.svel) == 0 && klabs(last_input.vel | last_input.svel) != 0) {
            PlaySOsound(sop.mid_sector, SO_IDLE_SOUND);
        }

        PLAYER_RUN_LOCK(pp);
        if (PLAYER_MOVING(pp) == 0) {
            pp.Flags &= ~(PF_PLAYER_MOVED);
        } else {
            pp.Flags |= (PF_PLAYER_MOVED);
        }

        pp.oxvect = pp.xvect;
        pp.oyvect = pp.yvect;

        if (sop.drive_speed != 0) {
            pp.xvect = mulscale(pp.input.vel, sop.drive_speed, 6);
            pp.yvect = mulscale(pp.input.svel, sop.drive_speed, 6);

            // does sliding/momentum
            pp.xvect = (pp.xvect + (pp.oxvect * (sop.drive_slide - 1))) / sop.drive_slide;
            pp.yvect = (pp.yvect + (pp.oyvect * (sop.drive_slide - 1))) / sop.drive_slide;
        } else {
            pp.xvect += ((pp.input.vel * synctics * 2) << 6);
            pp.yvect += ((pp.input.svel * synctics * 2) << 6);

            pp.xvect = mulscale(pp.xvect, TANK_FRICTION, 16);
            pp.yvect = mulscale(pp.yvect, TANK_FRICTION, 16);

            pp.xvect = (pp.xvect + (pp.oxvect)) / 2;
            pp.yvect = (pp.yvect + (pp.oyvect)) / 2;
        }

        if (klabs(pp.xvect) < 12800 && klabs(pp.yvect) < 12800) {
            pp.xvect = pp.yvect = 0;
        }

        pp.lastcursectnum = pp.cursectnum;
        int z = pp.posz + Z(10);

        if (RectClip) {
            int wallcount = 0;
            int count = 0;
            for (int s = 0; sop.sector[s] != -1; s++) {
                Sector sectp = boardService.getSector(sop.sector[s]);
                if (sectp != null) {
                    for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wp = wn.get();
                        if (wp.getExtra() != 0
                                && DTEST(wp.getExtra(), WALLFX_LOOP_OUTER | WALLFX_LOOP_OUTER_SECONDARY) == WALLFX_LOOP_OUTER) {
                            x[count] = wp.getX();
                            y[count] = wp.getY();

                            ox[count] = sop.xmid - sop.xorig[wallcount];
                            oy[count] = sop.ymid - sop.yorig[wallcount];

                            count++;
                        }

                        wallcount++;
                    }
                }
            }

        }

        final int save_sectnum = pp.cursectnum;
        OperateSectorObject(pp.sop, pp.getAnglef(), MAXSO, MAXSO, 1);
        pp.cursectnum = sop.op_main_sector; // for speed

        int floor_dist = klabs(z - sop.floor_loz);
        if (RectClip) {
            final int save_cstat = psp.getCstat();
            psp.setCstat(psp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
            DoPlayerTurnTankRect(pp, x, y, ox, oy);

            boolean ret = RectClipMove(pp, x, y);
            DriveCrush(pp, x, y);
            psp.setCstat(save_cstat);

            if (!ret) {
                int vel = FindDistance2D(pp.xvect >> 8, pp.yvect >> 8);
                if (vel > 13000) {
                    int nx = DIV2(x[0] + x[1]);
                    int ny = DIV2(y[0] + y[1]);
                    Sector opSector = boardService.getSector(pp.cursectnum);
                    if (u != null && opSector  != null) {
                        engine.hitscan(nx, ny, opSector.getFloorz() - Z(10), pp.cursectnum, MOVEx(256, pp.getAnglei()),
                                MOVEy(256, pp.getAnglei()), 0, pHitInfo, CLIPMASK_PLAYER);

                        if (FindDistance2D(pHitInfo.hitx - nx, pHitInfo.hity - ny) < 800) {
                            if (pHitInfo.hitwall != -1) {
                                u.moveSpriteReturn = pHitInfo.hitwall | HIT_WALL;
                            } else if (pHitInfo.hitsprite != -1) {
                                u.moveSpriteReturn = pHitInfo.hitsprite | HIT_SPRITE;
                            } else {
                                u.moveSpriteReturn = 0;
                            }

                            VehicleMoveHit(spnum);
                        }

                        if (!TEST(sop.flags, SOBJ_NO_QUAKE)) {
                            SetPlayerQuake(pp);
                        }
                    }
                }

                if (vel > 12000) {
                    pp.xvect = pp.yvect = pp.oxvect = pp.oyvect = 0;
                }
            }
        } else if (u != null) {
            DoPlayerTurnTank(pp, z, floor_dist);
            int save_cstat = psp.getCstat();
            psp.setCstat(psp.getCstat() & ~(CSTAT_SPRITE_BLOCK));
            if (sop.clipdist != 0) {
                u.moveSpriteReturn = engine.clipmove(pp.posx, pp.posy, z, pp.cursectnum, pp.xvect, pp.yvect, sop.clipdist, Z(4),
                        floor_dist, CLIPMASK_PLAYER);
                if (clipmove_sectnum != -1) {
                    pp.posx = clipmove_x;
                    pp.posy = clipmove_y;
                    pp.cursectnum = clipmove_sectnum;
                }
            } else {
                u.moveSpriteReturn = MultiClipMove(pp, z, floor_dist);
            }
            psp.setCstat( save_cstat);

            if (u.moveSpriteReturn != 0) {
                int vel;

                vel = FindDistance2D(pp.xvect >> 8, pp.yvect >> 8);

                if (vel > 13000) {
                    VehicleMoveHit(spnum);
                    pp.slide_xvect = -pp.xvect << 1;
                    pp.slide_yvect = -pp.yvect << 1;
                    if (!TEST(sop.flags, SOBJ_NO_QUAKE)) {
                        SetPlayerQuake(pp);
                    }
                }

                if (vel > 12000) {
                    pp.xvect = pp.yvect = pp.oxvect = pp.oyvect = 0;
                }
            }
        }

        OperateSectorObject(pp.sop, pp.getAnglef(), pp.posx, pp.posy, 1);
        pp.cursectnum = save_sectnum; // for speed

        DoPlayerHorizon(pp);
        DoTankTreads(pp);
    }

    public static void DoPlayerMoveTurret(PlayerStr pp) {
        PLAYER_RUN_LOCK(pp);

        DoPlayerTurnTurret(pp);

        if (PLAYER_MOVING(pp) == 0) {
            pp.Flags &= ~(PF_PLAYER_MOVED);
        } else {
            pp.Flags |= (PF_PLAYER_MOVED);
        }

        Sector_Object sop = SectorObject[pp.sop];

        OperateSectorObject(pp.sop, pp.getAnglef(), sop.xmid, sop.ymid, 1);

        DoPlayerHorizon(pp);
    }

    public static void DoPlayerBeginJump(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        pp.Flags |= (PF_JUMPING);
        pp.Flags &= ~(PF_FALLING);
        pp.Flags &= ~(PF_CRAWLING);
        pp.Flags &= ~(PF_LOCK_CRAWL);

        pp.floor_dist = PLAYER_JUMP_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_JUMP_CEILING_DIST;
        pp.friction = PLAYER_JUMP_FRICTION;

        pp.jump_speed = PLAYER_JUMP_AMT + pp.WadeDepth * 4;

        if (DoPlayerWadeSuperJump(pp)) {
            pp.jump_speed = PLAYER_JUMP_AMT - pp.WadeDepth * 5;
        }

        pp.JumpDuration = MAX_JUMP_DURATION;
        pp.DoPlayerAction = DoPlayerJump;

        NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Jump);
    }

    public static void DoPlayerBeginForceJump(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        pp.Flags |= (PF_JUMPING);
        pp.Flags &= ~(PF_FALLING | PF_CRAWLING | PF_CLIMBING | PF_LOCK_CRAWL);

        pp.JumpDuration = MAX_JUMP_DURATION;
        pp.DoPlayerAction = DoPlayerForceJump;

        pp.floor_dist = PLAYER_JUMP_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_JUMP_CEILING_DIST;
        pp.friction = PLAYER_JUMP_FRICTION;

        NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Jump);
    }

    public static void DoPlayerJump(PlayerStr pp) {
        // reset flag key for double jumps
        if (!TEST_SYNC_KEY(pp, SK_JUMP)) {
            FLAG_KEY_RESET(pp, SK_JUMP);
        }

        // instead of multiplying by synctics, use a loop for greater accuracy
        for (int i = 0; i < synctics; i++) {
            if (TEST_SYNC_KEY(pp, SK_JUMP)) {
                if (pp.JumpDuration > 0) {
                    pp.jump_speed -= PlayerGravity;
                    pp.JumpDuration--;
                }
            }

            // adjust jump speed by gravity - if jump speed greater than 0 player
            // have started falling
            if ((pp.jump_speed += PlayerGravity) > 0) {
                DoPlayerBeginFall(pp);
                DoPlayerFall(pp);
                return;
            }

            // adjust height by jump speed
            pp.posz += pp.jump_speed;

            // if player gets to close the ceiling while jumping
            if (PlayerCeilingHit(pp, pp.hiz + Z(4))) {
                // put player at the ceiling
                pp.posz = pp.hiz + Z(4);

                // reverse your speed to falling
                pp.jump_speed = -pp.jump_speed;

                // start falling
                DoPlayerBeginFall(pp);
                DoPlayerFall(pp);
                return;
            }

            // added this because jumping up to slopes or jumping on steep slopes
            // sometimes caused the view to go into the slope
            // if player gets to close the floor while jumping
            if (PlayerFloorHit(pp, pp.loz - pp.floor_dist)) {
                pp.posz = pp.loz - pp.floor_dist;

                pp.jump_speed = 0;
                PlayerSectorBound(pp, Z(1));
                DoPlayerBeginRun(pp);
                DoPlayerHeight(pp);
                return;
            }
        }

        if (PlayerFlyKey(pp)) {
            DoPlayerBeginFly(pp);
            return;
        }

        // If moving forward and tag is a ladder start climbing
        if (PlayerOnLadder(pp)) {
            DoPlayerBeginClimb(pp);
            return;
        }

        DoPlayerMove(pp);

        DoPlayerJumpHeight(pp);
    }

    public static void DoPlayerForceJump(PlayerStr pp) {
        int i;

        // instead of multiplying by synctics, use a loop for greater accuracy
        for (i = 0; i < synctics; i++) {
            // adjust jump speed by gravity - if jump speed greater than 0 player
            // have started falling
            if ((pp.jump_speed += PlayerGravity) > 0) {
                DoPlayerBeginFall(pp);
                DoPlayerFall(pp);
                return;
            }

            // adjust height by jump speed
            pp.posz += pp.jump_speed;

            // if player gets to close the ceiling while jumping
            if (PlayerCeilingHit(pp, pp.hiz + Z(4))) {
                // put player at the ceiling
                pp.posz = pp.hiz + Z(4);

                // reverse your speed to falling
                pp.jump_speed = -pp.jump_speed;

                // start falling
                DoPlayerBeginFall(pp);
                DoPlayerFall(pp);
                return;
            }
        }

        DoPlayerMove(pp);
    }

    public static void DoPlayerBeginFall(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        pp.Flags |= (PF_FALLING);
        pp.Flags &= ~(PF_JUMPING);
        pp.Flags &= ~(PF_CRAWLING);
        pp.Flags &= ~(PF_LOCK_CRAWL);

        pp.floor_dist = PLAYER_FALL_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_FALL_CEILING_DIST;
        pp.DoPlayerAction = DoPlayerFall;
        pp.friction = PLAYER_FALL_FRICTION;

        // Only change to falling frame if you were in the jump frame
        // Otherwise an animation may be messed up such as Running Jump Kick
        if (u.getRot() == u.ActorActionSet.Jump) {
            NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Fall);
        }
    }

    public static void StackedWaterSplash(PlayerStr pp) {
        if (FAF_ConnectArea(pp.cursectnum)) {
            Sprite psp = pp.getSprite();
            if (psp != null) {
                int sectnum = engine.updatesectorz(pp.posx, pp.posy, SPRITEp_BOS(psp), pp.cursectnum);
                if (SectorIsUnderwaterArea(sectnum)) {
                    PlaySound(DIGI_SPLASH1, pp, v3df_dontpan);
                }
            }
        }
    }

    public static void DoPlayerFall(PlayerStr pp) {
        // reset flag key for double jumps
        if (!TEST_SYNC_KEY(pp, SK_JUMP)) {
            FLAG_KEY_RESET(pp, SK_JUMP);
        }

        if (SectorIsUnderwaterArea(pp.cursectnum)) {
            StackedWaterSplash(pp);
            DoPlayerBeginDiveNoWarp(pp);
            return;
        }

        for (int i = 0; i < synctics; i++) {
            // adjust jump speed by gravity
            pp.jump_speed += PlayerGravity;
            if (pp.jump_speed > 4100) {
                pp.jump_speed = 4100;
            }

            // adjust player height by jump speed
            pp.posz += pp.jump_speed;

            if (pp.jump_speed > 2000) {
                PlayerSound(DIGI_FALLSCREAM, v3df_dontpan | v3df_doppler | v3df_follow, pp);
                handle = pp.TalkVocHandle; // Save id for later
            } else if (pp.jump_speed > 1300 && !cfg.isgMouseAim()) {
                if (TEST(pp.Flags, PF_LOCK_HORIZ)) {
                    pp.Flags &= ~(PF_LOCK_HORIZ);
                    pp.Flags |= (PF_LOOKING);
                }
            }

            int depth = GetZadjustment(pp.cursectnum, FLOOR_Z_ADJUST) >> 8;
            if (depth == 0) {
                depth = pp.WadeDepth;
            }

            int recoil_amt = 0;
            if (depth <= 20) {
                recoil_amt = Math.min(pp.jump_speed * 6, Z(35));
            }

            // need a test for head hits a sloped ceiling while falling
            // if player gets to close the Ceiling while Falling
            if (PlayerCeilingHit(pp, pp.hiz + pp.ceiling_dist)) {
                // put player at the ceiling
                pp.posz = pp.hiz + pp.ceiling_dist;
                // don't return or anything - allow to fall until
                // hit floor
            }

            if (PlayerFloorHit(pp, pp.loz - PLAYER_HEIGHT + recoil_amt)) {
                Sect_User sectu = getSectUser(pp.cursectnum);
                Sector sectp = boardService.getSector(pp.cursectnum);

                PlayerSectorBound(pp, Z(1));

                if (sectu != null && sectp != null && (DTEST(sectp.getExtra(), SECTFX_LIQUID_MASK) != SECTFX_LIQUID_NONE)) {
                    PlaySound(DIGI_SPLASH1, pp, v3df_dontpan);
                } else {
                    // Feet hitting ground sound
                    if (pp.jump_speed > 1020) {
                        PlaySound(DIGI_HITGROUND, pp, v3df_follow | v3df_dontpan);
                    }
                }

                if (handle != null && handle.isActive()) {
                    // My sound code will detect the sound has stopped and clean up
                    // for you.
                    handle.stop();
                    pp.PlayerTalking = false;
                    handle = null;
                }

                // i any kind of crawl key get rid of recoil
                if (DoPlayerTestCrawl(pp) || TEST_SYNC_KEY(pp, SK_CRAWL)) {
                    pp.posz = pp.loz - PLAYER_CRAWL_HEIGHT;
                } else {
                    // this was causing the z to snap immediately
                    // changed it so it stays gradual

                    pp.posz += recoil_amt;
                    DoPlayerHeight(pp);
                }

                // do some damage
                if (pp.jump_speed > 1700 && depth == 0) {

                    PlayerSound(DIGI_PLAYERPAIN2, v3df_follow | v3df_dontpan, pp);

                    if (pp.jump_speed > 1700 && pp.jump_speed < 4000) {
                        PlayerUpdateHealth(pp, -((pp.jump_speed - 1700) / 40));
                    } else if (pp.jump_speed >= 4000) {
                        USER u = getUser(pp.PlayerSprite);
                        if (u != null) {
                            PlayerUpdateHealth(pp, -u.Health); // Make sure he dies!
                            u.Health = 0;
                        }
                    }

                    PlayerCheckDeath(pp, -1);

                    if (TEST(pp.Flags, PF_DEAD)) {
                        return;
                    }
                }

                if (TEST_SYNC_KEY(pp, SK_CRAWL)) {
                    StackedWaterSplash(pp);
                    DoPlayerBeginCrawl(pp);
                    return;
                }

                if (PlayerCanDiveNoWarp(pp)) {
                    DoPlayerBeginDiveNoWarp(pp);
                    return;
                }

                StackedWaterSplash(pp);
                DoPlayerBeginRun(pp);
                return;
            }
        }

        if (PlayerFlyKey(pp)) {
            DoPlayerBeginFly(pp);
            return;
        }

        // If moving forward and tag is a ladder start climbing
        if (PlayerOnLadder(pp)) {
            DoPlayerBeginClimb(pp);
            return;
        }

        DoPlayerMove(pp);
    }

    public static void DoPlayerBeginClimb(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        if (sp == null) {
            return;
        }

        pp.Flags &= ~(PF_JUMPING | PF_FALLING);
        pp.Flags &= ~(PF_CRAWLING);
        pp.Flags &= ~(PF_LOCK_CRAWL);

        pp.DoPlayerAction = DoPlayerClimb;

        pp.Flags |= (PF_CLIMBING | PF_WEAPON_DOWN);
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YCENTER));

        NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerNinjaClimb);
    }

    public static void DoPlayerClimb(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sprite sp = pp.getSprite();
        if (sp == null || u == null) {
            return;
        }

        if (Prediction) {
            return;
        }

        pp.xvect += ((pp.input.vel * synctics * 2) << 6);
        pp.yvect += ((pp.input.svel * synctics * 2) << 6);
        pp.xvect = mulscale(pp.xvect, PLAYER_CLIMB_FRICTION, 16);
        pp.yvect = mulscale(pp.yvect, PLAYER_CLIMB_FRICTION, 16);
        if (klabs(pp.xvect) < 12800 && klabs(pp.yvect) < 12800) {
            pp.xvect = pp.yvect = 0;
        }

        int climbvel = FindDistance2D(pp.xvect, pp.yvect) >> 9;
        int dot = DOT_PRODUCT_2D(pp.xvect, pp.yvect, EngineUtils.sin(NORM_ANGLE(pp.getAnglei() + 512)), EngineUtils.sin(pp.getAnglei()));
        if (dot < 0) {
            climbvel = -climbvel;
        }

        // Run lock - routine doesn't call DoPlayerMove
        PLAYER_RUN_LOCK(pp);

        // need to rewrite this for FAF stuff

        // Jump off of the ladder
        if (TEST_SYNC_KEY(pp, SK_JUMP)) {
            pp.Flags &= ~(PF_CLIMBING | PF_WEAPON_DOWN);
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
            DoPlayerBeginJump(pp);
            return;
        }

        if (climbvel != 0) {
            // move player to center of ladder
            for (int i = synctics; i != 0; i--) {

                // player
                if (pp.posx != pp.lx) {
                    if (pp.posx < pp.lx) {
                        pp.posx += ADJ_AMT;
                    } else {
                        pp.posx -= ADJ_AMT;
                    }

                    if (klabs(pp.posx - pp.lx) <= ADJ_AMT) {
                        pp.posx = pp.lx;
                    }
                }

                if (pp.posy != pp.ly) {
                    if (pp.posy < pp.ly) {
                        pp.posy += ADJ_AMT;
                    } else {
                        pp.posy -= ADJ_AMT;
                    }

                    if (klabs(pp.posy - pp.ly) <= ADJ_AMT) {
                        pp.posy = pp.ly;
                    }
                }

                // sprite
                if (sp.getX() != u.sx) {
                    if (sp.getX() < u.sx) {
                        sp.setX(sp.getX() + ADJ_AMT);
                    } else if (sp.getX() > u.sx) {
                        sp.setX(sp.getX() - ADJ_AMT);
                    }

                    if (klabs(sp.getX() - u.sx) <= ADJ_AMT) {
                        sp.setX(u.sx);
                    }
                }

                if (sp.getY() != u.sy) {
                    if (sp.getY() < u.sy) {
                        sp.setY(sp.getY() + ADJ_AMT);
                    } else if (sp.getY() > u.sy) {
                        sp.setY(sp.getY() - ADJ_AMT);
                    }

                    if (klabs(sp.getY() - u.sy) <= ADJ_AMT) {
                        sp.setY(u.sy);
                    }
                }
            }
        }

        DoPlayerZrange(pp);

        // moving UP
        if (climbvel > 0) {
            int climb_amt = (climbvel >> 4) * 8;

            pp.climb_ndx &= 1023;

            pp.posz -= climb_amt;

            // if player gets to close the ceiling while climbing
            if (PlayerCeilingHit(pp, pp.hiz)) {
                // put player at the hiz
                pp.posz = pp.hiz;
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerNinjaClimb);
            }

            Sector ladderSec = boardService.getSector(pp.LadderSector);
            // if player gets to close the ceiling while climbing
            if (ladderSec != null && PlayerCeilingHit(pp, pp.hiz + Z(4))) {
                // put player at the ceiling
                pp.posz = ladderSec.getCeilingz() + Z(4);
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerNinjaClimb);
            }

            // if floor is ABOVE you && your head goes above it, do a jump up to
            // terrace
            if (ladderSec == null || pp.posz < ladderSec.getFloorz() - Z(6)) {
                pp.jump_speed = PLAYER_CLIMB_JUMP_AMT;
                pp.Flags &= ~(PF_CLIMBING | PF_WEAPON_DOWN);
                sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
                DoPlayerBeginForceJump(pp);
            }
        } else
            // move DOWN
            if (climbvel < 0) {
                int climb_amt = -(climbvel >> 4) * 8;

                pp.climb_ndx &= 1023;
                pp.posz += climb_amt;

                // if you are touching the floor
                // if (pp.posz >= pp.loz - Z(4) - PLAYER_HEIGHT)
                if (PlayerFloorHit(pp, pp.loz - Z(4) - PLAYER_HEIGHT)) {
                    // stand on floor
                    pp.posz = pp.loz - Z(4) - PLAYER_HEIGHT;

                    // if moving backwards start running
                    pp.Flags &= ~(PF_CLIMBING | PF_WEAPON_DOWN);
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
                    DoPlayerBeginRun(pp);
                    return;
                }
            } else {
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerNinjaClimb);
            }

        // setsprite to players location
        sp.setZ(pp.posz + PLAYER_HEIGHT);
        engine.changespritesect(pp.PlayerSprite, pp.cursectnum);

        DoPlayerHorizon(pp);

        boolean LadderUpdate = false;
        if (FAF_ConnectArea(pp.cursectnum)) {
            pp.cursectnum = engine.updatesectorz(pp.posx, pp.posy, pp.posz, pp.cursectnum);
            LadderUpdate = true;
        }

        if (WarpPlane(pp.posx, pp.posy, pp.posz, pp.cursectnum) != null) {
            pp.posx = warp.x;
            pp.posy = warp.y;
            pp.posz = warp.z;
            pp.cursectnum = warp.sectnum;
            PlayerWarpUpdatePos(pp);
            LadderUpdate = true;
        }

        if (LadderUpdate) {
            // constantly look for new ladder sector because of warping at any time
            engine.neartag(pp.posx, pp.posy, pp.posz, pp.cursectnum, pp.getAnglei(), neartag, 800, NTAG_SEARCH_LO_HI);

            Wall tagWall = boardService.getWall(neartag.tagwall);
            Sprite psp = pp.getSprite();
            if (psp != null && tagWall != null) {
                Sprite lsp = boardService.getSprite(FindNearSprite(psp, STAT_CLIMB_MARKER));
                if (lsp != null) {
                    // determine where the player is supposed to be in relation to the ladder
                    // move out in front of the ladder
                    int nx = MOVEx(100, lsp.getAng());
                    int ny = MOVEy(100, lsp.getAng());

                    // set angle player is supposed to face.
                    pp.LadderAngle = NORM_ANGLE(lsp.getAng() + 1024);
                    pp.LadderSector = tagWall.getNextsector();

                    // set players "view" distance from the ladder - needs to be farther than
                    // the sprite

                    pp.lx = lsp.getX() + nx * 5;
                    pp.ly = lsp.getY() + ny * 5;

                    pp.pang = pp.LadderAngle;
                }
            }
        }
    }

    public static boolean DoPlayerWadeSuperJump(PlayerStr pp) {
        Sector sec = boardService.getSector(pp.cursectnum);
        if (sec == null) {
            return false;
        }

        int zh = sec.getFloorz() - Z(pp.WadeDepth) - Z(2);
        if (Prediction) {
            return (false); // !JIM! 8/5/97 Teleporter FAFhitscan SuperJump bug.
        }

        for (int i = 0; i < 3; i++) {
            FAFhitscan(pp.posx, pp.posy, zh, pp.cursectnum, // Start position
                    EngineUtils.sin(NORM_ANGLE(pp.getAnglei() + angs[i] + 512)), // X vector of 3D ang
                    EngineUtils.sin(NORM_ANGLE(pp.getAnglei() + angs[i])), // Y vector of 3D ang
                    0, // Z vector of 3D ang
                    pHitInfo, CLIPMASK_MISSILE);

            Wall wall = boardService.getWall(pHitInfo.hitwall);
            if (wall != null && pHitInfo.hitsect != -1) {
                Sector hitSec = boardService.getSector(wall.getNextsector());
                if (hitSec != null && klabs(hitSec.getFloorz() - pp.posz) < Z(50)) {
                    Sprite psp = pp.getSprite();
                    if (psp != null && Distance(pp.posx, pp.posy, pHitInfo.hitx, pHitInfo.hity) < ((psp.getClipdist()) << 2) + 256) {
                        return (true);
                    }
                }
            }
        }

        return (false);
    }

    public static boolean PlayerFlyKey(PlayerStr pp) {
        if (!GodMode) {
            return (false);
        }

        if (MessageInputMode) {
            return (false);
        }

        boolean key = TEST_SYNC_KEY(pp, SK_FLY);
        if (key) {
            RESET_SYNC_KEY(pp, SK_FLY);
        }
        return (key);
    }

    public static void DoPlayerBeginCrawl(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        pp.Flags &= ~(PF_FALLING | PF_JUMPING);
        pp.Flags |= (PF_CRAWLING);

        pp.friction = PLAYER_CRAWL_FRICTION;
        pp.floor_dist = PLAYER_CRAWL_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_CRAWL_CEILING_DIST;
        pp.DoPlayerAction = DoPlayerCrawl;

        NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Crawl);
    }

    public static boolean PlayerFallTest(PlayerStr pp, int player_height) {
        // If the floor is far below you, fall hard instead of adjusting height
        if (klabs(pp.posz - pp.loz) > player_height + PLAYER_FALL_HEIGHT) {
            // if on a STEEP slope sector and you have not moved off of the sector
            Sector loSec = boardService.getSector(pp.lo_sectp);
            return loSec == null || klabs(loSec.getFloorheinum()) <= 3000
                    || !TEST(loSec.getFloorstat(), FLOOR_STAT_SLOPE) || pp.lo_sectp != pp.lastcursectnum;
        }

        return (false);
    }

    public static void DoPlayerCrawl(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        if (SectorIsUnderwaterArea(pp.cursectnum)) {
            // if stacked water - which it should be

            Sector psec = boardService.getSector(pp.cursectnum);
            if (psec != null && FAF_ConnectArea(pp.cursectnum)) {
                // adjust the z
                pp.posz = psec.getCeilingz() + Z(12);
            }

            DoPlayerBeginDiveNoWarp(pp);
        }

        if (TEST(pp.Flags, PF_LOCK_CRAWL)) {
            if (TEST_SYNC_KEY(pp, SK_CRAWL_LOCK)) {
                if (FLAG_KEY_PRESSED(pp, SK_CRAWL_LOCK)) {
                    if (klabs(pp.loz - pp.hiz) >= PLAYER_STANDING_ROOM) {
                        FLAG_KEY_RELEASE(pp, SK_CRAWL_LOCK);

                        pp.Flags &= ~(PF_CRAWLING);
                        DoPlayerBeginRun(pp);
                        return;
                    }
                }
            } else {
                FLAG_KEY_RESET(pp, SK_CRAWL_LOCK);
            }

            // Jump to get up
            if (TEST_SYNC_KEY(pp, SK_JUMP)) {
                if (klabs(pp.loz - pp.hiz) >= PLAYER_STANDING_ROOM) {
                    pp.Flags &= ~(PF_CRAWLING);
                    DoPlayerBeginRun(pp);
                    return;
                }
            }

        } else {
            // Let off of crawl to get up
            if (!TEST_SYNC_KEY(pp, SK_CRAWL)) {
                if (klabs(pp.loz - pp.hiz) >= PLAYER_STANDING_ROOM) {
                    pp.Flags &= ~(PF_CRAWLING);
                    DoPlayerBeginRun(pp);
                    return;
                }
            }
        }

        Sector loSec = boardService.getSector(pp.lo_sectp);
        if (loSec != null && TEST(loSec.getExtra(), SECTFX_CURRENT)) {
            DoPlayerCurrent(pp);
        }

        // Move around
        DoPlayerMove(pp);

        if (pp.WadeDepth > PLAYER_CRAWL_WADE_DEPTH) {
            pp.Flags &= ~(PF_CRAWLING);
            DoPlayerBeginRun(pp);
            return;
        }

        if (!TEST(pp.Flags, PF_PLAYER_MOVED)) {

            NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Crawl);
        }

        // If the floor is far below you, fall hard instead of adjusting height
        if (PlayerFallTest(pp, PLAYER_CRAWL_HEIGHT)) {
            pp.jump_speed = Z(1);
            pp.Flags &= ~(PF_CRAWLING);
            DoPlayerBeginFall(pp);
            // call PlayerFall now seems to iron out a hitch before falling
            DoPlayerFall(pp);
            return;
        }

        Sector psec = boardService.getSector(pp.cursectnum);
        if (psec != null && TEST(psec.getExtra(), SECTFX_DYNAMIC_AREA)) {
            pp.posz = pp.loz - PLAYER_CRAWL_HEIGHT;
        }

        DoPlayerBob(pp);
        DoPlayerCrawlHeight(pp);
    }

    public static void DoPlayerBeginFly(PlayerStr pp) {
        pp.Flags &= ~(PF_FALLING | PF_JUMPING | PF_CRAWLING);
        pp.Flags |= (PF_FLYING);

        pp.friction = PLAYER_FLY_FRICTION;
        pp.floor_dist = PLAYER_RUN_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_RUN_CEILING_DIST;
        pp.DoPlayerAction = DoPlayerFly;

        pp.z_speed = -Z(10);
        pp.jump_speed = 0;
        pp.bob_amt = 0;
        pp.bob_ndx = 1024;

        NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerNinjaFly);
    }

    public static void PlayerWarpUpdatePos(PlayerStr pp) {
        if (Prediction) {
            return;
        }

        pp.oposx = pp.posx;
        pp.oposy = pp.posy;
        pp.oposz = pp.posz;
        DoPlayerZrange(pp);
        UpdatePlayerSprite(pp);
    }

    public static boolean PlayerCeilingHit(PlayerStr pp, int zlimit) {
        return pp.posz < zlimit;
    }

    public static boolean PlayerFloorHit(PlayerStr pp, int zlimit) {
        return pp.posz > zlimit;
    }

    public static void DoPlayerFly(PlayerStr pp) {
        if (SectorIsUnderwaterArea(pp.cursectnum)) {
            DoPlayerBeginDiveNoWarp(pp);
            return;
        }

        if (TEST_SYNC_KEY(pp, SK_CRAWL)) {
            pp.z_speed += PLAYER_FLY_INC;

            if (pp.z_speed > PLAYER_FLY_MAX_SPEED) {
                pp.z_speed = PLAYER_FLY_MAX_SPEED;
            }
        }

        if (TEST_SYNC_KEY(pp, SK_JUMP)) {
            pp.z_speed -= PLAYER_FLY_INC;

            if (pp.z_speed < -PLAYER_FLY_MAX_SPEED) {
                pp.z_speed = -PLAYER_FLY_MAX_SPEED;
            }
        }

        pp.z_speed = mulscale(pp.z_speed, 58000, 16);

        pp.posz += pp.z_speed;

        // Make the min distance from the ceiling/floor match bobbing amount
        // so the player never goes into the ceiling/floor

        // Only get so close to the ceiling
        if (PlayerCeilingHit(pp, pp.hiz + PLAYER_FLY_BOB_AMT + Z(8))) {
            pp.posz = pp.hiz + PLAYER_FLY_BOB_AMT + Z(8);
            pp.z_speed = 0;
        }

        // Only get so close to the floor
        if (PlayerFloorHit(pp, pp.loz - PLAYER_HEIGHT - PLAYER_FLY_BOB_AMT)) {
            pp.posz = pp.loz - PLAYER_HEIGHT - PLAYER_FLY_BOB_AMT;
            pp.z_speed = 0;
        }

        if (PlayerFlyKey(pp)) {
            pp.Flags &= ~(PF_FLYING);
            pp.bob_amt = 0;
            pp.bob_ndx = 0;
            DoPlayerBeginFall(pp);
            DoPlayerFall(pp);
            return;
        }

        DoPlayerMove(pp);
    }

    public static int FindNearSprite(@NotNull Sprite sp, int stat) {
        int dist, near_dist = 15000;
        Sprite fp;
        int near_fp = -1;

        ListNode<Sprite> next_fs;
        for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = next_fs) {
            next_fs = node.getNext();
            fp = node.get();

            dist = Distance(sp.getX(), sp.getY(), fp.getX(), fp.getY());
            if (dist < near_dist) {
                near_dist = dist;
                near_fp = node.getIndex();
            }
        }

        return (near_fp);
    }

    public static boolean PlayerOnLadder(PlayerStr pp) {
        if (Prediction) {
            return false;
        }

        engine.neartag(pp.posx, pp.posy, pp.posz, pp.cursectnum, pp.getAnglei(), neartag, 1024 + 768, NTAG_SEARCH_LO_HI);
        int neartagwall = neartag.tagwall;

        int dir = DOT_PRODUCT_2D(pp.xvect, pp.yvect, EngineUtils.sin(NORM_ANGLE(pp.getAnglei() + 512)), EngineUtils.sin(pp.getAnglei()));
        if (dir < 0) {
            return (false);
        }

        Wall nearWall = boardService.getWall(neartagwall);
        if (nearWall == null || nearWall.getLotag() != TAG_WALL_CLIMB) {
            return (false);
        }

        Wall tagWall = null;
        for (short angle : angles) {
            engine.neartag(pp.posx, pp.posy, pp.posz, pp.cursectnum, NORM_ANGLE(pp.getAnglei() + angle), neartag, 600,
                    NTAG_SEARCH_LO_HI);

            int dist = neartag.taghitdist;
            tagWall = boardService.getWall(neartag.tagwall);
            if (tagWall == null || dist < 100 || tagWall.getLotag() != TAG_WALL_CLIMB) {
                return (false);
            }

            FAFhitscan(pp.posx, pp.posy, pp.posz, pp.cursectnum, EngineUtils.sin(NORM_ANGLE(pp.getAnglei() + angle + 512)),
                    EngineUtils.sin(NORM_ANGLE(pp.getAnglei() + angle)), 0, pHitInfo, CLIPMASK_MISSILE);

            Sprite hitSpr = boardService.getSprite(pHitInfo.hitsprite);
            if (hitSpr != null) {
                // if the sprite blocking you hit is not a wall sprite there is something
                // between
                // you and the ladder
                if (TEST(hitSpr.getCstat(), CSTAT_SPRITE_BLOCK) && !TEST(hitSpr.getCstat(), CSTAT_SPRITE_WALL)) {
                    return (false);
                }
            } else {
                Wall hitWall = boardService.getWall(pHitInfo.hitwall);
                // if you hit a wall and it is not a climb wall - forget it
                if (hitWall != null && hitWall.getLotag() != TAG_WALL_CLIMB) {
                    return (false);
                }
            }
        }

        Sprite psp = pp.getSprite();
        if (psp == null) {
            return false;
        }

        int lspi = FindNearSprite(psp, STAT_CLIMB_MARKER);
        Sprite lsp = boardService.getSprite(lspi);
        if (lsp == null) {
            return (false);
        }

        // determine where the player is supposed to be in relation to the ladder
        // move out in front of the ladder
        int nx = MOVEx(100, lsp.getAng());
        int ny = MOVEy(100, lsp.getAng());

        // set angle player is supposed to face.
        pp.LadderAngle = NORM_ANGLE(lsp.getAng() + 1024);
        pp.LadderSector = tagWall.getNextsector();

        // set players "view" distance from the ladder - needs to be farther than
        // the sprite

        pp.lx = lsp.getX() + nx * 5;
        pp.ly = lsp.getY() + ny * 5;

        pp.pang = pp.LadderAngle;

        return (true);
    }

    public static boolean DoPlayerTestCrawl(PlayerStr pp) {
        return klabs(pp.loz - pp.hiz) < PLAYER_STANDING_ROOM;
    }

    public static boolean PlayerInDiveArea(PlayerStr pp) {
        if (boardService.getSector(pp.lo_sectp) != null) {
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // Attention: This changed on 07/29/97
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            Sector sectp = boardService.getSector(pp.cursectnum);
            if (sectp != null && TEST(sectp.getExtra(), SECTFX_DIVE_AREA)) {
                CheckFootPrints(pp);
                return (true);
            }
        }
        return (false);
    }

    public static boolean PlayerCanDive(PlayerStr pp) {
        if (Prediction) {
            return (false);
        }

        // Crawl - check for diving
        if (TEST_SYNC_KEY(pp, SK_CRAWL) || TEST_SYNC_KEY(pp, SK_CRAWL_LOCK) || pp.jump_speed > 0) {
            if (PlayerInDiveArea(pp)) {
                pp.posz += Z(20);
                pp.z_speed = Z(20);
                pp.jump_speed = 0;

                if (pp.posz > pp.loz - Z(pp.WadeDepth) - Z(2)) {
                    DoPlayerBeginDive(pp);
                }

                return (true);
            }
        }

        return (false);
    }

    public static boolean PlayerCanDiveNoWarp(PlayerStr pp) {
        if (Prediction) {
            return (false);
        }

        // check for diving
        if (pp.jump_speed > 1400) {
            Sprite psp = pp.getSprite();
            if (psp != null && FAF_ConnectArea(pp.cursectnum)) {
                int sectnum = engine.updatesectorz(pp.posx, pp.posy, SPRITEp_BOS(psp), pp.cursectnum);
                Sector sec = boardService.getSector(sectnum);
                if (sec != null && SectorIsUnderwaterArea(sectnum)) {
                    pp.cursectnum = sectnum;
                    pp.posz = sec.getCeilingz();

                    pp.posz += Z(20);
                    pp.z_speed = Z(20);
                    pp.jump_speed = 0;

                    PlaySound(DIGI_SPLASH1, pp, v3df_dontpan);
                    DoPlayerBeginDiveNoWarp(pp);
                    return (true);
                }
            }
        }

        return (false);
    }

    public static int GetOverlapSector(int x, int y, LONGp over, LONGp under) {
        Sect_User usu = getSectUser(under.value);
        Sect_User osu = getSectUser(over.value);
        if ((usu != null && usu.number >= 30000)
                || (osu != null && osu.number >= 30000)) {
            return (GetOverlapSector2(x, y, over, under));
        }

        int found = 0;
        // instead of check ALL sectors, just check the two most likely first
        if (engine.inside(x, y,  over.value) != 0) {
            sf[found] =  over.value;
            found++;
        }

        if (engine.inside(x, y,  under.value) != 0) {
            sf[found] =  under.value;
            found++;
        }

        // if nothing was found, check them all
        if (found == 0) {
            for (int i = 0; i < boardService.getSectorCount() && found < 2; i++) {
                if (engine.inside(x, y,  i) != 0) {
                    sf[found] =  i;
                    found++;
                }
            }
        }

        if (found == 0) {
            return 0;
        }

        // the are overlaping - check the z coord
        if (found == 2) {
            Sector s0 = boardService.getSector(sf[0]);
            Sector s1 = boardService.getSector(sf[1]);
            if (s0 != null && s1 != null && s0.getFloorz() > s1.getFloorz()) {
                under.value = sf[0];
                over.value = sf[1];
            } else {
                under.value = sf[1];
                over.value = sf[0];
            }
        } else { // the are NOT overlaping
            over.value = sf[0];
            under.value = -1;
        }

        return (found);
    }

    private static int GetOverlapSector2(int x, int y, LONGp over, LONGp under) {
        // NOTE: For certain heavily overlapped areas in $seabase this is a better
        // method.

        int found = 0;
        // instead of check ALL sectors, just check the two most likely first
        if (engine.inside(x, y,  over.value) != 0) {
            sf[found] =  over.value;
            found++;
        }

        if (engine.inside(x, y,  under.value) != 0) {
            sf[found] =  under.value;
            found++;
        }

        ListNode<Sprite> nexti;

        // if nothing was found, check them all
        if (found == 0) {
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_DIVE_AREA); node != null; node = nexti) {
                nexti = node.getNext();
                Sprite sp = node.get();

                if (engine.inside(x, y, sp.getSectnum()) != 0) {
                    sf[found] = sp.getSectnum();
                    found++;
                }
            }

            for (int i : UnderStatList) {
                for (ListNode<Sprite> node = boardService.getStatNode(i); node != null; node = nexti) {
                    nexti = node.getNext();
                    Sprite sp = node.get();
                    // ignore underwater areas with lotag of 0
                    if (sp.getLotag() == 0) {
                        continue;
                    }

                    if (engine.inside(x, y, sp.getSectnum()) != 0) {
                        sf[found] = sp.getSectnum();
                        found++;
                    }
                }
            }
        }

        if (found == 0) {
            return 0;
        }

        // the are overlaping - check the z coord
        if (found == 2) {
            Sector s0 = boardService.getSector(sf[0]);
            Sector s1 = boardService.getSector(sf[1]);
            if (s0 != null && s1 != null && s0.getFloorz() > s1.getFloorz()) {
                under.value = sf[0];
                over.value = sf[1];
            } else {
                under.value = sf[1];
                over.value = sf[0];
            }
        } else { // the are NOT overlaping
            over.value = sf[0];
            under.value = -1;
        }

        return (found);
    }

    public static void DoPlayerWarpToUnderwater(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sect_User sectu = getSectUser(pp.cursectnum);

        if (sectu == null || u == null || Prediction) {
            return;
        }

        Sprite under_sp = null, over_sp = null;
        boolean Found = false;

        // search for DIVE_AREA "over" sprite for reference point
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_DIVE_AREA); node != null; node = nexti) {
            nexti = node.getNext();
            over_sp = node.get();
            Sector s = boardService.getSector(over_sp.getSectnum());
            Sect_User su = getSectUser(over_sp.getSectnum());

            if (s != null && TEST(s.getExtra(), SECTFX_DIVE_AREA) && su != null
                    && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return;
        }

        Found = false;

        // search for UNDERWATER "under" sprite for reference point
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_UNDERWATER); node != null; node = nexti) {
            nexti = node.getNext();
            under_sp = node.get();
            Sector s = boardService.getSector(under_sp.getSectnum());
            Sect_User su = getSectUser(under_sp.getSectnum());

            if (s != null && TEST(s.getExtra(), SECTFX_UNDERWATER) && su != null
                    && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return;
        }

        // get the offset from the sprite
        u.sx = over_sp.getX() - pp.posx;
        u.sy = over_sp.getY() - pp.posy;

        // update to the new x y position
        pp.posx = under_sp.getX() - u.sx;
        pp.posy = under_sp.getY() - u.sy;

        if (GetOverlapSector(pp.posx, pp.posy, tmp_ptr[0].set(over_sp.getSectnum()),
                tmp_ptr[1].set(under_sp.getSectnum())) == 2) {
            pp.cursectnum =  tmp_ptr[1].value;
        } else {
            pp.cursectnum =  tmp_ptr[0].value;
        }

        Sector s = boardService.getSector(under_sp.getSectnum());
        if (s != null) {
            pp.posz = s.getCeilingz() + Z(6);
        }

        pp.oposx = pp.posx;
        pp.oposy = pp.posy;
        pp.oposz = pp.posz;

        DoPlayerZrange(pp);
    }

    public static void DoPlayerWarpToSurface(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sect_User sectu = getSectUser(pp.cursectnum);

        Sprite under_sp = null, over_sp = null;
        boolean Found = false;

        if (sectu == null || u == null || Prediction) {
            return;
        }

        // search for UNDERWATER "under" sprite for reference point
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_UNDERWATER); node != null; node = nexti) {
            nexti = node.getNext();
            under_sp = node.get();
            Sector s = boardService.getSector(under_sp.getSectnum());
            Sect_User su = getSectUser(under_sp.getSectnum());

            if (s != null && TEST(s.getExtra(), SECTFX_UNDERWATER) && su != null
                    && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return;
        }

        Found = false;

        // search for DIVE_AREA "over" sprite for reference point
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_DIVE_AREA); node != null; node = nexti) {
            nexti = node.getNext();
            over_sp = node.get();
            Sector s = boardService.getSector(over_sp.getSectnum());
            Sect_User su = getSectUser(over_sp.getSectnum());

            if (s != null && TEST(s.getExtra(), SECTFX_DIVE_AREA) && su != null
                    && su.number == sectu.number) {
                Found = true;
                break;
            }
        }

        if (!Found) {
            return;
        }

        // get the offset from the under sprite
        u.sx = under_sp.getX() - pp.posx;
        u.sy = under_sp.getY() - pp.posy;

        // update to the new x y position
        pp.posx = over_sp.getX() - u.sx;
        pp.posy = over_sp.getY() - u.sy;

        if (GetOverlapSector(pp.posx, pp.posy, tmp_ptr[0].set(over_sp.getSectnum()), tmp_ptr[1].set(under_sp.getSectnum())) != 0) {
            pp.cursectnum =  tmp_ptr[0].value;
        }
        Sector s = boardService.getSector(over_sp.getSectnum());
        if (s != null) {
            pp.posz = s.getFloorz() - Z(2);
        }

        // set z range and wade depth so we know how high to set view
        DoPlayerZrange(pp);
        DoPlayerSetWadeDepth(pp);

        pp.posz -= Z(pp.WadeDepth);

        pp.oposx = pp.posx;
        pp.oposy = pp.posy;
        pp.oposz = pp.posz;

    }

    public static void DoPlayerBeginDive(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);

        if (sp == null || u == null || Prediction) {
            return;
        }

        if (pp.Bloody) {
            pp.Bloody = false; // Water washes away the blood
        }

        pp.Flags |= (PF_DIVING);
        DoPlayerDivePalette(pp);
        DoPlayerNightVisionPalette(pp);

        if (pp == Player[screenpeek]) {
            COVER_SetReverb(140); // Underwater echo
            pp.Reverb = 140;
        }

        SpawnSplash(pp.PlayerSprite);

        DoPlayerWarpToUnderwater(pp);
        OperateTripTrigger(pp);

        pp.Flags &= ~(PF_JUMPING | PF_FALLING);
        pp.Flags &= ~(PF_CRAWLING);
        pp.Flags &= ~(PF_LOCK_CRAWL);

        pp.friction = PLAYER_DIVE_FRICTION;
        pp.ceiling_dist = PLAYER_DIVE_CEILING_DIST;
        pp.floor_dist = PLAYER_DIVE_FLOOR_DIST;
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YCENTER));
        pp.DoPlayerAction = DoPlayerDive;

        pp.DiveTics = PLAYER_DIVE_TIME;
        pp.DiveDamageTics = 0;

        DoPlayerMove(pp); // needs to be called to reset the pp.loz/hiz variable

        NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Dive);

        DoPlayerDive(pp);
    }

    public static void DoPlayerBeginDiveNoWarp(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);

        if (sp == null || u == null || Prediction) {
            return;
        }

        if (!SectorIsUnderwaterArea(pp.cursectnum)) {
            return;
        }

        if (pp.Bloody) {
            pp.Bloody = false; // Water washes away the blood
        }

        if (pp == Player[screenpeek]) {
            COVER_SetReverb(140); // Underwater echo
            pp.Reverb = 140;
        }

        CheckFootPrints(pp);
        Sector loSec = boardService.getSector(pp.lo_sectp);

        if (loSec != null && DTEST(loSec.getExtra(), SECTFX_LIQUID_MASK) == SECTFX_LIQUID_LAVA) {
            pp.Flags |= (PF_DIVING_IN_LAVA);
            u.DamageTics = 0;
        }

        pp.Flags |= (PF_DIVING);
        DoPlayerDivePalette(pp);
        DoPlayerNightVisionPalette(pp);

        pp.Flags &= ~(PF_JUMPING | PF_FALLING);

        pp.friction = PLAYER_DIVE_FRICTION;
        pp.ceiling_dist = PLAYER_DIVE_CEILING_DIST;
        pp.floor_dist = PLAYER_DIVE_FLOOR_DIST;
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YCENTER));
        pp.DoPlayerAction = DoPlayerDive;
        pp.z_speed = 0;
        pp.DiveTics = PLAYER_DIVE_TIME;
        pp.DiveDamageTics = 0;
        DoPlayerMove(pp); // needs to be called to reset the pp.loz/hiz variable

        NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Dive);
        DoPlayerDive(pp);
    }

    public static void DoPlayerStopDiveNoWarp(PlayerStr pp) {
        Sprite psp = pp.getSprite();
        if (psp == null || Prediction) {
            return;
        }

        if (pp.TalkVocHandle != null && pp.TalkVocHandle.isActive()) {
            pp.TalkVocHandle.stop();
            pp.TalkVocHandle = null;
            pp.TalkVocnum = -1;
            pp.PlayerTalking = false;
        }

        // stop diving no warp
        PlayerSound(DIGI_SURFACE, v3df_dontpan | v3df_follow | v3df_doppler, pp);

        pp.bob_amt = 0;

        pp.Flags &= ~(PF_DIVING | PF_DIVING_IN_LAVA);
        DoPlayerDivePalette(pp);
        DoPlayerNightVisionPalette(pp);
        psp.setCstat(psp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
        if (pp == Player[screenpeek]) {
            COVER_SetReverb(0);
            pp.Reverb = 0;
        }

        DoPlayerZrange(pp);
    }

    public static void DoPlayerStopDive(PlayerStr pp) {
        Sprite sp = pp.getSprite();

        if (sp == null || Prediction) {
            return;
        }

        if (pp.TalkVocHandle != null && pp.TalkVocHandle.isActive()) {
            pp.TalkVocHandle.stop();
        }
        pp.TalkVocnum = -1;
        pp.TalkVocHandle = null;
        pp.PlayerTalking = false;

        // stop diving with warp
        PlayerSound(DIGI_SURFACE, v3df_dontpan | v3df_follow | v3df_doppler, pp);

        pp.bob_amt = 0;
        DoPlayerWarpToSurface(pp);
        DoPlayerBeginWade(pp);
        pp.Flags &= ~(PF_DIVING | PF_DIVING_IN_LAVA);

        DoPlayerDivePalette(pp);
        DoPlayerNightVisionPalette(pp);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
        if (pp == Player[screenpeek]) {
            COVER_SetReverb(0);
            pp.Reverb = 0;
        }
    }

    public static void DoPlayerDiveMeter(PlayerStr pp) {
        int color, metertics, meterunit;
        int y;

        if (NoMeters) {
            return;
        }

        // Don't draw bar from other players
        if (pp != Player[myconnectindex]) {
            return;
        }

        if (!TEST(pp.Flags, PF_DIVING | PF_DIVING_IN_LAVA)) {
            return;
        }

        Renderer renderer = game.getRenderer();
        meterunit = PLAYER_DIVE_TIME / 30;
        metertics = (pp.DiveTics / meterunit);

        if (metertics <= 0 && !TEST(pp.Flags, PF_DIVING | PF_DIVING_IN_LAVA)) {
            return;
        }

        if (metertics <= 0) {
            return;
        }

        if (numplayers < 2) {
            y = 10;
        } else if (numplayers <= 4) {
            y = 20;
        } else {
            y = 30;
        }

        if (metertics <= 12 && metertics > 6) {
            color = 20;
        } else if (metertics <= 6) {
            color = 25;
        } else {
            color = 22;
        }

        renderer.rotatesprite((200 + 8) << 16, y << 16, 65536, 0, 5408, 1, 1, (ROTATE_SPRITE_SCREEN_CLIP));

        renderer.rotatesprite((218 + 47) << 16, y << 16, 65536, 0, 5406 - metertics, 1, color,
                (ROTATE_SPRITE_SCREEN_CLIP));
    }

    public static void DoPlayerDive(PlayerStr pp) {
        Sector psec = boardService.getSector(pp.cursectnum);
        USER u = getUser(pp.PlayerSprite);
        Sect_User sectu = getSectUser(pp.cursectnum);
        if (psec == null || u == null) {
            return;
        }

        // whenever your view is not in a water area
        if (!SectorIsUnderwaterArea(pp.cursectnum)) {
            DoPlayerStopDiveNoWarp(pp);
            DoPlayerBeginRun(pp);
            return;
        }

        if ((pp.DiveTics -= synctics) < 0) {
            if ((pp.DiveDamageTics -= synctics) < 0) {
                pp.DiveDamageTics = PLAYER_DIVE_DAMAGE_TIME;
                // PlayerUpdateHealth(pp, PLAYER_DIVE_DAMAGE_AMOUNT);
                PlayerSound(DIGI_WANGDROWNING, v3df_dontpan | v3df_follow, pp);
                PlayerUpdateHealth(pp, -3 - (RANDOM_RANGE(7 << 8) >> 8));
                PlayerCheckDeath(pp, -1);
                if (TEST(pp.Flags, PF_DEAD)) {
                    return;
                }
            }
        }

        Sector loSec = boardService.getSector(pp.lo_sectp);
        // underwater current
        if (loSec != null && TEST(loSec.getExtra(), SECTFX_CURRENT)) {
            DoPlayerCurrent(pp);
        }

        // while diving in lava
        // every DamageTics time take some damage
        if (TEST(pp.Flags, PF_DIVING_IN_LAVA)) {
            if ((u.DamageTics -= synctics) < 0) {
                u.DamageTics = 30; // !JIM! Was DAMAGE_TIME

                PlayerUpdateHealth(pp, -40);
            }
        }

        if (TEST_SYNC_KEY(pp, SK_CRAWL) || TEST_SYNC_KEY(pp, SK_CRAWL_LOCK)) {
            pp.z_speed += PLAYER_DIVE_INC;

            if (pp.z_speed > PLAYER_DIVE_MAX_SPEED) {
                pp.z_speed = PLAYER_DIVE_MAX_SPEED;
            }
        }

        if (TEST_SYNC_KEY(pp, SK_JUMP)) {
            pp.z_speed -= PLAYER_DIVE_INC;

            if (pp.z_speed < -PLAYER_DIVE_MAX_SPEED) {
                pp.z_speed = -PLAYER_DIVE_MAX_SPEED;
            }
        }

        pp.z_speed = mulscale(pp.z_speed, 58000, 16);

        if (klabs(pp.z_speed) < 16) {
            pp.z_speed = 0;
        }

        pp.posz += pp.z_speed;

        if (pp.z_speed < 0 && FAF_ConnectArea(pp.cursectnum)) {
            if (pp.posz < psec.getCeilingz() + Z(10)) {
                int sectnum = pp.cursectnum;

                // check for sector above to see if it is an underwater sector also
                sectnum = engine.updatesectorz(pp.posx, pp.posy, psec.getCeilingz() - Z(8), sectnum);

                if (sectnum >= 0 && !SectorIsUnderwaterArea(sectnum)) {
                    // if not underwater sector we must surface
                    // force into above sector
                    pp.posz = psec.getCeilingz() - Z(8);
                    pp.cursectnum = sectnum;
                    DoPlayerStopDiveNoWarp(pp);
                    DoPlayerBeginRun(pp);
                    return;
                }
            }
        }

        // Only get so close to the ceiling
        // if its a dive sector without a match or a UNDER2 sector with CANT_SURFACE set
        if (sectu != null && (sectu.number == 0 || TEST(sectu.flags, SECTFU_CANT_SURFACE))) {
            // for room over room water the hiz will be the top rooms ceiling
            if (pp.posz < pp.hiz + pp.ceiling_dist) {
                pp.posz = pp.hiz + pp.ceiling_dist;
            }
        } else {
            // close to a warping sector - stop diveing with a warp to surface
            // !JIM! FRANK - I added !pp.hi_sp so that you don't warp to surface when
            // there is a sprite above you since getzrange returns a hiz < ceiling height
            // if you are clipping into a sprite and not the ceiling.
            if (pp.posz < pp.hiz + Z(4) && pp.hi_sp == -1) {
                DoPlayerStopDive(pp);
                return;
            }
        }

        // Only get so close to the floor
        if (pp.posz >= pp.loz - PLAYER_DIVE_HEIGHT) {
            pp.posz = pp.loz - PLAYER_DIVE_HEIGHT;
        }

        // make player bob if sitting still
        if (PLAYER_MOVING(pp) == 0 && pp.z_speed == 0 && pp.up_speed == 0) {
            DoPlayerSpriteBob(pp, PLAYER_DIVE_HEIGHT, PLAYER_DIVE_BOB_AMT, 3);
        }
        // player is moving
        else {
            // if bob_amt is approx 0
            if (klabs(pp.bob_amt) < Z(1)) {
                pp.bob_amt = 0;
                pp.bob_ndx = 0;
            }
            // else keep bobbing until its back close to 0
            else {
                DoPlayerSpriteBob(pp, PLAYER_DIVE_HEIGHT, PLAYER_DIVE_BOB_AMT, 3);
            }
        }

        // Reverse bobbing when getting close to the floor
        if (pp.posz + pp.bob_amt >= pp.loz - PLAYER_DIVE_HEIGHT) {
            pp.bob_ndx = NORM_ANGLE(pp.bob_ndx + ((1024 + 512) - pp.bob_ndx) * 2);
            DoPlayerSpriteBob(pp, PLAYER_DIVE_HEIGHT, PLAYER_DIVE_BOB_AMT, 3);
        }
        // Reverse bobbing when getting close to the ceiling
        if (pp.posz + pp.bob_amt < pp.hiz + pp.ceiling_dist) {
            pp.bob_ndx = NORM_ANGLE(pp.bob_ndx + ((512) - pp.bob_ndx) * 2);
            DoPlayerSpriteBob(pp, PLAYER_DIVE_HEIGHT, PLAYER_DIVE_BOB_AMT, 3);
        }

        DoPlayerMove(pp);

        if (!Prediction && pp.z_speed != 0 && ((RANDOM_P2(1024 << 5) >> 5) < 64)
                || (PLAYER_MOVING(pp) != 0 && (RANDOM_P2(1024 << 5) >> 5) < 64)) {
            PlaySound(DIGI_BUBBLES, pp, v3df_none);
            int bubble =  SpawnBubble(pp.PlayerSprite);
            Sprite bp = boardService.getSprite(bubble);
            if (bp != null) {

                // back it up a bit to get it out of your face
                int nx = MOVEx((128 + 64), NORM_ANGLE(bp.getAng() + 1024));
                int ny = MOVEy((128 + 64), NORM_ANGLE(bp.getAng() + 1024));

                move_sprite(bubble, nx, ny, 0, u.ceiling_dist, u.floor_dist, 0, synctics);
            }
        }
    }

    public static boolean DoPlayerTestPlaxDeath(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        Sector sec = boardService.getSector(pp.lo_sectp);

        // landed on a paralax floor
        if (u != null && sec != null && TEST(sec.getFloorstat(), FLOOR_STAT_PLAX)) {
            PlayerUpdateHealth(pp, -u.Health);
            PlayerCheckDeath(pp, -1);
            return (true);
        }

        return (false);
    }

    private static void DoPlayerCurrent(PlayerStr pp) {
        if (!boardService.isValidSector(pp.cursectnum)) {
            return;
        }

        Sprite psp = pp.getSprite();
        Sect_User sectu = getSectUser(pp.cursectnum);
        if (psp == null || sectu == null) {
            return;
        }

        int xvect = sectu.speed * synctics * EngineUtils.sin(NORM_ANGLE(sectu.ang + 512)) >> 4;
        int yvect = sectu.speed * synctics * EngineUtils.sin(sectu.ang) >> 4;

        int push_ret = engine.pushmove(pp.posx, pp.posy, pp.posz, pp.cursectnum, (psp.getClipdist() << 2),
                pp.ceiling_dist, pp.floor_dist, CLIPMASK_PLAYER);

        if (pushmove_sectnum != -1) {
            pp.posx = pushmove_x;
            pp.posy = pushmove_y;
            pp.posz = pushmove_z;
            pp.cursectnum = pushmove_sectnum;
        }

        if (push_ret < 0) {
            if (!TEST(pp.Flags, PF_DEAD)) {
                USER u = getUser(pp.PlayerSprite);
                if (u != null) {
                    PlayerUpdateHealth(pp, -u.Health); // Make sure he dies!
                    PlayerCheckDeath(pp, -1);

                    if (TEST(pp.Flags, PF_DEAD)) {
                        return;
                    }
                }
            }
            return;
        }
        engine.clipmove(pp.posx, pp.posy, pp.posz, pp.cursectnum, xvect, yvect, (psp.getClipdist() << 2),
                pp.ceiling_dist, pp.floor_dist, CLIPMASK_PLAYER);

        if (clipmove_sectnum != -1) {
            pp.posx = clipmove_x;
            pp.posy = clipmove_y;
            pp.posz = clipmove_z;
            pp.cursectnum = clipmove_sectnum;
        }

        PlayerCheckValidMove(pp);
        engine.pushmove(pp.posx, pp.posy, pp.posz, pp.cursectnum, (psp.getClipdist() << 2), pp.ceiling_dist,
                pp.floor_dist, CLIPMASK_PLAYER);

        if (pushmove_sectnum != -1) {
            pp.posx = pushmove_x;
            pp.posy = pushmove_y;
            pp.posz = pushmove_z;
            pp.cursectnum = pushmove_sectnum;
        }
    }

    public static void DoPlayerFireOutWater(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);

        if (Prediction) {
            return;
        }

        if (u != null && pp.WadeDepth > 20) {
            if (u.flame >= 0) {
                SetSuicide(u.flame);
            }
            u.flame = -2;
        }
    }

    public static void DoPlayerFireOutDeath(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null || Prediction) {
            return;
        }

        if (u.flame >= 0) {
            SetSuicide(u.flame);
        }

        u.flame = -2;
    }

    public static void DoPlayerBeginWade(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);

        // landed on a paralax floor?
        if (DoPlayerTestPlaxDeath(pp)) {
            return;
        }

        pp.Flags &= ~(PF_JUMPING | PF_FALLING);
        pp.Flags &= ~(PF_CRAWLING);

        pp.friction = PLAYER_WADE_FRICTION;
        pp.floor_dist = PLAYER_WADE_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_WADE_CEILING_DIST;
        pp.DoPlayerAction = DoPlayerWade;

        DoPlayerFireOutWater(pp);

        if (pp.jump_speed > 100) {
            SpawnSplash(pp.PlayerSprite);
        }

        // fix it so that you won't go under water unless you hit the water at a
        // certain speed
        if (pp.jump_speed > 0 && pp.jump_speed < 1300) {
            pp.jump_speed = 0;
        }

        if (u != null) {
            NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Run);
        }
    }

    public static void DoPlayerWade(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);

        DoPlayerFireOutWater(pp);

        if (TEST_SYNC_KEY(pp, SK_OPERATE)) {
            if (FLAG_KEY_PRESSED(pp, SK_OPERATE)) {
                Sector sec = boardService.getSector(pp.cursectnum);
                if (sec != null && TEST(sec.getExtra(), SECTFX_OPERATIONAL)) {
                    FLAG_KEY_RELEASE(pp, SK_OPERATE);
                    DoPlayerBeginOperate(pp);
                    pp.bob_amt = 0;
                    pp.bob_ndx = 0;
                    return;
                }
            }
        } else {
            FLAG_KEY_RESET(pp, SK_OPERATE);
        }

        // Crawl if in small area automatically
        if (DoPlayerTestCrawl(pp) && pp.WadeDepth <= PLAYER_CRAWL_WADE_DEPTH) {
            DoPlayerBeginCrawl(pp);
            return;
        }

        // Crawl Commanded
        if (TEST_SYNC_KEY(pp, SK_CRAWL) && pp.WadeDepth <= PLAYER_CRAWL_WADE_DEPTH) {
            DoPlayerBeginCrawl(pp);
            return;
        }

        if (TEST_SYNC_KEY(pp, SK_JUMP)) {
            if (FLAG_KEY_PRESSED(pp, SK_JUMP)) {
                FLAG_KEY_RELEASE(pp, SK_JUMP);
                DoPlayerBeginJump(pp);
                pp.bob_amt = 0;
                pp.bob_ndx = 0;
                return;
            }
        } else {
            FLAG_KEY_RESET(pp, SK_JUMP);
        }

        if (PlayerFlyKey(pp)) {
            DoPlayerBeginFly(pp);
            pp.bob_amt = 0;
            pp.bob_ndx = 0;
            return;
        }

        // If moving forward and tag is a ladder start climbing
        if (PlayerOnLadder(pp)) {
            DoPlayerBeginClimb(pp);
            return;
        }

        Sector loSec = boardService.getSector(pp.lo_sectp);
        if (loSec != null && TEST(loSec.getExtra(), SECTFX_CURRENT)) {
            DoPlayerCurrent(pp);
        }

        // Move about
        DoPlayerMove(pp);

        if (u != null) {
            if (TEST(pp.Flags, PF_PLAYER_MOVED)) {
                if (u.getRot() != u.ActorActionSet.Run) {
                    NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Run);
                }
            } else {
                if (u.getRot() != u.ActorActionSet.Stand) {
                    NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Stand);
                }
            }
        }

        // If the floor is far below you, fall hard instead of adjusting height
        if (klabs(pp.posz - pp.loz) > PLAYER_HEIGHT + PLAYER_FALL_HEIGHT) {
            pp.jump_speed = Z(1);
            DoPlayerBeginFall(pp);
            // call PlayerFall now seems to iron out a hitch before falling
            DoPlayerFall(pp);
            return;
        }

        if (PlayerCanDive(pp)) {
            pp.bob_amt = 0;
            pp.bob_ndx = 0;
            return;
        }

        // If the floor is far below you, fall hard instead of adjusting height
        if (klabs(pp.posz - pp.loz) > PLAYER_HEIGHT + PLAYER_FALL_HEIGHT) {
            pp.jump_speed = Z(1);
            DoPlayerBeginFall(pp);
            // call PlayerFall now seems to iron out a hitch before falling
            DoPlayerFall(pp);
            pp.bob_amt = 0;
            pp.bob_ndx = 0;
            return;
        }

        DoPlayerBob(pp);

        // Adjust height moving up and down sectors
        DoPlayerHeight(pp);

        if (pp.WadeDepth == 0) {
            DoPlayerBeginRun(pp);
        }
    }

    public static void DoPlayerBeginOperateBoat(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        pp.floor_dist = PLAYER_RUN_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_RUN_CEILING_DIST;
        pp.DoPlayerAction = DoPlayerOperateBoat;

        // temporary set to get weapons down
        if (TEST(SectorObject[pp.sop].flags, SOBJ_HAS_WEAPON)) {
            pp.Flags |= (PF_WEAPON_DOWN);
        }

        NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Run);
    }

    public static void DoPlayerBeginOperateTank(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        pp.floor_dist = PLAYER_RUN_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_RUN_CEILING_DIST;
        pp.DoPlayerAction = DoPlayerOperateTank;

        // temporary set to get weapons down
        if (TEST(SectorObject[pp.sop].flags, SOBJ_HAS_WEAPON)) {
            pp.Flags |= (PF_WEAPON_DOWN);
        }

        NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Stand);
    }

    public static void DoPlayerBeginOperateTurret(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        pp.floor_dist = PLAYER_RUN_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_RUN_CEILING_DIST;
        pp.DoPlayerAction = DoPlayerOperateTurret;

        // temporary set to get weapons down
        if (TEST(SectorObject[pp.sop].flags, SOBJ_HAS_WEAPON)) {
            pp.Flags |= (PF_WEAPON_DOWN);
        }
        NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Stand);
    }

    public static void FindMainSector(int sopi) {
        Sector_Object sop = SectorObject[sopi];

        // find the main sector - only do this once for each sector object
        if (sop.op_main_sector < 0) {
            int sx = sop.xmid;
            int sy = sop.ymid;

            PlaceSectorObject(sopi, sop.ang, MAXSO, MAXSO);

            // set it to something valid
            sop.op_main_sector = 0;

            sop.op_main_sector = engine.updatesectorz(sx, sy, sop.zmid, sop.op_main_sector);

            PlaceSectorObject(sopi, sop.ang, sx, sy);
        }
    }

    public static void DoPlayerOperateMatch(PlayerStr pp, boolean starting) {
        if (pp.sop == -1) {
            return;
        }

        Sector_Object sop = SectorObject[pp.sop];
        if (sop == null) {
            return;
        }

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getSectNode(sop.mid_sector); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            if (sp.getStatnum() == STAT_ST1 && sp.getHitag() == SO_DRIVABLE_ATTRIB) {
                if (starting) {
                    if (SP_TAG5(sp) != 0) {
                        DoMatchEverything(pp, SP_TAG5(sp), -1);
                    }
                } else {
                    if (TEST_BOOL2(sp) && SP_TAG5(sp) != 0) {
                        DoMatchEverything(pp, SP_TAG5(sp) + 1, -1);
                    }
                }
                break;
            }
        }
    }

    public static void DoPlayerBeginOperate(PlayerStr pp) {
        int i;

        int sopi = PlayerOnObject(pp.cursectnum);
        if (sopi == -1) {
            DoPlayerBeginRun(pp);
            return;
        }

        Sector_Object sop = SectorObject[sopi];

        // if someone already controlling it
        if (sop.controller != -1) {
            return;
        }

        if (TEST(sop.flags, SOBJ_REMOTE_ONLY)) {
            return;
        }

        // won't operate - broken
        if (sop.max_damage != -9999 && sop.max_damage <= 0) {
            if (pp.InventoryAmount[INVENTORY_REPAIR_KIT] != 0) {
                UseInventoryRepairKit(pp);
                USER u = getUser(sop.sp_child);
                if (u != null) {
                    sop.max_damage = (short) u.MaxHealth;
                    VehicleSetSmoke(sop, null);
                    sop.flags &= ~(SOBJ_BROKEN);
                }
            } else {
                PlayerSound(DIGI_USEBROKENVEHICLE, v3df_follow | v3df_dontpan, pp);
                return;
            }
        }
        pp.sop = pp.sop_control = sopi;
        sop.controller = pp.PlayerSprite;

        pp.pang = sop.ang;
        pp.posx = sop.xmid;
        pp.posy = sop.ymid;
        pp.cursectnum = COVERupdatesector(pp.posx, pp.posy, pp.cursectnum);
        engine.getzsofslope(pp.cursectnum, pp.posx, pp.posy, fz, cz);
        pp.posz = fz.get() - PLAYER_HEIGHT;

        pp.Flags &= ~(PF_CRAWLING | PF_JUMPING | PF_FALLING | PF_LOCK_CRAWL);

        DoPlayerOperateMatch(pp, true);

        // look for gun before trying to using it
        for (i = 0; sop.sp_num[i] != -1; i++) {
            Sprite s = boardService.getSprite(sop.sp_num[i]);
            if (s != null && s.getStatnum() == STAT_SO_SHOOT_POINT) {
                sop.flags |= (SOBJ_HAS_WEAPON);
                break;
            }
        }

        DoPlayerResetMovement(pp);

        switch (sop.track) {
            case SO_TANK:
                if ((pp.input.vel | pp.input.svel) != 0) {
                    PlaySOsound(sop.mid_sector, SO_DRIVE_SOUND);
                } else {
                    PlaySOsound(sop.mid_sector, SO_IDLE_SOUND);
                }
                pp.posz = fz.get() - PLAYER_HEIGHT;
                DoPlayerBeginOperateTank(pp);
                break;
            case SO_TURRET_MGUN:
            case SO_TURRET:
                if (pp.input.angvel != 0) {
                    PlaySOsound(sop.mid_sector, SO_DRIVE_SOUND);
                } else {
                    PlaySOsound(sop.mid_sector, SO_IDLE_SOUND);
                }
                pp.posz = fz.get() - PLAYER_HEIGHT;
                DoPlayerBeginOperateTurret(pp);
                break;
            case SO_SPEED_BOAT:
                if ((pp.input.vel | pp.input.svel) != 0) {
                    PlaySOsound(sop.mid_sector, SO_DRIVE_SOUND);
                } else {
                    PlaySOsound(sop.mid_sector, SO_IDLE_SOUND);
                }
                pp.posz = fz.get() - PLAYER_HEIGHT;
                DoPlayerBeginOperateBoat(pp);
                break;
            default:
        }

    }

    public static void DoPlayerBeginRemoteOperate(PlayerStr pp, int sopi) {
        int i;
        int save_sectnum;

        Sector_Object sop = SectorObject[sopi];

        pp.sop = pp.sop_remote = pp.sop_control = sopi;
        sop.controller = pp.PlayerSprite;

        // won't operate - broken
        if (sop.max_damage != -9999 && sop.max_damage <= 0) {
            if (pp.InventoryAmount[INVENTORY_REPAIR_KIT] != 0) {
                USER u = getUser(sop.sp_child);
                if (u != null) {
                    UseInventoryRepairKit(pp);
                    sop.max_damage = (short) u.MaxHealth;
                    VehicleSetSmoke(sop, null);
                    sop.flags &= ~(SOBJ_BROKEN);
                }
            } else {
                PlayerSound(DIGI_USEBROKENVEHICLE, v3df_follow | v3df_dontpan, pp);
                return;
            }
        }

        save_sectnum = pp.cursectnum;

        pp.pang = sop.ang;
        pp.posx = sop.xmid;
        pp.posy = sop.ymid;
        pp.cursectnum = COVERupdatesector(pp.posx, pp.posy, pp.cursectnum);
        engine.getzsofslope(pp.cursectnum, pp.posx, pp.posy, fz, cz);
        pp.posz = fz.get() - PLAYER_HEIGHT;

        pp.Flags &= ~(PF_CRAWLING | PF_JUMPING | PF_FALLING | PF_LOCK_CRAWL);

        DoPlayerOperateMatch(pp, true);

        // look for gun before trying to using it
        for (i = 0; sop.sp_num[i] != -1; i++) {
            Sprite s = boardService.getSprite(sop.sp_num[i]);
            if (s != null && s.getStatnum() == STAT_SO_SHOOT_POINT) {
                sop.flags |= (SOBJ_HAS_WEAPON);
                break;
            }
        }

        DoPlayerResetMovement(pp);

        PlayerToRemote(pp);
        PlayerRemoteInit(pp);

        switch (sop.track) {
            case SO_TANK:
                if ((pp.input.vel | pp.input.svel) != 0) {
                    PlaySOsound(sop.mid_sector, SO_DRIVE_SOUND);
                } else {
                    PlaySOsound(sop.mid_sector, SO_IDLE_SOUND);
                }
                pp.posz = fz.get() - PLAYER_HEIGHT;
                DoPlayerBeginOperateTank(pp);
                break;
            case SO_TURRET_MGUN:
            case SO_TURRET:
                if (pp.input.angvel != 0) {
                    PlaySOsound(sop.mid_sector, SO_DRIVE_SOUND);
                } else {
                    PlaySOsound(sop.mid_sector, SO_IDLE_SOUND);
                }
                pp.posz = fz.get() - PLAYER_HEIGHT;
                DoPlayerBeginOperateTurret(pp);
                break;
            case SO_SPEED_BOAT:
                if ((pp.input.vel | pp.input.svel) != 0) {
                    PlaySOsound(sop.mid_sector, SO_DRIVE_SOUND);
                } else {
                    PlaySOsound(sop.mid_sector, SO_IDLE_SOUND);
                }
                pp.posz = fz.get() - PLAYER_HEIGHT;
                DoPlayerBeginOperateBoat(pp);
                break;
            default:
                return;
        }

        PlayerRemoteReset(pp, save_sectnum);
    }

    public static void PlayerRemoteReset(PlayerStr pp, int sectnum) {
        Sector sec = boardService.getSector(sectnum);
        Sprite removeSpr = boardService.getSprite(pp.remote_sprite);
        if (removeSpr == null || sec == null) {
            return;
        }

        pp.cursectnum = pp.lastcursectnum = sectnum;

        pp.posx = removeSpr.getX();
        pp.posy = removeSpr.getY();
        pp.posz = sec.getFloorz() - PLAYER_HEIGHT;

        pp.xvect = pp.yvect = pp.oxvect = pp.oyvect = pp.slide_xvect = pp.slide_yvect = 0;

        UpdatePlayerSprite(pp);
    }

    public static void PlayerRemoteInit(PlayerStr pp) {
        pp.remote.xvect = 0;
        pp.remote.yvect = 0;
        pp.remote.oxvect = 0;
        pp.remote.oyvect = 0;
        pp.remote.slide_xvect = 0;
        pp.remote.slide_yvect = 0;
    }

    public static void RemoteToPlayer(PlayerStr pp) {
        pp.cursectnum = pp.remote.cursectnum;
        pp.lastcursectnum = pp.remote.lastcursectnum;

        pp.posx = pp.remote.posx;
        pp.posy = pp.remote.posy;
        pp.posz = pp.remote.posz;

        pp.xvect = pp.remote.xvect;
        pp.yvect = pp.remote.yvect;
        pp.oxvect = pp.remote.oxvect;
        pp.oyvect = pp.remote.oyvect;
        pp.slide_xvect = pp.remote.slide_xvect;
        pp.slide_yvect = pp.remote.slide_yvect;
    }

    public static void PlayerToRemote(PlayerStr pp) {
        if (pp.remote == null) {
            pp.remote = new Remote_Control();
        }
        pp.remote.cursectnum = pp.cursectnum;
        pp.remote.lastcursectnum = pp.lastcursectnum;

        pp.remote.oposx = pp.remote.posx;
        pp.remote.oposy = pp.remote.posy;
        pp.remote.oposz = pp.remote.posz;

        pp.remote.posx = pp.posx;
        pp.remote.posy = pp.posy;
        pp.remote.posz = pp.posz;

        pp.remote.xvect = pp.xvect;
        pp.remote.yvect = pp.yvect;
        pp.remote.oxvect = pp.oxvect;
        pp.remote.oyvect = pp.oyvect;
        pp.remote.slide_xvect = pp.slide_xvect;
        pp.remote.slide_yvect = pp.slide_yvect;
    }

    public static void DoPlayerStopOperate(PlayerStr pp) {
        pp.Flags &= ~(PF_WEAPON_DOWN);
        DoPlayerResetMovement(pp);
        DoTankTreads(pp);
        DoPlayerOperateMatch(pp, false);
        StopSOsound(SectorObject[pp.sop].mid_sector);

        Sprite remoteSpr = boardService.getSprite(pp.remote_sprite);
        if (remoteSpr != null) {
            if (TEST_BOOL1(remoteSpr)) {
                pp.pang = pp.oang = remoteSpr.getAng();
            } else {
                pp.pang = pp.oang = EngineUtils.getAngle(SectorObject[pp.sop_remote].xmid - pp.posx,
                        SectorObject[pp.sop_remote].ymid - pp.posy);
            }
        }

        if (pp.sop_control != -1) {
            SectorObject[pp.sop_control].controller = -1;
        }
        pp.remote_sprite = -1; // #GDX 31.12.2024 Leave vehicle fix (Control remote vh on map1 then control turret on map2 and left it)
        pp.sop_control = -1;
        pp.sop_riding = -1;
        pp.sop_remote = -1;
        pp.sop = -1;
        DoPlayerBeginRun(pp);
    }

    public static void DoPlayerOperateTurret(PlayerStr pp) {
        int save_sectnum;

        if (TEST_SYNC_KEY(pp, SK_OPERATE)) {
            if (FLAG_KEY_PRESSED(pp, SK_OPERATE)) {
                FLAG_KEY_RELEASE(pp, SK_OPERATE);
                DoPlayerStopOperate(pp);
                return;
            }
        } else {
            FLAG_KEY_RESET(pp, SK_OPERATE);
        }

        Sector_Object sop = SectorObject[pp.sop];
        if (sop.max_damage != -9999 && sop.max_damage <= 0) {
            DoPlayerStopOperate(pp);
            return;
        }

        save_sectnum = pp.cursectnum;

        if (pp.sop_remote != -1) {
            RemoteToPlayer(pp);
        }

        DoPlayerMoveTurret(pp);

        if (pp.sop_remote != -1) {
            PlayerToRemote(pp);
            PlayerRemoteReset(pp, save_sectnum);
        }
    }

    public static void DoPlayerOperateBoat(PlayerStr pp) {
        int save_sectnum;

        if (TEST_SYNC_KEY(pp, SK_OPERATE)) {
            if (FLAG_KEY_PRESSED(pp, SK_OPERATE)) {
                FLAG_KEY_RELEASE(pp, SK_OPERATE);
                DoPlayerStopOperate(pp);
                return;
            }
        } else {
            FLAG_KEY_RESET(pp, SK_OPERATE);
        }

        Sector_Object sop = SectorObject[pp.sop];
        if (sop.max_damage != -9999 && sop.max_damage <= 0) {
            DoPlayerStopOperate(pp);
            return;
        }

        save_sectnum = pp.cursectnum;

        if (pp.sop_remote != -1) {
            RemoteToPlayer(pp);
        }

        DoPlayerMoveBoat(pp);

        if (pp.sop_remote != -1) {
            PlayerToRemote(pp);
            PlayerRemoteReset(pp, save_sectnum);
        }
    }

    public static void DoPlayerOperateTank(PlayerStr pp) {
        int save_sectnum;

        if (TEST_SYNC_KEY(pp, SK_OPERATE)) {
            if (FLAG_KEY_PRESSED(pp, SK_OPERATE)) {
                FLAG_KEY_RELEASE(pp, SK_OPERATE);
                DoPlayerStopOperate(pp);
                return;
            }
        } else {
            FLAG_KEY_RESET(pp, SK_OPERATE);
        }

        Sector_Object sop = SectorObject[pp.sop];
        if (sop.max_damage != -9999 && sop.max_damage <= 0) {
            DoPlayerStopOperate(pp);
            return;
        }

        save_sectnum = pp.cursectnum;

        if (pp.sop_remote != -1) {
            RemoteToPlayer(pp);
        }

        DoPlayerMoveTank(pp);

        if (pp.sop_remote != -1) {
            PlayerToRemote(pp);
            PlayerRemoteReset(pp, save_sectnum);
        }
    }

    public static void DoPlayerDeathJump(PlayerStr pp) {
        int i;

        // instead of multiplying by synctics, use a loop for greater accuracy
        for (i = 0; i < synctics; i++) {
            // adjust jump speed by gravity - if jump speed greater than 0 player
            // have started falling
            if ((pp.jump_speed += PLAYER_DEATH_GRAV) > 0) {
                pp.Flags &= ~(PF_JUMPING);
                pp.Flags |= (PF_FALLING);
                DoPlayerDeathFall(pp);
                return;
            }

            // adjust height by jump speed
            pp.posz += pp.jump_speed;

            // if player gets to close the ceiling while jumping
            // if (pp.posz < pp.hiz + Z(4))
            if (PlayerCeilingHit(pp, pp.hiz + Z(4))) {
                // put player at the ceiling
                pp.posz = pp.hiz + Z(4);

                // reverse your speed to falling
                pp.jump_speed = -pp.jump_speed;

                // start falling
                pp.Flags &= ~(PF_JUMPING);
                pp.Flags |= (PF_FALLING);
                DoPlayerDeathFall(pp);
                return;
            }
        }
    }

    public static void DoPlayerDeathFall(PlayerStr pp) {
        int i;
        int loz;

        for (i = 0; i < synctics; i++) {
            // adjust jump speed by gravity
            pp.jump_speed += PLAYER_DEATH_GRAV;

            // adjust player height by jump speed
            pp.posz += pp.jump_speed;

            Sector loSec = boardService.getSector(pp.lo_sectp);
            if (loSec != null && TEST(loSec.getExtra(), SECTFX_SINK)) {
                loz = loSec.getFloorz();
            } else {
                loz = pp.loz;
            }

            if (PlayerFloorHit(pp, loz - PLAYER_DEATH_HEIGHT)) {
                if (loz != pp.loz) {
                    SpawnSplash(pp.PlayerSprite);
                }

                if (RANDOM_RANGE(1000) > 500) {
                    PlaySound(DIGI_BODYFALL1, pp, v3df_dontpan);
                } else {
                    PlaySound(DIGI_BODYFALL2, pp, v3df_dontpan);
                }

                pp.posz = loz - PLAYER_DEATH_HEIGHT;
                pp.Flags &= ~(PF_FALLING);
            }
        }
    }

    public static String KilledPlayerMessage(PlayerStr pp, PlayerStr killer) {

        int rnd = STD_RANDOM_RANGE(MAX_KILL_NOTES);
        String p1 = pp.getName();
        String p2 = killer.getName();

        if (pp.HitBy == killer.PlayerSprite) {
            return p1 + " was killed by " + p2 + ".";
        } else {
            switch (rnd) {
                case 0:
                    return p1 + " was wasted by " + p2 + "'s " + DeathString(pp.HitBy) + ".";
                case 1:
                    return p1 + " got his ass kicked by " + p2 + "'s " + DeathString(pp.HitBy) + ".";
                case 2:
                    return p1 + " bows down before the mighty power of " + p2 + ".";
                case 3:
                    return p1 + " was killed by " + p2 + "'s " + DeathString(pp.HitBy) + ".";
                case 4:
                    return p1 + " got slapped down hard by " + p2 + "'s " + DeathString(pp.HitBy) + ".";
                case 5:
                    return p1 + " got on his knees before " + p2 + ".";
                case 6:
                    return p1 + " was totally out classed by " + p2 + "'s " + DeathString(pp.HitBy) + ".";
                case 7:
                    return p1 + " got chewed apart by " + p2 + "'s " + DeathString(pp.HitBy) + ".";
                case 8:
                    return p1 + " was retired by " + p2 + "'s " + DeathString(pp.HitBy) + ".";
                case 9:
                    return p1 + " was greased by " + p2 + "'s " + DeathString(pp.HitBy) + ".";
                case 10:
                    return p1 + " was humbled lower than dirt by " + p2 + ".";
                case 11:
                    return p2 + " beats " + p1 + " like a red headed step child.";
                case 12:
                    return p1 + " begs for mercy as " + p2 + " terminates him with extreme prejudice.";
                case 13:
                    return p1 + " falls before the superior skills of " + p2 + ".";
                case 14:
                    return p2 + " gives " + p1 + " a beating he'll never forget.";
                case 15:
                    return p2 + " puts the Smack Dab on " + p1 + " with his " + DeathString(pp.HitBy);
            }
        }
        return (null);
    }

    public static void DoPlayerDeathMessage(int pp, int nkiller) {
        boolean SEND_OK = false;
        String ds = null;

        PlayerStr killer = Player[nkiller];

        killer.KilledPlayer[pp]++;

        if (Player[pp] == killer && pp == myconnectindex) {
            ds = Player[pp].getName() + " " + SuicideNote[STD_RANDOM_RANGE(MAX_SUICIDE)];
            SEND_OK = true;
        } else
            // I am being killed
            if (killer == Player[myconnectindex]) {
                ds = KilledPlayerMessage(Player[pp], killer);
                SEND_OK = true;
            }

        if (SEND_OK) {
            gNet.SendMessage(-1, ds);
            adduserquote(ds);
        }
    }

    public static void DoPlayerBeginDie(int ppnum) {
        int bak;
        int choosesnd;
        PlayerStr pp = Player[ppnum];
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        if (Prediction) {
            return;
        }

        if (GodMode) {
            return;
        }

        // Override any previous talking, death scream has precedance
        if (pp.PlayerTalking) {
            if (pp.TalkVocHandle.isActive()) {
                pp.TalkVocHandle.stop();
            }
            pp.PlayerTalking = false;
            pp.TalkVocnum = -1;
            pp.TalkVocHandle = null;
        }

        // Do the death scream
        choosesnd = RANDOM_RANGE(MAX_PAIN);

        PlayerSound(PlayerLowHealthPainVocs[choosesnd], v3df_dontpan | v3df_doppler | v3df_follow, pp);
        if (game.nNetMode == NetMode.Single && numplayers <= 1 && lastload != null && lastload.exists()) {
            game.menu.mOpen(game.menu.mMenus[LASTSAVE], -1);
        } else {
            bak = GlobInfoStringTime;
            GlobInfoStringTime = 999;
            PutStringInfo(pp, "Press \"USE\" or SPACE to restart");
            GlobInfoStringTime = bak;
        }

        if (pp.sop_control != -1) {
            DoPlayerStopOperate(pp);
        }

        // if diving force death to drown type
        if (TEST(pp.Flags, PF_DIVING)) {
            pp.DeathType = PLAYER_DEATH_DROWN;
        }

        pp.Flags &= ~(PF_JUMPING | PF_FALLING | PF_DIVING | PF_FLYING | PF_CLIMBING | PF_CRAWLING | PF_LOCK_CRAWL);

        pp.tilt_dest = 0;

        ActorCoughItem(pp.PlayerSprite);

        if (numplayers > 1 || gNet.FakeMultiplayer) {
            // Give kill credit to player if necessary
            if (pp.Killer >= 0) {
                USER ku = getUser(pp.Killer);

                if (ku != null && ku.PlayerP != -1) {
                    if (pp.pnum == ku.PlayerP) {
                        // Killed yourself
                        PlayerUpdateKills(ppnum, -1);
                        DoPlayerDeathMessage(ppnum, ppnum);
                    } else {
                        // someone else killed you
                        if (gNet.TeamPlay) {
                            // playing team play
                            if (u.spal == ku.spal) {
                                // Killed your team member
                                PlayerUpdateKills(ppnum, -1);
                                DoPlayerDeathMessage(ppnum, ku.PlayerP);
                            } else {
                                // killed another team member
                                PlayerUpdateKills(ku.PlayerP, 1);
                                DoPlayerDeathMessage(ppnum, ku.PlayerP);
                            }
                        } else {
                            // not playing team play
                            PlayerUpdateKills(ku.PlayerP, 1);
                            DoPlayerDeathMessage(ppnum, ku.PlayerP);
                        }
                    }
                }
            } else {
                // Killed by some hazard - negative frag
                PlayerUpdateKills(ppnum, -1);
                DoPlayerDeathMessage(ppnum, ppnum);
            }
        }

        // Get rid of all panel spells that are currently working
        KillInventoryBar(pp);

        pp.Flags |= (PF_LOCK_HORIZ);

        pp.friction = PLAYER_RUN_FRICTION;
        pp.slide_xvect = pp.slide_yvect = 0;
        pp.floor_dist = PLAYER_WADE_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_WADE_CEILING_DIST;
        pp.DoPlayerAction = PlayerDeathFunc[pp.DeathType];
        pp.sop_control = -1;
        pp.sop_remote = -1;
        pp.sop_riding = -1;
        pp.sop = -1;
        pp.Flags &= ~(PF_TWO_UZI);

        NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Run);
        pWeaponForceRest(pp);
        if (pp.Chops != null) {
            ChopsSetRetract(pp);
        }

        Sprite psp = pp.getSprite();
        if (psp == null) {
            return;
        }

        switch (pp.DeathType) {
            case PLAYER_DEATH_DROWN: {
                pp.Flags |= (PF_JUMPING);
                u.ID = NINJA_DEAD;
                pp.jump_speed = -200;
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerDeath);
                DoFindGround(pp.PlayerSprite);
                DoBeginJump(pp.PlayerSprite);
                u.jump_speed = -300;
                break;
            }
            case PLAYER_DEATH_FLIP:
            case PLAYER_DEATH_RIPPER:
                pp.Flags |= (PF_JUMPING);
                u.ID = NINJA_DEAD;
                pp.jump_speed = -300;
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerDeath);
                psp.setCstat(psp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                u.ceiling_dist =  Z(10);
                u.floor_dist =  Z(0);
                DoFindGround(pp.PlayerSprite);
                DoBeginJump(pp.PlayerSprite);
                u.jump_speed = -400;
                break;
            case PLAYER_DEATH_CRUMBLE:

                PlaySound(DIGI_BODYSQUISH1, pp, v3df_dontpan);

                pp.Flags |= (PF_DEAD_HEAD | PF_JUMPING);
                pp.jump_speed = -300;
                u.slide_vel = 0;
                SpawnShrap(pp.PlayerSprite, -1);
                psp.setCstat(psp.getCstat() | (CSTAT_SPRITE_YCENTER));
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerHeadFly);
                u.ID = NINJA_Head_R0;
                psp.setXrepeat(48);
                psp.setYrepeat(48);
                // Blood fountains
                InitBloodSpray(pp.PlayerSprite, true, 105);
                break;
            case PLAYER_DEATH_EXPLODE:

                PlaySound(DIGI_BODYSQUISH1, pp, v3df_dontpan);

                pp.Flags |= (PF_DEAD_HEAD | PF_JUMPING);
                pp.jump_speed = -650;
                SpawnShrap(pp.PlayerSprite, -1);
                psp.setCstat(psp.getCstat() | (CSTAT_SPRITE_YCENTER));
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerHeadFly);
                u.ID = NINJA_Head_R0;
                psp.setXrepeat(48);
                psp.setYrepeat(48);
                // Blood fountains
                InitBloodSpray(pp.PlayerSprite, true, -1);
                InitBloodSpray(pp.PlayerSprite, true, -1);
                InitBloodSpray(pp.PlayerSprite, true, -1);
                break;
            case PLAYER_DEATH_SQUISH:

                PlaySound(DIGI_BODYCRUSHED1, pp, v3df_dontpan);

                pp.Flags |= (PF_DEAD_HEAD | PF_JUMPING);
                pp.jump_speed = 200;
                u.slide_vel = 800;
                SpawnShrap(pp.PlayerSprite, -1);
                psp.setCstat(psp.getCstat() | (CSTAT_SPRITE_YCENTER));
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerHeadFly);
                u.ID = NINJA_Head_R0;
                psp.setXrepeat(48);
                psp.setYrepeat(48);
                // Blood fountains
                InitBloodSpray(pp.PlayerSprite, true, 105);
                break;
            default:
                break;
        }

        pp.Flags |= (PF_DEAD);
        u.Flags &= ~(SPR_BOUNCE);
        pp.Flags &= ~(PF_HEAD_CONTROL);
    }

    public static void DoPlayerDeathHoriz(PlayerStr pp, int target, int speed) {
        if (pp.horiz > target) {
            pp.horiz -= speed;
            if (pp.horiz <= target) {
                pp.horiz = target;
            }
        }

        if (pp.horiz < target) {
            pp.horiz += speed;
            if (pp.horiz >= target) {
                pp.horiz = target;
            }
        }
    }

    public static void DoPlayerDeathZrange(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        // make sure we don't land on a regular sprite
        DoFindGround(pp.PlayerSprite);

        // update player values with results from DoFindGround
        pp.loz = u.loz;
        pp.lo_sp = u.lo_sp;
        pp.lo_sectp = u.lo_sectp;
    }

    public static void DoPlayerDeathHurl(PlayerStr pp) {
        if (numplayers > 1 || gNet.FakeMultiplayer) {
            if (TEST_SYNC_KEY(pp, SK_SHOOT)) {
                if (FLAG_KEY_PRESSED(pp, SK_SHOOT)) {

                    pp.Flags |= (PF_HEAD_CONTROL);
                    NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerHeadHurl);
                    if (MoveSkip4 == 0) {
                        SpawnShrap(pp.PlayerSprite, -1);
                        if (RANDOM_RANGE(1000) > 400) {
                            PlayerSound(DIGI_DHVOMIT, v3df_dontpan | v3df_follow, pp);
                        }
                    }
                    return;
                }
            }
        }

        if (!TEST(pp.Flags, PF_JUMPING | PF_FALLING)) {
            NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerHead);
        }
    }

    public static void DoPlayerDeathFollowKiller(PlayerStr pp) {
        // if it didn't make it to this angle because of a low ceiling or something
        // continue on to it
        DoPlayerDeathHoriz(pp, PLAYER_DEATH_HORIZ_UP_VALUE, 4);

        // allow turning
        if ((TEST(pp.Flags, PF_DEAD_HEAD) && pp.input.angvel != 0) || TEST(pp.Flags, PF_HEAD_CONTROL)) {
            // Allow them to turn fast
            PLAYER_RUN_LOCK(pp);

            DoPlayerTurn(pp);
            return;
        }

        // follow what killed you if its available
        Sprite kp = boardService.getSprite(pp.Killer);
        if (kp != null) {
            int ang2, delta_ang;

            if (FAFcansee(kp.getX(), kp.getY(), SPRITEp_TOS(kp), kp.getSectnum(), pp.posx, pp.posy, pp.posz, pp.cursectnum)) {
                ang2 = EngineUtils.getAngle(kp.getX() - pp.posx, kp.getY() - pp.posy);

                delta_ang = GetDeltaAngle(ang2, pp.getAnglei());

                pp.pang += (delta_ang >> 4);
                pp.pang = BClampAngle(pp.pang);
            }
        }
    }

    public static void DoPlayerDeathRestart(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (sp == null || u == null) {
            return;
        }

        // Spawn a dead LoWang body for non-head deaths
        // Hey Frank, if you think of a better check, go ahead and put it in.
        if (PlayerFloorHit(pp, pp.loz - PLAYER_HEIGHT)) {
            if (pp.DeathType == PLAYER_DEATH_FLIP || pp.DeathType == PLAYER_DEATH_RIPPER) {
                QueueLoWangs(pp.PlayerSprite);
            }
        } else { // If he's not on the floor, then gib like a mo-fo!
            InitBloodSpray(pp.PlayerSprite, true, -1);
            InitBloodSpray(pp.PlayerSprite, true, -1);
            InitBloodSpray(pp.PlayerSprite, true, -1);
        }

        pClearTextLine(pp, TEXT_INFO_LINE(0));

        PlayerSpawnPosition(pp.pnum);

        NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Stand);
        sp.setPicnum(u.State.Pic);
        sp.setXrepeat(PLAYER_NINJA_XREPEAT);
        sp.setYrepeat(PLAYER_NINJA_XREPEAT);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
        sp.setX(pp.posx);
        sp.setY(pp.posy);
        sp.setZ(pp.posz + PLAYER_HEIGHT);
        sp.setAng(pp.getAnglei());

        DoSpawnTeleporterEffect(sp);
        PlaySound(DIGI_TELEPORT, pp, v3df_none);

        DoPlayerZrange(pp);

        pp.sop_control = -1;
        pp.sop_remote = -1;
        pp.sop_riding = -1;
        pp.sop = -1;

        pp.Flags &= ~(PF_WEAPON_DOWN | PF_WEAPON_RETRACT);
        pp.Flags &= ~(PF_DEAD);
        pp.Flags &= ~(PF_LOCK_HORIZ);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        sp.setXrepeat(PLAYER_NINJA_XREPEAT);
        sp.setYrepeat(PLAYER_NINJA_YREPEAT);

        pp.horiz = pp.horizbase = 100;
        DoPlayerResetMovement(pp);
        u.ID = NINJA_RUN_R0;

        PlayerDeathReset(pp);
        if (pp == Player[screenpeek]) {
            ResetPalette(pp, FORCERESET);  // screenpeek
        }

        pp.NightVision = false;
        pp.FadeAmt = 0;
        DoPlayerDivePalette(pp);
        DoPlayerNightVisionPalette(pp);

        if (numplayers > 1 || gNet.FakeMultiplayer) {
            // need to call this routine BEFORE resetting DEATH flag
            DoPlayerBeginRun(pp);
        } else {
            // restart the level in single play
            if (gDemoScreen.demfile != null) {
                if (rec != null) {
                    rec.close();
                }
            } else {
                ExitLevel = true;
            }
        }

        DoPlayerFireOutDeath(pp);
    }

    private static void DoPlayerDeathCheckKeys(PlayerStr pp) {
        if (TEST_SYNC_KEY(pp, SK_SPACE_BAR)) {
            DoPlayerDeathRestart(pp);
        }
    }

    public static void DoPlayerHeadDebris(PlayerStr pp) {
        Sector sectp = boardService.getSector(pp.cursectnum);
        if (sectp == null) {
            return;
        }

        if (TEST(sectp.getExtra(), SECTFX_SINK)) {
            DoPlayerSpriteBob(pp, Z(8), Z(4), 3);
        } else {
            pp.bob_amt = 0;
        }
    }

    public static void DoPlayerDeathCheckKick(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (sp == null || u == null) {
            return;
        }

        for (int j : StatDamageList) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite hp = node.get();
                USER hu = getUser(i);
                if (hu == null) {
                    continue;
                }

                if (i == pp.PlayerSprite) {
                    break;
                }

                // don't set off mine
                if (!TEST(hp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    continue;
                }

                if (DISTANCE(hp.getX(), hp.getY(), sp.getX(), sp.getY()) < hu.Radius + 100) {
                    pp.Killer = i;

                    u.slide_ang = EngineUtils.getAngle(sp.getX() - hp.getX(), sp.getY() - hp.getY());
                    u.slide_ang = NORM_ANGLE(u.slide_ang + (RANDOM_P2(128 << 5) >> 5) - 64);

                    u.slide_vel = hp.getXvel() << 1;
                    u.Flags &= ~(SPR_BOUNCE);
                    pp.jump_speed = -500;
                    NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerHeadFly);
                    pp.Flags |= (PF_JUMPING);
                    SpawnShrap(pp.PlayerSprite, -1);
                    return;
                }
            }
        }

        DoPlayerZrange(pp);

        // sector stomper kick
        if (klabs(pp.loz - pp.hiz) < SPRITEp_SIZE_Z(sp) - Z(8)) {
            u.slide_ang =  RANDOM_P2(2048);

            u.slide_vel = 1000;
            u.Flags &= ~(SPR_BOUNCE);
            pp.jump_speed = -100;
            NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerHeadFly);
            pp.Flags |= (PF_JUMPING);
            SpawnShrap(pp.PlayerSprite, -1);
        }
    }

    public static void DoPlayerDeathMoveHead(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (sp == null || u == null) {
            return;
        }

        int dax = MOVEx(u.slide_vel, u.slide_ang);
        int day = MOVEy(u.slide_vel, u.slide_ang);

        if ((u.moveSpriteReturn = move_sprite(pp.PlayerSprite, dax, day, 0, Z(16), Z(16), 1, synctics)) != 0) {
            switch (DTEST(u.moveSpriteReturn, HIT_MASK)) {
                case HIT_SPRITE: {
                    int wall_ang, dang;
                    int hitsprite = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Sprite hsp = boardService.getSprite(hitsprite);


                    if (hsp == null || !TEST(hsp.getCstat(), CSTAT_SPRITE_WALL)) {
                        break;
                    }

                    wall_ang = NORM_ANGLE(hsp.getAng());
                    dang = GetDeltaAngle(u.slide_ang, wall_ang);
                    u.slide_ang = NORM_ANGLE(wall_ang + 1024 - dang);

                    SpawnShrap(pp.PlayerSprite, -1);
                    break;
                }
                case HIT_WALL: {
                    int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);
                    Wall w = boardService.getWall(hitwall);

                    if (w != null) {
                        int wall_ang = NORM_ANGLE(w.getWallAngle() - 512);
                        int dang = GetDeltaAngle(u.slide_ang, wall_ang);
                        u.slide_ang = NORM_ANGLE(wall_ang + 1024 - dang);

                        SpawnShrap(pp.PlayerSprite, -1);
                    }
                    break;
                }
            }
        }

        pp.posx = sp.getX();
        pp.posy = sp.getY();
        pp.cursectnum = sp.getSectnum();

        // try to stay in valid area - death sometimes throws you out of the map
        int sectnum = COVERupdatesector(pp.posx, pp.posy, pp.cursectnum);
        if (sectnum < 0) {
            pp.cursectnum = pp.lv_sectnum;
            engine.changespritesect(pp.PlayerSprite, pp.lv_sectnum);
            pp.posx = pp.lv_x;
            pp.posy = pp.lv_y;
            sp.setX(pp.lv_x);
            sp.setY(pp.lv_y);
        } else {
            pp.lv_sectnum = sectnum;
            pp.lv_x = pp.posx;
            pp.lv_y = pp.posy;
        }
    }

    public static void DoPlayerDeathFlip(PlayerStr pp) {
        if (Prediction) {
            return;
        }

        DoPlayerDeathZrange(pp);

        if (TEST(pp.Flags, PF_JUMPING | PF_FALLING)) {
            if (TEST(pp.Flags, PF_JUMPING)) {
                DoPlayerDeathJump(pp);
                DoPlayerDeathHoriz(pp, PLAYER_DEATH_HORIZ_UP_VALUE, 2);
                if (!MoveSkip2) {
                    DoJump(pp.PlayerSprite);
                }
            }

            if (TEST(pp.Flags, PF_FALLING)) {
                DoPlayerDeathFall(pp);
                DoPlayerDeathHoriz(pp, PLAYER_DEATH_HORIZ_UP_VALUE, 4);
                if (!MoveSkip2) {
                    DoFall(pp.PlayerSprite);
                }
            }
        } else {
            DoPlayerDeathFollowKiller(pp);
        }

        DoPlayerDeathCheckKeys(pp);
    }

    public static void DoPlayerDeathDrown(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        if (sp == null) {
            return;
        }

        if (Prediction) {
            return;
        }

        DoPlayerDeathZrange(pp);

        if (TEST(pp.Flags, PF_JUMPING | PF_FALLING)) {
            if (TEST(pp.Flags, PF_JUMPING)) {
                DoPlayerDeathJump(pp);
                DoPlayerDeathHoriz(pp, PLAYER_DEATH_HORIZ_UP_VALUE, 2);
                if (!MoveSkip2) {
                    DoJump(pp.PlayerSprite);
                }
            }

            if (TEST(pp.Flags, PF_FALLING)) {
                pp.posz += Z(2);
                if (!MoveSkip2) {
                    sp.setZ(sp.getZ() + Z(4));
                }

                // Stick like glue when you hit the ground
                if (pp.posz > pp.loz - PLAYER_DEATH_HEIGHT) {
                    pp.posz = pp.loz - PLAYER_DEATH_HEIGHT;
                    pp.Flags &= ~(PF_FALLING);
                }
            }
        }

        DoPlayerDeathFollowKiller(pp);
        DoPlayerDeathCheckKeys(pp);
    }

    public static void DoPlayerDeathBounce(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (sp == null || u == null) {
            return;
        }

        if (Prediction) {
            return;
        }

        Sector loSec = boardService.getSector(pp.lo_sectp);
        if (loSec != null && TEST(loSec.getExtra(), SECTFX_SINK)) {
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
            NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerHead);
            u.slide_vel = 0;
            u.Flags |= (SPR_BOUNCE);
            return;
        }

        u.Flags |= (SPR_BOUNCE);
        pp.jump_speed = -300;
        u.slide_vel >>= 2;
        u.slide_ang = NORM_ANGLE((RANDOM_P2(64 << 8) >> 8) - 32);
        pp.Flags |= (PF_JUMPING);
        SpawnShrap(pp.PlayerSprite, -1);
    }

    public static void DoPlayerDeathCrumble(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (sp == null || u == null) {
            return;
        }

        if (Prediction) {
            return;
        }

        DoPlayerDeathZrange(pp);

        if (TEST(pp.Flags, PF_JUMPING | PF_FALLING)) {
            if (TEST(pp.Flags, PF_JUMPING)) {
                DoPlayerDeathJump(pp);
                DoPlayerDeathHoriz(pp, PLAYER_DEATH_HORIZ_JUMP_VALUE, 4);
            }

            if (TEST(pp.Flags, PF_FALLING)) {
                DoPlayerDeathFall(pp);
                DoPlayerDeathHoriz(pp, PLAYER_DEATH_HORIZ_FALL_VALUE, 3);
            }

            if (!TEST(pp.Flags, PF_JUMPING | PF_FALLING)) {
                if (!TEST(u.Flags, SPR_BOUNCE)) {
                    DoPlayerDeathBounce(pp);
                    return;
                }

                sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerHead);
            } else {
                DoPlayerDeathMoveHead(pp);
            }
        } else {
            DoPlayerDeathCheckKick(pp);
            DoPlayerDeathHurl(pp);
            DoPlayerDeathFollowKiller(pp);
        }

        DoPlayerDeathCheckKeys(pp);
        sp.setZ(pp.posz + PLAYER_DEAD_HEAD_FLOORZ_OFFSET);
        DoPlayerHeadDebris(pp);
    }

    public static void DoPlayerDeathExplode(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (sp == null || u == null) {
            return;
        }

        if (Prediction) {
            return;
        }

        DoPlayerDeathZrange(pp);

        if (TEST(pp.Flags, PF_JUMPING | PF_FALLING)) {
            if (TEST(pp.Flags, PF_JUMPING)) {
                DoPlayerDeathJump(pp);
                DoPlayerDeathHoriz(pp, PLAYER_DEATH_HORIZ_JUMP_VALUE, 4);
            }

            if (TEST(pp.Flags, PF_FALLING)) {
                DoPlayerDeathFall(pp);
                DoPlayerDeathHoriz(pp, PLAYER_DEATH_HORIZ_JUMP_VALUE, 3);
            }

            if (!TEST(pp.Flags, PF_JUMPING | PF_FALLING)) {
                if (!TEST(u.Flags, SPR_BOUNCE)) {
                    DoPlayerDeathBounce(pp);
                    return;
                }

                sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
                NewStateGroup(pp.PlayerSprite, PlayerStateGroup.sg_PlayerHead);
            } else {
                DoPlayerDeathMoveHead(pp);
            }
        } else {
            // special line for amoeba

            DoPlayerDeathCheckKick(pp);
            DoPlayerDeathHurl(pp);
            DoPlayerDeathFollowKiller(pp);
        }

        DoPlayerDeathCheckKeys(pp);
        sp.setZ(pp.posz + PLAYER_DEAD_HEAD_FLOORZ_OFFSET);
        DoPlayerHeadDebris(pp);
    }

    public static void DoPlayerBeginRun(PlayerStr pp) {
        USER u = getUser(pp.PlayerSprite);
        if (u == null) {
            return;
        }

        // Crawl if in small aread automatically
        if (DoPlayerTestCrawl(pp)) {
            DoPlayerBeginCrawl(pp);
            return;
        }

        pp.Flags &= ~(PF_CRAWLING | PF_JUMPING | PF_FALLING | PF_LOCK_CRAWL | PF_CLIMBING);

        if (pp.WadeDepth != 0) {
            DoPlayerBeginWade(pp);
            return;
        }

        pp.friction = PLAYER_RUN_FRICTION;
        pp.floor_dist = PLAYER_RUN_FLOOR_DIST;
        pp.ceiling_dist = PLAYER_RUN_CEILING_DIST;
        pp.DoPlayerAction = DoPlayerRun;

        if (TEST(pp.Flags, PF_PLAYER_MOVED)) {
            NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Run);
        } else {
            NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Stand);
        }
    }

    public static void DoPlayerRun(PlayerStr pp) {
        Sprite psp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (psp == null || u == null) {
            return;
        }

        if (SectorIsUnderwaterArea(pp.cursectnum)) {
            DoPlayerBeginDiveNoWarp(pp);
            return;
        }

        // Crawl if in small aread automatically
        if (DoPlayerTestCrawl(pp)) {
            DoPlayerBeginCrawl(pp);
            return;
        }

        // Crawl Commanded
        if (TEST_SYNC_KEY(pp, SK_CRAWL)) {
            DoPlayerBeginCrawl(pp);
            return;
        }

        // Jump
        if (TEST_SYNC_KEY(pp, SK_JUMP)) {
            if (FLAG_KEY_PRESSED(pp, SK_JUMP)) {
                FLAG_KEY_RELEASE(pp, SK_JUMP);
                // make sure you stand at full heights for jumps/double jumps
                pp.posz = pp.loz - PLAYER_HEIGHT;
                DoPlayerBeginJump(pp);
                return;
            }
        } else {
            FLAG_KEY_RESET(pp, SK_JUMP);
        }

        // Crawl lock
        if (TEST_SYNC_KEY(pp, SK_CRAWL_LOCK)) {
            if (FLAG_KEY_PRESSED(pp, SK_CRAWL_LOCK)) {
                FLAG_KEY_RELEASE(pp, SK_CRAWL_LOCK);
                pp.Flags |= (PF_LOCK_CRAWL);
                DoPlayerBeginCrawl(pp);
                return;
            }
        } else {
            FLAG_KEY_RESET(pp, SK_CRAWL_LOCK);
        }

        if (PlayerFlyKey(pp)) {
            DoPlayerBeginFly(pp);
            return;
        }

        if (!TEST(pp.Flags, PF_DEAD) && !Prediction) {
            if (TEST_SYNC_KEY(pp, SK_OPERATE)) {
                Sector sec = boardService.getSector(pp.cursectnum);
                if (sec != null && FLAG_KEY_PRESSED(pp, SK_OPERATE)) {
                    if (TEST(sec.getExtra(), SECTFX_OPERATIONAL)) {
                        FLAG_KEY_RELEASE(pp, SK_OPERATE);
                        DoPlayerBeginOperate(pp);
                        return;
                    } else if (TEST(sec.getExtra(), SECTFX_TRIGGER)) {
                        int sp = FindNearSprite(psp, STAT_TRIGGER);
                        Sprite spr = boardService.getSprite(sp);
                        if (spr != null && SP_TAG5(spr) == 0) {// TRIGGER_TYPE_REMOTE_SO
                            pp.remote_sprite = sp;
                            FLAG_KEY_RELEASE(pp, SK_OPERATE);
                            DoPlayerBeginRemoteOperate(pp, SP_TAG7(spr));
                            return;
                        }
                    }
                }
            } else {
                FLAG_KEY_RESET(pp, SK_OPERATE);
            }
        }

        DoPlayerBob(pp);

        if (pp.WadeDepth != 0) {
            DoPlayerBeginWade(pp);
            return;
        }

        // If moving forward and tag is a ladder start climbing
        if (PlayerOnLadder(pp)) {
            DoPlayerBeginClimb(pp);
            return;
        }

        // Move about
        DoPlayerMove(pp);

        if (u.getRot() != PlayerStateGroup.sg_PlayerNinjaSword && u.getRot() != PlayerStateGroup.sg_PlayerNinjaPunch) {
            if (TEST(pp.Flags, PF_PLAYER_MOVED)) {
                if (u.getRot() != u.ActorActionSet.Run) {
                    NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Run);
                }
            } else {
                if (u.getRot() != u.ActorActionSet.Stand) {
                    NewStateGroup(pp.PlayerSprite, u.ActorActionSet.Stand);
                }
            }
        }

        // If the floor is far below you, fall hard instead of adjusting height
        if (PlayerFallTest(pp, PLAYER_HEIGHT)) {
            pp.jump_speed = Z(1);
            DoPlayerBeginFall(pp);
            // call PlayerFall now seems to iron out a hitch before falling
            DoPlayerFall(pp);
            return;
        }

        Sector sec = boardService.getSector(pp.cursectnum);
        if (sec != null && TEST(sec.getExtra(), SECTFX_DYNAMIC_AREA)) {
            pp.posz = pp.loz - PLAYER_HEIGHT;
        }

        // Adjust height moving up and down sectors
        DoPlayerHeight(pp);
    }

    public static void PlayerStateControl(final int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.Tics += synctics;

        // Skip states if too much time has passed
        while (u.Tics >= DTEST(u.State.Tics, SF_TICS_MASK)) {

            // Set Tics
            u.Tics -= DTEST(u.State.Tics, SF_TICS_MASK);

            // Transition to the next state
            u.State = u.State.getNext();

            // !JIM! Added this so I can do quick calls in player states!
            // Need this in order for floor blood and footprints to not get called more than
            // once.
            while (TEST(u.State.Tics, SF_QUICK_CALL)) {
                // Call it once and go to the next state
                u.State.Animator.animatorInvoke(SpriteNum);

                // if still on the same QUICK_CALL should you
                // go to the next state.
                if (TEST(u.State.Tics, SF_QUICK_CALL)) {
                    u.State = u.State.getNext();
                }
            }

            if (u.State.Pic == 0) {
                NewStateGroup(SpriteNum, u.State.getNextGroup());
            }
        }

        // Set picnum to the correct pic
        if (u.RotNum > 1) {
            sp.setPicnum(u.getRot().getState(0).Pic);
        } else {
            sp.setPicnum(u.State.Pic);
        }

        // Call the correct animator
        if (TEST(u.State.Tics, SF_PLAYER_FUNC)) {
            if (u.State.Animator != null) {
                u.State.Animator.animatorInvoke(SpriteNum);
            }
        }
    }

    public static void MoveSkipSavePos() {
        Sprite sp;
        int pnum;
        PlayerStr pp;

        MoveSkip8 = (MoveSkip8 + 1) & 7;
        MoveSkip4 = (MoveSkip4 + 1) & 3;
        MoveSkip2 = !MoveSkip2;

        // Save off player
        for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            pp = Player[pnum];

            pp.oposx = pp.posx;
            pp.oposy = pp.posy;
            pp.oposz = pp.posz;
            pp.oang = pp.pang;
            pp.ohoriz = pp.horiz;

            pp.obob_z = pp.bob_z;
            pp.obob_amt = pp.bob_amt;
        }

        // save off stats for skip4
        if (MoveSkip4 == 0) {
            ListNode<Sprite> nexti;
            for (int stat = STAT_SKIP4_START; stat <= STAT_SKIP4_INTERP_END; stat++) {
                for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = nexti) {
                    int i = node.getIndex();
                    nexti = node.getNext();
                    sp = node.get();
                    USER u = getUser(i);

                    if (u != null) {
                        u.ox = sp.getX();
                        u.oy = sp.getY();
                        u.oz = sp.getZ();
                    }
                }
            }
        }

        // save off stats for skip2
        if (!MoveSkip2) {
            ListNode<Sprite> nexti;
            for (int stat = STAT_SKIP2_START; stat <= STAT_SKIP2_INTERP_END; stat++) {
                for (ListNode<Sprite> node = boardService.getStatNode(stat); node != null; node = nexti) {
                    int i = node.getIndex();
                    nexti = node.getNext();
                    sp = node.get();
                    USER u = getUser(i);
                    if (u != null) {
                        u.ox = sp.getX();
                        u.oy = sp.getY();
                        u.oz = sp.getZ();
                    }
                }
            }
        }
    }

    public static void PlayerTimers(PlayerStr pp) {
        InventoryTimer(pp);
    }

    public static void ChopsCheck(PlayerStr pp) {
        if (!game.pMenu.gShowMenu && !TEST(pp.Flags, PF_DEAD) && pp.sop_riding == -1
                && numplayers <= 1) {
            if (((pp.input.bits | pp.input.vel | pp.input.svel) != 0 || pp.input.angvel != 0.0f || pp.input.aimvel != 0.0f)
                    || TEST(pp.Flags, PF_CLIMBING | PF_FALLING | PF_DIVING)) {
                // Hit a input key or other reason to stop chops
                if (pp.Chops != null) {
                    if (pp.sop_control == -1) // specail case
                    {
                        pp.Flags &= ~(PF_WEAPON_DOWN);
                    }
                    ChopsSetRetract(pp);
                }
                ChopTics = 0;
            } else {
                ChopTics += synctics;
                if (pp.Chops == null) {
                    // Chops not up
                    if (ChopTics > ChopTimer) {
                        ChopTics = 0;
                        // take weapon down
                        pp.Flags |= (PF_WEAPON_DOWN);
                        InitChops(pp);
                    }
                } else {
                    // Chops already up
                    if (ChopTics > ChopTimer) {
                        ChopTics = 0;
                        // bring weapon back up
                        pp.Flags &= ~(PF_WEAPON_DOWN);
                        ChopsSetRetract(pp);
                    }
                }
            }
        }
    }

    public static void PlayerGlobal(PlayerStr pp) {
        // This is the place for things that effect the player no matter what hes
        // doing
        PlayerTimers(pp);

        if (TEST(pp.Flags, PF_RECOIL)) {
            DoPlayerRecoil(pp);
        }

        if (!TEST(pp.Flags, PF_CLIP_CHEAT)) {
            if (pp.hi_sectp != -1 && boardService.getSector(pp.lo_sectp) != null) {
                int min_height;

                // just adjusted min height to something small to take care of all cases
                min_height = PLAYER_MIN_HEIGHT;

                if (klabs(pp.loz - pp.hiz) < min_height) {
                    USER u = getUser(pp.PlayerSprite);
                    if (u != null && !TEST(pp.Flags, PF_DEAD)) {
                        PlayerUpdateHealth(pp, -u.Health); // Make sure he dies!
                        PlayerCheckDeath(pp, -1);

                        if (TEST(pp.Flags, PF_DEAD)) {
                            return;
                        }
                    }
                }
            }
        }

        if (pp.FadeAmt > 0 && MoveSkip4 == 0) {
            DoPaletteFlash(pp);
        }

        // camera stuff that can't be done in drawscreen
        if (pp.circle_camera_dist > CIRCLE_CAMERA_DIST_MIN) {
            pp.circle_camera_ang = NORM_ANGLE(pp.circle_camera_ang + 14);
        }

        if (pp.camera_check_time_delay > 0) {
            pp.camera_check_time_delay -= synctics;
            if (pp.camera_check_time_delay <= 0) {
                pp.camera_check_time_delay = 0;
            }
        }
    }

    public static void UpdateScrollingMessages() {
        int i;

        // Update the scrolling multiplayer messages
        for (i = 0; i < MAXUSERQUOTES; i++) {
            if (user_quote_time[i] != 0) {
                user_quote_time[i]--;

                if (user_quote_time[i] <= 0) {
                    user_quote_time[i] = 0;
                }
            }
        }

        if (cfg.BorderNum > BORDER_BAR + 1) {
            quotebot = quotebotgoal;
        } else {
            if ((klabs(quotebotgoal - quotebot) <= 16)) {
                quotebot += ksgn(quotebotgoal - quotebot);
            } else {
                quotebot = quotebotgoal;
            }
        }
    }

    public static void MultiPlayLimits() {
        int pnum;
        PlayerStr pp;
        boolean Done = false;

        if (ExitLevel) {
            return;
        }

        if (gNet.MultiGameType != MultiGameTypes.MULTI_GAME_COMMBAT) {
            return;
        }

        if (gNet.KillLimit != 0) {
            for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                pp = Player[pnum];
                if (pp.Kills >= gNet.KillLimit) {
                    Done = true;
                }
            }
        }

        if (gNet.TimeLimit != 0) {
            gNet.TimeLimitClock -= synctics;
            if (gNet.TimeLimitClock <= 0) {
                Done = true;
            }
        }

        if (Done) {
            gNet.TimeLimitClock = gNet.TimeLimit;

            // do not increment if level is 23 thru 28
            if (Level <= 22) {
                Level++;
            }

            ExitLevel = true;
            FinishedLevel = true;
        }
    }

    public static void domovethings(BuildNet net) {
        int i, pnum;
        PlayerStr pp;

        // grab values stored in the fifo and put them in the players vars
        for (i = connecthead; i != -1; i = connectpoint2[i]) {
            pp = Player[i];
            pp.input.Copy(net.gFifoInput[net.gNetFifoTail & (MOVEFIFOSIZ - 1)][i]);
        }
        net.gNetFifoTail++;
        Arrays.fill(bCopySpriteOffs, false);

        if (gNet.MyCommPlayerQuit()) {
            return;
        }

        // check for pause of multi-play game
        if (game.nNetMode == NetMode.Multiplayer) {
            gNet.PauseMultiPlay();
        }

        net.CalcChecksum();

        if (game.gPaused || gDemoScreen.demfile == null && CommPlayers < 2 && !game.isCurrentScreen(gDemoScreen) && (game.menu.gShowMenu || Console.out.isShowing())) {
            if (!game.menu.isOpened(game.menu.mMenus[LASTSAVE])) {
                return;
            }
        }

        UpdateScrollingMessages(); // Update the multiplayer type messages

        // count the number of times this loop is called and use
        // for things like sync testing
        gNet.MoveThingsCount++;

        // recording is done here
        if (DemoRecording && rec != null) {
            rec.record();
        }

        int synctics = engine.getTimer().getFrameTicks(); // TODO: Temporaly code to reset interpolation in LegacyTimer
        totalsynctics += synctics;

        MoveSkipSavePos();

        for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            pp = Player[pnum];
            DoPlayerMenuKeys(pp);
        }

        if (game.gPaused) {
            return;
        }

        PlayClock += synctics;
        if (rtsplaying > 0) {
            rtsplaying--;
        }

        if (!DebugAnim) {
            if (!DebugActorFreeze) {
                DoAnim(synctics);
            }
        }

        // should pass pnum and use syncbits
        if (!DebugSector) {
            DoSector();
        }

        ProcessVisOn();

        if (MoveSkip4 == 0) {
            ProcessQuakeOn();
            ProcessQuakeSpot();
            JS_ProcessEchoSpot();
        }

        SpriteControl();

        for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            pp = Player[pnum];
            GlobPlayerStr = pp;

            // auto tracking mode for single player multi-game
            if (numplayers <= 1 && PlayerTrackingMode && pnum == screenpeek && screenpeek != myconnectindex) {
                Player[screenpeek].pang = EngineUtils.getAngle(Player[myconnectindex].posx - Player[screenpeek].posx,
                        Player[myconnectindex].posy - Player[screenpeek].posy);
            }

            if (!TEST(pp.Flags, PF_DEAD)) {
                LookupOperate(pp);
                WeaponOperate(pp);
                PlayerOperateEnv(pp);
            }

            // do for moving sectors
            DoPlayerSectorUpdatePreMove(pp);
            ChopsCheck(pp);

            pp.DoPlayerAction.invoke(pp);
            UpdatePlayerSprite(pp);

            pSpriteControl(pp);

            PlayerStateControl(pp.PlayerSprite);
            DoPlayerSectorUpdatePostMove(pp);
            PlayerGlobal(pp);
        }

        MultiPlayLimits();

        DoUpdateSounds3D();
        if (MoveSkip8 == 0) {// 8=5x 4=10x, 2=20x, 0=40x per second
            // This function is already only call 10x per sec, this widdles it down even
            // more!
            MoveSkip8 = (MoveSkip8 + 1) & 15;
        }

        net.CorrectPrediction();

        if (FinishTimer != 0) {
            if ((FinishTimer -= synctics) <= 0) {
                FinishTimer = 0;
                ExitLevel = true;
                FinishedLevel = true;
            }
        }

        if (ExitLevel) // reset level
        {
            ExitLevel = false;
            if (rec != null) {
                rec.close();
            }

            System.err.println("LeaveMap");
            if (numplayers > 1 && game.pNet.bufferJitter >= 0 && myconnectindex == connecthead) {
                for (i = 0; i <= game.pNet.bufferJitter; i++) {
                    game.pNet.GetNetworkInput(); // wait for other player before level end
                }
            }

            if (!game.pNet.WaitForAllPlayers(5000)) {
                game.pNet.NetDisconnect(myconnectindex);
                return;
            }

            game.pNet.ready2send = false;
            if (game.isCurrentScreen(gDemoScreen)) {
                return;
            }

            Gdx.app.postRunnable(Game::TerminateLevel);
            if (FinishedLevel) {
                if (FinishAnim != 0) {
                    if (gAnmScreen.init(FinishAnim)) {
                        game.changeScreen(gAnmScreen.setCallback(() -> {
                            if (Level != 4 && Level != 20) {
                                Level++;
                            }
                            game.changeScreen(gStatisticScreen);
                        }).escSkipping(true));
                        return;
                    }
                }
                game.changeScreen(gStatisticScreen);
            } else {
                if (!gGameScreen.enterlevel(gGameScreen.getTitle())) {
                    game.show();
                }
            }
        }
    }

    private static void LookupOperate(PlayerStr pp) {
        pp.lookang -= (pp.lookang >> 2);
        if (pp.lookang != 0 && (pp.lookang >> 2) == 0) {
            pp.lookang -= ksgn(pp.lookang);
        }

        if (TEST_SYNC_KEY(pp, SK_TILT_LEFT)) {
            pp.lookang -= 152;
        }

        if (TEST_SYNC_KEY(pp, SK_TILT_RIGHT)) {
            pp.lookang += 152;
        }
    }

    public static void InitAllPlayers(boolean NewGame) {
        PlayerStr pp;
        PlayerStr pfirst = Player[connecthead];
        int i;

        pfirst.horiz = pfirst.horizbase = 100;

        // Initialize all [MAX_SW_PLAYERS] arrays here!
        for (int ppi = 0; ppi < MAX_SW_PLAYERS; ppi++) {
            pp = Player[ppi];
            pp.posx = pp.oposx = pfirst.posx;
            pp.posy = pp.oposy = pfirst.posy;
            pp.posz = pp.oposz = pfirst.posz;
            pp.pang = pp.oang = pfirst.pang;
            pp.horiz = pp.ohoriz = pfirst.horiz;
            pp.cursectnum = pfirst.cursectnum;
            // set like this so that player can trigger something on start of the level
            pp.lastcursectnum =  (pfirst.cursectnum + 1);

            pp.horizbase = pfirst.horizbase;
            pp.oldposx = 0;
            pp.oldposy = 0;
            pp.climb_ndx = 10;
            pp.Killer = -1;
            pp.Kills = 0;
            pp.bcnt = 0;
            pp.UziShellLeftAlt = false;
            pp.UziShellRightAlt = false;

            pp.ceiling_dist = PLAYER_RUN_CEILING_DIST;
            pp.floor_dist = PLAYER_RUN_FLOOR_DIST;

            pp.WpnGotOnceFlags = 0;
            pp.DoPlayerAction = DoPlayerBeginRun;
            pp.KeyPressFlags = 0xFFFFFFFF;
            Arrays.fill(pp.KilledPlayer,  0);

            if (NewGame) {
                for (i = 0; i < MAX_INVENTORY; i++) {
                    pp.InventoryAmount[i] = 0;
                    pp.InventoryPercent[i] = 0;
                }
            }

            // My palette flashing stuff
            pp.FadeAmt = 0;
            pp.FadeTics = 0;
            pp.StartColor = 0;
            pp.horizoff = 0;

            List.Init(pp.PanelSpriteList);
        }
    }

    public static int SearchSpawnPosition(PlayerStr pp) {
        PlayerStr opp; // other player
        Sprite sp;
        int pos_num;
        int pnum;
        boolean blocked;

        do {
            // get a spawn position
            pos_num = RANDOM_RANGE(MAX_SW_PLAYERS);
            ListNode<Sprite> node = boardService.getStatNode(STAT_MULTI_START + pos_num);
            if (node == null) {
                return (0);
            }

            sp = node.get();

            blocked = false;

            // check to see if anyone else is blocking this spot
            for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                opp = Player[pnum];

                if (opp != pp) // don't test for yourself
                {
                    if (FindDistance3D(sp.getX() - opp.posx, sp.getY() - opp.posy, (sp.getZ() - opp.posz) >> 4) < 1000) {
                        blocked = true;
                        break;
                    }
                }
            }
        } while (blocked);

        return (pos_num);
    }

    public static void PlayerSpawnPosition(int pnum) {
        Sprite sp;
        PlayerStr pp = Player[pnum];
        ListNode<Sprite> spawn_sprite = null;
        int pos_num = pnum;
        int i;

        // find the first unused spawn position
        // garauntees that the spawn pos 0 will be used
        // Note: This code is not used if the player is DEAD and respawning

        for (i = 0; i < MAX_SW_PLAYERS; i++) {
            if (!SpawnPositionUsed[i]) {
                pos_num = i;
                break;
            }
        }

        // need to call this routine BEFORE resetting DEATH flag

        switch (gNet.MultiGameType) {
            default:
                break;
            case MULTI_GAME_NONE:
                // start from the beginning
                spawn_sprite = boardService.getStatNode(STAT_MULTI_START);
                break;
            case MULTI_GAME_COMMBAT:
            case MULTI_GAME_AI_BOTS:
                // start from random position after death
                if (TEST(pp.Flags, PF_DEAD)) {
                    pos_num = SearchSpawnPosition(pp);
                }

                spawn_sprite = boardService.getStatNode(STAT_MULTI_START + pos_num);
                break;
            case MULTI_GAME_COOPERATIVE:
                // start your assigned spot
                spawn_sprite = boardService.getStatNode(STAT_CO_OP_START + pos_num);
                break;
        }

        SpawnPositionUsed[pos_num] = true;

        if (spawn_sprite == null) {
            spawn_sprite = boardService.getStatNode(STAT_MULTI_START);
            if (spawn_sprite == null) {
                return;
            }
        }

        sp = spawn_sprite.get();

        pp.posx = pp.oposx = sp.getX();
        pp.posy = pp.oposy = sp.getY();
        pp.posz = pp.oposz = sp.getZ();
        pp.pang = pp.oang = sp.getAng();
        pp.cursectnum = sp.getSectnum();

        engine.getzsofslope(pp.cursectnum, pp.posx, pp.posy, fz, cz);
        // if too close to the floor - stand up
        if (pp.posz > fz.get() - PLAYER_HEIGHT) {
            pp.posz = pp.oposz = fz.get() - PLAYER_HEIGHT;
        }
    }

    public static void InitMultiPlayerInfo() {
        PlayerStr pp = Player[0];
        Sprite sp;
        int pnum, start0, stat;
        int tag;

        // this routine is called before SpriteSetup - process start positions NOW
        ListNode<Sprite> NextSprite;
        for (ListNode<Sprite> node = boardService.getStatNode(0); node != null; node = NextSprite) {
            int SpriteNum = node.getIndex();
            NextSprite = node.getNext();
            sp = node.get();

            tag = sp.getHitag();

            if (sp.getPicnum() == ST1) {
                switch (tag) {
                    case MULTI_PLAYER_START:
                        change_sprite_stat(SpriteNum, STAT_MULTI_START + sp.getLotag());
                        break;
                    case MULTI_COOPERATIVE_START:
                        change_sprite_stat(SpriteNum, STAT_CO_OP_START + sp.getLotag());
                        break;
                }
            }
        }

        // set up the zero starting positions - its not saved in the map as a ST1 sprite
        // like the others

        for (stat = 0; stat < MultiStatList.length; stat++) {
            if (gNet.MultiGameType != MultiGameTypes.MULTI_GAME_NONE) {
                // if start position is physically set then don't spawn a new one
                if (boardService.getStatNode(MultiStatList[stat]) != null) {
                    continue;
                }
            }

            start0 = SpawnSprite(MultiStatList[stat], ST1, null, pp.cursectnum, pp.posx, pp.posy, pp.posz, pp.getAnglei(), 0);

            setUser(start0, null);
            Sprite sp0 = boardService.getSprite(start0);
            if (sp0 != null) {
                sp0.setPicnum(ST1);
            }
        }

        Arrays.fill(SpawnPositionUsed, false);

        // Initialize multi player positions here
        for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            pp = Player[pnum];
            switch (gNet.MultiGameType) {
                default:
                    break;
                case MULTI_GAME_NONE:
                case MULTI_GAME_COOPERATIVE:
                    PlayerSpawnPosition(pnum);
                    break;
                case MULTI_GAME_COMMBAT:
                case MULTI_GAME_AI_BOTS:
                    // there are no keys in deathmatch play
                    Arrays.fill(Player[0].HasKey, (byte) -1);
                    Arrays.fill(pp.HasKey, (byte) -1);

                    PlayerSpawnPosition(pnum);
                    break;
            }
        }
    }

    // If player stepped in something gooey, track it all over the place.
    public static void DoFootPrints(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u != null && u.PlayerP != -1) {
            if (FAF_ConnectArea(Player[u.PlayerP].cursectnum)) {
                return;
            }

            if (Player[u.PlayerP].NumFootPrints > 0) {
                QueueFootPrint(SpriteNum);
            }
        }
    }

    public static void CheckFootPrints(PlayerStr pp) {
        if (pp.NumFootPrints <= 0 || FootMode != FootType.WATER_FOOT) {
            // Hey, you just got your feet wet!
            pp.NumFootPrints =  (RANDOM_RANGE(10) + 3);
            FootMode = FootType.WATER_FOOT;
        }
    }

    public static void PlayerSaveable() {
        SaveData(DoPlayerSpriteReset);
        SaveData(DoFootPrints);
        SaveData(QueueFloorBlood);
        SaveData(pSetVisNorm);

        SaveData(s_PlayerNinjaRun);
        SaveGroup(PlayerStateGroup.sg_PlayerNinjaRun);
        SaveData(s_PlayerNinjaStand);
        SaveGroup(PlayerStateGroup.sg_PlayerNinjaStand);
        SaveData(s_PlayerNinjaJump);
        SaveGroup(PlayerStateGroup.sg_PlayerNinjaJump);
        SaveData(s_PlayerNinjaFall);
        SaveGroup(PlayerStateGroup.sg_PlayerNinjaFall);
        SaveData(s_PlayerNinjaClimb);
        SaveGroup(PlayerStateGroup.sg_PlayerNinjaClimb);
        SaveData(s_PlayerNinjaCrawl);
        SaveGroup(PlayerStateGroup.sg_PlayerNinjaCrawl);
        SaveData(s_PlayerNinjaSwim);
        SaveGroup(PlayerStateGroup.sg_PlayerNinjaSwim);
        SaveData(s_PlayerHeadFly);
        SaveGroup(PlayerStateGroup.sg_PlayerHeadFly);
        SaveData(s_PlayerHead);
        SaveGroup(PlayerStateGroup.sg_PlayerHead);
        SaveData(s_PlayerHeadHurl);
        SaveGroup(PlayerStateGroup.sg_PlayerHeadHurl);
        SaveData(s_PlayerDeath);
        SaveGroup(PlayerStateGroup.sg_PlayerDeath);
        SaveData(s_PlayerNinjaSword);
        SaveGroup(PlayerStateGroup.sg_PlayerNinjaSword);
        SaveData(s_PlayerNinjaPunch);
        SaveGroup(PlayerStateGroup.sg_PlayerNinjaPunch);
        SaveData(s_PlayerNinjaFly);
        SaveGroup(PlayerStateGroup.sg_PlayerNinjaFly);
    }

    public enum Player_Action_Func implements Func<PlayerStr> {
        DoPlayerCrawl(ru.m210projects.Wang.Player::DoPlayerCrawl),
        DoPlayerTeleportPause(ru.m210projects.Wang.Player::DoPlayerTeleportPause),
        DoPlayerJump(ru.m210projects.Wang.Player::DoPlayerJump),
        DoPlayerFall(ru.m210projects.Wang.Player::DoPlayerFall),
        DoPlayerForceJump(ru.m210projects.Wang.Player::DoPlayerForceJump),
        DoPlayerClimb(ru.m210projects.Wang.Player::DoPlayerClimb),
        DoPlayerFly(ru.m210projects.Wang.Player::DoPlayerFly),
        DoPlayerDive(ru.m210projects.Wang.Player::DoPlayerDive),
        DoPlayerWade(ru.m210projects.Wang.Player::DoPlayerWade),
        DoPlayerOperateTurret(ru.m210projects.Wang.Player::DoPlayerOperateTurret),
        DoPlayerOperateBoat(ru.m210projects.Wang.Player::DoPlayerOperateBoat),
        DoPlayerOperateTank(ru.m210projects.Wang.Player::DoPlayerOperateTank),
        DoPlayerRun(ru.m210projects.Wang.Player::DoPlayerRun),
        DoPlayerDeathFlip(ru.m210projects.Wang.Player::DoPlayerDeathFlip),
        DoPlayerDeathDrown(ru.m210projects.Wang.Player::DoPlayerDeathDrown),
        DoPlayerDeathCrumble(ru.m210projects.Wang.Player::DoPlayerDeathCrumble),
        DoPlayerDeathExplode(ru.m210projects.Wang.Player::DoPlayerDeathExplode),
        DoPlayerBeginRun(ru.m210projects.Wang.Player::DoPlayerBeginRun),
        InitWeaponSword(Sword::InitWeaponSword),
        InitWeaponFist(Fist::InitWeaponFist),
        InitWeaponStar(Star::InitWeaponStar),
        InitWeaponShotgun(Shotgun::InitWeaponShotgun),
        InitWeaponUzi(Uzi::InitWeaponUzi),
        InitWeaponMicro(Micro::InitWeaponMicro),
        InitWeaponGrenade(Grenade::InitWeaponGrenade),
        InitWeaponMine(Mine::InitWeaponMine),
        InitWeaponRail(Rail::InitWeaponRail),
        InitWeaponHothead(HotHead::InitWeaponHothead),
        InitWeaponHeart(Heart::InitWeaponHeart),
        UseInventoryMedkit(Inv::UseInventoryMedkit),
        UseInventoryCloak(Inv::UseInventoryCloak),
        UseInventoryNightVision(Inv::UseInventoryNightVision),
        UseInventoryChemBomb(Inv::UseInventoryChemBomb),
        UseInventoryFlashBomb(Inv::UseInventoryFlashBomb),
        UseInventoryCaltrops(Inv::UseInventoryCaltrops);

        private final Func<PlayerStr> func;

        Player_Action_Func(Func<PlayerStr> func) {
            this.func = func;
        }

        public void invoke(PlayerStr pp) {
            func.invoke(pp);
        }
    }

    public enum PlayerStateGroup implements StateGroup {
        sg_PlayerNinjaRun(s_PlayerNinjaRun[0], s_PlayerNinjaRun[1], s_PlayerNinjaRun[2], s_PlayerNinjaRun[3],
                s_PlayerNinjaRun[4]),
        sg_PlayerNinjaStand(s_PlayerNinjaStand[0], s_PlayerNinjaStand[1], s_PlayerNinjaStand[2], s_PlayerNinjaStand[3],
                s_PlayerNinjaStand[4]),
        sg_PlayerNinjaJump(s_PlayerNinjaJump[0], s_PlayerNinjaJump[1], s_PlayerNinjaJump[2], s_PlayerNinjaJump[3],
                s_PlayerNinjaJump[4]),
        sg_PlayerNinjaFall(s_PlayerNinjaFall[0], s_PlayerNinjaFall[1], s_PlayerNinjaFall[2], s_PlayerNinjaFall[3],
                s_PlayerNinjaFall[4]),
        sg_PlayerNinjaClimb(s_PlayerNinjaClimb[0], s_PlayerNinjaClimb[1], s_PlayerNinjaClimb[2], s_PlayerNinjaClimb[3],
                s_PlayerNinjaClimb[4]),
        sg_PlayerNinjaCrawl(s_PlayerNinjaCrawl[0], s_PlayerNinjaCrawl[1], s_PlayerNinjaCrawl[2], s_PlayerNinjaCrawl[3],
                s_PlayerNinjaCrawl[4]),
        sg_PlayerNinjaSwim(s_PlayerNinjaSwim[0], s_PlayerNinjaSwim[1], s_PlayerNinjaSwim[2], s_PlayerNinjaSwim[3],
                s_PlayerNinjaSwim[4]),
        sg_PlayerHeadFly(s_PlayerHeadFly[0], s_PlayerHeadFly[1], s_PlayerHeadFly[2], s_PlayerHeadFly[3],
                s_PlayerHeadFly[4]),
        sg_PlayerHead(s_PlayerHead[0], s_PlayerHead[1], s_PlayerHead[2], s_PlayerHead[3], s_PlayerHead[4]),
        sg_PlayerHeadHurl(s_PlayerHeadHurl[0], s_PlayerHeadHurl[1], s_PlayerHeadHurl[2], s_PlayerHeadHurl[3],
                s_PlayerHeadHurl[4]),
        sg_PlayerDeath(s_PlayerDeath[0], s_PlayerDeath[1], s_PlayerDeath[2], s_PlayerDeath[3], s_PlayerDeath[4]),
        sg_PlayerNinjaSword(s_PlayerNinjaSword[0], s_PlayerNinjaSword[1], s_PlayerNinjaSword[2], s_PlayerNinjaSword[3],
                s_PlayerNinjaSword[4]),
        sg_PlayerNinjaPunch(s_PlayerNinjaPunch[0], s_PlayerNinjaPunch[1], s_PlayerNinjaPunch[2], s_PlayerNinjaPunch[3],
                s_PlayerNinjaPunch[4]),
        sg_PlayerNinjaFly(s_PlayerNinjaFly[0], s_PlayerNinjaFly[1], s_PlayerNinjaFly[2], s_PlayerNinjaFly[3],
                s_PlayerNinjaFly[4]);

        private final State[][] group;
        private int index = -1;

        PlayerStateGroup(State[]... states) {
            group = states;
        }

        @Override
        public State getState(int rotation, int offset) {
            return group[rotation][offset];
        }

        @Override
        public State getState(int rotation) {
            return group[rotation][0];
        }

        @Override
        public int index() {
            return index;
        }

        @Override
        public void setIndex(int index) {
            this.index = index;
        }
    }
}
