package ru.m210projects.Wang;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Sound.SoundType;
import ru.m210projects.Wang.Type.Animator;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.Sect_User;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JWeapon.InitBloodSpray;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Sector.*;
import static ru.m210projects.Wang.Sprites.KillSprite;
import static ru.m210projects.Wang.Stag.SECT_LOCK_DOOR;
import static ru.m210projects.Wang.Stag.SECT_VATOR;
import static ru.m210projects.Wang.Tags.TAG_SPRITE_SWITCH_VATOR;
import static ru.m210projects.Wang.Text.KeyDoorMessage;
import static ru.m210projects.Wang.Text.PutStringInfo;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Weapon.UpdateSinglePlayKills;

public class Vator {

    private static int pDoVatorMove;
    public static final Animator DoVator = new Animator((Animator.Runnable) Vator::DoVator);
    public static final Animator DoVatorAuto = new Animator((Animator.Runnable) Vator::DoVatorAuto);
    public static void ReverseVator(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // if paused go ahead and start it up again
        if (u.Tics != 0) {
            u.Tics = 0;
            SetVatorActive(SpriteNum);
            return;
        }

        // moving toward to OFF pos
        if (u.z_tgt == u.oz) {
            if (sp.getZ() == u.oz) {
                u.z_tgt = u.sz;
            } else if (u.sz == u.oz) {
                u.z_tgt = sp.getZ();
            }
        } else if (u.z_tgt == u.sz) {
            if (sp.getZ() == u.oz) {
                u.z_tgt = sp.getZ();
            } else if (u.sz == u.oz) {
                u.z_tgt = u.sz;
            }
        }

        u.vel_rate =  -u.vel_rate;
    }

    public static void VatorSwitch(int match, int setting) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_DEFAULT); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            if (sp.getLotag() == TAG_SPRITE_SWITCH_VATOR && sp.getHitag() == match) {
                AnimateSwitch(sp, setting);
            }
        }

    }

    public static void SetVatorActive(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sector sectp = boardService.getSector(sp.getSectnum());
        if (sectp == null) {
            return;
        }

        if (TEST(sp.getCstat(), CSTAT_SPRITE_YFLIP)) {
            game.pInt.setceilinterpolate(sp.getSectnum(), sectp);
        } else {
            game.pInt.setfloorinterpolate(sp.getSectnum(), sectp);
        }

        InterpSectorSprites(sp.getSectnum(), true);

        // play activate sound
        DoSoundSpotMatch(SP_TAG2(sp), 1, SoundType.SOUND_OBJECT_TYPE);

        u.Flags |= (SPR_ACTIVE);
        u.Tics = 0;

        // moving to the ON position
        if (u.z_tgt == sp.getZ()) {
            VatorSwitch(SP_TAG2(sp), ON);
        } else
            // moving to the OFF position
            if (u.z_tgt == u.sz) {
                VatorSwitch(SP_TAG2(sp), OFF);
            }
    }

    public static void SetVatorInactive(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        InterpSectorSprites(sp.getSectnum(), false);

        // play inactivate sound
        DoSoundSpotMatch(SP_TAG2(sp), 2, SoundType.SOUND_OBJECT_TYPE);

        u.Flags &= ~(SPR_ACTIVE);
    }

    // called for operation from the space bar
    public static void DoVatorOperate(PlayerStr pp, int sectnum) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite fsp = node.get();

            if (fsp.getStatnum() == STAT_VATOR && SP_TAG1(fsp) == SECT_VATOR && SP_TAG3(fsp) == 0) {
                sectnum = fsp.getSectnum();

                // single play only vator
                // boolean 8 must be set for message to display
                if (TEST_BOOL4(fsp) && (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT
                        || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_AI_BOTS)) {
                    if (pp != null && TEST_BOOL11(fsp)) {
                        PutStringInfo(pp, "This only opens in single play.");
                    }
                    continue;
                }

                int match = SP_TAG2(fsp);
                if (match > 0) {
                    if (!TestVatorMatchActive(match)) {
                        DoVatorMatch(pp, match);
                    }
                    return;
                }

                Sect_User su = getSectUser(sectnum);
                if (pp != null && su != null && su.stag == SECT_LOCK_DOOR
                        && su.number != 0) {
                    int key_num;

                    key_num = su.number;
                    PutStringInfo(pp, KeyDoorMessage[key_num - 1]);
                    return;
                }

                SetVatorActive(i);
                break;
            }
        }
    }

    // called from switches and triggers
    // returns first vator found
    public static void DoVatorMatch(PlayerStr pp, int match) {
        // VatorSwitch(match, ON);
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_VATOR); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite fsp = node.get();

            if (SP_TAG1(fsp) == SECT_VATOR && SP_TAG2(fsp) == match) {
                USER fu = getUser(i);
                if (fu == null) {
                    continue;
                }

                // single play only vator
                // boolean 8 must be set for message to display
                if (TEST_BOOL4(fsp) && (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT
                        || gNet.MultiGameType == MultiGameTypes.MULTI_GAME_AI_BOTS)) {
                    if (pp != null && TEST_BOOL11(fsp)) {
                        PutStringInfo(pp, "This only opens in single play.");
                    }
                    continue;
                }

                // lock code
                int sectnum = fsp.getSectnum();
                Sect_User su = getSectUser(sectnum);
                if (pp != null && su != null && su.stag == SECT_LOCK_DOOR
                        && su.number != 0) {
                    int key_num = su.number;
                    if (key_num > 0 && key_num < NUM_KEYS) {
                        PutStringInfo(pp, KeyDoorMessage[key_num - 1]);
                    } else {
                        PutStringInfo(pp, "I think you'll never open this door :)");
                    }
                    return;
                }

                // remember the player than activated it
                fu.PlayerP = (pp != null) ? pp.pnum : -1;

                if (TEST(fu.Flags, SPR_ACTIVE)) {
                    ReverseVator(i);
                    continue;
                }

                SetVatorActive(i);
            }
        }

    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean TestVatorMatchActive(int match) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_VATOR); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite fsp = node.get();

            if (SP_TAG1(fsp) == SECT_VATOR && SP_TAG2(fsp) == match) {
                USER fu = getUser(i);

                // Does not have to be inactive to be operated
                if (fu == null || TEST_BOOL6(fsp)) {
                    continue;
                }

                if (TEST(fu.Flags, SPR_ACTIVE) || fu.Tics != 0) {
                    return (true);
                }
            }
        }

        return (false);
    }

    public static void InterpSectorSprites(int sectnum, boolean state) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();
            USER u = getUser(i);

            if (u != null) {
                if (TEST(u.Flags, SPR_SKIP4) && sp.getStatnum() <= STAT_SKIP4_INTERP_END) {
                    continue;
                }

                if (TEST(u.Flags, SPR_SKIP2) && sp.getStatnum() <= STAT_SKIP2_INTERP_END) {
                    continue;
                }
            }

            if (state) {
                game.pInt.setsprinterpolate(i, sp);
            }
        }
    }

    public static void MoveSpritesWithSector(int sectnum, int z_amt, boolean type) {
        boolean both = false;
        Sect_User su = getSectUser(sectnum);
        if (su != null) {
            both = TEST(su.flags, SECTFU_VATOR_BOTH);
        }

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();

            if (getUser(i) != null) {
                switch (sp.getStatnum()) {
                    case STAT_ITEM:
                    case STAT_NO_STATE:
                    case STAT_MINE_STUCK:
                    case STAT_WALLBLOOD_QUEUE:
                    case STAT_FLOORBLOOD_QUEUE:
                    case STAT_STATIC_FIRE:
                        break;
                    default:
                        continue;
                }
            } else {
                switch (sp.getStatnum()) {
                    case STAT_STAR_QUEUE:
                    case STAT_HOLE_QUEUE:
                        continue;
                }
            }

            if (TEST(sp.getExtra(), SPRX_STAY_PUT_VATOR)) {
                continue;
            }

            if (both) {
                // sprite started close to floor
                if (TEST(sp.getCstat(), CSTAT_SPRITE_CLOSE_FLOOR)) {
                    // this is a ceiling
                    if (type) {
                        continue;
                    }
                } else {
                    // this is a floor
                    if (!type) {
                        continue;
                    }
                }
            }
            game.pInt.setsprinterpolate(i, sp);
            sp.setZ(sp.getZ() + z_amt);
        }
    }

    public static int DoVatorMove(int SpriteNum, int lptr) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return 0;
        }

        int zval = lptr;
        int move_amt;

        // if LESS THAN goal
        if (zval < u.z_tgt) {
            // move it DOWN
            zval += (synctics * u.jump_speed);

            u.jump_speed += u.vel_rate * synctics;

            // if the other way make it equal
            if (zval > u.z_tgt) {
                zval = u.z_tgt;
            }
        }

        // if GREATER THAN goal
        if (zval > u.z_tgt) {
            // move it UP
            zval -= (synctics * u.jump_speed);

            u.jump_speed += u.vel_rate * synctics;

            if (zval < u.z_tgt) {
                zval = u.z_tgt;
            }
        }

        move_amt = zval - lptr;
        pDoVatorMove = zval;

        return (move_amt);
    }

    public static void DoVator(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sector sectp = boardService.getSector(sp.getSectnum());
        if (sectp == null) {
            return;
        }

        int amt, lptr;
        // u.sz - where the sector z started
        // u.z_tgt - current target z
        // u.oz - original z - where it initally starts off
        // sp.z - z of the sprite
        // u.vel_rate - velocity

        if (TEST(sp.getCstat(), CSTAT_SPRITE_YFLIP)) {
            amt = DoVatorMove(SpriteNum, sectp.getCeilingz());
            game.pInt.setceilinterpolate(sp.getSectnum(), sectp);
            sectp.setCeilingz(lptr = pDoVatorMove);
            MoveSpritesWithSector(sp.getSectnum(), amt, true); // ceiling
        } else {
            amt = DoVatorMove(SpriteNum, sectp.getFloorz());
            game.pInt.setfloorinterpolate(sp.getSectnum(), sectp);
            sectp.setFloorz(lptr = pDoVatorMove);
            MoveSpritesWithSector(sp.getSectnum(), amt, false); // floor
        }

        // EQUAL this entry has finished
        if (lptr == u.z_tgt) {
            // in the ON position
            if (u.z_tgt == sp.getZ()) {
                // change target
                u.z_tgt = u.sz;
                u.vel_rate =  -u.vel_rate;

                SetVatorInactive(SpriteNum);

                // if tag6 and nothing blocking door
                if (SP_TAG6(sp) != 0 && !TEST_BOOL8(sp)) {
                    DoMatchEverything(u.PlayerP != -1 ? Player[u.PlayerP] : null, SP_TAG6(sp), -1);
                }
            } else
                // in the OFF position
                if (u.z_tgt == u.sz) {
                    // change target
                    u.jump_speed =  u.vel_tgt;
                    u.vel_rate =  klabs(u.vel_rate);
                    u.z_tgt = sp.getZ();

                    RESET_BOOL8(sp);
                    SetVatorInactive(SpriteNum);

                    if (SP_TAG6(sp) != 0 && TEST_BOOL5(sp)) {
                        DoMatchEverything(u.PlayerP != -1 ? Player[u.PlayerP] : null, SP_TAG6(sp), -1);
                    }
                }

            // operate only once
            if (TEST_BOOL2(sp)) {
                SetVatorInactive(SpriteNum);
                KillSprite(SpriteNum);
                return;
            }

            // setup to go back to the original z
            if (lptr != u.oz) {
                if (u.WaitTics != 0) {
                    u.Tics = u.WaitTics;
                }
            }
        } else {
            // if heading for the OFF (original) position and should NOT CRUSH
            if (TEST_BOOL3(sp) && u.z_tgt == u.oz) {
                Sprite bsp;
                USER bu;
                boolean found = false;

                ListNode<Sprite> nexti;
                for (ListNode<Sprite> node = boardService.getSectNode(sp.getSectnum()); node != null; node = nexti) {
                    int i = node.getIndex();
                    nexti = node.getNext();
                    bsp = node.get();
                    bu = getUser(i);

                    if (bsp.getStatnum() == STAT_ENEMY) {
                        if (klabs(sectp.getCeilingz() - sectp.getFloorz()) < SPRITE_SIZE_Z(i)) {
                            InitBloodSpray(i, true, -1);
                            UpdateSinglePlayKills(i, null);
                            KillSprite(i);
                            continue;
                        }
                    }

                    if (bu != null && TEST(bsp.getCstat(), CSTAT_SPRITE_BLOCK) && TEST(bsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                        // found something blocking so reverse to ON position
                        ReverseVator(SpriteNum);
                        SET_BOOL8(sp); // tell vator that something blocking door
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    PlayerStr pp;
                    // go ahead and look for players clip box bounds
                    for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                        pp = Player[pnum];
                        if ((boardService.getSector(pp.lo_sectp) != null && pp.lo_sectp == sp.getSectnum())
                                || (pp.hi_sectp != -1 && pp.hi_sectp == sp.getSectnum())) {
                            ReverseVator(SpriteNum);

                            u.vel_rate =  -u.vel_rate;
                        }
                    }
                }
            } else {
                ListNode<Sprite> nexti;
                for (ListNode<Sprite> node = boardService.getSectNode(sp.getSectnum()); node != null; node = nexti) {
                    int i = node.getIndex();
                    nexti = node.getNext();
                    Sprite bsp = node.get();

                    if (bsp.getStatnum() == STAT_ENEMY) {
                        if (klabs(sectp.getCeilingz() - sectp.getFloorz()) < SPRITE_SIZE_Z(i)) {
                            InitBloodSpray(i, true, -1);
                            UpdateSinglePlayKills(i, null);
                            KillSprite(i);
                        }
                    }
                }
            }
        }
    }

    public static void DoVatorAuto(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sector sectp = boardService.getSector(sp.getSectnum());
        if (sectp == null) {
            return;
        }

        int amt, lptr;
        if (TEST(sp.getCstat(), CSTAT_SPRITE_YFLIP)) {
            lptr = sectp.getCeilingz();
            game.pInt.setceilinterpolate(sp.getSectnum(), sectp);
            amt = DoVatorMove(SpriteNum, lptr);
            sectp.setCeilingz(lptr = pDoVatorMove);
            MoveSpritesWithSector(sp.getSectnum(), amt, true); // ceiling
        } else {
            lptr = sectp.getFloorz();
            game.pInt.setfloorinterpolate(sp.getSectnum(), sectp);
            amt = DoVatorMove(SpriteNum, lptr);
            sectp.setFloorz(lptr = pDoVatorMove);
            MoveSpritesWithSector(sp.getSectnum(), amt, false); // floor
        }

        // EQUAL this entry has finished
        if (lptr == u.z_tgt) {
            // in the UP position
            if (u.z_tgt == sp.getZ()) {
                // change target
                u.z_tgt = u.sz;
                u.vel_rate =  -u.vel_rate;
                u.Tics = u.WaitTics;

                if (SP_TAG6(sp) != 0) {
                    DoMatchEverything(u.PlayerP != -1 ? Player[u.PlayerP] : null, SP_TAG6(sp), -1);
                }
            } else
                // in the DOWN position
                if (u.z_tgt == u.sz) {
                    // change target
                    u.jump_speed =  u.vel_tgt;
                    u.vel_rate =  klabs(u.vel_rate);
                    u.z_tgt = sp.getZ();
                    u.Tics = u.WaitTics;

                    if (SP_TAG6(sp) != 0 && TEST_BOOL5(sp)) {
                        DoMatchEverything(null, SP_TAG6(sp), -1);
                    }
                }
        }
    }

}
