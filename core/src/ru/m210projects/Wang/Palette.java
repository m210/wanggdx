package ru.m210projects.Wang;


import ru.m210projects.Build.Script.TextureHDInfo;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.WangScreenFade;

import static ru.m210projects.Build.Engine.MAXPALOOKUPS;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.screenpeek;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Inv.INVENTORY_NIGHT_VISION;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Type.MyTypes.TEST;

public class Palette {

    public static final WangScreenFade WANG_SCREEN_FADE = new WangScreenFade("ScreenFade");

    public static final int MAXFADETICS = 5;
    public static final int COLOR_PAIN = 128; // Light red range
    public static final int FADE_DAMAGE_FACTOR = 3; // 100 health / 32 shade cycles = 3.125
    public static final int FORCERESET = 256;
    public static final int LT_GREY = (1);
    public static final int DK_GREY = (16);
    public static final int LT_BROWN = (16 * 2);
    public static final int DK_BROWN = (16 * 3);
    public static final int LT_TAN = (16 * 4);
    public static final int DK_TAN = (16 * 5);
    public static final int RUST_RED = (16 * 6);
    public static final int RED = (16 * 7);
    public static final int YELLOW = (16 * 8);
    public static final int BRIGHT_GREEN = (16 * 9);
    public static final int DK_GREEN = (16 * 10);
    public static final int GREEN = (16 * 11);
    public static final int LT_BLUE = (16 * 12);
    public static final int DK_BLUE = (16 * 13);
    public static final int PURPLE = (16 * 14);
    public static final int FIRE = (16 * 15);
    public static final int PALETTE_DEFAULT = 0;
    public static final int PALETTE_FOG = 1;
    // blue sword blade test
    public static final int PALETTE_MENU_HIGHLIGHT = 2;
    // used for the elector gore pieces
    public static final int PALETTE_ELECTRO_GORE = 3;
    // turns ninjas belt and headband red
    public static final int PALETTE_BASIC_NINJA = 4;
    // diving in lava
    public static final int PALETTE_DIVE_LAVA = 5;
    // turns ninjas belt and headband red
    public static final int PALETTE_RED_NINJA = 6;
    // used for the mother ripper - she is bigger/stronger/brown
    public static final int PALETTE_BROWN_RIPPER = 7;
    // turns ninjas belt and headband red
    public static final int PALETTE_GREEN_NINJA = 8;
    // reserved diving palette this is copied over the default palette
    // when needed - NOTE: could move this to a normal memory buffer if palette
    // slot is needed.
    public static final int PALETTE_DIVE = 9;
    public static final int PALETTE_SKEL_GORE = 10;
    // turns ALL colors to shades of GREEN/BLUE/RED
    public static final int PALETTE_GREEN_LIGHTING = 11;
    public static final int PALETTE_BLUE_LIGHTING = 13;
    public static final int PALETTE_RED_LIGHTING = 14;
    // for brown bubbling sludge
    public static final int PALETTE_SLUDGE = 15;
    // Player 0 uses default palette - others use these
    // turns ninja's vests (when we get them) into different color ranges
    public static final int PALETTE_PLAYER0 = 16;
    public static final int PAL_XLAT_BROWN = 16;
    public static final int PALETTE_PLAYER1 = 17;
    public static final int PAL_XLAT_LT_GREY = 17;
    public static final int PALETTE_PLAYER3 = 19;
    public static final int PALETTE_PLAYER4 = 20;
    public static final int PALETTE_PLAYER5 = 21;
    public static final int PALETTE_PLAYER6 = 22;
    public static final int PALETTE_PLAYER7 = 23;
    public static final int PALETTE_PLAYER8 = 24;
    public static final int PAL_XLAT_LT_TAN = 24;
    public static final int PALETTE_PLAYER9 = 25;
    public static final int PAL_XLAT_BRIGHT_GREEN = 27;
    public static final int PALETTE_ILLUMINATE = 32; // Used to make sprites bright green in night vision
    public static final int PLAYER_COLOR_MAPS = 15;

    //////////////////////////////////////////
    // Set the amount of redness for damage
    // the player just took
    //////////////////////////////////////////
    public static void SetFadeAmt(PlayerStr pp, int damage, int startcolor) {

        if (klabs(pp.FadeAmt) > 0 && startcolor == (pp.StartColor & 0xFF)) {
            return;
        }

        // Don't ever over ride flash bomb
        if (pp.StartColor == 1 && klabs(pp.FadeAmt) > 0) {
            return;
        }

        // Reset the palette
        if (pp == Player[screenpeek]) {
            ResetPalette(pp, startcolor); // screenpeek
        }

        int fadedamage;
        if (damage < -150 && damage > -1000) {
            fadedamage = 150;
        } else if (damage < -1000) { // Underwater
            fadedamage = klabs(damage + 1000);
        } else {
            fadedamage = klabs(damage);
        }

        if (damage >= -5 && damage < 0) {
            fadedamage += 10;
        }

        // Don't let red to TOO red
        if (startcolor == COLOR_PAIN && fadedamage > 100) {
            fadedamage = 100;
        }

        pp.FadeAmt =  (fadedamage / FADE_DAMAGE_FACTOR);
        if (pp.FadeAmt <= 0) {
            pp.FadeAmt = 0;
            return;
        }

        // It's a health item, just do a preset flash amount
        if (damage > 0) {
            pp.FadeAmt = 3;
        }

        pp.StartColor = (byte) startcolor;
        pp.FadeTics = 0;

        // Do initial palette set
        if (pp == Player[screenpeek]) {
            byte[] palette = engine.getPaletteManager().getBasePalette();
            byte[] temp_pal = WANG_SCREEN_FADE.getPalette();
            int red = palette[3 * startcolor] & 0xFF;
            int green = palette[3 * startcolor + 1] & 0xFF;
            int blue = palette[3 * startcolor + 2] & 0xFF;

            int palreg, usereg = 0, tmpreg1, tmpreg2;
            for (palreg = 0; palreg < 768; palreg++) {
                tmpreg1 = (temp_pal[palreg] & 0xFF) + ((2 * pp.FadeAmt) + 4);
                tmpreg2 = (temp_pal[palreg] & 0xFF) - ((2 * pp.FadeAmt) + 4);
                if (tmpreg1 > 255) {
                    tmpreg1 = 255;
                }
                if (tmpreg2 < 0) {
                    tmpreg2 = 0;
                }

                if (usereg == 0) {
                    if ((temp_pal[palreg] & 0xFF) < red) {
                        if (((temp_pal[palreg] = (byte) tmpreg1) & 0xFF) > red) {
                            temp_pal[palreg] = (byte) red;
                        }
                    } else if ((temp_pal[palreg] & 0xFF) > red) {
                        if (((temp_pal[palreg] = (byte) tmpreg2) & 0xFF) < red) {
                            temp_pal[palreg] = (byte) red;
                        }
                    }
                } else if (usereg == 1) {
                    if ((temp_pal[palreg] & 0xFF) < green) {
                        if (((temp_pal[palreg] = (byte) tmpreg1) & 0xFF) > green) {
                            temp_pal[palreg] = (byte) green;
                        }
                    } else if ((temp_pal[palreg] & 0xFF) > green) {
                        if (((temp_pal[palreg] = (byte) tmpreg2) & 0xFF) < green) {
                            temp_pal[palreg] = (byte) green;
                        }
                    }
                } else if (usereg == 2) {
                    if ((temp_pal[palreg] & 0xFF) < blue) {
                        if (((temp_pal[palreg] = (byte) tmpreg1) & 0xFF) > blue) {
                            temp_pal[palreg] = (byte) blue;
                        }
                    } else if ((temp_pal[palreg] & 0xFF) > blue) {
                        if (((temp_pal[palreg] = (byte) tmpreg2) & 0xFF) < blue) {
                            temp_pal[palreg] = (byte) blue;
                        }
                    }
                }

                if (++usereg > 2) {
                    usereg = 0;
                }
            }

            WANG_SCREEN_FADE.set(red, green, blue, pp.FadeAmt);
            WANG_SCREEN_FADE.setPalette(temp_pal);

            if (startcolor == 210 || startcolor == 148 || startcolor == FORCERESET) {
                PaletteManager paletteManager = engine.getPaletteManager();
                engine.setbrightness(paletteManager.getPaletteGamma(), temp_pal);

                if (game.currentDef != null) {
                    TextureHDInfo hdInfo = game.currentDef.texInfo;

                    if (startcolor == 210) // underwater
                        hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 200, 240, 255, 0);
                    else if (startcolor == 148) // nightvision
                        hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 230, 255, 150, 0);
                    else
                        hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 255, 255, 255, 0);
                }
            }

            if (damage < -1000) {
                pp.FadeAmt = 1000; // Don't call DoPaletteFlash for underwater stuff
            }
        }
    }

    public static void DoPaletteFlash(PlayerStr pp) {
        if (pp.FadeAmt <= 1) {
            pp.FadeAmt = 0;
            pp.StartColor = 0;
            if (pp == Player[screenpeek]) {
                ResetPalette(pp, FORCERESET); // screenpeek
                DoPlayerDivePalette(pp); // Check Dive again
                DoPlayerNightVisionPalette(pp); // Check Night Vision again
            }
            return;
        }

        pp.FadeTics += synctics; // Add this frame's tic amount to counter
        if (pp.FadeTics >= MAXFADETICS) {
            while (pp.FadeTics >= MAXFADETICS) {
                pp.FadeTics -= MAXFADETICS;
                pp.FadeAmt--; // Decrement FadeAmt till it gets to 0 again.
            }
        } else {
            return; // Return if they were not >
        }

        if (pp.FadeAmt > 32) {
            return;
        }

        if (pp.FadeAmt <= 1) {
            pp.FadeAmt = 0;
            pp.StartColor = 0;
            if (pp == Player[screenpeek]) {
                ResetPalette(pp, FORCERESET); // screenpeek
                DoPlayerDivePalette(pp); // Check Dive again
                DoPlayerNightVisionPalette(pp); // Check Night Vision again
            }
        } else {
            if (pp == Player[screenpeek]) {
                byte[] palette = engine.getPaletteManager().getBasePalette();
                byte[] temp_pal = WANG_SCREEN_FADE.getPalette();

                for (int palreg = 0; palreg < 768; palreg++) {
                    int palcol = temp_pal[palreg] & 0xFF;
                    int tmpreg1 = Math.min(255, palcol + 2);
                    int tmpreg2 = Math.max(0, palcol - 2);

                    final int baseCol = palette[palreg] & 0xFF;
                    if (palcol < baseCol) {
                        temp_pal[palreg] = (byte) tmpreg1;
                        if ((temp_pal[palreg] & 0xFF) > baseCol) {
                            temp_pal[palreg] = palette[palreg];
                        }
                    } else if (palcol > baseCol) {
                        temp_pal[palreg] = (byte) tmpreg2;
                        if ((temp_pal[palreg] & 0xFF) < baseCol) {
                            temp_pal[palreg] = palette[palreg];
                        }
                    }
                }

                int startColor = pp.StartColor & 0xFF;
                WANG_SCREEN_FADE.set(
                        temp_pal[3 * startColor],
                        temp_pal[3 * startColor + 1],
                        temp_pal[3 * startColor + 2],
                        pp.FadeAmt);
                WANG_SCREEN_FADE.setPalette(temp_pal);
            }
        }
    }

    public static void DoPlayerDivePalette(PlayerStr pp) {
        if (pp != Player[screenpeek]) {
            return;
        }

        if ((pp.DeathType == PLAYER_DEATH_DROWN || TEST((Player[screenpeek]).Flags, PF_DIVING))
                && !TEST(pp.Flags, PF_DIVING_IN_LAVA)) {
            SetFadeAmt(pp, -1005, 210); // Dive color , org color 208
        } else {
            // Put it all back to normal
            if ((pp.StartColor & 0xFF) == 210) {
                ResetPalette(pp, FORCERESET); // Back to normal
                pp.FadeAmt = 0;
            }
        }
    }

    public static void DoPlayerNightVisionPalette(PlayerStr pp) {
        if (pp != Player[screenpeek]) {
            return;
        }

        if (pp.InventoryActive[INVENTORY_NIGHT_VISION]) {
            SetFadeAmt(pp, -1005, 148); // Night vision green tint
            pp.NightVision = true;
        } else {
            // Put it all back to normal
            if ((pp.StartColor & 0xFF) == 148) {
                ResetPalette(pp, FORCERESET); // Back to normal
                pp.FadeAmt = 0;
            }
            pp.NightVision = false;
        }
    }

    public static void ResetPalette(PlayerStr pp, int color) {
        pp.StartColor = 0;
        if (game.currentDef != null) {
            TextureHDInfo hdInfo = game.currentDef.texInfo;
            hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 255, 255, 255, 0);
        }

        PaletteManager paletteManager = engine.getPaletteManager();
        WANG_SCREEN_FADE.set(0, 0, 0, 0);
        WANG_SCREEN_FADE.setPalette(paletteManager.getBasePalette());
        if (color == 210 || color == 148 || color == FORCERESET) {
            engine.setbrightness(paletteManager.getPaletteGamma(), paletteManager.getBasePalette());
        }
    }
}
