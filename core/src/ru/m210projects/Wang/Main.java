package ru.m210projects.Wang;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.ByteArray;
import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.LogSender;
import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Pattern.ScreenAdapters.GameAdapter;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Wang.Factory.*;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.Factory.rts.RTSFile;
import ru.m210projects.Wang.Menus.Network.MenuMultiplayer;
import ru.m210projects.Wang.Screens.*;
import ru.m210projects.Wang.Type.VoxelScript;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.NORMAL;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Wang.Enemies.Bunny.InitBunnyStates;
import static ru.m210projects.Wang.Enemies.Coolg.InitCoolgStates;
import static ru.m210projects.Wang.Enemies.Coolie.InitCoolieStates;
import static ru.m210projects.Wang.Enemies.Eel.InitEelStates;
import static ru.m210projects.Wang.Enemies.GirlNinj.InitGNinjaStates;
import static ru.m210projects.Wang.Enemies.Goro.InitGoroStates;
import static ru.m210projects.Wang.Enemies.Hornet.InitHornetStates;
import static ru.m210projects.Wang.Enemies.Lava.InitLavaStates;
import static ru.m210projects.Wang.Enemies.Ninja.InitNinjaStates;
import static ru.m210projects.Wang.Enemies.Ripper.InitRipperStates;
import static ru.m210projects.Wang.Enemies.Ripper2.InitRipper2States;
import static ru.m210projects.Wang.Enemies.Serp.InitSerpStates;
import static ru.m210projects.Wang.Enemies.Skel.InitSkelStates;
import static ru.m210projects.Wang.Enemies.Skull.InitSkullStates;
import static ru.m210projects.Wang.Enemies.Sumo.InitSumoStates;
import static ru.m210projects.Wang.Enemies.Zilla.InitZillaStates;
import static ru.m210projects.Wang.Enemies.Zombie.InitZombieStates;
import static ru.m210projects.Wang.Factory.WangMenuHandler.MAIN;
import static ru.m210projects.Wang.Factory.WangMenuHandler.MULTIPLAYER;
import static ru.m210projects.Wang.Factory.WangNetwork.CommPlayers;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.JSector.InitJSectorStructs;
import static ru.m210projects.Wang.JWeapon.InitJWeaponStates;
import static ru.m210projects.Wang.LoadSave.FindSaves;
import static ru.m210projects.Wang.MiscActr.InitMiscStates;
import static ru.m210projects.Wang.Player.InitPlayerStates;
import static ru.m210projects.Wang.Sector.InitSectorStructs;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Type.ResourceHandler.resetEpisodeResources;
import static ru.m210projects.Wang.Type.ResourceHandler.usecustomarts;
import static ru.m210projects.Wang.Type.Saveable.InitSaveable;
import static ru.m210projects.Wang.Weapon.InitWeaponStates;

public class Main extends BuildGame {

    /*
     * TODO:
     * turn off the Lo Wang dialogue option
     * increase vehicle turn speed with joystick and mouse
     * demo sreenpeek
     * pause affect to demo
     * sync demo (and MP)
     * slopetilt affect
     *
     *
     * лифт не опускает трупы
     */

    public static final String appdef = "swgdx.def";
    public static final boolean NETTEST = false;
//    public static boolean AUTOCOMPARE = false;
    public static Main game;
    public static WangEngine engine;
    public static WangBoardService boardService;
    public static Config cfg;
    public static WangNetwork gNet;
    public static LoadingScreen gLoadingScreen;
    public static GameScreen gGameScreen;
    public static DemoScreen gDemoScreen;
    public static LogoAScreen gLogoScreen;
    public static AnmScreen gAnmScreen;
    public static MenuScreen gMenuScreen;
    public static StatisticScreen gStatisticScreen;
    public static NetScreen gNetScreen;
    public static DisconnectScreen gDisconnectScreen;
    public static CreditsScreen gCreditsScreen;
    public static UserFlag mUserFlag = UserFlag.None;
    public static RTSFile RTS_File;
    public WangMenuHandler menu;
    public WangNetwork net;
    public final String mainGrp = "sw.grp";
    public WangInterpolation pIntSkip2;
    public WangInterpolation pIntSkip4;

    public final Runnable rMenu = new Runnable() {
        @Override
        public void run() {
            if (game.getScreen() != gAnmScreen || gAnmScreen.getAnim() != ANIM_INTRO) {
                StopFX();
            }
            CDAudio_Play(2, true);
            Level = 0;
            nNetMode = NetMode.Single;
            CommPlayers = 1;
            gNet.MultiGameType = MultiGameTypes.MULTI_GAME_NONE;
            gNet.FakeMultiplayer = false;

            if (!menu.gShowMenu) {
                menu.mOpen(menu.mMenus[MAIN], -1);
            }

            Directory gameDir = cache.getGameDirectory();
            if (numplayers > 1 || gDemoScreen.demofiles.isEmpty() || cfg.gDemoSeq == 0 || !gDemoScreen.showDemo(gameDir)) {
                changeScreen(gMenuScreen);
            }
        }
    };

    public Main(List<String> args, GameConfig bcfg, String name, String version, boolean isRelease) throws IOException {
        super(args, bcfg, name, version, isRelease);
        game = this;
        cfg = (Config) bcfg;

        pIntSkip2 = new WangInterpolation() {
            @Override
            public int getSkipValue() {
                return MoveSkip2 ? 1 : 0;
            }

            @Override
            public int getSkipMax() {
                return 2;
            }
        };

        pIntSkip4 = new WangInterpolation() {
            @Override
            public int getSkipValue() {
                return MoveSkip4;
            }

            @Override
            public int getSkipMax() {
                return 4;
            }
        };
    }

    @Override
    protected MessageScreen createMessage(String header, String text, MessageType type) {
        return new WangMessageScreen(this, header, text, type);
    }

    @Override
    public GameProcessor createGameProcessor() {
        return new WangProcessor(this);
    }

    public WangProcessor getProcessor() {
        return (WangProcessor) controller;
    }

    @Override
    public BuildFactory getFactory() {
        return new WangFactory(this);
    }

    @Override
    public boolean init() throws Exception {
        boardService = engine.getBoardService();

        InitGame();

        InitPlayerStates();
        InitWeaponStates();
        InitJWeaponStates();
        InitSprStates();
        InitNinjaStates();
        InitZombieStates();
        InitRipper2States();
        InitBunnyStates();
        InitGoroStates();
        InitRipperStates();
        InitMiscStates();
        InitSerpStates();
        InitSumoStates();
        InitZillaStates();
        InitGNinjaStates();
        InitCoolgStates();
        InitCoolieStates();
        InitSkullStates();
        InitSkelStates();
        InitLavaStates();
        InitEelStates();
        InitHornetStates();

        InitJSectorStructs();
        InitSectorStructs();

        InitSaveable();

        Directory gameDir = cache.getGameDirectory();
        Console.out.println("Initializing def-scripts...");
        cache.loadGdxDef(baseDef, appdef, "swgdx.dat");

        if (cfg.isAutoloadFolder()) {
            Console.out.println("Initializing autoload folder");
            for (Entry file : gameDir.getDirectory(gameDir.getEntry("autoload"))) {
                switch (file.getExtension()) {
                    case "PK3":
                    case "ZIP": {
                        Group group = cache.newGroup(file);
                        Entry def = group.getEntry(appdef);
                        if (def.exists()) {
                            cache.addGroup(group, NORMAL); // HIGH?
                            baseDef.loadScript(file.getName(), def);
                        }
                    }
                    break;
                    case "DEF":
                        baseDef.loadScript(file);
                        break;
                }
            }
        }

        FileEntry filgdx = gameDir.getEntry(appdef);
        if (filgdx.exists()) {
            baseDef.loadScript(filgdx);
        }

        Entry kvxres = cache.getEntry("swvoxfil.txt", true);
        if (kvxres.exists()) {
            VoxelScript vox = new VoxelScript(kvxres);
            vox.apply(baseDef);
        }

        this.setDefs(baseDef);

        FindSaves(getUserDirectory());
        InitCDtracks();

        game.pNet.ResetTimers();

        gLoadingScreen = new LoadingScreen(this);
        gGameScreen = new GameScreen(this);
        gDemoScreen = new DemoScreen(this);
        gLogoScreen = new LogoAScreen(this, 5.0f);
        gAnmScreen = new AnmScreen(this);
        gMenuScreen = new MenuScreen(this);
        gStatisticScreen = new StatisticScreen(this);
        gNetScreen = new NetScreen(this);
        gDisconnectScreen = new DisconnectScreen(this);
        gCreditsScreen = new CreditsScreen(this);

        gDemoScreen.checkDemoEntry(gameDir);

        return true;
    }

    public void MainMenu() {
        rMenu.run();
    }

    @Override
    public void show() {
        if (!args.isEmpty()) {
            // Handle arguments at the first launch. args should be clear after handle
            parseArgumentsCommon();
            String netmode = args.getOrDefault("-netmode", "");
            String players = args.getOrDefault("-players", "");
            args.clear();

//            if (!file.isEmpty() && netmode.isEmpty()) {
//                onFilesDropped(new String[] {file});
//                return;
//            }

            if (!netmode.isEmpty()) {
                Console.out.println("Starting multiplayer as " + netmode, OsdColor.YELLOW);
                if (netmode.equalsIgnoreCase("master")) {
                    String[] param = { "-n0:" + (players.isEmpty() ? 2 : players), "-p " + cfg.getPort() };
                    ((MenuMultiplayer) menu.mMenus[MULTIPLAYER]).getMenuCreate(this).createGame(0, false, param);
                    return;
                } else if (netmode.equalsIgnoreCase("slave")) {
                    String[] param = new String[]{ "-n0", cfg.getmAddress(), "-p " + cfg.getPort() };
                    ((MenuMultiplayer) menu.mMenus[MULTIPLAYER]).getMenuJoin(this).joinGame(param);
                    return;
                }
            }
        }

        if (rec != null) {
            rec.close();
        }

        if (usecustomarts) {
            resetEpisodeResources();
        }

        changeScreen(gLogoScreen.setCallback(() -> {
            if (gAnmScreen.init(0)) {
                changeScreen(gAnmScreen.setCallback(rMenu).escSkipping(false));
            } else {
                MainMenu();
            }
        }));
    }

    @Override
    public void onDropEntry(FileEntry entry) {
        if (!entry.isExtension("map")) {
            return;
        }

        Console.out.println("Start dropped map: " + entry.getName());
        gGameScreen.newgame(false, entry, 0, 0, Skill);
    }


    @Override
    public DefaultPrecacheScreen getPrecacheScreen(Runnable readyCallback, PrecacheListener listener) {
        return new PrecacheScreen(readyCallback, listener);
    }

    public void Disconnect() {
        if (rec != null) {
            rec.close();
        }

        changeScreen(gDisconnectScreen);
    }

    @Override
    public void dispose() {
        if (rec != null) {
            rec.close();
        }
        super.dispose();
    }

    @Override
    public LogSender getLogSender() {
        return new LogSender(this) {
            @Override
            public byte[] reportData() {
                byte[] out;

                String text = "Mapname: " + gGameScreen.getTitle();
                text += "\r\n";
                text += "UserFlag: " + mUserFlag;
                text += "\r\n";

                if (mUserFlag == UserFlag.Addon && currentGame != null) {
                    try {
                        text += "Episode filename: " + currentGame.getPath() + "\r\n";
                        text += "Episode title: " + currentGame.Title + "\r\n";
                        text += "Episode name: " + currentGame.getNumEpisode(Level) + "\r\n";

                        text += "\r\n";
                    } catch (Exception e) {
                        text += "Episode filename get error \r\n";
                    }
                }

                text += "level " + Level;
                text += "\r\n";
                text += "nDifficulty: " + Skill;
                text += "\r\n";

                if (Player[myconnectindex] != null) {
                    text += "PlayerX: " + Player[myconnectindex].posx;
                    text += "\r\n";
                    text += "PlayerY: " + Player[myconnectindex].posy;
                    text += "\r\n";
                    text += "PlayerZ: " + Player[myconnectindex].posz;
                    text += "\r\n";
                    text += "PlayerAng: " + Player[myconnectindex].getAnglef();
                    text += "\r\n";
                    text += "PlayerHoriz: " + Player[myconnectindex].horiz;
                    text += "\r\n";
                    text += "PlayerSect: " + Player[myconnectindex].cursectnum;
                    text += "\r\n";
                }

                if (mUserFlag == UserFlag.UserMap && boardfilename != null) {
                    ByteArray array = new ByteArray();
                    byte[] data = boardfilename.getBytes();

                    text += "\r\n<------Start Map data------->\r\n";
                    array.addAll(text.getBytes());
                    array.addAll(data);
                    array.addAll("\r\n<------End Map data------->\r\n".getBytes());

                    out = Arrays.copyOf(array.items, array.size);
                } else {
                    out = text.getBytes();
                }

                return out;
            }
        };
    }

    public boolean isScreenSaving() {
        Screen scr = this.getScreen();
        if (scr instanceof GameAdapter) {
            return ((GameAdapter) scr).isScreenSaving();
        }
        return false;
    }

    public enum UserFlag {
        None, UserMap, Addon;

        public static UserFlag parseValue(byte index) {
            switch (index) {
                case 1:
                    return Addon;
                case 2:
                    return UserMap;
                default:
                    return None;
            }
        }
    }
}
