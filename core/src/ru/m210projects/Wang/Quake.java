package ru.m210projects.Wang;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.LONGp;
import ru.m210projects.Wang.Type.PlayerStr;

import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Wang.Digi.DIGI_ERUPTION;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Names.STAT_QUAKE_ON;
import static ru.m210projects.Wang.Names.STAT_QUAKE_SPOT;
import static ru.m210projects.Wang.Rooms.COVERinsertsprite;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.KillSprite;
import static ru.m210projects.Wang.Sprites.change_sprite_stat;

public class Quake {

    public static int QUAKE_Match(Sprite sp) {
        return (SP_TAG2(sp));
    }

    public static int QUAKE_Zamt(Sprite sp) {
        return (SP_TAG3(sp));
    }

    public static int QUAKE_Radius(Sprite sp) {
        return (SP_TAG4(sp));
    }

    public static int QUAKE_Duration(Sprite sp) {
        return (SP_TAG5(sp));
    }

    public static int QUAKE_WaitSecs(Sprite sp) {
        return (SP_TAG6(sp));
    }

    public static int QUAKE_AngAmt(Sprite sp) {
        return (SP_TAG7(sp));
    }

    public static int QUAKE_PosAmt(Sprite sp) {
        return (SP_TAG8(sp));
    }

    public static int QUAKE_RandomTest(Sprite sp) {
        return (SP_TAG9(sp));
    }

    public static int QUAKE_WaitTics(Sprite sp) {
        return (SP_TAG13(sp));
    }

    public static boolean QUAKE_TestDontTaper(Sprite sp) {
        return (TEST_BOOL1(sp));
    }

    public static boolean QUAKE_KillAfterQuake(Sprite sp) {
        return (TEST_BOOL2(sp));
    }

    // only for timed quakes
    public static boolean QUAKE_WaitForTrigger(Sprite sp) {
        return (TEST_BOOL3(sp));
    }

    public static void CopyQuakeSpotToOn(Sprite sp) {
        int newsp = COVERinsertsprite(sp.getSectnum(), STAT_QUAKE_SPOT);
        Sprite np = boardService.getSprite(newsp);
        if (np == null) {
            return;
        }

        np.set(sp);

        np.setCstat(0);
        np.setExtra(0);
        np.setOwner(-1);

        change_sprite_stat(newsp, STAT_QUAKE_ON);

        np.setXvel(np.getXvel() * 120); // QUAKE_Duration(np) *= 120;
    }

    public static void DoQuakeMatch(int match) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_QUAKE_SPOT); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            if (QUAKE_Match(sp) == match) {
                if (QUAKE_WaitTics(sp) > 0) {
                    // its not waiting any more
                    RESET_BOOL3(sp);
                } else {
                    CopyQuakeSpotToOn(sp);
                    if (QUAKE_KillAfterQuake(sp)) {
                        KillSprite(node.getIndex());
                    }
                }
            }
        }
    }

    public static void ProcessQuakeOn() {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_QUAKE_ON); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            // get rid of quake when timer runs out
            sp.setXvel(sp.getXvel() - synctics * 4); // QUAKE_Duration(sp) -= synctics*4;
            if (QUAKE_Duration(sp) < 0) {
                KillSprite(node.getIndex());
            }
        }
    }

    public static void ProcessQuakeSpot() {
        ListNode<Sprite> nexti;

        // check timed quakes and random quakes
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_QUAKE_SPOT); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            // not a timed quake
            if (QUAKE_WaitSecs(sp) == 0) {
                continue;
            }

            // don't process unless triggered
            if (QUAKE_WaitForTrigger(sp)) {
                continue;
            }

            // spawn a quake if time is up
            SET_SP_TAG13(sp, (QUAKE_WaitTics(sp) - 4 * synctics));
            if (QUAKE_WaitTics(sp) < 0) {
                // reset timer - add in Duration of quake
                SET_SP_TAG13(sp, (((QUAKE_WaitSecs(sp) * 10) + QUAKE_Duration(sp)) * 120));

                // spawn a quake if condition is met
                int rand_test = QUAKE_RandomTest(sp);
                // wrong - all quakes need to happen at the same time on all computerssg
                // if (!rand_test || (rand_test && STD_RANDOM_RANGE(128) < rand_test))
                if (rand_test == 0 || RANDOM_RANGE(128) < rand_test) {
                    CopyQuakeSpotToOn(sp);
                    // kill quake spot if needed
                    if (QUAKE_KillAfterQuake(sp)) {
                        int i = node.getIndex();
                        DeleteNoSoundOwner(i);
                        KillSprite(i);
                    }
                }
            }
        }
    }

    public static void QuakeViewChange(PlayerStr pp, LONGp z_diff, LONGp x_diff, LONGp y_diff, LONGp ang_diff) {
        ListNode<Sprite> nexti;
        Sprite save_sp = null;
        int save_dist = 999999;

        z_diff.value = 0;
        x_diff.value = 0;
        y_diff.value = 0;
        ang_diff.value = 0;

        if (game.gPaused) {
            return;
        }

        // find the closest quake - should be a strength value
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_QUAKE_ON); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            int dist = FindDistance3D(pp.posx - sp.getX(), pp.posy - sp.getY(), (pp.posz - sp.getZ()) >> 4);

            // shake whole level
            if (QUAKE_TestDontTaper(sp)) {
                save_dist = dist;
                save_sp = sp;
                break;
            }

            if (dist < save_dist) {
                save_dist = dist;
                save_sp = sp;
            }
        }

        if (save_sp == null) {
            return;
        }

        int radius = QUAKE_Radius(save_sp) * 8;
        if (save_dist > radius) {
            return;
        }

        z_diff.value = Z(STD_RANDOM_RANGE(QUAKE_Zamt(save_sp)) - (QUAKE_Zamt(save_sp) / 2));

        int ang_amt = QUAKE_AngAmt(save_sp) * 4;
        ang_diff.value = STD_RANDOM_RANGE(ang_amt) - (ang_amt / 2);

        int pos_amt = QUAKE_PosAmt(save_sp) * 4;
        x_diff.value = STD_RANDOM_RANGE(pos_amt) - (pos_amt / 2);
        y_diff.value = STD_RANDOM_RANGE(pos_amt) - (pos_amt / 2);

        if (!QUAKE_TestDontTaper(save_sp)) {
            // scale values from epicenter
            int dist_diff = radius - save_dist;
            int scale_value = divscale(dist_diff, radius, 16);

            z_diff.value = mulscale(z_diff.value, scale_value, 16);
            ang_diff.value = mulscale(ang_diff.value, scale_value, 16);
            x_diff.value = mulscale(x_diff.value, scale_value, 16);
            y_diff.value = mulscale(y_diff.value, scale_value, 16);
        }
    }

    public static void SpawnQuake(int sectnum, int x, int y, int z, int tics, int amt, int radius) {
        int SpriteNum = COVERinsertsprite(sectnum, STAT_QUAKE_ON);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        sp.setX(x);
        sp.setY(y);
        sp.setZ(z);
        sp.setCstat(0);
        sp.setOwner(-1);
        sp.setExtra(0);

        sp.setLotag(-1);
        sp.setClipdist(amt);
        sp.setAng( (radius / 8));
        sp.setXvel( tics);
        sp.setZvel(8);

        Set3DSoundOwner(SpriteNum, PlaySound(DIGI_ERUPTION, sp, v3df_follow | v3df_dontpan));

    }

    public static void SetExpQuake(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp != null) {
            SpawnQuake(sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), 40, 4, 20000); // !JIM! was 8, 40000
        }
    }

    public static void SetGunQuake(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null) {
            SpawnQuake(sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), 40, 8, 40000);
        }
    }

    public static void SetPlayerQuake(PlayerStr pp) {
        SpawnQuake(pp.cursectnum, pp.posx, pp.posy, pp.posz, 40, 8, 40000);
    }

    public static void SetNuclearQuake(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp != null) {
            SpawnQuake(sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), 400, 8, 64000);
        }
    }

    public static void SetSumoQuake(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null) {
            SpawnQuake(sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), 120, 4, 20000);
        }
    }

    public static void SetSumoFartQuake(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null) {
            SpawnQuake(sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), 60, 4, 4000);
        }
    }

}
