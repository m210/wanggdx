package ru.m210projects.Wang;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.LONGp;
import ru.m210projects.Wang.Type.Sector_Object;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Names.STAT_WALL_MOVE;
import static ru.m210projects.Wang.Names.STAT_WALL_MOVE_CANSEE;
import static ru.m210projects.Wang.Sprites.KillSprite;
import static ru.m210projects.Wang.Track.DetectSectorObjectByWall;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Weapon.PrevWall;

public class WallMove {

    private static final LONGp nx = new LONGp();
    private static final LONGp ny = new LONGp();

    public static void SOwallmove(Sector_Object sop, int sp, Wall find_WALL, int dist, LONGp nx, LONGp ny) {
        USER u = getUser(sp);

        if (u == null || TEST(sop.flags, SOBJ_SPRITE_OBJ)) {
            return;
        }

        int wallcount = 0;

        for (int j = 0; sop.sector[j] != -1; j++) {
            Sector sectp = boardService.getSector(sop.sector[j]);
            if (sectp == null) {
                continue;
            }

            // move all walls in sectors back to the original position
            for (ListNode<Wall> wn = sectp.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wp = wn.get();
                // find the one wall we want to adjust
                if (wp == find_WALL) {
                    int ang;
                    // move orig x and y in saved angle

                    ang = u.sang;

                    nx.value = ((dist * EngineUtils.sin(NORM_ANGLE(ang + 512))) >> 14);
                    ny.value = ((dist * EngineUtils.sin(ang)) >> 14);

                    sop.xorig[wallcount] -= nx.value;
                    sop.yorig[wallcount] -= ny.value;

                    sop.flags |= (SOBJ_UPDATE_ONCE);
                    return;
                }

                wallcount++;
            }
        }
    }

    public static void DoWallMove(int spnum) {
        Sprite sp = boardService.getSprite(spnum);
        if (sp == null) {
            return;
        }

        int dist = SP_TAG13(sp);
        int ang =  SP_TAG4(sp);
        int picnum1 =  SP_TAG5(sp);
        int picnum2 =  SP_TAG6(sp);
        int shade1 =  SP_TAG7(sp);
        int shade2 =  SP_TAG8(sp);
        int dang =  ((SP_TAG10(sp)) << 3);

        if (dang != 0) {
            ang = NORM_ANGLE(ang + (RANDOM_RANGE(dang) - dang / 2));
        }

        nx.value = (dist * EngineUtils.sin(NORM_ANGLE(ang + 512))) >> 14;
        ny.value = (dist * EngineUtils.sin(ang)) >> 14;

        boolean SOsprite = false;
        Wall[] walls = boardService.getBoard().getWalls();
        for (int w = 0; w < walls.length; w++) {
            Wall WALL = walls[w];
            if (WALL.getX() == sp.getX() && WALL.getY() == sp.getY()) {

                if (TEST(WALL.getExtra(), WALLFX_SECTOR_OBJECT)) {
                    Sector_Object sop;
                    sop = DetectSectorObjectByWall(WALL);
                    SOwallmove(sop, spnum, WALL, dist, nx, ny);

                    SOsprite = true;
                } else {
                    WALL.setX(sp.getX() + nx.value);
                    WALL.setY(sp.getY() + ny.value);
                }

                if (shade1 != 0) {
                    WALL.setShade((byte) shade1);
                }
                if (picnum1 != 0) {
                    WALL.setPicnum(picnum1);
                }

                // find the previous wall
                int prev_wall = PrevWall(w);
                Wall pw = boardService.getWall(prev_wall);
                if (pw != null) {
                    if (shade2 != 0) {
                        pw.setShade((byte) shade2);
                    }

                    if (picnum2 != 0) {
                        pw.setPicnum(picnum2);
                    }
                }
            }
        }

        SET_SP_TAG9(sp, SP_TAG9(sp) - 1);
        if ((byte) SP_TAG9(sp) <= 0) {
            KillSprite(spnum);
        } else {
            USER u = getUser(spnum);
            if (u != null && SOsprite) {
                // move the sprite offset from center
                u.sx -= nx.value;
                u.sy -= ny.value;
            } else {
                sp.setX(sp.getX() + nx.value);
                sp.setY(sp.getY() + ny.value);
            }
        }

    }

    public static boolean CanSeeWallMove(Sprite wp, int match) {
        boolean found = false;

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_WALL_MOVE_CANSEE); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            if (SP_TAG2(sp) == match) {
                found = true;

                if (engine.cansee(wp.getX(), wp.getY(), wp.getZ(), wp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum())) {
                    return (true);
                }
            }
        }

        return !found;
    }

    public static boolean DoWallMoveMatch(int match) {
        boolean found = false;

        // just all with the same matching tags
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_WALL_MOVE); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();

            if (SP_TAG2(sp) == match) {
                found = true;
                DoWallMove(i);
            }
        }

        return (found);
    }

}
