package ru.m210projects.Wang;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.STAT_LIGHTING;
import static ru.m210projects.Wang.Names.STAT_LIGHTING_DIFFUSE;
import static ru.m210projects.Wang.Type.MyTypes.*;

public class Light {

    public static final int LIGHT_CONSTANT = 0;
    public static final int LIGHT_FLICKER = 1;
    public static final int LIGHT_FADE = 2;
    public static final int LIGHT_FLICKER_ON = 3;
    public static final int LIGHT_FADE_TO_ON_OFF = 4;

    public static int LIGHT_Match(Sprite sp) {
        return (SP_TAG2((sp)));
    }

    public static int LIGHT_Type(Sprite sp) {
        return (SP_TAG3((sp)));
    }

    public static int LIGHT_MaxTics(Sprite sp) {
        return (SP_TAG4((sp)));
    }

    public static int LIGHT_MaxBright(Sprite sp) {
        return (SP_TAG5((sp)));
    }

    public static int LIGHT_MaxDark(Sprite sp) {
        return (SP_TAG6((sp)));
    }

    public static int LIGHT_ShadeInc(Sprite sp) {
        return SP_TAG7(sp);
    }

    public static boolean LIGHT_Dir(Sprite sp) {
        return (TEST((sp).getExtra(), SPRX_BOOL10));
    }

    public static void LIGHT_DirChange(Sprite sp) {
        int extra = sp.getExtra() ^ SPRX_BOOL10;
        sp.setExtra(extra);
    }

    public static int LIGHT_FloorShade(Sprite sp) {
        return ((sp).getXoffset());
    }

    public static int LIGHT_CeilingShade(Sprite sp) {
        return ((sp).getYoffset());
    }

    public static int LIGHT_Tics(Sprite sp) {
        return ((sp).getZ());
    }

    public static int LIGHT_DiffuseNum(Sprite sp) {
        return (SP_TAG3((sp)));
    }

    public static int LIGHT_DiffuseMult(Sprite sp) {
        return (SP_TAG4((sp)));
    }

    public static void SectorLightShade(int spnum, int intensity) {
        Sprite sp = boardService.getSprite(spnum);
        if (sp == null) {
            return;
        }

        if (TEST_BOOL8(sp)) {
            intensity =  -intensity;
        }

        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec == null) {
            return;
        }

        if (!TEST_BOOL2(sp)) {
            if (!TEST_BOOL6(sp)) {
                sec.setFloorpal(sp.getPal());
            }
            sec.setFloorshade((byte) (LIGHT_FloorShade(sp) + intensity)); // floor change
        }

        if (!TEST_BOOL3(sp)) {
            if (!TEST_BOOL6(sp)) {
                sec.setCeilingpal(sp.getPal());
            }
            sec.setCeilingshade((byte) (LIGHT_CeilingShade(sp) + intensity)); // ceiling change
        }

        // change wall
        if (!TEST_BOOL4(sp)) {
            USER u = getUser(spnum);
            if (u != null) {
                byte[] wall_shade = u.WallShade;
                int wallcount = 0;
                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    if (wallcount >= wall_shade.length) {
                        break;
                    }

                    int base_shade = wall_shade[wallcount];
                    wal.setShade((byte) (base_shade + intensity));
                    if (!TEST_BOOL6(sp)) {
                        wal.setPal(sp.getPal());
                    }
                    wallcount++;

                    Wall nw = boardService.getWall(wal.getNextwall());
                    if (TEST(sp.getExtra(), SPRX_BOOL5) && nw != null) {
                        if (wallcount >= wall_shade.length) {
                            break;
                        }

                        base_shade = wall_shade[wallcount];
                        nw.setShade((byte) (base_shade + intensity));
                        if (!TEST_BOOL6(sp)) {
                            nw.setPal(sp.getPal());
                        }
                        wallcount++;
                    }
                }
            }
        }
    }

    public static void DiffuseLighting(Sprite sp) {
        // diffused lighting
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_LIGHTING_DIFFUSE); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite dsp = node.get();

            // make sure matchs match
            if (LIGHT_Match(dsp) != LIGHT_Match(sp)) {
                continue;
            }

            int shade =  (sp.getShade() + ((LIGHT_DiffuseNum(dsp) + 1) * LIGHT_DiffuseMult(dsp)));

            if (shade > LIGHT_MaxDark(sp)) {
                shade =  LIGHT_MaxDark(sp);
            }

            if (!TEST_BOOL6(dsp)) {
                dsp.setPal(sp.getPal());
            }

            SectorLightShade(node.getIndex(), shade);
        }
    }

    public static void DoLightingMatch(int match, int state) {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_LIGHTING); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();

            Sprite sp = node.get();
            if (LIGHT_Match(sp) != match) {
                continue;
            }

            switch (LIGHT_Type(sp)) {
                case LIGHT_CONSTANT:

                    // initialized
                    SET_BOOL9(sp);

                    // toggle
                    if (state == -1) {
                        state = (!TEST_BOOL1(sp)) ? 1 : 0;
                    }

                    if (state == ON) {
                        SET_BOOL1(sp);
                        sp.setShade((byte) -LIGHT_MaxBright(sp));
                        USER u = getUser(i);
                        if (u != null) {
                            sp.setPal(u.spal); // on
                        }
                    } else {
                        RESET_BOOL1(sp);
                        sp.setShade((byte) LIGHT_MaxDark(sp));
                        sp.setPal(0); // off
                    }
                    SectorLightShade(i, sp.getShade());
                    DiffuseLighting(sp);
                    break;

                case LIGHT_FLICKER:
                case LIGHT_FADE:
                    // initialized
                    SET_BOOL9(sp);

                    // toggle
                    if (state == -1) {
                        state = (!TEST_BOOL1(sp)) ? 1 : 0;
                    }

                    if (state == ON) {
                        // allow fade or flicker
                        SET_BOOL1(sp);
                    } else {
                        RESET_BOOL1(sp);
                        sp.setShade((byte) LIGHT_MaxDark(sp));
                        SectorLightShade(i, sp.getShade());
                        DiffuseLighting(sp);
                    }
                    break;

                case LIGHT_FADE_TO_ON_OFF:

                    // initialized
                    SET_BOOL9(sp);

                    if (state == ON) {
                        if (LIGHT_Dir(sp)) {
                            LIGHT_DirChange(sp);
                        }
                    } else if (state == OFF) {
                        if (!LIGHT_Dir(sp)) {
                            LIGHT_DirChange(sp);
                        }
                    }

                    // allow fade or flicker
                    SET_BOOL1(sp);
                    break;

                case LIGHT_FLICKER_ON:

                    // initialized
                    SET_BOOL9(sp);

                    // toggle
                    if (state == -1) {
                        state = (!TEST_BOOL1(sp)) ? 1 : 0;
                    }

                    if (state == ON) {
                        // allow fade or flicker
                        SET_BOOL1(sp);
                    } else {
                        // turn it off till next switch
                        short spal = sp.getPal();
                        RESET_BOOL1(sp);
                        sp.setPal(0);
                        sp.setShade((byte) LIGHT_MaxDark(sp));
                        SectorLightShade(i, sp.getShade());
                        DiffuseLighting(sp);
                        sp.setPal(spal);
                    }
                    break;
            }
        }
    }

    public static void InitLighting() {
        // processed on level startup
        // puts lights in correct state
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_LIGHTING); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            if (!TEST_BOOL9(sp)) {
                DoLightingMatch(LIGHT_Match(sp), TEST_BOOL1(sp) ? 1 : 0);
            }
        }
    }

    public static void DoLighting() {
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_LIGHTING); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite sp = node.get();

            // on/off test
            if (!TEST_BOOL1(sp)) {
                continue;
            }

            switch (LIGHT_Type(sp)) {
                case LIGHT_CONSTANT:
                    break;

                case LIGHT_FLICKER:

                    sp.setZ(sp.getZ() + synctics);
                    while (LIGHT_Tics(sp) >= LIGHT_MaxTics(sp)) {
                        sp.setZ(sp.getZ() - LIGHT_MaxTics(sp));

                        if ((RANDOM_P2(128 << 8) >> 8) > 64) {
                            sp.setShade((byte) (-LIGHT_MaxBright(sp) + RANDOM_RANGE(LIGHT_MaxBright(sp) + LIGHT_MaxDark(sp))));
                            SectorLightShade(i, sp.getShade());
                            DiffuseLighting(sp);
                        } else {
                            // turn off lighting - even colored lighting
                            short spal = sp.getPal();
                            sp.setPal(0);
                            sp.setShade((byte) LIGHT_MaxDark(sp));
                            SectorLightShade(i, sp.getShade());
                            DiffuseLighting(sp);
                            sp.setPal(spal);
                        }
                    }

                    break;

                case LIGHT_FADE:

                    sp.setZ(sp.getZ() + synctics);

                    while (LIGHT_Tics(sp) >= LIGHT_MaxTics(sp)) {
                        sp.setZ(sp.getZ() - LIGHT_MaxTics(sp));

                        if (LIGHT_Dir(sp)) {
                            sp.setShade(sp.getShade() + LIGHT_ShadeInc(sp));
                            if (sp.getShade() >= LIGHT_MaxDark(sp)) {
                                LIGHT_DirChange(sp);
                            }
                        } else {
                            sp.setShade(sp.getShade() - LIGHT_ShadeInc(sp));
                            if (sp.getShade() <= -LIGHT_MaxBright(sp)) {
                                LIGHT_DirChange(sp);
                            }
                        }

                        SectorLightShade(i, sp.getShade());
                        DiffuseLighting(sp);
                    }

                    break;

                case LIGHT_FADE_TO_ON_OFF:

                    sp.setZ(sp.getZ() + synctics);

                    while (LIGHT_Tics(sp) >= LIGHT_MaxTics(sp)) {
                        sp.setZ(sp.getZ() - LIGHT_MaxTics(sp));

                        if (LIGHT_Dir(sp)) {
                            sp.setShade(sp.getShade() + LIGHT_ShadeInc(sp));
                            if (sp.getShade() >= LIGHT_MaxDark(sp)) {
                                sp.setPal(0); // off
                                LIGHT_DirChange(sp);
                                // stop it until switch is hit
                                RESET_BOOL1(sp);
                            }
                        } else {
                            sp.setShade(sp.getShade() - LIGHT_ShadeInc(sp));
                            USER u = getUser(i);
                            if (u != null) {
                                sp.setPal(u.spal); // on
                            }
                            if (sp.getShade() <= -LIGHT_MaxBright(sp)) {
                                LIGHT_DirChange(sp);
                                // stop it until switch is hit
                                RESET_BOOL1(sp);
                            }
                        }
                        SectorLightShade(i, sp.getShade());
                        DiffuseLighting(sp);
                    }

                    break;

                case LIGHT_FLICKER_ON:

                    sp.setZ(sp.getZ() + synctics);

                    while (LIGHT_Tics(sp) >= LIGHT_MaxTics(sp)) {
                        sp.setZ(sp.getZ() - LIGHT_MaxTics(sp));

                        if ((RANDOM_P2(128 << 8) >> 8) > 64) {
                            sp.setShade((byte) (-LIGHT_MaxBright(sp) + RANDOM_RANGE(LIGHT_MaxBright(sp) + LIGHT_MaxDark(sp))));
                            SectorLightShade(i, sp.getShade());
                            DiffuseLighting(sp);
                        } else {
                            // turn off lighting - even colored lighting
                            short spal = sp.getPal();
                            sp.setPal(0);
                            sp.setShade((byte) LIGHT_MaxDark(sp));
                            SectorLightShade(i, sp.getShade());
                            DiffuseLighting(sp);
                            sp.setPal(spal);
                        }

                        if ((RANDOM_P2(128 << 8) >> 8) < 8) {
                            // set to full brightness
                            sp.setShade((byte) -LIGHT_MaxBright(sp));
                            SectorLightShade(i, sp.getShade());
                            DiffuseLighting(sp);
                            // turn it off until a swith happens
                            RESET_BOOL1(sp);
                        }
                    }
                    break;
            }
        }
    }

}
