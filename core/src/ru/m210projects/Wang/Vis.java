package ru.m210projects.Wang;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.LONGp;
import ru.m210projects.Wang.Type.PlayerStr;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.game;
import static ru.m210projects.Wang.Names.STAT_VIS_ON;
import static ru.m210projects.Wang.Palette.PALETTE_DIVE_LAVA;
import static ru.m210projects.Wang.Palette.PALETTE_FOG;
import static ru.m210projects.Wang.Player.NormalVisibility;
import static ru.m210projects.Wang.Rooms.COVERinsertsprite;
import static ru.m210projects.Wang.Rooms.FAFcansee;
import static ru.m210projects.Wang.Sprites.KillSprite;

public class Vis {

    private static int VIS_VisCur(Sprite sp) {
        return (SP_TAG2(sp));
    }

    private static int VIS_VisDir(Sprite sp) {
        return (SP_TAG3(sp));
    }

    private static int VIS_VisGoal(Sprite sp) {
        return (SP_TAG4(sp));
    }

    public static void ProcessVisOn() {
        Sprite sp;

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_VIS_ON); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            sp = node.get();

            if (VIS_VisDir(sp) != 0) {
                // get brighter
                sp.setLotag(sp.getLotag() >> 1);
                if (VIS_VisCur(sp) <= VIS_VisGoal(sp)) {
                    sp.setLotag( VIS_VisGoal(sp));
                    sp.setClipdist(sp.getClipdist() ^ 1);
                }
            } else {
                // get darker
                sp.setLotag(sp.getLotag() << 1);
                sp.setLotag(sp.getLotag() + 1);

                if (VIS_VisCur(sp) >= NormalVisibility) {
                    sp.setLotag(NormalVisibility);
                    if (sp.getOwner() != -1) {
                        USER u = getUser(sp.getOwner());
                        if (u != null) {
                            u.Flags2 &= ~(SPR2_VIS_SHADING);
                        }
                    }
                    KillSprite(i);
                }
            }
        }
    }

    public static void VisViewChange(PlayerStr pp, LONGp vis) {
        int BrightestVis = NormalVisibility;
        if (game.gPaused) {
            return;
        }

        // find the closest quake - should be a strength value
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_VIS_ON); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite sp = node.get();

            int sectnum;
            int x, y, z;
            Sprite owner = boardService.getSprite(sp.getOwner());
            if (owner != null) {
                x = owner.getX();
                y = owner.getY();
                z = owner.getZ();
                sectnum = owner.getSectnum();
            } else {
                x = sp.getX();
                y = sp.getY();
                z = sp.getZ();
                sectnum = sp.getSectnum();
            }

            // save off the brightest vis that you can see
            if (FAFcansee(pp.posx, pp.posy, pp.posz, pp.cursectnum, x, y, z, sectnum)) {
                if (VIS_VisCur(sp) < BrightestVis) {
                    BrightestVis =  VIS_VisCur(sp);
                }
            }
        }
        vis.value = BrightestVis;
    }

    public static void SpawnVis(int Parent, int sectnum, int x, int y, int z, int amt) {
        Sprite psp = boardService.getSprite(Parent);
        USER pu = getUser(Parent);
        final int SpriteNum;
        final Sprite sp;
        if (psp != null && pu != null) {
            Sector psec = boardService.getSector(psp.getSectnum());
            if (psec != null && (psec.getFloorpal() == PALETTE_FOG || psec.getFloorpal() == PALETTE_DIVE_LAVA)) {
                return;
            }

            // kill any others with the same parent
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_VIS_ON); node != null; node = nexti) {
                nexti = node.getNext();
                Sprite spr = node.get();
                if (spr.getOwner() == Parent) {
                    KillSprite(node.getIndex());
                }
            }

            SpriteNum = COVERinsertsprite(psp.getSectnum(), STAT_VIS_ON);
            sp = boardService.getSprite(SpriteNum);
            if (sp == null) {
                return;
            }

            sp.setOwner(Parent);
            sp.setX(psp.getX());
            sp.setY(psp.getY());
            sp.setZ(psp.getZ());

            pu.Flags2 |= (SPR2_CHILDREN);
            pu.Flags2 |= (SPR2_VIS_SHADING);
        } else {
            Sector sec = boardService.getSector(sectnum);
            if (sec != null && sec.getFloorpal() == PALETTE_FOG) {
                return;
            }

            SpriteNum = COVERinsertsprite(sectnum, STAT_VIS_ON);
            sp = boardService.getSprite(SpriteNum);
            if (sp == null) {
                return;
            }

            sp.setX(x);
            sp.setY(y);
            sp.setZ(z - Z(20));
            sp.setOwner(-1);
        }

        sp.setCstat(0);
        sp.setExtra(0);

        sp.setClipdist(1);
        sp.setLotag(NormalVisibility);
        sp.setAng(amt);

    }
}
