package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.TAG_SWARMSPOT;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Sector.getSectUser;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.DoDamage;
import static ru.m210projects.Wang.Weapon.DoFindGroundPoint;

public class Hornet {

    private static final Animator InitHornetSting = new Animator((Animator.Runnable) Hornet::InitHornetSting);
    private static final Animator DoHornetMove = new Animator((Animator.Runnable) Hornet::DoHornetMove);
    private static final Animator DoHornetDeath = new Animator((Animator.Runnable) Hornet::DoHornetDeath);
    private static final Animator DoHornetCircle = new Animator((Animator.Runnable) Hornet::DoHornetCircle);
    private static final Animator InitHornetCircle = new Animator((Animator.Runnable) Hornet::InitHornetCircle);


    public static final int HORNET_RUN_RATE = 7;
    public static final int HORNET_STAND_RATE = (HORNET_RUN_RATE + 5);
    public static final int HORNET_DIE_RATE = 20;
    public static final int HORNET_BOB_AMT = (Z(16));


    private static final Decision[] HornetOffense = {new Decision(1022, InitActorMoveCloser),

            new Decision(1024, InitActorAlertNoise)};

    private static final Decision[] HornetBroadcast = {new Decision(3, InitActorAlertNoise),

            new Decision(6, InitActorAmbientNoise),
 new Decision(1024, InitActorDecide)};
    private static final Decision[] HornetLostTarget = {new Decision(900, InitActorFindPlayer),

            new Decision(1024, InitActorWanderAround)};
    private static final Decision[] HornetCloseRange = {new Decision(900, InitActorMoveCloser),

            new Decision(1024, InitActorReposition)};
    private static final ATTRIBUTE HornetAttrib = new ATTRIBUTE(new short[]{300, 350, 375, 400}, // Speeds
            new short[]{0, 0, 0, 0}, // Tic Adjusts
            0, // MaxWeapons;
            new int[]{0, 0, DIGI_HORNETSTING, DIGI_HORNETSTING, DIGI_HORNETDEATH, 0, 0, 0, 0, 0});

    private static final State[][] s_HornetRun = {{new State(HORNET_RUN_R0, HORNET_RUN_RATE, DoHornetMove),
 // s_HornetRun[0][1]},
            new State(HORNET_RUN_R0 + 1, HORNET_RUN_RATE, DoHornetMove),
// s_HornetRun[0][0]},
    }, {new State(HORNET_RUN_R1, HORNET_RUN_RATE, DoHornetMove),
 // s_HornetRun[1][1]},
            new State(HORNET_RUN_R1 + 1, HORNET_RUN_RATE, DoHornetMove),
// s_HornetRun[1][0]},
    }, {new State(HORNET_RUN_R2, HORNET_RUN_RATE, DoHornetMove),
 // s_HornetRun[2][1]},
            new State(HORNET_RUN_R2 + 1, HORNET_RUN_RATE, DoHornetMove),
// s_HornetRun[2][0]},
    }, {new State(HORNET_RUN_R3, HORNET_RUN_RATE, DoHornetMove),
 // s_HornetRun[3][1]},
            new State(HORNET_RUN_R3 + 1, HORNET_RUN_RATE, DoHornetMove),
// s_HornetRun[3][0]},
    }, {new State(HORNET_RUN_R4, HORNET_RUN_RATE, DoHornetMove),
 // s_HornetRun[4][1]},
            new State(HORNET_RUN_R4 + 1, HORNET_RUN_RATE, DoHornetMove),
// s_HornetRun[4][0]},
    }};
    private static final State[][] s_HornetStand = {{new State(HORNET_RUN_R0, HORNET_STAND_RATE, DoHornetMove),
 // s_HornetStand[0][1]},
            new State(HORNET_RUN_R0 + 1, HORNET_STAND_RATE, DoHornetMove),
// s_HornetStand[0][0]}
    }, {new State(HORNET_RUN_R1, HORNET_STAND_RATE, DoHornetMove),
 // s_HornetStand[1][1]},
            new State(HORNET_RUN_R1 + 1, HORNET_STAND_RATE, DoHornetMove),
// s_HornetStand[1][0]}
    }, {new State(HORNET_RUN_R2, HORNET_STAND_RATE, DoHornetMove),
 // s_HornetStand[2][1]},
            new State(HORNET_RUN_R2 + 1, HORNET_STAND_RATE, DoHornetMove),
// s_HornetStand[2][0]}
    }, {new State(HORNET_RUN_R3, HORNET_STAND_RATE, DoHornetMove),
 // s_HornetStand[3][1]},
            new State(HORNET_RUN_R3 + 1, HORNET_STAND_RATE, DoHornetMove),
// s_HornetStand[3][0]}
    }, {new State(HORNET_RUN_R4, HORNET_STAND_RATE, DoHornetMove),
 // s_HornetStand[4][1]},
            new State(HORNET_RUN_R4 + 1, HORNET_STAND_RATE, DoHornetMove),
// s_HornetStand[4][0]}
    }};

    //////////////////////
    //
    // HORNET RUN
    //////////////////////
    private static final State[] s_HornetDie = {new State(HORNET_DIE, HORNET_DIE_RATE, DoHornetDeath).setNext(),
// s_HornetDie[0]},
    };
    private static final State[] s_HornetDead = {new State(HORNET_DEAD, HORNET_DIE_RATE, DoActorDebris).setNext(),
// s_HornetDead[0]},
    };

    private static final EnemyStateGroup sg_HornetStand = new EnemyStateGroup(s_HornetStand[0], s_HornetStand[1], s_HornetStand[2], s_HornetStand[3], s_HornetStand[4]);
    private static final EnemyStateGroup sg_HornetRun = new EnemyStateGroup(s_HornetRun[0], s_HornetRun[1], s_HornetRun[2], s_HornetRun[3], s_HornetRun[4]);
    private static final EnemyStateGroup sg_HornetDie = new EnemyStateGroup(s_HornetDie);
    private static final EnemyStateGroup sg_HornetDead = new EnemyStateGroup(s_HornetDead);
    
    private static final Actor_Action_Set HornetActionSet = new Actor_Action_Set(sg_HornetStand,
            sg_HornetRun, null, null, null, null, null, null, null, null, null, // climb
            null, // pain
            sg_HornetDie, null, sg_HornetDead, null, null, null, new short[]{0},
            null, new short[]{0}, null, null, null);

    //////////////////////
    //
    // HORNET STAND
    //
    //////////////////////

    //////////////////////
    //
    // HORNET DIE
    //
    //////////////////////
    private static final Decision[] HornetBattle = {new Decision(50, InitHornetCircle),

            new Decision(798, InitActorMoveCloser),
 new Decision(800, InitActorAlertNoise),

            new Decision(1024, InitActorRunAway)};
    private static final Decision[] HornetSurprised = {new Decision(100, InitHornetCircle),

            new Decision(701, InitActorMoveCloser),
 new Decision(1024, InitActorDecide)};
    private static final Decision[] HornetEvasive = {new Decision(20, InitHornetCircle),
 new Decision(1024, null)};
    private static final Decision[] HornetTouchTarget = {new Decision(500, InitHornetCircle),

            new Decision(1024, InitHornetSting)};
    private static final Personality HornetPersonality = new Personality(HornetBattle, HornetOffense, HornetBroadcast,
            HornetSurprised, HornetEvasive, HornetLostTarget, HornetCloseRange, HornetTouchTarget);

    public static void InitHornetStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[] {
                sg_HornetStand,
                sg_HornetRun,
                sg_HornetDie,
                sg_HornetDead }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }

    public static void SetupHornet(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        USER u;

        if (TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, HORNET_RUN_R0, s_HornetRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_HORNET;
        }

        ChangeState(SpriteNum, s_HornetRun[0][0]);
        u.Attrib = HornetAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_HornetDie[0];
        u.setRot(sg_HornetRun);

        EnemyDefaults(SpriteNum, HornetActionSet, HornetPersonality);

        u.Flags |= (SPR_NO_SCAREDZ | SPR_XFLIP_TOGGLE);
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YCENTER));

        sp.setClipdist((100) >> 2);
        u.floor_dist =  Z(16);
        u.ceiling_dist =  Z(16);

        u.sz = sp.getZ();

        sp.setXrepeat(37);
        sp.setYrepeat(32);

        // Special looping buzz sound attached to each hornet spawned
        VOC3D voc = PlaySound(DIGI_HORNETBUZZ, sp, v3df_follow | v3df_init);
        Set3DSoundOwner(SpriteNum, voc);
    }

    private static void DoHornetMatchPlayerZ(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }

        // actor does a sine wave about u.sz - this is the z mid point

        // zdiff = (SPRITEp_LOWER(tsp) - Z(8)) - u.sz;
        int zdiff = (SPRITEp_MID(tsp)) - u.sz;

        // check z diff of the player and the sprite
        int zdist = Z(20 + RANDOM_RANGE(200)); // put a random amount
        if (klabs(zdiff) > zdist) {
            if (zdiff > 0)
            // manipulate the z midpoint
            // u.sz += 256 * ACTORMOVETICS;
            {
                u.sz += 1024 * ACTORMOVETICS;
            } else {
                u.sz -= 256 * ACTORMOVETICS;
            }
        }

        // save off lo and hi z
        int loz = u.loz;
        int hiz = u.hiz;

        // adjust loz/hiz for water depth
        Sect_User su = getSectUser(u.lo_sectp);
        if (u.lo_sectp != -1 && su != null && su.depth != 0) {
            loz -= Z(su.depth) - Z(8);
        }

        int bound;
        // lower bound
        if (u.lo_sp != -1) {
            bound = loz - u.floor_dist;
        } else {
            bound = loz - u.floor_dist - HORNET_BOB_AMT;
        }

        if (u.sz > bound) {
            u.sz = bound;
        }

        // upper bound
        if (u.hi_sp != -1) {
            bound = hiz + u.ceiling_dist;
        } else {
            bound = hiz + u.ceiling_dist + HORNET_BOB_AMT;
        }

        if (u.sz < bound) {
            u.sz = bound;
        }

        u.sz = Math.min(u.sz, loz - u.floor_dist);
        u.sz = Math.max(u.sz, hiz + u.ceiling_dist);

        u.Counter =  ((u.Counter + (ACTORMOVETICS << 3) + (ACTORMOVETICS << 1)) & 2047);
        sp.setZ(u.sz + ((HORNET_BOB_AMT * EngineUtils.sin(u.Counter)) >> 14));

        bound = u.hiz + u.ceiling_dist + HORNET_BOB_AMT;
        if (sp.getZ() < bound) {
            // bumped something
            sp.setZ(u.sz = bound + HORNET_BOB_AMT);
        }
    }

    private static void InitHornetCircle(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.ActorActionFunc = DoHornetCircle;

        NewStateGroup(SpriteNum, u.ActorActionSet.Run);

        // set it close
        DoActorSetSpeed(SpriteNum, FAST_SPEED);

        // set to really fast
        sp.setXvel(400);
        // angle adjuster
        u.Counter2 =  (sp.getXvel() / 3);
        // random angle direction
        if (RANDOM_P2(1024) < 512) {
            u.Counter2 =  -u.Counter2;
        }

        // z velocity
        u.jump_speed =  (200 + RANDOM_P2(128));
        if (klabs(u.sz - u.hiz) < klabs(u.sz - u.loz)) {
            u.jump_speed =  -u.jump_speed;
        }

        u.WaitTics =  ((RANDOM_RANGE(3) + 1) * 60);

        (u.ActorActionFunc).animatorInvoke(SpriteNum);
    }

    private static void DoHornetCircle(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setAng(NORM_ANGLE(sp.getAng() + u.Counter2));

        int nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        int ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

        if (!move_actor(SpriteNum, nx, ny, 0)) {
            // ActorMoveHitReact(SpriteNum);

            // try moving in the opposite direction
            u.Counter2 =  -u.Counter2;
            sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
            nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
            ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

            if (!move_actor(SpriteNum, nx, ny, 0)) {
                InitActorReposition.animatorInvoke(SpriteNum);
                return;
            }
        }

        // move in the z direction
        u.sz -= u.jump_speed * ACTORMOVETICS;

        int bound = u.hiz + u.ceiling_dist + HORNET_BOB_AMT;
        if (u.sz < bound) {
            // bumped something
            u.sz = bound;
            InitActorReposition.animatorInvoke(SpriteNum);
            return;
        }

        // time out
        if ((u.WaitTics -= ACTORMOVETICS) < 0) {
            InitActorReposition.animatorInvoke(SpriteNum);
            u.WaitTics = 0;
        }
    }

    private static void DoHornetDeath(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int nx, ny;

        if (TEST(u.Flags, SPR_FALLING)) {
            u.loz = u.zclip;
            DoFall(SpriteNum);
        } else {
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
            u.jump_speed = 0;
            u.floor_dist = 0;
            DoBeginFall(SpriteNum);
            DoFindGroundPoint(SpriteNum);
            u.zclip = u.loz;
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        // slide while falling
        nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

        u.moveSpriteReturn = move_sprite(SpriteNum, nx, ny, 0, u.ceiling_dist, u.floor_dist, 1, ACTORMOVETICS);

        // on the ground
        if (sp.getZ() >= u.loz) {
            u.Flags &= ~(SPR_FALLING | SPR_SLIDING);
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YFLIP)); // If upside down, reset it
            NewStateGroup(SpriteNum, u.ActorActionSet.Dead);
            DeleteNoSoundOwner(SpriteNum);
        }
    }

    // Hornets can swarm around other hornets or whatever is tagged as swarm target
    public static void DoCheckSwarm(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum), tsp;
        USER u = getUser(SpriteNum), tu;
        if (sp == null || u == null) {
            return;
        }

        int dist, pdist;
        PlayerStr pp;

        if (MoveSkip8 == 0) {
            return; // Don't over check
        }

        if (u.tgt_sp == -1) {
            return;
        }

        // Who's the closest meat!?
        DoActorPickClosePlayer(SpriteNum);

        USER tgu = getUser(u.tgt_sp);
        if (tgu != null && tgu.PlayerP != -1) {
            pp = Player[tgu.PlayerP];
            pdist = DISTANCE(sp.getX(), sp.getY(), pp.posx, pp.posy);
        } else {
            return;
        }

        // all enemys
        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ENEMY); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            tsp = node.get();
            tu = getUser(i);

            if (tu == null) {
                continue;
            }

            if (tsp.getHitag() != TAG_SWARMSPOT || tsp.getLotag() != 2) {
                continue;
            }

            dist = DISTANCE(sp.getX(),
 sp.getY(),
 tsp.getX(),
 tsp.getY());

            if (dist < pdist && u.ID == tu.ID) // Only flock to your own kind
            {
                u.tgt_sp = i; // Set target to swarm center
            }
        }

    }

    private static void DoHornetMove(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // Check for swarming
        // lotag of 1 = Swarm around lotags of 2
        // lotag of 0 is normal
        if (sp.getHitag() == TAG_SWARMSPOT && sp.getLotag() == 1) {
            DoCheckSwarm(SpriteNum);
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        DoHornetMatchPlayerZ(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    public static void InitHornetSting(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        int HitSprite = NORM_HIT_INDEX(u.moveSpriteReturn);

        DoDamage(HitSprite, SpriteNum);
        InitActorReposition.animatorInvoke(SpriteNum);
    }

    public static void HornetSaveable() {
        SaveData(InitHornetSting);
        SaveData(InitHornetCircle);
        SaveData(DoHornetCircle);
        SaveData(DoHornetDeath);
        SaveData(DoHornetMove);

        SaveData(HornetPersonality);

        SaveData(HornetAttrib);

        SaveData(s_HornetRun);
        SaveGroup(sg_HornetRun);
        SaveData(s_HornetStand);
        SaveGroup(sg_HornetStand);
        SaveData(s_HornetDie);
        SaveGroup(sg_HornetDie);
        SaveData(s_HornetDead);
        SaveGroup(sg_HornetDead);

        SaveData(HornetActionSet);
    }
}
