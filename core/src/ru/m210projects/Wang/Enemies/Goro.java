package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.LUMINOUS;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Player.QueueFloorBlood;
import static ru.m210projects.Wang.Sound.PlaySound;
import static ru.m210projects.Wang.Sound.v3df_none;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Goro {

    public static final int GORO_PAIN_R0 = GORO_STAND_R0;
    public static final int GORO_PAIN_R1 = GORO_STAND_R1;
    public static final int GORO_PAIN_R2 = GORO_STAND_R2;
    public static final int GORO_PAIN_R3 = GORO_STAND_R3;
    public static final int GORO_PAIN_R4 = GORO_STAND_R4;
    public static final int GORO_RUN_RATE = 18;
    public static final int GORO_CHOP_RATE = 14;
    public static final int GORO_SPELL_RATE = 6;
    public static final int GORO_SPELL_PAUSE = 30;
    public static final int GORO_STAND_RATE = 12;
    public static final int GORO_PAIN_RATE = 12;
    public static final int GORO_DIE_RATE = 16;
    private static final Animator DoGoroMove = new Animator((Animator.Runnable) Goro::DoGoroMove);
    private static final Animator NullGoro = new Animator((Animator.Runnable) Goro::NullGoro);
    private static final Animator InitGoroChop = new Animator((Animator.Runnable) Goro::InitGoroChop);
    private static final Animator InitEnemyFireball = new Animator((Animator.Runnable) Goro::InitEnemyFireball);
    private static final Animator DoGoroPain = new Animator((Animator.Runnable) Goro::DoGoroPain);
    private static final Decision[] GoroBattle = {new Decision(697, InitActorMoveCloser),
            new Decision(700, InitActorAmbientNoise),
            new Decision(1024, InitActorAttack)};
    private static final Decision[] GoroOffense = {new Decision(797, InitActorMoveCloser),
            new Decision(800, InitActorAttackNoise),
            new Decision(1024, InitActorAttack)};

    //////////////////////
    //
    // GORO RUN
    //
    //////////////////////
    private static final Decision[] GoroBroadcast = {new Decision(3, InitActorAmbientNoise),
            new Decision(1024, InitActorDecide)};
    private static final Decision[] GoroSurprised = {new Decision(701, InitActorMoveCloser),
            new Decision(1024, InitActorDecide)};
    private static final Decision[] GoroEvasive = {new Decision(10, InitActorEvade),
            new Decision(1024, InitActorMoveCloser)};

    //////////////////////
    //
    // GORO CHOP
    //
    //////////////////////
    private static final Decision[] GoroLostTarget = {new Decision(900, InitActorFindPlayer),
            new Decision(1024, InitActorWanderAround)};
    private static final Decision[] GoroCloseRange = {new Decision(700, InitActorAttack),
            new Decision(1024, InitActorReposition)};
    private static final Personality GoroPersonality = new Personality(GoroBattle, GoroOffense, GoroBroadcast, GoroSurprised, GoroEvasive, GoroLostTarget, GoroCloseRange, GoroCloseRange);
    private static final ATTRIBUTE GoroAttrib = new ATTRIBUTE(new short[]{160, 180, 200, 230}, // Speeds
            new short[]{3, 0, -2, -3}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_GRDAMBIENT, DIGI_GRDALERT, 0, DIGI_GRDPAIN, DIGI_GRDSCREAM, DIGI_GRDSWINGAXE, DIGI_GRDFIREBALL, 0, 0, 0});

    //////////////////////
    //
    // GORO SPELL
    //
    //////////////////////

    private static final State[][] s_GoroRun = {{new State(GORO_RUN_R0, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[0][1]},
            new State(GORO_RUN_R0 + 1, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[0][2]},
            new State(GORO_RUN_R0 + 2, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[0][3]},
            new State(GORO_RUN_R0 + 3, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
// s_GoroRun[0][0]},
    }, {new State(GORO_RUN_R1, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[1][1]},
            new State(GORO_RUN_R1 + 1, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[1][2]},
            new State(GORO_RUN_R1 + 2, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[1][3]},
            new State(GORO_RUN_R1 + 3, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
// s_GoroRun[1][0]},
    }, {new State(GORO_RUN_R2, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[2][1]},
            new State(GORO_RUN_R2 + 1, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[2][2]},
            new State(GORO_RUN_R2 + 2, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[2][3]},
            new State(GORO_RUN_R2 + 3, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
// s_GoroRun[2][0]},
    }, {new State(GORO_RUN_R3, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[3][1]},
            new State(GORO_RUN_R3 + 1, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[3][2]},
            new State(GORO_RUN_R3 + 2, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[3][3]},
            new State(GORO_RUN_R3 + 3, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
// s_GoroRun[3][0]},
    }, {new State(GORO_RUN_R4, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[4][1]},
            new State(GORO_RUN_R4 + 1, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[4][2]},
            new State(GORO_RUN_R4 + 2, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
            // s_GoroRun[4][3]},
            new State(GORO_RUN_R4 + 3, GORO_RUN_RATE | SF_TIC_ADJUST, DoGoroMove),
// s_GoroRun[4][0]},
    }};
    private static final EnemyStateGroup sg_GoroRun = new EnemyStateGroup(s_GoroRun[0], s_GoroRun[1], s_GoroRun[2], s_GoroRun[3], s_GoroRun[4]);
    //////////////////////
    //
    // GORO STAND
    //
    //////////////////////
    private static final State[][] s_GoroChop = {{new State(GORO_CHOP_R0, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[0][1]},
            new State(GORO_CHOP_R0 + 1, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[0][2]},
            new State(GORO_CHOP_R0 + 2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[0][3]},
            new State(GORO_CHOP_R0 + 2, SF_QUICK_CALL, InitGoroChop),
            // s_GoroChop[0][4]},
            new State(GORO_CHOP_R0 + 2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[0][5]},
            new State(GORO_CHOP_R0 + 2, SF_QUICK_CALL, InitActorDecide),
            // s_GoroChop[0][6]},
            new State(GORO_CHOP_R0 + 2, GORO_CHOP_RATE, DoGoroMove).setNext(),
// s_GoroChop[0][6]},
    }, {new State(GORO_CHOP_R1, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[1][1]},
            new State(GORO_CHOP_R1 + 1, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[1][2]},
            new State(GORO_CHOP_R1 + 2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[1][3]},
            new State(GORO_CHOP_R1 + 2, SF_QUICK_CALL, InitGoroChop),
            // s_GoroChop[1][4]},
            new State(GORO_CHOP_R1 + 2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[1][5]},
            new State(GORO_CHOP_R1 + 2, SF_QUICK_CALL, InitActorDecide),
            // s_GoroChop[1][6]},
            new State(GORO_CHOP_R1 + 2, GORO_CHOP_RATE, DoGoroMove).setNext(),
// s_GoroChop[1][6]},
    }, {new State(GORO_CHOP_R2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[2][1]},
            new State(GORO_CHOP_R2 + 1, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[2][2]},
            new State(GORO_CHOP_R2 + 2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[2][3]},
            new State(GORO_CHOP_R2 + 2, SF_QUICK_CALL, InitGoroChop),
            // s_GoroChop[2][4]},
            new State(GORO_CHOP_R2 + 2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[2][5]},
            new State(GORO_CHOP_R2 + 2, SF_QUICK_CALL, InitActorDecide),
            // s_GoroChop[2][6]},
            new State(GORO_CHOP_R2 + 2, GORO_CHOP_RATE, DoGoroMove).setNext(),
// s_GoroChop[2][6]},
    }, {new State(GORO_CHOP_R3, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[3][1]},
            new State(GORO_CHOP_R3 + 1, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[3][2]},
            new State(GORO_CHOP_R3 + 2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[3][3]},
            new State(GORO_CHOP_R3 + 2, SF_QUICK_CALL, InitGoroChop),
            // s_GoroChop[3][4]},
            new State(GORO_CHOP_R3 + 2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[3][5]},
            new State(GORO_CHOP_R3 + 2, SF_QUICK_CALL, InitActorDecide),
            // s_GoroChop[3][6]},
            new State(GORO_CHOP_R3 + 2, GORO_CHOP_RATE, DoGoroMove).setNext(),
// s_GoroChop[3][6]},
    }, {new State(GORO_CHOP_R4, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[4][1]},
            new State(GORO_CHOP_R4 + 1, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[4][2]},
            new State(GORO_CHOP_R4 + 2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[4][3]},
            new State(GORO_CHOP_R4 + 2, SF_QUICK_CALL, InitGoroChop),
            // s_GoroChop[4][4]},
            new State(GORO_CHOP_R4 + 2, GORO_CHOP_RATE, NullGoro),
            // s_GoroChop[4][5]},
            new State(GORO_CHOP_R4 + 2, SF_QUICK_CALL, InitActorDecide),
            // s_GoroChop[4][6]},
            new State(GORO_CHOP_R4 + 2, GORO_CHOP_RATE, DoGoroMove).setNext(),
// s_GoroChop[4][6]},
    }};
    private static final EnemyStateGroup sg_GoroChop = new EnemyStateGroup(s_GoroChop[0], s_GoroChop[1], s_GoroChop[2], s_GoroChop[3], s_GoroChop[4]);
    //////////////////////
    //
    // GORO PAIN
    //
    //////////////////////
    private static final State[][] s_GoroSpell = {{new State(GORO_SPELL_R0, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[0][1]},
            new State(GORO_SPELL_R0 + 1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[0][2]},
            new State(GORO_SPELL_R0 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[0][3]},
            new State(GORO_SPELL_R0 + 1, GORO_SPELL_RATE, NullGoro),
            // s_GoroSpell[0][4]},
            new State(GORO_SPELL_R0 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[0][5]},
            new State(GORO_SPELL_R0 + 1, GORO_SPELL_RATE, NullGoro),
            // s_GoroSpell[0][6]},
            new State(GORO_SPELL_R0 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[0][7]},
            new State(GORO_SPELL_R0 + 1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[0][8]},
            new State(GORO_SPELL_R0 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_GoroSpell[0][9]},
            new State(GORO_SPELL_R0 + 1, GORO_SPELL_RATE, DoGoroMove).setNext(),
// s_GoroSpell[0][9]},
    }, {new State(GORO_SPELL_R1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[1][1]},
            new State(GORO_SPELL_R1 + 1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[1][2]},
            new State(GORO_SPELL_R1 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[1][3]},
            new State(GORO_SPELL_R1 + 1, GORO_SPELL_RATE, NullGoro),
            // s_GoroSpell[1][4]},
            new State(GORO_SPELL_R1 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[1][5]},
            new State(GORO_SPELL_R1 + 1, GORO_SPELL_RATE, NullGoro),
            // s_GoroSpell[1][6]},
            new State(GORO_SPELL_R1 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[1][7]},
            new State(GORO_SPELL_R1 + 1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[1][8]},
            new State(GORO_SPELL_R1 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_GoroSpell[1][9]},
            new State(GORO_SPELL_R1 + 1, GORO_SPELL_RATE, DoGoroMove).setNext(),
// s_GoroSpell[1][9]},
    }, {new State(GORO_SPELL_R2, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[2][1]},
            new State(GORO_SPELL_R2 + 1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[2][2]},
            new State(GORO_SPELL_R2 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[2][3]},
            new State(GORO_SPELL_R2 + 1, GORO_SPELL_RATE, NullGoro),
            // s_GoroSpell[2][4]},
            new State(GORO_SPELL_R2 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[2][5]},
            new State(GORO_SPELL_R2 + 1, GORO_SPELL_RATE, NullGoro),
            // s_GoroSpell[2][6]},
            new State(GORO_SPELL_R2 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[2][7]},
            new State(GORO_SPELL_R2 + 1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[2][8]},
            new State(GORO_SPELL_R2 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_GoroSpell[2][9]},
            new State(GORO_SPELL_R2 + 1, GORO_SPELL_RATE, DoGoroMove).setNext(),
// s_GoroSpell[2][9]},
    }, {new State(GORO_SPELL_R3, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[3][1]},
            new State(GORO_SPELL_R3 + 1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[3][2]},
            new State(GORO_SPELL_R3 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[3][3]},
            new State(GORO_SPELL_R3 + 1, GORO_SPELL_RATE, NullGoro),
            // s_GoroSpell[3][4]},
            new State(GORO_SPELL_R3 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[3][5]},
            new State(GORO_SPELL_R3 + 1, GORO_SPELL_RATE, NullGoro),
            // s_GoroSpell[3][6]},
            new State(GORO_SPELL_R3 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[3][7]},
            new State(GORO_SPELL_R3 + 1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[3][8]},
            new State(GORO_SPELL_R3 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_GoroSpell[3][9]},
            new State(GORO_SPELL_R3 + 1, GORO_SPELL_RATE, DoGoroMove).setNext(),
// s_GoroSpell[3][9]},
    }, {new State(GORO_SPELL_R4, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[4][1]},
            new State(GORO_SPELL_R4 + 1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[4][2]},
            new State(GORO_SPELL_R4 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[4][3]},
            new State(GORO_SPELL_R4 + 1, GORO_SPELL_RATE, NullGoro),
            // s_GoroSpell[4][4]},
            new State(GORO_SPELL_R4 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[4][5]},
            new State(GORO_SPELL_R4 + 1, GORO_SPELL_RATE, NullGoro),
            // s_GoroSpell[4][6]},
            new State(GORO_SPELL_R4 + 1, GORO_SPELL_RATE, InitEnemyFireball),
            // s_GoroSpell[4][7]},
            new State(GORO_SPELL_R4 + 1, GORO_SPELL_PAUSE, NullGoro),
            // s_GoroSpell[4][8]},
            new State(GORO_SPELL_R4 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_GoroSpell[4][9]},
            new State(GORO_SPELL_R4 + 1, GORO_SPELL_RATE, DoGoroMove).setNext(),
// s_GoroSpell[4][9]},
    }};
    private static final EnemyStateGroup sg_GoroSpell = new EnemyStateGroup(s_GoroSpell[0], s_GoroSpell[1], s_GoroSpell[2], s_GoroSpell[3], s_GoroSpell[4]);
    private static final State[][] s_GoroStand = {{new State(GORO_STAND_R0, GORO_STAND_RATE, DoGoroMove).setNext(),
// s_GoroStand[0][0]},
    }, {new State(GORO_STAND_R1, GORO_STAND_RATE, DoGoroMove).setNext(),
// s_GoroStand[1][0]},
    }, {new State(GORO_STAND_R2, GORO_STAND_RATE, DoGoroMove).setNext(),
// s_GoroStand[2][0]},
    }, {new State(GORO_STAND_R3, GORO_STAND_RATE, DoGoroMove).setNext(),
// s_GoroStand[3][0]},
    }, {new State(GORO_STAND_R4, GORO_STAND_RATE, DoGoroMove).setNext(),
// s_GoroStand[4][0]},
    }};
    private static final EnemyStateGroup sg_GoroStand = new EnemyStateGroup(s_GoroStand[0], s_GoroStand[1], s_GoroStand[2], s_GoroStand[3], s_GoroStand[4]);
    //////////////////////
    //
    // GORO DIE
    //
    //////////////////////
    private static final State[][] s_GoroPain = {{new State(GORO_PAIN_R0, GORO_PAIN_RATE, DoGoroPain).setNext(),
// s_GoroPain[0][0]},
    }, {new State(GORO_PAIN_R1, GORO_PAIN_RATE, DoGoroPain).setNext(),
// s_GoroPain[1][0]},
    }, {new State(GORO_PAIN_R2, GORO_PAIN_RATE, DoGoroPain).setNext(),
// s_GoroPain[2][0]},
    }, {new State(GORO_PAIN_R3, GORO_PAIN_RATE, DoGoroPain).setNext(),
// s_GoroPain[3][0]},
    }, {new State(GORO_PAIN_R4, GORO_PAIN_RATE, DoGoroPain).setNext(),
// s_GoroPain[4][0]},
    }};
    private static final EnemyStateGroup sg_GoroPain = new EnemyStateGroup(s_GoroPain[0], s_GoroPain[1], s_GoroPain[2], s_GoroPain[3], s_GoroPain[4]);
    private static final State[] s_GoroDie = {new State(GORO_DIE, GORO_DIE_RATE, NullGoro),
            // s_GoroDie[1]},
            new State(GORO_DIE + 1, GORO_DIE_RATE, NullGoro),
            // s_GoroDie[2]},
            new State(GORO_DIE + 2, GORO_DIE_RATE, NullGoro),
            // s_GoroDie[3]},
            new State(GORO_DIE + 3, GORO_DIE_RATE, NullGoro),
            // s_GoroDie[4]},
            new State(GORO_DIE + 4, GORO_DIE_RATE, NullGoro),
            // s_GoroDie[5]},
            new State(GORO_DIE + 5, GORO_DIE_RATE, NullGoro),
            // s_GoroDie[6]},
            new State(GORO_DIE + 6, GORO_DIE_RATE, NullGoro),
            // s_GoroDie[7]},
            new State(GORO_DIE + 7, GORO_DIE_RATE, NullGoro),
            // s_GoroDie[8]},
            new State(GORO_DIE + 8, GORO_DIE_RATE, NullGoro),
            // s_GoroDie[9]},
            new State(GORO_DEAD, SF_QUICK_CALL, QueueFloorBlood),
            // s_GoroDie[10]},
            new State(GORO_DEAD, GORO_DIE_RATE, DoActorDebris).setNext(),
// s_GoroDie[10]},
    };
    private static final EnemyStateGroup sg_GoroDie = new EnemyStateGroup(s_GoroDie);
    private static final State[] s_GoroDead = {new State(GORO_DEAD, GORO_DIE_RATE, DoActorDebris).setNext(),
// s_GoroDead[0]},
    };
    private static final EnemyStateGroup sg_GoroDead = new EnemyStateGroup(s_GoroDead);
    private static final Actor_Action_Set GoroActionSet = new Actor_Action_Set(sg_GoroStand, sg_GoroRun, null, // sg_GoroJump,
            null, // sg_GoroFall,
            null, // sg_GoroCrawl,
            null, // sg_GoroSwim,
            null, // sg_GoroFly,
            null, // sg_GoroRise,
            null, // sg_GoroSit,
            null, // sg_GoroLook,
            null, // climb
            sg_GoroPain, sg_GoroDie, null, // sg_GoroHariKari,
            sg_GoroDead, null, // sg_GoroDeathJump,
            null, // sg_GoroDeathFall,
            new StateGroup[]{sg_GoroChop}, new short[]{1024}, new StateGroup[]{sg_GoroSpell}, new short[]{1024}, null, null, null);

    public static void InitGoroStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{
                sg_GoroStand,
                sg_GoroRun,
                sg_GoroPain,
                sg_GoroDie,
                sg_GoroDead,
                sg_GoroChop,
                sg_GoroSpell
        }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }

    public static void SetupGoro(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u;

        if (TEST(sp.getCstat(),
                CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, GORO_RUN_R0, s_GoroRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_GORO;
        }

        ChangeState(SpriteNum, s_GoroRun[0][0]);
        u.Attrib = GoroAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_GoroDie[0];
        u.setRot(sg_GoroRun);

        EnemyDefaults(SpriteNum, GoroActionSet, GoroPersonality);
        sp.setClipdist(512 >> 2);
        u.Flags |= (SPR_XFLIP_TOGGLE);
    }

    public static void InitEnemyFireball(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite wp, fp = null;
        int nz, dist;
        int size_z;
        int w;
        USER wu;
        Sprite tsp;
        int i, targ_z, xchange, ychange;

        tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }

        PlaySound(DIGI_FIREBALL1, sp, v3df_none);

        // get angle to player and also face player when attacking
        sp.setAng(NORM_ANGLE(EngineUtils.getAngle(tsp.getX() - sp.getX(),
                tsp.getY() - sp.getY())));

        size_z = Z(SPRITEp_SIZE_Y(sp));
        // nz = sp.z - size_z + DIV4(size_z) + DIV8(size_z);
        nz = sp.getZ() - size_z + DIV4(size_z) + DIV8(size_z) + Z(4);

        xchange = MOVEx(GORO_FIREBALL_VELOCITY, sp.getAng());
        ychange = MOVEy(GORO_FIREBALL_VELOCITY, sp.getAng());

        for (i = 0; i < 2; i++) {
            w = SpawnSprite(STAT_MISSILE, GORO_FIREBALL, s_Fireball[0], sp.getSectnum(),
                    sp.getX(),
                    sp.getY(),
                    nz, sp.getAng(),
                    GORO_FIREBALL_VELOCITY);

            wp = boardService.getSprite(w);
            wu = getUser(w);
            if (wp == null || wu == null) {
                return;
            }

            wp.setHitag(LUMINOUS); // Always full brightness
            wp.setXrepeat(20);
            wp.setYrepeat(20);
            wp.setShade(-40);
            // wp.owner = SpriteNum;
            SetOwner(SpriteNum, w);
            wp.setZvel(0);
            wp.setClipdist(16 >> 2);

            wp.setAng(NORM_ANGLE(wp.getAng() + lat_ang[i]));
            HelpMissileLateral(w, 500);
            wp.setAng(NORM_ANGLE(wp.getAng() - lat_ang[i]));

            wu.xchange = xchange;
            wu.ychange = ychange;

            // MissileSetPos(w, DoFireball, 700);
            MissileSetPos(w, DoFireball, 700);

            if (i == 0) {
                // back up first one
                fp = wp;

                // find the distance to the target (player)
                dist = EngineUtils.sqrt(SQ(wp.getX() - tsp.getX()) + SQ(wp.getY() - tsp.getY()));
                // dist = Distance(wp.x, wp.y, tsp.x, tsp.y);

                // Determine target Z value
                targ_z = tsp.getZ() - DIV2(Z(SPRITEp_SIZE_Y(sp)));

                // (velocity * difference between the target and the throwing star) /
                // distance
                if (dist != 0) {
                    wu.zchange = (short) ((GORO_FIREBALL_VELOCITY * (targ_z - wp.getZ())) / dist);
                    wp.setZvel(wu.zchange);
                }
            } else {
                // use the first calculations so the balls stay together
                wu.zchange = fp.getZvel();
                wp.setZvel(wu.zchange);
            }
        }
    }

    public static void InitGoroChop(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        Sprite hp;
        short stat;
        int dist;

        PlaySound(DIGI_GRDSWINGAXE, sp, v3df_none);

        ListNode<Sprite> nexti;
        for (stat = 0; stat < StatDamageList.length; stat++) {
            for (ListNode<Sprite> node = boardService.getStatNode(StatDamageList[stat]); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();

                hp = node.get();

                if (i == SpriteNum) {
                    break;
                }

                dist = DISTANCE(hp.getX(),
                        hp.getY(),
                        sp.getX(),
                        sp.getY());

                if (dist < CLOSE_RANGE_DIST_FUDGE(sp, hp, 700) && FACING_RANGE(hp, sp, 150)) {
                    PlaySound(DIGI_GRDAXEHIT, sp, v3df_none);
                    DoDamage(i, SpriteNum);
                }
            }
        }
    }

    private static void NullGoro(int SpriteNum) {
        USER u = getUser(SpriteNum);

        if (u != null && TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        KeepActorOnFloor(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    private static void DoGoroPain(int SpriteNum) {
        USER u = getUser(SpriteNum);

        NullGoro(SpriteNum);

        if (u != null && (u.WaitTics -= ACTORMOVETICS) <= 0) {
            InitActorDecide(SpriteNum);
        }
    }

    private static void DoGoroMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        KeepActorOnFloor(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    public static void GoroSaveable() {
        SaveData(InitGoroChop);
        SaveData(InitEnemyFireball);
        SaveData(NullGoro);
        SaveData(DoGoroPain);
        SaveData(DoGoroMove);
        SaveData(GoroPersonality);

        SaveData(GoroAttrib);

        SaveData(s_GoroRun);
        SaveGroup(sg_GoroRun);
        SaveData(s_GoroChop);
        SaveGroup(sg_GoroChop);
        SaveData(s_GoroSpell);
        SaveGroup(sg_GoroSpell);
        SaveData(s_GoroStand);
        SaveGroup(sg_GoroStand);
        SaveData(s_GoroPain);
        SaveGroup(sg_GoroPain);
        SaveData(s_GoroDie);
        SaveData(s_GoroDead);
        SaveGroup(sg_GoroDie);
        SaveGroup(sg_GoroDead);

        SaveData(GoroActionSet);
    }
}
