package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Game.Distance;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.LUMINOUS;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.DIV4;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Lava {

    private static final Animator DoLavaMove = new Animator((Animator.Runnable) Lava::DoLavaMove);
    private static final Animator NullLava = new Animator((Animator.Runnable) Lava::NullLava);
    private static final Animator InitLavaThrow = new Animator(Lava::InitLavaThrow);
    private static final Animator InitLavaFlame = new Animator(Animator.NULL);


    public static final int LAVA_STAND_RATE = 12;
    public static final int LAVA_RUN_RATE = 24;
    public static final int LAVA_THROW_RATE = 9;
    public static final int LAVA_FLAME_RATE = 18;
    public static final int LAVA_DIE_RATE = 20;
    private static final Decision[] LavaBattle = {new Decision(600, InitActorMoveCloser),
 new Decision(700, InitActorAlertNoise),
 new Decision(710, InitActorRunAway),
 new Decision(1024, InitActorAttack)};
    private static final Decision[] LavaOffense = {new Decision(700, InitActorMoveCloser),
 new Decision(800, InitActorAlertNoise),
 new Decision(1024, InitActorAttack)};
    private static final Decision[] LavaBroadcast = {new Decision(21, InitActorAlertNoise),
 new Decision(51, InitActorAmbientNoise),
 new Decision(1024, InitActorDecide)};
    private static final Decision[] LavaSurprised = {new Decision(701, InitActorMoveCloser),
 new Decision(1024, InitActorDecide)};
    private static final Decision[] LavaEvasive = {new Decision(10, InitActorEvade),
 new Decision(1024, null)};
    private static final Decision[] LavaLostTarget = {new Decision(900, InitActorFindPlayer),
 new Decision(1024, InitActorWanderAround)};

    //////////////////////
    //
    // LAVA STAND
    //
    //////////////////////
    private static final Decision[] LavaCloseRange = {new Decision(700, InitActorAttack),
 new Decision(1024, InitActorReposition)};
    private static final Personality LavaPersonality = new Personality(LavaBattle, LavaOffense, LavaBroadcast, LavaSurprised, LavaEvasive, LavaLostTarget, LavaCloseRange, LavaCloseRange);
    private static final ATTRIBUTE LavaAttrib = new ATTRIBUTE(
            new short[]{200, 220, 240, 270}, // Speeds
            new short[]{3, 0, -2, -3}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_LAVABOSSAMBIENT, DIGI_LAVABOSSALERT, DIGI_LAVABOSSMETEOR, DIGI_LAVABOSSPAIN, DIGI_LAVABOSSEXPLODE, DIGI_LAVABOSSSWIM, DIGI_LAVABOSSRISE, DIGI_LAVABOSSFLAME, DIGI_LAVABOSSMETEXP, DIGI_LAVABOSSSIZZLE});

    //////////////////////
    //
    // LAVA RUN
    //
    //////////////////////

    private static final State[][] s_LavaStand = {{new State(LAVA_RUN_R0, LAVA_STAND_RATE, DoLavaMove).setNext(),
// s_LavaStand[0][0]},
    }, {new State(LAVA_RUN_R1, LAVA_STAND_RATE, DoLavaMove).setNext(),
// s_LavaStand[1][0]},
    }, {new State(LAVA_RUN_R2, LAVA_STAND_RATE, DoLavaMove).setNext(),
// s_LavaStand[2][0]},
    }, {new State(LAVA_RUN_R3, LAVA_STAND_RATE, DoLavaMove).setNext(),
// s_LavaStand[3][0]},
    }, {new State(LAVA_RUN_R4, LAVA_STAND_RATE, DoLavaMove).setNext(),
// s_LavaStand[4][0]},
    }};

    //////////////////////
    //
    // LAVA THROW
    //
    //////////////////////
    private static final State[][] s_LavaRun = {{new State(LAVA_RUN_R0, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[0][1]},
            new State(LAVA_RUN_R0, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[0][2]},
            new State(LAVA_RUN_R0, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[0][3]},
            new State(LAVA_RUN_R0, LAVA_RUN_RATE, DoLavaMove),
// s_LavaRun[0][0]},
    }, {new State(LAVA_RUN_R1, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[1][1]},
            new State(LAVA_RUN_R1, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[1][2]},
            new State(LAVA_RUN_R1, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[1][3]},
            new State(LAVA_RUN_R1, LAVA_RUN_RATE, DoLavaMove),
// s_LavaRun[1][0]},
    }, {new State(LAVA_RUN_R2, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[2][1]},
            new State(LAVA_RUN_R2, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[2][2]},
            new State(LAVA_RUN_R2, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[2][3]},
            new State(LAVA_RUN_R2, LAVA_RUN_RATE, DoLavaMove),
// s_LavaRun[2][0]},
    }, {new State(LAVA_RUN_R3, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[3][1]},
            new State(LAVA_RUN_R3, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[3][2]},
            new State(LAVA_RUN_R3, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[3][3]},
            new State(LAVA_RUN_R3, LAVA_RUN_RATE, DoLavaMove),
// s_LavaRun[3][0]},
    }, {new State(LAVA_RUN_R4, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[4][1]},
            new State(LAVA_RUN_R4, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[4][2]},
            new State(LAVA_RUN_R4, LAVA_RUN_RATE, DoLavaMove),
 // s_LavaRun[4][3]},
            new State(LAVA_RUN_R4, LAVA_RUN_RATE, DoLavaMove),
// s_LavaRun[4][0]},
    }};

    private static final State[][] s_LavaThrow = {{new State(LAVA_THROW_R0, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[0][1]},
            new State(LAVA_THROW_R0, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[0][2]},
            new State(LAVA_THROW_R0, LAVA_THROW_RATE * 2, NullLava),
 // s_LavaThrow[0][3]},
            new State(LAVA_THROW_R0, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[0][4]},
            new State(LAVA_THROW_R0, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[0][5]},
            new State(LAVA_THROW_R0, SF_QUICK_CALL, InitLavaThrow),
 // s_LavaThrow[0][6]},
            new State(LAVA_THROW_R0, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[0][7]},
            new State(LAVA_THROW_R0, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[0][8]},
            new State(LAVA_THROW_R0, SF_QUICK_CALL, InitActorDecide),
 // s_LavaThrow[0][9]},
            new State(LAVA_THROW_R0, LAVA_THROW_RATE, DoLavaMove).setNext(),
// s_LavaThrow[0][9]},
    }, {new State(LAVA_THROW_R1, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[1][1]},
            new State(LAVA_THROW_R1, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[1][2]},
            new State(LAVA_THROW_R1, LAVA_THROW_RATE * 2, NullLava),
 // s_LavaThrow[1][3]},
            new State(LAVA_THROW_R1, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[1][4]},
            new State(LAVA_THROW_R1, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[1][5]},
            new State(LAVA_THROW_R1, SF_QUICK_CALL, InitLavaThrow),
 // s_LavaThrow[1][6]},
            new State(LAVA_THROW_R1, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[1][7]},
            new State(LAVA_THROW_R1, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[1][8]},
            new State(LAVA_THROW_R1, SF_QUICK_CALL, InitActorDecide),
 // s_LavaThrow[1][9]},
            new State(LAVA_THROW_R1, LAVA_THROW_RATE, DoLavaMove).setNext(),
// s_LavaThrow[1][9]},
    }, {new State(LAVA_THROW_R2, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[2][1]},
            new State(LAVA_THROW_R2, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[2][2]},
            new State(LAVA_THROW_R2, LAVA_THROW_RATE * 2, NullLava),
 // s_LavaThrow[2][3]},
            new State(LAVA_THROW_R2, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[2][4]},
            new State(LAVA_THROW_R2, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[2][5]},
            new State(LAVA_THROW_R2, SF_QUICK_CALL, InitLavaThrow),
 // s_LavaThrow[2][6]},
            new State(LAVA_THROW_R2, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[2][7]},
            new State(LAVA_THROW_R2, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[2][8]},
            new State(LAVA_THROW_R2, SF_QUICK_CALL, InitActorDecide),
 // s_LavaThrow[2][9]},
            new State(LAVA_THROW_R2, LAVA_THROW_RATE, DoLavaMove).setNext(),
// s_LavaThrow[2][9]},
    }, {new State(LAVA_THROW_R3, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[3][1]},
            new State(LAVA_THROW_R3, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[3][2]},
            new State(LAVA_THROW_R3, LAVA_THROW_RATE * 2, NullLava),
 // s_LavaThrow[3][3]},
            new State(LAVA_THROW_R3, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[3][4]},
            new State(LAVA_THROW_R3, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[3][5]},
            new State(LAVA_THROW_R3, SF_QUICK_CALL, InitLavaThrow),
 // s_LavaThrow[3][6]},
            new State(LAVA_THROW_R3, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[3][7]},
            new State(LAVA_THROW_R3, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[3][8]},
            new State(LAVA_THROW_R3, SF_QUICK_CALL, InitActorDecide),
 // s_LavaThrow[3][9]},
            new State(LAVA_THROW_R3, LAVA_THROW_RATE, DoLavaMove).setNext(),
// s_LavaThrow[3][9]},
    }, {new State(LAVA_THROW_R4, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[4][1]},
            new State(LAVA_THROW_R4, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[4][2]},
            new State(LAVA_THROW_R4, LAVA_THROW_RATE * 2, NullLava),
 // s_LavaThrow[4][3]},
            new State(LAVA_THROW_R4, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[4][4]},
            new State(LAVA_THROW_R4, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[4][5]},
            new State(LAVA_THROW_R4, SF_QUICK_CALL, InitLavaThrow),
 // s_LavaThrow[4][6]},
            new State(LAVA_THROW_R4, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[4][7]},
            new State(LAVA_THROW_R4, LAVA_THROW_RATE, NullLava),
 // s_LavaThrow[4][8]},
            new State(LAVA_THROW_R4, SF_QUICK_CALL, InitActorDecide),
 // s_LavaThrow[4][9]},
            new State(LAVA_THROW_R4, LAVA_THROW_RATE, DoLavaMove).setNext(),
// s_LavaThrow[4][9]},
    }};

    //////////////////////
    //
    // LAVA FLAME
    //
    //////////////////////

    private static final State[][] s_LavaFlame = {{new State(LAVA_FLAME_R0, LAVA_FLAME_RATE * 2, NullLava),
 // s_LavaFlame[0][1]},
            new State(LAVA_FLAME_R0, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[0][2]},
            new State(LAVA_FLAME_R0, LAVA_FLAME_RATE * 2, NullLava),
 // s_LavaFlame[0][3]},
            new State(LAVA_FLAME_R0, SF_QUICK_CALL, InitLavaFlame),
 // s_LavaFlame[0][4]},
            new State(LAVA_FLAME_R0, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[0][5]},
            new State(LAVA_FLAME_R0, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[0][6]},
            new State(LAVA_FLAME_R0, SF_QUICK_CALL, InitActorDecide),
 // s_LavaFlame[0][7]},
            new State(LAVA_FLAME_R0, LAVA_FLAME_RATE, DoLavaMove).setNext(),
// s_LavaFlame[0][7]},
    }, {new State(LAVA_FLAME_R1, LAVA_FLAME_RATE * 2, NullLava),
 // s_LavaFlame[1][1]},
            new State(LAVA_FLAME_R1, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[1][2]},
            new State(LAVA_FLAME_R1, LAVA_FLAME_RATE * 2, NullLava),
 // s_LavaFlame[1][3]},
            new State(LAVA_FLAME_R1, SF_QUICK_CALL, InitLavaFlame),
 // s_LavaFlame[1][4]},
            new State(LAVA_FLAME_R1, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[1][5]},
            new State(LAVA_FLAME_R1, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[1][6]},
            new State(LAVA_FLAME_R1, SF_QUICK_CALL, InitActorDecide),
 // s_LavaFlame[1][7]},
            new State(LAVA_FLAME_R1, LAVA_FLAME_RATE, DoLavaMove).setNext(),
// s_LavaFlame[1][7]},
    }, {new State(LAVA_FLAME_R2, LAVA_FLAME_RATE * 2, NullLava),
 // s_LavaFlame[2][1]},
            new State(LAVA_FLAME_R2, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[2][2]},
            new State(LAVA_FLAME_R2, LAVA_FLAME_RATE * 2, NullLava),
 // s_LavaFlame[2][3]},
            new State(LAVA_FLAME_R2, SF_QUICK_CALL, InitLavaFlame),
 // s_LavaFlame[2][4]},
            new State(LAVA_FLAME_R2, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[2][5]},
            new State(LAVA_FLAME_R2, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[2][6]},
            new State(LAVA_FLAME_R2, SF_QUICK_CALL, InitActorDecide),
 // s_LavaFlame[2][7]},
            new State(LAVA_FLAME_R2, LAVA_FLAME_RATE, DoLavaMove).setNext(),
// s_LavaFlame[2][7]},
    }, {new State(LAVA_FLAME_R3, LAVA_FLAME_RATE * 2, NullLava),
 // s_LavaFlame[3][1]},
            new State(LAVA_FLAME_R3, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[3][2]},
            new State(LAVA_FLAME_R3, LAVA_FLAME_RATE * 2, NullLava),
 // s_LavaFlame[3][3]},
            new State(LAVA_FLAME_R3, SF_QUICK_CALL, InitLavaFlame),
 // s_LavaFlame[3][4]},
            new State(LAVA_FLAME_R3, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[3][5]},
            new State(LAVA_FLAME_R3, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[3][6]},
            new State(LAVA_FLAME_R3, SF_QUICK_CALL, InitActorDecide),
 // s_LavaFlame[3][7]},
            new State(LAVA_FLAME_R3, LAVA_FLAME_RATE, DoLavaMove).setNext(),
// s_LavaFlame[3][7]},
    }, {new State(LAVA_FLAME_R4, LAVA_FLAME_RATE * 2, NullLava),
 // s_LavaFlame[4][1]},
            new State(LAVA_FLAME_R4, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[4][2]},
            new State(LAVA_FLAME_R4, LAVA_FLAME_RATE * 2, NullLava),
 // s_LavaFlame[4][3]},
            new State(LAVA_FLAME_R4, SF_QUICK_CALL, InitLavaFlame),
 // s_LavaFlame[4][4]},
            new State(LAVA_FLAME_R4, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[4][5]},
            new State(LAVA_FLAME_R4, LAVA_FLAME_RATE, NullLava),
 // s_LavaFlame[4][6]},
            new State(LAVA_FLAME_R4, SF_QUICK_CALL, InitActorDecide),
 // s_LavaFlame[4][7]},
            new State(LAVA_FLAME_R4, LAVA_FLAME_RATE, DoLavaMove).setNext(),
// s_LavaFlame[4][7]},
    }};
    private static final State[] s_LavaDie = {new State(LAVA_DIE, LAVA_DIE_RATE, NullLava),
 // s_LavaDie[1]},
            new State(LAVA_DEAD, LAVA_DIE_RATE, DoActorDebris).setNext(),
// s_LavaDie[1]}
    };

    //////////////////////
    //
    // LAVA DIE
    //
    //////////////////////
    private static final State[] s_LavaDead = {new State(LAVA_DEAD, LAVA_DIE_RATE, DoActorDebris).setNext(),
// s_LavaDead[0]},
    };

    private static final EnemyStateGroup sg_LavaStand = new EnemyStateGroup(s_LavaStand[0], s_LavaStand[1], s_LavaStand[2], s_LavaStand[3], s_LavaStand[4]);
    private static final EnemyStateGroup sg_LavaRun = new EnemyStateGroup(s_LavaRun[0], s_LavaRun[1], s_LavaRun[2], s_LavaRun[3], s_LavaRun[4]);
    private static final EnemyStateGroup sg_LavaDie = new EnemyStateGroup(s_LavaDie);
    private static final EnemyStateGroup sg_LavaDead = new EnemyStateGroup(s_LavaDead);
    private static final EnemyStateGroup sg_LavaFlame = new EnemyStateGroup(s_LavaFlame[0], s_LavaFlame[1], s_LavaFlame[2], s_LavaFlame[3], s_LavaFlame[4]);
    private static final EnemyStateGroup sg_LavaThrow = new EnemyStateGroup(s_LavaThrow[0], s_LavaThrow[1], s_LavaThrow[2], s_LavaThrow[3], s_LavaThrow[4]);
    
    private static final Actor_Action_Set LavaActionSet = new Actor_Action_Set(sg_LavaStand, sg_LavaRun, null, // sg_LavaJump,
            null, // sg_LavaFall,
            null, // sg_LavaCrawl,
            null, // sg_LavaSwim,
            null, // sg_LavaFly,
            null, // sg_LavaRise,
            null, // sg_LavaSit,
            null, // sg_LavaLook,
            null, // climb
            null, // pain
            sg_LavaDie, null, // sg_LavaHariKari,
            sg_LavaDead, null, // sg_LavaDeathJump,
            null, // sg_LavaDeathFall,
            new StateGroup[]{sg_LavaFlame}, new short[]{1024}, new StateGroup[]{sg_LavaFlame, sg_LavaThrow, sg_LavaThrow, sg_LavaThrow}, new short[]{256, 512, 768, 1024}, null, null, null);

    public static void InitLavaStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[] {
                sg_LavaStand,
                sg_LavaRun,
                sg_LavaDie,
                sg_LavaDead,
                sg_LavaFlame,
                sg_LavaThrow }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }

    public static void SetupLava(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u;
        if (TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, LAVA_RUN_R0, s_LavaRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = 100;
        }

        ChangeState(SpriteNum, s_LavaRun[0][0]);
        u.Attrib = LavaAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_LavaDie[0];
        u.setRot(sg_LavaRun);

        EnemyDefaults(SpriteNum, LavaActionSet, LavaPersonality);
        sp.setXrepeat(110);
        sp.setYrepeat(110);
        sp.setClipdist((512) >> 2);
        u.Flags |= (SPR_XFLIP_TOGGLE | SPR_ELECTRO_TOLERANT);

        u.loz = sp.getZ();
    }

    private static void NullLava(int SpriteNum) {
        USER u = getUser(SpriteNum);

        if (u != null && TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        KeepActorOnFloor(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    private static void DoLavaMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        KeepActorOnFloor(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    public static boolean InitLavaThrow(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return false;
        }

        // get angle to player and also face player when attacking
        int nang = EngineUtils.getAngle(tsp.getX() - sp.getX(), tsp.getY() - sp.getY());
        sp.setAng(nang);

        int nx = sp.getX();
        int ny = sp.getY();
        int nz = SPRITEp_TOS(sp) + DIV4(SPRITEp_SIZE_Z(sp));

        // Spawn a shot
        int w =  SpawnSprite(STAT_MISSILE, LAVA_BOULDER, s_LavaBoulder[0], sp.getSectnum(), nx, ny, nz, nang, NINJA_BOLT_VELOCITY);
        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return false;
        }

        SetOwner(SpriteNum, w);
        wp.setHitag(LUMINOUS); // Always full brightness
        wp.setYrepeat(72);
        wp.setXrepeat(72);
        wp.setShade(-15);
        wp.setZvel(0);
        wp.setAng(nang);

        if (RANDOM_P2(1024) > 512) {
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_XFLIP));
        }
        if (RANDOM_P2(1024) > 512) {
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YFLIP));
        }

        wu.Radius = 200;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setClipdist(256 >> 2);
        wu.ceiling_dist =  Z(14);
        wu.floor_dist =  Z(14);

        wu.xchange = MOVEx(wp.getXvel(),
 wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(),
 wp.getAng());
        wu.zchange = wp.getZvel();

        MissileSetPos(w, DoLavaBoulder, 1200);
        tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return false;
        }

        // find the distance to the target (player)
        int dist = Distance(wp.getX(), wp.getY(), tsp.getX(), tsp.getY());
        if (dist != 0) {
            wu.zchange = (short) ((wp.getXvel() * (SPRITEp_UPPER(tsp) - wp.getZ())) / dist);
            wp.setZvel(wu.zchange);
        }

        return (w) != 0;
    }

    public static void LavaSaveable() {
        SaveData(InitLavaThrow);
        SaveData(InitLavaFlame);

        SaveData(NullLava);
        SaveData(DoLavaMove);
        SaveData(LavaPersonality);

        SaveData(LavaAttrib);
        SaveData(s_LavaStand);
        SaveGroup(sg_LavaStand);
        SaveData(s_LavaRun);
        SaveGroup(sg_LavaRun);
        SaveData(s_LavaThrow);
        SaveGroup(sg_LavaThrow);
        SaveData(s_LavaFlame);
        SaveGroup(sg_LavaFlame);
        SaveData(s_LavaDie);
        SaveData(s_LavaDead);
        SaveGroup(sg_LavaDie);
        SaveGroup(sg_LavaDead);

        SaveData(LavaActionSet);
    }
}
