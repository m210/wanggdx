package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Wang.Actor;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Engine.HIT_WALL;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Enemies.Ripper2.InitRipperSlash;
import static ru.m210projects.Wang.Game.Distance;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.LUMINOUS;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.PALETTE_BROWN_RIPPER;
import static ru.m210projects.Wang.Player.QueueFloorBlood;
import static ru.m210projects.Wang.Rooms.FAFhitscan;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Ripper {

    public static final Animator InitCoolgFire = new Animator(Ripper::InitCoolgFire);
    public static final int RIPPER_RUN_RATE = 16;
    private static final Animator InitRipperHang = new Animator((Animator.Runnable) Ripper::InitRipperHang);
    public static final int RIPPER_STAND_RATE = 12;
    private static final Animator DoRipperMove = new Animator((Animator.Runnable) Ripper::DoRipperMove);
    public static final int RIPPER_SWIPE_RATE = 8;
    public static final int RIPPER_SPEW_RATE = 8;
    private static final Animator DoRipperStandHeart = new Animator((Animator.Runnable) Ripper::DoRipperStandHeart);
    public static final int RIPPER_HEART_RATE = 14;
    private static final Animator DoRipperHang = new Animator((Animator.Runnable) Ripper::DoRipperHang);
    public static final int RIPPER_HANG_RATE = 14;
    public static final int RIPPER_PAIN_RATE = 38;
    private static final Animator DoRipperMoveJump = new Animator((Animator.Runnable) Ripper::DoRipperMoveJump);
    public static final int RIPPER_JUMP_RATE = 25;
    public static final int RIPPER_FALL_RATE = 25;
    private static final Animator DoRipperHangJF = new Animator((Animator.Runnable) Ripper::DoRipperHangJF);
    //////////////////////
    //
    // RIPPER RUN
    //
    //////////////////////
    public static final int RIPPER_JUMP_ATTACK_RATE = 35;
    public static final int RIPPER_HANG_JUMP_RATE = 20;
    public static final int RIPPER_DIE_RATE = 16;
    //////////////////////
    //
    // RIPPER STAND
    //
    //////////////////////
    public static final int RIPPER_DEAD_RATE = 8;
    private static final Animator NullRipper = new Animator((Animator.Runnable) Ripper::NullRipper);
    private static final Animator DoRipperPain = new Animator((Animator.Runnable) Ripper::DoRipperPain);
    private static final Animator DoRipperBeginJumpAttack = new Animator((Animator.Runnable) Ripper::DoRipperBeginJumpAttack);
    private static final Decision[] RipperBattle = {new Decision(748, InitActorMoveCloser),
            new Decision(750, InitActorAlertNoise),
            new Decision(755, InitActorAttackNoise),
            new Decision(1024, InitActorAttack)};
    //////////////////////
    //
    // RIPPER SWIPE
    //
    //////////////////////
    private static final Decision[] RipperOffense = {new Decision(700, InitActorMoveCloser),
            new Decision(710, InitActorAlertNoise),
            new Decision(1024, InitActorAttack)};
    private static final Decision[] RipperBroadcast = {new Decision(3, InitActorAlertNoise),
            new Decision(6, InitActorAmbientNoise),
            new Decision(1024, InitActorDecide)};
    //////////////////////
    //
    // RIPPER SPEW
    //
    //////////////////////
    private static final Decision[] RipperCloseRange = {new Decision(900, InitActorAttack),
            new Decision(1024, InitActorReposition)};
    private static final Decision[] RipperLostTarget = {new Decision(980, InitActorFindPlayer),
            new Decision(1024, InitActorWanderAround)};
    private static final ATTRIBUTE RipperAttrib = new ATTRIBUTE(new short[]{200, 220, 240, 280}, // Speeds
            new short[]{5, 0, -2, -4}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_RIPPERAMBIENT, DIGI_RIPPERALERT, DIGI_RIPPERATTACK, DIGI_RIPPERPAIN, DIGI_RIPPERSCREAM, DIGI_RIPPERHEARTOUT, 0, 0, 0, 0});
    //////////////////////
    //
    // RIPPER JUMP ATTACK
    //
    //////////////////////
    private static final State[][] s_RipperPain = {{new State(RIPPER_JUMP_R0, RIPPER_PAIN_RATE, DoRipperPain).setNext(),
// s_RipperPain[0][0]},
    }, {new State(RIPPER_JUMP_R0, RIPPER_PAIN_RATE, DoRipperPain).setNext(),
// s_RipperPain[1][0]},
    }, {new State(RIPPER_JUMP_R0, RIPPER_PAIN_RATE, DoRipperPain).setNext(),
// s_RipperPain[2][0]},
    }, {new State(RIPPER_JUMP_R0, RIPPER_PAIN_RATE, DoRipperPain).setNext(),
// s_RipperPain[3][0]},
    }, {new State(RIPPER_JUMP_R0, RIPPER_PAIN_RATE, DoRipperPain).setNext(),
// s_RipperPain[4][0]},
    }};
    private static final EnemyStateGroup sg_RipperPain = new EnemyStateGroup(s_RipperPain[0], s_RipperPain[1], s_RipperPain[2], s_RipperPain[3], s_RipperPain[4]);
    private static final State[] s_RipperDie = {new State(RIPPER_DIE, RIPPER_DIE_RATE, NullRipper),
            // s_RipperDie[1]},
            new State(RIPPER_DIE + 1, RIPPER_DIE_RATE, NullRipper),
            // s_RipperDie[2]},
            new State(RIPPER_DIE + 2, RIPPER_DIE_RATE, NullRipper),
            // s_RipperDie[3]},
            new State(RIPPER_DIE + 3, RIPPER_DIE_RATE, NullRipper),
            // s_RipperDie[4]},
            new State(RIPPER_DEAD, RIPPER_DIE_RATE, DoActorDebris).setNext(),
// s_RipperDie[4]},
    };
    private static final EnemyStateGroup sg_RipperDie = new EnemyStateGroup(s_RipperDie);
    private static final State[] s_RipperDead = {new State(RIPPER_DIE + 2, RIPPER_DEAD_RATE, null),
            // s_RipperDead[1]},
            new State(RIPPER_DIE + 3, RIPPER_DEAD_RATE, null),
            // s_RipperDead[2]},
            new State(RIPPER_DEAD, SF_QUICK_CALL, QueueFloorBlood),
            // s_RipperDead[3]},
            new State(RIPPER_DEAD, RIPPER_DEAD_RATE, DoActorDebris).setNext(),
// s_RipperDead[3]},
    };
    private static final EnemyStateGroup sg_RipperDead = new EnemyStateGroup(s_RipperDead);    private static final Decision[] RipperSurprised = {new Decision(30, InitRipperHang),
            new Decision(701, InitActorMoveCloser),
            new Decision(1024, InitActorDecide)};
    private static final Animator DoActorDeathMove = new Animator((Animator.Runnable) Actor::DoActorDeathMove);    private static final Decision[] RipperEvasive = {new Decision(6, InitRipperHang),
            new Decision(1024, null)};
    private static final State[] s_RipperDeathJump = {new State(RIPPER_DIE, RIPPER_DIE_RATE, DoActorDeathMove).setNext(),
// s_RipperDeathJump[0]}
    };    private static final Personality RipperPersonality = new Personality(RipperBattle, RipperOffense, RipperBroadcast, RipperSurprised, RipperEvasive, RipperLostTarget, RipperCloseRange, RipperCloseRange);

    //////////////////////
    //
    // RIPPER HEART - show players heart
    //
    //////////////////////
    private static final EnemyStateGroup sg_RipperDeathJump = new EnemyStateGroup(s_RipperDeathJump);

    //////////////////////
    //
    // RIPPER HANG
    //
    //////////////////////
    private static final State[] s_RipperDeathFall = {new State(RIPPER_DIE + 1, RIPPER_DIE_RATE, DoActorDeathMove).setNext(),
// s_RipperDeathFall[0]}
    };    private static final State[][] s_RipperRun = {{new State(RIPPER_RUN_R0, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[0][1]},
            new State(RIPPER_RUN_R0 + 1, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[0][2]},
            new State(RIPPER_RUN_R0 + 2, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[0][3]},
            new State(RIPPER_RUN_R0 + 3, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
// s_RipperRun[0][0]},
    }, {new State(RIPPER_RUN_R1, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[1][1]},
            new State(RIPPER_RUN_R1 + 1, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[1][2]},
            new State(RIPPER_RUN_R1 + 2, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[1][3]},
            new State(RIPPER_RUN_R1 + 3, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
// s_RipperRun[1][0]},
    }, {new State(RIPPER_RUN_R2, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[2][1]},
            new State(RIPPER_RUN_R2 + 1, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[2][2]},
            new State(RIPPER_RUN_R2 + 2, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[2][3]},
            new State(RIPPER_RUN_R2 + 3, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
// s_RipperRun[2][0]},
    }, {new State(RIPPER_RUN_R3, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[3][1]},
            new State(RIPPER_RUN_R3 + 1, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[3][2]},
            new State(RIPPER_RUN_R3 + 2, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[3][3]},
            new State(RIPPER_RUN_R3 + 3, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
// s_RipperRun[3][0]},
    }, {new State(RIPPER_RUN_R4, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[4][1]},
            new State(RIPPER_RUN_R4 + 1, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[4][2]},
            new State(RIPPER_RUN_R4 + 2, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
            // s_RipperRun[4][3]},
            new State(RIPPER_RUN_R4 + 3, RIPPER_RUN_RATE | SF_TIC_ADJUST, DoRipperMove),
// s_RipperRun[4][0]},
    }};
    private static final EnemyStateGroup sg_RipperDeathFall = new EnemyStateGroup(s_RipperDeathFall);    private static final State[][] s_RipperStand = {{new State(RIPPER_STAND_R0, RIPPER_STAND_RATE, DoRipperMove).setNext(),
// s_RipperStand[0][0]},
    }, {new State(RIPPER_STAND_R1, RIPPER_STAND_RATE, DoRipperMove).setNext(),
// s_RipperStand[1][0]},
    }, {new State(RIPPER_STAND_R2, RIPPER_STAND_RATE, DoRipperMove).setNext(),
// s_RipperStand[2][0]},
    }, {new State(RIPPER_STAND_R3, RIPPER_STAND_RATE, DoRipperMove).setNext(),
// s_RipperStand[3][0]},
    }, {new State(RIPPER_STAND_R4, RIPPER_STAND_RATE, DoRipperMove).setNext(),
// s_RipperStand[4][0]},
    }};

    //////////////////////
    //
    // RIPPER PAIN
    //
    //////////////////////

    public static void InitRipperStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{
                sg_RipperStand,
                sg_RipperRun,
                sg_RipperJump,
                sg_RipperFall,
                sg_RipperPain,
                sg_RipperDie,
                sg_RipperDead,
                sg_RipperDeathJump,
                sg_RipperDeathFall,
                sg_RipperSpew,
                sg_RipperJumpAttack,
                sg_RipperHeart,
                sg_RipperHang,
                sg_RipperSwipe,
                sg_RipperHangJump,
                sg_RipperHangFall
        }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }    private static final State[][] s_RipperSwipe = {{new State(RIPPER_SWIPE_R0, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[0][1]},
            new State(RIPPER_SWIPE_R0 + 1, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[0][2]},
            new State(RIPPER_SWIPE_R0 + 1, SF_QUICK_CALL, InitRipperSlash),
            // s_RipperSwipe[0][3]},
            new State(RIPPER_SWIPE_R0 + 2, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[0][4]},
            new State(RIPPER_SWIPE_R0 + 3, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[0][5]},
            new State(RIPPER_SWIPE_R0 + 3, SF_QUICK_CALL, InitRipperSlash),
            // s_RipperSwipe[0][6]},
            new State(RIPPER_SWIPE_R0 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_RipperSwipe[0][7]},
            new State(RIPPER_SWIPE_R0 + 3, RIPPER_SWIPE_RATE, DoRipperMove).setNext(),
// s_RipperSwipe[0][7]},
    }, {new State(RIPPER_SWIPE_R1, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[1][1]},
            new State(RIPPER_SWIPE_R1 + 1, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[1][2]},
            new State(RIPPER_SWIPE_R1 + 1, SF_QUICK_CALL, InitRipperSlash),
            // s_RipperSwipe[1][3]},
            new State(RIPPER_SWIPE_R1 + 2, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[1][4]},
            new State(RIPPER_SWIPE_R1 + 3, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[1][5]},
            new State(RIPPER_SWIPE_R1 + 3, SF_QUICK_CALL, InitRipperSlash),
            // s_RipperSwipe[1][6]},
            new State(RIPPER_SWIPE_R1 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_RipperSwipe[1][7]},
            new State(RIPPER_SWIPE_R1 + 3, RIPPER_SWIPE_RATE, DoRipperMove).setNext(),
// s_RipperSwipe[1][7]},
    }, {new State(RIPPER_SWIPE_R2, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[2][1]},
            new State(RIPPER_SWIPE_R2 + 1, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[2][2]},
            new State(RIPPER_SWIPE_R2 + 1, SF_QUICK_CALL, InitRipperSlash),
            // s_RipperSwipe[2][3]},
            new State(RIPPER_SWIPE_R2 + 2, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[2][4]},
            new State(RIPPER_SWIPE_R2 + 3, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[2][5]},
            new State(RIPPER_SWIPE_R2 + 3, SF_QUICK_CALL, InitRipperSlash),
            // s_RipperSwipe[2][6]},
            new State(RIPPER_SWIPE_R2 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_RipperSwipe[2][7]},
            new State(RIPPER_SWIPE_R2 + 3, RIPPER_SWIPE_RATE, DoRipperMove).setNext(),
// s_RipperSwipe[2][7]},
    }, {new State(RIPPER_SWIPE_R3, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[3][1]},
            new State(RIPPER_SWIPE_R3 + 1, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[3][2]},
            new State(RIPPER_SWIPE_R3 + 1, SF_QUICK_CALL, InitRipperSlash),
            // s_RipperSwipe[3][3]},
            new State(RIPPER_SWIPE_R3 + 2, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[3][4]},
            new State(RIPPER_SWIPE_R3 + 3, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[3][5]},
            new State(RIPPER_SWIPE_R3 + 3, SF_QUICK_CALL, InitRipperSlash),
            // s_RipperSwipe[3][6]},
            new State(RIPPER_SWIPE_R3 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_RipperSwipe[3][7]},
            new State(RIPPER_SWIPE_R3 + 3, RIPPER_SWIPE_RATE, DoRipperMove).setNext(),
// s_RipperSwipe[3][7]},
    }, {new State(RIPPER_SWIPE_R4, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[4][1]},
            new State(RIPPER_SWIPE_R4 + 1, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[4][2]},
            new State(RIPPER_SWIPE_R4 + 1, SF_QUICK_CALL, InitRipperSlash),
            // s_RipperSwipe[4][3]},
            new State(RIPPER_SWIPE_R4 + 2, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[4][4]},
            new State(RIPPER_SWIPE_R4 + 3, RIPPER_SWIPE_RATE, NullRipper),
            // s_RipperSwipe[4][5]},
            new State(RIPPER_SWIPE_R4 + 3, SF_QUICK_CALL, InitRipperSlash),
            // s_RipperSwipe[4][6]},
            new State(RIPPER_SWIPE_R4 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_RipperSwipe[4][7]},
            new State(RIPPER_SWIPE_R4 + 3, RIPPER_SWIPE_RATE, DoRipperMove).setNext(),
// s_RipperSwipe[4][7]},
    }};

    public static void SetupRipper(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u;

        if (TEST(sp.getCstat(),
                CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, RIPPER_RUN_R0, s_RipperRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_RIPPER / 2; // Baby rippers are weaker
        }

        ChangeState(SpriteNum, s_RipperRun[0][0]);
        u.Attrib = RipperAttrib;
        DoActorSetSpeed(SpriteNum, FAST_SPEED);
        u.StateEnd = s_RipperDie[0];
        u.setRot(sg_RipperRun);
        sp.setXrepeat(64);
        sp.setYrepeat(64);

        if (sp.getPal() == PALETTE_BROWN_RIPPER) {
            EnemyDefaults(SpriteNum, RipperBrownActionSet, RipperPersonality);
            sp.setXrepeat(106);
            sp.setYrepeat(90);

            if (!TEST(sp.getCstat(),
                    CSTAT_SPRITE_RESTORE)) {
                u.Health = HEALTH_MOMMA_RIPPER;
            }

            sp.setClipdist(sp.getClipdist() + (128 >> 2));
        } else {
            EnemyDefaults(SpriteNum, RipperActionSet, RipperPersonality);
        }

        u.Flags |= (SPR_XFLIP_TOGGLE);
    }
    private static final State[][] s_RipperSpew = {{new State(RIPPER_SWIPE_R0, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[0][1]},
            new State(RIPPER_SWIPE_R0 + 1, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[0][2]},
            new State(RIPPER_SWIPE_R0 + 1, SF_QUICK_CALL, InitCoolgFire),
            // s_RipperSpew[0][3]},
            new State(RIPPER_SWIPE_R0 + 2, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[0][4]},
            new State(RIPPER_SWIPE_R0 + 3, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[0][5]},
            new State(RIPPER_SWIPE_R0 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_RipperSpew[0][6]},
            new State(RIPPER_SWIPE_R0 + 3, RIPPER_SPEW_RATE, DoRipperMove).setNext(),
// s_RipperSpew[0][6]},
    }, {new State(RIPPER_SWIPE_R1, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[1][1]},
            new State(RIPPER_SWIPE_R1 + 1, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[1][2]},
            new State(RIPPER_SWIPE_R1 + 1, SF_QUICK_CALL, InitCoolgFire),
            // s_RipperSpew[1][3]},
            new State(RIPPER_SWIPE_R1 + 2, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[1][4]},
            new State(RIPPER_SWIPE_R1 + 3, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[1][5]},
            new State(RIPPER_SWIPE_R1 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_RipperSpew[1][6]},
            new State(RIPPER_SWIPE_R1 + 3, RIPPER_SPEW_RATE, DoRipperMove).setNext(),
// s_RipperSpew[1][6]},
    }, {new State(RIPPER_SWIPE_R2, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[2][1]},
            new State(RIPPER_SWIPE_R2 + 1, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[2][2]},
            new State(RIPPER_SWIPE_R2 + 1, SF_QUICK_CALL, InitCoolgFire),
            // s_RipperSpew[2][3]},
            new State(RIPPER_SWIPE_R2 + 2, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[2][4]},
            new State(RIPPER_SWIPE_R2 + 3, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[2][5]},
            new State(RIPPER_SWIPE_R2 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_RipperSpew[2][6]},
            new State(RIPPER_SWIPE_R2 + 3, RIPPER_SPEW_RATE, DoRipperMove).setNext(),
// s_RipperSpew[2][6]},
    }, {new State(RIPPER_SWIPE_R3, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[3][1]},
            new State(RIPPER_SWIPE_R3 + 1, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[3][2]},
            new State(RIPPER_SWIPE_R3 + 1, SF_QUICK_CALL, InitCoolgFire),
            // s_RipperSpew[3][3]},
            new State(RIPPER_SWIPE_R3 + 2, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[3][4]},
            new State(RIPPER_SWIPE_R3 + 3, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[3][5]},
            new State(RIPPER_SWIPE_R3 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_RipperSpew[3][6]},
            new State(RIPPER_SWIPE_R3 + 3, RIPPER_SPEW_RATE, DoRipperMove).setNext(),
// s_RipperSpew[3][6]},
    }, {new State(RIPPER_SWIPE_R4, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[4][1]},
            new State(RIPPER_SWIPE_R4 + 1, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[4][2]},
            new State(RIPPER_SWIPE_R4 + 1, SF_QUICK_CALL, InitCoolgFire),
            // s_RipperSpew[4][3]},
            new State(RIPPER_SWIPE_R4 + 2, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[4][4]},
            new State(RIPPER_SWIPE_R4 + 3, RIPPER_SPEW_RATE, NullRipper),
            // s_RipperSpew[4][5]},
            new State(RIPPER_SWIPE_R4 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_RipperSpew[4][6]},
            new State(RIPPER_SWIPE_R4 + 3, RIPPER_SPEW_RATE, DoRipperMove).setNext(),
// s_RipperSpew[4][6]},
    }};

    //////////////////////
    //
    // RIPPER JUMP
    //
    //////////////////////

    private static int GetJumpHeight(int jump_speed, int jump_grav) {
        int jump_iterations;
        int height;

        jump_speed = klabs(jump_speed);

        jump_iterations = jump_speed / (jump_grav * ACTORMOVETICS);

        height = jump_speed * jump_iterations * ACTORMOVETICS;

        height = DIV256(height);

        return (DIV2(height));
    }    private static final State[][] s_RipperHeart = {{new State(RIPPER_HEART_R0, RIPPER_HEART_RATE, DoRipperStandHeart).setNext(),
// s_RipperHeart[0][0]},
    }, {new State(RIPPER_HEART_R1, RIPPER_HEART_RATE, DoRipperStandHeart).setNext(),
// s_RipperHeart[1][0]},
    }, {new State(RIPPER_HEART_R2, RIPPER_HEART_RATE, DoRipperStandHeart).setNext(),
// s_RipperHeart[2][0]},
    }, {new State(RIPPER_HEART_R3, RIPPER_HEART_RATE, DoRipperStandHeart).setNext(),
// s_RipperHeart[3][0]},
    }, {new State(RIPPER_HEART_R4, RIPPER_HEART_RATE, DoRipperStandHeart).setNext(),
// s_RipperHeart[4][0]},
    }};

    public static int PickJumpSpeed(int SpriteNum, int pix_height) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return 0;
        }

        u.jump_speed = -600;
        u.jump_grav = 8;

        while (GetJumpHeight(u.jump_speed, u.jump_grav) <= pix_height + 20) {
            u.jump_speed -= 100;
        }

        return (u.jump_speed);
    }

    //////////////////////
    //
    // RIPPER FALL
    //
    //////////////////////
    private static final State[][] s_RipperHang = {{new State(RIPPER_HANG_R0, RIPPER_HANG_RATE, DoRipperHang).setNext(),
// s_RipperHang[0][0]},
    }, {new State(RIPPER_HANG_R1, RIPPER_HANG_RATE, DoRipperHang).setNext(),
// s_RipperHang[1][0]},
    }, {new State(RIPPER_HANG_R2, RIPPER_HANG_RATE, DoRipperHang).setNext(),
// s_RipperHang[2][0]},
    }, {new State(RIPPER_HANG_R3, RIPPER_HANG_RATE, DoRipperHang).setNext(),
// s_RipperHang[3][0]},
    }, {new State(RIPPER_HANG_R4, RIPPER_HANG_RATE, DoRipperHang).setNext(),
// s_RipperHang[4][0]},
    }};

    public static void PickJumpMaxSpeed(int SpriteNum, int max_speed) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.jump_speed = max_speed;
        u.jump_grav = 8;

        int zh = SPRITEp_TOS(sp);

        while (true) {
            if (zh - Z(GetJumpHeight(u.jump_speed, u.jump_grav)) - Z(16) > u.hiz) {
                break;
            }

            u.jump_speed += 100;

            if (u.jump_speed > -200) {
                break;
            }
        }

    }

    private static void InitRipperHang(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int dist;

        int hitwall;
        int hitsect;
        int hitx, hity;

        boolean Found = false;
        int dang, tang;

        for (dang = 0; dang < 2048; dang += 128) {
            tang = NORM_ANGLE(sp.getAng() + dang);

            FAFhitscan(sp.getX(),
                    sp.getY(),
                    sp.getZ() - SPRITEp_SIZE_Z(sp),
                    sp.getSectnum(),
                    // Start position
                    EngineUtils.sin(NORM_ANGLE(tang + 512)),
                    // X vector of 3D ang
                    EngineUtils.sin(tang),
                    // Y vector of 3D ang
                    0, // Z vector of 3D ang
                    pHitInfo, CLIPMASK_MISSILE);

            hitsect = pHitInfo.hitsect;
            hitwall = pHitInfo.hitwall;
            hitx = pHitInfo.hitx;
            hity = pHitInfo.hity;

            if (hitsect == -1) {
                continue;
            }

            dist = Distance(sp.getX(),
                    sp.getY(),
                    hitx, hity);

            if (hitwall < 0 || dist < 2000 || dist > 7000) {
                continue;
            }

            Found = true;
            sp.setAng(tang);
            break;
        }

        if (!Found) {
            InitActorDecide(SpriteNum);
            return;
        }

        NewStateGroup(SpriteNum, sg_RipperHangJump);
        u.StateFallOverride = sg_RipperHangFall;
        DoActorSetSpeed(SpriteNum, FAST_SPEED);

        // u.jump_speed = -800;
        PickJumpMaxSpeed(SpriteNum, -800);

        u.Flags |= (SPR_JUMPING);
        u.Flags &= ~(SPR_FALLING);

        // set up individual actor jump gravity
        u.jump_grav = 8;

        DoJump(SpriteNum);
    }

    private static final State[][] s_RipperJump = {{new State(RIPPER_JUMP_R0, RIPPER_JUMP_RATE, NullRipper),
            // s_RipperJump[0][1]},
            new State(RIPPER_JUMP_R0 + 1, RIPPER_JUMP_RATE, DoRipperMoveJump).setNext(),
// s_RipperJump[0][1]},
    }, {new State(RIPPER_JUMP_R1, RIPPER_JUMP_RATE, NullRipper),
            // s_RipperJump[1][1]},
            new State(RIPPER_JUMP_R1 + 1, RIPPER_JUMP_RATE, DoRipperMoveJump).setNext(),
// s_RipperJump[1][1]},
    }, {new State(RIPPER_JUMP_R2, RIPPER_JUMP_RATE, NullRipper),
            // s_RipperJump[2][1]},
            new State(RIPPER_JUMP_R2 + 1, RIPPER_JUMP_RATE, DoRipperMoveJump).setNext(),
// s_RipperJump[2][1]},
    }, {new State(RIPPER_JUMP_R3, RIPPER_JUMP_RATE, NullRipper),
            // s_RipperJump[3][1]},
            new State(RIPPER_JUMP_R3 + 1, RIPPER_JUMP_RATE, DoRipperMoveJump).setNext(),
// s_RipperJump[3][1]},
    }, {new State(RIPPER_JUMP_R4, RIPPER_JUMP_RATE, NullRipper),
            // s_RipperJump[4][1]},
            new State(RIPPER_JUMP_R4 + 1, RIPPER_JUMP_RATE, DoRipperMoveJump).setNext(),
// s_RipperJump[4][1]},
    }};

    private static void DoRipperHang(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if ((u.WaitTics -= ACTORMOVETICS) > 0) {
            return;
        }

        NewStateGroup(SpriteNum, sg_RipperJumpAttack);
        // move to the 2nd frame - past the pause frame
        u.Tics += u.State.Tics;
    }    //////////////////////
    //
    // RIPPER HANG_JUMP
    //
    //////////////////////
    private static final State[][] s_RipperFall = {{new State(RIPPER_FALL_R0, RIPPER_FALL_RATE, DoRipperMoveJump).setNext(),
// s_RipperFall[0][0]},
    }, {new State(RIPPER_FALL_R1, RIPPER_FALL_RATE, DoRipperMoveJump).setNext(),
// s_RipperFall[1][0]},
    }, {new State(RIPPER_FALL_R2, RIPPER_FALL_RATE, DoRipperMoveJump).setNext(),
// s_RipperFall[2][0]},
    }, {new State(RIPPER_FALL_R3, RIPPER_FALL_RATE, DoRipperMoveJump).setNext(),
// s_RipperFall[3][0]},
    }, {new State(RIPPER_FALL_R4, RIPPER_FALL_RATE, DoRipperMoveJump).setNext(),
// s_RipperFall[4][0]},
    }};

    private static void DoRipperMoveHang(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // Move while jumping
        int nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        int ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

        // if cannot move the sprite
        if (!move_actor(SpriteNum, nx, ny, 0)) {
            if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_WALL) {
                int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);

                NewStateGroup(SpriteNum, u.ActorActionSet.Special[1]);
                u.WaitTics = (2 + ((RANDOM_P2(4 << 8) >> 8) * 120));

                // hang flush with the wall
                Wall wal = boardService.getWall(hitwall);
                if (wal != null) {
                    sp.setAng(NORM_ANGLE(wal.getWallAngle() - 512));
                }
            }
        }
    }

    private static final State[][] s_RipperJumpAttack = {{new State(RIPPER_JUMP_R0, RIPPER_JUMP_ATTACK_RATE, NullRipper),
            // s_RipperJumpAttack[0][1]},
            new State(RIPPER_JUMP_R0, SF_QUICK_CALL, DoRipperBeginJumpAttack),
            // s_RipperJumpAttack[0][2]},
            new State(RIPPER_JUMP_R0 + 1, RIPPER_JUMP_ATTACK_RATE, DoRipperMoveJump).setNext(),
// s_RipperJumpAttack[0][2]},
    }, {new State(RIPPER_JUMP_R1, RIPPER_JUMP_ATTACK_RATE, NullRipper),
            // s_RipperJumpAttack[1][1]},
            new State(RIPPER_JUMP_R1, SF_QUICK_CALL, DoRipperBeginJumpAttack),
            // s_RipperJumpAttack[1][2]},
            new State(RIPPER_JUMP_R1 + 1, RIPPER_JUMP_ATTACK_RATE, DoRipperMoveJump).setNext(),
// s_RipperJumpAttack[1][2]},
    }, {new State(RIPPER_JUMP_R2, RIPPER_JUMP_ATTACK_RATE, NullRipper),
            // s_RipperJumpAttack[2][1]},
            new State(RIPPER_JUMP_R2, SF_QUICK_CALL, DoRipperBeginJumpAttack),
            // s_RipperJumpAttack[2][2]},
            new State(RIPPER_JUMP_R2 + 1, RIPPER_JUMP_ATTACK_RATE, DoRipperMoveJump).setNext(),
// s_RipperJumpAttack[2][2]},
    }, {new State(RIPPER_JUMP_R3, RIPPER_JUMP_ATTACK_RATE, NullRipper),
            // s_RipperJumpAttack[3][1]},
            new State(RIPPER_JUMP_R3, SF_QUICK_CALL, DoRipperBeginJumpAttack),
            // s_RipperJumpAttack[3][2]},
            new State(RIPPER_JUMP_R3 + 1, RIPPER_JUMP_ATTACK_RATE, DoRipperMoveJump).setNext(),
// s_RipperJumpAttack[3][2]},
    }, {new State(RIPPER_JUMP_R4, RIPPER_JUMP_ATTACK_RATE, NullRipper),
            // s_RipperJumpAttack[4][1]},
            new State(RIPPER_JUMP_R4, SF_QUICK_CALL, DoRipperBeginJumpAttack),
            // s_RipperJumpAttack[4][2]},
            new State(RIPPER_JUMP_R4 + 1, RIPPER_JUMP_ATTACK_RATE, DoRipperMoveJump).setNext(),
// s_RipperJumpAttack[4][2]},
    }};

    //////////////////////
    //
    // RIPPER HANG_FALL
    //
    //////////////////////

    private static void DoRipperHangJF(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoJump(SpriteNum);
            } else {
                DoFall(SpriteNum);
            }
        }

        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (DoRipperQuickJump(SpriteNum) != 0) {
                return;
            }

            InitActorDecide(SpriteNum);
        }

        DoRipperMoveHang(SpriteNum);

    }

    //////////////////////
    //
    // RIPPER DIE
    //
    //////////////////////
    private static final State[][] s_RipperHangJump = {{new State(RIPPER_JUMP_R0, RIPPER_HANG_JUMP_RATE, NullRipper),
            // s_RipperHangJump[0][1]},
            new State(RIPPER_JUMP_R0 + 1, RIPPER_HANG_JUMP_RATE, DoRipperHangJF).setNext(),
// s_RipperHangJump[0][1]},
    }, {new State(RIPPER_JUMP_R1, RIPPER_HANG_JUMP_RATE, NullRipper),
            // s_RipperHangJump[1][1]},
            new State(RIPPER_JUMP_R1 + 1, RIPPER_HANG_JUMP_RATE, DoRipperHangJF).setNext(),
// s_RipperHangJump[1][1]},
    }, {new State(RIPPER_JUMP_R2, RIPPER_HANG_JUMP_RATE, NullRipper),
            // s_RipperHangJump[2][1]},
            new State(RIPPER_JUMP_R2 + 1, RIPPER_HANG_JUMP_RATE, DoRipperHangJF).setNext(),
// s_RipperHangJump[2][1]},
    }, {new State(RIPPER_JUMP_R3, RIPPER_HANG_JUMP_RATE, NullRipper),
            // s_RipperHangJump[3][1]},
            new State(RIPPER_JUMP_R3 + 1, RIPPER_HANG_JUMP_RATE, DoRipperHangJF).setNext(),
// s_RipperHangJump[3][1]},
    }, {new State(RIPPER_JUMP_R4, RIPPER_HANG_JUMP_RATE, NullRipper),
            // s_RipperHangJump[4][1]},
            new State(RIPPER_JUMP_R4 + 1, RIPPER_HANG_JUMP_RATE, DoRipperHangJF).setNext(),
// s_RipperHangJump[4][1]},
    }};

    private static int RANDOM_NEG() {
        return (RANDOM_P2((16384) << 1) - (16384));
    }

    private static final State[][] s_RipperHangFall = {{new State(RIPPER_FALL_R0, RIPPER_FALL_RATE, DoRipperHangJF).setNext(),
// s_RipperHangFall[0][0]},
    }, {new State(RIPPER_FALL_R1, RIPPER_FALL_RATE, DoRipperHangJF).setNext(),
// s_RipperHangFall[1][0]},
    }, {new State(RIPPER_FALL_R2, RIPPER_FALL_RATE, DoRipperHangJF).setNext(),
// s_RipperHangFall[2][0]},
    }, {new State(RIPPER_FALL_R3, RIPPER_FALL_RATE, DoRipperHangJF).setNext(),
// s_RipperHangFall[3][0]},
    }, {new State(RIPPER_FALL_R4, RIPPER_FALL_RATE, DoRipperHangJF).setNext(),
// s_RipperHangFall[4][0]},
    }};

    private static void DoRipperBeginJumpAttack(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite psp = boardService.getSprite(u.tgt_sp);
        if (psp == null) {
            return;
        }

        int tang = EngineUtils.getAngle(psp.getX() - sp.getX(),
                psp.getY() - sp.getY());

        if (move_sprite(SpriteNum, EngineUtils.sin(NORM_ANGLE(tang + 512)) >> 7, EngineUtils.sin(tang) >> 7, 0, u.ceiling_dist, u.floor_dist, CLIPMASK_ACTOR, ACTORMOVETICS) != 0) {
            sp.setAng(NORM_ANGLE((sp.getAng() + 1024) + (RANDOM_NEG() >> 6)));
        } else {
            sp.setAng(NORM_ANGLE(tang + (RANDOM_NEG() >> 6)));
        }

        DoActorSetSpeed(SpriteNum, FAST_SPEED);

        PickJumpMaxSpeed(SpriteNum, -400); // was -800

        u.Flags |= (SPR_JUMPING);
        u.Flags &= ~(SPR_FALLING);

        // set up individual actor jump gravity
        u.jump_grav = 17; // was 8

        // if I didn't do this here they get stuck in the air sometimes
        DoActorZrange(SpriteNum);

        DoJump(SpriteNum);
    }

    private static void DoRipperMoveJump(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoJump(SpriteNum);
            } else {
                DoFall(SpriteNum);
            }
        }

        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (DoRipperQuickJump(SpriteNum) != 0) {
                return;
            }

            InitActorDecide(SpriteNum);
        }

        DoRipperMoveHang(SpriteNum);
    }

    private static int DoRipperQuickJump(int SpriteNum) {
        USER u = getUser(SpriteNum);

        // Tests to see if ripper is on top of a player/enemy and then immediatly
        // does another jump

        if (u != null && u.lo_sp != -1) {
            Sprite tsp = boardService.getSprite(u.lo_sp);

            if (tsp != null && TEST(tsp.getExtra(),
                    SPRX_PLAYER_OR_ENEMY)) {
                NewStateGroup(SpriteNum, sg_RipperJumpAttack);
                // move past the first private static final State
                u.Tics = 30;
                return (TRUE);
            }
        }

        return (FALSE);
    }

    private static void NullRipper(int SpriteNum) {
        USER u = getUser(SpriteNum);

        if (u != null && TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
    }

    private static void DoRipperPain(int SpriteNum) {
        USER u = getUser(SpriteNum);

        NullRipper(SpriteNum);
        if (u != null && (u.WaitTics -= ACTORMOVETICS) <= 0) {
            InitActorDecide(SpriteNum);
        }
    }

    public static void DoRipperRipHeart(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }

        NewStateGroup(SpriteNum, sg_RipperHeart);
        u.WaitTics = 6 * 120;

        // player face ripper
        tsp.setAng(EngineUtils.getAngle(sp.getX() - tsp.getX(),
                sp.getY() - tsp.getY()));
    }

    private static final EnemyStateGroup sg_RipperStand = new EnemyStateGroup(s_RipperStand[0], s_RipperStand[1], s_RipperStand[2], s_RipperStand[3], s_RipperStand[4]);

    // CTW MODIFICATION
    private static void DoRipperStandHeart(int SpriteNum)
// CTW MODIFICATION END
    {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        NullRipper(SpriteNum);

        if ((u.WaitTics -= ACTORMOVETICS) <= 0) {
            NewStateGroup(SpriteNum, sg_RipperRun);
        }
    }

    private static final EnemyStateGroup sg_RipperRun = new EnemyStateGroup(s_RipperRun[0], s_RipperRun[1], s_RipperRun[2], s_RipperRun[3], s_RipperRun[4]);

    private static void DoRipperMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (u.scale_speed != 0) {
            DoScaleSprite(SpriteNum);
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoJump(SpriteNum);
            } else {
                DoFall(SpriteNum);
            }
        }

        // if on a player/enemy sprite jump quickly
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (DoRipperQuickJump(SpriteNum) != 0) {
                return;
            }

            KeepActorOnFloor(SpriteNum);
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
    }

    private static final EnemyStateGroup sg_RipperJump = new EnemyStateGroup(s_RipperJump[0], s_RipperJump[1], s_RipperJump[2], s_RipperJump[3], s_RipperJump[4]);

    public static boolean InitCoolgFire(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum), wp;
        USER u = getUser(SpriteNum), wu;
        if (sp == null || u == null) {
            return false;
        }

        int nx, ny, nz, dist, nang;
        int w;

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return false;
        }

        // get angle to player and also face player when attacking
        sp.setAng((nang = NORM_ANGLE(EngineUtils.getAngle(tsp.getX() - sp.getX(),
                tsp.getY() - sp.getY()))));

        nx = sp.getX();
        ny = sp.getY();

        nz = sp.getZ() - Z(16);

        // Spawn a shot
        // Inserting and setting up variables

        PlaySound(DIGI_CGMAGIC, sp, v3df_follow);

        w = SpawnSprite(STAT_MISSILE, COOLG_FIRE, s_CoolgFire[0], sp.getSectnum(),
                nx, ny, nz, tsp.getAng(),
                COOLG_FIRE_VELOCITY);

        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return false;
        }

        // wp.owner = SpriteNum;
        SetOwner(SpriteNum, w);
        wp.setHitag(LUMINOUS);
        wp.setYrepeat(18);
        wp.setXrepeat(18);
        wp.setShade(-40);
        wp.setZvel(0);
        wp.setAng(nang);
        wp.setClipdist(32 >> 2);
        wu.ceiling_dist = Z(4);
        wu.floor_dist = Z(4);
        if (u.ID == RIPPER_RUN_R0) {
            wu.spal = 27;
            wp.setPal(27); // Bright Green
        } else {
            wu.spal = 25;
            wp.setPal(25); // Bright Red
        }

        PlaySound(DIGI_MAGIC1, wp, v3df_follow | v3df_doppler);

        // find the distance to the target (player)
        dist = Distance(nx, ny, tsp.getX(),
                tsp.getY());

        if (dist != 0)
        // (velocity * difference between the target and the throwing star) /
        // distance
        {
            wp.setZvel(((wp.getXvel() * (SPRITEp_UPPER(tsp) - nz)) / dist));
        }

        wu.xchange = MOVEx(wp.getXvel(),
                wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(),
                wp.getAng());
        wu.zchange = wp.getZvel();

        nx = (int) (((long) 728 * (long) EngineUtils.sin(NORM_ANGLE(nang + 512))) >> 14);
        ny = (int) (((long) 728 * (long) EngineUtils.sin(nang)) >> 14);

        move_missile(w, nx, ny, 0, wu.ceiling_dist, wu.floor_dist, 0, 3);

        return (w) != 0;
    }    private static final EnemyStateGroup sg_RipperFall = new EnemyStateGroup(s_RipperFall[0], s_RipperFall[1], s_RipperFall[2], s_RipperFall[3], s_RipperFall[4]);

    public static void RipperSaveable() {
        SaveData(InitCoolgFire);
        SaveData(DoActorDeathMove);

        SaveData(InitRipperHang);
        SaveData(DoRipperHang);
        SaveData(DoRipperHangJF);

        SaveData(DoRipperBeginJumpAttack);
        SaveData(DoRipperMoveJump);

        SaveData(NullRipper);
        SaveData(DoRipperPain);
        SaveData(DoRipperStandHeart);
        SaveData(DoRipperMove);
        SaveData(RipperPersonality);

        SaveData(RipperAttrib);

        SaveData(s_RipperRun);
        SaveGroup(sg_RipperRun);
        SaveData(s_RipperStand);
        SaveGroup(sg_RipperStand);
        SaveData(s_RipperSwipe);
        SaveGroup(sg_RipperSwipe);
        SaveData(s_RipperSpew);
        SaveGroup(sg_RipperSpew);
        SaveData(s_RipperHeart);
        SaveGroup(sg_RipperHeart);
        SaveData(s_RipperHang);
        SaveGroup(sg_RipperHang);
        SaveData(s_RipperPain);
        SaveGroup(sg_RipperPain);
        SaveData(s_RipperJump);
        SaveGroup(sg_RipperJump);
        SaveData(s_RipperFall);
        SaveGroup(sg_RipperFall);
        SaveData(s_RipperJumpAttack);
        SaveGroup(sg_RipperJumpAttack);
        SaveData(s_RipperHangJump);
        SaveGroup(sg_RipperHangJump);
        SaveData(s_RipperHangFall);
        SaveGroup(sg_RipperHangFall);
        SaveData(s_RipperDie);
        SaveData(s_RipperDead);
        SaveGroup(sg_RipperDie);
        SaveGroup(sg_RipperDead);
        SaveData(s_RipperDeathJump);
        SaveData(s_RipperDeathFall);
        SaveGroup(sg_RipperDeathJump);
        SaveGroup(sg_RipperDeathFall);

        SaveData(RipperActionSet);
        SaveData(RipperBrownActionSet);
    }


    private static final EnemyStateGroup sg_RipperSpew = new EnemyStateGroup(s_RipperSpew[0], s_RipperSpew[1], s_RipperSpew[2], s_RipperSpew[3], s_RipperSpew[4]);
    private static final EnemyStateGroup sg_RipperJumpAttack = new EnemyStateGroup(s_RipperJumpAttack[0], s_RipperJumpAttack[1], s_RipperJumpAttack[2], s_RipperJumpAttack[3], s_RipperJumpAttack[4]);
    private static final EnemyStateGroup sg_RipperHeart = new EnemyStateGroup(s_RipperHeart[0], s_RipperHeart[1], s_RipperHeart[2], s_RipperHeart[3], s_RipperHeart[4]);
    private static final EnemyStateGroup sg_RipperHang = new EnemyStateGroup(s_RipperHang[0], s_RipperHang[1], s_RipperHang[2], s_RipperHang[3], s_RipperHang[4]);
    private static final EnemyStateGroup sg_RipperSwipe = new EnemyStateGroup(s_RipperSwipe[0], s_RipperSwipe[1], s_RipperSwipe[2], s_RipperSwipe[3], s_RipperSwipe[4]);
    private static final EnemyStateGroup sg_RipperHangJump = new EnemyStateGroup(s_RipperHangJump[0], s_RipperHangJump[1], s_RipperHangJump[2], s_RipperHangJump[3], s_RipperHangJump[4]);
    private static final EnemyStateGroup sg_RipperHangFall = new EnemyStateGroup(s_RipperHangFall[0], s_RipperHangFall[1], s_RipperHangFall[2], s_RipperHangFall[3], s_RipperHangFall[4]);


    private static final Actor_Action_Set RipperActionSet = new Actor_Action_Set(sg_RipperStand, sg_RipperRun, sg_RipperJump, sg_RipperFall, null, // sg_RipperCrawl,
            null, // sg_RipperSwim,
            null, // sg_RipperFly,
            null, // sg_RipperRise,
            null, // sg_RipperSit,
            null, // sg_RipperLook,
            null, // climb
            sg_RipperPain, sg_RipperDie, null, // sg_RipperHariKari,
            sg_RipperDead, sg_RipperDeathJump, sg_RipperDeathFall, new StateGroup[]{sg_RipperSwipe, sg_RipperSpew}, new short[]{800, 1024}, new StateGroup[]{sg_RipperJumpAttack, sg_RipperSpew}, new short[]{400, 1024}, new StateGroup[]{sg_RipperHeart, sg_RipperHang}, null, null);
    private static final Actor_Action_Set RipperBrownActionSet = new Actor_Action_Set(sg_RipperStand, sg_RipperRun, sg_RipperJump, sg_RipperFall, null, // sg_RipperCrawl,
            null, // sg_RipperSwim,
            null, // sg_RipperFly,
            null, // sg_RipperRise,
            null, // sg_RipperSit,
            null, // sg_RipperLook,
            null, // climb
            sg_RipperPain, // pain
            sg_RipperDie, null, // sg_RipperHariKari,
            sg_RipperDead, sg_RipperDeathJump, sg_RipperDeathFall, new StateGroup[]{sg_RipperSwipe}, new short[]{1024}, new StateGroup[]{sg_RipperJumpAttack, sg_RipperSwipe}, new short[]{800, 1024}, new StateGroup[]{sg_RipperHeart, sg_RipperHang}, null, null);













//
// HANGING - Jumping/Falling/Stationary
//









//
// JUMP ATTACK
//







//
// STD MOVEMENT
//
















}
