package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Enemies.Ninja.InitEnemyUzi;
import static ru.m210projects.Wang.Enemies.Sumo.BossSpriteNum;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Rooms.cz;
import static ru.m210projects.Wang.Rooms.fz;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.DIV2;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Zilla {

    private static final Animator DoZillaMove = new Animator((Animator.Runnable) Zilla::DoZillaMove);
    private static final Animator DoZillaStomp = new Animator((Animator.Runnable) Zilla::DoZillaStomp);
    private static final Animator NullZilla = new Animator((Animator.Runnable) Zilla::NullZilla);
    private static final Animator InitZillaRail = new Animator((Animator.Runnable) Zilla::InitZillaRail);
    private static final Animator InitZillaRocket = new Animator(Zilla::InitZillaRocket);
    private static final Animator DoZillaDeathMelt = new Animator((Animator.Runnable) Zilla::DoZillaDeathMelt);


    public static final MISSILE_PLACEMENT[] mp2 = {new MISSILE_PLACEMENT(600 * 6, 400, 512),
 new MISSILE_PLACEMENT(900 * 6, 400, 512),
 new MISSILE_PLACEMENT(1100 * 6, 400, 512),
 new MISSILE_PLACEMENT(600 * 6, 400, -512),
 new MISSILE_PLACEMENT(900 * 6, 400, -512),
 new MISSILE_PLACEMENT(1100 * 6, 400, -512)};
    private static VOC3D handle;
    private static boolean alreadydid = false;

    private static final Decision[] ZillaBattle = {new Decision(100, InitActorRunAway),
 new Decision(690, InitActorMoveCloser),
 new Decision(692, InitActorAlertNoise),
 new Decision(1024, InitActorAttack)};

    private static final Decision[] ZillaOffense = {new Decision(100, InitActorRunAway),
 new Decision(690, InitActorMoveCloser),
 new Decision(692, InitActorAlertNoise),
 new Decision(1024, InitActorAttack)};

    private static final Decision[] ZillaBroadcast = {new Decision(2, InitActorAlertNoise),
 new Decision(4, InitActorAmbientNoise),
 new Decision(1024, InitActorDecide)};

    private static final Decision[] ZillaSurprised = {new Decision(700, InitActorMoveCloser),
 new Decision(703, InitActorAlertNoise),
 new Decision(1024, InitActorDecide)};

    private static final Decision[] ZillaEvasive = {new Decision(1024, InitActorWanderAround)};

    private static final Decision[] ZillaLostTarget = {new Decision(900, InitActorFindPlayer),
 new Decision(1024, InitActorWanderAround)};

    private static final Decision[] ZillaCloseRange = {new Decision(1024, InitActorAttack)};

    private static final Personality ZillaPersonality = new Personality(ZillaBattle, ZillaOffense, ZillaBroadcast, ZillaSurprised, ZillaEvasive, ZillaLostTarget, ZillaCloseRange, ZillaCloseRange);

    private static final ATTRIBUTE ZillaAttrib = new ATTRIBUTE(new short[]{100, 100, 100, 100}, // Speeds
            new short[]{3, 0, 0, 0}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_Z17010, DIGI_Z17010, DIGI_Z17025, DIGI_Z17052, DIGI_Z17025, 0, 0, 0, 0, 0});

    //////////////////////
    //
    // ZILLA RUN
    //
    //////////////////////

    public static final int ZILLA_RATE = 48;


    private static final State[][] s_ZillaRun = {{new State(ZILLA_RUN_R0, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[0][1]},
            new State(ZILLA_RUN_R0 + 1, SF_QUICK_CALL, DoZillaStomp),
 // s_ZillaRun[0][2]},
            new State(ZILLA_RUN_R0 + 1, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[0][3]},
            new State(ZILLA_RUN_R0 + 2, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[0][4]},
            new State(ZILLA_RUN_R0 + 3, SF_QUICK_CALL, DoZillaStomp),
 // s_ZillaRun[0][5]},
            new State(ZILLA_RUN_R0 + 3, ZILLA_RATE, DoZillaMove),
// s_ZillaRun[0][0]}
    }, {new State(ZILLA_RUN_R1, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[1][1]},
            new State(ZILLA_RUN_R1 + 1, SF_QUICK_CALL, DoZillaStomp),
 // s_ZillaRun[1][2]},
            new State(ZILLA_RUN_R1 + 1, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[1][3]},
            new State(ZILLA_RUN_R1 + 2, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[1][4]},
            new State(ZILLA_RUN_R1 + 3, SF_QUICK_CALL, DoZillaStomp),
 // s_ZillaRun[1][5]},
            new State(ZILLA_RUN_R1 + 3, ZILLA_RATE, DoZillaMove),
// s_ZillaRun[1][0]}
    }, {new State(ZILLA_RUN_R2, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[2][1]},
            new State(ZILLA_RUN_R2 + 1, SF_QUICK_CALL, DoZillaStomp),
 // s_ZillaRun[2][2]},
            new State(ZILLA_RUN_R2 + 1, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[2][3]},
            new State(ZILLA_RUN_R2 + 2, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[2][4]},
            new State(ZILLA_RUN_R2 + 3, SF_QUICK_CALL, DoZillaStomp),
 // s_ZillaRun[2][5]},
            new State(ZILLA_RUN_R2 + 3, ZILLA_RATE, DoZillaMove),
// s_ZillaRun[2][0]}
    }, {new State(ZILLA_RUN_R3, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[3][1]},
            new State(ZILLA_RUN_R3 + 1, SF_QUICK_CALL, DoZillaStomp),
 // s_ZillaRun[3][2]},
            new State(ZILLA_RUN_R3 + 1, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[3][3]},
            new State(ZILLA_RUN_R3 + 2, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[3][4]},
            new State(ZILLA_RUN_R3 + 3, SF_QUICK_CALL, DoZillaStomp),
 // s_ZillaRun[3][5]},
            new State(ZILLA_RUN_R3 + 3, ZILLA_RATE, DoZillaMove),
// s_ZillaRun[3][0]}
    }, {new State(ZILLA_RUN_R4, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[4][1]},
            new State(ZILLA_RUN_R4 + 1, SF_QUICK_CALL, DoZillaStomp),
 // s_ZillaRun[4][2]},
            new State(ZILLA_RUN_R4 + 1, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[4][3]},
            new State(ZILLA_RUN_R4 + 2, ZILLA_RATE, DoZillaMove),
 // s_ZillaRun[4][4]},
            new State(ZILLA_RUN_R4 + 3, SF_QUICK_CALL, DoZillaStomp),
 // s_ZillaRun[4][5]},
            new State(ZILLA_RUN_R4 + 3, ZILLA_RATE, DoZillaMove),
// s_ZillaRun[4][0]}
    }};

    //////////////////////
    //
    // ZILLA STAND
    //
    //////////////////////

    private static final State[][] s_ZillaStand = {{new State(ZILLA_RUN_R0, ZILLA_RATE, DoZillaMove).setNext(),
// s_ZillaStand[0][0]}
    }, {new State(ZILLA_RUN_R1, ZILLA_RATE, DoZillaMove).setNext(),
// s_ZillaStand[1][0]}
    }, {new State(ZILLA_RUN_R2, ZILLA_RATE, DoZillaMove).setNext(),
// s_ZillaStand[2][0]}
    }, {new State(ZILLA_RUN_R3, ZILLA_RATE, DoZillaMove).setNext(),
// s_ZillaStand[3][0]}
    }, {new State(ZILLA_RUN_R4, ZILLA_RATE, DoZillaMove).setNext(),
// s_ZillaStand[4][0]}
    }};

    //////////////////////
    //
    // ZILLA PAIN
    //
    //////////////////////

    public static final int ZILLA_PAIN_RATE = 30;


    private static final State[][] s_ZillaPain = {{new State(ZILLA_PAIN_R0, ZILLA_PAIN_RATE, NullZilla),
 // s_ZillaPain[0][1]},
            new State(ZILLA_PAIN_R0, SF_QUICK_CALL, InitActorDecide),
// s_ZillaPain[0][0]}
    }, {new State(ZILLA_PAIN_R1, ZILLA_PAIN_RATE, NullZilla),
 // s_ZillaPain[1][1]},
            new State(ZILLA_PAIN_R1, SF_QUICK_CALL, InitActorDecide),
// s_ZillaPain[1][0]}
    }, {new State(ZILLA_PAIN_R2, ZILLA_PAIN_RATE, NullZilla),
 // s_ZillaPain[2][1]},
            new State(ZILLA_PAIN_R2, SF_QUICK_CALL, InitActorDecide),
// s_ZillaPain[2][0]}
    }, {new State(ZILLA_PAIN_R3, ZILLA_PAIN_RATE, NullZilla),
 // s_ZillaPain[3][1]},
            new State(ZILLA_PAIN_R3, SF_QUICK_CALL, InitActorDecide),
// s_ZillaPain[3][0]}
    }, {new State(ZILLA_PAIN_R4, ZILLA_PAIN_RATE, NullZilla),
 // s_ZillaPain[4][1]},
            new State(ZILLA_PAIN_R4, SF_QUICK_CALL, InitActorDecide),
// s_ZillaPain[4][0]}
    }};

    //////////////////////
    //
    // ZILLA RAIL
    //
    //////////////////////

    public static final int ZILLA_RAIL_RATE = 12;


    private static final State[][] s_ZillaRail = {{new State(ZILLA_RAIL_R0, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[0][1]},
            new State(ZILLA_RAIL_R0 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[0][2]},
            new State(ZILLA_RAIL_R0 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[0][3]},
            new State(ZILLA_RAIL_R0 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[0][4]},
            new State(ZILLA_RAIL_R0, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[0][5]},
            new State(ZILLA_RAIL_R0 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[0][6]},
            new State(ZILLA_RAIL_R0 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[0][7]},
            new State(ZILLA_RAIL_R0 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[0][8]},
            new State(ZILLA_RAIL_R0, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[0][9]},
            new State(ZILLA_RAIL_R0 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[0][10]},
            new State(ZILLA_RAIL_R0 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[0][11]},
            new State(ZILLA_RAIL_R0 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[0][12]},
            new State(ZILLA_RAIL_R0 + 3, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[0][13]},
            new State(ZILLA_RAIL_R0 + 3, SF_QUICK_CALL, InitActorDecide),
// s_ZillaRail[0][0]}
    }, {new State(ZILLA_RAIL_R1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[1][1]},
            new State(ZILLA_RAIL_R1 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[1][2]},
            new State(ZILLA_RAIL_R1 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[1][3]},
            new State(ZILLA_RAIL_R1 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[1][4]},
            new State(ZILLA_RAIL_R1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[1][5]},
            new State(ZILLA_RAIL_R1 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[1][6]},
            new State(ZILLA_RAIL_R1 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[1][7]},
            new State(ZILLA_RAIL_R1 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[1][8]},
            new State(ZILLA_RAIL_R1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[1][9]},
            new State(ZILLA_RAIL_R1 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[1][10]},
            new State(ZILLA_RAIL_R1 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[1][11]},
            new State(ZILLA_RAIL_R1 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[1][12]},
            new State(ZILLA_RAIL_R1 + 3, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[1][13]},
            new State(ZILLA_RAIL_R1 + 3, SF_QUICK_CALL, InitActorDecide),
// s_ZillaRail[1][0]}
    }, {new State(ZILLA_RAIL_R2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[2][1]},
            new State(ZILLA_RAIL_R2 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[2][2]},
            new State(ZILLA_RAIL_R2 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[2][3]},
            new State(ZILLA_RAIL_R2 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[2][4]},
            new State(ZILLA_RAIL_R2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[2][5]},
            new State(ZILLA_RAIL_R2 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[2][6]},
            new State(ZILLA_RAIL_R2 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[2][7]},
            new State(ZILLA_RAIL_R2 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[2][8]},
            new State(ZILLA_RAIL_R2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[2][9]},
            new State(ZILLA_RAIL_R2 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[2][10]},
            new State(ZILLA_RAIL_R2 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[2][11]},
            new State(ZILLA_RAIL_R2 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[2][12]},
            new State(ZILLA_RAIL_R2 + 3, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[2][13]},
            new State(ZILLA_RAIL_R2 + 3, SF_QUICK_CALL, InitActorDecide),
// s_ZillaRail[2][0]}
    }, {new State(ZILLA_RAIL_R3, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[3][1]},
            new State(ZILLA_RAIL_R3 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[3][2]},
            new State(ZILLA_RAIL_R3 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[3][3]},
            new State(ZILLA_RAIL_R3 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[3][4]},
            new State(ZILLA_RAIL_R3, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[3][5]},
            new State(ZILLA_RAIL_R3 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[3][6]},
            new State(ZILLA_RAIL_R3 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[3][7]},
            new State(ZILLA_RAIL_R3 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[3][8]},
            new State(ZILLA_RAIL_R3, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[3][9]},
            new State(ZILLA_RAIL_R3 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[3][10]},
            new State(ZILLA_RAIL_R3 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[3][11]},
            new State(ZILLA_RAIL_R3 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[3][12]},
            new State(ZILLA_RAIL_R3 + 3, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[3][13]},
            new State(ZILLA_RAIL_R3 + 3, SF_QUICK_CALL, InitActorDecide),
// s_ZillaRail[3][0]}
    }, {new State(ZILLA_RAIL_R4, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[4][1]},
            new State(ZILLA_RAIL_R4 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[4][2]},
            new State(ZILLA_RAIL_R4 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[4][3]},
            new State(ZILLA_RAIL_R4 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[4][4]},
            new State(ZILLA_RAIL_R4, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[4][5]},
            new State(ZILLA_RAIL_R4 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[4][6]},
            new State(ZILLA_RAIL_R4 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[4][7]},
            new State(ZILLA_RAIL_R4 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[4][8]},
            new State(ZILLA_RAIL_R4, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[4][9]},
            new State(ZILLA_RAIL_R4 + 1, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[4][10]},
            new State(ZILLA_RAIL_R4 + 2, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[4][11]},
            new State(ZILLA_RAIL_R4 + 3, SF_QUICK_CALL, InitZillaRail),
 // s_ZillaRail[4][12]},
            new State(ZILLA_RAIL_R4 + 3, ZILLA_RAIL_RATE, NullZilla),
 // s_ZillaRail[4][13]},
            new State(ZILLA_RAIL_R4 + 3, SF_QUICK_CALL, InitActorDecide),
// s_ZillaRail[4][0]}
    }};

    //////////////////////
    //
    // ZILLA ROCKET
    //
    //////////////////////

    public static final int ZILLA_ROCKET_RATE = 12;


    private static final State[][] s_ZillaRocket = {{new State(ZILLA_ROCKET_R0, ZILLA_ROCKET_RATE, NullZilla),
 // s_ZillaRocket[0][1]},
            new State(ZILLA_ROCKET_R0 + 1, ZILLA_ROCKET_RATE, NullZilla),
 // s_ZillaRocket[0][2]},
            new State(ZILLA_ROCKET_R0 + 2, ZILLA_ROCKET_RATE * 4, NullZilla),
 // s_ZillaRocket[0][3]},
            new State(ZILLA_ROCKET_R0 + 2, SF_QUICK_CALL, InitZillaRocket),
 // s_ZillaRocket[0][4]},
            new State(ZILLA_ROCKET_R0 + 2, ZILLA_ROCKET_RATE * 4, NullZilla),
 // s_ZillaRocket[0][5]},
            new State(ZILLA_ROCKET_R0 + 3, SF_QUICK_CALL, InitActorDecide),
 // s_ZillaRocket[0][6]},
            new State(ZILLA_ROCKET_R0 + 3, ZILLA_ROCKET_RATE * 10, NullZilla),
// s_ZillaRocket[0][5]}
    }, {new State(ZILLA_ROCKET_R1, ZILLA_ROCKET_RATE, NullZilla),
 // s_ZillaRocket[1][1]},
            new State(ZILLA_ROCKET_R1 + 1, ZILLA_ROCKET_RATE, NullZilla),
 // s_ZillaRocket[1][2]},
            new State(ZILLA_ROCKET_R1 + 2, ZILLA_ROCKET_RATE * 4, NullZilla),
 // s_ZillaRocket[1][3]},
            new State(ZILLA_ROCKET_R1 + 2, SF_QUICK_CALL, InitZillaRocket),
 // s_ZillaRocket[1][4]},
            new State(ZILLA_ROCKET_R1 + 2, ZILLA_ROCKET_RATE * 4, NullZilla),
 // s_ZillaRocket[1][5]},
            new State(ZILLA_ROCKET_R1 + 3, SF_QUICK_CALL, InitActorDecide),
 // s_ZillaRocket[1][6]},
            new State(ZILLA_ROCKET_R1 + 3, ZILLA_ROCKET_RATE * 10, NullZilla),
// s_ZillaRocket[1][5]}
    }, {new State(ZILLA_ROCKET_R2, ZILLA_ROCKET_RATE, NullZilla),
 // s_ZillaRocket[2][1]},
            new State(ZILLA_ROCKET_R2 + 1, ZILLA_ROCKET_RATE, NullZilla),
 // s_ZillaRocket[2][2]},
            new State(ZILLA_ROCKET_R2 + 2, ZILLA_ROCKET_RATE * 4, NullZilla),
 // s_ZillaRocket[2][3]},
            new State(ZILLA_ROCKET_R2 + 2, SF_QUICK_CALL, InitZillaRocket),
 // s_ZillaRocket[2][4]},
            new State(ZILLA_ROCKET_R2 + 2, ZILLA_ROCKET_RATE * 4, NullZilla),
 // s_ZillaRocket[2][5]},
            new State(ZILLA_ROCKET_R2 + 3, SF_QUICK_CALL, InitActorDecide),
 // s_ZillaRocket[2][6]},
            new State(ZILLA_ROCKET_R2 + 3, ZILLA_ROCKET_RATE * 10, NullZilla),
// s_ZillaRocket[2][5]}
    }, {new State(ZILLA_ROCKET_R3, ZILLA_ROCKET_RATE, NullZilla),
 // s_ZillaRocket[3][1]},
            new State(ZILLA_ROCKET_R3 + 1, ZILLA_ROCKET_RATE, NullZilla),
 // s_ZillaRocket[3][2]},
            new State(ZILLA_ROCKET_R3 + 2, ZILLA_ROCKET_RATE * 4, NullZilla),
 // s_ZillaRocket[3][3]},
            new State(ZILLA_ROCKET_R3 + 2, SF_QUICK_CALL, InitZillaRocket),
 // s_ZillaRocket[3][4]},
            new State(ZILLA_ROCKET_R3 + 2, ZILLA_ROCKET_RATE * 4, NullZilla),
 // s_ZillaRocket[3][5]},
            new State(ZILLA_ROCKET_R3 + 3, SF_QUICK_CALL, InitActorDecide),
 // s_ZillaRocket[3][6]},
            new State(ZILLA_ROCKET_R3 + 3, ZILLA_ROCKET_RATE * 10, NullZilla),
// s_ZillaRocket[3][5]}
    }, {new State(ZILLA_ROCKET_R4, ZILLA_ROCKET_RATE, NullZilla),
 // s_ZillaRocket[4][1]},
            new State(ZILLA_ROCKET_R4 + 1, ZILLA_ROCKET_RATE, NullZilla),
 // s_ZillaRocket[4][2]},
            new State(ZILLA_ROCKET_R4 + 2, ZILLA_ROCKET_RATE * 4, NullZilla),
 // s_ZillaRocket[4][3]},
            new State(ZILLA_ROCKET_R4 + 2, SF_QUICK_CALL, InitZillaRocket),
 // s_ZillaRocket[4][4]},
            new State(ZILLA_ROCKET_R4 + 2, ZILLA_ROCKET_RATE * 4, NullZilla),
 // s_ZillaRocket[4][5]},
            new State(ZILLA_ROCKET_R4 + 3, SF_QUICK_CALL, InitActorDecide),
 // s_ZillaRocket[4][6]},
            new State(ZILLA_ROCKET_R4 + 3, ZILLA_ROCKET_RATE * 10, NullZilla),
// s_ZillaRocket[4][5]}
    }};

    //////////////////////
    //
    // ZILLA UZI
    //
    //////////////////////

    public static final int ZILLA_UZI_RATE = 8;

    private static final State[][] s_ZillaUzi = {{new State(ZILLA_SHOOT_R0, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[0][1]},
            new State(ZILLA_SHOOT_R0, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[0][2]},
            new State(ZILLA_SHOOT_R0, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[0][3]},
            new State(ZILLA_SHOOT_R0, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[0][4]},
            new State(ZILLA_SHOOT_R0, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[0][5]},
            new State(ZILLA_SHOOT_R0, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[0][6]},
            new State(ZILLA_SHOOT_R0, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[0][7]},
            new State(ZILLA_SHOOT_R0, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[0][8]},
            new State(ZILLA_SHOOT_R0, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[0][9]},
            new State(ZILLA_SHOOT_R0, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[0][10]},
            new State(ZILLA_SHOOT_R0, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[0][11]},
            new State(ZILLA_SHOOT_R0, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[0][12]},
            new State(ZILLA_SHOOT_R0, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[0][13]},
            new State(ZILLA_SHOOT_R0, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[0][14]},
            new State(ZILLA_SHOOT_R0, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[0][15]},
            new State(ZILLA_SHOOT_R0, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[0][16]},
            new State(ZILLA_SHOOT_R0, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_ZillaUzi[0][16]},
    }, {new State(ZILLA_SHOOT_R1, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[1][1]},
            new State(ZILLA_SHOOT_R1, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[1][2]},
            new State(ZILLA_SHOOT_R1, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[1][3]},
            new State(ZILLA_SHOOT_R1, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[1][4]},
            new State(ZILLA_SHOOT_R1, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[1][5]},
            new State(ZILLA_SHOOT_R1, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[1][6]},
            new State(ZILLA_SHOOT_R1, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[1][7]},
            new State(ZILLA_SHOOT_R1, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[1][8]},
            new State(ZILLA_SHOOT_R1, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[1][9]},
            new State(ZILLA_SHOOT_R1, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[1][10]},
            new State(ZILLA_SHOOT_R1, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[1][11]},
            new State(ZILLA_SHOOT_R1, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[1][12]},
            new State(ZILLA_SHOOT_R1, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[1][13]},
            new State(ZILLA_SHOOT_R1, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[1][14]},
            new State(ZILLA_SHOOT_R1, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[1][15]},
            new State(ZILLA_SHOOT_R1, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[1][16]},
            new State(ZILLA_SHOOT_R1, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_ZillaUzi[1][16]},
    }, {new State(ZILLA_SHOOT_R2, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[2][1]},
            new State(ZILLA_SHOOT_R2, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[2][2]},
            new State(ZILLA_SHOOT_R2, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[2][3]},
            new State(ZILLA_SHOOT_R2, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[2][4]},
            new State(ZILLA_SHOOT_R2, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[2][5]},
            new State(ZILLA_SHOOT_R2, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[2][6]},
            new State(ZILLA_SHOOT_R2, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[2][7]},
            new State(ZILLA_SHOOT_R2, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[2][8]},
            new State(ZILLA_SHOOT_R2, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[2][9]},
            new State(ZILLA_SHOOT_R2, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[2][10]},
            new State(ZILLA_SHOOT_R2, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[2][11]},
            new State(ZILLA_SHOOT_R2, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[2][12]},
            new State(ZILLA_SHOOT_R2, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[2][13]},
            new State(ZILLA_SHOOT_R2, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[2][14]},
            new State(ZILLA_SHOOT_R2, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[2][15]},
            new State(ZILLA_SHOOT_R2, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[2][16]},
            new State(ZILLA_SHOOT_R2, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_ZillaUzi[2][16]},
    }, {new State(ZILLA_SHOOT_R3, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[3][1]},
            new State(ZILLA_SHOOT_R3, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[3][2]},
            new State(ZILLA_SHOOT_R3, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[3][3]},
            new State(ZILLA_SHOOT_R3, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[3][4]},
            new State(ZILLA_SHOOT_R3, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[3][5]},
            new State(ZILLA_SHOOT_R3, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[3][6]},
            new State(ZILLA_SHOOT_R3, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[3][7]},
            new State(ZILLA_SHOOT_R3, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[3][8]},
            new State(ZILLA_SHOOT_R3, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[3][9]},
            new State(ZILLA_SHOOT_R3, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[3][10]},
            new State(ZILLA_SHOOT_R3, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[3][11]},
            new State(ZILLA_SHOOT_R3, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[3][12]},
            new State(ZILLA_SHOOT_R3, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[3][13]},
            new State(ZILLA_SHOOT_R3, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[3][14]},
            new State(ZILLA_SHOOT_R3, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[3][15]},
            new State(ZILLA_SHOOT_R3, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[3][16]},
            new State(ZILLA_SHOOT_R3, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_ZillaUzi[3][16]},
    }, {new State(ZILLA_SHOOT_R4, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[4][1]},
            new State(ZILLA_SHOOT_R4, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[4][2]},
            new State(ZILLA_SHOOT_R4, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[4][3]},
            new State(ZILLA_SHOOT_R4, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[4][4]},
            new State(ZILLA_SHOOT_R4, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[4][5]},
            new State(ZILLA_SHOOT_R4, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[4][6]},
            new State(ZILLA_SHOOT_R4, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[4][7]},
            new State(ZILLA_SHOOT_R4, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[4][8]},
            new State(ZILLA_SHOOT_R4, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[4][9]},
            new State(ZILLA_SHOOT_R4, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[4][10]},
            new State(ZILLA_SHOOT_R4, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[4][11]},
            new State(ZILLA_SHOOT_R4, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[4][12]},
            new State(ZILLA_SHOOT_R4, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[4][13]},
            new State(ZILLA_SHOOT_R4, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[4][14]},
            new State(ZILLA_SHOOT_R4, ZILLA_UZI_RATE, NullZilla),
 // s_ZillaUzi[4][15]},
            new State(ZILLA_SHOOT_R4, SF_QUICK_CALL, InitEnemyUzi),
 // s_ZillaUzi[4][16]},
            new State(ZILLA_SHOOT_R4, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_ZillaUzi[4][16]},
    },};

    //////////////////////
    //
    // ZILLA DIE
    //
    //////////////////////

    public static final int ZILLA_DIE_RATE = 30;


    private static final State[] s_ZillaDie = {new State(ZILLA_DIE, ZILLA_DIE_RATE * 15, DoZillaDeathMelt),
 // s_ZillaDie[1]},
            new State(ZILLA_DIE + 1, ZILLA_DIE_RATE, NullZilla),
 // s_ZillaDie[2]},
            new State(ZILLA_DIE + 2, ZILLA_DIE_RATE, NullZilla),
 // s_ZillaDie[3]},
            new State(ZILLA_DIE + 3, ZILLA_DIE_RATE, NullZilla),
 // s_ZillaDie[4]},
            new State(ZILLA_DIE + 4, ZILLA_DIE_RATE, NullZilla),
 // s_ZillaDie[5]},
            new State(ZILLA_DIE + 5, ZILLA_DIE_RATE, NullZilla),
 // s_ZillaDie[6]},
            new State(ZILLA_DIE + 6, ZILLA_DIE_RATE, NullZilla),
 // s_ZillaDie[7]},
            new State(ZILLA_DIE + 7, ZILLA_DIE_RATE * 3, NullZilla),
 // s_ZillaDie[8]},
            new State(ZILLA_DEAD, ZILLA_DIE_RATE, DoActorDebris).setNext(),
// s_ZillaDie[8]}
    };

    private static final State[] s_ZillaDead = {new State(ZILLA_DEAD, ZILLA_DIE_RATE, DoActorDebris).setNext(),
// s_ZillaDead[0]},
    };

    private static final EnemyStateGroup sg_ZillaStand = new EnemyStateGroup(s_ZillaStand[0], s_ZillaStand[1], s_ZillaStand[2], s_ZillaStand[3], s_ZillaStand[4]);
    private static final EnemyStateGroup sg_ZillaRun = new EnemyStateGroup(s_ZillaRun[0], s_ZillaRun[1], s_ZillaRun[2], s_ZillaRun[3], s_ZillaRun[4]);
    private static final EnemyStateGroup sg_ZillaPain = new EnemyStateGroup(s_ZillaPain[0], s_ZillaPain[1], s_ZillaPain[2], s_ZillaPain[3], s_ZillaPain[4]);
    private static final EnemyStateGroup sg_ZillaDie = new EnemyStateGroup(s_ZillaDie);
    private static final EnemyStateGroup sg_ZillaDead = new EnemyStateGroup(s_ZillaDead);
    private static final EnemyStateGroup sg_ZillaRail = new EnemyStateGroup(s_ZillaRail[0], s_ZillaRail[1], s_ZillaRail[2], s_ZillaRail[3], s_ZillaRail[4]);
    private static final EnemyStateGroup sg_ZillaUzi = new EnemyStateGroup(s_ZillaUzi[0], s_ZillaUzi[1], s_ZillaUzi[2], s_ZillaUzi[3], s_ZillaUzi[4]);
    private static final EnemyStateGroup sg_ZillaRocket = new EnemyStateGroup(s_ZillaRocket[0], s_ZillaRocket[1], s_ZillaRocket[2], s_ZillaRocket[3], s_ZillaRocket[4]);
    
    public static void InitZillaStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[] {
                sg_ZillaStand,
                sg_ZillaRun,
                sg_ZillaPain,
                sg_ZillaDie,
                sg_ZillaDead,
                sg_ZillaRail,
                sg_ZillaUzi,
                sg_ZillaRocket
        }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);

                if (sg == sg_ZillaRocket) sg.getGroup()[rot][6].setNext(sg.getGroup()[rot][5]);
            }
        }
    }

    private static final Actor_Action_Set ZillaActionSet = new Actor_Action_Set(sg_ZillaStand, sg_ZillaRun, null, null, null, null, null, null, null, null, null, // climb
            sg_ZillaPain, // pain
            sg_ZillaDie, null, sg_ZillaDead, null, null, new StateGroup[]{sg_ZillaUzi, sg_ZillaRail}, new short[]{950, 1024}, new StateGroup[]{sg_ZillaUzi, sg_ZillaRocket, sg_ZillaRail}, new short[]{400, 950, 1024}, null, null, null);

    public static void SetupZilla(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        USER u;

        if (TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, ZILLA_RUN_R0, s_ZillaRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = 6000;
        }

        if (Skill == 0) {
            u.Health = 2000;
        }
        if (Skill == 1) {
            u.Health = 4000;
        }

        ChangeState(SpriteNum, s_ZillaRun[0][0]);
        u.Attrib = ZillaAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_ZillaDie[0];
        u.setRot(sg_ZillaRun);

        EnemyDefaults(SpriteNum, ZillaActionSet, ZillaPersonality);

        sp.setClipdist((512) >> 2);
        sp.setXrepeat(97);
        sp.setYrepeat(79);

        alreadydid = false;
    }

    public static void InitZillaRail(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz;
        int w;
        int oclipdist;
        int zvel;

        PlaySound(DIGI_RAILFIRE, sp, v3df_dontpan | v3df_doppler);

        // Make sprite shade brighter
        u.Vis = 128;

        nx = sp.getX();
        ny = sp.getY();

        nz = SPRITEp_TOS(sp);

        // Spawn a shot
        // Inserting and setting up variables

        w =  SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R1, s_Rail[0][0], sp.getSectnum(),
 nx, ny, nz, sp.getAng(),
 1200);

        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        SetOwner(SpriteNum, w);
        wp.setYrepeat(52);
        wp.setXrepeat(52);
        wp.setShade(-15);
        zvel = (100 * (HORIZ_MULT + 17));

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_Rail);

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = RAIL_RADIUS;
        wu.ceiling_dist =  Z(1);
        wu.floor_dist =  Z(1);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER | CSTAT_SPRITE_INVISIBLE));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        // at certain angles the clipping box was big enough to block the
        // initial positioning
        oclipdist =  sp.getClipdist();
        sp.setClipdist(0);
        wp.setClipdist(32 >> 2);

        wp.setAng(NORM_ANGLE(wp.getAng() + 512));
        HelpMissileLateral(w, 700);
        wp.setAng(NORM_ANGLE(wp.getAng() - 512));

        if (SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        if (TestMissileSetPos(w, DoRailStart, 1200, zvel)) {
            sp.setClipdist(oclipdist);
            KillSprite(w);
            return;
        }

        sp.setClipdist(oclipdist);

        wp.setZvel( (zvel >> 1));
        if (WeaponAutoAim(SpriteNum, w, 32, 0) == -1) {
            wp.setAng(NORM_ANGLE(wp.getAng() - 4));
        } else {
            zvel = wp.getZvel(); // Let autoaiming set zvel now
        }

        wu.xchange = MOVEx(wp.getXvel(),
 wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(),
 wp.getAng());
        wu.zchange = zvel;
    }

    private static void NullZilla(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        engine.getzsofslope(sp.getSectnum(),
 sp.getX(),
 sp.getY(),
 fz, cz);
        u.loz = fz.get();
        u.hiz = cz.get();
        u.lo_sectp = sp.getSectnum();
        u.hi_sectp = sp.getSectnum();
        u.lo_sp = -1;
        u.hi_sp = -1;
        sp.setZ(u.loz);

        DoActorSectorDamage(SpriteNum);
    }

    public static boolean InitZillaRocket(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return false;
        }

        int nx, ny, nz, dist, nang;
        int w = -1, i;

        PlaySound(DIGI_NINJARIOTATTACK, sp, v3df_none);

        // get angle to player and also face player when attacking
        sp.setAng( (nang = EngineUtils.getAngle(tsp.getX() - sp.getX(), tsp.getY() - sp.getY())));

        for (i = 0; i < mp2.length; i++) {
            nx = sp.getX();
            ny = sp.getY();
            nz = sp.getZ() - DIV2(SPRITEp_SIZE_Z(sp)) - Z(8);

            // Spawn a shot
            w =  SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R2, s_Rocket[0][0], sp.getSectnum(),
 nx, ny, nz - Z(8),
                    tsp.getAng(),
 NINJA_BOLT_VELOCITY);

            Sprite wp = boardService.getSprite(w);
            USER wu = getUser(w);
            if (wp == null || wu == null) {
                return false;
            }

            SetOwner(SpriteNum, w);
            wp.setYrepeat(28);
            wp.setXrepeat(28);
            wp.setShade(-15);
            wp.setZvel(0);
            wp.setAng( nang);
            wp.setClipdist(64 >> 2);

            wu.RotNum = 5;
            NewStateGroup(w, WeaponStateGroup.sg_Rocket);
            wu.Radius = 200;
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

            wu.xchange = MOVEx(wp.getXvel(),
 wp.getAng());
            wu.ychange = MOVEy(wp.getXvel(),
 wp.getAng());
            wu.zchange = wp.getZvel();

            // Zilla has seekers!
            if (i != 1 && i != 4) {
                wp.setPal(wu.spal = 17); // White
            } else {
                wu.Flags |= (SPR_FIND_PLAYER);
                wp.setPal(wu.spal = 20); // Yellow
            }

            if (mp2[i].dist_over != 0) {
                wp.setAng(NORM_ANGLE(wp.getAng() + mp2[i].ang));
                HelpMissileLateral(w, mp2[i].dist_over);
                wp.setAng(NORM_ANGLE(wp.getAng() - mp2[i].ang));
            }

            MissileSetPos(w, DoBoltThinMan, mp2[i].dist_out);

            // find the distance to the target (player)
            dist = Distance(wp.getX(),
 wp.getY(),
 tsp.getX(),
 tsp.getY());

            if (dist != 0) {
                wu.zchange = (short) ((wp.getXvel() * (SPRITEp_UPPER(tsp) - wp.getZ())) / dist);
                wp.setZvel(wu.zchange);
            }
        }

        return (w) != 0;
    }

    private static void DoZillaMove(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // Random Zilla taunts
        if (handle != null && !handle.isActive()) {
            int choose = STD_RANDOM_RANGE(1000);
            if (choose > 990) {
                handle = PlaySound(DIGI_Z16004, sp, v3df_none);
            } else if (choose > 985) {
                handle = PlaySound(DIGI_Z16004, sp, v3df_none);
            } else if (choose > 980) {
                handle = PlaySound(DIGI_Z16004, sp, v3df_none);
            } else if (choose > 975) {
                handle = PlaySound(DIGI_Z16004, sp, v3df_none);
            }
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        KeepActorOnFloor(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    private static void DoZillaStomp(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);

        PlaySound(DIGI_ZILLASTOMP, sp, v3df_follow);
    }

    private static void DoZillaDeathMelt(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (RANDOM_RANGE(1000) > 800) {
            SpawnGrenadeExp(SpriteNum);
        }

        u.ID = ZILLA_RUN_R0;
        u.Flags &= ~(SPR_JUMPING | SPR_FALLING | SPR_MOVED);

        if (!cfg.isMuteMusic() && !alreadydid) {
            CDAudio_Play(RedBookSong[Level], true);
            alreadydid = true;
        }

        engine.getzsofslope(sp.getSectnum(),
 sp.getX(),
 sp.getY(),
 fz, cz);
        u.loz = fz.get();
        u.hiz = cz.get();

        u.lo_sectp = sp.getSectnum();
        u.hi_sectp = sp.getSectnum();
        u.lo_sp = -1;
        u.hi_sp = -1;
        sp.setZ(u.loz);

        BossSpriteNum[2] = -2;
    }

    public static void ZillaSaveable() {
        SaveData(InitZillaRail);
        SaveData(InitZillaRocket);

        SaveData(NullZilla);
        SaveData(DoZillaMove);
        SaveData(DoZillaStomp);
        SaveData(DoZillaDeathMelt);

        SaveData(ZillaPersonality);

        SaveData(ZillaAttrib);

        SaveData(s_ZillaRun);
        SaveGroup(sg_ZillaRun);
        SaveData(s_ZillaStand);
        SaveGroup(sg_ZillaStand);
        SaveData(s_ZillaPain);
        SaveGroup(sg_ZillaPain);
        SaveData(s_ZillaRail);
        SaveGroup(sg_ZillaRail);
        SaveData(s_ZillaRocket);
        SaveGroup(sg_ZillaRocket);
        SaveData(s_ZillaUzi);
        SaveGroup(sg_ZillaUzi);
        SaveData(s_ZillaDie);
        SaveGroup(sg_ZillaDie);
        SaveData(s_ZillaDead);
        SaveGroup(sg_ZillaDead);

        SaveData(ZillaActionSet);
    }
}
