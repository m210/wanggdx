package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Factory.WangNetwork;
import ru.m210projects.Wang.Type.*;
import ru.m210projects.Wang.Weapon;

import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Game.Distance;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JWeapon.InitBloodSpray;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Player.PLAYER_NINJA_XREPEAT;
import static ru.m210projects.Wang.Player.PLAYER_NINJA_YREPEAT;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Zombie {

    public static final Animator InitEnemyRail = new Animator(Zombie::InitEnemyRail);
    public static final Animator DoZombieMove = new Animator((Animator.Runnable) Zombie::DoZombieMove);
    public static final Animator NullZombie = new Animator((Animator.Runnable) Zombie::NullZombie);
    public static final Animator DoZombiePain = new Animator((Animator.Runnable) Zombie::DoZombiePain);


    private static final int ZOMBIE_RATE = 32;
    private static final int ZOMBIE_TIME_LIMIT = ((120 * 20) / ACTORMOVETICS);

    private static final State[][] s_ZombieRun = {
            {new State(PLAYER_NINJA_RUN_R0, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
                    // &s_ZombieRun[0][1]},
                    new State(PLAYER_NINJA_RUN_R0 + 1, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
                    // &s_ZombieRun[0][2]},
                    new State(PLAYER_NINJA_RUN_R0 + 2, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
                    // &s_ZombieRun[0][3]},
                    new State(PLAYER_NINJA_RUN_R0 + 3, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
                    // &s_ZombieRun[0][0]},
            }, {new State(PLAYER_NINJA_RUN_R1, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[1][1]},
            new State(PLAYER_NINJA_RUN_R1 + 1, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[1][2]},
            new State(PLAYER_NINJA_RUN_R1 + 2, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[1][3]},
            new State(PLAYER_NINJA_RUN_R1 + 3, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[1][0]},
    }, {new State(PLAYER_NINJA_RUN_R2, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[2][1]},
            new State(PLAYER_NINJA_RUN_R2 + 1, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[2][2]},
            new State(PLAYER_NINJA_RUN_R2 + 2, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[2][3]},
            new State(PLAYER_NINJA_RUN_R2 + 3, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[2][0]},
    }, {new State(PLAYER_NINJA_RUN_R3, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[3][1]},
            new State(PLAYER_NINJA_RUN_R3 + 1, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[3][2]},
            new State(PLAYER_NINJA_RUN_R3 + 2, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[3][3]},
            new State(PLAYER_NINJA_RUN_R3 + 3, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[3][0]},
    }, {new State(PLAYER_NINJA_RUN_R4, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[4][1]},
            new State(PLAYER_NINJA_RUN_R4 + 1, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[4][2]},
            new State(PLAYER_NINJA_RUN_R4 + 2, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[4][3]},
            new State(PLAYER_NINJA_RUN_R4 + 3, ZOMBIE_RATE | SF_TIC_ADJUST, DoZombieMove),
            // &s_ZombieRun[4][0]},
    }};


    private static final int ZOMBIE_PAIN_RATE = 15;
    private static final State[][] s_ZombiePain = {
            {new State(PLAYER_NINJA_STAND_R0, ZOMBIE_PAIN_RATE, DoZombiePain),
                    // &s_ZombiePain[0][1]},
                    new State(PLAYER_NINJA_STAND_R0 + 1, ZOMBIE_PAIN_RATE, DoZombiePain),
                    // &s_ZombiePain[0][1]},
            }, {new State(PLAYER_NINJA_STAND_R1, ZOMBIE_PAIN_RATE, DoZombiePain),
            // &s_ZombiePain[1][1]},
            new State(PLAYER_NINJA_STAND_R1, ZOMBIE_PAIN_RATE, DoZombiePain),
            // &s_ZombiePain[1][1]},
    }, {new State(PLAYER_NINJA_STAND_R2, ZOMBIE_PAIN_RATE, DoZombiePain),
            // &s_ZombiePain[2][1]},
            new State(PLAYER_NINJA_STAND_R2, ZOMBIE_PAIN_RATE, DoZombiePain),
            // &s_ZombiePain[2][1]},
    }, {new State(PLAYER_NINJA_STAND_R3, ZOMBIE_PAIN_RATE, DoZombiePain),
            // &s_ZombiePain[3][1]},
            new State(PLAYER_NINJA_STAND_R3, ZOMBIE_PAIN_RATE, DoZombiePain),
            // &s_ZombiePain[3][1]},
    }, {new State(PLAYER_NINJA_STAND_R4, ZOMBIE_PAIN_RATE, DoZombiePain),
            // &s_ZombiePain[4][1]},
            new State(PLAYER_NINJA_STAND_R4, ZOMBIE_PAIN_RATE, DoZombiePain),
            // &s_ZombiePain[4][1]},
    }};
    private static final ATTRIBUTE ZombieAttrib = new ATTRIBUTE(new short[]{120, 140, 170, 200}, // Speeds
            new short[]{4, 0, 0, -2}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_NINJAAMBIENT, DIGI_NINJAALERT, DIGI_STAR, DIGI_NINJAPAIN, DIGI_NINJASCREAM, 0, 0, 0, 0,
                    0});
    private static final Decision[] ZombieBattle = {new Decision(399, InitActorMoveCloser),

            new Decision(1024, InitActorAttack)};
    private static final Decision[] ZombieOffense = {new Decision(399, InitActorMoveCloser),

            new Decision(1024, InitActorAttack)};
    private static final Decision[] ZombieBroadcast = {new Decision(6, InitActorAmbientNoise),

            new Decision(1024, InitActorDecide)};
    private static final Decision[] ZombieSurprised = {new Decision(701, InitActorMoveCloser),

            new Decision(1024, InitActorDecide)};
    private static final Decision[] ZombieEvasive = {new Decision(400, InitActorDuck),
            new Decision(1024, null)};
    private static final Decision[] ZombieLostTarget = {new Decision(900, InitActorFindPlayer),

            new Decision(1024, InitActorWanderAround)};
    private static final Decision[] ZombieCloseRange = {new Decision(800, InitActorAttack),

            new Decision(1024, InitActorReposition)};
    private static final Personality ZombiePersonality = new Personality(ZombieBattle, ZombieOffense, ZombieBroadcast,
            ZombieSurprised, ZombieEvasive, ZombieLostTarget, ZombieCloseRange, ZombieCloseRange);
    private static final int ZOMBIE_STAND_RATE = 10;
    private static final State[][] s_ZombieStand = {
            {new State(PLAYER_NINJA_STAND_R0, ZOMBIE_STAND_RATE, NullZombie).setNext()},
            {new State(PLAYER_NINJA_STAND_R1, ZOMBIE_STAND_RATE, NullZombie).setNext(),
            },
            {new State(PLAYER_NINJA_STAND_R2, ZOMBIE_STAND_RATE, NullZombie).setNext(),
            },
            {new State(PLAYER_NINJA_STAND_R3, ZOMBIE_STAND_RATE, NullZombie).setNext(),
            },
            {new State(PLAYER_NINJA_STAND_R4, ZOMBIE_STAND_RATE, NullZombie).setNext(),
            }};
    private static final int ZOMBIE_FALL_RATE = 25;
    private static final State[][] s_ZombieFall = {
            {new State(PLAYER_NINJA_JUMP_R0 + 3, ZOMBIE_FALL_RATE, DoZombieMove).setNext(),
            },
            {new State(PLAYER_NINJA_JUMP_R1 + 3, ZOMBIE_FALL_RATE, DoZombieMove).setNext(),
            },
            {new State(PLAYER_NINJA_JUMP_R2 + 3, ZOMBIE_FALL_RATE, DoZombieMove).setNext(),
            },
            {new State(PLAYER_NINJA_JUMP_R3 + 3, ZOMBIE_FALL_RATE, DoZombieMove).setNext(),
            },
            {new State(PLAYER_NINJA_JUMP_R4 + 3, ZOMBIE_FALL_RATE, DoZombieMove).setNext(),
            }};
    private static final int ZOMBIE_RAIL_RATE = 14;
    private static final State[][] s_ZombieRail = {
            {new State(PLAYER_NINJA_STAND_R0, ZOMBIE_RAIL_RATE * 2, NullZombie),
                    // &s_ZombieRail[0][1]},
                    new State(PLAYER_NINJA_STAND_R0, SF_QUICK_CALL, InitEnemyRail),
                    // &s_ZombieRail[0][2]},
                    new State(PLAYER_NINJA_STAND_R0, ZOMBIE_RAIL_RATE, NullZombie),
                    // &s_ZombieRail[0][3]},
                    new State(PLAYER_NINJA_STAND_R0, SF_QUICK_CALL, InitActorDecide),
                    // &s_ZombieRail[0][4]},
                    new State(PLAYER_NINJA_STAND_R0, ZOMBIE_RAIL_RATE, DoZombieMove).setNext(),
                    // &s_ZombieRail[0][4]},
            }, {new State(PLAYER_NINJA_STAND_R1, ZOMBIE_RAIL_RATE * 2, NullZombie),
            // &s_ZombieRail[1][1]},
            new State(PLAYER_NINJA_STAND_R1, SF_QUICK_CALL, InitEnemyRail),
            // &s_ZombieRail[1][2]},
            new State(PLAYER_NINJA_STAND_R1, ZOMBIE_RAIL_RATE, NullZombie),
            // &s_ZombieRail[1][3]},
            new State(PLAYER_NINJA_STAND_R1, SF_QUICK_CALL, InitActorDecide),
            // &s_ZombieRail[1][4]},
            new State(PLAYER_NINJA_STAND_R1, ZOMBIE_RAIL_RATE, DoZombieMove).setNext(),
            // &s_ZombieRail[1][4]},
    }, {new State(PLAYER_NINJA_STAND_R2, ZOMBIE_RAIL_RATE * 2, NullZombie),
            // &s_ZombieRail[2][1]},
            new State(PLAYER_NINJA_STAND_R2, SF_QUICK_CALL, InitEnemyRail),
            // &s_ZombieRail[2][2]},
            new State(PLAYER_NINJA_STAND_R2, ZOMBIE_RAIL_RATE, NullZombie),
            // &s_ZombieRail[2][3]},
            new State(PLAYER_NINJA_STAND_R2, SF_QUICK_CALL, InitActorDecide),
            // &s_ZombieRail[2][4]},
            new State(PLAYER_NINJA_STAND_R2, ZOMBIE_RAIL_RATE, DoZombieMove).setNext(),
            // &s_ZombieRail[2][4]},
    }, {new State(PLAYER_NINJA_STAND_R3, ZOMBIE_RAIL_RATE * 2, NullZombie),
            // &s_ZombieRail[3][1]},
            new State(PLAYER_NINJA_STAND_R3, SF_QUICK_CALL, InitEnemyRail),
            // &s_ZombieRail[3][2]},
            new State(PLAYER_NINJA_STAND_R3, ZOMBIE_RAIL_RATE, NullZombie),
            // &s_ZombieRail[3][3]},
            new State(PLAYER_NINJA_STAND_R3, SF_QUICK_CALL, InitActorDecide),
            // &s_ZombieRail[3][4]},
            new State(PLAYER_NINJA_STAND_R3, ZOMBIE_RAIL_RATE, DoZombieMove).setNext(),
            // &s_ZombieRail[3][4]},
    }, {new State(PLAYER_NINJA_STAND_R4, ZOMBIE_RAIL_RATE * 2, NullZombie),
            // &s_ZombieRail[4][1]},
            new State(PLAYER_NINJA_STAND_R4, SF_QUICK_CALL, InitEnemyRail),
            // &s_ZombieRail[4][2]},
            new State(PLAYER_NINJA_STAND_R4, ZOMBIE_RAIL_RATE, NullZombie),
            // &s_ZombieRail[4][3]},
            new State(PLAYER_NINJA_STAND_R4, SF_QUICK_CALL, InitActorDecide),
            // &s_ZombieRail[4][4]},
            new State(PLAYER_NINJA_STAND_R4, ZOMBIE_RAIL_RATE, DoZombieMove).setNext(),
            // &s_ZombieRail[4][4]},
    }};

    private static final EnemyStateGroup sg_ZombieStand = new EnemyStateGroup(s_ZombieStand[0], s_ZombieStand[1], s_ZombieStand[2], s_ZombieStand[3], s_ZombieStand[4]);
    private static final EnemyStateGroup sg_ZombieRun = new EnemyStateGroup(s_ZombieRun[0], s_ZombieRun[1], s_ZombieRun[2], s_ZombieRun[3], s_ZombieRun[4]);
    private static final EnemyStateGroup sg_ZombieFall = new EnemyStateGroup(s_ZombieFall[0], s_ZombieFall[1], s_ZombieFall[2], s_ZombieFall[3], s_ZombieFall[4]);
    private static final EnemyStateGroup sg_ZombiePain = new EnemyStateGroup(s_ZombiePain[0], s_ZombiePain[1], s_ZombiePain[2], s_ZombiePain[3], s_ZombiePain[4]);
    private static final EnemyStateGroup sg_ZombieRail = new EnemyStateGroup(s_ZombieRail[0], s_ZombieRail[1], s_ZombieRail[2], s_ZombieRail[3], s_ZombieRail[4]);


    private static final Actor_Action_Set ZombieActionSet = new Actor_Action_Set(sg_ZombieStand,
            sg_ZombieRun, null, sg_ZombieFall, null, null, null,
            sg_ZombieRun, sg_ZombieRun, null, null, sg_ZombiePain,
            sg_ZombieRun, null, null, null, null, new StateGroup[]{sg_ZombieRail},
            new short[]{1024}, new StateGroup[]{sg_ZombieRail}, new short[]{1024}, null, null,
            null);

    public static void SpawnZombie2(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        if (sp == null) {
            return;
        }

        Sector sectp = boardService.getSector(sp.getSectnum());
        int owner = sp.getOwner();
        if (owner < 0) {
            return;
        }

        if (sectp != null && (DTEST(sectp.getExtra(),
                SECTFX_LIQUID_MASK) != SECTFX_LIQUID_NONE)) {
            return;
        }

        if (SectorIsUnderwaterArea(sp.getSectnum())) {
            return;
        }

        if (FAF_ConnectArea(sp.getSectnum())) {
            int sectnum = engine.updatesectorz(sp.getX(),
                    sp.getY(),
                    sp.getZ() + Z(10),
                    sp.getSectnum());
            if (sectnum >= 0 && SectorIsUnderwaterArea(sectnum)) {
                return;
            }
        }

        // Zombies++;
        int newsp = SpawnSprite(STAT_ENEMY, ZOMBIE_RUN_R0, s_ZombieRun[0][0], sp.getSectnum(),
                sp.getX(),
                sp.getY(),
                sp.getZ(),
                sp.getAng(),
                0);
        Sprite np = boardService.getSprite(newsp);
        USER nu = getUser(newsp);
        USER ou = getUser(owner);
        if (ou  == null || np == null || nu == null) {
            return;
        }

        nu.Counter3 = 0;
        np.setOwner(owner);
        np.setPal(nu.spal = ou.spal);
        np.setAng( RANDOM_P2(2048));
        SetupZombie(newsp);
        np.setShade(-10);
        nu.Flags2 |= (SPR2_DONT_TARGET_OWNER);
        np.setCstat(np.getCstat() | (CSTAT_SPRITE_TRANSLUCENT));

        DoActorPickClosePlayer(newsp);

        // make immediately active
        nu.Flags |= (SPR_ACTIVE);

        nu.Flags &= ~(SPR_JUMPING);
        nu.Flags &= ~(SPR_FALLING);

        // if I didn't do this here they get stuck in the air sometimes
        DoActorZrange(newsp);

    }

    public static void NullZombie(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (u.Counter3++ >= ZOMBIE_TIME_LIMIT) {
            InitBloodSpray(SpriteNum, true, 105);
            InitBloodSpray(SpriteNum, true, 105);
            InitBloodSpray(SpriteNum, true, 105);
            SetSuicide(SpriteNum);
            return;
        }

        USER tu = getUser(u.tgt_sp);
        if (u.tgt_sp != -1 && tu != null && TEST(tu.Flags, PF_DEAD)) {
            DoActorPickClosePlayer(SpriteNum);
        }

        if (u.WaitTics > 0) {
            u.WaitTics -= ACTORMOVETICS;
        }

        if (TEST(u.Flags, SPR_SLIDING) && !TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            DoActorSlide(SpriteNum);
        }

        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            KeepActorOnFloor(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
    }

    public static void DoZombieMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (u.Counter3++ >= ZOMBIE_TIME_LIMIT) {
            InitBloodSpray(SpriteNum, true, 105);
            InitBloodSpray(SpriteNum, true, 105);
            InitBloodSpray(SpriteNum, true, 105);
            SetSuicide(SpriteNum);
            return;
        }

        USER tu = getUser(u.tgt_sp);
        if (u.tgt_sp != -1 && tu != null && TEST(tu.Flags, PF_DEAD)) {
            DoActorPickClosePlayer(SpriteNum);
        }

        // jumping and falling
        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoActorJump(SpriteNum);
            } else if (TEST(u.Flags, SPR_FALLING)) {
                DoActorFall(SpriteNum);
            }
        }

        // sliding
        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        // Do track or call current action function - such as DoActorMoveCloser()
        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        // stay on floor unless doing certain things
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            KeepActorOnFloor(SpriteNum);
        }

        // take damage from environment
        DoActorSectorDamage(SpriteNum);
    }

    public static void InitZombieStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[] {
                sg_ZombieStand,
                sg_ZombieRun,
                sg_ZombieFall,
                sg_ZombiePain,
                sg_ZombieRail
        }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }

    public static void DoZombiePain(int SpriteNum) {
        USER u = getUser(SpriteNum);

        NullZombie.animatorInvoke(SpriteNum);
        if (u != null && (u.WaitTics -= ACTORMOVETICS) <= 0) {
            InitActorDecide(SpriteNum);
        }
    }

    private static void SetupZombie(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.Health = 100;
        u.StateEnd = s_ZombiePain[0][0];
        u.setRot(sg_ZombieRun);
        sp.setXrepeat(PLAYER_NINJA_XREPEAT);
        sp.setYrepeat(PLAYER_NINJA_YREPEAT);

        u.Attrib = ZombieAttrib;
        EnemyDefaults(SpriteNum, ZombieActionSet, ZombiePersonality);

        ChangeState(SpriteNum, s_ZombieRun[0][0]);
        DoActorSetSpeed(SpriteNum, NORM_SPEED);

        u.Radius = 280;
        u.Flags |= (SPR_XFLIP_TOGGLE);
    }

    public static boolean InitEnemyRail(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz, dist;
        int w;
        int pnum;

        // if co-op don't hurt teammate
        if (gNet.MultiGameType == WangNetwork.MultiGameTypes.MULTI_GAME_COOPERATIVE && u.ID == ZOMBIE_RUN_R0) {
            PlayerStr pp;

            // Check all players
            for (pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
                pp = Player[pnum];

                if (u.tgt_sp == pp.PlayerSprite) {
                    return false;
                }
            }
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return false;
        }

        PlaySound(DIGI_RAILFIRE, sp, v3df_dontpan | v3df_doppler);

        // get angle to player and also face player when attacking
        sp.setAng((EngineUtils.getAngle(tsp.getX() - sp.getX(),
                tsp.getY() - sp.getY())));

        // add a bit of randomness
        if (RANDOM_P2(1024) < 512) {
            sp.setAng(NORM_ANGLE(sp.getAng() + RANDOM_P2(128) - 64));
        }

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ() - DIV2(SPRITEp_SIZE_Z(sp)) - Z(8);

        // Spawn a shot
        // Inserting and setting up variables

        w =  SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R1, s_Rail[0][0], sp.getSectnum(), nx, ny, nz, sp.getAng(), 1200);

        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return false;
        }

        if (u.ID == ZOMBIE_RUN_R0) {
            SetOwner(sp.getOwner(), w);
        } else {
            SetOwner(SpriteNum, w);
        }

        wp.setYrepeat(52);
        wp.setXrepeat(52);
        wp.setShade(-15);
        wp.setZvel(0);

        wu.RotNum = 5;
        NewStateGroup(w, Weapon.WeaponStateGroup.sg_Rail);

        wu.Radius = 200;
        wu.ceiling_dist =  Z(1);
        wu.floor_dist =  Z(1);
        wu.Flags2 |= (SPR2_SO_MISSILE);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER | CSTAT_SPRITE_INVISIBLE));
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        wp.setClipdist(64 >> 2);

        wu.xchange = MOVEx(wp.getXvel(),
                wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(),
                wp.getAng());
        wu.zchange = wp.getZvel();

        if (TestMissileSetPos(w, DoRailStart, 600, wp.getZvel())) {
            // sprite.clipdist = oclipdist;
            KillSprite(w);
            return false;
        }

        // find the distance to the target (player)
        dist = Distance(wp.getX(),
                wp.getY(),
                tsp.getX(),
                tsp.getY());

        if (dist != 0) {
            wu.zchange = (short) ((wp.getXvel() * (SPRITEp_UPPER(tsp) - wp.getZ())) / dist);
            wp.setZvel(wu.zchange);
        }

        return (w) != 0;
    }

    public static void ZombieSaveable() {
        SaveData(InitEnemyRail);

        SaveData(DoZombieMove);
        SaveData(NullZombie);
        SaveData(DoZombiePain);

        SaveData(ZombiePersonality);
        SaveData(ZombieAttrib);

        SaveData(s_ZombieRun);
        SaveGroup(sg_ZombieRun);
        SaveData(s_ZombieStand);
        SaveGroup(sg_ZombieStand);
        SaveData(s_ZombiePain);
        SaveGroup(sg_ZombiePain);
        SaveData(s_ZombieRail);
        SaveGroup(sg_ZombieRail);
        SaveData(s_ZombieFall);
        SaveGroup(sg_ZombieFall);

        SaveData(ZombieActionSet);
    }


}
