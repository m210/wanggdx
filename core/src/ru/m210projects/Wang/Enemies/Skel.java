package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Game.Distance;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.DIV2;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Skel {

    public static final int SKEL_RUN_RATE = 12;
    public static final int SKEL_SLASH_RATE = 20;
    public static final int SKEL_SPELL_RATE = 20;
    public static final int SKEL_PAIN_RATE = 38;
    public static final int SKEL_TELEPORT_RATE = 20;
    public static final int SKEL_STAND_RATE = 12;
    public static final int SKEL_DIE_RATE = 25;
    private static final Animator NullSkel = new Animator((Animator.Runnable) Skel::NullSkel);
    private static final Animator InitSkelSlash = new Animator((Animator.Runnable) Skel::InitSkelSlash);
    private static final Animator InitSkelSpell = new Animator(Skel::InitSkelSpell);
    private static final Animator DoSkelMove = new Animator((Animator.Runnable) Skel::DoSkelMove);
    private static final Animator DoSkelPain = new Animator((Animator.Runnable) Skel::DoSkelPain);
    private static final Animator DoSkelInitTeleport = new Animator((Animator.Runnable)Skel::DoSkelInitTeleport);
    private static final Animator DoSkelTeleport = new Animator((Animator.Runnable) Skel::DoSkelTeleport);
    private static final Animator DoSkelTermTeleport = new Animator((Animator.Runnable) Skel::DoSkelTermTeleport);
    private static final Decision[] SkelBattle = {new Decision(600, InitActorMoveCloser),
            new Decision(602, InitActorAlertNoise),
            new Decision(700, InitActorRunAway),
            new Decision(1024, InitActorAttack)};
    private static final Decision[] SkelOffense = {new Decision(700, InitActorMoveCloser),
            new Decision(702, InitActorAlertNoise),
            new Decision(1024, InitActorAttack)};
    private static final Decision[] SkelBroadcast = {new Decision(3, InitActorAlertNoise),
            new Decision(6, InitActorAmbientNoise),
            new Decision(1024, InitActorDecide)};
    private static final Decision[] SkelSurprised = {new Decision(701, InitActorMoveCloser),
            new Decision(1024, InitActorDecide)};

    //////////////////////
    //
    // SKEL RUN
    //
    //////////////////////
    private static final Decision[] SkelEvasive = {new Decision(22, InitActorDuck),
            new Decision(30, InitActorEvade),
            new Decision(1024, null)};

    // +4 on frame #3 to add character
    private static final Decision[] SkelLostTarget = {new Decision(900, InitActorFindPlayer),
            new Decision(1024, InitActorWanderAround)};
    private static final Decision[] SkelCloseRange = {new Decision(800, InitActorAttack),
            new Decision(1024, InitActorReposition)};

    //////////////////////
    //
    // SKEL SLASH
    //
    //////////////////////
    private static final Personality SkelPersonality = new Personality(SkelBattle, SkelOffense, SkelBroadcast, SkelSurprised, SkelEvasive, SkelLostTarget, SkelCloseRange, SkelCloseRange);
    private static final ATTRIBUTE SkelAttrib = new ATTRIBUTE(new short[]{60, 80, 100, 130}, // Speeds
            new short[]{3, 0, -2, -3}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_SPAMBIENT, DIGI_SPALERT, 0, DIGI_SPPAIN, DIGI_SPSCREAM, DIGI_SPBLADE, DIGI_SPELEC, DIGI_SPTELEPORT, 0, 0});

    private static final State[][] s_SkelRun = {{new State(SKEL_RUN_R0, SKEL_RUN_RATE + 4, DoSkelMove),
            // s_SkelRun[0][1]},
            new State(SKEL_RUN_R0 + 1, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[0][2]},
            new State(SKEL_RUN_R0 + 2, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[0][3]},
            new State(SKEL_RUN_R0 + 3, SKEL_RUN_RATE + 4, DoSkelMove),
            // s_SkelRun[0][4]},
            new State(SKEL_RUN_R0 + 4, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[0][5]},
            new State(SKEL_RUN_R0 + 5, SKEL_RUN_RATE, DoSkelMove),
// s_SkelRun[0][0]},
    }, {new State(SKEL_RUN_R1, SKEL_RUN_RATE + 4, DoSkelMove),
            // s_SkelRun[1][1]},
            new State(SKEL_RUN_R1 + 1, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[1][2]},
            new State(SKEL_RUN_R1 + 2, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[1][3]},
            new State(SKEL_RUN_R1 + 3, SKEL_RUN_RATE + 4, DoSkelMove),
            // s_SkelRun[1][4]},
            new State(SKEL_RUN_R1 + 4, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[1][5]},
            new State(SKEL_RUN_R1 + 5, SKEL_RUN_RATE, DoSkelMove),
// s_SkelRun[1][0]},
    }, {new State(SKEL_RUN_R2, SKEL_RUN_RATE + 4, DoSkelMove),
            // s_SkelRun[2][1]},
            new State(SKEL_RUN_R2 + 1, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[2][2]},
            new State(SKEL_RUN_R2 + 2, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[2][3]},
            new State(SKEL_RUN_R2 + 3, SKEL_RUN_RATE + 4, DoSkelMove),
            // s_SkelRun[2][4]},
            new State(SKEL_RUN_R2 + 4, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[2][5]},
            new State(SKEL_RUN_R2 + 5, SKEL_RUN_RATE, DoSkelMove),
// s_SkelRun[2][0]},
    }, {new State(SKEL_RUN_R3, SKEL_RUN_RATE + 4, DoSkelMove),
            // s_SkelRun[3][1]},
            new State(SKEL_RUN_R3 + 1, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[3][2]},
            new State(SKEL_RUN_R3 + 2, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[3][3]},
            new State(SKEL_RUN_R3 + 3, SKEL_RUN_RATE + 4, DoSkelMove),
            // s_SkelRun[3][4]},
            new State(SKEL_RUN_R3 + 4, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[3][5]},
            new State(SKEL_RUN_R3 + 5, SKEL_RUN_RATE, DoSkelMove),
// s_SkelRun[3][0]},
    }, {new State(SKEL_RUN_R4, SKEL_RUN_RATE + 4, DoSkelMove),
            // s_SkelRun[4][1]},
            new State(SKEL_RUN_R4 + 1, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[4][2]},
            new State(SKEL_RUN_R4 + 2, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[4][3]},
            new State(SKEL_RUN_R4 + 3, SKEL_RUN_RATE + 4, DoSkelMove),
            // s_SkelRun[4][4]},
            new State(SKEL_RUN_R4 + 4, SKEL_RUN_RATE, DoSkelMove),
            // s_SkelRun[4][5]},
            new State(SKEL_RUN_R4 + 5, SKEL_RUN_RATE, DoSkelMove),
// s_SkelRun[4][0]},
    }};

    //////////////////////
    //
    // SKEL SPELL
    //
    //////////////////////
    private static final EnemyStateGroup sg_SkelRun = new EnemyStateGroup(s_SkelRun[0], s_SkelRun[1], s_SkelRun[2], s_SkelRun[3], s_SkelRun[4]);

    //////////////////////
    //
    // SKEL PAIN
    //
    //////////////////////
    private static final State[][] s_SkelSlash = {{new State(SKEL_SLASH_R0, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[0][1]},
            new State(SKEL_SLASH_R0 + 1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[0][2]},
            new State(SKEL_SLASH_R0 + 2, SF_QUICK_CALL, InitSkelSlash),
            // s_SkelSlash[0][3]},
            new State(SKEL_SLASH_R0 + 2, SKEL_SLASH_RATE * 2, NullSkel),
            // s_SkelSlash[0][4]},
            new State(SKEL_SLASH_R0 + 1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[0][5]},
            new State(SKEL_SLASH_R0 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SkelSlash[0][6]},
            new State(SKEL_SLASH_R0 + 1, SKEL_SLASH_RATE, DoSkelMove).setNext(),
// s_SkelSlash[0][6]},
    }, {new State(SKEL_SLASH_R1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[1][1]},
            new State(SKEL_SLASH_R1 + 1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[1][2]},
            new State(SKEL_SLASH_R1 + 2, SF_QUICK_CALL, InitSkelSlash),
            // s_SkelSlash[1][3]},
            new State(SKEL_SLASH_R1 + 2, SKEL_SLASH_RATE * 2, NullSkel),
            // s_SkelSlash[1][4]},
            new State(SKEL_SLASH_R1 + 1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[1][5]},
            new State(SKEL_SLASH_R1 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SkelSlash[1][6]},
            new State(SKEL_SLASH_R1 + 1, SKEL_SLASH_RATE, DoSkelMove).setNext(),
// s_SkelSlash[1][6]},
    }, {new State(SKEL_SLASH_R2, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[2][1]},
            new State(SKEL_SLASH_R2 + 1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[2][2]},
            new State(SKEL_SLASH_R2 + 2, SF_QUICK_CALL, InitSkelSlash),
            // s_SkelSlash[2][3]},
            new State(SKEL_SLASH_R2 + 2, SKEL_SLASH_RATE * 2, NullSkel),
            // s_SkelSlash[2][4]},
            new State(SKEL_SLASH_R2 + 1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[2][5]},
            new State(SKEL_SLASH_R2 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SkelSlash[2][6]},
            new State(SKEL_SLASH_R2 + 1, SKEL_SLASH_RATE, DoSkelMove).setNext(),
// s_SkelSlash[2][6]},
    }, {new State(SKEL_SLASH_R3, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[3][1]},
            new State(SKEL_SLASH_R3 + 1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[3][2]},
            new State(SKEL_SLASH_R3 + 2, SF_QUICK_CALL, InitSkelSlash),
            // s_SkelSlash[3][3]},
            new State(SKEL_SLASH_R3 + 2, SKEL_SLASH_RATE * 2, NullSkel),
            // s_SkelSlash[3][4]},
            new State(SKEL_SLASH_R3 + 1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[3][5]},
            new State(SKEL_SLASH_R3 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SkelSlash[3][6]},
            new State(SKEL_SLASH_R3 + 1, SKEL_SLASH_RATE, DoSkelMove).setNext(),
// s_SkelSlash[3][6]},
    }, {new State(SKEL_SLASH_R4, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[4][1]},
            new State(SKEL_SLASH_R4 + 1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[4][2]},
            new State(SKEL_SLASH_R4 + 2, SF_QUICK_CALL, InitSkelSlash),
            // s_SkelSlash[4][3]},
            new State(SKEL_SLASH_R4 + 2, SKEL_SLASH_RATE * 2, NullSkel),
            // s_SkelSlash[4][4]},
            new State(SKEL_SLASH_R4 + 1, SKEL_SLASH_RATE, NullSkel),
            // s_SkelSlash[4][5]},
            new State(SKEL_SLASH_R4 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SkelSlash[4][6]},
            new State(SKEL_SLASH_R4 + 1, SKEL_SLASH_RATE, DoSkelMove).setNext(),
// s_SkelSlash[4][6]},
    }};
    private static final EnemyStateGroup sg_SkelSlash = new EnemyStateGroup(s_SkelSlash[0], s_SkelSlash[1], s_SkelSlash[2], s_SkelSlash[3], s_SkelSlash[4]);
    private static final State[][] s_SkelSpell = {{new State(SKEL_SPELL_R0, SKEL_SPELL_RATE + 9, NullSkel),
            // s_SkelSpell[0][1]},
            new State(SKEL_SPELL_R0 + 1, SKEL_SPELL_RATE, NullSkel),
            // s_SkelSpell[0][2]},
            new State(SKEL_SPELL_R0 + 2, SKEL_SPELL_RATE, NullSkel),
            // s_SkelSpell[0][3]},
            new State(SKEL_SPELL_R0 + 3, SKEL_SPELL_RATE * 2, NullSkel),
            // s_SkelSpell[0][4]},
            new State(SKEL_SPELL_R0 + 3, SF_QUICK_CALL, InitSkelSpell),
            // s_SkelSpell[0][5]},
            new State(SKEL_SPELL_R0 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_SkelSpell[0][6]},
            new State(SKEL_SPELL_R0 + 3, SKEL_SPELL_RATE, DoSkelMove).setNext(),
// s_SkelSpell[0][6]},
    }, {new State(SKEL_SPELL_R1, SKEL_SPELL_RATE + 9, NullSkel),
            // s_SkelSpell[1][1]},
            new State(SKEL_SPELL_R1 + 1, SKEL_SPELL_RATE, NullSkel),
            // s_SkelSpell[1][2]},
            new State(SKEL_SPELL_R1 + 2, SKEL_SPELL_RATE, NullSkel),
            // s_SkelSpell[1][3]},
            new State(SKEL_SPELL_R1 + 3, SKEL_SPELL_RATE * 2, NullSkel),
            // s_SkelSpell[1][4]},
            new State(SKEL_SPELL_R1 + 3, SF_QUICK_CALL, InitSkelSpell),
            // s_SkelSpell[1][5]},
            new State(SKEL_SPELL_R1 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_SkelSpell[1][6]},
            new State(SKEL_SPELL_R1 + 3, SKEL_SPELL_RATE, DoSkelMove).setNext(),
// s_SkelSpell[1][6]},
    }, {new State(SKEL_SPELL_R2, SKEL_SPELL_RATE + 9, NullSkel),
            // s_SkelSpell[2][1]},
            new State(SKEL_SPELL_R2 + 1, SKEL_SPELL_RATE, NullSkel),
            // s_SkelSpell[2][2]},
            new State(SKEL_SPELL_R2 + 2, SKEL_SPELL_RATE, NullSkel),
            // s_SkelSpell[2][3]},
            new State(SKEL_SPELL_R2 + 3, SKEL_SPELL_RATE * 2, NullSkel),
            // s_SkelSpell[2][4]},
            new State(SKEL_SPELL_R2 + 3, SF_QUICK_CALL, InitSkelSpell),
            // s_SkelSpell[2][5]},
            new State(SKEL_SPELL_R2 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_SkelSpell[2][6]},
            new State(SKEL_SPELL_R2 + 3, SKEL_SPELL_RATE, DoSkelMove).setNext(),
// s_SkelSpell[2][6]},
    }, {new State(SKEL_SPELL_R3, SKEL_SPELL_RATE + 9, NullSkel),
            // s_SkelSpell[3][1]},
            new State(SKEL_SPELL_R3 + 1, SKEL_SPELL_RATE, NullSkel),
            // s_SkelSpell[3][2]},
            new State(SKEL_SPELL_R3 + 2, SKEL_SPELL_RATE, NullSkel),
            // s_SkelSpell[3][3]},
            new State(SKEL_SPELL_R3 + 3, SKEL_SPELL_RATE * 2, NullSkel),
            // s_SkelSpell[3][4]},
            new State(SKEL_SPELL_R3 + 3, SF_QUICK_CALL, InitSkelSpell),
            // s_SkelSpell[3][5]},
            new State(SKEL_SPELL_R3 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_SkelSpell[3][6]},
            new State(SKEL_SPELL_R3 + 3, SKEL_SPELL_RATE, DoSkelMove).setNext(),
// s_SkelSpell[3][6]},
    }, {new State(SKEL_SPELL_R4, SKEL_SPELL_RATE + 9, NullSkel),
            // s_SkelSpell[4][1]},
            new State(SKEL_SPELL_R4 + 1, SKEL_SPELL_RATE, NullSkel),
            // s_SkelSpell[4][2]},
            new State(SKEL_SPELL_R4 + 2, SKEL_SPELL_RATE, NullSkel),
            // s_SkelSpell[4][3]},
            new State(SKEL_SPELL_R4 + 3, SKEL_SPELL_RATE * 2, NullSkel),
            // s_SkelSpell[4][4]},
            new State(SKEL_SPELL_R4 + 3, SF_QUICK_CALL, InitSkelSpell),
            // s_SkelSpell[4][5]},
            new State(SKEL_SPELL_R4 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_SkelSpell[4][6]},
            new State(SKEL_SPELL_R4 + 3, SKEL_SPELL_RATE, DoSkelMove).setNext(),
// s_SkelSpell[4][6]},
    }};
    private static final EnemyStateGroup sg_SkelSpell = new EnemyStateGroup(s_SkelSpell[0], s_SkelSpell[1], s_SkelSpell[2], s_SkelSpell[3], s_SkelSpell[4]);
    //////////////////////
    //
    // SKEL TELEPORT
    //
    //////////////////////
    private static final State[][] s_SkelPain = {{new State(SKEL_PAIN_R0, SKEL_PAIN_RATE, DoSkelPain).setNext(),
// s_SkelPain[0][0]},
    }, {new State(SKEL_PAIN_R1, SKEL_PAIN_RATE, DoSkelPain).setNext(),
// s_SkelPain[1][0]},
    }, {new State(SKEL_PAIN_R2, SKEL_PAIN_RATE, DoSkelPain).setNext(),
// s_SkelPain[2][0]},
    }, {new State(SKEL_PAIN_R3, SKEL_PAIN_RATE, DoSkelPain).setNext(),
// s_SkelPain[3][0]},
    }, {new State(SKEL_PAIN_R4, SKEL_PAIN_RATE, DoSkelPain).setNext(),
// s_SkelPain[4][0]},
    }};
    private static final EnemyStateGroup sg_SkelPain = new EnemyStateGroup(s_SkelPain[0], s_SkelPain[1], s_SkelPain[2], s_SkelPain[3], s_SkelPain[4]);
    private static final State[] s_SkelTeleport = {new State(SKEL_TELEPORT, 1, null),
            // s_SkelTeleport[1]},
            new State(SKEL_TELEPORT, SF_QUICK_CALL, DoSkelInitTeleport),
            // s_SkelTeleport[2]},
            new State(SKEL_TELEPORT, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[3]},
            new State(SKEL_TELEPORT + 1, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[4]},
            new State(SKEL_TELEPORT + 2, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[5]},
            new State(SKEL_TELEPORT + 3, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[6]},
            new State(SKEL_TELEPORT + 4, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[7]},
            new State(SKEL_TELEPORT + 5, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[8]},

            new State(SKEL_TELEPORT + 5, SF_QUICK_CALL, DoSkelTeleport),
            // s_SkelTeleport[9]},

            new State(SKEL_TELEPORT + 5, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[10]},
            new State(SKEL_TELEPORT + 4, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[11]},
            new State(SKEL_TELEPORT + 3, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[12]},
            new State(SKEL_TELEPORT + 2, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[13]},
            new State(SKEL_TELEPORT + 1, SKEL_TELEPORT_RATE, null),
            // s_SkelTeleport[14]},
            new State(SKEL_TELEPORT, SKEL_TELEPORT_RATE, DoSkelTermTeleport),
            // s_SkelTeleport[15]},
            new State(SKEL_TELEPORT, SF_QUICK_CALL, InitActorDecide),
            // s_SkelTeleport[16]},
            new State(SKEL_TELEPORT, SKEL_TELEPORT_RATE, DoSkelMove).setNext(),
// s_SkelTeleport[16]},
    };
    private static final EnemyStateGroup sg_SkelTeleport = new EnemyStateGroup(s_SkelTeleport, s_SkelTeleport, s_SkelTeleport, s_SkelTeleport, s_SkelTeleport);
    //////////////////////
    //
    // SKEL STAND
    //
    //////////////////////
    private static final State[][] s_SkelStand = {{new State(SKEL_RUN_R0, SKEL_STAND_RATE, DoSkelMove).setNext(),
// s_SkelStand[0][0]},
    }, {new State(SKEL_RUN_R1, SKEL_STAND_RATE, DoSkelMove).setNext(),
// s_SkelStand[1][0]},
    }, {new State(SKEL_RUN_R2, SKEL_STAND_RATE, DoSkelMove).setNext(),
// s_SkelStand[2][0]},
    }, {new State(SKEL_RUN_R3, SKEL_STAND_RATE, DoSkelMove).setNext(),
// s_SkelStand[3][0]},
    }, {new State(SKEL_RUN_R4, SKEL_STAND_RATE, DoSkelMove).setNext(),
// s_SkelStand[4][0]},
    }};
    private static final EnemyStateGroup sg_SkelStand = new EnemyStateGroup(s_SkelStand[0], s_SkelStand[1], s_SkelStand[2], s_SkelStand[3], s_SkelStand[4]);
    private static final State[] s_SkelDie = {new State(SKEL_DIE, SKEL_DIE_RATE, null),
            // s_SkelDie[1]},
            new State(SKEL_DIE + 1, SKEL_DIE_RATE, null),
            // s_SkelDie[2]},
            new State(SKEL_DIE + 2, SKEL_DIE_RATE, null),
            // s_SkelDie[3]},
            new State(SKEL_DIE + 3, SKEL_DIE_RATE, null),
            // s_SkelDie[4]},
            new State(SKEL_DIE + 4, SKEL_DIE_RATE, null),
            // s_SkelDie[5]},
            new State(SKEL_DIE + 5, SKEL_DIE_RATE, DoSuicide).setNext(),
// s_SkelDie[5]},
    };
    private static final EnemyStateGroup sg_SkelDie = new EnemyStateGroup(s_SkelDie);
    //////////////////////
    //
    // SKEL DIE
    //
    //////////////////////
    private static final Actor_Action_Set SkelActionSet = new Actor_Action_Set(sg_SkelStand, sg_SkelRun, null, // sg_SkelJump,
            null, // sg_SkelFall,
            null, // sg_SkelCrawl,
            null, // sg_SkelSwim,
            null, // sg_SkelFly,
            null, // sg_SkelRise,
            null, // sg_SkelSit,
            null, // sg_SkelLook,
            null, // climb
            sg_SkelPain, // pain
            sg_SkelDie, null, // sg_SkelHariKari,
            null, // sg_SkelDead,
            null, // sg_SkelDeathJump,
            null, // sg_SkelDeathFall,
            new StateGroup[]{sg_SkelSlash}, new short[]{1024}, new StateGroup[]{sg_SkelSpell}, new short[]{1024}, null, sg_SkelTeleport, null);

    public static void InitSkelStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{
                sg_SkelRun,
                sg_SkelTeleport,
                sg_SkelSpell,
                sg_SkelDie,
                sg_SkelSlash,
                sg_SkelPain,
                sg_SkelStand
        }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }

    public static void SetupSkel(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u;

        if (TEST(sp.getCstat(),
                CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, SKEL_RUN_R0, s_SkelRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_SKEL_PRIEST;
        }

        ChangeState(SpriteNum, s_SkelRun[0][0]);
        u.Attrib = SkelAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_SkelDie[0];
        u.setRot(sg_SkelRun);

        EnemyDefaults(SpriteNum, SkelActionSet, SkelPersonality);
        u.Flags |= (SPR_XFLIP_TOGGLE);
    }

    private static void DoSkelInitTeleport(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        PlaySpriteSound(SpriteNum, Attrib_Snds.attr_extra3, v3df_follow);
    }

    private static void DoSkelTeleport(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        int x = sp.getX();
        int y = sp.getY();

        do {
            sp.setX(x);
            sp.setY(y);

            if (RANDOM_P2(1024) < 512) {
                sp.setX(sp.getX() + 512 + RANDOM_P2(1024));
            } else {
                sp.setX(sp.getX() - (512 + RANDOM_P2(1024)));
            }

            if (RANDOM_P2(1024) < 512) {
                sp.setY(sp.getY() + 512 + RANDOM_P2(1024));
            } else {
                sp.setY(sp.getY() - (512 + RANDOM_P2(1024)));
            }

            engine.setspritez(SpriteNum, sp.getX(),
                    sp.getY(),
                    sp.getZ());
        } while (sp.getSectnum() == -1);
    }

    private static void DoSkelTermTeleport(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp != null) {
            sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        }
    }

    private static void NullSkel(int SpriteNum) {
        USER u = getUser(SpriteNum);

        if (u != null && TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        KeepActorOnFloor(SpriteNum);
        DoActorSectorDamage(SpriteNum);
    }

    private static void DoSkelPain(int SpriteNum) {
        USER u = getUser(SpriteNum);

        NullSkel(SpriteNum);

        if (u != null && (u.WaitTics -= ACTORMOVETICS) <= 0) {
            InitActorDecide(SpriteNum);
        }
    }

    private static void DoSkelMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        KeepActorOnFloor(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    public static void InitSkelSlash(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        PlaySound(DIGI_SPBLADE, sp, v3df_none);

        ListNode<Sprite> nexti;
        for (int j : StatDamageList) {
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();

                Sprite hp = node.get();

                if (i == SpriteNum) {
                    break;
                }

                int dist = DISTANCE(hp.getX(),
                        hp.getY(),
                        sp.getX(),
                        sp.getY());

                if (dist < CLOSE_RANGE_DIST_FUDGE(sp, hp, 600) && FACING_RANGE(hp, sp, 150)) {
//	                PlaySound(PlayerPainVocs[RANDOM_RANGE(MAX_PAIN)], sp.x, sp.y, sp.z, v3df_none);
                    DoDamage(i, SpriteNum);
                }
            }
        }
    }

    public static boolean InitSkelSpell(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum),
                wp;

        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return false;
        }

        int nx, ny, nz, dist, nang;
        int w;

        PlaySound(DIGI_SPELEC, sp, v3df_none);

        // get angle to player and also face player when attacking
        sp.setAng((nang = NORM_ANGLE(EngineUtils.getAngle(tsp.getX() - sp.getX(),
                tsp.getY() - sp.getY()))));

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ() - DIV2(SPRITEp_SIZE_Z(sp));

        // Spawn a shot
        w = SpawnSprite(STAT_MISSILE, ELECTRO_ENEMY, s_Electro[0], sp.getSectnum(),
                nx, ny, nz, tsp.getAng(),
                SKEL_ELECTRO_VELOCITY);

        wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return false;
        }

        // wp.owner = SpriteNum;
        SetOwner(SpriteNum, w);
        wp.setXrepeat(wp.getXrepeat() - 20);
        wp.setYrepeat(wp.getYrepeat() - 20);
        wp.setShade(-40);
        wp.setZvel(0);
        wp.setAng(nang);
        wp.setClipdist(64 >> 2);
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        // find the distance to the target (player)
        dist = Distance(nx, ny, tsp.getX(),
                tsp.getY());

        if (dist != 0) {
            wp.setZvel(((wp.getXvel() * (SPRITEp_UPPER(tsp) - nz)) / dist));
        }

        wu.xchange = MOVEx(wp.getXvel(),
                wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(),
                wp.getAng());
        wu.zchange = wp.getZvel();

        MissileSetPos(w, DoElectro, 400);

        return (w) != 0;
    }

    public static void SkelSaveable() {
        SaveData(InitSkelSlash);
        SaveData(InitSkelSpell);
        SaveData(DoSkelInitTeleport);
        SaveData(DoSkelTeleport);
        SaveData(DoSkelTermTeleport);
        SaveData(NullSkel);
        SaveData(DoSkelPain);
        SaveData(DoSkelMove);

        SaveData(SkelPersonality);

        SaveData(SkelAttrib);

        SaveData(s_SkelRun);
        SaveGroup(sg_SkelRun);
        SaveData(s_SkelSlash);
        SaveGroup(sg_SkelSlash);
        SaveData(s_SkelSpell);
        SaveGroup(sg_SkelSpell);
        SaveData(s_SkelPain);
        SaveGroup(sg_SkelPain);
        SaveData(s_SkelTeleport);
        SaveGroup(sg_SkelTeleport);
        SaveData(s_SkelStand);
        SaveGroup(sg_SkelStand);
        SaveData(s_SkelDie);
        SaveGroup(sg_SkelDie);

        SaveData(SkelActionSet);
    }
}
