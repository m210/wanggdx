package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Enemies.Skull.s_SkullExplode;
import static ru.m210projects.Wang.Enemies.Skull.s_SkullRing;
import static ru.m210projects.Wang.Enemies.Sumo.BossSpriteNum;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.cfg;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Sector.DoMatchEverything;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Serp {

    public static final short[] delta_ang = {-10, 10};
    private static final Animator DoSerpMove = new Animator((Animator.Runnable) Serp::DoSerpMove);
    private static final Animator NullSerp = new Animator((Animator.Runnable) Serp::NullSerp);
    private static final Animator InitSerpSlash = new Animator((Animator.Runnable) Serp::InitSerpSlash);
    private static final Animator InitSerpRing = new Animator((Animator.Runnable) Serp::InitSerpRing);
    private static final Animator InitSerpSpell = new Animator((Animator.Runnable) Serp::InitSerpSpell);
    private static final Decision[] SerpBattle = {new Decision(670, InitActorMoveCloser),
            new Decision(700, InitActorAmbientNoise),
            new Decision(710, InitActorRunAway),
            new Decision(1024, InitActorAttack),
    };
    private static final Decision[] SerpOffense = {new Decision(775, InitActorMoveCloser),
            new Decision(800, InitActorAmbientNoise),
            new Decision(1024, InitActorAttack),
    };
    private static final Decision[] SerpBroadcast = {new Decision(10, InitActorAmbientNoise),
            new Decision(1024, InitActorDecide),
    };
    private static final Decision[] SerpSurprised = {new Decision(701, InitActorMoveCloser),
            new Decision(1024, InitActorDecide),
    };
    private static final Decision[] SerpEvasive = {new Decision(10, InitActorEvade),
            new Decision(1024, null),
    };
    private static final Decision[] SerpLostTarget = {new Decision(900, InitActorFindPlayer),
            new Decision(921, InitActorAmbientNoise),
            new Decision(1024, InitActorWanderAround),
    };
    private static final Decision[] SerpCloseRange = {new Decision(700, InitActorAttack),
            new Decision(1024, InitActorReposition),
    };
    private static final Personality SerpPersonality = new Personality(SerpBattle, SerpOffense, SerpBroadcast, SerpSurprised, SerpEvasive, SerpLostTarget, SerpCloseRange, SerpCloseRange);
    private static final ATTRIBUTE SerpAttrib = new ATTRIBUTE(new short[]{200, 220, 240, 270}, // Speeds
            new short[]{3, 0, -2, -3}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_SERPAMBIENT, DIGI_SERPALERT, DIGI_SERPSWORDATTACK, DIGI_SERPPAIN, DIGI_SERPSCREAM, DIGI_SERPMAGICLAUNCH, DIGI_SERPSUMMONHEADS, DIGI_SERPTAUNTYOU, DIGI_SERPDEATHEXPLODE, 0});
    private static final int SERP_RUN_RATE = 24;
    private static final int SERP_SLASH_RATE = 9;
    private static final int SERP_SKULL_SPELL_RATE = 18;

    //////////////////////
    //
    // SERP RUN
    //
    //////////////////////
    private static final int SERP_SPELL_RATE = 18;
    private static final int SERP_STAND_RATE = 12;    private static final State[][] s_SerpRun = {{new State(SERP_RUN_R0, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[0][1]},
            new State(SERP_RUN_R0 + 1, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[0][2]},
            new State(SERP_RUN_R0 + 2, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[0][3]},
            new State(SERP_RUN_R0 + 1, SERP_RUN_RATE, DoSerpMove),
// s_SerpRun[0][0]},
    }, {new State(SERP_RUN_R1, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[1][1]},
            new State(SERP_RUN_R1 + 1, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[1][2]},
            new State(SERP_RUN_R1 + 2, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[1][3]},
            new State(SERP_RUN_R1 + 1, SERP_RUN_RATE, DoSerpMove),
// s_SerpRun[1][0]},
    }, {new State(SERP_RUN_R2, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[2][1]},
            new State(SERP_RUN_R2 + 1, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[2][2]},
            new State(SERP_RUN_R2 + 2, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[2][3]},
            new State(SERP_RUN_R2 + 1, SERP_RUN_RATE, DoSerpMove),
// s_SerpRun[2][0]},
    }, {new State(SERP_RUN_R3, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[3][1]},
            new State(SERP_RUN_R3 + 1, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[3][2]},
            new State(SERP_RUN_R3 + 2, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[3][3]},
            new State(SERP_RUN_R3 + 1, SERP_RUN_RATE, DoSerpMove),
// s_SerpRun[3][0]},
    }, {new State(SERP_RUN_R4, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[4][1]},
            new State(SERP_RUN_R4 + 1, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[4][2]},
            new State(SERP_RUN_R4 + 2, SERP_RUN_RATE, DoSerpMove),
            // s_SerpRun[4][3]},
            new State(SERP_RUN_R4 + 1, SERP_RUN_RATE, DoSerpMove),
// s_SerpRun[4][0]},
    }};

    //////////////////////
    //
    // SERP SLASH
    //
    //////////////////////
    private static final int SERP_DIE_RATE = 20;
    private static final State[] s_SerpDead = {new State(SERP_DEAD, SERP_DIE_RATE, DoActorDebris),
// s_SerpDead[0]},
    };    private static final State[][] s_SerpSlash = {{new State(SERP_SLASH_R0 + 2, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[0][1]},
            new State(SERP_SLASH_R0 + 1, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[0][2]},
            new State(SERP_SLASH_R0, SERP_SLASH_RATE * 2, NullSerp),
            // s_SerpSlash[0][3]},
            new State(SERP_SLASH_R0 + 1, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[0][4]},
            new State(SERP_SLASH_R0 + 2, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[0][5]},
            new State(SERP_SLASH_R0 + 3, SF_QUICK_CALL, InitSerpSlash),
            // s_SerpSlash[0][6]},
            new State(SERP_SLASH_R0 + 3, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[0][7]},
            new State(SERP_SLASH_R0 + 4, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[0][8]},
            new State(SERP_SLASH_R0 + 4, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSlash[0][9]},
            new State(SERP_SLASH_R0 + 4, SERP_SLASH_RATE, DoSerpMove).setNext(),
// s_SerpSlash[0][9]},
    }, {new State(SERP_SLASH_R1 + 2, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[1][1]},
            new State(SERP_SLASH_R1 + 1, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[1][2]},
            new State(SERP_SLASH_R1, SERP_SLASH_RATE * 2, NullSerp),
            // s_SerpSlash[1][3]},
            new State(SERP_SLASH_R1 + 1, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[1][4]},
            new State(SERP_SLASH_R1 + 2, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[1][5]},
            new State(SERP_SLASH_R1 + 3, SF_QUICK_CALL, InitSerpSlash),
            // s_SerpSlash[1][6]},
            new State(SERP_SLASH_R1 + 3, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[1][7]},
            new State(SERP_SLASH_R1 + 4, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[1][8]},
            new State(SERP_SLASH_R1 + 4, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSlash[1][9]},
            new State(SERP_SLASH_R1 + 4, SERP_SLASH_RATE, DoSerpMove).setNext(),
// s_SerpSlash[1][9]},
    }, {new State(SERP_SLASH_R2 + 2, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[2][1]},
            new State(SERP_SLASH_R2 + 1, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[2][2]},
            new State(SERP_SLASH_R2, SERP_SLASH_RATE * 2, NullSerp),
            // s_SerpSlash[2][3]},
            new State(SERP_SLASH_R2 + 1, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[2][4]},
            new State(SERP_SLASH_R2 + 2, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[2][5]},
            new State(SERP_SLASH_R2 + 3, SF_QUICK_CALL, InitSerpSlash),
            // s_SerpSlash[2][6]},
            new State(SERP_SLASH_R2 + 3, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[2][7]},
            new State(SERP_SLASH_R2 + 4, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[2][8]},
            new State(SERP_SLASH_R2 + 4, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSlash[2][9]},
            new State(SERP_SLASH_R2 + 4, SERP_SLASH_RATE, DoSerpMove).setNext(),
// s_SerpSlash[2][9]},
    }, {new State(SERP_SLASH_R3 + 2, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[3][1]},
            new State(SERP_SLASH_R3 + 1, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[3][2]},
            new State(SERP_SLASH_R3, SERP_SLASH_RATE * 2, NullSerp),
            // s_SerpSlash[3][3]},
            new State(SERP_SLASH_R3 + 1, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[3][4]},
            new State(SERP_SLASH_R3 + 2, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[3][5]},
            new State(SERP_SLASH_R3 + 3, SF_QUICK_CALL, InitSerpSlash),
            // s_SerpSlash[3][6]},
            new State(SERP_SLASH_R3 + 3, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[3][7]},
            new State(SERP_SLASH_R3 + 4, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[3][8]},
            new State(SERP_SLASH_R3 + 4, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSlash[3][9]},
            new State(SERP_SLASH_R3 + 4, SERP_SLASH_RATE, DoSerpMove).setNext(),
// s_SerpSlash[3][9]},
    }, {new State(SERP_SLASH_R4 + 2, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[4][1]},
            new State(SERP_SLASH_R4 + 1, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[4][2]},
            new State(SERP_SLASH_R4, SERP_SLASH_RATE * 2, NullSerp),
            // s_SerpSlash[4][3]},
            new State(SERP_SLASH_R4 + 1, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[4][4]},
            new State(SERP_SLASH_R4 + 2, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[4][5]},
            new State(SERP_SLASH_R4 + 3, SF_QUICK_CALL, InitSerpSlash),
            // s_SerpSlash[4][6]},
            new State(SERP_SLASH_R4 + 3, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[4][7]},
            new State(SERP_SLASH_R4 + 4, SERP_SLASH_RATE, NullSerp),
            // s_SerpSlash[4][8]},
            new State(SERP_SLASH_R4 + 4, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSlash[4][9]},
            new State(SERP_SLASH_R4 + 4, SERP_SLASH_RATE, DoSerpMove).setNext(),
// s_SerpSlash[4][9]},
    }};

    //////////////////////
    //
    // SERP SKULL SPELL
    //
    //////////////////////
    private static final EnemyStateGroup sg_SerpDead = new EnemyStateGroup(s_SerpDead);
    private static boolean alreadydid = false;
    private static final State[][] s_SerpSkullSpell = {{new State(SERP_SPELL_R0 + 2, SERP_SKULL_SPELL_RATE * 2, NullSerp),
            // s_SerpSkullSpell[0][1]},
            new State(SERP_SPELL_R0 + 1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[0][2]},
            new State(SERP_SPELL_R0, SERP_SKULL_SPELL_RATE * 2, NullSerp),
            // s_SerpSkullSpell[0][3]},
            new State(SERP_SPELL_R0, SF_QUICK_CALL, InitSerpRing),
            // s_SerpSkullSpell[0][4]},
            new State(SERP_SPELL_R0, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[0][5]},
            new State(SERP_SPELL_R0 + 1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[0][6]},
            new State(SERP_SPELL_R0 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSkullSpell[0][7]},
            new State(SERP_SPELL_R0 + 1, SERP_SKULL_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpSkullSpell[0][7]},
    }, {new State(SERP_SPELL_R1 + 2, SERP_SKULL_SPELL_RATE * 2, NullSerp),
            // s_SerpSkullSpell[1][1]},
            new State(SERP_SPELL_R1 + 1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[1][2]},
            new State(SERP_SPELL_R1, SERP_SKULL_SPELL_RATE * 2, NullSerp),
            // s_SerpSkullSpell[1][3]},
            new State(SERP_SPELL_R1, SF_QUICK_CALL, InitSerpRing),
            // s_SerpSkullSpell[1][4]},
            new State(SERP_SPELL_R1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[1][5]},
            new State(SERP_SPELL_R1 + 1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[1][6]},
            new State(SERP_SPELL_R1 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSkullSpell[1][7]},
            new State(SERP_SPELL_R1 + 1, SERP_SKULL_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpSkullSpell[1][7]},
    }, {new State(SERP_SPELL_R2 + 2, SERP_SKULL_SPELL_RATE * 2, NullSerp),
            // s_SerpSkullSpell[2][1]},
            new State(SERP_SPELL_R2 + 1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[2][2]},
            new State(SERP_SPELL_R2, SERP_SKULL_SPELL_RATE * 2, NullSerp),
            // s_SerpSkullSpell[2][3]},
            new State(SERP_SPELL_R2, SF_QUICK_CALL, InitSerpRing),
            // s_SerpSkullSpell[2][4]},
            new State(SERP_SPELL_R2, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[2][5]},
            new State(SERP_SPELL_R2 + 1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[2][6]},
            new State(SERP_SPELL_R2 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSkullSpell[2][7]},
            new State(SERP_SPELL_R2 + 1, SERP_SKULL_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpSkullSpell[2][7]},
    }, {new State(SERP_SPELL_R3 + 2, SERP_SKULL_SPELL_RATE * 2, NullSerp),
            // s_SerpSkullSpell[3][1]},
            new State(SERP_SPELL_R3 + 1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[3][2]},
            new State(SERP_SPELL_R3, SERP_SKULL_SPELL_RATE * 2, NullSerp),
            // s_SerpSkullSpell[3][3]},
            new State(SERP_SPELL_R3, SF_QUICK_CALL, InitSerpRing),
            // s_SerpSkullSpell[3][4]},
            new State(SERP_SPELL_R3, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[3][5]},
            new State(SERP_SPELL_R3 + 1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[3][6]},
            new State(SERP_SPELL_R3 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSkullSpell[3][7]},
            new State(SERP_SPELL_R3 + 1, SERP_SKULL_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpSkullSpell[3][7]},
    }, {new State(SERP_SPELL_R4 + 2, SERP_SKULL_SPELL_RATE * 2, NullSerp),
            // s_SerpSkullSpell[4][1]},
            new State(SERP_SPELL_R4 + 1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[4][2]},
            new State(SERP_SPELL_R4, SERP_SKULL_SPELL_RATE * 2, NullSerp),
            // s_SerpSkullSpell[4][3]},
            new State(SERP_SPELL_R4, SF_QUICK_CALL, InitSerpRing),
            // s_SerpSkullSpell[4][4]},
            new State(SERP_SPELL_R4, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[4][5]},
            new State(SERP_SPELL_R4 + 1, SERP_SKULL_SPELL_RATE, NullSerp),
            // s_SerpSkullSpell[4][6]},
            new State(SERP_SPELL_R4 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSkullSpell[4][7]},
            new State(SERP_SPELL_R4 + 1, SERP_SKULL_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpSkullSpell[4][7]},
    }};

    //////////////////////
    //
    // SERP SPELL
    //
    //////////////////////
    private static final Animator DoDeathSpecial = new Animator((Animator.Runnable) Serp::DoDeathSpecial);
    private static final State[] s_SerpDie = {new State(SERP_DIE, SERP_DIE_RATE, NullSerp),
            // s_SerpDie[1]},
            new State(SERP_DIE + 1, SERP_DIE_RATE, NullSerp),
            // s_SerpDie[2]},
            new State(SERP_DIE + 2, SERP_DIE_RATE, NullSerp),
            // s_SerpDie[3]},
            new State(SERP_DIE + 3, SERP_DIE_RATE, NullSerp),
            // s_SerpDie[4]},
            new State(SERP_DIE + 4, SERP_DIE_RATE, NullSerp),
            // s_SerpDie[5]},
            new State(SERP_DIE + 5, SERP_DIE_RATE, NullSerp),
            // s_SerpDie[6]},
            new State(SERP_DIE + 6, SERP_DIE_RATE, NullSerp),
            // s_SerpDie[7]},
            new State(SERP_DIE + 7, SERP_DIE_RATE, NullSerp),
            // s_SerpDie[8]},
            new State(SERP_DIE + 8, SERP_DIE_RATE, NullSerp),
            // s_SerpDie[9]},
            new State(SERP_DIE + 8, SF_QUICK_CALL, DoDeathSpecial),
            // s_SerpDie[10]},
            new State(SERP_DEAD, SERP_DIE_RATE, DoActorDebris).setNext(),
// s_SerpDie[10]}
    };    private static final State[][] s_SerpSpell = {{new State(SERP_SPELL_R0 + 2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpSpell[0][1]},
            new State(SERP_SPELL_R0 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[0][2]},
            new State(SERP_SPELL_R0, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpSpell[0][3]},
            new State(SERP_SPELL_R0, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpSpell[0][4]},
            new State(SERP_SPELL_R0, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[0][5]},
            new State(SERP_SPELL_R0 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[0][6]},
            new State(SERP_SPELL_R0 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSpell[0][7]},
            new State(SERP_SPELL_R0 + 1, SERP_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpSpell[0][7]},
    }, {new State(SERP_SPELL_R1 + 2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpSpell[1][1]},
            new State(SERP_SPELL_R1 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[1][2]},
            new State(SERP_SPELL_R1, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpSpell[1][3]},
            new State(SERP_SPELL_R1, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpSpell[1][4]},
            new State(SERP_SPELL_R1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[1][5]},
            new State(SERP_SPELL_R1 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[1][6]},
            new State(SERP_SPELL_R1 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSpell[1][7]},
            new State(SERP_SPELL_R1 + 1, SERP_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpSpell[1][7]},
    }, {new State(SERP_SPELL_R2 + 2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpSpell[2][1]},
            new State(SERP_SPELL_R2 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[2][2]},
            new State(SERP_SPELL_R2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpSpell[2][3]},
            new State(SERP_SPELL_R2, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpSpell[2][4]},
            new State(SERP_SPELL_R2, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[2][5]},
            new State(SERP_SPELL_R2 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[2][6]},
            new State(SERP_SPELL_R2 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSpell[2][7]},
            new State(SERP_SPELL_R2 + 1, SERP_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpSpell[2][7]},
    }, {new State(SERP_SPELL_R3 + 2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpSpell[3][1]},
            new State(SERP_SPELL_R3 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[3][2]},
            new State(SERP_SPELL_R3, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpSpell[3][3]},
            new State(SERP_SPELL_R3, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpSpell[3][4]},
            new State(SERP_SPELL_R3, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[3][5]},
            new State(SERP_SPELL_R3 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[3][6]},
            new State(SERP_SPELL_R3 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSpell[3][7]},
            new State(SERP_SPELL_R3 + 1, SERP_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpSpell[3][7]},
    }, {new State(SERP_SPELL_R4 + 2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpSpell[4][1]},
            new State(SERP_SPELL_R4 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[4][2]},
            new State(SERP_SPELL_R4, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpSpell[4][3]},
            new State(SERP_SPELL_R4, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpSpell[4][4]},
            new State(SERP_SPELL_R4, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[4][5]},
            new State(SERP_SPELL_R4 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpSpell[4][6]},
            new State(SERP_SPELL_R4 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpSpell[4][7]},
            new State(SERP_SPELL_R4 + 1, SERP_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpSpell[4][7]},
    }};

    //////////////////////
    //
    // SERP RAPID SPELL
    //
    //////////////////////
    private static final EnemyStateGroup sg_SerpDie = new EnemyStateGroup(s_SerpDie);    private static final State[][] s_SerpRapidSpell = {{new State(SERP_SPELL_R0 + 2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[0][1]},
            new State(SERP_SPELL_R0 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[0][2]},
            new State(SERP_SPELL_R0, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[0][3]},
            new State(SERP_SPELL_R0, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpRapidSpell[0][4]},
            new State(SERP_SPELL_R0, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[0][5]},
            new State(SERP_SPELL_R0, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpRapidSpell[0][6]},
            new State(SERP_SPELL_R0, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[0][7]},
            new State(SERP_SPELL_R0 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[0][8]},
            new State(SERP_SPELL_R0 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpRapidSpell[0][9]},
            new State(SERP_SPELL_R0 + 1, SERP_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpRapidSpell[0][9]},
    }, {new State(SERP_SPELL_R1 + 2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[1][1]},
            new State(SERP_SPELL_R1 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[1][2]},
            new State(SERP_SPELL_R1, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[1][3]},
            new State(SERP_SPELL_R1, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpRapidSpell[1][4]},
            new State(SERP_SPELL_R1, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[1][5]},
            new State(SERP_SPELL_R1, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpRapidSpell[1][6]},
            new State(SERP_SPELL_R1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[1][7]},
            new State(SERP_SPELL_R1 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[1][8]},
            new State(SERP_SPELL_R1 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpRapidSpell[1][9]},
            new State(SERP_SPELL_R1 + 1, SERP_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpRapidSpell[1][9]},
    }, {new State(SERP_SPELL_R2 + 2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[2][1]},
            new State(SERP_SPELL_R2 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[2][2]},
            new State(SERP_SPELL_R2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[2][3]},
            new State(SERP_SPELL_R2, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpRapidSpell[2][4]},
            new State(SERP_SPELL_R2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[2][5]},
            new State(SERP_SPELL_R2, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpRapidSpell[2][6]},
            new State(SERP_SPELL_R2, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[2][7]},
            new State(SERP_SPELL_R2 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[2][8]},
            new State(SERP_SPELL_R2 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpRapidSpell[2][9]},
            new State(SERP_SPELL_R2 + 1, SERP_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpRapidSpell[2][9]},
    }, {new State(SERP_SPELL_R3 + 2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[3][1]},
            new State(SERP_SPELL_R3 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[3][2]},
            new State(SERP_SPELL_R3, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[3][3]},
            new State(SERP_SPELL_R3, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpRapidSpell[3][4]},
            new State(SERP_SPELL_R3, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[3][5]},
            new State(SERP_SPELL_R3, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpRapidSpell[3][6]},
            new State(SERP_SPELL_R3, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[3][7]},
            new State(SERP_SPELL_R3 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[3][8]},
            new State(SERP_SPELL_R3 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpRapidSpell[3][9]},
            new State(SERP_SPELL_R3 + 1, SERP_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpRapidSpell[3][9]},
    }, {new State(SERP_SPELL_R4 + 2, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[4][1]},
            new State(SERP_SPELL_R4 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[4][2]},
            new State(SERP_SPELL_R4, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[4][3]},
            new State(SERP_SPELL_R4, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpRapidSpell[4][4]},
            new State(SERP_SPELL_R4, SERP_SPELL_RATE * 2, NullSerp),
            // s_SerpRapidSpell[4][5]},
            new State(SERP_SPELL_R4, SF_QUICK_CALL, InitSerpSpell),
            // s_SerpRapidSpell[4][6]},
            new State(SERP_SPELL_R4, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[4][7]},
            new State(SERP_SPELL_R4 + 1, SERP_SPELL_RATE, NullSerp),
            // s_SerpRapidSpell[4][8]},
            new State(SERP_SPELL_R4 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_SerpRapidSpell[4][9]},
            new State(SERP_SPELL_R4 + 1, SERP_SPELL_RATE, DoSerpMove).setNext(),
// s_SerpRapidSpell[4][9]},
    }};

    //////////////////////
    //
    // SERP STAND
    //
    //////////////////////

    public static void SetupSerp(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u;

        if (TEST(sp.getCstat(),
                CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, SERP_RUN_R0, s_SerpRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_SERP_GOD;
        }

        if (Skill == 0) {
            u.Health = 1100;
        }
        if (Skill == 1) {
            u.Health = 2200;
        }

        ChangeState(SpriteNum, s_SerpRun[0][0]);
        u.Attrib = SerpAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_SerpDie[0];
        u.setRot(sg_SerpRun);

        EnemyDefaults(SpriteNum, SerpActionSet, SerpPersonality);

        // Mini-Boss Serp
        if (sp.getPal() == 16) {
            u.Health = 1000;
            sp.setYrepeat(74);
            sp.setXrepeat(74);
        } else {
            sp.setYrepeat(100);
            sp.setXrepeat(128);
        }

        sp.setClipdist((512) >> 2);
        u.Flags |= (SPR_XFLIP_TOGGLE | SPR_ELECTRO_TOLERANT);

        u.loz = sp.getZ();

        // amount to move up for clipmove
        u.zclip = Z(80);
        // size of step can walk off of
        u.lo_step = Z(40);

        u.floor_dist = (u.zclip - u.lo_step);
        u.ceiling_dist = (SPRITEp_SIZE_Z(sp) - u.zclip);

        alreadydid = false;
    }

    public static void InitSerpSpell(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum),
                np;
        USER u = getUser(SpriteNum), nu;
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }

        int dist;
        int newsp, i;
        int oclipdist;

        for (i = 0; i < 2; i++) {
            sp.setAng(EngineUtils.getAngle(tsp.getX() - sp.getX(),
                    tsp.getY() - sp.getY()));

            newsp = SpawnSprite(STAT_MISSILE, SERP_METEOR, WeaponStateGroup.sg_SerpMeteor.getState(0),
                    sp.getSectnum(),
                    sp.getX(),
                    sp.getY(),
                    sp.getZ(),
                    sp.getAng(),
                    1500);

            np = boardService.getSprite(newsp);
            nu = getUser(newsp);
            if (np == null || nu == null) {
                return;
            }

            np.setZ(SPRITEp_TOS(sp));

            nu.RotNum = 5;
            NewStateGroup(newsp, WeaponStateGroup.sg_SerpMeteor);
            nu.StateEnd = s_MirvMeteorExp[0];

            // np.owner = SpriteNum;
            SetOwner(SpriteNum, newsp);
            np.setShade(-40);
            PlaySound(DIGI_SERPMAGICLAUNCH, sp, v3df_none);
            nu.spal = 27;
            np.setPal(27); // Bright Green
            np.setXrepeat(64);
            np.setYrepeat(64);
            np.setClipdist(32 >> 2);
            np.setZvel(0);
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));

            nu.ceiling_dist = Z(16);
            nu.floor_dist = Z(16);
            nu.Dist = 200;

            oclipdist = sp.getClipdist();
            sp.setClipdist(1);

            np.setAng(NORM_ANGLE(np.getAng() + lat_ang[i]));
            HelpMissileLateral(newsp, 4200);
            np.setAng(NORM_ANGLE(np.getAng() - lat_ang[i]));

            // find the distance to the target (player)
            dist = Distance(np.getX(),
                    np.getY(),
                    tsp.getX(),
                    tsp.getY());
            if (dist != 0) {
                np.setZvel(((np.getXvel() * (SPRITEp_UPPER(tsp) - np.getZ())) / dist));
            }

            np.setAng(NORM_ANGLE(np.getAng() + delta_ang[i]));

            nu.xchange = MOVEx(np.getXvel(),
                    np.getAng());
            nu.ychange = MOVEy(np.getXvel(),
                    np.getAng());
            nu.zchange = np.getZvel();

            MissileSetPos(newsp, DoMirvMissile, 400);
            sp.setClipdist(oclipdist);

            if (TEST(u.Flags, SPR_UNDERWATER)) {
                nu.Flags |= (SPR_UNDERWATER);
            }
        }

    }

    private static final State[][] s_SerpStand = {{new State(SERP_RUN_R0, SERP_STAND_RATE, DoSerpMove).setNext(),
// s_SerpStand[0][0]},
    }, {new State(SERP_RUN_R1, SERP_STAND_RATE, DoSerpMove).setNext(),
// s_SerpStand[1][0]},
    }, {new State(SERP_RUN_R2, SERP_STAND_RATE, DoSerpMove).setNext(),
// s_SerpStand[2][0]},
    }, {new State(SERP_RUN_R3, SERP_STAND_RATE, DoSerpMove).setNext(),
// s_SerpStand[3][0]},
    }, {new State(SERP_RUN_R4, SERP_STAND_RATE, DoSerpMove).setNext(),
// s_SerpStand[4][0]},
    },};

    public static void InitSerpSlash(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        Sprite hp;
        int stat;
        int dist;

        PlaySound(DIGI_SERPSWORDATTACK, sp, v3df_none);

        for (stat = 0; stat < StatDamageList.length; stat++) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(StatDamageList[stat]); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                hp = node.get();

                if (i == SpriteNum) {
                    break;
                }

                dist = DISTANCE(hp.getX(),
                        hp.getY(),
                        sp.getX(),
                        sp.getY());

                if (dist < CLOSE_RANGE_DIST_FUDGE(sp, hp, 800) && FACING_RANGE(hp, sp, 150)) {
//	                PlaySound(PlayerPainVocs[RANDOM_RANGE(MAX_PAIN)], sp.x, sp.y, sp.z, v3df_none);
                    DoDamage(i, SpriteNum);
                }
            }
        }
    }

    public static void InitSerpRing(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum),
                np;
        USER u = getUser(SpriteNum), nu;
        if (sp == null || u == null) {
            return;
        }

        int ang, ang_diff, ang_start, missiles, newsp;
        int max_missiles;

        max_missiles = 12;

        u.Counter = max_missiles;

        ang_diff = (2048 / max_missiles);

        ang_start = NORM_ANGLE(sp.getAng() - DIV2(2048));

        PlaySound(DIGI_SERPSUMMONHEADS, sp, v3df_none);

        for (missiles = 0, ang = ang_start; missiles < max_missiles; ang += ang_diff, missiles++) {
            newsp = SpawnSprite(STAT_SKIP4, SKULL_SERP, s_SkullRing[0][0], sp.getSectnum(),
                    sp.getX(),
                    sp.getY(),
                    sp.getZ(),
                    ang, 0);

            np = boardService.getSprite(newsp);
            nu = getUser(newsp);
            if (np == null || nu == null) {
                return;
            }

            np.setXvel(500);
            // np.owner = SpriteNum;
            SetOwner(SpriteNum, newsp);
            np.setShade(-20);
            np.setXrepeat(64);
            np.setYrepeat(64);
            np.setYvel(2 * RINGMOVETICS);
            np.setZvel(Z(3));
            np.setPal(0);

            np.setZ(SPRITEp_TOS(sp) - Z(20));
            nu.sz = Z(50);

            // ang around the serp is now slide_ang
            nu.slide_ang = np.getAng();
            // randomize the head turning angle
            np.setAng((RANDOM_P2(2048 << 5) >> 5));

            // control direction of spinning
            u.Flags ^= (SPR_BOUNCE);
            nu.Flags |= (DTEST(u.Flags, SPR_BOUNCE));

            nu.Dist = 600;
            nu.TargetDist = SERP_RING_DIST;
            nu.Counter2 = 0;

            nu.StateEnd = s_SkullExplode[0];
            nu.setRot(Skull.sg_SkullRing);

            // defaults do change the statnum
            EnemyDefaults(newsp, null, null);
            change_sprite_stat(newsp, STAT_SKIP4);
            np.setExtra(np.getExtra() & ~(SPRX_PLAYER_OR_ENEMY));

            np.setClipdist((128 + 64) >> 2);
            nu.Flags |= (SPR_XFLIP_TOGGLE);
            np.setCstat(np.getCstat() | (CSTAT_SPRITE_YCENTER));

            nu.Radius = 400;
        }
    }

    private static void NullSerp(int SpriteNum) {
        USER u = getUser(SpriteNum);

        if (u != null && TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        KeepActorOnFloor(SpriteNum);
    }

    private static final EnemyStateGroup sg_SerpStand = new EnemyStateGroup(s_SerpStand[0], s_SerpStand[1], s_SerpStand[2], s_SerpStand[3], s_SerpStand[4]);

    private static void DoSerpMove(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        // serp ring
        if (sp.getPal() != 16) {
            switch (u.Counter2) {
                case 0:
                    if (u.Health != u.MaxHealth) {
                        NewStateGroup(SpriteNum, sg_SerpSkullSpell);
                        u.Counter2++;
                    }
                    break;

                case 1: {
                    if (u.Counter <= 0) {
                        NewStateGroup(SpriteNum, sg_SerpSkullSpell);
                    }
                }
                break;
            }
        }

        KeepActorOnFloor(SpriteNum);
    }

    private static final EnemyStateGroup sg_SerpRun = new EnemyStateGroup(s_SerpRun[0], s_SerpRun[1], s_SerpRun[2], s_SerpRun[3], s_SerpRun[4]);

    private static void DoDeathSpecial(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        DoMatchEverything(null, sp.getLotag(), ON);

        if (!cfg.isMuteMusic() && !alreadydid) {
            CDAudio_Play(RedBookSong[Level], true);
            alreadydid = true;
        }

        BossSpriteNum[0] = -2;
    }

    public static void InitSerpStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{
                sg_SerpStand,
                sg_SerpRun,
                sg_SerpDie,
                sg_SerpDead,
                sg_SerpSlash,
                sg_SerpSpell,
                sg_SerpRapidSpell,
                sg_SerpSkullSpell
        }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }

    public static void SerpSaveable() {
        SaveData(InitSerpSlash);
        SaveData(InitSerpRing);
        SaveData(InitSerpSpell);

        SaveData(NullSerp);
        SaveData(DoSerpMove);
        SaveData(DoDeathSpecial);

        SaveData(SerpPersonality);

        SaveData(SerpAttrib);

        SaveData(s_SerpRun);
        SaveGroup(sg_SerpRun);
        SaveData(s_SerpSlash);
        SaveGroup(sg_SerpSlash);
        SaveData(s_SerpSkullSpell);
        SaveGroup(sg_SerpSkullSpell);
        SaveData(s_SerpSpell);
        SaveGroup(sg_SerpSpell);
        SaveData(s_SerpRapidSpell);
        SaveGroup(sg_SerpRapidSpell);
        SaveData(s_SerpStand);
        SaveGroup(sg_SerpStand);
        SaveData(s_SerpDie);
        SaveData(s_SerpDead);
        SaveGroup(sg_SerpDie);
        SaveGroup(sg_SerpDead);

        SaveData(SerpActionSet);
    }    private static final EnemyStateGroup sg_SerpSlash = new EnemyStateGroup(s_SerpSlash[0], s_SerpSlash[1], s_SerpSlash[2], s_SerpSlash[3], s_SerpSlash[4]);
    private static final EnemyStateGroup sg_SerpSpell = new EnemyStateGroup(s_SerpSpell[0], s_SerpSpell[1], s_SerpSpell[2], s_SerpSpell[3], s_SerpSpell[4]);
    private static final EnemyStateGroup sg_SerpRapidSpell = new EnemyStateGroup(s_SerpRapidSpell[0], s_SerpRapidSpell[1], s_SerpRapidSpell[2], s_SerpRapidSpell[3], s_SerpRapidSpell[4]);
    private static final EnemyStateGroup sg_SerpSkullSpell = new EnemyStateGroup(s_SerpSkullSpell[0], s_SerpSkullSpell[1], s_SerpSkullSpell[2], s_SerpSkullSpell[3], s_SerpSkullSpell[4]);


    private static final Actor_Action_Set SerpActionSet = new Actor_Action_Set(sg_SerpStand, sg_SerpRun, null, // sg_SerpJump,
            null, // sg_SerpFall,
            null, // sg_SerpCrawl,
            null, // sg_SerpSwim,
            null, // sg_SerpFly,
            null, // sg_SerpRise,
            null, // sg_SerpSit,
            null, // sg_SerpLook,
            null, // climb
            null, // pain
            sg_SerpDie, null, // sg_SerpHariKari,
            sg_SerpDead, null, // sg_SerpDeathJump,
            null, // sg_SerpDeathFall,
            new StateGroup[]{sg_SerpSlash}, new short[]{1024}, new StateGroup[]{sg_SerpSlash, sg_SerpSpell, sg_SerpRapidSpell, sg_SerpRapidSpell}, new short[]{256, 724, 900, 1024}, null, null, null);






















}
