package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.Pattern.ScreenAdapters.GameAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Factory.WangNetwork.CommPlayers;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JWeapon.InitChemBomb;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Player.QueueFloorBlood;
import static ru.m210projects.Wang.Quake.SetSumoFartQuake;
import static ru.m210projects.Wang.Quake.SetSumoQuake;
import static ru.m210projects.Wang.Sector.DoMatchEverything;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.ChangeState;
import static ru.m210projects.Wang.Sprites.SpawnUser;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.ON;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Sumo {

    private static final Animator DoSumoMove = new Animator((Animator.Runnable) Sumo::DoSumoMove);
    private static final Animator NullSumo = new Animator((Animator.Runnable) Sumo::NullSumo);
    private static final Animator InitSumoFart = new Animator((Animator.Runnable) Sumo::InitSumoFart);
    private static final Animator InitSumoClap = new Animator((Animator.Runnable) Sumo::InitSumoClap);
    private static final Animator InitSumoStomp = new Animator((Animator.Runnable) Sumo::InitSumoStomp);
    private static final Animator DoSumoDeathMelt = new Animator((Animator.Runnable) Sumo::DoSumoDeathMelt);


    private static boolean alreadydid = false;
    public static boolean serpwasseen = false;
    public static boolean sumowasseen = false;
    public static boolean zillawasseen = false;
    public static boolean triedplay = false;

    public static final int[] BossSpriteNum = { -1, -1, -1 };

    private static final Decision[] SumoBattle = { new Decision(690, InitActorMoveCloser),

            new Decision(692, InitActorAlertNoise),
 new Decision(1024, InitActorAttack) };

    private static final Decision[] SumoOffense = { new Decision(690, InitActorMoveCloser),

            new Decision(692, InitActorAlertNoise),
 new Decision(1024, InitActorAttack) };

    private static final Decision[] SumoBroadcast = { new Decision(2, InitActorAlertNoise),

            new Decision(4, InitActorAmbientNoise),
 new Decision(1024, InitActorDecide) };

    private static final Decision[] SumoSurprised = { new Decision(700, InitActorMoveCloser),

            new Decision(703, InitActorAlertNoise),
 new Decision(1024, InitActorDecide) };

    private static final Decision[] SumoEvasive = { new Decision(1024, InitActorAttack) };

    private static final Decision[] SumoLostTarget = { new Decision(900, InitActorFindPlayer),

            new Decision(1024, InitActorWanderAround) };

    private static final Decision[] SumoCloseRange = { new Decision(1024, InitActorAttack) };

    private static final Personality SumoPersonality = new Personality(SumoBattle, SumoOffense, SumoBroadcast,
            SumoSurprised, SumoEvasive, SumoLostTarget, SumoCloseRange, SumoCloseRange);

    private static final ATTRIBUTE SumoAttrib = new ATTRIBUTE(new short[] { 160, 180, 180, 180 }, // Speeds
            new short[] { 3, 0, 0, 0 }, // Tic Adjusts
            3, // MaxWeapons;
            new int[] { DIGI_SUMOAMBIENT, DIGI_SUMOALERT, DIGI_SUMOSCREAM, DIGI_SUMOPAIN, DIGI_SUMOSCREAM, 0, 0, 0, 0,
                    0 });

    //////////////////////
    //
    // SUMO RUN
    //
    //////////////////////

    public static final int SUMO_RATE = 24;



    private static final State[][] s_SumoRun = { { new State(SUMO_RUN_R0, SUMO_RATE, DoSumoMove),
 // s_SumoRun[0][1]},
            new State(SUMO_RUN_R0 + 1, SUMO_RATE, DoSumoMove),
 // s_SumoRun[0][2]},
            new State(SUMO_RUN_R0 + 2, SUMO_RATE, DoSumoMove),
 // s_SumoRun[0][3]},
            new State(SUMO_RUN_R0 + 3, SUMO_RATE, DoSumoMove),
// s_SumoRun[0][0]}
    }, { new State(SUMO_RUN_R1, SUMO_RATE, DoSumoMove),
 // s_SumoRun[1][1]},
            new State(SUMO_RUN_R1 + 1, SUMO_RATE, DoSumoMove),
 // s_SumoRun[1][2]},
            new State(SUMO_RUN_R1 + 2, SUMO_RATE, DoSumoMove),
 // s_SumoRun[1][3]},
            new State(SUMO_RUN_R1 + 3, SUMO_RATE, DoSumoMove),
// s_SumoRun[1][0]}
    }, { new State(SUMO_RUN_R2, SUMO_RATE, DoSumoMove),
 // s_SumoRun[2][1]},
            new State(SUMO_RUN_R2 + 1, SUMO_RATE, DoSumoMove),
 // s_SumoRun[2][2]},
            new State(SUMO_RUN_R2 + 2, SUMO_RATE, DoSumoMove),
 // s_SumoRun[2][3]},
            new State(SUMO_RUN_R2 + 3, SUMO_RATE, DoSumoMove),
// s_SumoRun[2][0]}
    }, { new State(SUMO_RUN_R3, SUMO_RATE, DoSumoMove),
 // s_SumoRun[3][1]},
            new State(SUMO_RUN_R3 + 1, SUMO_RATE, DoSumoMove),
 // s_SumoRun[3][2]},
            new State(SUMO_RUN_R3 + 2, SUMO_RATE, DoSumoMove),
 // s_SumoRun[3][3]},
            new State(SUMO_RUN_R3 + 3, SUMO_RATE, DoSumoMove),
// s_SumoRun[3][0]}
    }, { new State(SUMO_RUN_R4, SUMO_RATE, DoSumoMove),
 // s_SumoRun[4][1]},
            new State(SUMO_RUN_R4 + 1, SUMO_RATE, DoSumoMove),
 // s_SumoRun[4][2]},
            new State(SUMO_RUN_R4 + 2, SUMO_RATE, DoSumoMove),
 // s_SumoRun[4][3]},
            new State(SUMO_RUN_R4 + 3, SUMO_RATE, DoSumoMove),
// s_SumoRun[4][0]},
    } };

    //////////////////////
    //
    // SUMO STAND
    //
    //////////////////////

    private static final State[][] s_SumoStand = { { new State(SUMO_RUN_R0, SUMO_RATE, DoSumoMove).setNext(),
// s_SumoStand[0][0]}
    }, { new State(SUMO_RUN_R1, SUMO_RATE, DoSumoMove).setNext(),
// s_SumoStand[1][0]}
    }, { new State(SUMO_RUN_R2, SUMO_RATE, DoSumoMove).setNext(),
// s_SumoStand[2][0]}
    }, { new State(SUMO_RUN_R3, SUMO_RATE, DoSumoMove).setNext(),
// s_SumoStand[3][0]}
    }, { new State(SUMO_RUN_R4, SUMO_RATE, DoSumoMove).setNext(),
// s_SumoStand[4][0]}
    } };

    //////////////////////
    //
    // SUMO PAIN
    //
    //////////////////////

    public static final int SUMO_PAIN_RATE = 30;


    private static final State[][] s_SumoPain = { { new State(SUMO_PAIN_R0, SUMO_PAIN_RATE, NullSumo),
 // s_SumoPain[0][1]},
            new State(SUMO_PAIN_R0, SF_QUICK_CALL, InitActorDecide),
// s_SumoPain[0][0]}
    }, { new State(SUMO_PAIN_R1, SUMO_PAIN_RATE, NullSumo),
 // s_SumoPain[1][1]},
            new State(SUMO_PAIN_R1, SF_QUICK_CALL, InitActorDecide),
// s_SumoPain[1][0]}
    }, { new State(SUMO_PAIN_R2, SUMO_PAIN_RATE, NullSumo),
 // s_SumoPain[2][1]},
            new State(SUMO_PAIN_R2, SF_QUICK_CALL, InitActorDecide),
// s_SumoPain[2][0]}
    }, { new State(SUMO_PAIN_R3, SUMO_PAIN_RATE, NullSumo),
 // s_SumoPain[3][1]},
            new State(SUMO_PAIN_R3, SF_QUICK_CALL, InitActorDecide),
// s_SumoPain[3][0]}
    }, { new State(SUMO_PAIN_R4, SUMO_PAIN_RATE, NullSumo),
 // s_SumoPain[4][1]},
            new State(SUMO_PAIN_R4, SF_QUICK_CALL, InitActorDecide),
// s_SumoPain[4][0]}
    } };

    //////////////////////
    //
    // SUMO FART
    //
    //////////////////////

    public static final int SUMO_FART_RATE = 12;



    private static final State[][] s_SumoFart = { { new State(SUMO_FART_R0, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[0][1]},
            new State(SUMO_FART_R0, SF_QUICK_CALL, InitSumoFart),
 // s_SumoFart[0][2]},
            new State(SUMO_FART_R0 + 1, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[0][3]},
            new State(SUMO_FART_R0 + 2, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[0][4]},
            new State(SUMO_FART_R0 + 3, SUMO_FART_RATE * 10, NullSumo),
 // s_SumoFart[0][5]},
            new State(SUMO_FART_R0 + 3, SF_QUICK_CALL, InitActorDecide),
// s_SumoFart[0][0]}
    }, { new State(SUMO_FART_R1, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[1][1]},
            new State(SUMO_FART_R1, SF_QUICK_CALL, InitSumoFart),
 // s_SumoFart[1][2]},
            new State(SUMO_FART_R1 + 1, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[1][3]},
            new State(SUMO_FART_R1 + 2, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[1][4]},
            new State(SUMO_FART_R1 + 3, SUMO_FART_RATE * 10, NullSumo),
 // s_SumoFart[1][5]},
            new State(SUMO_FART_R1, SF_QUICK_CALL, InitActorDecide),
// s_SumoFart[1][0]}
    }, { new State(SUMO_FART_R2, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[2][1]},
            new State(SUMO_FART_R2, SF_QUICK_CALL, InitSumoFart),
 // s_SumoFart[2][2]},
            new State(SUMO_FART_R2 + 1, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[2][3]},
            new State(SUMO_FART_R2 + 2, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[2][4]},
            new State(SUMO_FART_R2 + 3, SUMO_FART_RATE * 10, NullSumo),
 // s_SumoFart[2][5]},
            new State(SUMO_FART_R2, SF_QUICK_CALL, InitActorDecide),
// s_SumoFart[2][0]}
    }, { new State(SUMO_FART_R3, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[3][1]},
            new State(SUMO_FART_R3, SF_QUICK_CALL, InitSumoFart),
 // s_SumoFart[3][2]},
            new State(SUMO_FART_R3 + 1, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[3][3]},
            new State(SUMO_FART_R3 + 2, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[3][4]},
            new State(SUMO_FART_R3 + 3, SUMO_FART_RATE * 10, NullSumo),
 // s_SumoFart[3][5]},
            new State(SUMO_FART_R3, SF_QUICK_CALL, InitActorDecide),
// s_SumoFart[3][0]}
    }, { new State(SUMO_FART_R4, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[4][1]},
            new State(SUMO_FART_R4, SF_QUICK_CALL, InitSumoFart),
 // s_SumoFart[4][2]},
            new State(SUMO_FART_R4 + 1, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[4][3]},
            new State(SUMO_FART_R4 + 2, SUMO_FART_RATE, NullSumo),
 // s_SumoFart[4][4]},
            new State(SUMO_FART_R4 + 3, SUMO_FART_RATE * 10, NullSumo),
 // s_SumoFart[4][5]},
            new State(SUMO_FART_R4, SF_QUICK_CALL, InitActorDecide),
// s_SumoFart[4][0]}
    } };

    //////////////////////
    //
    // SUMO CLAP
    //
    //////////////////////

    public static final int SUMO_CLAP_RATE = 12;


    private static final State[][] s_SumoClap = { { new State(SUMO_CLAP_R0, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[0][1]},
            new State(SUMO_CLAP_R0 + 1, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[0][2]},
            new State(SUMO_CLAP_R0 + 2, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[0][3]},
            new State(SUMO_CLAP_R0 + 2, SF_QUICK_CALL, InitSumoClap),
 // s_SumoClap[0][4]},
            new State(SUMO_CLAP_R0 + 3, SUMO_CLAP_RATE * 10, NullSumo),
 // s_SumoClap[0][5]},
            new State(SUMO_CLAP_R0 + 3, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_SumoClap[0][5]}
    }, { new State(SUMO_CLAP_R1, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[1][1]},
            new State(SUMO_CLAP_R1 + 1, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[1][2]},
            new State(SUMO_CLAP_R1 + 2, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[1][3]},
            new State(SUMO_CLAP_R1 + 2, SF_QUICK_CALL, InitSumoClap),
 // s_SumoClap[1][4]},
            new State(SUMO_CLAP_R1 + 3, SUMO_CLAP_RATE * 10, NullSumo),
 // s_SumoClap[1][5]},
            new State(SUMO_CLAP_R1 + 3, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_SumoClap[1][5]}
    }, { new State(SUMO_CLAP_R2, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[2][1]},
            new State(SUMO_CLAP_R2 + 1, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[2][2]},
            new State(SUMO_CLAP_R2 + 2, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[2][3]},
            new State(SUMO_CLAP_R2 + 2, SF_QUICK_CALL, InitSumoClap),
 // s_SumoClap[2][4]},
            new State(SUMO_CLAP_R2 + 3, SUMO_CLAP_RATE * 10, NullSumo),
 // s_SumoClap[2][5]},
            new State(SUMO_CLAP_R2 + 3, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_SumoClap[2][5]}
    }, { new State(SUMO_CLAP_R3, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[3][1]},
            new State(SUMO_CLAP_R3 + 1, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[3][2]},
            new State(SUMO_CLAP_R3 + 2, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[3][3]},
            new State(SUMO_CLAP_R3 + 2, SF_QUICK_CALL, InitSumoClap),
 // s_SumoClap[3][4]},
            new State(SUMO_CLAP_R3 + 3, SUMO_CLAP_RATE * 10, NullSumo),
 // s_SumoClap[3][5]},
            new State(SUMO_CLAP_R3 + 3, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_SumoClap[3][5]}
    }, { new State(SUMO_CLAP_R4, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[4][1]},
            new State(SUMO_CLAP_R4 + 1, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[4][2]},
            new State(SUMO_CLAP_R4 + 2, SUMO_CLAP_RATE, NullSumo),
 // s_SumoClap[4][3]},
            new State(SUMO_CLAP_R4 + 2, SF_QUICK_CALL, InitSumoClap),
 // s_SumoClap[4][4]},
            new State(SUMO_CLAP_R4 + 3, SUMO_CLAP_RATE * 10, NullSumo),
 // s_SumoClap[4][5]},
            new State(SUMO_CLAP_R4 + 3, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_SumoClap[4][5]}
    } };

    //////////////////////
    //
    // SUMO STOMP
    //
    //////////////////////

    public static final int SUMO_STOMP_RATE = 30;


    private static final State[][] s_SumoStomp = { { new State(SUMO_STOMP_R0, SUMO_STOMP_RATE, NullSumo),
 // s_SumoStomp[0][1]},
            new State(SUMO_STOMP_R0 + 1, SUMO_STOMP_RATE * 3, NullSumo),
 // s_SumoStomp[0][2]},
            new State(SUMO_STOMP_R0 + 2, SUMO_STOMP_RATE, NullSumo),
 // s_SumoStomp[0][3]},
            new State(SUMO_STOMP_R0 + 2, SF_QUICK_CALL, InitSumoStomp),
 // s_SumoStomp[0][4]},
            new State(SUMO_STOMP_R0 + 2, 8, NullSumo),
 // s_SumoStomp[0][5]},
            new State(SUMO_STOMP_R0 + 2, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_SumoStomp[0][5]}
    }, { new State(SUMO_STOMP_R1, SUMO_STOMP_RATE, NullSumo),
 // s_SumoStomp[1][1]},
            new State(SUMO_STOMP_R1 + 1, SUMO_STOMP_RATE * 3, NullSumo),
 // s_SumoStomp[1][2]},
            new State(SUMO_STOMP_R1 + 2, SUMO_STOMP_RATE, NullSumo),
 // s_SumoStomp[1][3]},
            new State(SUMO_STOMP_R1 + 2, SF_QUICK_CALL, InitSumoStomp),
 // s_SumoStomp[1][4]},
            new State(SUMO_STOMP_R1 + 2, 8, NullSumo),
 // s_SumoStomp[1][5]},
            new State(SUMO_STOMP_R1 + 2, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_SumoStomp[1][5]}
    }, { new State(SUMO_STOMP_R2, SUMO_STOMP_RATE, NullSumo),
 // s_SumoStomp[2][1]},
            new State(SUMO_STOMP_R2 + 1, SUMO_STOMP_RATE * 3, NullSumo),
 // s_SumoStomp[2][2]},
            new State(SUMO_STOMP_R2 + 2, SUMO_STOMP_RATE, NullSumo),
 // s_SumoStomp[2][3]},
            new State(SUMO_STOMP_R2 + 2, SF_QUICK_CALL, InitSumoStomp),
 // s_SumoStomp[2][4]},
            new State(SUMO_STOMP_R2 + 2, 8, NullSumo),
 // s_SumoStomp[2][5]},
            new State(SUMO_STOMP_R2 + 2, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_SumoStomp[2][5]}
    }, { new State(SUMO_STOMP_R3, SUMO_STOMP_RATE, NullSumo),
 // s_SumoStomp[3][1]},
            new State(SUMO_STOMP_R3 + 1, SUMO_STOMP_RATE * 3, NullSumo),
 // s_SumoStomp[3][2]},
            new State(SUMO_STOMP_R3 + 2, SUMO_STOMP_RATE, NullSumo),
 // s_SumoStomp[3][3]},
            new State(SUMO_STOMP_R3 + 2, SF_QUICK_CALL, InitSumoStomp),
 // s_SumoStomp[3][4]},
            new State(SUMO_STOMP_R3 + 2, 8, NullSumo),
 // s_SumoStomp[3][5]},
            new State(SUMO_STOMP_R3 + 2, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_SumoStomp[3][5]}
    }, { new State(SUMO_STOMP_R4, SUMO_STOMP_RATE, NullSumo),
 // s_SumoStomp[4][1]},
            new State(SUMO_STOMP_R4 + 1, SUMO_STOMP_RATE * 3, NullSumo),
 // s_SumoStomp[4][2]},
            new State(SUMO_STOMP_R4 + 2, SUMO_STOMP_RATE, NullSumo),
 // s_SumoStomp[4][3]},
            new State(SUMO_STOMP_R4 + 2, SF_QUICK_CALL, InitSumoStomp),
 // s_SumoStomp[4][4]},
            new State(SUMO_STOMP_R4 + 2, 8, NullSumo),
 // s_SumoStomp[4][5]},
            new State(SUMO_STOMP_R4 + 2, SF_QUICK_CALL, InitActorDecide).setNext(),
// s_SumoStomp[4][5]}
    } };

    //////////////////////
    //
    // SUMO DIE
    //
    //////////////////////

    public static final int SUMO_DIE_RATE = 30;


    private static final State[] s_SumoDie = { new State(SUMO_DIE, SUMO_DIE_RATE * 2, NullSumo),
 // s_SumoDie[1]},
            new State(SUMO_DIE + 1, SUMO_DIE_RATE, NullSumo),
 // s_SumoDie[2]},
            new State(SUMO_DIE + 2, SUMO_DIE_RATE, NullSumo),
 // s_SumoDie[3]},
            new State(SUMO_DIE + 3, SUMO_DIE_RATE, NullSumo),
 // s_SumoDie[4]},
            new State(SUMO_DIE + 4, SUMO_DIE_RATE, NullSumo),
 // s_SumoDie[5]},
            new State(SUMO_DIE + 5, SUMO_DIE_RATE, NullSumo),
 // s_SumoDie[6]},
            new State(SUMO_DIE + 6, SUMO_DIE_RATE, NullSumo),
 // s_SumoDie[7]},
            new State(SUMO_DIE + 6, SUMO_DIE_RATE * 3, NullSumo),
 // s_SumoDie[8]},
            new State(SUMO_DIE + 7, SUMO_DIE_RATE, NullSumo),
 // s_SumoDie[9]},
            new State(SUMO_DIE + 6, SUMO_DIE_RATE, NullSumo),
 // s_SumoDie[10]},
            new State(SUMO_DIE + 7, SUMO_DIE_RATE, NullSumo),
 // s_SumoDie[11]},
            new State(SUMO_DIE + 6, SUMO_DIE_RATE - 8, NullSumo),
 // s_SumoDie[12]},
            new State(SUMO_DIE + 7, SUMO_DIE_RATE, NullSumo),
 // s_SumoDie[13]},
            new State(SUMO_DIE + 7, SF_QUICK_CALL, DoSumoDeathMelt),
 // s_SumoDie[14]},
            new State(SUMO_DIE + 6, SUMO_DIE_RATE - 15, NullSumo),
 // s_SumoDie[15]},
            new State(SUMO_DEAD, SF_QUICK_CALL, QueueFloorBlood),
 // s_SumoDie[16]},
            new State(SUMO_DEAD, SUMO_DIE_RATE, DoActorDebris).setNext(),
// s_SumoDie[16]}
    };

    private static final State[] s_SumoDead = { new State(SUMO_DEAD, SUMO_DIE_RATE, DoActorDebris).setNext(),
// s_SumoDead[0]},
    };

    private static final EnemyStateGroup sg_SumoRun = new EnemyStateGroup(s_SumoRun[0], s_SumoRun[1], s_SumoRun[2], s_SumoRun[3], s_SumoRun[4]);
    private static final EnemyStateGroup sg_SumoStand = new EnemyStateGroup(s_SumoStand[0], s_SumoStand[1], s_SumoStand[2], s_SumoStand[3], s_SumoStand[4]);
    private static final EnemyStateGroup sg_SumoPain = new EnemyStateGroup(s_SumoPain[0], s_SumoPain[1], s_SumoPain[2], s_SumoPain[3], s_SumoPain[4]);
    private static final EnemyStateGroup sg_SumoDie = new EnemyStateGroup(s_SumoDie);
    private static final EnemyStateGroup sg_SumoDead = new EnemyStateGroup(s_SumoDead);
    private static final EnemyStateGroup sg_SumoStomp = new EnemyStateGroup(s_SumoStomp[0], s_SumoStomp[1], s_SumoStomp[2], s_SumoStomp[3], s_SumoStomp[4]);
    private static final EnemyStateGroup sg_SumoFart = new EnemyStateGroup(s_SumoFart[0], s_SumoFart[1], s_SumoFart[2], s_SumoFart[3], s_SumoFart[4]);
    private static final EnemyStateGroup sg_SumoClap = new EnemyStateGroup(s_SumoClap[0], s_SumoClap[1], s_SumoClap[2], s_SumoClap[3], s_SumoClap[4]);

    public static void InitSumoStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[] {
                sg_SumoRun,
                sg_SumoStand,
                sg_SumoPain,
                sg_SumoDie,
                sg_SumoDead,
                sg_SumoStomp,
                sg_SumoFart,
                sg_SumoClap
        }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }

    private static final Actor_Action_Set SumoActionSet = new Actor_Action_Set(sg_SumoStand,
            sg_SumoRun, null, null, null, null, null, null, null, null, null, // climb
            sg_SumoPain, // pain
            sg_SumoDie, null, sg_SumoDead, null, null,
            new StateGroup[] { sg_SumoStomp, sg_SumoFart }, new short[] { 800, 1024 },
            new StateGroup[] { sg_SumoClap, sg_SumoStomp, sg_SumoFart },
            new short[] { 400, 750, 1024 }, null, null, null);

    private static final Actor_Action_Set MiniSumoActionSet = new Actor_Action_Set(sg_SumoStand,
            sg_SumoRun, null, null, null, null, null, null, null, null, null, // climb
            sg_SumoPain, // pain
            sg_SumoDie, null, sg_SumoDead, null, null,
            new StateGroup[] { sg_SumoClap }, new short[] { 1024 },
            new StateGroup[] { sg_SumoClap }, new short[] { 1024 }, null, null, null);

    public static void SetupSumo(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        USER u;

        if (TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, SUMO_RUN_R0, s_SumoRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = 6000;
        }

        if (Skill == 0) {
            u.Health = 2000;
        }
        if (Skill == 1) {
            u.Health = 4000;
        }

        ChangeState(SpriteNum, s_SumoRun[0][0]);
        u.Attrib = SumoAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_SumoDie[0];
        u.setRot(sg_SumoRun);

        EnemyDefaults(SpriteNum, SumoActionSet, SumoPersonality);

        sp.setClipdist((512) >> 2);
        if (sp.getPal() == 16) {
            // Mini Sumo
            sp.setXrepeat(43);
            sp.setYrepeat(29);
            u.ActorActionSet = MiniSumoActionSet;
            u.Health = 500;
        } else {
            sp.setXrepeat(115);
            sp.setYrepeat(75);
        }
        alreadydid = false;
    }

    private static void NullSumo(int SpriteNum) {
        USER u = getUser(SpriteNum);

        if (u != null && !TEST(u.Flags, SPR_CLIMBING)) {
            KeepActorOnFloor(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
    }

    private static void DoSumoMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        KeepActorOnFloor(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    private static void InitSumoFart(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);

        PlaySound(DIGI_SUMOFART, sp, v3df_follow);

        InitChemBomb(SpriteNum);

        SetSumoFartQuake(SpriteNum);
        InitSumoNapalm(SpriteNum);
    }

    private static void InitSumoStomp(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);

        PlaySound(DIGI_SUMOSTOMP, sp, v3df_none);
        SetSumoQuake(SpriteNum);
        InitSumoStompAttack(SpriteNum);
    }

    private static void InitSumoClap(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);

        if (sp != null && sp.getPal() == 16 && RANDOM_RANGE(1000) <= 800) {
            InitMiniSumoClap(SpriteNum);
        } else {
            InitSumoSkull(SpriteNum);
        }
    }

    private static void DoSumoDeathMelt(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        PlaySound(DIGI_SUMOFART, sp, v3df_follow);

        u.ID = SUMO_RUN_R0;
        InitChemBomb(SpriteNum);
        u.ID = 0;

        DoMatchEverything(null, sp.getLotag(), ON);

        if (!cfg.isMuteMusic() && !alreadydid) {
            CDAudio_Play(RedBookSong[Level], true);
            alreadydid = true;
        }

        BossSpriteNum[1] = -2; // Sprite is gone, set it back to keep it valid!
    }

    public static void BossHealthMeter() {
        Renderer renderer = game.getRenderer();
        PlayerStr pp = Player[myconnectindex];
        int color, metertics, meterunit;
        int y;

        short health;
        boolean bosswasseen;

        if (Level != 20 && Level != 4 && Level != 11 && Level != 5) {
            return;
        }

        // Don't draw bar for other players
        if (pp != Player[myconnectindex]) {
            return;
        }

        // all enemys
        if ((Level == 20 && (BossSpriteNum[0] == -1 || BossSpriteNum[1] == -1 || BossSpriteNum[2] == -1)) || (Level == 4 && BossSpriteNum[0] == -1) || (Level == 5 && BossSpriteNum[0] == -1) || (Level == 11 && BossSpriteNum[1] == -1)) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(STAT_ENEMY); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite sp = node.get();
                USER u = getUser(i);
                if (u == null) {
                    continue;
                }

                if ((u.ID == SERP_RUN_R0 || u.ID == SUMO_RUN_R0 || u.ID == ZILLA_RUN_R0) && sp.getPal() != 16) {
                    if (u.ID == SERP_RUN_R0) {
                        BossSpriteNum[0] =  i;
                    } else if (u.ID == SUMO_RUN_R0) {
                        BossSpriteNum[1] =  i;
                    } else if (u.ID == ZILLA_RUN_R0) {
                        BossSpriteNum[2] =  i;
                    }
                }
            }
        }

        if (BossSpriteNum[0] <= -1 && BossSpriteNum[1] <= -1 && BossSpriteNum[2] <= -1) {
            return;
        }

        // Frank, good optimization for other levels, but it broke level 20. :(
        // I kept this but had to add a fix.
        bosswasseen = serpwasseen | sumowasseen | zillawasseen;

        // Only show the meter when you can see the boss
        if ((Level == 20 && (!serpwasseen || !sumowasseen || !zillawasseen)) || !bosswasseen) {
            for (int i = 0; i < 3; i++) {
                if (BossSpriteNum[i] >= 0) {
                    Sprite sp = boardService.getSprite(BossSpriteNum[i]);

                    if (sp != null && engine.cansee(sp.getX(),
                         sp.getY(),
                         SPRITEp_TOS(sp),
                         sp.getSectnum(),
                         pp.posx, pp.posy, pp.posz - Z(40),
                         pp.cursectnum)) {
                        if (i == 0 && !serpwasseen) {
                            serpwasseen = true;

                            if (!cfg.isMuteMusic()) {
                                CDAudio_Play(13, true);
                            }

                        } else if (i == 1 && !sumowasseen) {
                            sumowasseen = true;

                            if (!cfg.isMuteMusic()) {
                                CDAudio_Play(13, true);
                            }

                        } else if (i == 2 && !zillawasseen) {
                            zillawasseen = true;

                            if (!cfg.isMuteMusic()) {
                                CDAudio_Play(13, true);
                            }

                        }
                    }
                }
            }
        }

        for (int i = 0; i < 3; i++) {

            if (i == 0 && (!serpwasseen || BossSpriteNum[0] < 0)) {
                continue;
            }
            if (i == 1 && (!sumowasseen || BossSpriteNum[1] < 0)) {
                continue;
            }
            if (i == 2 && (!zillawasseen || BossSpriteNum[2] < 0)) {
                continue;
            }

            // This is needed because of possible saved game situation
            if (game.getScreen() instanceof GameAdapter) {
                if ((!CDAudio_Playing() || playTrack != 13) && !triedplay) {
                    CDAudio_Play(13, true);
                    triedplay = true; // Only try once, then give up
                }
            }

            USER u = getUser(BossSpriteNum[i]);
            if (u == null) {
                continue;
            }

            if (u.ID == SERP_RUN_R0 && serpwasseen) {
                if (Skill == 0) {
                    health = 1100;
                } else if (Skill == 1) {
                    health = 2200;
                } else {
                    health = HEALTH_SERP_GOD;
                }
                meterunit = health / 30;
            } else if (u.ID == SUMO_RUN_R0 && sumowasseen) {
                if (Skill == 0) {
                    health = 2000;
                } else if (Skill == 1) {
                    health = 4000;
                } else {
                    health = 6000;
                }
                meterunit = health / 30;
            } else if (u.ID == ZILLA_RUN_R0 && zillawasseen) {
                if (Skill == 0) {
                    health = 2000;
                } else if (Skill == 1) {
                    health = 4000;
                } else {
                    health = 6000;
                }
                meterunit = health / 30;
            } else {
                continue;
            }

            if (u.Health < meterunit && u.Health > 0) {
                metertics = 1;
            } else {
                metertics = u.Health / meterunit;
            }

            if (metertics <= 0) {
                continue;
            }

            if (CommPlayers < 2) {
                y = 10;
            } else if (CommPlayers <= 4) {
                y = 20;
            } else {
                y = 30;
            }

            if (Level == 20 && CommPlayers >= 2) {
                if (u.ID == SUMO_RUN_R0 && sumowasseen) {
                    y += 10;
                } else if (u.ID == ZILLA_RUN_R0 && zillawasseen) {
                    y += 20;
                }
            }

            if (metertics <= 12 && metertics > 6) {
                color = 20;
            } else if (metertics <= 6) {
                color = 25;
            } else {
                color = 22;
            }

            renderer.rotatesprite((73 + 12) << 16, y << 16, 65536, 0, 5407, 1, 1, (ROTATE_SPRITE_SCREEN_CLIP));
            renderer.rotatesprite((100 + 47) << 16, y << 16, 65536, 0, 5406 - metertics, 1, color, (ROTATE_SPRITE_SCREEN_CLIP));
        }

    }

    public static void SumoSaveable() {
        SaveData(NullSumo);
        SaveData(DoSumoMove);
        SaveData(InitSumoFart);
        SaveData(InitSumoStomp);
        SaveData(InitSumoClap);
        SaveData(DoSumoDeathMelt);

        SaveData(SumoPersonality);

        SaveData(SumoAttrib);

        SaveData(s_SumoRun);
        SaveGroup(sg_SumoRun);
        SaveData(s_SumoStand);
        SaveGroup(sg_SumoStand);
        SaveData(s_SumoPain);
        SaveGroup(sg_SumoPain);
        SaveData(s_SumoFart);
        SaveGroup(sg_SumoFart);
        SaveData(s_SumoClap);
        SaveGroup(sg_SumoClap);
        SaveData(s_SumoStomp);
        SaveGroup(sg_SumoStomp);
        SaveData(s_SumoDie);
        SaveGroup(sg_SumoDie);
        SaveData(s_SumoDead);
        SaveGroup(sg_SumoDead);

        SaveData(SumoActionSet);
        SaveData(MiniSumoActionSet);
    }
}
