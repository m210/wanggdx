package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.DIGI_GIBS1;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Sector.getSectUser;
import static ru.m210projects.Wang.Sound.PlaySound;
import static ru.m210projects.Wang.Sound.v3df_none;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Eel {

    public static final int EEL_RUN_RATE = 20;
    public static final int EEL_FIRE_RATE = 12;
    public static final int EEL_DIE_RATE = 20;
    private static final Animator InitEelFire = new Animator((Animator.Runnable) Eel::InitEelFire);
    private static final Animator DoEelDeath = new Animator((Animator.Runnable) Eel::DoEelDeath);
    private static final Decision[] EelBattle = {new Decision(649, InitActorMoveCloser),
            new Decision(650, InitActorAlertNoise),

            new Decision(1024, InitActorMoveCloser)};
    private static final Decision[] EelOffense = {new Decision(649, InitActorMoveCloser),

            new Decision(750, InitActorAlertNoise),

            new Decision(1024, InitActorMoveCloser)};
    private static final Decision[] EelBroadcast = {new Decision(3, InitActorAlertNoise),

            new Decision(6, InitActorAmbientNoise),

            new Decision(1024, InitActorDecide)};
    private static final Decision[] EelSurprised = {new Decision(701, InitActorMoveCloser),

            new Decision(1024, InitActorDecide)};
    private static final Decision[] EelEvasive = {new Decision(790, InitActorRunAway),

            new Decision(1024, InitActorMoveCloser)};
    private static final Decision[] EelLostTarget = {new Decision(900, InitActorFindPlayer),

            new Decision(1024, InitActorWanderAround)};
    private static final Decision[] EelCloseRange = {new Decision(950, InitActorAttack),

            new Decision(1024, InitActorReposition)};
    private static final Decision[] EelTouchTarget = {new Decision(1024, InitActorAttack)};
    private static final Personality EelPersonality = new Personality(EelBattle, EelOffense, EelBroadcast, EelSurprised, EelEvasive, EelLostTarget, EelCloseRange, EelTouchTarget);
    //////////////////////
    // EEL RUN
    //////////////////////
    private static final ATTRIBUTE EelAttrib = new ATTRIBUTE(new short[]{100, 110, 120, 130}, // Speeds
            new short[]{3, 0, -2, -3}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
    //////////////////////
    //
    // EEL STAND
    //
    //////////////////////
    private static final State[] s_EelDie = {new State(EEL_DIE, EEL_DIE_RATE, DoEelDeath),

            // s_EelDie[1]},
            new State(EEL_DIE, EEL_DIE_RATE, DoEelDeath),
            // s_EelDie[2]},
            new State(EEL_DIE, EEL_DIE_RATE, DoEelDeath),
            // s_EelDie[3]},
            new State(EEL_DIE, EEL_DIE_RATE, DoEelDeath),
            // s_EelDie[4]},
            new State(EEL_DIE, EEL_DIE_RATE, DoEelDeath),
            // s_EelDie[5]},
            new State(EEL_DIE, EEL_DIE_RATE, DoEelDeath).setNext(),
// s_EelDie[5]},
    };
    private static final EnemyStateGroup sg_EelDie = new EnemyStateGroup(s_EelDie);
    //////////////////////
    //
    // EEL FIRE
    //
    //////////////////////
    private static final State[] s_EelDead = {new State(EEL_DEAD, EEL_DIE_RATE, DoActorDebris).setNext(),
// s_EelDead[0]},
    };
    private static final EnemyStateGroup sg_EelDead = new EnemyStateGroup(s_EelDead);
    private static final int EEL_BOB_AMT = (Z(4));
    private static final Animator NullEel = new Animator((Animator.Runnable) Eel::NullEel);
    private static final Animator DoEelMove = new Animator((Animator.Runnable) Eel::DoEelMove);
    //////////////////////
    //
    // EEL DIE
    //
    //////////////////////
    private static final State[][] s_EelRun = {{new State(EEL_RUN_R0, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[0][1]},
            new State(EEL_RUN_R0 + 1, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[0][2]},
            new State(EEL_RUN_R0 + 2, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[0][3]},
            new State(EEL_RUN_R0 + 1, EEL_RUN_RATE, DoEelMove),
// s_EelRun[0][0]},
    }, {new State(EEL_RUN_R1, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[1][1]},
            new State(EEL_RUN_R1 + 1, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[1][2]},
            new State(EEL_RUN_R1 + 2, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[1][3]},
            new State(EEL_RUN_R1 + 1, EEL_RUN_RATE, DoEelMove),
// s_EelRun[1][0]},
    }, {new State(EEL_RUN_R2, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[2][1]},
            new State(EEL_RUN_R2 + 1, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[2][2]},
            new State(EEL_RUN_R2 + 2, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[2][3]},
            new State(EEL_RUN_R2 + 1, EEL_RUN_RATE, DoEelMove),
// s_EelRun[2][0]},
    }, {new State(EEL_RUN_R3, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[3][1]},
            new State(EEL_RUN_R3 + 1, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[3][2]},
            new State(EEL_RUN_R3 + 2, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[3][3]},
            new State(EEL_RUN_R3 + 1, EEL_RUN_RATE, DoEelMove),
// s_EelRun[3][0]},
    }, {new State(EEL_RUN_R4, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[4][1]},
            new State(EEL_RUN_R4 + 1, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[4][2]},
            new State(EEL_RUN_R4 + 2, EEL_RUN_RATE, DoEelMove),
            // s_EelRun[4][3]},
            new State(EEL_RUN_R4 + 1, EEL_RUN_RATE, DoEelMove),
// s_EelRun[4][0]},
    }};
    private static final EnemyStateGroup sg_EelRun = new EnemyStateGroup(s_EelRun[0], s_EelRun[1], s_EelRun[2], s_EelRun[3], s_EelRun[4]);
    private static final State[][] s_EelStand = {{new State(EEL_RUN_R0, EEL_RUN_RATE, DoEelMove).setNext(),
// s_EelStand[0][0]},
    }, {new State(EEL_RUN_R1, EEL_RUN_RATE, DoEelMove).setNext(),
// s_EelStand[1][0]},
    }, {new State(EEL_RUN_R2, EEL_RUN_RATE, DoEelMove).setNext(),
// s_EelStand[2][0]},
    }, {new State(EEL_RUN_R3, EEL_RUN_RATE, DoEelMove).setNext(),
// s_EelStand[3][0]},
    }, {new State(EEL_RUN_R4, EEL_RUN_RATE, DoEelMove).setNext(),
// s_EelStand[4][0]},
    }};
    private static final EnemyStateGroup sg_EelStand = new EnemyStateGroup(s_EelStand[0], s_EelStand[1], s_EelStand[2], s_EelStand[3], s_EelStand[4]);
    private static final State[][] s_EelAttack = {{new State(EEL_FIRE_R0, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[0][1]},
            new State(EEL_FIRE_R0 + 1, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[0][2]},
            new State(EEL_FIRE_R0 + 2, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[0][3]},
            new State(EEL_FIRE_R0 + 2, SF_QUICK_CALL, InitEelFire),
            // s_EelAttack[0][4]},
            new State(EEL_FIRE_R0 + 2, EEL_FIRE_RATE, NullEel),
            // s_EelAttack[0][5]},
            new State(EEL_FIRE_R0 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_EelAttack[0][6]},
            new State(EEL_RUN_R0 + 3, EEL_FIRE_RATE, DoEelMove).setNext(),
// s_EelAttack[0][6]}
    }, {new State(EEL_FIRE_R1, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[1][1]},
            new State(EEL_FIRE_R1 + 1, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[1][2]},
            new State(EEL_FIRE_R1 + 2, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[1][3]},
            new State(EEL_FIRE_R1 + 2, SF_QUICK_CALL, InitEelFire),
            // s_EelAttack[1][5]},
            new State(EEL_FIRE_R1 + 2, EEL_FIRE_RATE, NullEel),
            // s_EelAttack[1][6]},
            new State(EEL_FIRE_R1 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_EelAttack[1][7]},
            new State(EEL_RUN_R0 + 3, EEL_FIRE_RATE, DoEelMove).setNext(),
// s_EelAttack[1][7]}
    }, {new State(EEL_FIRE_R2, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[2][1]},
            new State(EEL_FIRE_R2 + 1, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[2][2]},
            new State(EEL_FIRE_R2 + 2, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[2][3]},
            new State(EEL_FIRE_R2 + 2, SF_QUICK_CALL, InitEelFire),
            // s_EelAttack[2][4]},
            new State(EEL_FIRE_R2 + 2, EEL_FIRE_RATE, NullEel),
            // s_EelAttack[2][5]},
            new State(EEL_FIRE_R2 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_EelAttack[2][6]},
            new State(EEL_RUN_R0 + 3, EEL_FIRE_RATE, DoEelMove).setNext(),
// s_EelAttack[2][6]}
    }, {new State(EEL_RUN_R3, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[3][1]},
            new State(EEL_RUN_R3 + 1, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[3][2]},
            new State(EEL_RUN_R3 + 2, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[3][3]},
            new State(EEL_RUN_R3 + 2, SF_QUICK_CALL, InitEelFire),
            // s_EelAttack[3][4]},
            new State(EEL_RUN_R3 + 2, EEL_FIRE_RATE, NullEel),
            // s_EelAttack[3][5]},
            new State(EEL_RUN_R3 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_EelAttack[3][6]},
            new State(EEL_RUN_R0 + 3, EEL_FIRE_RATE, DoEelMove).setNext(),
// s_EelAttack[3][6]}
    }, {new State(EEL_RUN_R4, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[4][1]},
            new State(EEL_RUN_R4 + 1, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[4][2]},
            new State(EEL_RUN_R4 + 2, EEL_FIRE_RATE * 2, NullEel),
            // s_EelAttack[4][3]},
            new State(EEL_RUN_R4 + 2, SF_QUICK_CALL, InitEelFire),
            // s_EelAttack[4][4]},
            new State(EEL_RUN_R4 + 2, EEL_FIRE_RATE, NullEel),
            // s_EelAttack[4][5]},
            new State(EEL_RUN_R4 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_EelAttack[4][6]},
            new State(EEL_RUN_R0 + 3, EEL_FIRE_RATE, DoEelMove).setNext()// s_EelAttack[4][6]}
    }};
    private static final EnemyStateGroup sg_EelAttack = new EnemyStateGroup(s_EelAttack[0], s_EelAttack[1], s_EelAttack[2], s_EelAttack[3], s_EelAttack[4]);
    private static final Actor_Action_Set EelActionSet = new Actor_Action_Set(sg_EelStand, sg_EelRun, null, null, null, sg_EelRun, null, null, sg_EelStand, null, null, // climb
            sg_EelStand, // pain
            sg_EelDie, null, sg_EelDead, null, null, new StateGroup[]{sg_EelAttack}, new short[]{1024}, new StateGroup[]{sg_EelAttack}, new short[]{1024}, null, null, null);


    public static void InitEelStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{sg_EelRun, sg_EelStand, sg_EelDie, sg_EelAttack, sg_EelDead}) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }

    private static void EelCommon(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setClipdist((100) >> 2);
        u.floor_dist = Z(16);
        u.ceiling_dist = Z(20);

        u.sz = sp.getZ();

        sp.setXrepeat(35);
        sp.setYrepeat(27);
        u.Radius = 400;
    }

    public static void SetupEel(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        USER u;

        if (TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, EEL_RUN_R0, s_EelRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = 40;
        }

        ChangeState(SpriteNum, s_EelRun[0][0]);
        u.Attrib = EelAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_EelDie[0];
        u.setRot(sg_EelRun);

        EnemyDefaults(SpriteNum, EelActionSet, EelPersonality);

        u.Flags |= (SPR_NO_SCAREDZ | SPR_XFLIP_TOGGLE);

        EelCommon(SpriteNum);

        u.Flags &= ~(SPR_SHADOW); // Turn off shadows
        u.zclip = Z(8);
    }

    private static void NullEel(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        DoEelMatchPlayerZ(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    private static void DoEelMatchPlayerZ(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }
        int zdiff, zdist;
        int loz, hiz;
        int dist;

        int bound;

        ru.m210projects.Build.Types.Sector s = boardService.getSector(sp.getSectnum());
        if (s != null && FAF_ConnectArea(sp.getSectnum())) {
            if (u.hi_sectp != -1) {
                u.hiz = s.getCeilingz() + Z(16);
                u.hi_sectp = sp.getSectnum();
            } else {
                if (u.hiz < s.getCeilingz() + Z(16)) {
                    u.hiz = s.getCeilingz() + Z(16);
                }
            }
        }

        // actor does a sine wave about u.sz - this is the z mid point

        zdiff = (SPRITEp_BOS(tsp) - Z(8)) - u.sz;

        // check z diff of the player and the sprite
        zdist = Z(20 + RANDOM_RANGE(64)); // put a random amount
        if (klabs(zdiff) > zdist) {
            if (zdiff > 0)
            // manipulate the z midpoint
            {
                u.sz += 160 * ACTORMOVETICS;
            } else {
                u.sz -= 160 * ACTORMOVETICS;
            }
        }

        // save off lo and hi z
        loz = u.loz;
        hiz = u.hiz;

        // adjust loz/hiz for water depth
        Sect_User su = getSectUser(u.lo_sectp);
        if (u.lo_sectp != -1 && su != null && su.depth != 0) {
            loz -= Z(su.depth) - Z(8);
        }

        Sprite losp = boardService.getSprite(u.lo_sp);
        // lower bound
        if (losp != null && u.tgt_sp == u.hi_sp) {
            dist = DISTANCE(sp.getX(), sp.getY(), losp.getX(), losp.getY());
            if (dist <= 300) {
                bound = u.sz;
            } else {
                bound = loz - u.floor_dist;
            }
        } else {
            bound = loz - u.floor_dist - EEL_BOB_AMT;
        }

        if (u.sz > bound) {
            u.sz = bound;
        }

        Sprite hisp = boardService.getSprite(u.hi_sp);
        // upper bound
        if (hisp != null && u.tgt_sp == u.hi_sp) {
            dist = DISTANCE(sp.getX(), sp.getY(), hisp.getX(), hisp.getY());
            if (dist <= 300) {
                bound = u.sz;
            } else {
                bound = hiz + u.ceiling_dist;
            }
        } else {
            bound = hiz + u.ceiling_dist + EEL_BOB_AMT;
        }

        if (u.sz < bound) {
            u.sz = bound;
        }

        u.sz = Math.min(u.sz, loz - u.floor_dist);
        u.sz = Math.max(u.sz, hiz + u.ceiling_dist);

        u.Counter = ((u.Counter + (ACTORMOVETICS << 3) + (ACTORMOVETICS << 1)) & 2047);
        sp.setZ(u.sz + ((EEL_BOB_AMT * EngineUtils.sin(u.Counter)) >> 14));

        bound = u.hiz + u.ceiling_dist + EEL_BOB_AMT;
        if (sp.getZ() < bound) {
            // bumped something
            sp.setZ(u.sz = bound + EEL_BOB_AMT);
        }
    }

    public static void InitEelFire(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        ListNode<Sprite> nexti;
        for (int j : StatDamageList) {
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite hp = node.get();
                USER hu = getUser(i);

                if (i == SpriteNum) {
                    continue;
                }

                if (i != u.tgt_sp) {
                    continue;
                }

                if (hu != null && FindDistance3D(sp.getX() - hp.getX(),
                        sp.getY() - hp.getY(),
                        (sp.getZ() - hp.getZ()) >> 4) > hu.Radius + u.Radius) {
                    continue;
                }

                int dist = DISTANCE(hp.getX(),
                        hp.getY(),
                        sp.getX(),
                        sp.getY());

                if (dist < CLOSE_RANGE_DIST_FUDGE(sp, hp, 600) && FACING_RANGE(hp, sp, 150)) {
                    PlaySound(DIGI_GIBS1, sp, v3df_none);
                    DoDamage(i, SpriteNum);
                } else {
                    InitActorReposition.animatorInvoke(SpriteNum);
                }
            }
        }
    }

    private static void DoEelDeath(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int nx, ny;
        if (TEST(u.Flags, SPR_FALLING)) {
            DoFall(SpriteNum);
        } else {
            DoFindGroundPoint(SpriteNum);
            u.floor_dist = 0;
            DoBeginFall(SpriteNum);
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        // slide while falling
        nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

        u.moveSpriteReturn = move_sprite(SpriteNum, nx, ny, 0, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, ACTORMOVETICS);
        DoFindGroundPoint(SpriteNum);

        // on the ground
        if (sp.getZ() >= u.loz) {
            u.Flags &= ~(SPR_FALLING | SPR_SLIDING);
            if (RANDOM_RANGE(1000) > 500) {
                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_XFLIP));
            }
            if (RANDOM_RANGE(1000) > 500) {
                sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YFLIP));
            }
            NewStateGroup(SpriteNum, u.ActorActionSet.Dead);
        }
    }

    private static void DoEelMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (SpriteOverlap(SpriteNum, u.tgt_sp)) {
            NewStateGroup(SpriteNum, u.ActorActionSet.CloseAttack[0]);
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        DoEelMatchPlayerZ(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    public static void EelSaveable() {
        SaveData(NullEel);
        SaveData(InitEelFire);

        SaveData(DoEelDeath);
        SaveData(DoEelMove);
        SaveData(EelPersonality);
        SaveData(EelAttrib);

        SaveData(s_EelRun);
        SaveGroup(sg_EelRun);
        SaveData(s_EelStand);
        SaveGroup(sg_EelStand);
        SaveData(s_EelAttack);
        SaveGroup(sg_EelAttack);
        SaveData(s_EelDie);
        SaveGroup(sg_EelDie);
        SaveData(s_EelDead);
        SaveGroup(sg_EelDead);

        SaveData(EelActionSet);
    }
}
