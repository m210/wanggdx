package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Enemies.Ripper.InitCoolgFire;
import static ru.m210projects.Wang.Game.TotalKillable;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JWeapon.InitBloodSpray;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Player.QueueFloorBlood;
import static ru.m210projects.Wang.Sector.getSectUser;
import static ru.m210projects.Wang.Sound.PlaySound;
import static ru.m210projects.Wang.Sound.v3df_follow;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Coolg {

    public static final int COOLG_RUN_RATE = 40;
//    public static final int COOLG_RATE = 16;
    public static final int COOLG_FIRE_RATE = 12;
    public static final int COOLG_PAIN_RATE = 15;
    public static final int COOLG_DIE_RATE = 20;
    public static final int COOLG_DIE = 4307;
    public static final int COOLG_DEAD = 4307 + 5;
    public static final int COOLG_BIRTH_RATE = 20;
    public static final int COOLG_BIRTH = 4268;
    private static final Animator DoCoolgDeath = new Animator((Animator.Runnable) Coolg::DoCoolgDeath);
    private static final Decision[] CoolgOffense = {new Decision(449, InitActorMoveCloser),
            // new Decision(554, InitActorAmbientNoise ),
            new Decision(1024, InitActorAttack)};
    private static final Decision[] CoolgBroadcast = {
            // new Decision(1, InitActorAlertNoise ),
            new Decision(1, InitActorAmbientNoise), new Decision(1024, InitActorDecide)};
    private static final Decision[] CoolgLostTarget = {new Decision(900, InitActorFindPlayer), new Decision(1024, InitActorWanderAround)};
    private static final Decision[] CoolgCloseRange = {new Decision(800, InitActorAttack), new Decision(1024, InitActorReposition)};
    //////////////////////
    //
    // COOLG RUN
    //////////////////////
    private static final Decision[] CoolgTouchTarget = {
            // new Decision(50, InitCoolgCircle ),
            new Decision(1024, InitActorAttack)};
    private static final ATTRIBUTE CoolgAttrib = new ATTRIBUTE(new short[]{60, 80, 150, 190}, // Speeds
            new short[]{3, 0, -2, -3}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_CGAMBIENT, DIGI_CGALERT, 0, DIGI_CGPAIN, DIGI_CGSCREAM, DIGI_CGMATERIALIZE, DIGI_CGTHIGHBONE, DIGI_CGMAGIC, DIGI_CGMAGICHIT, 0});
    private static final State[] s_CoolgDie = {new State(COOLG_DIE, COOLG_DIE_RATE, DoCoolgDeath), // s_CoolgDie[1]},
            new State(COOLG_DIE + 1, COOLG_DIE_RATE, DoCoolgDeath), // s_CoolgDie[2]},
            new State(COOLG_DIE + 2, COOLG_DIE_RATE, DoCoolgDeath), // s_CoolgDie[3]},
            new State(COOLG_DIE + 3, COOLG_DIE_RATE, DoCoolgDeath), // s_CoolgDie[4]},
            new State(COOLG_DIE + 4, COOLG_DIE_RATE, DoCoolgDeath), // s_CoolgDie[5]},
            new State(COOLG_DIE + 5, COOLG_DIE_RATE, DoCoolgDeath).setNext(),// s_CoolgDie[5]},
    };
    private static final EnemyStateGroup sg_CoolgDie = new EnemyStateGroup(s_CoolgDie);
    private static final State[] s_CoolgDead = {new State(COOLG_DEAD, SF_QUICK_CALL, QueueFloorBlood), // s_CoolgDead[1]},
            new State(COOLG_DEAD, COOLG_DIE_RATE, DoActorDebris).setNext(),// s_CoolgDead[1]},
    };
    private static final EnemyStateGroup sg_CoolgDead = new EnemyStateGroup(s_CoolgDead);
    //////////////////////
    //
    // COOLG FIRE
    //
    //////////////////////
    private static final int COOLG_BOB_AMT = (Z(8));
    private static final Animator DoCoolgMove = new Animator((Animator.Runnable) Coolg::DoCoolgMove);
    //////////////////////
    //
    // COOLG PAIN
    //
    //////////////////////
    private static final State[][] s_CoolgRun = {{new State(COOLG_RUN_R0, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[0][1]},
            new State(COOLG_RUN_R0 + 1, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[0][2]},
            new State(COOLG_RUN_R0 + 2, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[0][3]},
            new State(COOLG_RUN_R0 + 3, COOLG_RUN_RATE, DoCoolgMove),// s_CoolgRun[0][0]},
    }, {new State(COOLG_RUN_R1, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[1][1]},
            new State(COOLG_RUN_R1 + 1, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[1][2]},
            new State(COOLG_RUN_R1 + 2, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[1][3]},
            new State(COOLG_RUN_R1 + 3, COOLG_RUN_RATE, DoCoolgMove),// s_CoolgRun[1][0]},
    }, {new State(COOLG_RUN_R2, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[2][1]},
            new State(COOLG_RUN_R2 + 1, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[2][2]},
            new State(COOLG_RUN_R2 + 2, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[2][3]},
            new State(COOLG_RUN_R2 + 3, COOLG_RUN_RATE, DoCoolgMove),// s_CoolgRun[2][0]},
    }, {new State(COOLG_RUN_R3, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[3][1]},
            new State(COOLG_RUN_R3 + 1, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[3][2]},
            new State(COOLG_RUN_R3 + 2, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[3][3]},
            new State(COOLG_RUN_R3 + 3, COOLG_RUN_RATE, DoCoolgMove),// s_CoolgRun[3][0]},
    }, {new State(COOLG_RUN_R4, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[4][1]},
            new State(COOLG_RUN_R4 + 1, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[4][2]},
            new State(COOLG_RUN_R4 + 2, COOLG_RUN_RATE, DoCoolgMove), // s_CoolgRun[4][3]},
            new State(COOLG_RUN_R4 + 3, COOLG_RUN_RATE, DoCoolgMove),// s_CoolgRun[4][0]},
    }};
    private static final EnemyStateGroup sg_CoolgRun = new EnemyStateGroup(s_CoolgRun[0], s_CoolgRun[1], s_CoolgRun[2], s_CoolgRun[3], s_CoolgRun[4]);
    private static final State[][] s_CoolgStand = {{new State(COOLG_RUN_R0, COOLG_RUN_RATE, DoCoolgMove).setNext()// s_CoolgStand[0][0]},
    }, {new State(COOLG_RUN_R1, COOLG_RUN_RATE, DoCoolgMove).setNext()// s_CoolgStand[1][0]},
    }, {new State(COOLG_RUN_R2, COOLG_RUN_RATE, DoCoolgMove).setNext()// s_CoolgStand[2][0]},
    }, {new State(COOLG_RUN_R3, COOLG_RUN_RATE, DoCoolgMove).setNext()// s_CoolgStand[3][0]},
    }, {new State(COOLG_RUN_R4, COOLG_RUN_RATE, DoCoolgMove).setNext()// s_CoolgStand[4][0]},
    }};
    private static final EnemyStateGroup sg_CoolgStand = new EnemyStateGroup(s_CoolgStand[0], s_CoolgStand[1], s_CoolgStand[2], s_CoolgStand[3], s_CoolgStand[4]);
    private static final Animator DoCoolgCircle = new Animator((Animator.Runnable) Coolg::DoCoolgCircle);
    private static final Animator InitCoolgCircle = new Animator((Animator.Runnable) Coolg::InitCoolgCircle);
    private static final Decision[] CoolgBattle = {new Decision(50, InitCoolgCircle), new Decision(450, InitActorMoveCloser),
            // new Decision(456, InitActorAmbientNoise ),
            // new Decision(760, InitActorRunAway ),
            new Decision(1024, InitActorAttack)};
    //////////////////////
    //
    // COOLG BIRTH
    //
    //////////////////////
    private static final Decision[] CoolgSurprised = {new Decision(100, InitCoolgCircle), new Decision(701, InitActorMoveCloser), new Decision(1024, InitActorDecide)};
    private static final Decision[] CoolgEvasive = {new Decision(20, InitCoolgCircle), new Decision(1024, InitActorRunAway)};
    private static final Personality CoolgPersonality = new Personality(CoolgBattle, CoolgOffense, CoolgBroadcast, CoolgSurprised, CoolgEvasive, CoolgLostTarget, CoolgCloseRange, CoolgTouchTarget);
    private static final Animator NullCoolg = new Animator((Animator.Runnable) Coolg::NullCoolg);
    //////////////////////
    //
    // COOLG DIE
    //
    //////////////////////
    private static final State[][] s_CoolgAttack = {{new State(COOLG_FIRE_R0, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[0][1]},
            new State(COOLG_FIRE_R0 + 1, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[0][2]},
            new State(COOLG_FIRE_R0 + 2, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[0][3]},
            new State(COOLG_FIRE_R0 + 2, SF_QUICK_CALL, InitCoolgFire), // s_CoolgAttack[0][4]},
            new State(COOLG_FIRE_R0 + 2, COOLG_FIRE_RATE, NullCoolg), // s_CoolgAttack[0][5]},
            new State(COOLG_FIRE_R0 + 2, SF_QUICK_CALL, InitActorDecide), // s_CoolgAttack[0][6]},
            new State(COOLG_RUN_R0 + 2, COOLG_FIRE_RATE, DoCoolgMove).setNext(),// s_CoolgAttack[0][6]}
    }, {new State(COOLG_FIRE_R1, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[1][1]},
            new State(COOLG_FIRE_R1 + 1, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[1][2]},
            new State(COOLG_FIRE_R1 + 2, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[1][3]},
            new State(COOLG_FIRE_R1 + 2, SF_QUICK_CALL, InitCoolgFire), // s_CoolgAttack[1][4]},
            new State(COOLG_FIRE_R1 + 2, COOLG_FIRE_RATE, NullCoolg), // s_CoolgAttack[1][5]},
            new State(COOLG_FIRE_R1 + 2, SF_QUICK_CALL, InitActorDecide), // s_CoolgAttack[1][6]},
            new State(COOLG_RUN_R0 + 2, COOLG_FIRE_RATE, DoCoolgMove).setNext(),// s_CoolgAttack[1][6]}
    }, {new State(COOLG_FIRE_R2, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[2][1]},
            new State(COOLG_FIRE_R2 + 1, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[2][2]},
            new State(COOLG_FIRE_R2 + 2, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[2][3]},
            new State(COOLG_FIRE_R2 + 2, SF_QUICK_CALL, InitCoolgFire), // s_CoolgAttack[2][4]},
            new State(COOLG_FIRE_R2 + 2, COOLG_FIRE_RATE, NullCoolg), // s_CoolgAttack[2][5]},
            new State(COOLG_FIRE_R2 + 2, SF_QUICK_CALL, InitActorDecide), // s_CoolgAttack[2][6]},
            new State(COOLG_RUN_R0 + 2, COOLG_FIRE_RATE, DoCoolgMove).setNext(),// s_CoolgAttack[2][6]}
    }, {new State(COOLG_RUN_R3, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[3][1]},
            new State(COOLG_RUN_R3 + 1, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[3][2]},
            new State(COOLG_RUN_R3 + 2, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[3][3]},
            new State(COOLG_RUN_R3 + 2, SF_QUICK_CALL, InitCoolgFire), // s_CoolgAttack[3][4]},
            new State(COOLG_RUN_R3 + 2, COOLG_FIRE_RATE, NullCoolg), // s_CoolgAttack[3][5]},
            new State(COOLG_RUN_R3 + 2, SF_QUICK_CALL, InitActorDecide), // s_CoolgAttack[3][6]},
            new State(COOLG_RUN_R0 + 2, COOLG_FIRE_RATE, DoCoolgMove).setNext(),// s_CoolgAttack[3][6]}
    }, {new State(COOLG_RUN_R4, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[4][1]},
            new State(COOLG_RUN_R4 + 1, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[4][2]},
            new State(COOLG_RUN_R4 + 2, COOLG_FIRE_RATE * 2, NullCoolg), // s_CoolgAttack[4][3]},
            new State(COOLG_RUN_R4 + 2, SF_QUICK_CALL, InitCoolgFire), // s_CoolgAttack[4][4]},
            new State(COOLG_RUN_R4 + 2, COOLG_FIRE_RATE, NullCoolg), // s_CoolgAttack[4][5]},
            new State(COOLG_RUN_R4 + 2, SF_QUICK_CALL, InitActorDecide), // s_CoolgAttack[4][6]},
            new State(COOLG_RUN_R0 + 2, COOLG_FIRE_RATE, DoCoolgMove).setNext(),// s_CoolgAttack[4][6]}
    }};
    private static final EnemyStateGroup sg_CoolgAttack = new EnemyStateGroup(s_CoolgAttack[0], s_CoolgAttack[1], s_CoolgAttack[2], s_CoolgAttack[3], s_CoolgAttack[4]);
    private static final Animator DoCoolgPain = new Animator((Animator.Runnable) Coolg::DoCoolgPain);
    private static final State[][] s_CoolgPain = {{new State(COOLG_PAIN_R0, COOLG_PAIN_RATE, DoCoolgPain), // s_CoolgPain[0][1]},
            new State(COOLG_PAIN_R0, COOLG_PAIN_RATE, DoCoolgPain).setNext(),// s_CoolgPain[0][1]},
    }, {new State(COOLG_RUN_R1, COOLG_PAIN_RATE, DoCoolgPain), // s_CoolgPain[1][1]},
            new State(COOLG_RUN_R1, COOLG_PAIN_RATE, DoCoolgPain).setNext(),// s_CoolgPain[1][1]},
    }, {new State(COOLG_RUN_R2, COOLG_PAIN_RATE, DoCoolgPain), // s_CoolgPain[2][1]},
            new State(COOLG_RUN_R2, COOLG_PAIN_RATE, DoCoolgPain).setNext(),// s_CoolgPain[2][1]},
    }, {new State(COOLG_RUN_R3, COOLG_PAIN_RATE, DoCoolgPain), // s_CoolgPain[3][1]},
            new State(COOLG_RUN_R3, COOLG_PAIN_RATE, DoCoolgPain).setNext(),// s_CoolgPain[3][1]},
    }, {new State(COOLG_RUN_R4, COOLG_PAIN_RATE, DoCoolgPain), // s_CoolgPain[4][1]},
            new State(COOLG_RUN_R4, COOLG_PAIN_RATE, DoCoolgPain).setNext(),// s_CoolgPain[4][1]},
    }};
    private static final EnemyStateGroup sg_CoolgPain = new EnemyStateGroup(s_CoolgPain[0], s_CoolgPain[1], s_CoolgPain[2], s_CoolgPain[3], s_CoolgPain[4]);
    private static final Actor_Action_Set CoolgActionSet = new Actor_Action_Set(sg_CoolgStand, sg_CoolgRun, null, null, null, null, null, null, null, null, null, // climb
            sg_CoolgPain, // pain
            sg_CoolgDie, null, sg_CoolgDead, null, null, new StateGroup[]{sg_CoolgAttack}, new short[]{1024}, new StateGroup[]{sg_CoolgAttack}, new short[]{1024}, null, null, null);
    private static final Animator DoCoolgBirth = new Animator((Animator.Runnable) Coolg::DoCoolgBirth);
    private static final State[] s_CoolgBirth = {new State(COOLG_BIRTH, COOLG_BIRTH_RATE, null), // s_CoolgBirth[1]},
            new State(COOLG_BIRTH + 1, COOLG_BIRTH_RATE, null), // s_CoolgBirth[2]},
            new State(COOLG_BIRTH + 2, COOLG_BIRTH_RATE, null), // s_CoolgBirth[3]},
            new State(COOLG_BIRTH + 3, COOLG_BIRTH_RATE, null), // s_CoolgBirth[4]},
            new State(COOLG_BIRTH + 4, COOLG_BIRTH_RATE, null), // s_CoolgBirth[5]},
            new State(COOLG_BIRTH + 5, COOLG_BIRTH_RATE, null), // s_CoolgBirth[6]},
            new State(COOLG_BIRTH + 6, COOLG_BIRTH_RATE, null), // s_CoolgBirth[7]},
            new State(COOLG_BIRTH + 7, COOLG_BIRTH_RATE, null), // s_CoolgBirth[8]},
            new State(COOLG_BIRTH + 8, COOLG_BIRTH_RATE, null), // s_CoolgBirth[9]},
            new State(COOLG_BIRTH + 8, COOLG_BIRTH_RATE, null), // s_CoolgBirth[10]},
            new State(COOLG_BIRTH + 8, SF_QUICK_CALL, DoCoolgBirth).setNext(),// s_CoolgBirth[10]}
    };

    public static void InitCoolgStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{sg_CoolgStand, sg_CoolgRun, sg_CoolgPain, sg_CoolgDie, sg_CoolgDead, sg_CoolgAttack}) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }

        State.InitState(s_CoolgBirth);
    }

    private static void CoolgCommon(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setClipdist((200) >> 2);
        u.floor_dist =  Z(16);
        u.ceiling_dist =  Z(20);

        u.sz = sp.getZ();

        sp.setXrepeat(42);
        sp.setYrepeat(42);
        sp.setExtra(sp.getExtra() | (SPRX_PLAYER_OR_ENEMY));
    }

    public static void SetupCoolg(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        USER u;

        if (TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, COOLG_RUN_R0, s_CoolgRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_COOLIE_GHOST;
        }

        ChangeState(SpriteNum, s_CoolgRun[0][0]);
        u.Attrib = CoolgAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_CoolgDie[0];
        u.setRot(sg_CoolgRun);

        EnemyDefaults(SpriteNum, CoolgActionSet, CoolgPersonality);

        u.Flags |= (SPR_NO_SCAREDZ | SPR_XFLIP_TOGGLE);

        CoolgCommon(SpriteNum);
    }

    public static void NewCoolg(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        USER nu;
        Sprite np;

        int newsp = SpawnSprite(STAT_ENEMY, COOLG_RUN_R0, s_CoolgBirth[0], sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 50);

        nu = getUser(newsp);
        np = boardService.getSprite(newsp);
        if (nu == null || np == null) {
            return;
        }

        ChangeState(newsp, s_CoolgBirth[0]);
        nu.StateEnd = s_CoolgDie[0];
        nu.setRot(sg_CoolgRun);
        np.setPal(nu.spal = u.spal);

        nu.ActorActionSet = CoolgActionSet;

        np.setShade(sp.getShade());
        nu.Personality = CoolgPersonality;
        nu.Attrib = CoolgAttrib;

        // special case
        TotalKillable++;
        CoolgCommon(newsp);
    }

    private static void DoCoolgBirth(int newsp) {
        USER u = getUser(newsp);
        if (u == null) {
            return;
        }

        u.Health = HEALTH_COOLIE_GHOST;
        u.Attrib = CoolgAttrib;
        DoActorSetSpeed(newsp, NORM_SPEED);

        ChangeState(newsp, s_CoolgRun[0][0]);
        u.StateEnd = s_CoolgDie[0];
        u.setRot(sg_CoolgRun);

        EnemyDefaults(newsp, CoolgActionSet, CoolgPersonality);
        // special case
        TotalKillable--;

        u.Flags |= (SPR_NO_SCAREDZ | SPR_XFLIP_TOGGLE);
        CoolgCommon(newsp);
    }

    private static void NullCoolg(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        u.ShellNum -= ACTORMOVETICS;

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        DoCoolgMatchPlayerZ(SpriteNum);

        DoActorSectorDamage(SpriteNum);
    }

    private static void DoCoolgMatchPlayerZ(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }
        int zdiff, zdist;
        int loz, hiz;

        int bound;

        // If blocking bits get unset, just die
        if (!TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK) || !TEST(sp.getCstat(), CSTAT_SPRITE_BLOCK_HITSCAN)) {
            InitBloodSpray(SpriteNum, true, 105);
            InitBloodSpray(SpriteNum, true, 105);
            UpdateSinglePlayKills(SpriteNum, null);
            SetSuicide(SpriteNum);
        }

        // actor does a sine wave about u.sz - this is the z mid point

        zdiff = (SPRITEp_MID(tsp)) - u.sz;

        // check z diff of the player and the sprite
        zdist = Z(20 + RANDOM_RANGE(100)); // put a random amount

        if (klabs(zdiff) > zdist) {
            if (zdiff > 0) {
                u.sz += 170 * ACTORMOVETICS;
            } else {
                u.sz -= 170 * ACTORMOVETICS;
            }
        }

        // save off lo and hi z
        loz = u.loz;
        hiz = u.hiz;

        Sect_User su = getSectUser(u.lo_sectp);
        // adjust loz/hiz for water depth
        if (u.lo_sectp != -1 && su != null && su.depth != 0) {
            loz -= Z(su.depth) - Z(8);
        }

        // lower bound
        if (u.lo_sp != -1) {
            bound = loz - u.floor_dist;
        } else {
            bound = loz - u.floor_dist - COOLG_BOB_AMT;
        }

        if (u.sz > bound) {
            u.sz = bound;
        }

        // upper bound
        if (u.hi_sp != -1) {
            bound = hiz + u.ceiling_dist;
        } else {
            bound = hiz + u.ceiling_dist + COOLG_BOB_AMT;
        }

        if (u.sz < bound) {
            u.sz = bound;
        }

        u.sz = Math.min(u.sz, loz - u.floor_dist);
        u.sz = Math.max(u.sz, hiz + u.ceiling_dist);

        u.Counter =  ((u.Counter + (ACTORMOVETICS << 3)) & 2047);
        sp.setZ(u.sz + ((COOLG_BOB_AMT * EngineUtils.sin(u.Counter)) >> 14));

        bound = u.hiz + u.ceiling_dist + COOLG_BOB_AMT;
        if (sp.getZ() < bound) {
            // bumped something
            sp.setZ(u.sz = bound + COOLG_BOB_AMT);
        }
    }

    private static void InitCoolgCircle(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.ActorActionFunc = DoCoolgCircle;

        NewStateGroup(SpriteNum, u.ActorActionSet.Run);

        // set it close
        DoActorSetSpeed(SpriteNum, FAST_SPEED);

        // set to really fast
        sp.setXvel(400);
        // angle adjuster
        u.Counter2 =  (sp.getXvel() / 3);
        // random angle direction
        if (RANDOM_P2(1024) < 512) {
            u.Counter2 =  -u.Counter2;
        }

        // z velocity
        u.jump_speed =  (400 + RANDOM_P2(256));
        if (klabs(u.sz - u.hiz) < klabs(u.sz - u.loz)) {
            u.jump_speed =  -u.jump_speed;
        }

        u.WaitTics =  ((RANDOM_RANGE(3) + 1) * 120);

        (u.ActorActionFunc).animatorInvoke(SpriteNum);
    }

    private static void DoCoolgCircle(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        sp.setAng(NORM_ANGLE(sp.getAng() + u.Counter2));

        int nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        int ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

        if (!move_actor(SpriteNum, nx, ny, 0)) {
            InitActorReposition.animatorInvoke(SpriteNum);
            return;
        }

        // move in the z direction
        u.sz -= u.jump_speed * ACTORMOVETICS;

        int bound = u.hiz + u.ceiling_dist + COOLG_BOB_AMT;
        if (u.sz < bound) {
            // bumped something
            u.sz = bound;
            InitActorReposition.animatorInvoke(SpriteNum);
            return;
        }

        // time out
        if ((u.WaitTics -= ACTORMOVETICS) < 0) {
            InitActorReposition.animatorInvoke(SpriteNum);
            u.WaitTics = 0;
        }
    }

    private static void DoCoolgDeath(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int nx, ny;

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE));
        sp.setXrepeat(42);
        sp.setShade(-10);

        if (TEST(u.Flags, SPR_FALLING)) {
            DoFall(SpriteNum);
        } else {
            DoFindGroundPoint(SpriteNum);
            u.floor_dist = 0;
            DoBeginFall(SpriteNum);
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        // slide while falling
        nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

        u.moveSpriteReturn = move_sprite(SpriteNum, nx, ny, 0, u.ceiling_dist, u.floor_dist, CLIPMASK_MISSILE, ACTORMOVETICS);
        DoFindGroundPoint(SpriteNum);

        // on the ground
        if (sp.getZ() >= u.loz) {
            u.Flags &= ~(SPR_FALLING | SPR_SLIDING);
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YFLIP)); // If upside down, reset it
            NewStateGroup(SpriteNum, u.ActorActionSet.Dead);
        }
    }

    private static void DoCoolgMove(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if ((u.ShellNum -= ACTORMOVETICS) <= 0) {
            switch (u.FlagOwner) {
                case 0:
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT));
                    u.ShellNum = SEC(2);
                    break;
                case 1:
                    PlaySound(DIGI_VOID3, sp, v3df_follow);
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE));
                    u.ShellNum = SEC(1) + SEC(RANDOM_RANGE(2));
                    break;
                case 2:
                    sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT));
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE));
                    u.ShellNum = SEC(2);
                    break;
                case 3:
                    PlaySound(DIGI_VOID3, sp, v3df_follow);
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));
                    sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE));
                    u.ShellNum = SEC(2) + SEC(RANDOM_RANGE(3));
                    break;
                default:
                    u.FlagOwner = 0;
                    break;
            }
            u.FlagOwner++;
            if (u.FlagOwner > 3) {
                u.FlagOwner = 0;
            }
        }

        if (u.FlagOwner - 1 == 0) {
            sp.setXrepeat(sp.getXrepeat() - 1);
            sp.setShade(sp.getShade() + 1);
            if (sp.getXrepeat() < 4) {
                sp.setXrepeat(4);
            }
            if (sp.getShade() > 126) {
                sp.setShade(127);
                sp.setHitag(9998);
            }
        } else if (u.FlagOwner - 1 == 2) {
            sp.setHitag(0);
            sp.setXrepeat(sp.getXrepeat() + 1);
            sp.setShade(sp.getShade() - 1);
            if (sp.getXrepeat() > 42) {
                sp.setXrepeat(42);
            }
            if (sp.getShade() < -10) {
                sp.setShade(-10);
            }
        } else if (u.FlagOwner == 0) {
            sp.setXrepeat(42);
            sp.setShade(-10);
            sp.setHitag(0);
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        if (RANDOM_P2(1024) < 32 && !TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
            InitCoolgDrip(SpriteNum);
        }

        DoCoolgMatchPlayerZ(SpriteNum);

        DoActorSectorDamage(SpriteNum);

    }

    private static void DoCoolgPain(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        NullCoolg(SpriteNum);

        if ((u.WaitTics -= ACTORMOVETICS) <= 0) {
            InitActorDecide(SpriteNum);
        }
    }

    public static void CoolgSaveable() {
        SaveData(DoCoolgBirth);
        SaveData(NullCoolg);
        SaveData(InitCoolgCircle);
        SaveData(DoCoolgCircle);
        SaveData(DoCoolgDeath);
        SaveData(DoCoolgMove);
        SaveData(DoCoolgPain);
        SaveData(CoolgPersonality);
        SaveData(CoolgAttrib);
        SaveData(s_CoolgRun);
        SaveGroup(sg_CoolgRun);
        SaveData(s_CoolgStand);
        SaveGroup(sg_CoolgStand);
        SaveData(s_CoolgAttack);
        SaveGroup(sg_CoolgAttack);
        SaveData(s_CoolgPain);
        SaveGroup(sg_CoolgPain);
        SaveData(s_CoolgDie);
        SaveGroup(sg_CoolgDie);
        SaveData(s_CoolgDead);
        SaveGroup(sg_CoolgDead);
        SaveData(s_CoolgBirth);

        SaveData(CoolgActionSet);
    }
}
