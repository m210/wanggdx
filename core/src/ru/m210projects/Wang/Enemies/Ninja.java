package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Actor;
import ru.m210projects.Wang.Factory.WangNetwork.MultiGameTypes;
import ru.m210projects.Wang.JWeapon;
import ru.m210projects.Wang.Type.*;
import ru.m210projects.Wang.Weapon;

import java.util.Arrays;

import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Game.*;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.LUMINOUS;
import static ru.m210projects.Wang.JWeapon.InitBloodSpray;
import static ru.m210projects.Wang.Main.*;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.*;
import static ru.m210projects.Wang.Panel.*;
import static ru.m210projects.Wang.Player.QueueFloorBlood;
import static ru.m210projects.Wang.Player.*;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Ninja {

    public static final Animator DoActorDeathMove = new Animator((Animator.Runnable) Actor::DoActorDeathMove);
    public static final Animator DoNinjaSpecial = new Animator((Animator.Runnable) Ninja::DoNinjaSpecial);
    public static final Animator InitEnemyStar = new Animator(Ninja::InitEnemyStar);
    public static final Animator InitEnemyMirv = new Animator((Animator.Runnable) Ninja::InitEnemyMirv);
    public static final Animator InitEnemyNapalm = new Animator((Animator.Runnable) Ninja::InitEnemyNapalm);
    public static final Animator InitEnemyRocket = new Animator(Ninja::InitEnemyRocket);
    public static final Animator InitSpriteGrenade = new Animator((Animator.Runnable) Weapon::InitSpriteGrenade);
    public static final Animator InitFlashBomb = new Animator((Animator.Runnable) JWeapon::InitFlashBomb);
    public static final Animator CheckFire = new Animator((Animator.Runnable) Ninja::CheckFire);
    public static final Animator InitEnemyUzi = new Animator((Animator.Runnable) Weapon::InitEnemyUzi);
    public static final Animator DoNinjaHariKari = new Animator((Animator.Runnable) Ninja::DoNinjaHariKari);
    public static final Animator DoNinjaGrabThroat = new Animator((Animator.Runnable) Ninja::DoNinjaGrabThroat);
    public static final Animator NinjaJumpActionFunc = new Animator((Animator.Runnable) Ninja::NinjaJumpActionFunc);
    public static final Decision[] NinjaBattle = {new Decision(499, InitActorMoveCloser),

            new Decision(1024, InitActorAttack)};
    public static final Decision[] NinjaOffense = {new Decision(499, InitActorMoveCloser),

            new Decision(1024, InitActorAttack)};
    public static final Decision[] NinjaBroadcast = {new Decision(6, InitActorAmbientNoise),

            new Decision(1024, InitActorDecide)};
    public static final Decision[] NinjaSurprised = {new Decision(701, InitActorMoveCloser),

            new Decision(1024, InitActorDecide)};


    /*
     *
     * !AIC - Decision tables used in mostly ai.c DoActorActionDecide().
     *
     */
    public static final Decision[] NinjaEvasive = {new Decision(400, InitActorDuck),
            new Decision(1024, null)};
    public static final Decision[] NinjaLostTarget = {new Decision(900, InitActorFindPlayer),

            new Decision(1024, InitActorWanderAround)};
    public static final Decision[] NinjaCloseRange = {new Decision(700, InitActorAttack),

            new Decision(1024, InitActorReposition)};
    public static final Personality NinjaPersonality = new Personality(NinjaBattle, NinjaOffense, NinjaBroadcast,
            NinjaSurprised, NinjaEvasive, NinjaLostTarget, NinjaCloseRange, NinjaCloseRange);
    // Sniper Ninjas
    public static final Decision[] NinjaSniperRoam = {new Decision(1023, InitActorDuck),

            new Decision(1024, InitActorAmbientNoise)};
    public static final Decision[] NinjaSniperBattle = {new Decision(499, InitActorDuck),

            new Decision(500, InitActorAmbientNoise),
            new Decision(1024, InitActorAttack)};
    public static final Personality NinjaSniperPersonality = new Personality(NinjaSniperBattle, NinjaSniperBattle,
            NinjaSniperRoam, NinjaSniperRoam, NinjaSniperRoam, NinjaSniperRoam, NinjaSniperBattle, NinjaSniperBattle);

    /*
     *
     * !AIC - Collection of Decision tables
     *
     */
    public static final int NINJA_RATE = 15;
    public static final int NINJA_STAND_RATE = 10;
    public static final int NINJA_RISE_RATE = 10;
    //////////////////////
    //
    // NINJA RUN
    //
    //////////////////////
    public static final int NINJA_CRAWL_RATE = 14;

    /*
     *
     * !AIC - Extra attributes - speeds for running, animation tic adjustments for
     * speeeds, etc
     *
     */
    public static final int NINJA_KNEEL_CRAWL_RATE = 20;
    //////////////////////
    //
    // NINJA STAND
    //
    //////////////////////
    public static final int NINJA_DUCK_RATE = 10;
    public static final int NINJA_JUMP_RATE = 24;
    //////////////////////
    //
    // NINJA RISE
    //
    //////////////////////
    public static final int NINJA_FALL_RATE = 16;
    public static final int NINJA_SWIM_RATE = 18;
    public static final int NINJA_DIVE_RATE = 23;
    //////////////////////
    //
    // NINJA CRAWL
    //
    //////////////////////
    public static final int NINJA_CLIMB_RATE = 20;
    public static final int NINJA_FLY_RATE = 12;
    //////////////////////
    //
    // NINJA KNEEL_CRAWL
    //
    //////////////////////
    public static final int NINJA_PAIN_RATE = 15;
    public static final int NINJA_STAR_RATE = 18;
    public static final int NINJA_MIRV_RATE = 18;
    //////////////////////
    //
    // NINJA CEILING
    //
    //////////////////////
    public static final int NINJA_NAPALM_RATE = 18;
    //////////////////////
    //
    // NINJA JUMP
    //
    //////////////////////
    public static final int NINJA_ROCKET_RATE = 14;
    public static final int NINJA_FLASHBOMB_RATE = 14;

    //////////////////////
    //
    // NINJA DUCK
    //
    //////////////////////
    public static final int NINJA_UZI_RATE = 8;

    //////////////////////
    //
    // NINJA SIT
    //
    //////////////////////
    //////////////////////
    //
    // NINJA CLIMB
    //
    //////////////////////
    public static final int NINJA_HARI_KARI_WAIT_RATE = 200;
    public static final int NINJA_HARI_KARI_FALL_RATE = 16;

    //////////////////////
    //
    // NINJA FALL
    //
    //////////////////////
    public static final int NINJA_GRAB_THROAT_RATE = 32;

    //////////////////////
    //
    // NINJA SWIM
    //
    //////////////////////
    //////////////////////
    //
    // NINJA PAIN
    //
    //////////////////////
    public static final int NINJA_GRAB_THROAT_R0 = 4237;

    //////////////////////
    //
    // NINJA DIVE
    //
    //////////////////////
    public static final int NINJA_DIE_RATE = 14;
    //////////////////////
    //
    // NINJA STAR
    //
    //////////////////////
    public static final int NINJA_DIESLICED_RATE = 20;

    //////////////////////
    //
    // NINJA FLY
    //
    //////////////////////
    public static final State[] s_NinjaDieSliced = new State[]{
            new State(NINJA_SLICED, NINJA_DIESLICED_RATE * 6, null),

            new State(NINJA_SLICED + 1, NINJA_DIESLICED_RATE, null),

            new State(NINJA_SLICED + 2, NINJA_DIESLICED_RATE, null),

            new State(NINJA_SLICED + 3, NINJA_DIESLICED_RATE, null),

            new State(NINJA_SLICED + 4, NINJA_DIESLICED_RATE - 1, null),

            new State(NINJA_SLICED + 5, NINJA_DIESLICED_RATE - 2, null),

            new State(NINJA_SLICED + 6, NINJA_DIESLICED_RATE - 3, null),

            new State(NINJA_SLICED + 7, NINJA_DIESLICED_RATE - 4, null),

            new State(NINJA_SLICED + 7, SF_QUICK_CALL, DoNinjaSpecial),

            new State(NINJA_SLICED + 8, NINJA_DIESLICED_RATE - 5, null),

            new State(NINJA_SLICED + 9, SF_QUICK_CALL, QueueFloorBlood),

            new State(NINJA_SLICED + 9, NINJA_DIESLICED_RATE, DoActorDebris).setNext()};
    public static final Animator nullNinja = new Animator((Animator.Runnable) Ninja::NullNinja);
    //////////////////////
    //
    // NINJA MIRV
    //
    //////////////////////
    public static final State[][] s_NinjaUzi = {
            {new State(NINJA_FIRE_R0, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R0, SF_QUICK_CALL, CheckFire),

                    new State(NINJA_FIRE_R0 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R0 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R0, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R0, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R0 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R0 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R0, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R0, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R0 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R0 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R0, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R0, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R0 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R0 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R0, SF_QUICK_CALL, InitActorDecide).setNext()},
            {new State(NINJA_FIRE_R1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R1, SF_QUICK_CALL, CheckFire),

                    new State(NINJA_FIRE_R1 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R1 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R1 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R1 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R1 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R1 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R1 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R1 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R1, SF_QUICK_CALL, InitActorDecide).setNext()},
            {new State(NINJA_FIRE_R2, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R2, SF_QUICK_CALL, CheckFire),

                    new State(NINJA_FIRE_R2 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R2 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R2, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R2, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R2 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R2 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R2, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R2, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R2 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R2 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R2, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R2, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R2 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R2 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R2, SF_QUICK_CALL, InitActorDecide).setNext()},
            {new State(NINJA_FIRE_R3, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R3, SF_QUICK_CALL, CheckFire),

                    new State(NINJA_FIRE_R3 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R3 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R3, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R3, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R3 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R3 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R3, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R3, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R3 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R3 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R3, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R3, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R3 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R3 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R3, SF_QUICK_CALL, InitActorDecide).setNext()},
            {new State(NINJA_FIRE_R4, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R4, SF_QUICK_CALL, CheckFire),

                    new State(NINJA_FIRE_R4 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R4 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R4, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R4, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R4 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R4 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R4, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R4, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R4 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R4 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R4, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R4, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R4 + 1, NINJA_UZI_RATE, nullNinja),

                    new State(NINJA_FIRE_R4 + 1, SF_QUICK_CALL, InitEnemyUzi),

                    new State(NINJA_FIRE_R4, SF_QUICK_CALL, InitActorDecide).setNext()}};
    private static final EnemyStateGroup sg_NinjaUzi = new EnemyStateGroup(s_NinjaUzi[0], s_NinjaUzi[1], s_NinjaUzi[2], s_NinjaUzi[3], s_NinjaUzi[4]);
    public static final State[] s_NinjaHariKari = {
            new State(NINJA_HARI_KARI_R0, NINJA_HARI_KARI_FALL_RATE, nullNinja),

            new State(NINJA_HARI_KARI_R0, SF_QUICK_CALL, DoNinjaSpecial),

            new State(NINJA_HARI_KARI_R0 + 1, NINJA_HARI_KARI_WAIT_RATE, nullNinja),

            new State(NINJA_HARI_KARI_R0 + 2, SF_QUICK_CALL, DoNinjaHariKari),

            new State(NINJA_HARI_KARI_R0 + 2, NINJA_HARI_KARI_FALL_RATE, null),

            new State(NINJA_HARI_KARI_R0 + 3, NINJA_HARI_KARI_FALL_RATE, null),

            new State(NINJA_HARI_KARI_R0 + 4, NINJA_HARI_KARI_FALL_RATE, null),

            new State(NINJA_HARI_KARI_R0 + 5, NINJA_HARI_KARI_FALL_RATE, null),

            new State(NINJA_HARI_KARI_R0 + 6, NINJA_HARI_KARI_FALL_RATE, null),

            new State(NINJA_HARI_KARI_R0 + 7, NINJA_HARI_KARI_FALL_RATE, null),

            new State(NINJA_HARI_KARI_R0 + 7, NINJA_HARI_KARI_FALL_RATE, null).setNext()};
    private static final EnemyStateGroup sg_NinjaHariKari = new EnemyStateGroup(s_NinjaHariKari, s_NinjaHariKari, s_NinjaHariKari, s_NinjaHariKari, s_NinjaHariKari);
    public static final State[] s_NinjaGrabThroat = {
            new State(NINJA_GRAB_THROAT_R0, NINJA_GRAB_THROAT_RATE, nullNinja),

            new State(NINJA_GRAB_THROAT_R0, SF_QUICK_CALL, DoNinjaSpecial),

            new State(NINJA_GRAB_THROAT_R0 + 1, NINJA_GRAB_THROAT_RATE, nullNinja),

            new State(NINJA_GRAB_THROAT_R0 + 2, SF_QUICK_CALL, DoNinjaGrabThroat),

            new State(NINJA_GRAB_THROAT_R0 + 2, NINJA_GRAB_THROAT_RATE, nullNinja),

            new State(NINJA_GRAB_THROAT_R0 + 1, NINJA_GRAB_THROAT_RATE, nullNinja)};
    public static final EnemyStateGroup sg_NinjaGrabThroat = new EnemyStateGroup(s_NinjaGrabThroat, s_NinjaGrabThroat, s_NinjaGrabThroat, s_NinjaGrabThroat, s_NinjaGrabThroat);
    public static final Animator DoNinjaPain = new Animator((Animator.Runnable) Ninja::DoNinjaPain);
    public static final State[][] s_NinjaPain = {
            {new State(NINJA_PAIN_R0, NINJA_PAIN_RATE, DoNinjaPain),

                    new State(NINJA_PAIN_R0 + 1, NINJA_PAIN_RATE, DoNinjaPain).setNext()},
            {new State(NINJA_STAND_R1, NINJA_PAIN_RATE, DoNinjaPain),

                    new State(NINJA_STAND_R1, NINJA_PAIN_RATE, DoNinjaPain).setNext()},
            {new State(NINJA_STAND_R2, NINJA_PAIN_RATE, DoNinjaPain),

                    new State(NINJA_STAND_R2, NINJA_PAIN_RATE, DoNinjaPain).setNext()},
            {new State(NINJA_STAND_R3, NINJA_PAIN_RATE, DoNinjaPain),

                    new State(NINJA_STAND_R3, NINJA_PAIN_RATE, DoNinjaPain).setNext()},
            {new State(NINJA_STAND_R4, NINJA_PAIN_RATE, DoNinjaPain),

                    new State(NINJA_STAND_R4, NINJA_PAIN_RATE, DoNinjaPain).setNext()}};

    //////////////////////
    //
    // NINJA ROCKET
    //
    //////////////////////
    private static final EnemyStateGroup sg_NinjaPain = new EnemyStateGroup(s_NinjaPain[0], s_NinjaPain[1], s_NinjaPain[2], s_NinjaPain[3], s_NinjaPain[4]);
    public static final Animator DoNinjaMove = new Animator((Animator.Runnable) Ninja::DoNinjaMove);
    //////////////////////
    //
    // NINJA UZI
    //
    //////////////////////
    public static final State[][] s_NinjaRun = {
            {new State(NINJA_RUN_R0, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R0 + 1, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R0 + 2, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R0 + 3, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove)},
            {new State(NINJA_RUN_R1, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R1 + 1, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R1 + 2, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R1 + 3, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove)},
            {new State(NINJA_RUN_R2, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R2 + 1, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R2 + 2, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R2 + 3, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove)},
            {new State(NINJA_RUN_R3, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R3 + 1, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R3 + 2, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R3 + 3, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove)},
            {new State(NINJA_RUN_R4, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R4 + 1, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R4 + 2, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove),

                    new State(NINJA_RUN_R4 + 3, NINJA_RATE | SF_TIC_ADJUST, DoNinjaMove)}};

    //////////////////////
    //
    // NINJA ROCKET
    //
    //////////////////////
    public static final State[][] s_NinjaRise = {
            {new State(NINJA_KNEEL_R0, NINJA_RISE_RATE, nullNinja),

                    new State(NINJA_STAND_R0, NINJA_STAND_RATE, nullNinja),

                    new State(0, 0, null).setNext(s_NinjaRun[0][0]),

                    new State(0, 0, null).setNext(s_NinjaRun[0][0])},
            {new State(NINJA_KNEEL_R1, NINJA_RISE_RATE, nullNinja),

                    new State(NINJA_STAND_R1, NINJA_STAND_RATE, nullNinja),

                    new State(0, 0, null).setNext(s_NinjaRun[0][0]),

                    new State(0, 0, null).setNext(s_NinjaRun[0][0])},
            {new State(NINJA_KNEEL_R2, NINJA_RISE_RATE, nullNinja),

                    new State(NINJA_STAND_R2, NINJA_STAND_RATE, nullNinja),

                    new State(0, 0, null).setNext(s_NinjaRun[0][0]),

                    new State(0, 0, null).setNext(s_NinjaRun[0][0])},
            {new State(NINJA_KNEEL_R3, NINJA_RISE_RATE, nullNinja),

                    new State(NINJA_STAND_R3, NINJA_STAND_RATE, nullNinja),

                    new State(0, 0, null).setNext(s_NinjaRun[0][0]),

                    new State(0, 0, null).setNext(s_NinjaRun[0][0])},
            {new State(NINJA_KNEEL_R4, NINJA_RISE_RATE, nullNinja),

                    new State(NINJA_STAND_R4, NINJA_STAND_RATE, nullNinja),

                    new State(0, 0, null).setNext(s_NinjaRun[0][0]),

                    new State(0, 0, null).setNext(s_NinjaRun[0][0])}};
    private static final EnemyStateGroup sg_NinjaRise = new EnemyStateGroup(s_NinjaRise[0], s_NinjaRise[1], s_NinjaRise[2], s_NinjaRise[3], s_NinjaRise[4]);
    private static final EnemyStateGroup sg_NinjaRun = new EnemyStateGroup(s_NinjaRun[0], s_NinjaRun[1], s_NinjaRun[2], s_NinjaRun[3], s_NinjaRun[4]);
    public static final State[][] s_NinjaStand = {
            {new State(NINJA_STAND_R0, NINJA_STAND_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R1, NINJA_STAND_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R2, NINJA_STAND_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R3, NINJA_STAND_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R4, NINJA_STAND_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaStand = new EnemyStateGroup(s_NinjaStand[0], s_NinjaStand[1], s_NinjaStand[2], s_NinjaStand[3], s_NinjaStand[4]);
    public static final State[][] s_NinjaCrawl = {
            {new State(NINJA_CRAWL_R0, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R0 + 1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R0 + 2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R0 + 1, NINJA_CRAWL_RATE, DoNinjaMove)},
            {new State(NINJA_CRAWL_R1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R1 + 1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R1 + 2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R1 + 1, NINJA_CRAWL_RATE, DoNinjaMove)},
            {new State(NINJA_CRAWL_R2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R2 + 1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R2 + 2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R2 + 1, NINJA_CRAWL_RATE, DoNinjaMove)},
            {new State(NINJA_CRAWL_R3, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R3 + 1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R3 + 2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R3 + 1, NINJA_CRAWL_RATE, DoNinjaMove)},
            {new State(NINJA_CRAWL_R4, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R4 + 1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R4 + 2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R4 + 1, NINJA_CRAWL_RATE, DoNinjaMove)}};
    private static final EnemyStateGroup sg_NinjaCrawl = new EnemyStateGroup(s_NinjaCrawl[0], s_NinjaCrawl[1], s_NinjaCrawl[2], s_NinjaCrawl[3], s_NinjaCrawl[4]);
    //////////////////////
    //
    // NINJA HARI KARI
    //
    //////////////////////
    public static final State[][] s_NinjaKneelCrawl = {
            {new State(NINJA_KNEEL_R0, NINJA_KNEEL_CRAWL_RATE, nullNinja),

                    new State(NINJA_CRAWL_R0, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R0 + 1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R0 + 2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R0 + 1, NINJA_CRAWL_RATE, DoNinjaMove)},
            {new State(NINJA_KNEEL_R1, NINJA_KNEEL_CRAWL_RATE, nullNinja),

                    new State(NINJA_CRAWL_R1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R1 + 1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R1 + 2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R1 + 1, NINJA_CRAWL_RATE, DoNinjaMove)},
            {new State(NINJA_KNEEL_R2, NINJA_KNEEL_CRAWL_RATE, nullNinja),

                    new State(NINJA_CRAWL_R2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R2 + 1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R2 + 2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R2 + 1, NINJA_CRAWL_RATE, DoNinjaMove)},
            {new State(NINJA_KNEEL_R3, NINJA_KNEEL_CRAWL_RATE, nullNinja),

                    new State(NINJA_CRAWL_R3, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R3 + 1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R3 + 2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R3 + 1, NINJA_CRAWL_RATE, DoNinjaMove)},
            {new State(NINJA_KNEEL_R4, NINJA_KNEEL_CRAWL_RATE, nullNinja),

                    new State(NINJA_CRAWL_R4, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R4 + 1, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R4 + 2, NINJA_CRAWL_RATE, DoNinjaMove),

                    new State(NINJA_CRAWL_R4 + 1, NINJA_CRAWL_RATE, DoNinjaMove)}};
    private static final EnemyStateGroup sg_NinjaKneelCrawl = new EnemyStateGroup(s_NinjaKneelCrawl[0], s_NinjaKneelCrawl[1], s_NinjaKneelCrawl[2], s_NinjaKneelCrawl[3], s_NinjaKneelCrawl[4]);
    public static final State[][] s_NinjaDuck = {
            {new State(NINJA_KNEEL_R0, NINJA_DUCK_RATE, nullNinja),

                    new State(NINJA_CRAWL_R0, NINJA_CRAWL_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_KNEEL_R1, NINJA_DUCK_RATE, nullNinja),

                    new State(NINJA_CRAWL_R1, NINJA_CRAWL_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_KNEEL_R2, NINJA_DUCK_RATE, nullNinja),

                    new State(NINJA_CRAWL_R2, NINJA_CRAWL_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_KNEEL_R3, NINJA_DUCK_RATE, nullNinja),

                    new State(NINJA_CRAWL_R3, NINJA_CRAWL_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_KNEEL_R4, NINJA_DUCK_RATE, nullNinja),

                    new State(NINJA_CRAWL_R4, NINJA_CRAWL_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaDuck = new EnemyStateGroup(s_NinjaDuck[0], s_NinjaDuck[1], s_NinjaDuck[2], s_NinjaDuck[3], s_NinjaDuck[4]);
    public static final State[][] s_NinjaSit = {
            {new State(NINJA_KNEEL_R0, NINJA_RISE_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_KNEEL_R1, NINJA_RISE_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_KNEEL_R2, NINJA_RISE_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_KNEEL_R3, NINJA_RISE_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_KNEEL_R4, NINJA_RISE_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaSit = new EnemyStateGroup(s_NinjaSit[0], s_NinjaSit[1], s_NinjaSit[2], s_NinjaSit[3], s_NinjaSit[4]);
    public static final State[][] s_NinjaJump = {
            {new State(NINJA_JUMP_R0, NINJA_JUMP_RATE, DoNinjaMove),

                    new State(NINJA_JUMP_R0 + 1, NINJA_JUMP_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_JUMP_R1, NINJA_JUMP_RATE, DoNinjaMove),

                    new State(NINJA_JUMP_R1 + 1, NINJA_JUMP_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_JUMP_R2, NINJA_JUMP_RATE, DoNinjaMove),

                    new State(NINJA_JUMP_R2 + 1, NINJA_JUMP_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_JUMP_R3, NINJA_JUMP_RATE, DoNinjaMove),

                    new State(NINJA_JUMP_R3 + 1, NINJA_JUMP_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_JUMP_R4, NINJA_JUMP_RATE, DoNinjaMove),

                    new State(NINJA_JUMP_R4 + 1, NINJA_JUMP_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaJump = new EnemyStateGroup(s_NinjaJump[0], s_NinjaJump[1], s_NinjaJump[2], s_NinjaJump[3], s_NinjaJump[4]);
    //////////////////////
    //
    // NINJA GRAB THROAT
    //
    //////////////////////
    public static final State[][] s_NinjaFall = {
            {new State(NINJA_JUMP_R0 + 1, NINJA_FALL_RATE, DoNinjaMove),

                    new State(NINJA_JUMP_R0 + 2, NINJA_FALL_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_JUMP_R1 + 1, NINJA_FALL_RATE, DoNinjaMove),

                    new State(NINJA_JUMP_R1 + 2, NINJA_FALL_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_JUMP_R2 + 1, NINJA_FALL_RATE, DoNinjaMove),

                    new State(NINJA_JUMP_R2 + 2, NINJA_FALL_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_JUMP_R3 + 1, NINJA_FALL_RATE, DoNinjaMove),

                    new State(NINJA_JUMP_R3 + 2, NINJA_FALL_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_JUMP_R4 + 1, NINJA_FALL_RATE, DoNinjaMove),

                    new State(NINJA_JUMP_R4 + 2, NINJA_FALL_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaFall = new EnemyStateGroup(s_NinjaFall[0], s_NinjaFall[1], s_NinjaFall[2], s_NinjaFall[3], s_NinjaFall[4]);
    public static final State[][] s_NinjaSwim = {
            {new State(NINJA_SWIM_R0 + 1, NINJA_SWIM_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R0 + 2, NINJA_SWIM_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R0 + 3, NINJA_SWIM_RATE, DoNinjaMove)},
            {new State(NINJA_SWIM_R1 + 1, NINJA_SWIM_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R1 + 2, NINJA_SWIM_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R1 + 3, NINJA_SWIM_RATE, DoNinjaMove)},
            {new State(NINJA_SWIM_R2 + 1, NINJA_SWIM_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R2 + 2, NINJA_SWIM_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R2 + 3, NINJA_SWIM_RATE, DoNinjaMove)},
            {new State(NINJA_SWIM_R3 + 1, NINJA_SWIM_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R3 + 2, NINJA_SWIM_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R3 + 3, NINJA_SWIM_RATE, DoNinjaMove)},
            {new State(NINJA_SWIM_R4 + 1, NINJA_SWIM_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R4 + 2, NINJA_SWIM_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R4 + 3, NINJA_SWIM_RATE, DoNinjaMove)}};
    private static final EnemyStateGroup sg_NinjaSwim = new EnemyStateGroup(s_NinjaSwim[0], s_NinjaSwim[1], s_NinjaSwim[2], s_NinjaSwim[3], s_NinjaSwim[4]);
    public static final State[][] s_NinjaDive = {
            {new State(NINJA_SWIM_R0, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R0 + 1, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R0 + 2, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R0 + 3, NINJA_DIVE_RATE, DoNinjaMove)},
            {new State(NINJA_SWIM_R1, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R1 + 1, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R1 + 2, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R1 + 3, NINJA_DIVE_RATE, DoNinjaMove)},
            {new State(NINJA_SWIM_R2, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R2 + 1, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R2 + 2, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R2 + 3, NINJA_DIVE_RATE, DoNinjaMove),
            },
            {new State(NINJA_SWIM_R3, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R3 + 1, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R3 + 2, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R3 + 3, NINJA_DIVE_RATE, DoNinjaMove),
            },
            {new State(NINJA_SWIM_R4, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R4 + 1, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R4 + 2, NINJA_DIVE_RATE, DoNinjaMove),

                    new State(NINJA_SWIM_R4 + 3, NINJA_DIVE_RATE, DoNinjaMove),
            }};
    private static final EnemyStateGroup sg_NinjaDive = new EnemyStateGroup(s_NinjaDive[0], s_NinjaDive[1], s_NinjaDive[2], s_NinjaDive[3], s_NinjaDive[4]);
    public static final State[][] s_NinjaClimb = {
            {new State(NINJA_CLIMB_R0, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R0 + 1, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R0 + 2, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R0 + 3, NINJA_CLIMB_RATE, DoNinjaMove),
            },
            {new State(NINJA_CLIMB_R1, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R1 + 1, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R1 + 2, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R1 + 3, NINJA_CLIMB_RATE, DoNinjaMove),
            },
            {new State(NINJA_CLIMB_R4, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R4 + 1, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R4 + 2, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R4 + 3, NINJA_CLIMB_RATE, DoNinjaMove),
            },
            {new State(NINJA_CLIMB_R3, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R3 + 1, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R3 + 2, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R3 + 3, NINJA_CLIMB_RATE, DoNinjaMove),
            },
            {new State(NINJA_CLIMB_R2, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R2 + 1, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R2 + 2, NINJA_CLIMB_RATE, DoNinjaMove),

                    new State(NINJA_CLIMB_R2 + 3, NINJA_CLIMB_RATE, DoNinjaMove),
            }};
    private static final EnemyStateGroup sg_NinjaClimb = new EnemyStateGroup(s_NinjaClimb[0], s_NinjaClimb[1], s_NinjaClimb[2], s_NinjaClimb[3], s_NinjaClimb[4]);
    //////////////////////
    //
    // NINJA DIE
    //
    //////////////////////
    public static final State[][] s_NinjaFly = {{new State(NINJA_FLY_R0, NINJA_FLY_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_FLY_R1, NINJA_FLY_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_FLY_R2, NINJA_FLY_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_FLY_R3, NINJA_FLY_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_FLY_R4, NINJA_FLY_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaFly = new EnemyStateGroup(s_NinjaFly[0], s_NinjaFly[1], s_NinjaFly[2], s_NinjaFly[3], s_NinjaFly[4]);
    public static final State[][] s_NinjaStar = {
            {new State(NINJA_THROW_R0, NINJA_STAR_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R0, NINJA_STAR_RATE, nullNinja),

                    new State(NINJA_THROW_R0 + 1, SF_QUICK_CALL, InitEnemyStar),

                    new State(NINJA_THROW_R0 + 1, NINJA_STAR_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R0 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R0 + 2, NINJA_STAR_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R1, NINJA_STAR_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R1, NINJA_STAR_RATE, nullNinja),

                    new State(NINJA_THROW_R1 + 1, SF_QUICK_CALL, InitEnemyStar),

                    new State(NINJA_THROW_R1 + 1, NINJA_STAR_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R1 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R1 + 2, NINJA_STAR_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R2, NINJA_STAR_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2, NINJA_STAR_RATE, nullNinja),

                    new State(NINJA_THROW_R2 + 1, SF_QUICK_CALL, InitEnemyStar),

                    new State(NINJA_THROW_R2 + 1, NINJA_STAR_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R2 + 2, NINJA_STAR_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R2, NINJA_STAR_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2, NINJA_STAR_RATE, nullNinja),

                    new State(NINJA_THROW_R2 + 1, SF_QUICK_CALL, InitEnemyStar),

                    new State(NINJA_THROW_R2 + 1, NINJA_STAR_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R2 + 2, NINJA_STAR_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R2, NINJA_STAR_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2, NINJA_STAR_RATE, nullNinja),

                    new State(NINJA_THROW_R2 + 1, SF_QUICK_CALL, InitEnemyStar),

                    new State(NINJA_THROW_R2 + 1, NINJA_STAR_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R2 + 2, NINJA_STAR_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaStar = new EnemyStateGroup(s_NinjaStar[0], s_NinjaStar[1], s_NinjaStar[2], s_NinjaStar[3], s_NinjaStar[4]);
    public static final State[][] s_NinjaMirv = {
            {new State(NINJA_THROW_R0, NINJA_MIRV_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R0 + 1, NINJA_MIRV_RATE, nullNinja),

                    new State(NINJA_THROW_R0 + 2, SF_QUICK_CALL, InitEnemyMirv),

                    new State(NINJA_THROW_R0 + 2, NINJA_MIRV_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R0 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R0 + 2, NINJA_MIRV_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R1, NINJA_MIRV_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R1 + 1, NINJA_MIRV_RATE, nullNinja),

                    new State(NINJA_THROW_R1 + 2, SF_QUICK_CALL, InitEnemyMirv),

                    new State(NINJA_THROW_R1 + 2, NINJA_MIRV_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R1 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R1 + 2, NINJA_MIRV_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R2, NINJA_MIRV_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 1, NINJA_MIRV_RATE, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitEnemyMirv),

                    new State(NINJA_THROW_R2 + 2, NINJA_MIRV_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R2 + 2, NINJA_MIRV_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R2, NINJA_MIRV_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 1, NINJA_MIRV_RATE, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitEnemyMirv),

                    new State(NINJA_THROW_R2 + 2, NINJA_MIRV_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R2 + 2, NINJA_MIRV_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R2, NINJA_MIRV_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 1, NINJA_MIRV_RATE, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitEnemyMirv),

                    new State(NINJA_THROW_R2 + 2, NINJA_MIRV_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R2 + 2, NINJA_MIRV_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaMirv = new EnemyStateGroup(s_NinjaMirv[0], s_NinjaMirv[1], s_NinjaMirv[2], s_NinjaMirv[3], s_NinjaMirv[4]);
    public static final State[][] s_NinjaNapalm = {
            {new State(NINJA_THROW_R0, NINJA_NAPALM_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R0 + 1, NINJA_NAPALM_RATE, nullNinja),

                    new State(NINJA_THROW_R0 + 2, SF_QUICK_CALL, InitEnemyNapalm),

                    new State(NINJA_THROW_R0 + 2, NINJA_NAPALM_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R0 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R0 + 2, NINJA_NAPALM_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R1, NINJA_NAPALM_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R1 + 1, NINJA_NAPALM_RATE, nullNinja),

                    new State(NINJA_THROW_R1 + 2, SF_QUICK_CALL, InitEnemyNapalm),

                    new State(NINJA_THROW_R1 + 2, NINJA_NAPALM_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R1 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R1 + 2, NINJA_NAPALM_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R2, NINJA_NAPALM_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 1, NINJA_NAPALM_RATE, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitEnemyNapalm),

                    new State(NINJA_THROW_R2 + 2, NINJA_NAPALM_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R2 + 2, NINJA_NAPALM_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R2, NINJA_NAPALM_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 1, NINJA_NAPALM_RATE, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitEnemyNapalm),

                    new State(NINJA_THROW_R2 + 2, NINJA_NAPALM_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R2 + 2, NINJA_NAPALM_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_THROW_R2, NINJA_NAPALM_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 1, NINJA_NAPALM_RATE, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitEnemyNapalm),

                    new State(NINJA_THROW_R2 + 2, NINJA_NAPALM_RATE * 2, nullNinja),

                    new State(NINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_THROW_R2 + 2, NINJA_NAPALM_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaNapalm = new EnemyStateGroup(s_NinjaNapalm[0], s_NinjaNapalm[1], s_NinjaNapalm[2], s_NinjaNapalm[3], s_NinjaNapalm[4]);
    public static final State[][] s_NinjaRocket = {
            {new State(NINJA_STAND_R0, NINJA_ROCKET_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R0, SF_QUICK_CALL, InitEnemyRocket),

                    new State(NINJA_STAND_R0, NINJA_ROCKET_RATE, nullNinja),

                    new State(NINJA_STAND_R0, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R0, NINJA_ROCKET_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R1, NINJA_ROCKET_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R1, SF_QUICK_CALL, InitEnemyRocket),

                    new State(NINJA_STAND_R1, NINJA_ROCKET_RATE, nullNinja),

                    new State(NINJA_STAND_R1, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R1, NINJA_ROCKET_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R2, NINJA_ROCKET_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R2, SF_QUICK_CALL, InitEnemyRocket),

                    new State(NINJA_STAND_R2, NINJA_ROCKET_RATE, nullNinja),

                    new State(NINJA_STAND_R2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R2, NINJA_ROCKET_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R3, NINJA_ROCKET_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R3, SF_QUICK_CALL, InitEnemyRocket),

                    new State(NINJA_STAND_R3, NINJA_ROCKET_RATE, nullNinja),

                    new State(NINJA_STAND_R3, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R3, NINJA_ROCKET_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R4, NINJA_ROCKET_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R4, SF_QUICK_CALL, InitEnemyRocket),

                    new State(NINJA_STAND_R4, NINJA_ROCKET_RATE, nullNinja),

                    new State(NINJA_STAND_R4, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R4, NINJA_ROCKET_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaRocket = new EnemyStateGroup(s_NinjaRocket[0], s_NinjaRocket[1], s_NinjaRocket[2], s_NinjaRocket[3], s_NinjaRocket[4]);
    public static final State[][] s_NinjaGrenade = {
            {new State(NINJA_STAND_R0, NINJA_ROCKET_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R0, SF_QUICK_CALL, InitSpriteGrenade),

                    new State(NINJA_STAND_R0, NINJA_ROCKET_RATE, nullNinja),

                    new State(NINJA_STAND_R0, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R0, NINJA_ROCKET_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R1, NINJA_ROCKET_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R1, SF_QUICK_CALL, InitSpriteGrenade),

                    new State(NINJA_STAND_R1, NINJA_ROCKET_RATE, nullNinja),

                    new State(NINJA_STAND_R1, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R1, NINJA_ROCKET_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R2, NINJA_ROCKET_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R2, SF_QUICK_CALL, InitSpriteGrenade),

                    new State(NINJA_STAND_R2, NINJA_ROCKET_RATE, nullNinja),

                    new State(NINJA_STAND_R2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R2, NINJA_ROCKET_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R3, NINJA_ROCKET_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R3, SF_QUICK_CALL, InitSpriteGrenade),

                    new State(NINJA_STAND_R3, NINJA_ROCKET_RATE, nullNinja),

                    new State(NINJA_STAND_R3, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R3, NINJA_ROCKET_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R4, NINJA_ROCKET_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R4, SF_QUICK_CALL, InitSpriteGrenade),

                    new State(NINJA_STAND_R4, NINJA_ROCKET_RATE, nullNinja),

                    new State(NINJA_STAND_R4, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R4, NINJA_ROCKET_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaGrenade = new EnemyStateGroup(s_NinjaGrenade[0], s_NinjaGrenade[1], s_NinjaGrenade[2], s_NinjaGrenade[3], s_NinjaGrenade[4]);
    public static final State[][] s_NinjaFlashBomb = {
            {new State(NINJA_STAND_R0, NINJA_FLASHBOMB_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R0, SF_QUICK_CALL, InitFlashBomb),

                    new State(NINJA_STAND_R0, NINJA_FLASHBOMB_RATE, nullNinja),

                    new State(NINJA_STAND_R0, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R0, NINJA_FLASHBOMB_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R1, NINJA_FLASHBOMB_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R1, SF_QUICK_CALL, InitFlashBomb),

                    new State(NINJA_STAND_R1, NINJA_FLASHBOMB_RATE, nullNinja),

                    new State(NINJA_STAND_R1, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R1, NINJA_FLASHBOMB_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R2, NINJA_FLASHBOMB_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R2, SF_QUICK_CALL, InitFlashBomb),

                    new State(NINJA_STAND_R2, NINJA_FLASHBOMB_RATE, nullNinja),

                    new State(NINJA_STAND_R2, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R2, NINJA_FLASHBOMB_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R3, NINJA_FLASHBOMB_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R3, SF_QUICK_CALL, InitFlashBomb),

                    new State(NINJA_STAND_R3, NINJA_FLASHBOMB_RATE, nullNinja),

                    new State(NINJA_STAND_R3, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R3, NINJA_FLASHBOMB_RATE, DoNinjaMove).setNext()},
            {new State(NINJA_STAND_R4, NINJA_FLASHBOMB_RATE * 2, nullNinja),

                    new State(NINJA_STAND_R4, SF_QUICK_CALL, InitFlashBomb),

                    new State(NINJA_STAND_R4, NINJA_FLASHBOMB_RATE, nullNinja),

                    new State(NINJA_STAND_R4, SF_QUICK_CALL, InitActorDecide),

                    new State(NINJA_STAND_R4, NINJA_FLASHBOMB_RATE, DoNinjaMove).setNext()}};
    private static final EnemyStateGroup sg_NinjaFlashBomb = new EnemyStateGroup(s_NinjaFlashBomb[0], s_NinjaFlashBomb[1], s_NinjaFlashBomb[2], s_NinjaFlashBomb[3], s_NinjaFlashBomb[4]);
    //////////////////////
    //
    // NINJA NAPALM
    //
    //////////////////////
    public static final State[] s_NinjaDie = {new State(NINJA_DIE, NINJA_DIE_RATE, nullNinja),

            new State(NINJA_DIE + 1, NINJA_DIE_RATE, nullNinja),
            new State(NINJA_DIE + 2, NINJA_DIE_RATE, nullNinja),

            new State(NINJA_DIE + 3, NINJA_DIE_RATE, nullNinja),
            new State(NINJA_DIE + 4, NINJA_DIE_RATE, nullNinja),

            new State(NINJA_DIE + 5, NINJA_DIE_RATE - 4, nullNinja),

            new State(NINJA_DIE + 6, NINJA_DIE_RATE - 6, nullNinja),

            new State(NINJA_DIE + 6, SF_QUICK_CALL, DoNinjaSpecial),

            new State(NINJA_DIE + 6, NINJA_DIE_RATE - 10, nullNinja),

            new State(NINJA_DIE + 7, SF_QUICK_CALL, QueueFloorBlood),

            new State(NINJA_DIE + 7, NINJA_DIE_RATE - 12, DoActorDebris).setNext()};
    private static final EnemyStateGroup sg_NinjaDie = new EnemyStateGroup(s_NinjaDie);
    private static final Animator DoNinjaCeiling = new Animator(Actor::DoActorSectorDamage);
    public static final State[][] s_NinjaCeiling = {
            {new State(NINJA_KNEEL_R0, NINJA_RISE_RATE, DoNinjaCeiling).setNext()},
            {new State(NINJA_KNEEL_R1, NINJA_RISE_RATE, DoNinjaCeiling).setNext()},
            {new State(NINJA_KNEEL_R2, NINJA_RISE_RATE, DoNinjaCeiling).setNext()},
            {new State(NINJA_KNEEL_R3, NINJA_RISE_RATE, DoNinjaCeiling).setNext()},
            {new State(NINJA_KNEEL_R4, NINJA_RISE_RATE, DoNinjaCeiling).setNext()}};
    private static final EnemyStateGroup sg_NinjaCeiling = new EnemyStateGroup(s_NinjaCeiling[0], s_NinjaCeiling[1], s_NinjaCeiling[2], s_NinjaCeiling[3], s_NinjaCeiling[4]);
    private static final EnemyStateGroup sg_NinjaDieSliced = new EnemyStateGroup(s_NinjaDieSliced);
    public static final State[] s_NinjaDead = {new State(NINJA_DIE + 5, NINJA_DIE_RATE, DoActorDebris),

            new State(NINJA_DIE + 6, SF_QUICK_CALL, DoNinjaSpecial),

            new State(NINJA_DIE + 6, NINJA_DIE_RATE, DoActorDebris),

            new State(NINJA_DIE + 7, SF_QUICK_CALL, QueueFloorBlood),

            new State(NINJA_DIE + 7, NINJA_DIE_RATE, DoActorDebris).setNext()};
    private static final EnemyStateGroup sg_NinjaDead = new EnemyStateGroup(s_NinjaDead);
    public static final State[] s_NinjaDeathJump = {new State(NINJA_DIE, NINJA_DIE_RATE, DoActorDeathMove),

            new State(NINJA_DIE + 1, NINJA_DIE_RATE, DoActorDeathMove),

            new State(NINJA_DIE + 2, NINJA_DIE_RATE, DoActorDeathMove).setNext()};
    private static final EnemyStateGroup sg_NinjaDeathJump = new EnemyStateGroup(s_NinjaDeathJump);
    //////////////////////
    //
    // NINJA FLASHBOMB
    //
    //////////////////////
    public static final State[] s_NinjaDeathFall = {new State(NINJA_DIE + 3, NINJA_DIE_RATE, DoActorDeathMove),

            new State(NINJA_DIE + 4, NINJA_DIE_RATE, DoActorDeathMove).setNext()};
    private static final EnemyStateGroup sg_NinjaDeathFall = new EnemyStateGroup(s_NinjaDeathFall);
    public static final Actor_Action_Set PlayerNinjaActionSet = new Actor_Action_Set(PlayerStateGroup.sg_PlayerNinjaStand,
            PlayerStateGroup.sg_PlayerNinjaRun, PlayerStateGroup.sg_PlayerNinjaJump,
            PlayerStateGroup.sg_PlayerNinjaFall, PlayerStateGroup.sg_PlayerNinjaCrawl,
            PlayerStateGroup.sg_PlayerNinjaSwim, sg_NinjaFly, sg_NinjaRise,
            sg_NinjaSit, null, PlayerStateGroup.sg_PlayerNinjaClimb, sg_NinjaPain,
            sg_NinjaDie, sg_NinjaHariKari, sg_NinjaDead,
            sg_NinjaDeathJump, sg_NinjaDeathFall,
            new StateGroup[]{sg_NinjaStar, sg_NinjaUzi}, new short[]{1000, 1024},
            new StateGroup[]{sg_NinjaStar, sg_NinjaUzi}, new short[]{800, 1024},
            null, sg_NinjaDuck, PlayerStateGroup.sg_PlayerNinjaSwim);
    public static final Actor_Action_Set NinjaSniperActionSet = new Actor_Action_Set(sg_NinjaDuck,
            sg_NinjaCrawl, sg_NinjaJump, sg_NinjaFall,
            sg_NinjaKneelCrawl, sg_NinjaSwim, sg_NinjaFly,
            sg_NinjaUzi, sg_NinjaDuck, null, sg_NinjaClimb,
            sg_NinjaPain, sg_NinjaDie, sg_NinjaHariKari,
            sg_NinjaDead, sg_NinjaDeathJump, sg_NinjaDeathFall,
            new StateGroup[]{sg_NinjaUzi}, new short[]{1024},
            new StateGroup[]{sg_NinjaUzi}, new short[]{1024}, null, sg_NinjaDuck,
            sg_NinjaDive);
    public static final Actor_Action_Set NinjaActionSet = new Actor_Action_Set(sg_NinjaStand,
            sg_NinjaRun, sg_NinjaJump, sg_NinjaFall,
            sg_NinjaKneelCrawl, sg_NinjaSwim, sg_NinjaFly,
            sg_NinjaRise, sg_NinjaSit, null, sg_NinjaClimb,
            sg_NinjaPain, sg_NinjaDie, sg_NinjaHariKari,
            sg_NinjaDead, sg_NinjaDeathJump, sg_NinjaDeathFall,
            new StateGroup[]{sg_NinjaUzi, sg_NinjaStar}, new short[]{1000, 1024},
            new StateGroup[]{sg_NinjaUzi, sg_NinjaStar}, new short[]{800, 1024},
            null, sg_NinjaDuck, sg_NinjaDive);
    public static final Actor_Action_Set NinjaRedActionSet = new Actor_Action_Set(sg_NinjaStand,
            sg_NinjaRun, sg_NinjaJump, sg_NinjaFall,
            sg_NinjaKneelCrawl, sg_NinjaSwim, sg_NinjaFly,
            sg_NinjaRise, sg_NinjaSit, null, sg_NinjaClimb,
            sg_NinjaPain, sg_NinjaDie, sg_NinjaHariKari,
            sg_NinjaDead, sg_NinjaDeathJump, sg_NinjaDeathFall,
            new StateGroup[]{sg_NinjaUzi, sg_NinjaUzi}, new short[]{812, 1024},
            new StateGroup[]{sg_NinjaUzi, sg_NinjaRocket}, new short[]{812, 1024},
            null, sg_NinjaDuck, sg_NinjaDive);
    public static final Actor_Action_Set NinjaSeekerActionSet = new Actor_Action_Set(sg_NinjaStand,
            sg_NinjaRun, sg_NinjaJump, sg_NinjaFall,
            sg_NinjaKneelCrawl, sg_NinjaSwim, sg_NinjaFly,
            sg_NinjaRise, sg_NinjaSit, null, sg_NinjaClimb,
            sg_NinjaPain, sg_NinjaDie, sg_NinjaHariKari,
            sg_NinjaDead, sg_NinjaDeathJump, sg_NinjaDeathFall,
            new StateGroup[]{sg_NinjaUzi, sg_NinjaStar}, new short[]{812, 1024},
            new StateGroup[]{sg_NinjaUzi, sg_NinjaRocket}, new short[]{812, 1024},
            null, sg_NinjaDuck, sg_NinjaDive);
    public static final Actor_Action_Set NinjaGrenadeActionSet = new Actor_Action_Set(sg_NinjaStand,
            sg_NinjaRun, sg_NinjaJump, sg_NinjaFall,
            sg_NinjaKneelCrawl, sg_NinjaSwim, sg_NinjaFly,
            sg_NinjaRise, sg_NinjaSit, null, sg_NinjaClimb,
            sg_NinjaPain, sg_NinjaDie, sg_NinjaHariKari,
            sg_NinjaDead, sg_NinjaDeathJump, sg_NinjaDeathFall,
            new StateGroup[]{sg_NinjaUzi, sg_NinjaUzi}, new short[]{812, 1024},
            new StateGroup[]{sg_NinjaUzi, sg_NinjaGrenade},
            new short[]{812, 1024}, null, sg_NinjaDuck, sg_NinjaDive);
    public static final Actor_Action_Set NinjaGreenActionSet = new Actor_Action_Set(sg_NinjaStand,
            sg_NinjaRun, sg_NinjaJump, sg_NinjaFall,
            sg_NinjaKneelCrawl, sg_NinjaSwim, sg_NinjaFly,
            sg_NinjaRise, sg_NinjaSit, null, sg_NinjaClimb,
            sg_NinjaPain, sg_NinjaDie, sg_NinjaHariKari,
            sg_NinjaDead, sg_NinjaDeathJump, sg_NinjaDeathFall,
            new StateGroup[]{sg_NinjaUzi, sg_NinjaFlashBomb},
            new short[]{912, 1024},
            new StateGroup[]{sg_NinjaFlashBomb, sg_NinjaUzi,
                    sg_NinjaMirv, sg_NinjaNapalm},
            new short[]{150, 500, 712, 1024}, null, sg_NinjaDuck, sg_NinjaDive);
    private static final ATTRIBUTE NinjaAttrib = new ATTRIBUTE(new short[]{110, 130, 150, 180}, // Speeds
            new short[]{4, 0, 0, -2}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_NINJAAMBIENT, DIGI_NINJAALERT, DIGI_STAR, DIGI_NINJAPAIN, DIGI_NINJASCREAM, 0, 0, 0, 0,
                    0});
    private static final ATTRIBUTE InvisibleNinjaAttrib = new ATTRIBUTE(new short[]{220, 240, 300, 360}, // Speeds
            new short[]{4, 0, 0, -2}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_NINJAAMBIENT, DIGI_NINJAALERT, DIGI_STAR, DIGI_NINJAPAIN, DIGI_NINJASCREAM, 0, 0, 0, 0,
                    0});

    public static void DoNinjaPain(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        NullNinja(SpriteNum);

        if (TEST(u.Flags2, SPR2_DYING)) {
            NewStateGroup(SpriteNum, sg_NinjaGrabThroat);
            return;
        }

        if ((u.WaitTics -= ACTORMOVETICS) <= 0) {
            InitActorDecide(SpriteNum);
        }
    }

    public static void InitNinjaStates() {
        for (State[] states : s_NinjaKneelCrawl) {
            states[4].setNext(states[1]);
        }

        for (EnemyStateGroup sg : new EnemyStateGroup[]{
                sg_NinjaRun,
                sg_NinjaStand,
                sg_NinjaRise,
                sg_NinjaCrawl,
                sg_NinjaKneelCrawl,
                sg_NinjaDuck,
                sg_NinjaSit,
                sg_NinjaCeiling,
                sg_NinjaJump,
                sg_NinjaFall,
                sg_NinjaSwim,
                sg_NinjaDive,
                sg_NinjaClimb,
                sg_NinjaFly,
                sg_NinjaPain,
                sg_NinjaStar,
                sg_NinjaMirv,
                sg_NinjaNapalm,
                sg_NinjaRocket,
                sg_NinjaGrenade,
                sg_NinjaFlashBomb,
                sg_NinjaUzi,
                sg_NinjaHariKari,
                sg_NinjaGrabThroat,
                sg_NinjaDie,
                sg_NinjaDieSliced,
                sg_NinjaDead,
                sg_NinjaDeathJump,
                sg_NinjaDeathFall
        }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }

    /*
     *
     * !AIC - Every actor has a setup where they are initialized
     *
     */

    public static void SetupNinja(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        USER u;
        int pic = sp.getPicnum();

        if (TEST(sp.getCstat(),
                CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, NINJA_RUN_R0, s_NinjaRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_NINJA;
        }

        u.StateEnd = s_NinjaDie[0];
        u.setRot(sg_NinjaRun);
        sp.setXrepeat(46);
        sp.setYrepeat(46);

        if (sp.getPal() == PALETTE_PLAYER5) {
            u.Attrib = InvisibleNinjaAttrib;
            EnemyDefaults(SpriteNum, NinjaGreenActionSet, NinjaPersonality);
            if (!TEST(sp.getCstat(),
                    CSTAT_SPRITE_RESTORE)) {
                u.Health = HEALTH_RED_NINJA;
            }
            sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT));
            sp.setShade(127);
            sp.setPal(u.spal = PALETTE_PLAYER5);
            sp.setHitag(9998);
            if (pic == NINJA_CRAWL_R0) {
                if (TEST(sp.getCstat(),
                        CSTAT_SPRITE_YFLIP)) {
                    u.Attrib = NinjaAttrib;
                    u.ActorActionSet = NinjaActionSet;
                    u.Personality = NinjaPersonality;
                    ChangeState(SpriteNum, s_NinjaCeiling[0][0]);
                } else {
                    u.Attrib = NinjaAttrib;
                    u.ActorActionSet = NinjaSniperActionSet;
                    u.Personality = NinjaSniperPersonality;
                    ChangeState(SpriteNum, s_NinjaDuck[0][0]);
                }
            }
        } else if (sp.getPal() == PALETTE_PLAYER3) {
            u.Attrib = NinjaAttrib;
            EnemyDefaults(SpriteNum, NinjaRedActionSet, NinjaPersonality);
            if (!TEST(sp.getCstat(),
                    CSTAT_SPRITE_RESTORE)) {
                u.Health = HEALTH_RED_NINJA;
            }
            sp.setPal(u.spal = PALETTE_PLAYER3);
            if (pic == NINJA_CRAWL_R0) {
                if (TEST(sp.getCstat(),
                        CSTAT_SPRITE_YFLIP)) {
                    u.Attrib = NinjaAttrib;
                    u.ActorActionSet = NinjaActionSet;
                    u.Personality = NinjaPersonality;
                    ChangeState(SpriteNum, s_NinjaCeiling[0][0]);
                } else {
                    u.Attrib = NinjaAttrib;
                    u.ActorActionSet = NinjaSniperActionSet;
                    u.Personality = NinjaSniperPersonality;
                    ChangeState(SpriteNum, s_NinjaDuck[0][0]);
                }
            }
        } else if (sp.getPal() == PAL_XLAT_LT_TAN) {
            u.Attrib = NinjaAttrib;
            EnemyDefaults(SpriteNum, NinjaSeekerActionSet, NinjaPersonality);
            if (!TEST(sp.getCstat(),
                    CSTAT_SPRITE_RESTORE)) {
                u.Health = HEALTH_RED_NINJA;
            }
            sp.setPal(u.spal = PAL_XLAT_LT_TAN);
            u.Attrib = NinjaAttrib;
        } else if (sp.getPal() == PAL_XLAT_LT_GREY) {
            u.Attrib = NinjaAttrib;
            EnemyDefaults(SpriteNum, NinjaGrenadeActionSet, NinjaPersonality);
            if (!TEST(sp.getCstat(),
                    CSTAT_SPRITE_RESTORE)) {
                u.Health = HEALTH_RED_NINJA;
            }
            sp.setPal(u.spal = PAL_XLAT_LT_GREY);
            u.Attrib = NinjaAttrib;
        } else {
            u.Attrib = NinjaAttrib;
            sp.setPal(u.spal = PALETTE_PLAYER0);
            EnemyDefaults(SpriteNum, NinjaActionSet, NinjaPersonality);
            if (pic == NINJA_CRAWL_R0) {
                u.Attrib = NinjaAttrib;
                u.ActorActionSet = NinjaSniperActionSet;
                u.Personality = NinjaSniperPersonality;
                ChangeState(SpriteNum, s_NinjaDuck[0][0]);
            }
        }

        ChangeState(SpriteNum, s_NinjaRun[0][0]);
        DoActorSetSpeed(SpriteNum, NORM_SPEED);

        u.Radius = 280;
        u.Flags |= (SPR_XFLIP_TOGGLE);
    }

    public static void DoNinjaHariKari(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        UpdateSinglePlayKills(SpriteNum, null);
        change_sprite_stat(SpriteNum, STAT_DEAD_ACTOR);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        u.Flags |= (SPR_DEAD);
        u.Flags &= ~(SPR_FALLING | SPR_JUMPING);
        u.floor_dist = Z(40);
        u.RotNum = 0;
        u.ActorActionFunc = null;

        sp.setExtra(sp.getExtra() | (SPRX_BREAKABLE));
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BREAKABLE));

        PlaySound(DIGI_NINJAUZIATTACK, sp, v3df_follow);

        SpawnBlood(SpriteNum, SpriteNum, -1, -1, -1, -1);

        int cnt = (RANDOM_RANGE(4) + 1);
        for (int i = 0; i <= cnt; i++) {
            InitBloodSpray(SpriteNum, true, -2);
        }
    }

    public static void DoNinjaGrabThroat(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if ((u.WaitTics -= ACTORMOVETICS) <= 0) {
            UpdateSinglePlayKills(SpriteNum, null);
            u.Flags2 &= ~(SPR2_DYING);
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YFLIP));
            change_sprite_stat(SpriteNum, STAT_DEAD_ACTOR);
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
            u.Flags |= (SPR_DEAD);
            u.Flags &= ~(SPR_FALLING | SPR_JUMPING);
            u.floor_dist = Z(40);
            u.RotNum = 0;
            u.ActorActionFunc = null;

            sp.setExtra(sp.getExtra() | (SPRX_BREAKABLE));
            sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BREAKABLE));

            ChangeState(SpriteNum, u.StateEnd);
            sp.setXvel(0);
            PlaySound(DIGI_NINJASCREAM, sp, v3df_follow);
        }
    }

    /*
     *
     * !AIC - Most actors have one of these and the all look similar
     *
     */

    public static void DoNinjaMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags2, SPR2_DYING)) {
            NewStateGroup(SpriteNum, sg_NinjaGrabThroat);
            return;
        }

        // jumping and falling
        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING) && !TEST(u.Flags, SPR_CLIMBING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoActorJump(SpriteNum);
            } else if (TEST(u.Flags, SPR_FALLING)) {
                DoActorFall(SpriteNum);
            }
        }

        // sliding
        if (TEST(u.Flags, SPR_SLIDING) && !TEST(u.Flags, SPR_CLIMBING)) {
            DoActorSlide(SpriteNum);
        }

        // !AIC - do track or call current action function - such as DoActorMoveCloser()
        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            u.ActorActionFunc.animatorInvoke(SpriteNum);
        }

        // stay on floor unless doing certain things
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING | SPR_CLIMBING)) {
            KeepActorOnFloor(SpriteNum);
        }

        // take damage from environment
        DoActorSectorDamage(SpriteNum);
    }

    public static void NinjaJumpActionFunc(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // Move while jumping
        int nx = sp.getXvel() * EngineUtils.cos(sp.getAng()) >> 14;
        int ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

        // if cannot move the sprite
        if (!move_actor(SpriteNum, nx, ny, 0)) {
            return;
        }

        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            InitActorDecide(SpriteNum);
        }
    }

    public static void NullNinja(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (u.WaitTics > 0) {
            u.WaitTics -= ACTORMOVETICS;
        }

        if (TEST(u.Flags, SPR_SLIDING) && !TEST(u.Flags, SPR_CLIMBING) && !TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            DoActorSlide(SpriteNum);
        }

        if (!TEST(u.Flags, SPR_CLIMBING) && !TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            KeepActorOnFloor(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
    }

    /*
     *
     * !AIC - Short version of DoNinjaMove without the movement code. For times when
     * the actor is doing something but not moving.
     *
     */

    public static void DoNinjaSpecial(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (u.spal == PALETTE_PLAYER5) {
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));
            sp.setHitag(0);
            sp.setShade(-10);
        }
    }

    public static void CheckFire(int SpriteNum) {
        if (!CanSeePlayer(SpriteNum)) {
            InitActorDuck.animatorInvoke(SpriteNum);
        }
    }

    public static void InitAllPlayerSprites(boolean NewGame) {
        for (int i = connecthead; i != -1; i = connectpoint2[i]) {
            InitPlayerSprite(i, NewGame);
        }
    }

    //
    // !AIC - Stuff from here down is really Player related. Should be moved but it
    // was
    // too convienent to put it here.
    //

    public static void PlayerLevelReset(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (sp == null || u == null) {
            return;
        }

        if (gNet.MultiGameType == MultiGameTypes.MULTI_GAME_COMMBAT) {
            PlayerDeathReset(pp);
            return;
        }

        if (TEST(pp.Flags, PF_DIVING)) {
            DoPlayerStopDiveNoWarp(pp);
        }

        COVER_SetReverb(0); // Turn off any echoing that may have been going before
        pp.Reverb = 0;
        pp.SecretsFound = 0;
        pp.WpnFirstType = WPN_SWORD;
        pp.Kills = 0;
        pp.Killer = -1;
        pp.NightVision = false;
        pp.StartColor = 0;
        pp.FadeAmt = 0;
        pp.DeathType = 0;
        pp.lookang = 0;
        PlayerUpdatePanelInfo(pp);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_YCENTER));
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));
        pp.Flags &= ~(PF_WEAPON_DOWN | PF_WEAPON_RETRACT);
        pp.Flags &= ~(PF_DEAD);
        pp.last_used_weapon = 0;

        pp.sop_control = -1;
        pp.sop_riding = -1;
        pp.sop_remote = -1;
        pp.sop = -1;
        DoPlayerResetMovement(pp);
        DamageData[u.WeaponNum].init.invoke(pp);
    }

    public static void PlayerDeathReset(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(pp.Flags, PF_DIVING)) {
            DoPlayerStopDiveNoWarp(pp);
        }

        COVER_SetReverb(0); // Turn off any echoing that may have been going before
        pp.Reverb = 0;
        // second weapon - whatever it is
        u.WeaponNum = WPN_SWORD;
        pp.WpnFirstType = (byte) u.WeaponNum;
        pp.WpnRocketType = 0;
        pp.WpnRocketHeat = 0; // 5 to 0 range
        pp.WpnRocketNuke = 0; // 1, you have it, or you don't
        pp.WpnFlameType = 0; // Guardian weapons fire
        pp.WpnUziType = 2;
        pp.WpnShotgunType = 0; // Shotgun has normal or fully automatic fire
        pp.WpnShotgunAuto = 0; // 50-0 automatic shotgun rounds
        pp.WpnShotgunLastShell = 0; // Number of last shell fired
        pp.Bloody = false;
        pp.TestNukeInit = false;
        pp.InitingNuke = false;
        pp.nukevochandle = null;
        pp.NukeInitialized = false;
        pp.BunnyMode = false;

        Arrays.fill(pp.WpnAmmo, (short) 0);
        Arrays.fill(pp.InventoryTics, (short) 0);
        Arrays.fill(pp.InventoryPercent, (short) 0);
        Arrays.fill(pp.InventoryAmount, (short) 0);
        Arrays.fill(pp.InventoryActive, false);
        pp.WpnAmmo[WPN_STAR] = 30;
        pp.WpnAmmo[WPN_SWORD] = pp.WpnAmmo[WPN_FIST] = 30;
        pp.WpnFlags = 0;
        pp.WpnGotOnceFlags = 0;
        pp.WpnFlags |= (BIT(WPN_SWORD));
        pp.WpnFlags |= (BIT(WPN_FIST) | BIT(u.WeaponNum));
        pp.WpnFlags |= (BIT(WPN_STAR) | BIT(u.WeaponNum));
        pp.Flags &= ~(PF_PICKED_UP_AN_UZI);
        pp.Flags &= ~(PF_TWO_UZI);

        u.Health = 100;
        pp.MaxHealth = 100;

        puser[pp.pnum].Health = u.Health;
        pp.Armor = 0;
        PlayerUpdateArmor(pp, 0);
        pp.Killer = -1;
        pp.NightVision = false;
        pp.StartColor = 0;
        pp.FadeAmt = 0;
        pp.DeathType = 0;
        pp.lookang = 0;
        PlayerUpdatePanelInfo(pp);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));
        pp.Flags &= ~(PF_WEAPON_DOWN | PF_WEAPON_RETRACT);
        pp.Flags &= ~(PF_DEAD);
        pp.last_used_weapon = 0;
        pp.NumFootPrints = 0;

        pp.sop_control = -1;
        pp.sop_riding = -1;
        pp.sop_remote = -1;
        pp.sop = -1;
        DoPlayerResetMovement(pp);
        DamageData[u.WeaponNum].init.invoke(pp);
    }

    public static void PlayerPanelSetup() {
        // For every player setup the panel weapon stuff
        for (int pnum = connecthead; pnum != -1; pnum = connectpoint2[pnum]) {
            PlayerStr pp = Player[pnum];

            USER u = getUser(pp.PlayerSprite);
            if (u != null) {
                PlayerUpdateWeapon(pp, u.WeaponNum);
            }
        }
    }

    public static void PlayerGameReset(PlayerStr pp) {
        Sprite sp = pp.getSprite();
        USER u = getUser(pp.PlayerSprite);
        if (sp == null || u == null) {
            return;
        }

        COVER_SetReverb(0); // Turn off any echoing that may have been going before
        pp.Reverb = 0;
        u.WeaponNum = WPN_SWORD;
        pp.WpnFirstType = (byte) u.WeaponNum;
        pp.WpnRocketType = 0;
        pp.WpnRocketHeat = 0; // 5 to 0 range
        pp.WpnRocketNuke = 0; // 1, you have it, or you don't
        pp.WpnFlameType = 0; // Guardian weapons fire
        pp.WpnUziType = 2;
        pp.WpnShotgunType = 0; // Shotgun has normal or fully automatic fire
        pp.WpnShotgunAuto = 0; // 50-0 automatic shotgun rounds
        pp.WpnShotgunLastShell = 0; // Number of last shell fired
        pp.Bloody = false;
        pp.TestNukeInit = false;
        pp.InitingNuke = false;
        pp.nukevochandle = null;
        pp.NukeInitialized = false;
        pp.BunnyMode = false;
        pp.SecretsFound = 0;

        pp.WpnAmmo[WPN_STAR] = 30;
        pp.WpnAmmo[WPN_SWORD] = pp.WpnAmmo[WPN_FIST] = 30;
        pp.WpnFlags = 0;
        pp.WpnGotOnceFlags = 0;
        pp.WpnFlags |= (BIT(WPN_SWORD));
        pp.WpnFlags |= (BIT(WPN_FIST) | BIT(u.WeaponNum));
        pp.WpnFlags |= (BIT(WPN_STAR) | BIT(u.WeaponNum));
        pp.Flags &= ~(PF_PICKED_UP_AN_UZI);
        pp.Flags &= ~(PF_TWO_UZI);
        pp.MaxHealth = 100;
        PlayerUpdateHealth(pp, 500);
        pp.Armor = 0;
        PlayerUpdateArmor(pp, 0);
        pp.Killer = -1;

        if (pp == Player[screenpeek]) {
            byte[] palette = engine.getPaletteManager().getBasePalette();
            engine.setbrightness(cfg.getPaletteGamma(), palette);
        }
        pp.NightVision = false;
        pp.StartColor = 0;
        pp.FadeAmt = 0;
        pp.DeathType = 0;

        PlayerUpdatePanelInfo(pp);
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));

        pp.sop_control = -1;
        pp.sop_riding = -1;
        pp.sop_remote = -1;
        pp.sop = -1;
        DoPlayerResetMovement(pp);
        DamageData[u.WeaponNum].init.invoke(pp);
    }

    public static void InitEnemyMirv(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        PlaySound(DIGI_MIRVFIRE, sp, v3df_none);

        int w = SpawnSprite(STAT_MISSILE, MIRV_METEOR, s_Mirv[0], sp.getSectnum(),
                sp.getX(),
                sp.getY(),
                SPRITEp_TOS(sp) + DIV4(SPRITEp_SIZE_Z(sp)),
                sp.getAng(),
                MIRV_VELOCITY);

        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        Set3DSoundOwner(w, PlaySound(DIGI_MIRVWIZ, wp, v3df_follow));

        SetOwner(SpriteNum, w);
        wp.setShade(-40);
        wp.setXrepeat(72);
        wp.setYrepeat(72);
        wp.setClipdist(32 >> 2);

        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        wu.floor_dist = Z(16);
        wu.ceiling_dist = Z(16);
        wu.Dist = 200;

        wu.xchange = MOVEx(wp.getXvel(),
                wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(),
                wp.getAng());
        wu.zchange = wp.getZvel();

        MissileSetPos(w, DoMirv, 600);

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp != null) {
            // find the distance to the target (player)
            int dist = Distance(wp.getX(),
                    wp.getY(),
                    tsp.getX(),
                    tsp.getY());

            if (dist != 0) {
                wu.zchange = (short) ((wp.getXvel() * (SPRITEp_UPPER(boardService.getSprite(u.tgt_sp)) - wp.getZ())) / dist);
                wp.setZvel(wu.zchange);
            }
        }
    }

    public static void InitPlayerSprite(int pnum, boolean NewGame) {
        PlayerStr pp = Player[pnum];

        COVER_SetReverb(0); // Turn off any echoing that may have been going before
        pp.Reverb = 0;
        int sp_num = pp.PlayerSprite = SpawnSprite(STAT_PLAYER0 + pnum, NINJA_RUN_R0, null, pp.cursectnum, pp.posx,
                pp.posy, pp.posz, pp.getAnglei(),
                0);

        Sprite sp = boardService.getSprite(sp_num);
        USER u = getUser(sp_num);
        if (sp == null || u == null) {
            return;
        }

        pp.pnum = pnum;

        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        sp.setExtra(sp.getExtra() | (SPRX_PLAYER_OR_ENEMY));
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));

        // Grouping items that need to be reset after a LoadLevel
        ChangeState(sp_num, s_NinjaRun[0][0]);
        u.setRot(sg_NinjaRun);
        u.ActorActionSet = PlayerNinjaActionSet;

        u.RotNum = 5;

        u.Radius = 400;
        u.PlayerP = pnum;
        u.Flags |= (SPR_XFLIP_TOGGLE);

        sp.setPicnum(u.State.Pic);
        sp.setShade(-60); // was 15
        sp.setClipdist((char) (256L >> 2));

        sp.setXrepeat(PLAYER_NINJA_XREPEAT);
        sp.setYrepeat(PLAYER_NINJA_YREPEAT);
        sp.setPal((PALETTE_PLAYER0 + pp.pnum));
        u.spal = (byte) sp.getPal();

        NewStateGroup(sp_num, u.ActorActionSet.Run);

        pp.PlayerUnderSprite = -1;

        DoPlayerZrange(pp);

        if (NewGame) {
            PlayerGameReset(pp);
        } else {
            // save stuff from last level
            u.WeaponNum = puser[pnum].WeaponNum;
            u.LastWeaponNum = puser[pnum].LastWeaponNum;
            u.Health = puser[pnum].Health;
            PlayerLevelReset(pp);
        }

        Arrays.fill(pp.InventoryTics, (short) 0);
        if (pnum == screenpeek) {
            byte[] palette = engine.getPaletteManager().getBasePalette();
            engine.setbrightness(cfg.getPaletteGamma(), palette);
        }

        pp.NightVision = false;
        pp.StartColor = 0;
        pp.FadeAmt = 0;
        pp.DeathType = 0;
        PlayerUpdatePanelInfo(pp);
    }

    public static void SpawnPlayerUnderSprite(int pnum) {
        PlayerStr pp = Player[pnum];
        USER pu = getUser(pp.PlayerSprite);
        Sprite psp = pp.getSprite();

        int sp_num = pp.PlayerUnderSprite = SpawnSprite(STAT_PLAYER_UNDER0 + pnum, NINJA_RUN_R0, null,
                pp.cursectnum, pp.posx, pp.posy, pp.posz, pp.getAnglei(),
                0);

        Sprite sp = boardService.getSprite(sp_num);
        USER u = getUser(sp_num);
        if (sp == null || u == null || pu == null || psp == null) {
            return;
        }

        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        sp.setExtra(sp.getExtra() | (SPRX_PLAYER_OR_ENEMY));

        u.setRot(sg_NinjaRun);
        u.RotNum = pu.RotNum;
        NewStateGroup(sp_num, pu.getRot());

        u.Radius = pu.Radius;
        u.PlayerP = pnum;
        u.Health = pp.MaxHealth;
        u.Flags |= (SPR_XFLIP_TOGGLE);

        u.ActorActionSet = pu.ActorActionSet;

        sp.setPicnum(psp.getPicnum());
        sp.setClipdist(psp.getClipdist());
        sp.setXrepeat(psp.getXrepeat());
        sp.setYrepeat(psp.getYrepeat());
    }

    public static boolean InitEnemyStar(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);

        if (sp == null || u == null) {
            return false;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return false;
        }

        // get angle to player and also face player when attacking
        int nang = NORM_ANGLE(EngineUtils.getAngle(tsp.getX() - sp.getX(),
                tsp.getY() - sp.getY()));
        sp.setAng(nang);

        int nx = sp.getX();
        int ny = sp.getY();
        int nz = SPRITEp_MID(sp);

        // Spawn a shot
        int w = SpawnSprite(STAT_MISSILE, STAR1, s_Star[0], sp.getSectnum(),
                nx, ny, nz, tsp.getAng(),
                NINJA_STAR_VELOCITY);

        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return false;
        }

        // wp.owner = SpriteNum;
        SetOwner(SpriteNum, w);
        wp.setYrepeat(16);
        wp.setXrepeat(16);
        wp.setShade(-25);
        wp.setZvel(0);
        wp.setAng(nang);
        wp.setClipdist(64 >> 2);

        wu.xchange = MOVEx(wp.getXvel(),
                wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(),
                wp.getAng());
        wu.zchange = wp.getZvel();

        MissileSetPos(w, DoStar, 400);

        // find the distance to the target (player)
        int dist = Distance(wp.getX(),
                wp.getY(),
                tsp.getX(),
                tsp.getY());

        if (dist != 0) {
            wu.zchange = (short) ((wp.getXvel() * (SPRITEp_UPPER(tsp) - wp.getZ())) / dist);
            wp.setZvel(wu.zchange);
        }

        //
        // Star Power Up Code
        //

        PlaySound(DIGI_STAR, sp, v3df_none);

        return (w) != 0;
    }

    public static void InitEnemyNapalm(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (u == null || sp == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }

        PlaySound(DIGI_NAPFIRE, sp, v3df_none);

        for (int i = 0; i < mp.length; i++) {
            int w = SpawnSprite(STAT_MISSILE, FIREBALL1, s_Napalm[0], sp.getSectnum(),
                    sp.getX(),
                    sp.getY(),
                    SPRITEp_TOS(sp) + DIV4(SPRITEp_SIZE_Z(sp)),
                    sp.getAng(),
                    NAPALM_VELOCITY);

            Sprite wp = boardService.getSprite(w);
            USER wu = getUser(w);
            if (wp == null || wu == null) {
                return;
            }

            wp.setHitag(LUMINOUS); // Always full brightness
            if (i == 0) // Only attach sound to first projectile
            {
                Set3DSoundOwner(w, PlaySound(DIGI_NAPWIZ, wp, v3df_follow));
            }

            if (u.ID == ZOMBIE_RUN_R0) {
                SetOwner(sp.getOwner(),
                        w);
            } else {
                SetOwner(SpriteNum, w);
            }

            wp.setShade(-40);
            wp.setXrepeat(32);
            wp.setYrepeat(32);
            wp.setClipdist(0);
            wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_TRANSLUCENT | CSTAT_SPRITE_YCENTER));
            wp.setCstat(wp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
            wu.Flags2 |= (SPR2_BLUR_TAPER_FAST);

            wu.floor_dist = Z(1);
            wu.ceiling_dist = Z(1);
            wu.Dist = 200;

            int oclipdist = sp.getClipdist();
            sp.setClipdist(1);

            if (mp[i].dist_over != 0) {
                wp.setAng(NORM_ANGLE(wp.getAng() + mp[i].ang));
                HelpMissileLateral(w, mp[i].dist_over);
                wp.setAng(NORM_ANGLE(wp.getAng() - mp[i].ang));
            }

            // find the distance to the target (player)
            int dist = Distance(wp.getX(),
                    wp.getY(),
                    tsp.getX(),
                    tsp.getY());

            if (dist != 0) {
                wp.setZvel(((wp.getXvel() * (SPRITEp_UPPER(tsp) - wp.getZ())) / dist));
            }

            wu.xchange = MOVEx(wp.getXvel(),
                    wp.getAng());
            wu.ychange = MOVEy(wp.getXvel(),
                    wp.getAng());
            wu.zchange = wp.getZvel();

            MissileSetPos(w, DoNapalm, mp[i].dist_out);

            sp.setClipdist(oclipdist);

            u.Counter = 0;
        }
    }

    public static boolean InitEnemyRocket(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return false;
        }

        PlaySound(DIGI_NINJARIOTATTACK, sp, v3df_none);

        // get angle to player and also face player when attacking
        int nang = EngineUtils.getAngle(tsp.getX() - sp.getX(),
                tsp.getY() - sp.getY());
        sp.setAng(nang);

        int nx = sp.getX();
        int ny = sp.getY();
        int nz = sp.getZ() - DIV2(SPRITEp_SIZE_Z(sp)) - Z(8);

        // Spawn a shot
        int w = SpawnSprite(STAT_MISSILE, BOLT_THINMAN_R2, s_Rocket[0][0], sp.getSectnum(),
                nx, ny, nz - Z(8),
                tsp.getAng(),
                NINJA_BOLT_VELOCITY);

        Sprite wp = boardService.getSprite(w);
        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return false;
        }

        // Set default palette
        wp.setPal(wu.spal = 17); // White

        if (u.ID == ZOMBIE_RUN_R0) {
            SetOwner(sp.getOwner(),
                    w);
        } else {
            SetOwner(SpriteNum, w);
        }
        wp.setYrepeat(28);
        wp.setXrepeat(28);
        wp.setShade(-15);
        wp.setZvel(0);
        wp.setAng(nang);
        wp.setClipdist(64 >> 2);

        wu.RotNum = 5;
        NewStateGroup(w, WeaponStateGroup.sg_Rocket);
        wu.Radius = 200;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));

        wu.xchange = MOVEx(wp.getXvel(),
                wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(),
                wp.getAng());
        wu.zchange = wp.getZvel();

        if (u.spal == PAL_XLAT_LT_TAN) {
            wu.Flags |= (SPR_FIND_PLAYER);
            wp.setPal(wu.spal = 20); // Yellow
        }

        MissileSetPos(w, DoBoltThinMan, 400);

        // find the distance to the target (player)
        int dist = Distance(wp.getX(),
                wp.getY(),
                tsp.getX(),
                tsp.getY());

        if (dist != 0) {
            wu.zchange = (short) ((wp.getXvel() * (SPRITEp_UPPER(tsp) - wp.getZ())) / dist);
            wp.setZvel(wu.zchange);
        }

        return (w) != 0;
    }

    public static void NinjaSaveable() {
        SaveData(nullNinja);
        SaveData(InitEnemyStar);
        SaveData(InitEnemyMirv);
        SaveData(InitEnemyNapalm);
        SaveData(InitEnemyRocket);
        SaveData(InitSpriteGrenade);
        SaveData(InitFlashBomb);
        SaveData(InitEnemyUzi);
        SaveData(DoActorDeathMove);

        SaveData(DoNinjaHariKari);
        SaveData(DoNinjaGrabThroat);
        SaveData(DoNinjaMove);
        SaveData(NinjaJumpActionFunc);
        SaveData(DoNinjaPain);
        SaveData(DoNinjaSpecial);
        SaveData(CheckFire);
        SaveData(DoNinjaCeiling);

        SaveData(NinjaPersonality);
        SaveData(NinjaSniperPersonality);

        SaveData(NinjaAttrib);
        SaveData(InvisibleNinjaAttrib);

        SaveData(s_NinjaRun);
        SaveGroup(sg_NinjaRun);
        SaveData(s_NinjaStand);
        SaveGroup(sg_NinjaStand);
        SaveData(s_NinjaRise);
        SaveGroup(sg_NinjaRise);
        SaveData(s_NinjaCrawl);
        SaveGroup(sg_NinjaCrawl);
        SaveData(s_NinjaKneelCrawl);
        SaveGroup(sg_NinjaKneelCrawl);
        SaveData(s_NinjaDuck);
        SaveGroup(sg_NinjaDuck);
        SaveData(s_NinjaSit);
        SaveGroup(sg_NinjaSit);
        SaveData(s_NinjaCeiling);
        SaveGroup(sg_NinjaCeiling);
        SaveData(s_NinjaJump);
        SaveGroup(sg_NinjaJump);
        SaveData(s_NinjaFall);
        SaveGroup(sg_NinjaFall);
        SaveData(s_NinjaSwim);
        SaveGroup(sg_NinjaSwim);
        SaveData(s_NinjaDive);
        SaveGroup(sg_NinjaDive);
        SaveData(s_NinjaClimb);
        SaveGroup(sg_NinjaClimb);
        SaveData(s_NinjaFly);
        SaveGroup(sg_NinjaFly);
        SaveData(s_NinjaPain);
        SaveGroup(sg_NinjaPain);
        SaveData(s_NinjaStar);
        SaveGroup(sg_NinjaStar);
        SaveData(s_NinjaMirv);
        SaveGroup(sg_NinjaMirv);
        SaveData(s_NinjaNapalm);
        SaveGroup(sg_NinjaNapalm);
        SaveData(s_NinjaRocket);
        SaveGroup(sg_NinjaRocket);
        SaveData(s_NinjaGrenade);
        SaveGroup(sg_NinjaGrenade);
        SaveData(s_NinjaFlashBomb);
        SaveGroup(sg_NinjaFlashBomb);
        SaveData(s_NinjaUzi);
        SaveGroup(sg_NinjaUzi);
        SaveData(s_NinjaHariKari);
        SaveGroup(sg_NinjaHariKari);
        SaveData(s_NinjaGrabThroat);
        SaveGroup(sg_NinjaGrabThroat);
        SaveData(s_NinjaDie);
        SaveData(s_NinjaDieSliced);
        SaveData(s_NinjaDead);
        SaveData(s_NinjaDeathJump);
        SaveData(s_NinjaDeathFall);
        SaveGroup(sg_NinjaDie);
        SaveGroup(sg_NinjaDieSliced);
        SaveGroup(sg_NinjaDead);
        SaveGroup(sg_NinjaDeathJump);
        SaveGroup(sg_NinjaDeathFall);

        SaveData(NinjaSniperActionSet);
        SaveData(NinjaActionSet);
        SaveData(NinjaRedActionSet);
        SaveData(NinjaSeekerActionSet);
        SaveData(NinjaGrenadeActionSet);
        SaveData(NinjaGreenActionSet);
        SaveData(PlayerNinjaActionSet);
    }
}
