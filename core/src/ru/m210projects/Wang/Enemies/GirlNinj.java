package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Actor;
import ru.m210projects.Wang.Type.*;
import ru.m210projects.Wang.Weapon;

import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Game.Distance;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.PALETTE_PLAYER5;
import static ru.m210projects.Wang.Player.QueueFloorBlood;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class GirlNinj {

    public static final int GIRLNINJA_RATE = 18;
    public static final int GIRLNINJA_STAND_RATE = 10;
    public static final int GIRLNINJA_RISE_RATE = 10;
    public static final int GIRLNINJA_DUCK_RATE = 10;
    public static final int GIRLNINJA_CRAWL_RATE = 14;
    public static final int GIRLNINJA_JUMP_RATE = 24;
    public static final int GIRLNINJA_FALL_RATE = 16;
    public static final int GIRLNINJA_PAIN_RATE = 15;
    public static final int GIRLNINJA_STICKY_RATE = 32;
    /*
     *
     * !AIC - Collection of decision tables
     *
     */
    public static final int GIRLNINJA_CROSSBOW_RATE = 14;
    public static final int GIRLNINJA_DIE_RATE = 30;
    private static final Animator DoGirlNinjaMove = new Animator((Animator.Runnable) GirlNinj::DoGirlNinjaMove);
    private static final Animator NullGirlNinja = new Animator((Animator.Runnable) GirlNinj::NullGirlNinja);
    private static final Animator DoGirlNinjaPain = new Animator((Animator.Runnable) GirlNinj::DoGirlNinjaPain);
    private static final Animator InitEnemyMine = new Animator((Animator.Runnable) GirlNinj::InitEnemyMine);
    private static final Animator InitEnemyCrossbow = new Animator(GirlNinj::InitEnemyCrossbow);
    private static final Animator DoGirlNinjaSpecial = new Animator((Animator.Runnable) GirlNinj::DoGirlNinjaSpecial);
    private static final Animator DoActorDeathMove = new Animator((Animator.Runnable) Actor::DoActorDeathMove);
    //////////////////////
    //
    // GIRLNINJA RUN
    //
    //////////////////////
    private static final Decision[] GirlNinjaBattle = {new Decision(499, InitActorMoveCloser),

            // new Decision(509, InitActorAmbientNoise),

            // new Decision(710, InitActorRunAway),

            new Decision(1024, InitActorAttack)};
    private static final Decision[] GirlNinjaOffense = {new Decision(499, InitActorMoveCloser),

            // new Decision(509, InitActorAmbientNoise),

            new Decision(1024, InitActorAttack)};
    private static final Decision[] GirlNinjaBroadcast = {
            // new Decision(1, InitActorAlertNoise),

            new Decision(6, InitActorAmbientNoise),
            new Decision(1024, InitActorDecide)};

    //////////////////////
    //
    // GIRLNINJA STAND
    //
    //////////////////////
    private static final Decision[] GirlNinjaSurprised = {new Decision(701, InitActorMoveCloser),
            new Decision(1024, InitActorDecide)};
    private static final Decision[] GirlNinjaEvasive = {new Decision(400, InitActorDuck),
            // 100
//	    new Decision(300,   InitActorEvade),

//	    new Decision(800,   InitActorRunAway),

            new Decision(1024, null)};

    //////////////////////
    //
    // GIRLNINJA RISE
    //
    //////////////////////
    private static final Decision[] GirlNinjaLostTarget = {new Decision(900, InitActorFindPlayer),
            new Decision(1024, InitActorWanderAround)};
    private static final Decision[] GirlNinjaCloseRange = {new Decision(900, InitActorAttack),
            new Decision(1024, InitActorReposition)};
    private static final Personality GirlNinjaPersonality = new Personality(GirlNinjaBattle, GirlNinjaOffense, GirlNinjaBroadcast, GirlNinjaSurprised, GirlNinjaEvasive, GirlNinjaLostTarget, GirlNinjaCloseRange, GirlNinjaCloseRange);

    //////////////////////
    //
    // GIRLNINJA DUCK
    //
    //////////////////////
    private static final ATTRIBUTE GirlNinjaAttrib = new ATTRIBUTE(new short[]{120, 140, 160, 190}, // Speeds
            new short[]{4, 0, 0, -2}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_GIRLNINJAALERT, DIGI_GIRLNINJAALERT, DIGI_STAR, DIGI_GIRLNINJAALERTT, DIGI_GIRLNINJASCREAM, 0, 0, 0, 0, 0});

    private static final State[][] s_GirlNinjaRun = {

            {new State(GIRLNINJA_RUN_R0, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
                    // s_GirlNinjaRun[0][1]},
                    new State(GIRLNINJA_RUN_R0 + 1, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
                    // s_GirlNinjaRun[0][2]},
                    new State(GIRLNINJA_RUN_R0 + 2, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
                    // s_GirlNinjaRun[0][3]},
                    new State(GIRLNINJA_RUN_R0 + 3, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
// s_GirlNinjaRun[0][0]},
            }, {new State(GIRLNINJA_RUN_R1, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[1][1]},
            new State(GIRLNINJA_RUN_R1 + 1, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[1][2]},
            new State(GIRLNINJA_RUN_R1 + 2, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[1][3]},
            new State(GIRLNINJA_RUN_R1 + 3, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
// s_GirlNinjaRun[1][0]},
    }, {new State(GIRLNINJA_RUN_R2, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[2][1]},
            new State(GIRLNINJA_RUN_R2 + 1, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[2][2]},
            new State(GIRLNINJA_RUN_R2 + 2, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[2][3]},
            new State(GIRLNINJA_RUN_R2 + 3, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
// s_GirlNinjaRun[2][0]},
    }, {new State(GIRLNINJA_RUN_R3, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[3][1]},
            new State(GIRLNINJA_RUN_R3 + 1, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[3][2]},
            new State(GIRLNINJA_RUN_R3 + 2, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[3][3]},
            new State(GIRLNINJA_RUN_R3 + 3, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
// s_GirlNinjaRun[3][0]},
    }, {new State(GIRLNINJA_RUN_R4, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[4][1]},
            new State(GIRLNINJA_RUN_R4 + 1, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[4][2]},
            new State(GIRLNINJA_RUN_R4 + 2, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
            // s_GirlNinjaRun[4][3]},
            new State(GIRLNINJA_RUN_R4 + 3, GIRLNINJA_RATE | SF_TIC_ADJUST, DoGirlNinjaMove),
// s_GirlNinjaRun[4][0]},
    },

    };
    private static final EnemyStateGroup sg_GirlNinjaRun = new EnemyStateGroup(s_GirlNinjaRun[0], s_GirlNinjaRun[1], s_GirlNinjaRun[2], s_GirlNinjaRun[3], s_GirlNinjaRun[4]);

    //////////////////////
    //
    // GIRLNINJA JUMP
    //
    //////////////////////
    //////////////////////
    //
    // GIRLNINJA SIT
    //
    //////////////////////
    private static final State[][] s_GirlNinjaStand = {{new State(GIRLNINJA_STAND_R0, GIRLNINJA_STAND_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaStand[0][0]},
    }, {new State(GIRLNINJA_STAND_R1, GIRLNINJA_STAND_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaStand[1][0]},
    }, {new State(GIRLNINJA_STAND_R2, GIRLNINJA_STAND_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaStand[2][0]},
    }, {new State(GIRLNINJA_STAND_R3, GIRLNINJA_STAND_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaStand[3][0]},
    }, {new State(GIRLNINJA_STAND_R4, GIRLNINJA_STAND_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaStand[4][0]},
    }};
    private static final EnemyStateGroup sg_GirlNinjaStand = new EnemyStateGroup(s_GirlNinjaStand[0], s_GirlNinjaStand[1], s_GirlNinjaStand[2], s_GirlNinjaStand[3], s_GirlNinjaStand[4]);
    private static final State[][] s_GirlNinjaRise = {{new State(GIRLNINJA_KNEEL_R0, GIRLNINJA_RISE_RATE, NullGirlNinja),
            // s_GirlNinjaRise[0][1]},
            new State(GIRLNINJA_STAND_R0, GIRLNINJA_STAND_RATE, NullGirlNinja),
            // s_GirlNinjaRise[0][2]},
            new State(0, 0, null),
    }, {new State(GIRLNINJA_KNEEL_R1, GIRLNINJA_RISE_RATE, NullGirlNinja),
            // s_GirlNinjaRise[1][1]},
            new State(GIRLNINJA_STAND_R1, GIRLNINJA_STAND_RATE, NullGirlNinja),
            // s_GirlNinjaRise[1][2]},
            new State(0, 0, null),
    }, {new State(GIRLNINJA_KNEEL_R2, GIRLNINJA_RISE_RATE, NullGirlNinja),
            // s_GirlNinjaRise[2][1]},
            new State(GIRLNINJA_STAND_R2, GIRLNINJA_STAND_RATE, NullGirlNinja),
            // s_GirlNinjaRise[2][2]},
            new State(0, 0, null),
    }, {new State(GIRLNINJA_KNEEL_R3, GIRLNINJA_RISE_RATE, NullGirlNinja),
            // s_GirlNinjaRise[3][1]},
            new State(GIRLNINJA_STAND_R3, GIRLNINJA_STAND_RATE, NullGirlNinja),
            // s_GirlNinjaRise[3][2]},
            new State(0, 0, null),
    }, {new State(GIRLNINJA_KNEEL_R4, GIRLNINJA_RISE_RATE, NullGirlNinja),
            // s_GirlNinjaRise[4][1]},
            new State(GIRLNINJA_STAND_R4, GIRLNINJA_STAND_RATE, NullGirlNinja),
            // s_GirlNinjaRise[4][2]},
            new State(0, 0, null),
    }};
    private static final EnemyStateGroup sg_GirlNinjaRise = new EnemyStateGroup(s_GirlNinjaRise[0], s_GirlNinjaRise[1], s_GirlNinjaRise[2], s_GirlNinjaRise[3], s_GirlNinjaRise[4]);
    //////////////////////
    //
    // GIRLNINJA FALL
    //
    //////////////////////
    private static final State[][] s_GirlNinjaDuck = {{new State(GIRLNINJA_KNEEL_R0, GIRLNINJA_DUCK_RATE, NullGirlNinja),
            // s_GirlNinjaDuck[0][1]},
            new State(GIRLNINJA_CRAWL_R0, GIRLNINJA_CRAWL_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaDuck[0][1]},
    }, {new State(GIRLNINJA_KNEEL_R1, GIRLNINJA_DUCK_RATE, NullGirlNinja),
            // s_GirlNinjaDuck[1][1]},
            new State(GIRLNINJA_CRAWL_R1, GIRLNINJA_CRAWL_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaDuck[1][1]},
    }, {new State(GIRLNINJA_KNEEL_R2, GIRLNINJA_DUCK_RATE, NullGirlNinja),
            // s_GirlNinjaDuck[2][1]},
            new State(GIRLNINJA_CRAWL_R2, GIRLNINJA_CRAWL_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaDuck[2][1]},
    }, {new State(GIRLNINJA_KNEEL_R3, GIRLNINJA_DUCK_RATE, NullGirlNinja),
            // s_GirlNinjaDuck[3][1]},
            new State(GIRLNINJA_CRAWL_R3, GIRLNINJA_CRAWL_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaDuck[3][1]},
    }, {new State(GIRLNINJA_KNEEL_R4, GIRLNINJA_DUCK_RATE, NullGirlNinja),
            // s_GirlNinjaDuck[4][1]},
            new State(GIRLNINJA_CRAWL_R4, GIRLNINJA_CRAWL_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaDuck[4][1]},
    }};
    private static final EnemyStateGroup sg_GirlNinjaDuck = new EnemyStateGroup(s_GirlNinjaDuck[0], s_GirlNinjaDuck[1], s_GirlNinjaDuck[2], s_GirlNinjaDuck[3], s_GirlNinjaDuck[4]);
    private static final State[][] s_GirlNinjaSit = {{new State(GIRLNINJA_KNEEL_R0, GIRLNINJA_RISE_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaSit[0][0]},
    }, {new State(GIRLNINJA_KNEEL_R1, GIRLNINJA_RISE_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaSit[1][0]},
    }, {new State(GIRLNINJA_KNEEL_R2, GIRLNINJA_RISE_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaSit[2][0]},
    }, {new State(GIRLNINJA_KNEEL_R3, GIRLNINJA_RISE_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaSit[3][0]},
    }, {new State(GIRLNINJA_KNEEL_R4, GIRLNINJA_RISE_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaSit[4][0]},
    }};

    //////////////////////
    //
    // GIRLNINJA CROSSBOW
    //
    //////////////////////
    private static final EnemyStateGroup sg_GirlNinjaSit = new EnemyStateGroup(s_GirlNinjaSit[0], s_GirlNinjaSit[1], s_GirlNinjaSit[2], s_GirlNinjaSit[3], s_GirlNinjaSit[4]);
    //////////////////////
    //
    // GIRLNINJA PAIN
    //
    //////////////////////
    private static final State[][] s_GirlNinjaJump = {{new State(GIRLNINJA_JUMP_R0, GIRLNINJA_JUMP_RATE, DoGirlNinjaMove),
            // s_GirlNinjaJump[0][1]},
            new State(GIRLNINJA_JUMP_R0 + 1, GIRLNINJA_JUMP_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaJump[0][1]},
    }, {new State(GIRLNINJA_JUMP_R1, GIRLNINJA_JUMP_RATE, DoGirlNinjaMove),
            // s_GirlNinjaJump[1][1]},
            new State(GIRLNINJA_JUMP_R1 + 1, GIRLNINJA_JUMP_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaJump[1][1]},
    }, {new State(GIRLNINJA_JUMP_R2, GIRLNINJA_JUMP_RATE, DoGirlNinjaMove),
            // s_GirlNinjaJump[2][1]},
            new State(GIRLNINJA_JUMP_R2 + 1, GIRLNINJA_JUMP_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaJump[2][1]},
    }, {new State(GIRLNINJA_JUMP_R3, GIRLNINJA_JUMP_RATE, DoGirlNinjaMove),
            // s_GirlNinjaJump[3][1]},
            new State(GIRLNINJA_JUMP_R3 + 1, GIRLNINJA_JUMP_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaJump[3][1]},
    }, {new State(GIRLNINJA_JUMP_R4, GIRLNINJA_JUMP_RATE, DoGirlNinjaMove),
            // s_GirlNinjaJump[4][1]},
            new State(GIRLNINJA_JUMP_R4 + 1, GIRLNINJA_JUMP_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaJump[4][1]},
    }};
    private static final EnemyStateGroup sg_GirlNinjaJump = new EnemyStateGroup(s_GirlNinjaJump[0], s_GirlNinjaJump[1], s_GirlNinjaJump[2], s_GirlNinjaJump[3], s_GirlNinjaJump[4]);
    private static final State[][] s_GirlNinjaFall = {{new State(GIRLNINJA_JUMP_R0 + 1, GIRLNINJA_FALL_RATE, DoGirlNinjaMove),
            // s_GirlNinjaFall[0][1]},
            new State(GIRLNINJA_JUMP_R0 + 2, GIRLNINJA_FALL_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaFall[0][1]},
    }, {new State(GIRLNINJA_JUMP_R1 + 1, GIRLNINJA_FALL_RATE, DoGirlNinjaMove),
            // s_GirlNinjaFall[1][1]},
            new State(GIRLNINJA_JUMP_R1 + 2, GIRLNINJA_FALL_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaFall[1][1]},
    }, {new State(GIRLNINJA_JUMP_R2 + 1, GIRLNINJA_FALL_RATE, DoGirlNinjaMove),
            // s_GirlNinjaFall[2][1]},
            new State(GIRLNINJA_JUMP_R2 + 2, GIRLNINJA_FALL_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaFall[2][1]},
    }, {new State(GIRLNINJA_JUMP_R3 + 1, GIRLNINJA_FALL_RATE, DoGirlNinjaMove),
            // s_GirlNinjaFall[3][1]},
            new State(GIRLNINJA_JUMP_R3 + 2, GIRLNINJA_FALL_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaFall[3][1]},
    }, {new State(GIRLNINJA_JUMP_R4 + 1, GIRLNINJA_FALL_RATE, DoGirlNinjaMove),
            // s_GirlNinjaFall[4][1]},
            new State(GIRLNINJA_JUMP_R4 + 2, GIRLNINJA_FALL_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaFall[4][1]},
    }};
    private static final EnemyStateGroup sg_GirlNinjaFall = new EnemyStateGroup(s_GirlNinjaFall[0], s_GirlNinjaFall[1], s_GirlNinjaFall[2], s_GirlNinjaFall[3], s_GirlNinjaFall[4]);
    //////////////////////
    //
    // GIRLNINJA STICKY
    //
    //////////////////////
    private static final State[][] s_GirlNinjaPain = {{new State(GIRLNINJA_PAIN_R0, GIRLNINJA_PAIN_RATE, DoGirlNinjaPain).setNext(),
// s_GirlNinjaPain[0][0]},
    }, {new State(GIRLNINJA_PAIN_R1, GIRLNINJA_PAIN_RATE, DoGirlNinjaPain).setNext(),
// s_GirlNinjaPain[1][0]},
    }, {new State(GIRLNINJA_PAIN_R2, GIRLNINJA_PAIN_RATE, DoGirlNinjaPain).setNext(),
// s_GirlNinjaPain[2][0]},
    }, {new State(GIRLNINJA_PAIN_R3, GIRLNINJA_PAIN_RATE, DoGirlNinjaPain).setNext(),
// s_GirlNinjaPain[3][0]},
    }, {new State(GIRLNINJA_PAIN_R4, GIRLNINJA_PAIN_RATE, DoGirlNinjaPain).setNext(),
// s_GirlNinjaPain[4][0]},
    }};
    private static final EnemyStateGroup sg_GirlNinjaPain = new EnemyStateGroup(s_GirlNinjaPain[0], s_GirlNinjaPain[1], s_GirlNinjaPain[2], s_GirlNinjaPain[3], s_GirlNinjaPain[4]);
    private static final State[][] s_GirlNinjaSticky = {{new State(GIRLNINJA_THROW_R0, GIRLNINJA_STICKY_RATE * 2, NullGirlNinja),
            // s_GirlNinjaSticky[0][1]},
            new State(GIRLNINJA_THROW_R0, GIRLNINJA_STICKY_RATE, NullGirlNinja),
            // s_GirlNinjaSticky[0][2]},
            new State(GIRLNINJA_THROW_R0 + 1, SF_QUICK_CALL, InitEnemyMine),
            // s_GirlNinjaSticky[0][3]},
            new State(GIRLNINJA_THROW_R0 + 1, GIRLNINJA_STICKY_RATE * 2, NullGirlNinja),
            // s_GirlNinjaSticky[0][4]},
            new State(GIRLNINJA_THROW_R0 + 2, SF_QUICK_CALL, InitActorDecide),
            // s_GirlNinjaSticky[0][5]},
            new State(GIRLNINJA_THROW_R0 + 2, GIRLNINJA_STICKY_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaSticky[0][5]},
    }, {new State(GIRLNINJA_THROW_R1, GIRLNINJA_STICKY_RATE * 2, NullGirlNinja),
            // s_GirlNinjaSticky[1][1]},
            new State(GIRLNINJA_THROW_R1, GIRLNINJA_STICKY_RATE, NullGirlNinja),
            // s_GirlNinjaSticky[1][2]},
            new State(GIRLNINJA_THROW_R1 + 1, SF_QUICK_CALL, InitEnemyMine),
            // s_GirlNinjaSticky[1][3]},
            new State(GIRLNINJA_THROW_R1 + 1, GIRLNINJA_STICKY_RATE * 2, NullGirlNinja),
            // s_GirlNinjaSticky[1][4]},
            new State(GIRLNINJA_THROW_R1 + 2, SF_QUICK_CALL, InitActorDecide),
            // s_GirlNinjaSticky[1][5]},
            new State(GIRLNINJA_THROW_R1 + 2, GIRLNINJA_STICKY_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaSticky[1][5]},
    }, {new State(GIRLNINJA_THROW_R2, GIRLNINJA_STICKY_RATE * 2, NullGirlNinja),
            // s_GirlNinjaSticky[2][1]},
            new State(GIRLNINJA_THROW_R2, GIRLNINJA_STICKY_RATE, NullGirlNinja),
            // s_GirlNinjaSticky[2][2]},
            new State(GIRLNINJA_THROW_R2 + 1, SF_QUICK_CALL, InitEnemyMine),
            // s_GirlNinjaSticky[2][3]},
            new State(GIRLNINJA_THROW_R2 + 1, GIRLNINJA_STICKY_RATE * 2, NullGirlNinja),
            // s_GirlNinjaSticky[2][4]},
            new State(GIRLNINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),
            // s_GirlNinjaSticky[2][5]},
            new State(GIRLNINJA_THROW_R2 + 2, GIRLNINJA_STICKY_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaSticky[2][5]},
    }, {new State(GIRLNINJA_THROW_R2, GIRLNINJA_STICKY_RATE * 2, NullGirlNinja),
            // s_GirlNinjaSticky[3][1]},
            new State(GIRLNINJA_THROW_R2, GIRLNINJA_STICKY_RATE, NullGirlNinja),
            // s_GirlNinjaSticky[3][2]},
            new State(GIRLNINJA_THROW_R2 + 1, SF_QUICK_CALL, InitEnemyMine),
            // s_GirlNinjaSticky[3][3]},
            new State(GIRLNINJA_THROW_R2 + 1, GIRLNINJA_STICKY_RATE * 2, NullGirlNinja),
            // s_GirlNinjaSticky[3][4]},
            new State(GIRLNINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),
            // s_GirlNinjaSticky[3][5]},
            new State(GIRLNINJA_THROW_R2 + 2, GIRLNINJA_STICKY_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaSticky[3][5]},
    }, {new State(GIRLNINJA_THROW_R2, GIRLNINJA_STICKY_RATE * 2, NullGirlNinja),
            // s_GirlNinjaSticky[4][1]},
            new State(GIRLNINJA_THROW_R2, GIRLNINJA_STICKY_RATE, NullGirlNinja),
            // s_GirlNinjaSticky[4][2]},
            new State(GIRLNINJA_THROW_R2 + 1, SF_QUICK_CALL, InitEnemyMine),
            // s_GirlNinjaSticky[4][3]},
            new State(GIRLNINJA_THROW_R2 + 1, GIRLNINJA_STICKY_RATE * 2, NullGirlNinja),
            // s_GirlNinjaSticky[4][4]},
            new State(GIRLNINJA_THROW_R2 + 2, SF_QUICK_CALL, InitActorDecide),
            // s_GirlNinjaSticky[4][5]},
            new State(GIRLNINJA_THROW_R2 + 2, GIRLNINJA_STICKY_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaSticky[4][5]},
    }};
    private static final EnemyStateGroup sg_GirlNinjaSticky = new EnemyStateGroup(s_GirlNinjaSticky[0], s_GirlNinjaSticky[1], s_GirlNinjaSticky[2], s_GirlNinjaSticky[3], s_GirlNinjaSticky[4]);
    private static final State[][] s_GirlNinjaCrossbow = {{new State(GIRLNINJA_FIRE_R0, GIRLNINJA_CROSSBOW_RATE * 2, NullGirlNinja),
            // s_GirlNinjaCrossbow[0][1]},
            new State(GIRLNINJA_FIRE_R0 + 1, SF_QUICK_CALL, InitEnemyCrossbow),
            // s_GirlNinjaCrossbow[0][2]},
            new State(GIRLNINJA_FIRE_R0 + 1, GIRLNINJA_CROSSBOW_RATE, NullGirlNinja),
            // s_GirlNinjaCrossbow[0][3]},
            new State(GIRLNINJA_FIRE_R0 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_GirlNinjaCrossbow[0][4]},
            new State(GIRLNINJA_FIRE_R0 + 1, GIRLNINJA_CROSSBOW_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaCrossbow[0][4]},
    }, {new State(GIRLNINJA_FIRE_R1, GIRLNINJA_CROSSBOW_RATE * 2, NullGirlNinja),
            // s_GirlNinjaCrossbow[1][1]},
            new State(GIRLNINJA_FIRE_R1 + 1, SF_QUICK_CALL, InitEnemyCrossbow),
            // s_GirlNinjaCrossbow[1][2]},
            new State(GIRLNINJA_FIRE_R1 + 1, GIRLNINJA_CROSSBOW_RATE, NullGirlNinja),
            // s_GirlNinjaCrossbow[1][3]},
            new State(GIRLNINJA_FIRE_R1 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_GirlNinjaCrossbow[1][4]},
            new State(GIRLNINJA_FIRE_R1 + 1, GIRLNINJA_CROSSBOW_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaCrossbow[1][4]},
    }, {new State(GIRLNINJA_FIRE_R2, GIRLNINJA_CROSSBOW_RATE * 2, NullGirlNinja),
            // s_GirlNinjaCrossbow[2][1]},
            new State(GIRLNINJA_FIRE_R2 + 1, SF_QUICK_CALL, InitEnemyCrossbow),
            // s_GirlNinjaCrossbow[2][2]},
            new State(GIRLNINJA_FIRE_R2 + 1, GIRLNINJA_CROSSBOW_RATE, NullGirlNinja),
            // s_GirlNinjaCrossbow[2][3]},
            new State(GIRLNINJA_FIRE_R2 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_GirlNinjaCrossbow[2][4]},
            new State(GIRLNINJA_FIRE_R2 + 1, GIRLNINJA_CROSSBOW_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaCrossbow[2][4]},
    }, {new State(GIRLNINJA_FIRE_R3, GIRLNINJA_CROSSBOW_RATE * 2, NullGirlNinja),
            // s_GirlNinjaCrossbow[3][1]},
            new State(GIRLNINJA_FIRE_R3 + 1, SF_QUICK_CALL, InitEnemyCrossbow),
            // s_GirlNinjaCrossbow[3][2]},
            new State(GIRLNINJA_FIRE_R3 + 1, GIRLNINJA_CROSSBOW_RATE, NullGirlNinja),
            // s_GirlNinjaCrossbow[3][3]},
            new State(GIRLNINJA_FIRE_R3 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_GirlNinjaCrossbow[3][4]},
            new State(GIRLNINJA_FIRE_R3 + 1, GIRLNINJA_CROSSBOW_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaCrossbow[3][4]},
    }, {new State(GIRLNINJA_FIRE_R4, GIRLNINJA_CROSSBOW_RATE * 2, NullGirlNinja),
            // s_GirlNinjaCrossbow[4][1]},
            new State(GIRLNINJA_FIRE_R4 + 1, SF_QUICK_CALL, InitEnemyCrossbow),
            // s_GirlNinjaCrossbow[4][2]},
            new State(GIRLNINJA_FIRE_R4 + 1, GIRLNINJA_CROSSBOW_RATE, NullGirlNinja),
            // s_GirlNinjaCrossbow[4][3]},
            new State(GIRLNINJA_FIRE_R4 + 1, SF_QUICK_CALL, InitActorDecide),
            // s_GirlNinjaCrossbow[4][4]},
            new State(GIRLNINJA_FIRE_R4 + 1, GIRLNINJA_CROSSBOW_RATE, DoGirlNinjaMove).setNext(),
// s_GirlNinjaCrossbow[4][4]},
    }};
    private static final EnemyStateGroup sg_GirlNinjaCrossbow = new EnemyStateGroup(s_GirlNinjaCrossbow[0], s_GirlNinjaCrossbow[1], s_GirlNinjaCrossbow[2], s_GirlNinjaCrossbow[3], s_GirlNinjaCrossbow[4]);
    //////////////////////
    //
    // GIRLNINJA DIE
    //
    //////////////////////
    private static final State[] s_GirlNinjaDie = {new State(GIRLNINJA_DIE, GIRLNINJA_DIE_RATE * 2, NullGirlNinja),
            // s_GirlNinjaDie[1]},
            new State(GIRLNINJA_DIE + 1, GIRLNINJA_DIE_RATE, NullGirlNinja),
            // s_GirlNinjaDie[2]},
            new State(GIRLNINJA_DIE + 2, GIRLNINJA_DIE_RATE, NullGirlNinja),
            // s_GirlNinjaDie[3]},
            new State(GIRLNINJA_DIE + 3, GIRLNINJA_DIE_RATE, NullGirlNinja),
            // s_GirlNinjaDie[4]},
            new State(GIRLNINJA_DIE + 4, GIRLNINJA_DIE_RATE, NullGirlNinja),
            // s_GirlNinjaDie[5]},
            new State(GIRLNINJA_DIE + 5, GIRLNINJA_DIE_RATE, NullGirlNinja),
            // s_GirlNinjaDie[6]},
            new State(GIRLNINJA_DIE + 6, GIRLNINJA_DIE_RATE, NullGirlNinja),
            // s_GirlNinjaDie[7]},
            new State(GIRLNINJA_DIE + 6, SF_QUICK_CALL, DoGirlNinjaSpecial),
            // s_GirlNinjaDie[8]},
            new State(GIRLNINJA_DIE + 7, GIRLNINJA_DIE_RATE, NullGirlNinja),
            // s_GirlNinjaDie[9]},
            new State(GIRLNINJA_DIE + 8, SF_QUICK_CALL, QueueFloorBlood),
            // s_GirlNinjaDie[10]},
            new State(GIRLNINJA_DIE + 8, GIRLNINJA_DIE_RATE, DoActorDebris).setNext(),
// s_GirlNinjaDie[10]},
    };
    private static final EnemyStateGroup sg_GirlNinjaDie = new EnemyStateGroup(s_GirlNinjaDie);
    private static final State[] s_GirlNinjaDead = {new State(GIRLNINJA_DIE + 6, GIRLNINJA_DIE_RATE, DoActorDebris),
            // s_GirlNinjaDead[1]},
            new State(GIRLNINJA_DIE + 7, SF_QUICK_CALL, DoGirlNinjaSpecial),
            // s_GirlNinjaDead[2]},
            new State(GIRLNINJA_DIE + 7, GIRLNINJA_DIE_RATE, DoActorDebris),
            // s_GirlNinjaDead[3]},
            new State(GIRLNINJA_DIE + 8, SF_QUICK_CALL, QueueFloorBlood),
            // s_GirlNinjaDead[4]},
            new State(GIRLNINJA_DIE + 8, GIRLNINJA_DIE_RATE, DoActorDebris).setNext(),
// s_GirlNinjaDead[4]},
    };
    private static final EnemyStateGroup sg_GirlNinjaDead = new EnemyStateGroup(s_GirlNinjaDead);
    private static final State[] s_GirlNinjaDeathJump = {new State(GIRLNINJA_DIE, GIRLNINJA_DIE_RATE, DoActorDeathMove),
            // s_GirlNinjaDeathJump[1]},
            new State(GIRLNINJA_DIE + 1, GIRLNINJA_DIE_RATE, DoActorDeathMove),
            // s_GirlNinjaDeathJump[2]},
            new State(GIRLNINJA_DIE + 2, GIRLNINJA_DIE_RATE, DoActorDeathMove).setNext(),
// s_GirlNinjaDeathJump[2]},
    };
    private static final EnemyStateGroup sg_GirlNinjaDeathJump = new EnemyStateGroup(s_GirlNinjaDeathJump);
    private static final State[] s_GirlNinjaDeathFall = {new State(GIRLNINJA_DIE + 3, GIRLNINJA_DIE_RATE, DoActorDeathMove),
            // s_GirlNinjaDeathFall[1]},
            new State(GIRLNINJA_DIE + 4, GIRLNINJA_DIE_RATE, DoActorDeathMove).setNext(),
// s_GirlNinjaDeathFall[1]},
    };
    private static final EnemyStateGroup sg_GirlNinjaDeathFall = new EnemyStateGroup(s_GirlNinjaDeathFall);
    private static final Actor_Action_Set GirlNinjaActionSet = new Actor_Action_Set(sg_GirlNinjaStand, sg_GirlNinjaRun, sg_GirlNinjaJump, sg_GirlNinjaFall, null, null, null, sg_GirlNinjaRise, sg_GirlNinjaSit, null, null, sg_GirlNinjaPain, sg_GirlNinjaDie, null, sg_GirlNinjaDead, sg_GirlNinjaDeathJump, sg_GirlNinjaDeathFall, new StateGroup[]{sg_GirlNinjaCrossbow, sg_GirlNinjaSticky}, new short[]{800, 1024}, new StateGroup[]{sg_GirlNinjaCrossbow, sg_GirlNinjaSticky}, new short[]{800, 1024}, null, sg_GirlNinjaDuck, null);

    public static void InitGNinjaStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{
                sg_GirlNinjaStand,
                sg_GirlNinjaRun,
                sg_GirlNinjaJump,
                sg_GirlNinjaFall,
                sg_GirlNinjaRise,
                sg_GirlNinjaSit,
                sg_GirlNinjaPain,
                sg_GirlNinjaDie,
                sg_GirlNinjaDead,
                sg_GirlNinjaDeathJump,
                sg_GirlNinjaDeathFall,
                sg_GirlNinjaCrossbow,
                sg_GirlNinjaSticky,
                sg_GirlNinjaDuck}) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);

                if (sg == sg_GirlNinjaRise) {
                    sg.getGroup()[rot][2].setNextGroup(sg_GirlNinjaRun);
                }
            }
        }
    }

    /*
     *
     * !AIC - Collection of private static final States that connect action to
     * private static final States
     *
     */

    public static void SetupGirlNinja(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        USER u;

        if (TEST(sp.getCstat(),
                CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, GIRLNINJA_RUN_R0, s_GirlNinjaRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = 100;
        }

        u.StateEnd = s_GirlNinjaDie[0];
        u.setRot(sg_GirlNinjaRun);
        sp.setXrepeat(51);
        sp.setYrepeat(43);

        u.Attrib = GirlNinjaAttrib;
        sp.setPal(u.spal = 26);
        EnemyDefaults(SpriteNum, GirlNinjaActionSet, GirlNinjaPersonality);

        ChangeState(SpriteNum, s_GirlNinjaRun[0][0]);
        DoActorSetSpeed(SpriteNum, NORM_SPEED);

        u.Radius = 280;
        u.Flags &= ~(SPR_XFLIP_TOGGLE);
    }

    private static void DoGirlNinjaMove(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        // jumping and falling
        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING) && !TEST(u.Flags, SPR_CLIMBING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoActorJump(SpriteNum);
            } else if (TEST(u.Flags, SPR_FALLING)) {
                DoActorFall(SpriteNum);
            }
        }

        // sliding
        if (TEST(u.Flags, SPR_SLIDING) && !TEST(u.Flags, SPR_CLIMBING)) {
            DoActorSlide(SpriteNum);
        }

        // !AIC - do track or call current action function - such as DoActorMoveCloser()
        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        // stay on floor unless doing certain things
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING | SPR_CLIMBING)) {
            KeepActorOnFloor(SpriteNum);
        }

        // take damage from environment
        DoActorSectorDamage(SpriteNum);
    }

    private static void NullGirlNinja(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (u.WaitTics > 0) {
            u.WaitTics -= ACTORMOVETICS;
        }

        if (TEST(u.Flags, SPR_SLIDING) && !TEST(u.Flags, SPR_CLIMBING) && !TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            DoActorSlide(SpriteNum);
        }

        if (!TEST(u.Flags, SPR_CLIMBING) && !TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            KeepActorOnFloor(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
    }

    private static void DoGirlNinjaPain(int SpriteNum) {
        USER u = getUser(SpriteNum);

        NullGirlNinja(SpriteNum);

        if (u != null && (u.WaitTics -= ACTORMOVETICS) <= 0) {
            InitActorDecide(SpriteNum);
        }
    }

    private static void DoGirlNinjaSpecial(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (u.spal == PALETTE_PLAYER5) {
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_TRANSLUCENT));
            sp.setHitag(0);
            sp.setShade(-10);
        }
    }

    public static void InitEnemyMine(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        USER wu;
        Sprite wp;
        int nx, ny, nz;
        int w;

        PlaySound(DIGI_MINETHROW, sp, v3df_dontpan | v3df_doppler);

        nx = sp.getX();
        ny = sp.getY();
        nz = sp.getZ() - Z(40);

        // Spawn a shot
        // Inserting and setting up variables
        w = SpawnSprite(STAT_MISSILE, MINE, s_Mine[0], sp.getSectnum(),
                nx, ny, nz, sp.getAng(),
                MINE_VELOCITY);
        if (w == -1) {
            return;
        }

        wp = boardService.getSprite(w);
        wu = getUser(w);
        if (wp == null || wu == null) {
            return;
        }

        SetOwner(SpriteNum, w);
        wp.setYrepeat(32);
        wp.setXrepeat(32);
        wp.setShade(-15);
        wp.setClipdist(128 >> 2);

        // (velocity * difference between the target and the object) /
        // distance

        wu.WeaponNum = u.WeaponNum;
        wu.Radius = 200;
        wu.ceiling_dist = Z(5);
        wu.floor_dist = Z(5);
        wu.Counter = 0;
        wp.setCstat(wp.getCstat() | (CSTAT_SPRITE_YCENTER));
        wp.setCstat(wp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        wu.spal = u.spal;
        wp.setPal(wu.spal); // Set sticky color

        if (SpriteInUnderwaterArea(wp)) {
            wu.Flags |= (SPR_UNDERWATER);
        }

        MissileSetPos(w, DoMine, 300);
        wp.setAng(NORM_ANGLE(wp.getAng() - 512));
        MissileSetPos(w, DoMine, 300);
        wp.setAng(NORM_ANGLE(wp.getAng() + 512));

        wu.zchange = -5000;
        // CON_Message("change = %ld",wu.zchange);
        wu.xchange = MOVEx(wp.getXvel(),
                wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(),
                wp.getAng());
    }

    public static boolean InitEnemyCrossbow(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return false;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return false;
        }

        int nang;
        int w;

        // get angle to player and also face player when attacking
        sp.setAng((nang = NORM_ANGLE(EngineUtils.getAngle(tsp.getX() - sp.getX(),
                tsp.getY() - sp.getY()))));

        int nx = sp.getX();
        int ny = sp.getY();
        int nz = SPRITEp_MID(sp) - Z(14);

        // Spawn a shot
        Sprite wp = boardService.getSprite(w = SpawnSprite(STAT_MISSILE, CROSSBOLT, s_CrossBolt[0][0], sp.getSectnum(),
                nx, ny, nz, tsp.getAng(),
                800));

        USER wu = getUser(w);
        if (wp == null || wu == null) {
            return false;
        }

        // wp.owner = SpriteNum;
        SetOwner(SpriteNum, w);
        wp.setXrepeat(16);
        wp.setYrepeat(26);
        wp.setShade(-25);
        wp.setZvel(0);
        wp.setAng(nang);
        wp.setClipdist(64 >> 2);

        wu.RotNum = 5;
        NewStateGroup(w, Weapon.WeaponStateGroup.sg_CrossBolt);

        wu.xchange = MOVEx(wp.getXvel(),
                wp.getAng());
        wu.ychange = MOVEy(wp.getXvel(),
                wp.getAng());
        wu.zchange = wp.getZvel();

        wu.Flags |= (SPR_XFLIP_TOGGLE);

        MissileSetPos(w, DoStar, 400);

        // find the distance to the target (player)
        int dist = Distance(wp.getX(),
                wp.getY(),
                tsp.getX(),
                tsp.getY());

        if (dist != 0) {
            wu.zchange = (short) ((wp.getXvel() * (SPRITEp_UPPER(tsp) - wp.getZ())) / dist);
            wp.setZvel(wu.zchange);
        }

        //
        // Star Power Up Code
        //

        PlaySound(DIGI_STAR, sp, v3df_none);

        return (w) != 0;
    }

    public static void GirlNinjSaveable() {

        SaveData(InitEnemyMine);
        SaveData(InitEnemyCrossbow);
        SaveData(DoActorDeathMove);

        SaveData(DoGirlNinjaMove);
        SaveData(NullGirlNinja);
        SaveData(DoGirlNinjaPain);
        SaveData(DoGirlNinjaSpecial);
        SaveData(GirlNinjaPersonality);

        SaveData(GirlNinjaAttrib);

        SaveData(s_GirlNinjaRun);
        SaveGroup(sg_GirlNinjaRun);
        SaveData(s_GirlNinjaStand);
        SaveGroup(sg_GirlNinjaStand);
        SaveData(s_GirlNinjaRise);
        SaveGroup(sg_GirlNinjaRise);
        SaveData(s_GirlNinjaDuck);
        SaveGroup(sg_GirlNinjaDuck);
        SaveData(s_GirlNinjaSit);
        SaveGroup(sg_GirlNinjaSit);
        SaveData(s_GirlNinjaJump);
        SaveGroup(sg_GirlNinjaJump);
        SaveData(s_GirlNinjaFall);
        SaveGroup(sg_GirlNinjaFall);
        SaveData(s_GirlNinjaPain);
        SaveGroup(sg_GirlNinjaPain);
        SaveData(s_GirlNinjaSticky);
        SaveGroup(sg_GirlNinjaSticky);
        SaveData(s_GirlNinjaCrossbow);
        SaveGroup(sg_GirlNinjaCrossbow);
        SaveData(s_GirlNinjaDie);
        SaveData(s_GirlNinjaDead);
        SaveData(s_GirlNinjaDeathJump);
        SaveData(s_GirlNinjaDeathFall);

        SaveData(GirlNinjaActionSet);
    }
}
