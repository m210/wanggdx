package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.Engine.HIT_WALL;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Enemies.Hornet.DoCheckSwarm;
import static ru.m210projects.Wang.Enemies.Ninja.DoActorDeathMove;
import static ru.m210projects.Wang.Enemies.Ripper.PickJumpMaxSpeed;
import static ru.m210projects.Wang.Game.Distance;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JTags.TAG_SWARMSPOT;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.PALETTE_BROWN_RIPPER;
import static ru.m210projects.Wang.Player.QueueFloorBlood;
import static ru.m210projects.Wang.Rooms.COVERinsertsprite;
import static ru.m210projects.Wang.Rooms.FAFhitscan;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.*;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Ripper2 {

    public static final Animator NullRipper2 = new Animator((Animator.Runnable) Ripper2::NullRipper2);
    public static final Animator ChestRipper2 = new Animator((Animator.Runnable) Ripper2::ChestRipper2);
    public static final Animator InitRipper2Charge = new Animator((Animator.Runnable) Ripper2::InitRipper2Charge);
    public static final Animator DoRipper2Pain = new Animator((Animator.Runnable) Ripper2::DoRipper2Pain);    public static final Animator InitRipper2Hang = new Animator((Animator.Runnable) Ripper2::InitRipper2Hang);
    public static final Animator DoRipper2BeginJumpAttack = new Animator((Animator.Runnable) Ripper2::DoRipper2BeginJumpAttack);    public static final Animator DoRipper2Move = new Animator((Animator.Runnable) Ripper2::DoRipper2Move);
    private static final Decision[] Ripper2Broadcast = {new Decision(3, InitActorAmbientNoise),

            new Decision(1024, InitActorDecide),
    };
    private static final Decision[] Ripper2LostTarget = {new Decision(900, InitActorFindPlayer),

            new Decision(1024, InitActorWanderAround)};    public static final Animator DoRipper2MoveJump = new Animator((Animator.Runnable) Ripper2::DoRipper2MoveJump);
    private static final Decision[] Ripper2CloseRange = {new Decision(1024, InitActorAttack)};
    private static final ATTRIBUTE Ripper2Attrib = new ATTRIBUTE(new short[]{100, 120, 300, 380}, // Speeds
            new short[]{5, 0, -2, -4}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_RIPPER2AMBIENT, DIGI_RIPPER2ALERT, DIGI_RIPPER2ATTACK, DIGI_RIPPER2PAIN,
                    DIGI_RIPPER2SCREAM, DIGI_RIPPER2HEARTOUT, 0, 0, 0, 0});    public static final Animator InitRipperSlash = new Animator((Animator.Runnable) Ripper2::InitRipperSlash);
    private static final int RIPPER2_RUN_RATE = 16;    public static final Animator DoRipper2StandHeart = new Animator((Animator.Runnable) Ripper2::DoRipper2StandHeart);
    private static final int RIPPER2_STAND_RATE = 12;    public static final Animator DoRipper2Hang = new Animator((Animator.Runnable) Ripper2::DoRipper2Hang);
    private static final int RIPPER2_RUNFAST_RATE = 14;
    private static final int RIPPER2_JUMP_RATE = 25;
    private static final int RIPPER2_DIE_RATE = 18;    public static final Animator DoRipper2HangJF = new Animator((Animator.Runnable) Ripper2::DoRipper2HangJF);
    private static final State[] s_Ripper2Die = {new State(RIPPER2_DIE, RIPPER2_DIE_RATE, NullRipper2),
            // s_Ripper2Die[1]},
            new State(RIPPER2_DIE + 1, RIPPER2_DIE_RATE, NullRipper2),
            // s_Ripper2Die[2]},
            new State(RIPPER2_DIE + 2, RIPPER2_DIE_RATE, NullRipper2),
            // s_Ripper2Die[3]},
            new State(RIPPER2_DIE + 3, RIPPER2_DIE_RATE, NullRipper2),
            // s_Ripper2Die[4]},
            new State(RIPPER2_DIE + 4, RIPPER2_DIE_RATE, NullRipper2),
            // s_Ripper2Die[5]},
            new State(RIPPER2_DIE + 5, RIPPER2_DIE_RATE, NullRipper2),
            // s_Ripper2Die[6]},
            new State(RIPPER2_DIE + 6, RIPPER2_DIE_RATE, NullRipper2),
            // s_Ripper2Die[7]},
            new State(RIPPER2_DEAD, RIPPER2_DIE_RATE, DoActorDebris).setNext(),
            // s_Ripper2Die[7]},
    };    private static final Decision[] Ripper2Battle = {new Decision(879, InitRipper2Charge),

            new Decision(883, InitActorAttackNoise),
            new Decision(900, InitRipper2Hang),

            new Decision(1024, InitActorAttack),
    };
    private static final EnemyStateGroup sg_Ripper2Die = new EnemyStateGroup(s_Ripper2Die);    private static final Decision[] Ripper2Offense = {new Decision(789, InitActorMoveCloser),

            new Decision(790, InitActorAttackNoise),
            new Decision(800, InitRipper2Hang),

            new Decision(1024, InitActorAttack),
    };
    private static final int RIPPER2_SWIPE_RATE = 14;
    private static final int RIPPER2_MEKONG_RATE = 18;    private static final Decision[] Ripper2Surprised = {new Decision(40, InitRipper2Hang),

            new Decision(701, InitActorMoveCloser),
            new Decision(1024, InitActorDecide),
    };
    private static final int RIPPER2_HEART_RATE = 20;    private static final Decision[] Ripper2Evasive = {new Decision(10, InitActorMoveCloser),

            new Decision(1024, InitRipper2Charge),
    };
    private static final int RIPPER2_HANG_RATE = 14;
    private static final int RIPPER2_PAIN_RATE = 38;
    private static final State[][] s_Ripper2Pain = {{new State(4414, RIPPER2_PAIN_RATE, DoRipper2Pain).setNext(),
            // s_Ripper2Pain[0][0]},
    }, {new State(4414, RIPPER2_PAIN_RATE, DoRipper2Pain).setNext(),
            // s_Ripper2Pain[1][0]},
    }, {new State(4414, RIPPER2_PAIN_RATE, DoRipper2Pain).setNext(),
            // s_Ripper2Pain[2][0]},
    }, {new State(4414, RIPPER2_PAIN_RATE, DoRipper2Pain).setNext(),
            // s_Ripper2Pain[3][0]},
    }, {new State(4414, RIPPER2_PAIN_RATE, DoRipper2Pain).setNext(),
            // s_Ripper2Pain[4][0]},
    }};    private static final Personality Ripper2Personality = new Personality(Ripper2Battle, Ripper2Offense,
            Ripper2Broadcast, Ripper2Surprised, Ripper2Evasive, Ripper2LostTarget, Ripper2CloseRange,
            Ripper2CloseRange);
    private static final EnemyStateGroup sg_Ripper2Pain = new EnemyStateGroup(s_Ripper2Pain[0], s_Ripper2Pain[1], s_Ripper2Pain[2], s_Ripper2Pain[3], s_Ripper2Pain[4]);
    private static final int RIPPER2_FALL_RATE = 25;
    private static final int RIPPER2_JUMP_ATTACK_RATE = 35;    private static final State[][] s_Ripper2Run = {
            {new State(RIPPER2_RUN_R0, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
                    // s_Ripper2Run[0][1]},
                    new State(RIPPER2_RUN_R0 + 1, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
                    // s_Ripper2Run[0][2]},
                    new State(RIPPER2_RUN_R0 + 2, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
                    // s_Ripper2Run[0][3]},
                    new State(RIPPER2_RUN_R0 + 3, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
                    // s_Ripper2Run[0][0]},
            }, {new State(RIPPER2_RUN_R1, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[1][1]},
            new State(RIPPER2_RUN_R1 + 1, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[1][2]},
            new State(RIPPER2_RUN_R1 + 2, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[1][3]},
            new State(RIPPER2_RUN_R1 + 3, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[1][0]},
    }, {new State(RIPPER2_RUN_R2, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[2][1]},
            new State(RIPPER2_RUN_R2 + 1, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[2][2]},
            new State(RIPPER2_RUN_R2 + 2, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[2][3]},
            new State(RIPPER2_RUN_R2 + 3, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[2][0]},
    }, {new State(RIPPER2_RUN_R3, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[3][1]},
            new State(RIPPER2_RUN_R3 + 1, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[3][2]},
            new State(RIPPER2_RUN_R3 + 2, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[3][3]},
            new State(RIPPER2_RUN_R3 + 3, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[3][0]},
    }, {new State(RIPPER2_RUN_R4, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[4][1]},
            new State(RIPPER2_RUN_R4 + 1, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[4][2]},
            new State(RIPPER2_RUN_R4 + 2, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[4][3]},
            new State(RIPPER2_RUN_R4 + 3, RIPPER2_RUN_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2Run[4][0]},
    }};
    private static final int RIPPER2_HANG_JUMP_RATE = 20;
    private static final State[] s_Ripper2DeathJump = {
            new State(RIPPER2_DIE, RIPPER2_DIE_RATE, DoActorDeathMove).setNext(),
            // s_Ripper2DeathJump[0]}
    };    private static final State[][] s_Ripper2Stand = {
            {new State(RIPPER2_STAND_R0, RIPPER2_STAND_RATE, DoRipper2Move).setNext(),
                    // s_Ripper2Stand[0][0]},
            }, {new State(RIPPER2_STAND_R1, RIPPER2_STAND_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Stand[1][0]},
    }, {new State(RIPPER2_STAND_R2, RIPPER2_STAND_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Stand[2][0]},
    }, {new State(RIPPER2_STAND_R3, RIPPER2_STAND_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Stand[3][0]},
    }, {new State(RIPPER2_STAND_R4, RIPPER2_STAND_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Stand[4][0]},
    },};
    private static final EnemyStateGroup sg_Ripper2DeathJump = new EnemyStateGroup(s_Ripper2DeathJump);
    private static final State[] s_Ripper2DeathFall = {
            new State(RIPPER2_DIE + 1, RIPPER2_DIE_RATE, DoActorDeathMove).setNext(),
            // s_Ripper2DeathFall[0]}
    };    private static final State[][] s_Ripper2RunFast = {
            {new State(RIPPER2_RUNFAST_R0, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
                    // s_Ripper2RunFast[0][1]},
                    new State(RIPPER2_RUNFAST_R0 + 1, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
                    // s_Ripper2RunFast[0][2]},
                    new State(RIPPER2_RUNFAST_R0 + 2, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
                    // s_Ripper2RunFast[0][3]},
                    new State(RIPPER2_RUNFAST_R0 + 3, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
                    // s_Ripper2RunFast[0][0]},
            }, {new State(RIPPER2_RUNFAST_R1, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[1][1]},
            new State(RIPPER2_RUNFAST_R1 + 1, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[1][2]},
            new State(RIPPER2_RUNFAST_R1 + 2, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[1][3]},
            new State(RIPPER2_RUNFAST_R1 + 3, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[1][0]},
    }, {new State(RIPPER2_RUNFAST_R2, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[2][1]},
            new State(RIPPER2_RUNFAST_R2 + 1, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[2][2]},
            new State(RIPPER2_RUNFAST_R2 + 2, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[2][3]},
            new State(RIPPER2_RUNFAST_R2 + 3, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[2][0]},
    }, {new State(RIPPER2_RUNFAST_R3, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[3][1]},
            new State(RIPPER2_RUNFAST_R3 + 1, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[3][2]},
            new State(RIPPER2_RUNFAST_R3 + 2, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[3][3]},
            new State(RIPPER2_RUNFAST_R3 + 3, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[3][0]},
    }, {new State(RIPPER2_RUNFAST_R4, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[4][1]},
            new State(RIPPER2_RUNFAST_R4 + 1, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[4][2]},
            new State(RIPPER2_RUNFAST_R4 + 2, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[4][3]},
            new State(RIPPER2_RUNFAST_R4 + 3, RIPPER2_RUNFAST_RATE | SF_TIC_ADJUST, DoRipper2Move),
            // s_Ripper2RunFast[4][0]},
    }};
    private static final EnemyStateGroup sg_Ripper2DeathFall = new EnemyStateGroup(s_Ripper2DeathFall);
    private static final int RIPPER2_DEAD_RATE = 8;    private static final State[][] s_Ripper2Jump = {{new State(RIPPER2_JUMP_R0, RIPPER2_JUMP_RATE, NullRipper2),
            // s_Ripper2Jump[0][1]},
            new State(RIPPER2_JUMP_R0 + 1, RIPPER2_JUMP_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2Jump[0][1]},
    }, {new State(RIPPER2_JUMP_R1, RIPPER2_JUMP_RATE, NullRipper2),
            // s_Ripper2Jump[1][1]},
            new State(RIPPER2_JUMP_R1 + 1, RIPPER2_JUMP_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2Jump[1][1]},
    }, {new State(RIPPER2_JUMP_R2, RIPPER2_JUMP_RATE, NullRipper2),
            // s_Ripper2Jump[2][1]},
            new State(RIPPER2_JUMP_R2 + 1, RIPPER2_JUMP_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2Jump[2][1]},
    }, {new State(RIPPER2_JUMP_R3, RIPPER2_JUMP_RATE, NullRipper2),
            // s_Ripper2Jump[3][1]},
            new State(RIPPER2_JUMP_R3 + 1, RIPPER2_JUMP_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2Jump[3][1]},
    }, {new State(RIPPER2_JUMP_R4, RIPPER2_JUMP_RATE, NullRipper2),
            // s_Ripper2Jump[4][1]},
            new State(RIPPER2_JUMP_R4 + 1, RIPPER2_JUMP_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2Jump[4][1]},
    }};
    private static final State[] s_Ripper2Dead = {new State(RIPPER2_DIE, RIPPER2_DIE_RATE, NullRipper2),
            // &s_Ripper2Dead[1]},
            new State(RIPPER2_DIE + 1, RIPPER2_DIE_RATE, NullRipper2),
            // &s_Ripper2Dead[2]},
            new State(RIPPER2_DIE + 2, RIPPER2_DIE_RATE, NullRipper2),
            // &s_Ripper2Dead[3]},
            new State(RIPPER2_DIE + 3, RIPPER2_DIE_RATE, NullRipper2),
            // &s_Ripper2Dead[4]},
            new State(RIPPER2_DIE + 4, RIPPER2_DIE_RATE, NullRipper2),
            // &s_Ripper2Dead[5]},
            new State(RIPPER2_DIE + 5, RIPPER2_DIE_RATE, NullRipper2),
            // &s_Ripper2Dead[6]},
            new State(RIPPER2_DIE + 6, RIPPER2_DIE_RATE, NullRipper2),
            // &s_Ripper2Dead[7]},
            new State(RIPPER2_DEAD, SF_QUICK_CALL, QueueFloorBlood),
            // &s_Ripper2Dead[8]},
            new State(RIPPER2_DEAD, RIPPER2_DEAD_RATE, DoActorDebris).setNext(),
            // &s_Ripper2Dead[8]},
    };
    private static final EnemyStateGroup sg_Ripper2Dead = new EnemyStateGroup(s_Ripper2Dead);

//////////////////////
//
//RIPPER2 SWIPE
//
//////////////////////
    private static VOC3D riphearthandle = null;

    public static void InitRipper2States() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{
                sg_Ripper2Stand,
                sg_Ripper2Run,
                sg_Ripper2RunFast,
                sg_Ripper2Jump,
                sg_Ripper2Swipe,
                sg_Ripper2Kong,
                sg_Ripper2Heart,
                sg_Ripper2Pain,
                sg_Ripper2Fall,
                sg_Ripper2JumpAttack,
                sg_Ripper2HangJump,
                sg_Ripper2HangFall,
                sg_Ripper2Hang,
                sg_Ripper2Die,
                sg_Ripper2Dead,
                sg_Ripper2DeathJump,
                sg_Ripper2DeathFall
        }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }    private static final State[][] s_Ripper2Swipe = {
            {new State(RIPPER2_SWIPE_R0, RIPPER2_SWIPE_RATE, NullRipper2),
                    // s_Ripper2Swipe[0][1]},
                    new State(RIPPER2_SWIPE_R0 + 1, RIPPER2_SWIPE_RATE, NullRipper2),
                    // s_Ripper2Swipe[0][2]},
                    new State(RIPPER2_SWIPE_R0 + 1, SF_QUICK_CALL, InitRipperSlash),
                    // s_Ripper2Swipe[0][3]},
                    new State(RIPPER2_SWIPE_R0 + 2, RIPPER2_SWIPE_RATE, NullRipper2),
                    // s_Ripper2Swipe[0][4]},
                    new State(RIPPER2_SWIPE_R0 + 3, RIPPER2_SWIPE_RATE, NullRipper2),
                    // s_Ripper2Swipe[0][5]},
                    new State(RIPPER2_SWIPE_R0 + 3, SF_QUICK_CALL, InitRipperSlash),
                    // s_Ripper2Swipe[0][6]},
                    new State(RIPPER2_SWIPE_R0 + 3, SF_QUICK_CALL, InitActorDecide),
                    // s_Ripper2Swipe[0][7]},
                    new State(RIPPER2_SWIPE_R0 + 3, RIPPER2_SWIPE_RATE, DoRipper2Move).setNext(),
                    // s_Ripper2Swipe[0][7]},
            }, {new State(RIPPER2_SWIPE_R1, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[1][1]},
            new State(RIPPER2_SWIPE_R1 + 1, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[1][2]},
            new State(RIPPER2_SWIPE_R1 + 1, SF_QUICK_CALL, InitRipperSlash),
            // s_Ripper2Swipe[1][3]},
            new State(RIPPER2_SWIPE_R1 + 2, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[1][4]},
            new State(RIPPER2_SWIPE_R1 + 3, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[1][5]},
            new State(RIPPER2_SWIPE_R1 + 3, SF_QUICK_CALL, InitRipperSlash),
            // s_Ripper2Swipe[1][6]},
            new State(RIPPER2_SWIPE_R1 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_Ripper2Swipe[1][7]},
            new State(RIPPER2_SWIPE_R1 + 3, RIPPER2_SWIPE_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Swipe[1][7]},
    }, {new State(RIPPER2_SWIPE_R2, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[2][1]},
            new State(RIPPER2_SWIPE_R2 + 1, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[2][2]},
            new State(RIPPER2_SWIPE_R2 + 1, SF_QUICK_CALL, InitRipperSlash),
            // s_Ripper2Swipe[2][3]},
            new State(RIPPER2_SWIPE_R2 + 2, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[2][4]},
            new State(RIPPER2_SWIPE_R2 + 3, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[2][5]},
            new State(RIPPER2_SWIPE_R2 + 3, SF_QUICK_CALL, InitRipperSlash),
            // s_Ripper2Swipe[2][6]},
            new State(RIPPER2_SWIPE_R2 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_Ripper2Swipe[2][7]},
            new State(RIPPER2_SWIPE_R2 + 3, RIPPER2_SWIPE_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Swipe[2][7]},
    }, {new State(RIPPER2_SWIPE_R3, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[3][1]},
            new State(RIPPER2_SWIPE_R3 + 1, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[3][2]},
            new State(RIPPER2_SWIPE_R3 + 1, SF_QUICK_CALL, InitRipperSlash),
            // s_Ripper2Swipe[3][3]},
            new State(RIPPER2_SWIPE_R3 + 2, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[3][4]},
            new State(RIPPER2_SWIPE_R3 + 3, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[3][5]},
            new State(RIPPER2_SWIPE_R3 + 3, SF_QUICK_CALL, InitRipperSlash),
            // s_Ripper2Swipe[3][6]},
            new State(RIPPER2_SWIPE_R3 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_Ripper2Swipe[3][7]},
            new State(RIPPER2_SWIPE_R3 + 3, RIPPER2_SWIPE_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Swipe[3][7]},
    }, {new State(RIPPER2_SWIPE_R4, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[4][1]},
            new State(RIPPER2_SWIPE_R4 + 1, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[4][2]},
            new State(RIPPER2_SWIPE_R4 + 1, SF_QUICK_CALL, InitRipperSlash),
            // s_Ripper2Swipe[4][3]},
            new State(RIPPER2_SWIPE_R4 + 2, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[4][4]},
            new State(RIPPER2_SWIPE_R4 + 3, RIPPER2_SWIPE_RATE, NullRipper2),
            // s_Ripper2Swipe[4][5]},
            new State(RIPPER2_SWIPE_R4 + 3, SF_QUICK_CALL, InitRipperSlash),
            // s_Ripper2Swipe[4][6]},
            new State(RIPPER2_SWIPE_R4 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_Ripper2Swipe[4][7]},
            new State(RIPPER2_SWIPE_R4 + 3, RIPPER2_SWIPE_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Swipe[4][7]},
    }};

//////////////////////
//
//RIPPER2 KONG
//
//////////////////////

    public static void SetupRipper2(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        USER u;

        if (TEST(sp.getCstat(),
                CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, RIPPER2_RUN_R0, s_Ripper2Run[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_RIPPER2;
        }

        ChangeState(SpriteNum, s_Ripper2Run[0][0]);
        u.Attrib = Ripper2Attrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_Ripper2Die[0];
        u.setRot(sg_Ripper2Run);
        sp.setClipdist(512 >> 2); // This actor is bigger, needs bigger box.
        sp.setXrepeat(55);
        sp.setYrepeat(55);

        if (sp.getPal() == PALETTE_BROWN_RIPPER) {
            EnemyDefaults(SpriteNum, Ripper2BrownActionSet, Ripper2Personality);
            sp.setXrepeat(sp.getXrepeat() + 40);
            sp.setYrepeat(sp.getYrepeat() + 40);

            if (!TEST(sp.getCstat(),
                    CSTAT_SPRITE_RESTORE)) {
                u.Health = HEALTH_MOMMA_RIPPER;
            }

            sp.setClipdist(sp.getClipdist() + (128 >> 2));
        } else {
            EnemyDefaults(SpriteNum, Ripper2ActionSet, Ripper2Personality);
        }

        u.Flags |= (SPR_XFLIP_TOGGLE);
    }

    public static void InitRipper2Hang(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int dist;

        int hitwall;
        int hitsect;
        int hitx, hity;

        boolean Found = false;
        int dang, tang;

        for (dang = 0; dang < 2048; dang += 128) {
            tang = NORM_ANGLE(sp.getAng() + dang);

            FAFhitscan(sp.getX(),
                    sp.getY(),
                    sp.getZ() - SPRITEp_SIZE_Z(sp),
                    sp.getSectnum(),
                    // Start position
                    EngineUtils.sin(NORM_ANGLE(tang + 512)),
                    // X vector of 3D ang
                    EngineUtils.sin(tang),
                    // Y vector of 3D ang
                    0, // Z vector of 3D ang
                    pHitInfo, CLIPMASK_MISSILE);

            hitsect = pHitInfo.hitsect;
            hitwall = pHitInfo.hitwall;
            hitx = pHitInfo.hitx;
            hity = pHitInfo.hity;

            if (hitsect == -1) {
                continue;
            }

            dist = Distance(sp.getX(),
                    sp.getY(),
                    hitx, hity);

            if (hitwall < 0 || dist < 2000 || dist > 7000) {
                continue;
            }

            Found = true;

            sp.setAng(tang);
            break;
        }

        if (!Found) {
            InitActorDecide(SpriteNum);
            return;
        }

        NewStateGroup(SpriteNum, sg_Ripper2HangJump);
        u.StateFallOverride = sg_Ripper2HangFall;
        DoActorSetSpeed(SpriteNum, FAST_SPEED);

        PickJumpMaxSpeed(SpriteNum, -(RANDOM_RANGE(400) + 100));

        u.Flags |= (SPR_JUMPING);
        u.Flags &= ~(SPR_FALLING);

        // set up individual actor jump gravity
        u.jump_grav = 8;

        DoJump(SpriteNum);
    }

    private static final State[][] s_Ripper2Kong = {
            {new State(RIPPER2_MEKONG_R0, RIPPER2_MEKONG_RATE, NullRipper2),
                    // s_Ripper2Kong[0][1]},
                    new State(RIPPER2_MEKONG_R0, SF_QUICK_CALL, ChestRipper2),
                    // s_Ripper2Kong[0][2]},
                    new State(RIPPER2_MEKONG_R0 + 1, RIPPER2_MEKONG_RATE, NullRipper2),
                    // s_Ripper2Kong[0][3]},
                    new State(RIPPER2_MEKONG_R0 + 2, RIPPER2_MEKONG_RATE, NullRipper2),
                    // s_Ripper2Kong[0][4]},
                    new State(RIPPER2_MEKONG_R0 + 3, RIPPER2_MEKONG_RATE, NullRipper2),
                    // s_Ripper2Kong[0][5]},
                    new State(RIPPER2_MEKONG_R0 + 3, SF_QUICK_CALL, InitActorDecide),
                    // s_Ripper2Kong[0][6]},
                    new State(RIPPER2_MEKONG_R0, RIPPER2_MEKONG_RATE, DoRipper2Move).setNext(),
                    // s_Ripper2Kong[0][6]},
            }, {new State(RIPPER2_MEKONG_R1, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[1][1]},
            new State(RIPPER2_MEKONG_R0, SF_QUICK_CALL, ChestRipper2),
            // s_Ripper2Kong[1][2]},
            new State(RIPPER2_MEKONG_R1 + 1, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[1][3]},
            new State(RIPPER2_MEKONG_R1 + 2, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[1][4]},
            new State(RIPPER2_MEKONG_R1 + 3, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[1][5]},
            new State(RIPPER2_MEKONG_R1 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_Ripper2Kong[1][6]},
            new State(RIPPER2_MEKONG_R1, RIPPER2_MEKONG_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Kong[1][6]},
    }, {new State(RIPPER2_MEKONG_R2, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[2][1]},
            new State(RIPPER2_MEKONG_R0, SF_QUICK_CALL, ChestRipper2),
            // s_Ripper2Kong[2][2]},
            new State(RIPPER2_MEKONG_R2 + 1, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[2][3]},
            new State(RIPPER2_MEKONG_R2 + 2, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[2][4]},
            new State(RIPPER2_MEKONG_R2 + 3, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[2][5]},
            new State(RIPPER2_MEKONG_R2 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_Ripper2Kong[2][6]},
            new State(RIPPER2_MEKONG_R2, RIPPER2_MEKONG_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Kong[2][6]},
    }, {new State(RIPPER2_MEKONG_R3, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[3][1]},
            new State(RIPPER2_MEKONG_R0, SF_QUICK_CALL, ChestRipper2),
            // s_Ripper2Kong[3][2]},
            new State(RIPPER2_MEKONG_R3 + 1, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[3][3]},
            new State(RIPPER2_MEKONG_R3 + 2, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[3][4]},
            new State(RIPPER2_MEKONG_R3 + 3, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[3][5]},
            new State(RIPPER2_MEKONG_R3 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_Ripper2Kong[3][6]},
            new State(RIPPER2_MEKONG_R3, RIPPER2_MEKONG_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Kong[3][6]},
    }, {new State(RIPPER2_MEKONG_R4, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[4][1]},
            new State(RIPPER2_MEKONG_R0, SF_QUICK_CALL, ChestRipper2),
            // s_Ripper2Kong[4][2]},
            new State(RIPPER2_MEKONG_R4 + 1, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[4][3]},
            new State(RIPPER2_MEKONG_R4 + 2, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[4][4]},
            new State(RIPPER2_MEKONG_R4 + 3, RIPPER2_MEKONG_RATE, NullRipper2),
            // s_Ripper2Kong[4][5]},
            new State(RIPPER2_MEKONG_R4 + 3, SF_QUICK_CALL, InitActorDecide),
            // s_Ripper2Kong[4][6]},
            new State(RIPPER2_MEKONG_R4, RIPPER2_MEKONG_RATE, DoRipper2Move).setNext(),
            // s_Ripper2Kong[4][6]},
    }};

    //////////////////////
    //
    //RIPPER2 HEART - show players heart
    //
    //////////////////////

    public static void DoRipper2Hang(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if  (u == null) {
            return;
        }

        if ((u.WaitTics -= ACTORMOVETICS) > 0) {
            return;
        }

        NewStateGroup(SpriteNum, sg_Ripper2JumpAttack);
        // move to the 2nd frame - past the pause frame
        u.Tics += u.State.Tics;
    }

    public static void DoRipper2MoveHang(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // Move while jumping
        int nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
        int ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

        // if cannot move the sprite
        if (!move_actor(SpriteNum, nx, ny, 0)) {
            if (DTEST(u.moveSpriteReturn, HIT_MASK) == HIT_WALL) {
                Sprite tsp = boardService.getSprite(u.tgt_sp);
                if (tsp == null) {
                    return;
                }

                // Don't keep clinging and going ever higher!
                if (klabs(sp.getZ() - tsp.getZ()) > (4000 << 4)) {
                    return;
                }

                int hitwall = NORM_HIT_INDEX(u.moveSpriteReturn);

                NewStateGroup(SpriteNum, u.ActorActionSet.Special[1]); //sg_Ripper2Hang
                if (RANDOM_P2(1024 << 8) >> 8 > 500) {
                    u.WaitTics = ((RANDOM_P2(2 << 8) >> 8) * 120);
                } else {
                    u.WaitTics = 0; // Double jump
                }

                // hang flush with the wall
                Wall wal = boardService.getWall(hitwall);
                if (wal != null) {
                    sp.setAng(NORM_ANGLE(wal.getWallAngle() - 512));
                }
            }
        }

    }    private static final State[][] s_Ripper2Heart = {
            {new State(RIPPER2_HEART_R0, RIPPER2_HEART_RATE, DoRipper2StandHeart),
                    // s_Ripper2Heart[0][1]},
                    new State(RIPPER2_HEART_R0 + 1, RIPPER2_HEART_RATE, DoRipper2StandHeart),
                    // s_Ripper2Heart[0][0]},
            }, {new State(RIPPER2_HEART_R1, RIPPER2_HEART_RATE, DoRipper2StandHeart),
            // s_Ripper2Heart[1][1]},
            new State(RIPPER2_HEART_R1 + 1, RIPPER2_HEART_RATE, DoRipper2StandHeart),
            // s_Ripper2Heart[1][0]},
    }, {new State(RIPPER2_HEART_R2, RIPPER2_HEART_RATE, DoRipper2StandHeart),
            // s_Ripper2Heart[2][1]},
            new State(RIPPER2_HEART_R2 + 1, RIPPER2_HEART_RATE, DoRipper2StandHeart),
            // s_Ripper2Heart[2][0]},
    }, {new State(RIPPER2_HEART_R3, RIPPER2_HEART_RATE, DoRipper2StandHeart),
            // s_Ripper2Heart[3][1]},
            new State(RIPPER2_HEART_R3 + 1, RIPPER2_HEART_RATE, DoRipper2StandHeart),
            // s_Ripper2Heart[3][0]},
    }, {new State(RIPPER2_HEART_R4, RIPPER2_HEART_RATE, DoRipper2StandHeart),
            // s_Ripper2Heart[4][1]},
            new State(RIPPER2_HEART_R4 + 1, RIPPER2_HEART_RATE, DoRipper2StandHeart),
            // s_Ripper2Heart[4][0]},
    }};

//////////////////////
//
//RIPPER2 HANG
//
//////////////////////

    public static void DoRipper2HangJF(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoJump(SpriteNum);
            } else {
                DoFall(SpriteNum);
            }
        }

        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (DoRipper2QuickJump(SpriteNum) != 0) {
                return;
            }

            InitActorDecide(SpriteNum);
        }

        DoRipper2MoveHang(SpriteNum);
    }

    private static int RANDOM_NEG() {
        return (RANDOM_P2((16384) << 1) - (16384));
    }

    private static final State[][] s_Ripper2Hang = {
            {new State(RIPPER2_HANG_R0, RIPPER2_HANG_RATE, DoRipper2Hang).setNext(),
                    // s_Ripper2Hang[0][0]},
            }, {new State(RIPPER2_HANG_R1, RIPPER2_HANG_RATE, DoRipper2Hang).setNext(),
            // s_Ripper2Hang[1][0]},
    }, {new State(RIPPER2_HANG_R2, RIPPER2_HANG_RATE, DoRipper2Hang).setNext(),
            // s_Ripper2Hang[2][0]},
    }, {new State(RIPPER2_HANG_R3, RIPPER2_HANG_RATE, DoRipper2Hang).setNext(),
            // s_Ripper2Hang[3][0]},
    }, {new State(RIPPER2_HANG_R4, RIPPER2_HANG_RATE, DoRipper2Hang).setNext(),
            // s_Ripper2Hang[4][0]},
    }};

//////////////////////
//
//RIPPER2 PAIN
//
//////////////////////

    public static void DoRipper2BeginJumpAttack(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite psp = boardService.getSprite(u.tgt_sp);
        if (psp == null) {
            return;
        }

        int tang = EngineUtils.getAngle(psp.getX() - sp.getX(),
                psp.getY() - sp.getY());
        if (move_sprite(SpriteNum, EngineUtils.sin(NORM_ANGLE(tang + 512)) >> 7, EngineUtils.sin(tang) >> 7, 0, u.ceiling_dist,
                u.floor_dist, CLIPMASK_ACTOR, ACTORMOVETICS) != 0) {
            sp.setAng(NORM_ANGLE((sp.getAng() + 1024) + (RANDOM_NEG() >> 6)));
        } else {
            sp.setAng(NORM_ANGLE(tang));
        }

        DoActorSetSpeed(SpriteNum, FAST_SPEED);

        PickJumpMaxSpeed(SpriteNum, -(RANDOM_RANGE(400) + 100));

        u.Flags |= (SPR_JUMPING);
        u.Flags &= ~(SPR_FALLING);

        // set up individual actor jump gravity
        u.jump_grav = 8;

        // if I didn't do this here they get stuck in the air sometimes
        DoActorZrange(SpriteNum);

        DoJump(SpriteNum);
    }

    public static void DoRipper2MoveJump(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoJump(SpriteNum);
            } else {
                DoFall(SpriteNum);
            }
        }

        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (DoRipper2QuickJump(SpriteNum) != 0) {
                return;
            }

            InitActorDecide(SpriteNum);
        }

        DoRipper2MoveHang(SpriteNum);
    }

//////////////////////
//
//RIPPER2 FALL
//
//////////////////////

    public static int DoRipper2QuickJump(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return FALSE;
        }

        // Tests to see if ripper2 is on top of a player/enemy and then immediatly
        // does another jump

        if (u.lo_sp != -1) {
            Sprite tsp = boardService.getSprite(u.lo_sp);
            if (tsp != null && TEST(tsp.getExtra(),
                    SPRX_PLAYER_OR_ENEMY)) {
                NewStateGroup(SpriteNum, sg_Ripper2JumpAttack);
                // move past the first state
                u.Tics = 30;
                return (TRUE);
            }
        }

        return (FALSE);
    }

    public static void NullRipper2(int SpriteNum) {
        USER u = getUser(SpriteNum);

        if (u != null && TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
    }

    private static final State[][] s_Ripper2Fall = {
            {new State(RIPPER2_FALL_R0, RIPPER2_FALL_RATE, DoRipper2MoveJump).setNext(),
                    // s_Ripper2Fall[0][0]},
            }, {new State(RIPPER2_FALL_R1, RIPPER2_FALL_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2Fall[1][0]},
    }, {new State(RIPPER2_FALL_R2, RIPPER2_FALL_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2Fall[2][0]},
    }, {new State(RIPPER2_FALL_R3, RIPPER2_FALL_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2Fall[3][0]},
    }, {new State(RIPPER2_FALL_R4, RIPPER2_FALL_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2Fall[4][0]},
    }};

//////////////////////
//
//RIPPER2 JUMP ATTACK
//
//////////////////////

    public static void DoRipper2Pain(int SpriteNum) {
        USER u = getUser(SpriteNum);

        NullRipper2(SpriteNum);

        if (u != null && (u.WaitTics -= ACTORMOVETICS) <= 0) {
            InitActorDecide(SpriteNum);
        }
    }

    public static void DoRipper2RipHeart(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }

        NewStateGroup(SpriteNum, sg_Ripper2Heart);
        u.WaitTics = 6 * 120;

        // player face ripper2
        tsp.setAng(EngineUtils.getAngle(sp.getX() - tsp.getX(),
                sp.getY() - tsp.getY()));
    }

    private static final State[][] s_Ripper2JumpAttack = {
            {new State(RIPPER2_JUMP_R0, RIPPER2_JUMP_ATTACK_RATE, NullRipper2),
                    // s_Ripper2JumpAttack[0][1]},
                    new State(RIPPER2_JUMP_R0, SF_QUICK_CALL, DoRipper2BeginJumpAttack),
                    // s_Ripper2JumpAttack[0][2]},
                    new State(RIPPER2_JUMP_R0 + 2, RIPPER2_JUMP_ATTACK_RATE, DoRipper2MoveJump),
                    // s_Ripper2JumpAttack[0][3]},
                    new State(RIPPER2_JUMP_R0 + 1, RIPPER2_JUMP_ATTACK_RATE, DoRipper2MoveJump).setNext(),
                    // s_Ripper2JumpAttack[0][3]},
            }, {new State(RIPPER2_JUMP_R1, RIPPER2_JUMP_ATTACK_RATE, NullRipper2),
            // s_Ripper2JumpAttack[1][1]},
            new State(RIPPER2_JUMP_R1, SF_QUICK_CALL, DoRipper2BeginJumpAttack),
            // s_Ripper2JumpAttack[1][2]},
            new State(RIPPER2_JUMP_R1 + 2, RIPPER2_JUMP_ATTACK_RATE, DoRipper2MoveJump),
            // s_Ripper2JumpAttack[1][3]},
            new State(RIPPER2_JUMP_R1 + 1, RIPPER2_JUMP_ATTACK_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2JumpAttack[1][3]},
    }, {new State(RIPPER2_JUMP_R2, RIPPER2_JUMP_ATTACK_RATE, NullRipper2),
            // s_Ripper2JumpAttack[2][1]},
            new State(RIPPER2_JUMP_R2, SF_QUICK_CALL, DoRipper2BeginJumpAttack),
            // s_Ripper2JumpAttack[2][2]},
            new State(RIPPER2_JUMP_R2 + 2, RIPPER2_JUMP_ATTACK_RATE, DoRipper2MoveJump),
            // s_Ripper2JumpAttack[2][3]},
            new State(RIPPER2_JUMP_R1 + 1, RIPPER2_JUMP_ATTACK_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2JumpAttack[2][3]},
    }, {new State(RIPPER2_JUMP_R3, RIPPER2_JUMP_ATTACK_RATE, NullRipper2),
            // s_Ripper2JumpAttack[3][1]},
            new State(RIPPER2_JUMP_R3, SF_QUICK_CALL, DoRipper2BeginJumpAttack),
            // s_Ripper2JumpAttack[3][2]},
            new State(RIPPER2_JUMP_R3 + 2, RIPPER2_JUMP_ATTACK_RATE, DoRipper2MoveJump),
            // s_Ripper2JumpAttack[3][3]},
            new State(RIPPER2_JUMP_R1 + 1, RIPPER2_JUMP_ATTACK_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2JumpAttack[3][3]},
    }, {new State(RIPPER2_JUMP_R4, RIPPER2_JUMP_ATTACK_RATE, NullRipper2),
            // s_Ripper2JumpAttack[4][1]},
            new State(RIPPER2_JUMP_R4, SF_QUICK_CALL, DoRipper2BeginJumpAttack),
            // s_Ripper2JumpAttack[4][2]},
            new State(RIPPER2_JUMP_R4 + 2, RIPPER2_JUMP_ATTACK_RATE, DoRipper2MoveJump),
            // s_Ripper2JumpAttack[4][3]},
            new State(RIPPER2_JUMP_R1 + 1, RIPPER2_JUMP_ATTACK_RATE, DoRipper2MoveJump).setNext(),
            // s_Ripper2JumpAttack[4][3]},
    }};

//////////////////////
//
//RIPPER2 HANG_JUMP
//
//////////////////////

    private static void DoRipper2StandHeart(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        NullRipper2(SpriteNum);

        if (riphearthandle != null && !riphearthandle.isActive()) {
            riphearthandle = PlaySound(DIGI_RIPPER2HEARTOUT, sp, v3df_none);
        }

        if ((u.WaitTics -= ACTORMOVETICS) <= 0) {
            NewStateGroup(SpriteNum, sg_Ripper2Run);
        }
    }

    public static void Ripper2Hatch(int Weapon) {
        Sprite wp = boardService.getSprite(Weapon);
        if (wp == null) {
            return;
        }

        Sprite np;

        int rip_ang = RANDOM_P2(2048);

        int newsp = COVERinsertsprite(wp.getSectnum(),
                STAT_DEFAULT);
        np = boardService.getSprite(newsp);
        if (np == null) {
            return;
        }

        np.reset();

        np.setSectnum(wp.getSectnum());
        np.setStatnum(STAT_DEFAULT);
        np.setX(wp.getX());
        np.setY(wp.getY());
        np.setZ(wp.getZ());
        np.setOwner(-1);
        // np.xrepeat = np.yrepeat = 36;
        np.setXrepeat(64);
        np.setYrepeat(64);
        np.setAng(rip_ang);
        np.setPal(0);
        np.setShade(-10);
        SetupRipper2(newsp);
        USER nu = getUser(newsp);
        if (nu == null) {
            return;
        }

        // make immediately active
        nu.Flags |= (SPR_ACTIVE);

        NewStateGroup(newsp, nu.ActorActionSet.Jump);
        nu.ActorActionFunc = DoActorMoveJump;
        DoActorSetSpeed(newsp, FAST_SPEED);
        PickJumpMaxSpeed(newsp, -600);

        nu.Flags |= (SPR_JUMPING);
        nu.Flags &= ~(SPR_FALLING);

        nu.jump_grav = 8;

        // if I didn't do this here they get stuck in the air sometimes
        DoActorZrange(newsp);

        DoJump(newsp);
    }    private static final State[][] s_Ripper2HangJump = {
            {new State(RIPPER2_JUMP_R0, RIPPER2_HANG_JUMP_RATE, NullRipper2),
                    // s_Ripper2HangJump[0][1]},
                    new State(RIPPER2_JUMP_R0 + 1, RIPPER2_HANG_JUMP_RATE, DoRipper2HangJF).setNext(),
                    // s_Ripper2HangJump[0][1]},
            }, {new State(RIPPER2_JUMP_R1, RIPPER2_HANG_JUMP_RATE, NullRipper2),
            // s_Ripper2HangJump[1][1]},
            new State(RIPPER2_JUMP_R1 + 1, RIPPER2_HANG_JUMP_RATE, DoRipper2HangJF).setNext(),
            // s_Ripper2HangJump[1][1]},
    }, {new State(RIPPER2_JUMP_R2, RIPPER2_HANG_JUMP_RATE, NullRipper2),
            // s_Ripper2HangJump[2][1]},
            new State(RIPPER2_JUMP_R2 + 1, RIPPER2_HANG_JUMP_RATE, DoRipper2HangJF).setNext(),
            // s_Ripper2HangJump[2][1]},
    }, {new State(RIPPER2_JUMP_R3, RIPPER2_HANG_JUMP_RATE, NullRipper2),
            // s_Ripper2HangJump[3][1]},
            new State(RIPPER2_JUMP_R3 + 1, RIPPER2_HANG_JUMP_RATE, DoRipper2HangJF).setNext(),
            // s_Ripper2HangJump[3][1]},
    }, {new State(RIPPER2_JUMP_R4, RIPPER2_HANG_JUMP_RATE, NullRipper2),
            // s_Ripper2HangJump[4][1]},
            new State(RIPPER2_JUMP_R4 + 1, RIPPER2_HANG_JUMP_RATE, DoRipper2HangJF).setNext(),
            // s_Ripper2HangJump[4][1]},
    }};

//////////////////////
//
//RIPPER2 HANG_FALL
//
//////////////////////

    public static void DoRipper2Move(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (sp.getHitag() == TAG_SWARMSPOT && sp.getLotag() == 1) {
            DoCheckSwarm(SpriteNum);
        }

        if (u.scale_speed != 0) {
            DoScaleSprite(SpriteNum);
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoJump(SpriteNum);
            } else {
                DoFall(SpriteNum);
            }
        }

        // if on a player/enemy sprite jump quickly
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (DoRipper2QuickJump(SpriteNum) != 0) {
                return;
            }

            KeepActorOnFloor(SpriteNum);
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
    }

    private static final State[][] s_Ripper2HangFall = {
            {new State(RIPPER2_FALL_R0, RIPPER2_FALL_RATE, DoRipper2HangJF).setNext(),
                    // s_Ripper2HangFall[0][0]},
            }, {new State(RIPPER2_FALL_R1, RIPPER2_FALL_RATE, DoRipper2HangJF).setNext(),
            // s_Ripper2HangFall[1][0]},
    }, {new State(RIPPER2_FALL_R2, RIPPER2_FALL_RATE, DoRipper2HangJF).setNext(),
            // s_Ripper2HangFall[2][0]},
    }, {new State(RIPPER2_FALL_R3, RIPPER2_FALL_RATE, DoRipper2HangJF).setNext(),
            // s_Ripper2HangFall[3][0]},
    }, {new State(RIPPER2_FALL_R4, RIPPER2_FALL_RATE, DoRipper2HangJF).setNext(),
            // s_Ripper2HangFall[4][0]},
    }};

    public static void InitRipper2Charge(int SpriteNum) {
        DoActorSetSpeed(SpriteNum, FAST_SPEED);

        InitActorMoveCloser.animatorInvoke(SpriteNum);

        NewStateGroup(SpriteNum, sg_Ripper2RunFast);
    }

    public static void ChestRipper2(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);

        PlaySound(DIGI_RIPPER2CHEST, sp, v3df_follow);
    }

    public static void InitRipperSlash(int SpriteNum) {
        USER u = getUser(SpriteNum), hu;
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }
        Sprite hp;
        short stat;

        PlaySound(DIGI_RIPPER2ATTACK, sp, v3df_none);

        for (stat = 0; stat < StatDamageList.length; stat++) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(StatDamageList[stat]); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                hp = node.get();
                hu = getUser(i);

                if (i == SpriteNum) {
                    break;
                }

                if (u != null && hu != null && FindDistance3D(sp.getX() - hp.getX(),
                        sp.getY() - hp.getY(),
                        (sp.getZ() - hp.getZ()) >> 4) > hu.Radius + u.Radius) {
                    continue;
                }

                if (DISTANCE(hp.getX(),
                        hp.getY(),
                        sp.getX(),
                        sp.getY()) < CLOSE_RANGE_DIST_FUDGE(sp, hp, 600) && FACING_RANGE(hp, sp, 150)) {
                    DoDamage(i, SpriteNum);
                }
            }
        }
    }

    public static void Ripper2Saveable() {
        SaveData(InitRipperSlash);
        SaveData(InitRipper2Hang);
        SaveData(DoRipper2Hang);
        SaveData(DoRipper2HangJF);

        SaveData(DoRipper2BeginJumpAttack);
        SaveData(DoRipper2MoveJump);
        SaveData(NullRipper2);
        SaveData(DoRipper2Pain);
        SaveData(DoRipper2StandHeart);
        SaveData(DoRipper2Move);
        SaveData(InitRipper2Charge);
        SaveData(ChestRipper2);

        SaveData(Ripper2Personality);

        SaveData(Ripper2Attrib);

        SaveData(s_Ripper2Run);
        SaveGroup(sg_Ripper2Run);
        SaveData(s_Ripper2RunFast);
        SaveGroup(sg_Ripper2RunFast);
        SaveData(s_Ripper2Stand);
        SaveGroup(sg_Ripper2Stand);
        SaveData(s_Ripper2Swipe);
        SaveGroup(sg_Ripper2Swipe);
        SaveData(s_Ripper2Kong);
        SaveGroup(sg_Ripper2Kong);
        SaveData(s_Ripper2Heart);
        SaveGroup(sg_Ripper2Heart);
        SaveData(s_Ripper2Hang);
        SaveGroup(sg_Ripper2Hang);
        SaveData(s_Ripper2Pain);
        SaveGroup(sg_Ripper2Pain);
        SaveData(s_Ripper2Jump);
        SaveGroup(sg_Ripper2Jump);
        SaveData(s_Ripper2Fall);
        SaveGroup(sg_Ripper2Fall);
        SaveData(s_Ripper2JumpAttack);
        SaveGroup(sg_Ripper2JumpAttack);
        SaveData(s_Ripper2HangJump);
        SaveGroup(sg_Ripper2HangJump);
        SaveData(s_Ripper2HangFall);
        SaveGroup(sg_Ripper2HangFall);
        SaveData(s_Ripper2Die);
        SaveData(s_Ripper2Dead);
        SaveGroup(sg_Ripper2Die);
        SaveGroup(sg_Ripper2Dead);
        SaveData(s_Ripper2DeathJump);
        SaveData(s_Ripper2DeathFall);
        SaveGroup(sg_Ripper2DeathJump);
        SaveGroup(sg_Ripper2DeathFall);

        SaveData(Ripper2ActionSet);
        SaveData(Ripper2BrownActionSet);
    }

    private static final EnemyStateGroup sg_Ripper2Stand = new EnemyStateGroup(s_Ripper2Stand[0], s_Ripper2Stand[1], s_Ripper2Stand[2], s_Ripper2Stand[3], s_Ripper2Stand[4]);
    private static final EnemyStateGroup sg_Ripper2Run = new EnemyStateGroup(s_Ripper2Run[0], s_Ripper2Run[1], s_Ripper2Run[2], s_Ripper2Run[3], s_Ripper2Run[4]);
    private static final EnemyStateGroup sg_Ripper2RunFast = new EnemyStateGroup(s_Ripper2RunFast[0], s_Ripper2RunFast[1], s_Ripper2RunFast[2], s_Ripper2RunFast[3], s_Ripper2RunFast[4]);
    private static final EnemyStateGroup sg_Ripper2Jump = new EnemyStateGroup(s_Ripper2Jump[0], s_Ripper2Jump[1], s_Ripper2Jump[2], s_Ripper2Jump[3], s_Ripper2Jump[4]);
    private static final EnemyStateGroup sg_Ripper2Swipe = new EnemyStateGroup(s_Ripper2Swipe[0], s_Ripper2Swipe[1], s_Ripper2Swipe[2], s_Ripper2Swipe[3], s_Ripper2Swipe[4]);
    private static final EnemyStateGroup sg_Ripper2Kong = new EnemyStateGroup(s_Ripper2Kong[0], s_Ripper2Kong[1], s_Ripper2Kong[2], s_Ripper2Kong[3], s_Ripper2Kong[4]);
    private static final EnemyStateGroup sg_Ripper2Heart = new EnemyStateGroup(s_Ripper2Heart[0], s_Ripper2Heart[1], s_Ripper2Heart[2], s_Ripper2Heart[3], s_Ripper2Heart[4]);

    private static final EnemyStateGroup sg_Ripper2Fall = new EnemyStateGroup(s_Ripper2Fall[0], s_Ripper2Fall[1], s_Ripper2Fall[2], s_Ripper2Fall[3], s_Ripper2Fall[4]);
    private static final EnemyStateGroup sg_Ripper2JumpAttack = new EnemyStateGroup(s_Ripper2JumpAttack[0], s_Ripper2JumpAttack[1], s_Ripper2JumpAttack[2], s_Ripper2JumpAttack[3], s_Ripper2JumpAttack[4]);
    private static final EnemyStateGroup sg_Ripper2HangJump = new EnemyStateGroup(s_Ripper2HangJump[0], s_Ripper2HangJump[1], s_Ripper2HangJump[2], s_Ripper2HangJump[3], s_Ripper2HangJump[4]);
    private static final EnemyStateGroup sg_Ripper2HangFall = new EnemyStateGroup(s_Ripper2HangFall[0], s_Ripper2HangFall[1], s_Ripper2HangFall[2], s_Ripper2HangFall[3], s_Ripper2HangFall[4]);
    private static final EnemyStateGroup sg_Ripper2Hang = new EnemyStateGroup(s_Ripper2Hang[0], s_Ripper2Hang[1], s_Ripper2Hang[2], s_Ripper2Hang[3], s_Ripper2Hang[4]);






    private static final Actor_Action_Set Ripper2ActionSet = new Actor_Action_Set(sg_Ripper2Stand,
            sg_Ripper2Run, sg_Ripper2Jump, sg_Ripper2Fall, null, // sg_Ripper2Crawl,
            null, // sg_Ripper2Swim,
            null, // sg_Ripper2Fly,
            null, // sg_Ripper2Rise,
            null, // sg_Ripper2Sit,
            null, // sg_Ripper2Look,
            null, // climb
            sg_Ripper2Pain, sg_Ripper2Die, null, // sg_Ripper2HariKari,
            sg_Ripper2Dead, sg_Ripper2DeathJump,
            sg_Ripper2DeathFall, new StateGroup[]{sg_Ripper2Swipe},
            new short[]{1024},
            new StateGroup[]{sg_Ripper2JumpAttack, sg_Ripper2Kong},
            new short[]{500, 1024},
            new StateGroup[]{sg_Ripper2Heart, sg_Ripper2Hang}, null, null);

    private static final Actor_Action_Set Ripper2BrownActionSet = new Actor_Action_Set(
            sg_Ripper2Stand, sg_Ripper2Run, sg_Ripper2Jump,
            sg_Ripper2Fall, null, // sg_Ripper2Crawl,
            null, // sg_Ripper2Swim,
            null, // sg_Ripper2Fly,
            null, // sg_Ripper2Rise,
            null, // sg_Ripper2Sit,
            null, // sg_Ripper2Look,
            null, // climb
            sg_Ripper2Pain, // pain
            sg_Ripper2Die, null, // sg_Ripper2HariKari,
            sg_Ripper2Dead, sg_Ripper2DeathJump,
            sg_Ripper2DeathFall, new StateGroup[]{sg_Ripper2Swipe},
            new short[]{1024},
            new StateGroup[]{sg_Ripper2JumpAttack, sg_Ripper2Kong},
            new short[]{400, 1024},
            new StateGroup[]{sg_Ripper2Heart, sg_Ripper2Hang}, null, null);
}
