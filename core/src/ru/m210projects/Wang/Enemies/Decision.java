package ru.m210projects.Wang.Enemies;

import ru.m210projects.Wang.Type.Animator;

public class Decision {
    public final int range;
    public final Animator action;

    public Decision(int range, Animator action) {
        this.range =  range;
        this.action = action;
    }
}
