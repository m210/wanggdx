package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Actor;
import ru.m210projects.Wang.Type.*;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Enemies.Ripper.PickJumpMaxSpeed;
import static ru.m210projects.Wang.Game.Global_PLock;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JWeapon.InitBloodSpray;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.cfg;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Palette.*;
import static ru.m210projects.Wang.Player.QueueFloorBlood;
import static ru.m210projects.Wang.Rooms.COVERinsertsprite;
import static ru.m210projects.Wang.Rooms.FAFcansee;
import static ru.m210projects.Wang.Shrap.SpawnShrap;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Bunny {

    public static final int BUNNY_RUN_RATE = 10;
    public static final int BUNNY_STAND_RATE = 12;
    public static final int BUNNY_SCREW_RATE = 16;
    public static final int BUNNY_SWIPE_RATE = 8;
    public static final Animator NullBunny = new Animator((Animator.Runnable) Bunny::NullBunny);
    public static final int BUNNY_HEART_RATE = 14;
    public static final int BUNNY_PAIN_RATE = 38;
    public static final Animator DoBunnyPain = new Animator((Animator.Runnable) Bunny::DoBunnyPain);
    public static final int BUNNY_JUMP_RATE = 25;
    public static final Animator DoBunnyMoveJump = new Animator((Animator.Runnable) Bunny::DoBunnyMoveJump);
    public static final int BUNNY_FALL_RATE = 25;

    public static final Animator DoBunnyBeginJumpAttack = new Animator((Animator.Runnable) Bunny::DoBunnyBeginJumpAttack);
    public static final int BUNNY_DIE_RATE = 16;
    public static final Animator BunnySpew = new Animator((Animator.Runnable) Bunny::BunnySpew);
    public static final int BUNNY_DEAD_RATE = 8;
    public static final Animator DoActorDeathMove = new Animator((Animator.Runnable) Actor::DoActorDeathMove);
    private static final Decision[] BunnyBattle = {new Decision(748, InitActorMoveCloser), new Decision(750, InitActorAlertNoise), new Decision(760, InitActorAttackNoise), new Decision(1024, InitActorMoveCloser)};
    private static final Decision[] BunnyOffense = {new Decision(600, InitActorMoveCloser),

            new Decision(700, InitActorAlertNoise), new Decision(1024, InitActorMoveCloser)};
    private static final Decision[] BunnyBroadcast = {new Decision(21, InitActorAlertNoise),

            new Decision(51, InitActorAmbientNoise), new Decision(1024, InitActorDecide)};
    private static final Decision[] BunnySurprised = {new Decision(500, InitActorRunAway),

            new Decision(701, InitActorMoveCloser), new Decision(1024, InitActorDecide)};
    private static final Decision[] BunnyEvasive = {new Decision(500, InitActorWanderAround),

            new Decision(1020, InitActorRunAway), new Decision(1024, InitActorAmbientNoise)};
    private static final Decision[] BunnyLostTarget = {new Decision(900, InitActorFindPlayer),

            new Decision(1024, InitActorWanderAround)};
    private static final Decision[] BunnyCloseRange = {new Decision(1024, InitActorAttack)};
    private static final Decision[] BunnyWander = {new Decision(1024, InitActorReposition)};
    private static final Personality WhiteBunnyPersonality = new Personality(BunnyBattle, BunnyOffense, BunnyBroadcast, BunnySurprised, BunnyEvasive, BunnyLostTarget, BunnyCloseRange, BunnyCloseRange);
    private static final Personality BunnyPersonality = new Personality(BunnyEvasive, BunnyEvasive, BunnyEvasive, BunnyWander, BunnyWander, BunnyWander, BunnyEvasive, BunnyEvasive);
    private static final ATTRIBUTE BunnyAttrib = new ATTRIBUTE(new short[]{100, 120, 140, 180}, new short[]{5, 0, -2, -4}, 3, new int[]{DIGI_BUNNYAMBIENT, 0, DIGI_BUNNYATTACK, DIGI_BUNNYATTACK, DIGI_BUNNYDIE2, 0, 0, 0, 0, 0});
    private static final ATTRIBUTE WhiteBunnyAttrib = new ATTRIBUTE(new short[]{200, 220, 340, 380}, new short[]{5, 0, -2, -4}, 3, new int[]{DIGI_BUNNYAMBIENT, 0, DIGI_BUNNYATTACK, DIGI_BUNNYATTACK, DIGI_BUNNYDIE2, 0, 0, 0, 0, 0});
    private static final State[][] s_BunnyPain = {{new State(BUNNY_SWIPE_R0, BUNNY_PAIN_RATE, DoBunnyPain).setNext(),

    }, {new State(BUNNY_SWIPE_R0, BUNNY_PAIN_RATE, DoBunnyPain).setNext(),

    }, {new State(BUNNY_SWIPE_R0, BUNNY_PAIN_RATE, DoBunnyPain).setNext(),

    }, {new State(BUNNY_SWIPE_R0, BUNNY_PAIN_RATE, DoBunnyPain).setNext(),

    }, {new State(BUNNY_SWIPE_R0, BUNNY_PAIN_RATE, DoBunnyPain).setNext(),

    }};
    private static final EnemyStateGroup sg_BunnyPain = new EnemyStateGroup(s_BunnyPain[0], s_BunnyPain[1], s_BunnyPain[2], s_BunnyPain[3], s_BunnyPain[4]);
    private static final State[][] s_BunnyJump = {{new State(BUNNY_RUN_R0 + 1, BUNNY_JUMP_RATE, DoBunnyMoveJump),

            new State(BUNNY_RUN_R0 + 2, BUNNY_JUMP_RATE, DoBunnyMoveJump).setNext(),

    }, {new State(BUNNY_RUN_R1 + 1, BUNNY_JUMP_RATE, DoBunnyMoveJump),

            new State(BUNNY_RUN_R1 + 2, BUNNY_JUMP_RATE, DoBunnyMoveJump).setNext(),

    }, {new State(BUNNY_RUN_R2 + 1, BUNNY_JUMP_RATE, DoBunnyMoveJump),

            new State(BUNNY_RUN_R2 + 2, BUNNY_JUMP_RATE, DoBunnyMoveJump).setNext(),

    }, {new State(BUNNY_RUN_R3 + 1, BUNNY_JUMP_RATE, DoBunnyMoveJump),

            new State(BUNNY_RUN_R3 + 2, BUNNY_JUMP_RATE, DoBunnyMoveJump).setNext(),

    }, {new State(BUNNY_RUN_R4 + 1, BUNNY_JUMP_RATE, DoBunnyMoveJump),

            new State(BUNNY_RUN_R4 + 2, BUNNY_JUMP_RATE, DoBunnyMoveJump).setNext(),

    }};
    private static final EnemyStateGroup sg_BunnyJump = new EnemyStateGroup(s_BunnyJump[0], s_BunnyJump[1], s_BunnyJump[2], s_BunnyJump[3], s_BunnyJump[4]);
    private static final State[][] s_BunnyFall = {{new State(BUNNY_RUN_R0 + 3, BUNNY_FALL_RATE, DoBunnyMoveJump).setNext(),

    }, {new State(BUNNY_RUN_R1 + 3, BUNNY_FALL_RATE, DoBunnyMoveJump).setNext(),

    }, {new State(BUNNY_RUN_R2 + 3, BUNNY_FALL_RATE, DoBunnyMoveJump).setNext(),

    }, {new State(BUNNY_RUN_R3 + 3, BUNNY_FALL_RATE, DoBunnyMoveJump).setNext(),

    }, {new State(BUNNY_RUN_R4 + 3, BUNNY_FALL_RATE, DoBunnyMoveJump).setNext(),

    }};
    private static final EnemyStateGroup sg_BunnyFall = new EnemyStateGroup(s_BunnyFall[0], s_BunnyFall[1], s_BunnyFall[2], s_BunnyFall[3], s_BunnyFall[4]);
    private static final State[] s_BunnyDie = {new State(BUNNY_DIE, BUNNY_DIE_RATE, NullBunny),

            new State(BUNNY_DIE, SF_QUICK_CALL, BunnySpew),

            new State(BUNNY_DIE + 1, BUNNY_DIE_RATE, NullBunny),

            new State(BUNNY_DIE + 2, BUNNY_DIE_RATE, NullBunny),

            new State(BUNNY_DIE + 2, BUNNY_DIE_RATE, NullBunny),

            new State(BUNNY_DEAD, BUNNY_DIE_RATE, DoActorDebris).setNext(),

    };
    private static final EnemyStateGroup sg_BunnyDie = new EnemyStateGroup(s_BunnyDie);
    private static final State[] s_BunnyDead = {new State(BUNNY_DIE, BUNNY_DEAD_RATE, null),

            new State(BUNNY_DIE, SF_QUICK_CALL, BunnySpew),

            new State(BUNNY_DIE + 1, BUNNY_DEAD_RATE, null),

            new State(BUNNY_DIE + 2, BUNNY_DEAD_RATE, null),

            new State(BUNNY_DEAD, SF_QUICK_CALL, QueueFloorBlood),

            new State(BUNNY_DEAD, BUNNY_DEAD_RATE, DoActorDebris).setNext()
    };
    private static final EnemyStateGroup sg_BunnyDead = new EnemyStateGroup(s_BunnyDead);
    private static final State[] s_BunnyDeathJump = {new State(BUNNY_DIE, BUNNY_DIE_RATE, DoActorDeathMove).setNext()};
    private static final EnemyStateGroup sg_BunnyDeathJump = new EnemyStateGroup(s_BunnyDeathJump);
    private static final State[] s_BunnyDeathFall = {new State(BUNNY_DIE + 1, BUNNY_DIE_RATE, DoActorDeathMove).setNext()};
    private static final EnemyStateGroup sg_BunnyDeathFall = new EnemyStateGroup(s_BunnyDeathFall);        public static int Bunny_Count = 0;
    public static void InitBunnyStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{sg_BunnyStand, sg_BunnyRun, sg_BunnyJump, sg_BunnyFall, sg_BunnyPain, sg_BunnyDie, sg_BunnyDead, sg_BunnyDeathJump, sg_BunnyDeathFall, sg_BunnyHeart, sg_BunnySwipe, sg_BunnyScrew}) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }public static final Animator DoBunnyMove = new Animator((Animator.Runnable) Bunny::DoBunnyMove);

    public static void SetupBunny(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u;
        if (TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, BUNNY_RUN_R0, s_BunnyRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = 10;
        }

        Bunny_Count++;

        ChangeState(SpriteNum, s_BunnyRun[0][0]);
        u.StateEnd = s_BunnyDie[0];
        u.setRot(sg_BunnyRun);
        u.ShellNum = 0; // Not Pregnant right now
        u.FlagOwner = 0;

        sp.setClipdist((150) >> 2);

        if (sp.getPal() == PALETTE_PLAYER1) {
            EnemyDefaults(SpriteNum, BunnyWhiteActionSet, WhiteBunnyPersonality);
            u.Attrib = WhiteBunnyAttrib;
            sp.setXrepeat(96);
            sp.setYrepeat(90);

            sp.setClipdist(200 >> 2);

            if (!TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
                u.Health = 60;
            }
        } else if (sp.getPal() == PALETTE_PLAYER8) // Male Rabbit
        {
            EnemyDefaults(SpriteNum, BunnyActionSet, BunnyPersonality);
            u.Attrib = BunnyAttrib;

            // sp.shade = 0; // darker
            if (!TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
                u.Health = 20;
            }
            u.Flag1 = 0;
        } else { // Female Rabbit
            EnemyDefaults(SpriteNum, BunnyActionSet, BunnyPersonality);
            u.Attrib = BunnyAttrib;
            u.spal = PALETTE_PLAYER0;
            sp.setPal(PALETTE_PLAYER0);
            u.Flag1 = SEC(5);
            // sp.shade = 0; // darker
        }

        DoActorSetSpeed(SpriteNum, FAST_SPEED);

        u.Flags |= (SPR_XFLIP_TOGGLE);

        u.zclip = Z(16);
        u.floor_dist =  Z(8);
        u.ceiling_dist =  Z(8);
        u.lo_step =  Z(16);
    }

    private static int RANDOM_NEG() {
        return (RANDOM_P2((16384) << 1) - (16384));
    }

    private static void DoBunnyBeginJumpAttack(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite psp = boardService.getSprite(u.tgt_sp);
        if (psp == null) {
            return;
        }

        int tang = EngineUtils.getAngle(psp.getX() - sp.getX(), psp.getY() - sp.getY());

        if (move_sprite(SpriteNum, EngineUtils.sin(NORM_ANGLE(tang + 512)) >> 7, EngineUtils.sin(tang) >> 7, 0, u.ceiling_dist, u.floor_dist, CLIPMASK_ACTOR, ACTORMOVETICS) != 0) {
            sp.setAng( (NORM_ANGLE(sp.getAng() + 1024) + (RANDOM_NEG() >> 6)));
        } else {
            sp.setAng(NORM_ANGLE(tang + (RANDOM_NEG() >> 6)));
        }

        DoActorSetSpeed(SpriteNum, FAST_SPEED);

        PickJumpMaxSpeed(SpriteNum, -400); // was -800

        u.Flags |= (SPR_JUMPING);
        u.Flags &= ~(SPR_FALLING);

        // set up individual actor jump gravity
        u.jump_grav = 17; // was 8

        // if I didn't do this here they get stuck in the air sometimes
        DoActorZrange(SpriteNum);

        DoJump(SpriteNum);
    }

    private static void DoBunnyMoveJump(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            int nx, ny;

            // Move while jumping
            nx = sp.getXvel() * EngineUtils.sin(NORM_ANGLE(sp.getAng() + 512)) >> 14;
            ny = sp.getXvel() * EngineUtils.sin(sp.getAng()) >> 14;

            move_actor(SpriteNum, nx, ny, 0);

            if (TEST(u.Flags, SPR_JUMPING)) {
                DoActorJump(SpriteNum);
            } else {
                DoActorFall(SpriteNum);
            }
        }

        DoActorZrange(SpriteNum);

        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            InitActorDecide(SpriteNum);
        }
    }

    private static void DoPickCloseBunny(int SpriteNum) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int dist, near_dist = 1000;

        // if actor can still see the player
        int look_height = SPRITEp_TOS(sp);
        boolean ICanSee;

        ListNode<Sprite> nexti;
        for (ListNode<Sprite> node = boardService.getStatNode(STAT_ENEMY); node != null; node = nexti) {
            int i = node.getIndex();
            nexti = node.getNext();
            Sprite tsp = node.get();
            USER tu = getUser(i);

            if (tu == null || sp == tsp) {
                continue;
            }

            if (tu.ID != BUNNY_RUN_R0) {
                continue;
            }

            dist = DISTANCE(tsp.getX(), tsp.getY(), sp.getX(), sp.getY());

            if (dist > near_dist) {
                continue;
            }

            ICanSee = FAFcansee(sp.getX(), sp.getY(), look_height, sp.getSectnum(), tsp.getX(), tsp.getY(), SPRITEp_UPPER(tsp), tsp.getSectnum());

            if (ICanSee && dist < near_dist && tu.ID == BUNNY_RUN_R0) {
                u.tgt_sp = u.lo_sp = i;
                return;
            }
        }
    }

    private static void DoBunnyQuickJump(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (u.spal != PALETTE_PLAYER8) {
            return;
        }

        if (u.lo_sp == -1 && MoveSkip4 != 0) {
            DoPickCloseBunny(SpriteNum);
        }

        // Random Chance of like sexes fighting
        Sprite tsp = boardService.getSprite(u.lo_sp);
        if (tsp != null) {
            int hitsprite = u.lo_sp;
            USER tu = getUser(hitsprite);

            if (tu == null || tu.ID != BUNNY_RUN_R0) {
                return;
            }

            // Not mature enough yet
            if (sp.getXrepeat() != 64 || sp.getYrepeat() != 64) {
                return;
            }
            if (tsp.getXrepeat() != 64 || tsp.getYrepeat() != 64) {
                return;
            }

            // Kill a rival
            // Only males fight
            if (tu.spal == sp.getPal() && RANDOM_RANGE(1000) > 995) {
                if (u.spal == PALETTE_PLAYER8 && tu.spal == PALETTE_PLAYER8) {
                    PlaySound(DIGI_BUNNYATTACK, sp, v3df_follow);
                    PlaySound(DIGI_BUNNYDIE2, tsp, v3df_follow);
                    tu.Health = 0;

                    // Blood fountains
                    InitBloodSpray(hitsprite, true, -1);

                    if (SpawnShrap(hitsprite, SpriteNum)) {
                        SetSuicide(hitsprite);
                    } else {
                        DoActorDie(hitsprite, SpriteNum);
                    }

                    Bunny_Count--; // Bunny died

                    u.lo_sp = -1;
                    return;
                }
            }
        }

        // Get layed!
        if (tsp != null && u.spal == PALETTE_PLAYER8) // Only males check this
        {
            int hitsprite = u.lo_sp;
            USER tu = getUser(hitsprite);

            if (tu == null || tu.ID != BUNNY_RUN_R0) {
                return;
            }

            // Not mature enough to mate yet
            if (sp.getXrepeat() != 64 || sp.getYrepeat() != 64) {
                return;
            }
            if (tsp.getXrepeat() != 64 || tsp.getYrepeat() != 64) {
                return;
            }

            if (tu.ShellNum <= 0 && tu.WaitTics <= 0 && u.WaitTics <= 0) {
                if (TEST(tsp.getExtra(), SPRX_PLAYER_OR_ENEMY)) {
                    PlayerStr pp = null;

                    if (RANDOM_RANGE(1000) < 995 && tu.spal != PALETTE_PLAYER0) {
                        return;
                    }

                    DoActorPickClosePlayer(SpriteNum);

                    USER tgu = getUser(u.tgt_sp);
                    if (tgu != null && tgu.PlayerP != -1) {
                        pp = Player[tgu.PlayerP];
                    }

                    if (tu.spal != PALETTE_PLAYER0) {
                        if (tu.Flag1 > 0) {
                            return;
                        }
                        tu.FlagOwner = 1; // FAG!
                        tu.Flag1 = SEC(10);
                        if (pp != null) {
                            int choose_snd;
                            int[] fagsnds = {DIGI_FAGRABBIT1, DIGI_FAGRABBIT2, DIGI_FAGRABBIT3};

                            if (pp == Player[myconnectindex]) {
                                choose_snd =  (STD_RANDOM_RANGE(2 << 8) >> 8);
                                Sprite tgt = boardService.getSprite(u.tgt_sp);
                                if (tgt != null && FAFcansee(sp.getX(), sp.getY(), SPRITEp_TOS(sp), sp.getSectnum(), pp.posx, pp.posy, pp.posz, pp.cursectnum) && FACING(sp, tgt)) {
                                    PlayerSound(fagsnds[choose_snd], v3df_doppler | v3df_follow | v3df_dontpan, pp);
                                }
                            }
                        }
                    } else {
                        if (pp != null && RANDOM_RANGE(1000) > 200) {
                            int choose_snd;
                            int[] straightsnds = {DIGI_RABBITHUMP1, DIGI_RABBITHUMP2, DIGI_RABBITHUMP3, DIGI_RABBITHUMP4};

                            if (pp == Player[myconnectindex]) {
                                choose_snd =  (STD_RANDOM_RANGE(3 << 8) >> 8);
                                Sprite tgt = boardService.getSprite(u.tgt_sp);

                                if (tgt != null && FAFcansee(sp.getX(), sp.getY(), SPRITEp_TOS(sp), sp.getSectnum(), pp.posx, pp.posy, pp.posz, pp.cursectnum) && FACING(sp, tgt)) {
                                    PlayerSound(straightsnds[choose_snd], v3df_doppler | v3df_follow | v3df_dontpan, pp);
                                }
                            }
                        }
                    }

                    sp.setX(tsp.getX()); // Mount up little bunny
                    sp.setY(tsp.getY());
                    sp.setAng(tsp.getAng());
                    sp.setAng(NORM_ANGLE(sp.getAng() + 1024));
                    HelpMissileLateral(SpriteNum, 2000);
                    sp.setAng(tsp.getAng());
                    u.Vis = sp.getAng(); // Remember angles for later
                    tu.Vis = tsp.getAng();

                    NewStateGroup(SpriteNum, sg_BunnyScrew);
                    NewStateGroup(hitsprite, sg_BunnyScrew);
                    if (cfg.ParentalLock || Global_PLock) {
                        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_INVISIBLE)); // Turn em' invisible
                        tsp.setCstat(tsp.getCstat() | (CSTAT_SPRITE_INVISIBLE)); // Turn em' invisible
                    }
                    u.WaitTics = tu.WaitTics =  SEC(10); // Mate for this int
                }
            }
        }

    }

    private static void NullBunny(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoActorJump(SpriteNum);
            } else {
                DoActorFall(SpriteNum);
            }
        }

        // stay on floor unless doing certain things
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            KeepActorOnFloor(SpriteNum);
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
    }

    private static void DoBunnyPain(int SpriteNum) {
        USER u = getUser(SpriteNum);

        NullBunny(SpriteNum);

        if (u != null && (u.WaitTics -= ACTORMOVETICS) <= 0) {
            InitActorDecide(SpriteNum);
        }
    }

    public static void DoBunnyRipHeart(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);

        NewStateGroup(SpriteNum, sg_BunnyHeart);
        u.WaitTics = 6 * 120;

        // player face bunny
        if (tsp != null) {
            tsp.setAng(EngineUtils.getAngle(sp.getX() - tsp.getX(), sp.getY() - tsp.getY()));
        }
    }

    private static void DoBunnyStandKill(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        NullBunny(SpriteNum);

        // Growl like the bad ass bunny you are!
        if (RANDOM_RANGE(1000) > 800) {
            PlaySound(DIGI_BUNNYATTACK, sp, v3df_none);
        }

        if ((u.WaitTics -= ACTORMOVETICS) <= 0) {
            NewStateGroup(SpriteNum, sg_BunnyRun);
        }
    }

    private static void BunnyHatch(int Weapon) {
        Sprite wp = boardService.getSprite(Weapon);
        USER wu = getUser(Weapon);
        if (wp == null) {
            return;
        }

        int newsp;
        Sprite np;
        USER nu;

        int rip_ang;

        rip_ang =  RANDOM_P2(2048);

        newsp = COVERinsertsprite(wp.getSectnum(), STAT_DEFAULT);
        np = boardService.getSprite(newsp);
        if (np == null) {
            return;
        }
        np.reset();

        np.setSectnum(wp.getSectnum());
        np.setStatnum(STAT_DEFAULT);
        np.setX(wp.getX());
        np.setY(wp.getY());
        np.setZ(wp.getZ());
        np.setOwner(-1);
        np.setXrepeat(30); // Baby size
        np.setYrepeat(24);
        np.setAng(rip_ang);
        np.setPal(0);
        SetupBunny(newsp);
        nu = getUser(newsp);
        if (nu == null) {
            return;
        }

        np.setShade(wp.getShade());

        // make immediately active
        nu.Flags |= (SPR_ACTIVE);
        if (RANDOM_RANGE(1000) > 500) // Boy or Girl?
        {
            nu.spal = PALETTE_PLAYER0; // Girl
            np.setPal(PALETTE_PLAYER0);
        } else {
            nu.spal = PALETTE_PLAYER8; // Boy
            np.setPal(PALETTE_PLAYER8);

            // Oops, mommy died giving birth to a boy
            if (RANDOM_RANGE(1000) > 500 && wu != null) {
                wu.Health = 0;
                Bunny_Count--; // Bunny died

                // Blood fountains
                InitBloodSpray(Weapon, true, -1);

                if (SpawnShrap(Weapon, newsp)) {
                    SetSuicide(Weapon);
                } else {
                    DoActorDie(Weapon, newsp);
                }
            }
        }

        nu.ShellNum = 0; // Not Pregnant right now

        NewStateGroup(newsp, nu.ActorActionSet.Jump);
        nu.ActorActionFunc = DoActorMoveJump;
        DoActorSetSpeed(newsp, FAST_SPEED);
        PickJumpMaxSpeed(newsp, -600);

        nu.Flags |= (SPR_JUMPING);
        nu.Flags &= ~(SPR_FALLING);

        nu.jump_grav = 8;

        // if I didn't do this here they get stuck in the air sometimes
        DoActorZrange(newsp);

        DoActorJump(newsp);

    }

    public static int BunnyHatch2(int Weapon) {
        Sprite wp = boardService.getSprite(Weapon);
        if (wp == null) {
            return -1;
        }

        Sprite np;
        USER nu;

        int newsp = COVERinsertsprite(wp.getSectnum(), STAT_DEFAULT);
        np = boardService.getSprite(newsp);
        if (np == null) {
            return -1;
        }

        np.reset();
        np.setSectnum(wp.getSectnum());
        np.setStatnum(STAT_DEFAULT);
        np.setX(wp.getX());
        np.setY(wp.getY());
        np.setZ(wp.getZ());
        np.setOwner(-1);
        np.setXrepeat(30); // Baby size
        np.setYrepeat(24);
        np.setAng( RANDOM_P2(2048));
        np.setPal(0);
        SetupBunny(newsp);
        nu = getUser(newsp);
        if (nu == null) {
            return -1;
        }

        np.setShade(wp.getShade());

        // make immediately active
        nu.Flags |= (SPR_ACTIVE);
        if (RANDOM_RANGE(1000) > 500) // Boy or Girl?
        {
            nu.spal = PALETTE_PLAYER0;
            np.setPal(PALETTE_PLAYER0); // Girl
            nu.Flag1 = SEC(5);
        } else {
            nu.spal = PALETTE_PLAYER8;
            np.setPal(PALETTE_PLAYER8); // Boy
            nu.Flag1 = 0;
        }

        nu.ShellNum = 0; // Not Pregnant right now

        NewStateGroup(newsp, nu.ActorActionSet.Jump);
        nu.ActorActionFunc = DoActorMoveJump;
        DoActorSetSpeed(newsp, FAST_SPEED);
        if (TEST_BOOL3(wp)) {
            PickJumpMaxSpeed(newsp, -600 - RANDOM_RANGE(600));
            np.setXrepeat(64);
            np.setYrepeat(64);
            np.setXvel( (150 + RANDOM_RANGE(1000)));
            nu.Health = 1; // Easy to pop. Like shootn' skeet.
            np.setAng(np.getAng() - RANDOM_RANGE(128));
            np.setAng(np.getAng() + RANDOM_RANGE(128));
        } else {
            PickJumpMaxSpeed(newsp, -600);
        }

        nu.Flags |= (SPR_JUMPING);
        nu.Flags &= ~(SPR_FALLING);

        nu.jump_grav = 8;
        nu.FlagOwner = 0;

        nu.active_range = 75000; // Set it far

        // if I didn't do this here they get stuck in the air sometimes
        DoActorZrange(newsp);

        DoActorJump(newsp);

        return (newsp);
    }

    private static void DoBunnyMove(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        // Parental lock crap
        if (TEST(sp.getCstat(), CSTAT_SPRITE_INVISIBLE)) {
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE)); // Turn em' back on
        }

        // Sometimes they just won't die!
        if (u.Health <= 0) {
            SetSuicide(SpriteNum);
        }

        if (u.scale_speed != 0) {
            DoScaleSprite(SpriteNum);
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoActorJump(SpriteNum);
            } else {
                DoActorFall(SpriteNum);
            }
        }

        // if on a player/enemy sprite jump quickly
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            DoBunnyQuickJump(SpriteNum);
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        // stay on floor unless doing certain things
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            KeepActorOnFloor(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);

        if (RANDOM_RANGE(1000) > 985 && sp.getPal() != PALETTE_PLAYER1 && u.track < 0) {
            Sector sec = boardService.getSector(sp.getSectnum());
            if (sec != null) {
                switch (sec.getFloorpicnum()) {
                    case 153:
                    case 154:
                    case 193:
                    case 219:
                    case 2636:
                    case 2689:
                    case 3561:
                    case 3562:
                    case 3563:
                    case 3564:
                        NewStateGroup(SpriteNum, sg_BunnyStand);
                        break;
                    default:
                        sp.setAng(NORM_ANGLE(RANDOM_RANGE(2048 << 6) >> 6));
                        u.jump_speed = -350;
                        DoActorBeginJump(SpriteNum);
                        u.ActorActionFunc = DoActorMoveJump;
                        break;
                }
            }
        }
    }

    private static void BunnySpew(int SpriteNum) {
        InitBloodSpray(SpriteNum, true, -1);
    }

    private static void DoBunnyEat(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoActorJump(SpriteNum);
            } else {
                DoActorFall(SpriteNum);
            }
        }

        // if on a player/enemy sprite jump quickly
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            DoBunnyQuickJump(SpriteNum);
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        // stay on floor unless doing certain things
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            KeepActorOnFloor(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec != null) {
            switch (sec.getFloorpicnum()) {
                case 153:
                case 154:
                case 193:
                case 219:
                case 2636:
                case 2689:
                case 3561:
                case 3562:
                case 3563:
                case 3564:
                    if (RANDOM_RANGE(1000) > 970) {
                        NewStateGroup(SpriteNum, sg_BunnyRun);
                    }
                    break;
                default:
                    NewStateGroup(SpriteNum, sg_BunnyRun);
                    break;
            }
        }
    }

    private static void DoBunnyScrew(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            if (TEST(u.Flags, SPR_JUMPING)) {
                DoActorJump(SpriteNum);
            } else {
                DoActorFall(SpriteNum);
            }
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        // stay on floor unless doing certain things
        if (!TEST(u.Flags, SPR_JUMPING | SPR_FALLING)) {
            KeepActorOnFloor(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);

        if (RANDOM_RANGE(1000) > 990) // Bunny sex sounds
        {
            if (!cfg.ParentalLock && !Global_PLock) {
                PlaySound(DIGI_BUNNYATTACK, sp, v3df_follow);
            }
        }

        u.WaitTics -= ACTORMOVETICS;

        if ((u.FlagOwner != 0 || u.spal == PALETTE_PLAYER0) && u.WaitTics > 0) // Keep Girl still
        {
            NewStateGroup(SpriteNum, sg_BunnyScrew);
        }

        if (u.spal == PALETTE_PLAYER0 && u.WaitTics <= 0) // Female has baby
        {
            u.Flag1 = SEC(5); // Count down to babies
            u.ShellNum = 1; // She's pregnant now
        }

        if (u.WaitTics <= 0) {
            sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_INVISIBLE)); // Turn em' back on
            u.FlagOwner = 0;
            NewStateGroup(SpriteNum, sg_BunnyRun);
        }
    }

    private static void DoBunnyGrowUp(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (sp.getPal() == PALETTE_PLAYER1) {
            return; // Don't bother white bunnies
        }

        if ((u.Counter -= ACTORMOVETICS) <= 0) {
            int xsiz = sp.getXrepeat() + 1;
            int ysiz = sp.getYrepeat() + 1;
            sp.setXrepeat(xsiz);
            sp.setYrepeat(ysiz);
            if (xsiz > 64) {
                sp.setXrepeat(64);
            }

            if (ysiz > 64) {
                sp.setYrepeat(64);
            }
            u.Counter = 60;
        }

        // Don't go homo too much!
        if (sp.getPal() != PALETTE_PLAYER0 && u.Flag1 > 0) {
            u.Flag1 -= ACTORMOVETICS;
        }

        // Gestation period for female rabbits
        if (sp.getPal() == PALETTE_PLAYER0 && u.ShellNum > 0) {
            if ((u.Flag1 -= ACTORMOVETICS) <= 0) {
                if (Bunny_Count < 20) {
                    PlaySound(DIGI_BUNNYDIE2, sp, v3df_follow);
                    BunnyHatch(SpriteNum); // Baby time
                }
                u.ShellNum = 0; // Not pregnent anymore
            }
        }
    }

    public static void InitBunnySlash(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        PlaySound(DIGI_BUNNYATTACK, sp, v3df_none);

        for (int j : StatDamageList) {
            ListNode<Sprite> nexti;
            for (ListNode<Sprite> node = boardService.getStatNode(j); node != null; node = nexti) {
                int i = node.getIndex();
                nexti = node.getNext();
                Sprite hp = node.get();

                if (i == SpriteNum) {
                    break;
                }

                int dist = DISTANCE(hp.getX(), hp.getY(), sp.getX(), sp.getY());
                if (dist < CLOSE_RANGE_DIST_FUDGE(sp, hp, 600) && FACING_RANGE(hp, sp, 150)) {
                    DoDamage(i, SpriteNum);
                }
            }
        }
    }

    public static void BunnySaveable() {
        SaveData(InitBunnySlash);
        SaveData(DoActorDeathMove);
        SaveData(DoBunnyBeginJumpAttack);
        SaveData(DoBunnyMoveJump);
        SaveData(NullBunny);
        SaveData(DoBunnyPain);
        SaveData(DoBunnyStandKill);
        SaveData(DoBunnyMove);
        SaveData(BunnySpew);
        SaveData(DoBunnyEat);
        SaveData(DoBunnyScrew);
        SaveData(DoBunnyGrowUp);
        SaveData(WhiteBunnyPersonality);
        SaveData(BunnyPersonality);
        SaveData(WhiteBunnyAttrib);
        SaveData(BunnyAttrib);
        SaveData(s_BunnyRun);
        SaveGroup(sg_BunnyRun);
        SaveData(s_BunnyStand);
        SaveGroup(sg_BunnyStand);
        SaveData(s_BunnyScrew);
        SaveGroup(sg_BunnyScrew);
        SaveData(s_BunnySwipe);
        SaveGroup(sg_BunnySwipe);
        SaveData(s_BunnyHeart);
        SaveGroup(sg_BunnyHeart);
        SaveData(s_BunnyPain);
        SaveGroup(sg_BunnyPain);
        SaveData(s_BunnyJump);
        SaveGroup(sg_BunnyJump);
        SaveData(s_BunnyFall);
        SaveGroup(sg_BunnyFall);
        SaveData(s_BunnyDie);
        SaveGroup(sg_BunnyDie);
        SaveData(s_BunnyDead);
        SaveGroup(sg_BunnyDead);
        SaveData(s_BunnyDeathJump);
        SaveGroup(sg_BunnyDeathJump);
        SaveData(s_BunnyDeathFall);
        SaveGroup(sg_BunnyDeathFall);
        SaveData(BunnyActionSet);
        SaveData(BunnyWhiteActionSet);
    }








    public static final Animator DoBunnyGrowUp = new Animator((Animator.Runnable) Bunny::DoBunnyGrowUp);


    private static final State[][] s_BunnyRun = {{new State(BUNNY_RUN_R0, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove), new State(BUNNY_RUN_R0 + 1, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove), new State(BUNNY_RUN_R0 + 2, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove), new State(BUNNY_RUN_R0 + 3, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove), new State(BUNNY_RUN_R0 + 4, SF_QUICK_CALL, DoBunnyGrowUp), new State(BUNNY_RUN_R0 + 4, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),}, {new State(BUNNY_RUN_R1, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R1 + 1, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R1 + 2, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R1 + 3, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R1 + 4, SF_QUICK_CALL, DoBunnyGrowUp),

            new State(BUNNY_RUN_R1 + 4, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

    }, {new State(BUNNY_RUN_R2, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R2 + 1, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R2 + 2, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R2 + 3, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R2 + 4, SF_QUICK_CALL, DoBunnyGrowUp),

            new State(BUNNY_RUN_R2 + 4, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

    }, {new State(BUNNY_RUN_R3, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R3 + 1, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R3 + 2, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R3 + 3, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R3 + 4, SF_QUICK_CALL, DoBunnyGrowUp),

            new State(BUNNY_RUN_R3 + 4, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

    }, {new State(BUNNY_RUN_R4, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R4 + 1, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R4 + 2, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R4 + 3, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

            new State(BUNNY_RUN_R4 + 4, SF_QUICK_CALL, DoBunnyGrowUp),

            new State(BUNNY_RUN_R4 + 4, BUNNY_RUN_RATE | SF_TIC_ADJUST, DoBunnyMove),

    }};


    public static final Animator DoBunnyEat = new Animator((Animator.Runnable) Bunny::DoBunnyEat);


    private static final State[][] s_BunnyStand = {{new State(BUNNY_STAND_R0, BUNNY_STAND_RATE, DoBunnyEat),

            new State(BUNNY_STAND_R0 + 4, SF_QUICK_CALL, DoBunnyGrowUp),

            new State(BUNNY_STAND_R0 + 4, BUNNY_STAND_RATE, DoBunnyEat),

    }, {new State(BUNNY_STAND_R1, BUNNY_STAND_RATE, DoBunnyEat),

            new State(BUNNY_STAND_R1 + 4, SF_QUICK_CALL, DoBunnyGrowUp),

            new State(BUNNY_STAND_R1 + 4, BUNNY_STAND_RATE, DoBunnyEat),

    }, {new State(BUNNY_STAND_R2, BUNNY_STAND_RATE, DoBunnyEat),

            new State(BUNNY_STAND_R2 + 4, SF_QUICK_CALL, DoBunnyGrowUp),

            new State(BUNNY_STAND_R2 + 4, BUNNY_STAND_RATE, DoBunnyEat),

    }, {new State(BUNNY_STAND_R3, BUNNY_STAND_RATE, DoBunnyEat),

            new State(BUNNY_STAND_R3 + 4, SF_QUICK_CALL, DoBunnyGrowUp),

            new State(BUNNY_STAND_R3 + 4, BUNNY_STAND_RATE, DoBunnyEat),

    }, {new State(BUNNY_STAND_R4, BUNNY_STAND_RATE, DoBunnyEat),

            new State(BUNNY_STAND_R4 + 4, SF_QUICK_CALL, DoBunnyGrowUp),

            new State(BUNNY_STAND_R4 + 4, BUNNY_STAND_RATE, DoBunnyEat),

    }};


    public static final Animator DoBunnyScrew = new Animator((Animator.Runnable) Bunny::DoBunnyScrew);


    private static final State[][] s_BunnyScrew = {{new State(BUNNY_STAND_R0, BUNNY_SCREW_RATE, DoBunnyScrew),

            new State(BUNNY_STAND_R0 + 2, BUNNY_SCREW_RATE, DoBunnyScrew),

    }, {new State(BUNNY_STAND_R1, BUNNY_SCREW_RATE, DoBunnyScrew),

            new State(BUNNY_STAND_R1 + 2, BUNNY_SCREW_RATE, DoBunnyScrew),

    }, {new State(BUNNY_STAND_R2, BUNNY_SCREW_RATE, DoBunnyScrew),

            new State(BUNNY_STAND_R2 + 2, BUNNY_SCREW_RATE, DoBunnyScrew),

    }, {new State(BUNNY_STAND_R3, BUNNY_SCREW_RATE, DoBunnyScrew),

            new State(BUNNY_STAND_R3 + 2, BUNNY_SCREW_RATE, DoBunnyScrew),

    }, {new State(BUNNY_STAND_R4, BUNNY_SCREW_RATE, DoBunnyScrew),

            new State(BUNNY_STAND_R4 + 2, BUNNY_SCREW_RATE, DoBunnyScrew),

    }};


    public static final Animator InitBunnySlash = new Animator((Animator.Runnable) Bunny::InitBunnySlash);


    private static final State[][] s_BunnySwipe = {{new State(BUNNY_SWIPE_R0, BUNNY_SWIPE_RATE, NullBunny),

            new State(BUNNY_SWIPE_R0 + 1, BUNNY_SWIPE_RATE, NullBunny),

            new State(BUNNY_SWIPE_R0 + 1, SF_QUICK_CALL, InitBunnySlash),

            new State(BUNNY_SWIPE_R0 + 2, BUNNY_SWIPE_RATE, NullBunny),

            new State(BUNNY_SWIPE_R0 + 3, BUNNY_SWIPE_RATE, NullBunny),

            new State(BUNNY_SWIPE_R0 + 3, SF_QUICK_CALL, InitBunnySlash),

            new State(BUNNY_SWIPE_R0 + 3, SF_QUICK_CALL, InitActorDecide),

            new State(BUNNY_SWIPE_R0 + 3, BUNNY_SWIPE_RATE, DoBunnyMove).setNext(),

    },

            {new State(BUNNY_RUN_R1, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R1 + 1, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R1 + 1, SF_QUICK_CALL, InitBunnySlash),

                    new State(BUNNY_RUN_R1 + 2, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R1 + 3, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R1 + 3, SF_QUICK_CALL, InitBunnySlash),

                    new State(BUNNY_RUN_R1 + 3, SF_QUICK_CALL, InitActorDecide),

                    new State(BUNNY_RUN_R1 + 3, BUNNY_SWIPE_RATE, DoBunnyMove).setNext(),

            },

            {new State(BUNNY_RUN_R2, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R2 + 1, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R2 + 1, SF_QUICK_CALL, InitBunnySlash),

                    new State(BUNNY_RUN_R2 + 2, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R2 + 3, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R2 + 3, SF_QUICK_CALL, InitBunnySlash),

                    new State(BUNNY_RUN_R2 + 3, SF_QUICK_CALL, InitActorDecide),

                    new State(BUNNY_RUN_R2 + 3, BUNNY_SWIPE_RATE, DoBunnyMove).setNext(),

            },


            {new State(BUNNY_RUN_R3, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R3 + 1, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R3 + 1, SF_QUICK_CALL, InitBunnySlash),

                    new State(BUNNY_RUN_R3 + 2, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R3 + 3, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R3 + 3, SF_QUICK_CALL, InitBunnySlash),

                    new State(BUNNY_RUN_R3 + 3, SF_QUICK_CALL, InitActorDecide),

                    new State(BUNNY_RUN_R3 + 3, BUNNY_SWIPE_RATE, DoBunnyMove).setNext(),

            },

            {new State(BUNNY_RUN_R4, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R4 + 1, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R4 + 1, SF_QUICK_CALL, InitBunnySlash),

                    new State(BUNNY_RUN_R4 + 2, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R4 + 3, BUNNY_SWIPE_RATE, NullBunny),

                    new State(BUNNY_RUN_R4 + 3, SF_QUICK_CALL, InitBunnySlash),

                    new State(BUNNY_RUN_R4 + 3, SF_QUICK_CALL, InitActorDecide),

                    new State(BUNNY_RUN_R4 + 3, BUNNY_SWIPE_RATE, DoBunnyMove).setNext(),

            }};


    public static final Animator DoBunnyStandKill = new Animator((Animator.Runnable) Bunny::DoBunnyStandKill);


    private static final State[][] s_BunnyHeart = {{new State(BUNNY_SWIPE_R0, BUNNY_HEART_RATE, DoBunnyStandKill).setNext(),

    }, {new State(BUNNY_SWIPE_R1, BUNNY_HEART_RATE, DoBunnyStandKill).setNext(),

    }, {new State(BUNNY_SWIPE_R2, BUNNY_HEART_RATE, DoBunnyStandKill).setNext(),

    }, {new State(BUNNY_SWIPE_R3, BUNNY_HEART_RATE, DoBunnyStandKill).setNext(),

    }, {new State(BUNNY_SWIPE_R4, BUNNY_HEART_RATE, DoBunnyStandKill).setNext(),

    }};


    private static final EnemyStateGroup sg_BunnyStand = new EnemyStateGroup(s_BunnyStand[0], s_BunnyStand[1], s_BunnyStand[2], s_BunnyStand[3], s_BunnyStand[4]);


    private static final EnemyStateGroup sg_BunnyRun = new EnemyStateGroup(s_BunnyRun[0], s_BunnyRun[1], s_BunnyRun[2], s_BunnyRun[3], s_BunnyRun[4]);


    private static final EnemyStateGroup sg_BunnyHeart = new EnemyStateGroup(s_BunnyHeart[0], s_BunnyHeart[1], s_BunnyHeart[2], s_BunnyHeart[3], s_BunnyHeart[4]);
    private static final EnemyStateGroup sg_BunnySwipe = new EnemyStateGroup(s_BunnySwipe[0], s_BunnySwipe[1], s_BunnySwipe[2], s_BunnySwipe[3], s_BunnySwipe[4]);
    private static final EnemyStateGroup sg_BunnyScrew = new EnemyStateGroup(s_BunnyScrew[0], s_BunnyScrew[1], s_BunnyScrew[2], s_BunnyScrew[3], s_BunnyScrew[4]);
    private static final Actor_Action_Set BunnyActionSet = new Actor_Action_Set(sg_BunnyStand, sg_BunnyRun, sg_BunnyJump, sg_BunnyFall, null, // sg_BunnyCrawl,
            null, // sg_BunnySwim,
            null, // sg_BunnyFly,
            null, // sg_BunnyRise,
            null, // sg_BunnySit,
            null, // sg_BunnyLook,
            null, // climb
            sg_BunnyPain, sg_BunnyDie, null, sg_BunnyDead, sg_BunnyDeathJump, sg_BunnyDeathFall, null, new short[]{1024}, null, new short[]{1024}, new StateGroup[]{sg_BunnyHeart, sg_BunnyRun}, null, null);
    private static final Actor_Action_Set BunnyWhiteActionSet = new Actor_Action_Set(sg_BunnyStand, sg_BunnyRun, sg_BunnyJump, sg_BunnyFall, null, // sg_BunnyCrawl,
            null, // sg_BunnySwim,
            null, // sg_BunnyFly,
            null, // sg_BunnyRise,
            null, // sg_BunnySit,
            null, // sg_BunnyLook,
            null, // climb
            sg_BunnyPain, // pain
            sg_BunnyDie, null, sg_BunnyDead, sg_BunnyDeathJump, sg_BunnyDeathFall, new StateGroup[]{sg_BunnySwipe}, new short[]{1024}, new StateGroup[]{sg_BunnySwipe}, new short[]{1024}, new StateGroup[]{sg_BunnyHeart, sg_BunnySwipe}, null, null);

}
