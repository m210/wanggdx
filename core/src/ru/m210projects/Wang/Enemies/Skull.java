package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Wang.Type.ATTRIBUTE;
import ru.m210projects.Wang.Type.Animator;
import ru.m210projects.Wang.Type.State;
import ru.m210projects.Wang.Type.USER;

import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolie.EnemyDefaults;
import static ru.m210projects.Wang.Game.Player;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.JWeapon.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Main.engine;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Rooms.*;
import static ru.m210projects.Wang.Shrap.SpawnShrap;
import static ru.m210projects.Wang.Sound.PlaySound;
import static ru.m210projects.Wang.Sound.v3df_none;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Type.MyTypes.DIV2;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.*;

public class Skull {

    public static final int SKULL_RATE = 10;
    public static final ATTRIBUTE SkullAttrib = new ATTRIBUTE(new short[]{60, 80, 100, 130}, // Speeds
            new short[]{3, 0, -2, -3}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_AHAMBIENT, 0, 0, 0, DIGI_AHSCREAM, DIGI_AHEXPLODE, 0, 0, 0, 0});
    public static final int SKULL_EXPLODE_RATE = 11;
    public static final int BETTY_RATE = 10;
    public static final int BETTY_EXPLODE_RATE = 11;
    public static final int BETTY_EXPLODE = BETTY_R0;
    //////////////////////
    //
    // SKULL for Serp God
    //
    //////////////////////
    // actor does a sine wave about u.sz - this is the z mid point
    public static final int BETTY_BOB_AMT = (Z(16));
    private static final Animator DoSkullJump = new Animator((Animator.Runnable) Skull::DoSkullJump);
    private static final Animator DoSkullSpawnShrap = new Animator((Animator.Runnable) Skull::DoSkullSpawnShrap);
    public static final State[] s_SkullExplode = {new State(SKULL_EXPLODE, 1, null),
            // s_SkullExplode[1]},
            new State(SKULL_EXPLODE, SF_QUICK_CALL, DoDamageTest),
            // s_SkullExplode[2]},
            new State(SKULL_EXPLODE, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[3]},
            new State(SKULL_EXPLODE + 1, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[4]},
            new State(SKULL_EXPLODE + 2, SF_QUICK_CALL, DoSkullSpawnShrap),
            // s_SkullExplode[5]},
            new State(SKULL_EXPLODE + 2, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[6]},
            new State(SKULL_EXPLODE + 3, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[7]},
            new State(SKULL_EXPLODE + 4, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[8]},
            new State(SKULL_EXPLODE + 5, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[9]},
            new State(SKULL_EXPLODE + 6, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[10]},
            new State(SKULL_EXPLODE + 7, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[11]},
            new State(SKULL_EXPLODE + 8, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[12]},
            new State(SKULL_EXPLODE + 9, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[13]},
            new State(SKULL_EXPLODE + 10, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[14]},
            new State(SKULL_EXPLODE + 11, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[15]},
            new State(SKULL_EXPLODE + 12, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[16]},
            new State(SKULL_EXPLODE + 13, SKULL_EXPLODE_RATE, null),
            // s_SkullExplode[17]},
            new State(SKULL_EXPLODE + 13, SKULL_EXPLODE_RATE, DoSuicide).setNext(),
// s_SkullExplode[17]}
    };
    private static final Animator DoBettyJump = new Animator((Animator.Runnable) Skull::DoBettyJump);
    //////////////////////
    //
    // SKULL Explode
    //
    //////////////////////
    private static final State[][] s_SkullJump = {{new State(SKULL_R0, SKULL_RATE, DoSkullJump).setNext(),
// s_SkullJump[0][0]},
    }, {new State(SKULL_R1, SKULL_RATE, DoSkullJump).setNext(),
// s_SkullJump[1][0]},
    }, {new State(SKULL_R2, SKULL_RATE, DoSkullJump).setNext(),
// s_SkullJump[2][0]},
    }, {new State(SKULL_R3, SKULL_RATE, DoSkullJump).setNext(),
// s_SkullJump[3][0]},
    }, {new State(SKULL_R4, SKULL_RATE, DoSkullJump).setNext(),
// s_SkullJump[4][0]},
    }};
    private static final EnemyStateGroup sg_SkullJump = new EnemyStateGroup(s_SkullJump[0], s_SkullJump[1], s_SkullJump[2], s_SkullJump[3], s_SkullJump[4]);
    private static final Animator DoSerpRing = new Animator((Animator.Runnable) Skull::DoSerpRing);
    //////////////////////
    //
    // SKULL Jump
    //
    //////////////////////
    public static final State[][] s_SkullRing = {{new State(SKULL_R0, SKULL_RATE, DoSerpRing).setNext(),
// s_SkullRing[0][0]},
    }, {new State(SKULL_R1, SKULL_RATE, DoSerpRing).setNext(),
// s_SkullRing[1][0]},
    }, {new State(SKULL_R2, SKULL_RATE, DoSerpRing).setNext(),
// s_SkullRing[2][0]},
    }, {new State(SKULL_R3, SKULL_RATE, DoSerpRing).setNext(),
// s_SkullRing[3][0]},
    }, {new State(SKULL_R4, SKULL_RATE, DoSerpRing).setNext(),
// s_SkullRing[4][0]},
    }};
    public static final EnemyStateGroup sg_SkullRing = new EnemyStateGroup(s_SkullRing[0], s_SkullRing[1], s_SkullRing[2], s_SkullRing[3], s_SkullRing[4]);
    private static final Animator DoSkullWait = new Animator((Animator.Runnable) Skull::DoSkullWait);
    public static final State[][] s_SkullWait = {{new State(SKULL_R0, SKULL_RATE, DoSkullWait).setNext(),
// s_SkullWait[0][0]},
    }, {new State(SKULL_R1, SKULL_RATE, DoSkullWait).setNext(),
// s_SkullWait[1][0]},
    }, {new State(SKULL_R2, SKULL_RATE, DoSkullWait).setNext(),
// s_SkullWait[2][0]},
    }, {new State(SKULL_R3, SKULL_RATE, DoSkullWait).setNext(),
// s_SkullWait[3][0]},
    }, {new State(SKULL_R4, SKULL_RATE, DoSkullWait).setNext(),
// s_SkullWait[4][0]},
    }};
    public static final EnemyStateGroup sg_SkullWait = new EnemyStateGroup(s_SkullWait[0], s_SkullWait[1], s_SkullWait[2], s_SkullWait[3], s_SkullWait[4]);
    // actor does a sine wave about u.sz - this is the z mid point
    private static final int SKULL_BOB_AMT = (Z(16));
    private static final ATTRIBUTE BettyAttrib = new ATTRIBUTE(new short[]{60, 80, 100, 130}, // Speeds
            new short[]{3, 0, -2, -3}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
    private static final State[][] s_BettyJump = {{new State(BETTY_R0, BETTY_RATE, DoBettyJump).setNext(),
// s_BettyJump[0][0]},
    }, {new State(BETTY_R1, BETTY_RATE, DoBettyJump).setNext(),
// s_BettyJump[1][0]},
    }, {new State(BETTY_R2, BETTY_RATE, DoBettyJump).setNext(),
// s_BettyJump[2][0]},
    }, {new State(BETTY_R3, BETTY_RATE, DoBettyJump).setNext(),
// s_BettyJump[3][0]},
    }, {new State(BETTY_R4, BETTY_RATE, DoBettyJump).setNext(),
// s_BettyJump[4][0]},
    }};
    private static final EnemyStateGroup sg_BettyJump = new EnemyStateGroup(s_BettyJump[0], s_BettyJump[1], s_BettyJump[2], s_BettyJump[3], s_BettyJump[4]);
    private static final Animator DoBettyWait = new Animator((Animator.Runnable) Skull::DoBettyWait);
    private static final State[][] s_BettyWait = {{new State(BETTY_R0, BETTY_RATE, DoBettyWait),
            // s_BettyWait[0][1]},
            new State(BETTY_R0 + 1, BETTY_RATE, DoBettyWait),
            // s_BettyWait[0][2]},
            new State(BETTY_R0 + 2, BETTY_RATE, DoBettyWait),
// s_BettyWait[0][0]},
    }, {new State(BETTY_R1, BETTY_RATE, DoBettyWait),
            // s_BettyWait[1][1]},
            new State(BETTY_R1 + 1, BETTY_RATE, DoBettyWait),
            // s_BettyWait[1][2]},
            new State(BETTY_R1 + 2, BETTY_RATE, DoBettyWait),
// s_BettyWait[1][0]},
    }, {new State(BETTY_R2, BETTY_RATE, DoBettyWait),
            // s_BettyWait[2][1]},
            new State(BETTY_R2 + 1, BETTY_RATE, DoBettyWait),
            // s_BettyWait[2][2]},
            new State(BETTY_R2 + 2, BETTY_RATE, DoBettyWait),
// s_BettyWait[2][0]},
    }, {new State(BETTY_R3, BETTY_RATE, DoBettyWait),
            // s_BettyWait[3][1]},
            new State(BETTY_R3 + 1, BETTY_RATE, DoBettyWait),
            // s_BettyWait[3][2]},
            new State(BETTY_R3 + 2, BETTY_RATE, DoBettyWait),
// s_BettyWait[3][0]},
    }, {new State(BETTY_R4, BETTY_RATE, DoBettyWait),
            // s_BettyWait[4][1]},
            new State(BETTY_R4 + 1, BETTY_RATE, DoBettyWait),
            // s_BettyWait[4][2]},
            new State(BETTY_R4 + 2, BETTY_RATE, DoBettyWait),
// s_BettyWait[4][0]},
    }};
    private static final EnemyStateGroup sg_BettyWait = new EnemyStateGroup(s_BettyWait[0], s_BettyWait[1], s_BettyWait[2], s_BettyWait[3], s_BettyWait[4]);

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////
    //
    // BETTY Wait
    //
    //////////////////////
    private static final State[] s_BettyExplode = {new State(BETTY_EXPLODE, SF_QUICK_CALL, DoDamageTest),
            // s_BettyExplode[1]},
            new State(BETTY_EXPLODE, BETTY_EXPLODE_RATE, DoSuicide),
// s_BettyExplode[0]}
    };

    public static void InitSkullStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{
                sg_SkullWait,
                sg_SkullJump,
                sg_BettyWait,
                sg_BettyJump,
                sg_SkullRing
        }) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }

        State.InitState(s_SkullExplode);
        State.InitState(s_BettyExplode);
    }

    public static void SetupSkull(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u;

        if (TEST(sp.getCstat(),
                CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, SKULL_R0, s_SkullWait[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_SKULL;
        }

        ChangeState(SpriteNum, s_SkullWait[0][0]);
        u.Attrib = SkullAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_SkullExplode[0];
        u.setRot(sg_SkullWait);

        u.ID = SKULL_R0;

        EnemyDefaults(SpriteNum, null, null);
        sp.setClipdist((128 + 64) >> 2);
        u.Flags |= (SPR_XFLIP_TOGGLE);
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YCENTER));

        u.Radius = 400;

        if (SPRITEp_BOS(sp) > u.loz - Z(16)) {
            sp.setZ(u.loz + Z(SPRITEp_YOFF(sp)));

            u.loz = sp.getZ();
            // leave 8 pixels above the ground
            sp.setZ(sp.getZ() + SPRITEp_SIZE_TOS(sp) - Z(3));
        } else {
            u.Counter = RANDOM_P2(2048);
            u.sz = sp.getZ();
        }
    }

    private static void DoSkullMove(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int dax, day, daz;

        dax = MOVEx(sp.getXvel(),
                sp.getAng());
        day = MOVEy(sp.getXvel(),
                sp.getAng());
        daz = sp.getZvel();

        u.moveSpriteReturn = move_missile(SpriteNum, dax, day, daz, Z(16),
                Z(16),
                CLIPMASK_MISSILE, ACTORMOVETICS);

        DoFindGroundPoint(SpriteNum);
    }

    //////////////////////
    //
    // BETTY Jump
    //
    //////////////////////

    public static void DoSkullBeginDeath(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int i, num_ord;

        // Decrease for Serp God
        if (sp.getOwner() >= 0) {
            USER ou = getUser(sp.getOwner());
            if (ou != null) {
                ou.Counter--;
            }
        }

        // starts the explosion that does the actual damage

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        switch (sp.getHitag()) {
            case 1:
                if (sp.getLotag() != 0) {
                    num_ord = sp.getLotag();
                } else {
                    num_ord = 2;
                }
                if (num_ord > 3) {
                    num_ord = 3;
                }
                for (i = 0; i < num_ord; i++) {
                    sp.setAng(NORM_ANGLE(sp.getAng() + (i * 1024)));
                    InitSpriteChemBomb(SpriteNum);
                }
                break;

            case 2:
                if (sp.getLotag() != 0) {
                    num_ord = sp.getLotag();
                } else {
                    num_ord = 5;
                }
                if (num_ord > 10) {
                    num_ord = 10;
                }
                for (i = 0; i < num_ord; i++) {
                    sp.setAng(NORM_ANGLE(RANDOM_RANGE(2048)));
                    InitCaltrops(SpriteNum);
                }
                break;

            case 3:
                UpdateSinglePlayKills(SpriteNum, null);
                InitFlashBomb(SpriteNum);
                break;

            case 4:
                if (sp.getLotag() != 0) {
                    num_ord = sp.getLotag();
                } else {
                    num_ord = 5;
                }
                if (num_ord > 10) {
                    num_ord = 10;
                }
                for (i = 0; i < num_ord; i++) {
                    sp.setAng(NORM_ANGLE(sp.getAng() + (i * (2048 / num_ord))));
                    InitSpriteGrenade(SpriteNum);
                }
                break;
            default:
                SpawnMineExp(SpriteNum);
                for (i = 0; i < 3; i++) {
                    sp.setAng(NORM_ANGLE(RANDOM_RANGE(2048)));
                    InitPhosphorus(SpriteNum);
                }
                break;
        }

        u.RotNum = 0;
        u.Tics = 0;

        u.ID = SKULL_R0;
        u.Radius = DamageData[DMG_SKULL_EXP].radius;
        u.OverlapZ = Z(64);
        change_sprite_stat(SpriteNum, STAT_DEAD_ACTOR);
        sp.setShade(-40);

        SpawnLittleExp(SpriteNum);
        SetSuicide(SpriteNum);
    }

    private static void DoSkullJump(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (sp.getXvel() != 0) {
            DoSkullMove(SpriteNum);
        } else {
            sp.setAng(NORM_ANGLE(sp.getAng() + (64 * ACTORMOVETICS)));
        }

        if (TEST(u.Flags, SPR_JUMPING)) {
            DoJump(SpriteNum);
        } else if (TEST(u.Flags, SPR_FALLING)) {
            DoFall(SpriteNum);

            // jump/fall type
            if (sp.getXvel() != 0) {
                Sprite tsp = boardService.getSprite(u.tgt_sp);
                if (tsp == null) {
                    return;
                }

                int dist = DISTANCE(sp.getX(),
                        sp.getY(),
                        tsp.getX(),
                        tsp.getY());

                if (dist < 1000 && SpriteOverlapZ(SpriteNum, u.tgt_sp, Z(32))) {
                    UpdateSinglePlayKills(SpriteNum, null);
                    DoSkullBeginDeath(SpriteNum);
                    return;
                }

                if ((sp.getZ() > u.loz - Z(36))) {
                    sp.setZ(u.loz - Z(36));
                    UpdateSinglePlayKills(SpriteNum, null);
                    DoSkullBeginDeath(SpriteNum);
                }
            }
            // non jumping type
            else {
                if (u.jump_speed > 200) {
                    UpdateSinglePlayKills(SpriteNum, null);
                    DoSkullBeginDeath(SpriteNum);
                }
            }

        } else {
            UpdateSinglePlayKills(SpriteNum, null);
            DoSkullBeginDeath(SpriteNum);
        }
    }

    //////////////////////
    //
    // BETTY Explode
    //
    //////////////////////

    private static void DoSkullBob(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.Counter = ((u.Counter + (ACTORMOVETICS << 3) + (ACTORMOVETICS << 1)) & 2047);
        sp.setZ(u.sz + ((SKULL_BOB_AMT * EngineUtils.sin(u.Counter)) >> 14) + ((DIV2(SKULL_BOB_AMT) * EngineUtils.sin(u.Counter)) >> 14));
    }

    private static void DoSkullSpawnShrap(int SpriteNum) {
        SpawnShrap(SpriteNum, -1);
    }

    private static void DoSkullWait(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }

        int dist = DISTANCE(sp.getX(),
                sp.getY(),
                tsp.getX(),
                tsp.getY());

        DoActorPickClosePlayer(SpriteNum);

        if ((u.WaitTics -= ACTORMOVETICS) <= 0) {
            PlaySound(DIGI_AHSCREAM, sp, v3df_none);
            u.WaitTics = (SEC(3) + RANDOM_RANGE(360));
        }

        // below the floor type
        if (sp.getZ() > u.loz) {
            // look for closest player every once in a while
            if (dist < 3500) {
                sp.setXvel(0);
                u.jump_speed = -600;
                NewStateGroup(SpriteNum, sg_SkullJump);
                DoBeginJump(SpriteNum);
            }
        } else
        // above the floor type
        {
            sp.setAng(NORM_ANGLE(sp.getAng() + (48 * ACTORMOVETICS)));

            DoSkullBob(SpriteNum);

            if (dist < 8000) {
                sp.setAng(EngineUtils.getAngle(tsp.getX() - sp.getX(),
                        tsp.getY() - sp.getY()));
                sp.setXvel((128 + (RANDOM_P2(256 << 8) >> 8)));
                u.jump_speed = -700;
                NewStateGroup(SpriteNum, sg_SkullJump);
                DoBeginJump(SpriteNum);
            }
        }
    }

    public static void SetupBetty(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u;
        if (TEST(sp.getCstat(),
                CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, BETTY_R0, s_BettyWait[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_SKULL;
        }

        ChangeState(SpriteNum, s_BettyWait[0][0]);
        u.Attrib = BettyAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_BettyExplode[0];
        u.setRot(sg_BettyWait);

        u.ID = BETTY_R0;

        EnemyDefaults(SpriteNum, null, null);
        sp.setClipdist((128 + 64) >> 2);
        u.Flags |= (SPR_XFLIP_TOGGLE);
        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_YCENTER));

        u.Radius = 400;

        if (SPRITEp_BOS(sp) > u.loz - Z(16)) {
            sp.setZ(u.loz + Z(SPRITEp_YOFF(sp)));

            u.loz = sp.getZ();
            // leave 8 pixels above the ground
            sp.setZ(sp.getZ() + SPRITEp_SIZE_TOS(sp) - Z(3));
        } else {
            u.Counter = RANDOM_P2(2048);
            u.sz = sp.getZ();
        }
    }

    private static void DoBettyMove(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int dax, day, daz;

        dax = MOVEx(sp.getXvel(),
                sp.getAng());
        day = MOVEy(sp.getXvel(),
                sp.getAng());
        daz = sp.getZvel();

        u.moveSpriteReturn = move_missile(SpriteNum, dax, day, daz, Z(16),
                Z(16),
                CLIPMASK_MISSILE, ACTORMOVETICS);

        DoFindGroundPoint(SpriteNum);
    }

    public static void DoBettyBeginDeath(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int i, num_ord;

        // starts the explosion that does the actual damage
        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));

        switch (sp.getHitag()) {
            case 1:
                if (sp.getLotag() != 0) {
                    num_ord = sp.getLotag();
                } else {
                    num_ord = 2;
                }
                if (num_ord > 3) {
                    num_ord = 3;
                }
                for (i = 0; i < num_ord; i++) {
                    sp.setAng(NORM_ANGLE(sp.getAng() + (i * 1024)));
                    InitSpriteChemBomb(SpriteNum);
                }
                break;

            case 2:
                if (sp.getLotag() != 0) {
                    num_ord = sp.getLotag();
                } else {
                    num_ord = 5;
                }
                if (num_ord > 10) {
                    num_ord = 10;
                }
                for (i = 0; i < num_ord; i++) {
                    sp.setAng(NORM_ANGLE(RANDOM_RANGE(2048)));
                    InitCaltrops(SpriteNum);
                }
                break;

            case 3:
                InitFlashBomb(SpriteNum);
                break;

            case 4:
                if (sp.getLotag() != 0) {
                    num_ord = sp.getLotag();
                } else {
                    num_ord = 5;
                }
                if (num_ord > 10) {
                    num_ord = 10;
                }
                for (i = 0; i < num_ord; i++) {
                    sp.setAng(NORM_ANGLE(sp.getAng() + (i * (2048 / num_ord))));
                    InitSpriteGrenade(SpriteNum);
                }
                break;
            default:
                for (i = 0; i < 5; i++) {
                    sp.setAng(NORM_ANGLE(RANDOM_RANGE(2048)));
                    InitPhosphorus(SpriteNum);
                    SpawnMineExp(SpriteNum);
                }
                break;
        }

        u.RotNum = 0;
        u.Tics = 0;

        u.ID = BETTY_R0;
        u.Radius = DamageData[DMG_SKULL_EXP].radius;// *DamageRadiusBetty;
        u.OverlapZ = Z(64);
        change_sprite_stat(SpriteNum, STAT_DEAD_ACTOR);
        sp.setShade(-40);

        SpawnLittleExp(SpriteNum);
        SetSuicide(SpriteNum);
    }

    private static void DoBettyJump(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (sp.getXvel() != 0) {
            DoBettyMove(SpriteNum);
        } else {
            sp.setAng(NORM_ANGLE(sp.getAng() + (64 * ACTORMOVETICS)));
        }

        if (TEST(u.Flags, SPR_JUMPING)) {
            DoJump(SpriteNum);
        } else if (TEST(u.Flags, SPR_FALLING)) {
            DoFall(SpriteNum);

            // jump/fall type
            if (sp.getXvel() != 0) {
                Sprite tsp = boardService.getSprite(u.tgt_sp);
                if (tsp == null) {
                    return;
                }

                int dist = DISTANCE(sp.getX(),
                        sp.getY(),
                        tsp.getX(),
                        tsp.getY());

                if (dist < 1000 && SpriteOverlapZ(SpriteNum, u.tgt_sp, Z(32))) {
                    UpdateSinglePlayKills(SpriteNum, null);
                    DoBettyBeginDeath(SpriteNum);
                    return;
                }

                if ((sp.getZ() > u.loz - Z(36))) {
                    sp.setZ(u.loz - Z(36));
                    UpdateSinglePlayKills(SpriteNum, null);
                    DoBettyBeginDeath(SpriteNum);
                }
            }
            // non jumping type
            else {
                if (u.jump_speed > 200) {
                    UpdateSinglePlayKills(SpriteNum, null);
                    DoBettyBeginDeath(SpriteNum);
                }
            }

        } else {
            UpdateSinglePlayKills(SpriteNum, null);
            DoBettyBeginDeath(SpriteNum);
        }
    }

    private static void DoBettyBob(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        u.Counter = ((u.Counter + (ACTORMOVETICS << 3) + (ACTORMOVETICS << 1)) & 2047);
        sp.setZ(u.sz + ((BETTY_BOB_AMT * EngineUtils.sin(u.Counter)) >> 14) + ((DIV2(BETTY_BOB_AMT) * EngineUtils.sin(u.Counter)) >> 14));

    }

    private static void DoBettyWait(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp == null) {
            return;
        }

        int dist = DISTANCE(sp.getX(),
                sp.getY(),
                tsp.getX(),
                tsp.getY());

        DoActorPickClosePlayer(SpriteNum);

        if ((u.WaitTics -= ACTORMOVETICS) <= 0) {
            PlaySound(DIGI_MINEBEEP, sp, v3df_none);
            u.WaitTics = SEC(3);
        }

        // below the floor type
        if (sp.getZ() > u.loz) {
            // look for closest player every once in a while
            if (dist < 3500) {
                sp.setXvel(0);
                u.jump_speed = -600;
                NewStateGroup(SpriteNum, sg_BettyJump);
                DoBeginJump(SpriteNum);
            }
        } else
        // above the floor type
        {
            sp.setAng(NORM_ANGLE(sp.getAng() + (48 * ACTORMOVETICS)));

            DoBettyBob(SpriteNum);

            if (dist < 8000) {
                sp.setAng(EngineUtils.getAngle(tsp.getX() - sp.getX(),
                        tsp.getY() - sp.getY()));
                sp.setXvel((128 + (RANDOM_P2(256 << 8) >> 8)));
                u.jump_speed = -700;
                NewStateGroup(SpriteNum, sg_BettyJump);
                DoBeginJump(SpriteNum);
            }
        }
    }

    public static void DoSerpRing(int Weapon) {
        Sprite sp = boardService.getSprite(Weapon);
        USER u = getUser(Weapon);
        if (sp == null || u == null) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        USER ou = getUser(sp.getOwner());

        // if owner does not exist or he's dead on the floor
        // kill off all of his skull children
        if (sp.getOwner() == -1 || ou != null && ou.RotNum < 5) {
            UpdateSinglePlayKills(Weapon, null);
            DoSkullBeginDeath(Weapon);
            // +2 does not spawn shrapnel
            u.ID = SKULL_SERP;
            return;
        }

        Sprite osp = boardService.getSprite(sp.getOwner());
        if (osp == null) {
            return;
        }

        // move the center with the player
        sp.setX(osp.getX());
        sp.setY(osp.getY());

        sp.setZ(sp.getZ() + sp.getZvel());
        if (sp.getZ() > osp.getZ() - u.sz) {
            sp.setZ(osp.getZ() - u.sz);
        }

        // go out until its time to come back in
        if (u.Counter2 == 0) {
            u.Dist += 8 * RINGMOVETICS;

            if (u.Dist > u.TargetDist) {
                u.Counter2 = 1;
            }
        }

        // rotate the ring
        u.slide_ang = NORM_ANGLE(u.slide_ang + sp.getYvel());

        // rotate the heads
        if (TEST(u.Flags, SPR_BOUNCE)) {
            sp.setAng(NORM_ANGLE(sp.getAng() + (28 * RINGMOVETICS)));
        } else {
            sp.setAng(NORM_ANGLE(sp.getAng() - (28 * RINGMOVETICS)));
        }

        // put it out there
        sp.setX(sp.getX() + ((u.Dist * EngineUtils.sin(NORM_ANGLE(u.slide_ang + 512))) >> 14));
        sp.setY(sp.getY() + ((u.Dist * EngineUtils.sin(u.slide_ang)) >> 14));

        engine.setsprite(Weapon, sp.getX(),
                sp.getY(),
                sp.getZ());

        engine.getzsofslope(sp.getSectnum(),
                sp.getX(),
                sp.getY(),
                fz, cz);

        // bound the sprite by the sectors ceiling and floor
        if (sp.getZ() > fz.get()) {
            sp.setZ(fz.get());
        }

        if (sp.getZ() < cz.get() + SPRITEp_SIZE_Z(sp)) {
            sp.setZ(cz.get() + SPRITEp_SIZE_Z(sp));
        }

        if (u.Counter2 > 0 && ou != null) {
            USER tu = getUser(ou.tgt_sp);
            if (tsp != null && (tu == null || tu.PlayerP == -1 || !TEST(Player[tu.PlayerP].Flags, PF_DEAD))) {
                u.tgt_sp = ou.tgt_sp;
                int dist = DISTANCE(sp.getX(),
                        sp.getY(),
                        tsp.getX(),
                        tsp.getY());

                // if ((dist ok and random ok) OR very few skulls left)
                if ((dist < 18000 && (RANDOM_P2(2048 << 5) >> 5) < 16) || (sp.getOwner() != -1 && ou.Counter < 4)) {
                    int sectnum = COVERupdatesector(sp.getX(),
                            sp.getY(),
                            sp.getSectnum());

                    // if (valid sector and can see target)
                    if (sectnum != -1 && CanSeePlayer(Weapon)) {
                        u.ID = SKULL_R0;
                        sp.setAng(EngineUtils.getAngle(tsp.getX() - sp.getX(),
                                tsp.getY() - sp.getY()));
                        sp.setXvel((dist >> 5));
                        sp.setXvel(sp.getXvel() + DIV2(sp.getXvel()));
                        sp.setXvel(sp.getXvel() + (RANDOM_P2(128 << 8) >> 8));
                        u.jump_speed = -800;
                        change_sprite_stat(Weapon, STAT_ENEMY);
                        NewStateGroup(Weapon, sg_SkullJump);
                        DoBeginJump(Weapon);
                    }
                }
            }
        }
    }

    public static void SkullSaveable() {
        SaveData(DoSerpRing);
        SaveData(DoSkullJump);
        SaveData(DoSkullSpawnShrap);
        SaveData(DoSkullWait);
        SaveData(DoBettyJump);
        SaveData(DoBettyWait);

        SaveData(s_SkullWait);
        SaveGroup(sg_SkullWait);

        SaveData(SkullAttrib);

        SaveData(s_SkullRing);
        SaveGroup(sg_SkullRing);
        SaveData(s_SkullJump);
        SaveGroup(sg_SkullJump);
        SaveData(s_SkullExplode);
        SaveData(s_BettyWait);
        SaveGroup(sg_BettyWait);

        SaveData(BettyAttrib);

        SaveData(s_BettyJump);
        SaveGroup(sg_BettyJump);
        SaveData(s_BettyExplode);
    }


}
