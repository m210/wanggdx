package ru.m210projects.Wang.Enemies;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Wang.Type.*;
import ru.m210projects.Wang.Weapon;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Wang.Actor.*;
import static ru.m210projects.Wang.Ai.*;
import static ru.m210projects.Wang.Digi.*;
import static ru.m210projects.Wang.Enemies.Coolg.NewCoolg;
import static ru.m210projects.Wang.Game.Distance;
import static ru.m210projects.Wang.Game.TotalKillable;
import static ru.m210projects.Wang.Gameutils.*;
import static ru.m210projects.Wang.Main.boardService;
import static ru.m210projects.Wang.Names.*;
import static ru.m210projects.Wang.Player.QueueFloorBlood;
import static ru.m210projects.Wang.Sector.getSectUser;
import static ru.m210projects.Wang.Sound.*;
import static ru.m210projects.Wang.Sprites.*;
import static ru.m210projects.Wang.Stag.SECT_SINK;
import static ru.m210projects.Wang.Track.ActorFollowTrack;
import static ru.m210projects.Wang.Type.MyTypes.DIV16;
import static ru.m210projects.Wang.Type.MyTypes.TEST;
import static ru.m210projects.Wang.Type.Saveable.SaveData;
import static ru.m210projects.Wang.Type.Saveable.SaveGroup;
import static ru.m210projects.Wang.Weapon.UpdateSinglePlayKills;

public class Coolie {

    public static final int COOLIE_RATE = 12;
    public static final int COOLIE_PAIN_RATE = 60;
    public static final int COOLIE_DIE_RATE = 30;
    private static final Animator DoCoolieMove = new Animator((Animator.Runnable)  Coolie::DoCoolieMove);
    private static final Animator CooliePain = new Animator((Animator.Runnable) Coolie::CooliePain);
    private static final Animator NullCoolie = new Animator((Animator.Runnable) Coolie::NullCoolie);
    private static final Animator SpawnCoolg = new Animator((Animator.Runnable) Coolie::SpawnCoolg);
    private static final Animator SpawnCoolieExp = new Animator(Weapon::SpawnCoolieExp);
    private static final Decision[] CoolieBroadcast = {
            // new Decision(1, InitActorAlertNoise ),
            new Decision(16, InitActorAmbientNoise), new Decision(1024, InitActorDecide)};
    private static final Animator DoCoolieWaitBirth = new Animator((Animator.Runnable) Coolie::DoCoolieWaitBirth);
    private static final Decision[] CoolieSurprised = {new Decision(700, InitActorMoveCloser), new Decision(703, InitActorAmbientNoise), new Decision(1024, InitActorDecide)};
    private static final Decision[] CoolieEvasive = {new Decision(10, InitActorEvade), new Decision(1024, null)};
    private static final Decision[] CoolieLostTarget = {new Decision(900, InitActorFindPlayer), new Decision(1024, InitActorWanderAround)};
    //////////////////////
    //
    // COOLIE RUN
    //
    //////////////////////
    private static final ATTRIBUTE CoolieAttrib = new ATTRIBUTE(new short[]{60, 80, 100, 200}, // Speeds
            new short[]{3, 0, -2, -3}, // Tic Adjusts
            3, // MaxWeapons;
            new int[]{DIGI_COOLIEAMBIENT, DIGI_COOLIEALERT, DIGI_COOLIEALERT, DIGI_COOLIEPAIN, 0, DIGI_CGMATERIALIZE, DIGI_COOLIEEXPLODE, 0, 0, 0});
    private static final State[][] s_CoolieRun = {{new State(COOLIE_RUN_R0, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[0][1]},
            new State(COOLIE_RUN_R0 + 1, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[0][2]},
            new State(COOLIE_RUN_R0 + 2, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[0][3]},
            new State(COOLIE_RUN_R0 + 3, COOLIE_RATE, DoCoolieMove),// s_CoolieRun[0][0]}
    }, {new State(COOLIE_RUN_R1, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[1][1]},
            new State(COOLIE_RUN_R1 + 1, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[1][2]},
            new State(COOLIE_RUN_R1 + 2, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[1][3]},
            new State(COOLIE_RUN_R1 + 3, COOLIE_RATE, DoCoolieMove),// s_CoolieRun[1][0]}
    }, {new State(COOLIE_RUN_R2, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[2][1]},
            new State(COOLIE_RUN_R2 + 1, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[2][2]},
            new State(COOLIE_RUN_R2 + 2, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[2][3]},
            new State(COOLIE_RUN_R2 + 3, COOLIE_RATE, DoCoolieMove),// s_CoolieRun[2][0]}
    }, {new State(COOLIE_RUN_R3, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[3][1]},
            new State(COOLIE_RUN_R3 + 1, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[3][2]},
            new State(COOLIE_RUN_R3 + 2, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[3][3]},
            new State(COOLIE_RUN_R3 + 3, COOLIE_RATE, DoCoolieMove),// s_CoolieRun[3][0]}
    }, {new State(COOLIE_RUN_R4, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[4][1]},
            new State(COOLIE_RUN_R4 + 1, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[4][2]},
            new State(COOLIE_RUN_R4 + 2, COOLIE_RATE, DoCoolieMove), // s_CoolieRun[4][3]},
            new State(COOLIE_RUN_R4 + 3, COOLIE_RATE, DoCoolieMove),// s_CoolieRun[4][0]},
    }};
    private static final EnemyStateGroup sg_CoolieRun = new EnemyStateGroup(s_CoolieRun[0], s_CoolieRun[1], s_CoolieRun[2], s_CoolieRun[3], s_CoolieRun[4]);
    private static final State[][] s_CoolieCharge = {{new State(COOLIE_CHARGE_R0, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[0][1]},
            new State(COOLIE_CHARGE_R0 + 1, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[0][2]},
            new State(COOLIE_CHARGE_R0 + 2, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[0][3]},
            new State(COOLIE_CHARGE_R0 + 3, COOLIE_RATE, DoCoolieMove),// s_CoolieCharge[0][0]}
    }, {new State(COOLIE_CHARGE_R1, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[1][1]},
            new State(COOLIE_CHARGE_R1 + 1, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[1][2]},
            new State(COOLIE_CHARGE_R1 + 2, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[1][3]},
            new State(COOLIE_CHARGE_R1 + 3, COOLIE_RATE, DoCoolieMove),// s_CoolieCharge[1][0]}
    }, {new State(COOLIE_CHARGE_R2, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[2][1]},
            new State(COOLIE_CHARGE_R2 + 1, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[2][2]},
            new State(COOLIE_CHARGE_R2 + 2, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[2][3]},
            new State(COOLIE_CHARGE_R2 + 3, COOLIE_RATE, DoCoolieMove),// s_CoolieCharge[2][0]}
    }, {new State(COOLIE_CHARGE_R3, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[3][1]},
            new State(COOLIE_CHARGE_R3 + 1, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[3][2]},
            new State(COOLIE_CHARGE_R3 + 2, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[3][3]},
            new State(COOLIE_CHARGE_R3 + 3, COOLIE_RATE, DoCoolieMove),// s_CoolieCharge[3][0]}
    }, {new State(COOLIE_CHARGE_R4, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[4][1]},
            new State(COOLIE_CHARGE_R4 + 1, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[4][2]},
            new State(COOLIE_CHARGE_R4 + 2, COOLIE_RATE, DoCoolieMove), // s_CoolieCharge[4][3]},
            new State(COOLIE_CHARGE_R4 + 3, COOLIE_RATE, DoCoolieMove),// s_CoolieCharge[4][0]},
    }};
    private static final EnemyStateGroup sg_CoolieCharge = new EnemyStateGroup(s_CoolieCharge[0], s_CoolieCharge[1], s_CoolieCharge[2], s_CoolieCharge[3], s_CoolieCharge[4]);
    private static final Animator InitCoolieCharge = new Animator((Animator.Runnable) Coolie::InitCoolieCharge);
    private static final Decision[] CoolieBattle = {new Decision(700, InitCoolieCharge), new Decision(990, InitActorMoveCloser), new Decision(1000, InitActorAttackNoise), new Decision(1024, InitActorRunAway)};
    private static final Decision[] CoolieOffense = {new Decision(700, InitCoolieCharge), new Decision(1015, InitActorMoveCloser), new Decision(1024, InitActorAttackNoise)};
    private static final Decision[] CoolieCloseRange = {new Decision(400, InitCoolieCharge), new Decision(1024, InitActorReposition)};
    private static final Personality CooliePersonality = new Personality(CoolieBattle, CoolieOffense, CoolieBroadcast, CoolieSurprised, CoolieEvasive, CoolieLostTarget, CoolieCloseRange, CoolieCloseRange);
    //////////////////////
    //
    // COOLIE CHARGE
    //
    //////////////////////
    private static final State[][] s_CoolieStand = {{new State(COOLIE_RUN_R0, COOLIE_RATE, DoCoolieMove).setNext(),// s_CoolieStand[0][0]}
    }, {new State(COOLIE_RUN_R1, COOLIE_RATE, DoCoolieMove).setNext(),// s_CoolieStand[1][0]}
    }, {new State(COOLIE_RUN_R2, COOLIE_RATE, DoCoolieMove).setNext(),// s_CoolieStand[2][0]}
    }, {new State(COOLIE_RUN_R3, COOLIE_RATE, DoCoolieMove).setNext(),// s_CoolieStand[3][0]}
    }, {new State(COOLIE_RUN_R4, COOLIE_RATE, DoCoolieMove).setNext(),// s_CoolieStand[4][0]}
    }};
    private static final EnemyStateGroup sg_CoolieStand = new EnemyStateGroup(s_CoolieStand[0], s_CoolieStand[1], s_CoolieStand[2], s_CoolieStand[3], s_CoolieStand[4]);
    //////////////////////
    //
    // COOLIE STAND
    //
    //////////////////////
    private static final State[][] s_CooliePain = {{new State(COOLIE_PAIN_R0, COOLIE_PAIN_RATE, CooliePain).setNext(),// s_CooliePain[0][0]},
    }, {new State(COOLIE_PAIN_R1, COOLIE_PAIN_RATE, CooliePain).setNext(),// s_CooliePain[1][0]},
    }, {new State(COOLIE_PAIN_R2, COOLIE_PAIN_RATE, CooliePain).setNext(),// s_CooliePain[2][0]},
    }, {new State(COOLIE_PAIN_R3, COOLIE_PAIN_RATE, CooliePain).setNext(),// s_CooliePain[3][0]},
    }, {new State(COOLIE_PAIN_R4, COOLIE_PAIN_RATE, CooliePain).setNext(),// s_CooliePain[4][0]},
    }};

    private static final State[] s_CoolieDie = {new State(COOLIE_DIE, COOLIE_DIE_RATE, NullCoolie), // s_CoolieDie[1]},
            new State(COOLIE_DIE, SF_QUICK_CALL, SpawnCoolieExp), // s_CoolieDie[2]},
            new State(COOLIE_DIE + 1, COOLIE_DIE_RATE, NullCoolie), // s_CoolieDie[3]},
            new State(COOLIE_DIE + 2, COOLIE_DIE_RATE, NullCoolie), // s_CoolieDie[4]},
            new State(COOLIE_DIE + 3, COOLIE_DIE_RATE, NullCoolie), // s_CoolieDie[5]},
            new State(COOLIE_DIE + 4, COOLIE_DIE_RATE, NullCoolie), // s_CoolieDie[6]},
            new State(COOLIE_DIE + 5, COOLIE_DIE_RATE, NullCoolie), // s_CoolieDie[7]},
            new State(COOLIE_DIE + 6, COOLIE_DIE_RATE, NullCoolie), // s_CoolieDie[8]},
            new State(COOLIE_DIE + 7, COOLIE_DIE_RATE, DoCoolieWaitBirth).setNext(), // s_CoolieDie[8]},
            new State(COOLIE_DIE + 7, COOLIE_DIE_RATE * 5, DoActorDebris), // s_CoolieDie[10]},
            new State(COOLIE_DIE + 7, SF_QUICK_CALL, SpawnCoolg), // s_CoolieDie[11]},
            new State(COOLIE_DEAD_NOHEAD, SF_QUICK_CALL, QueueFloorBlood), // s_CoolieDie[12]},
            new State(COOLIE_DEAD_NOHEAD, COOLIE_DIE_RATE, DoActorDebris).setNext(),// s_CoolieDie[12]}
    };
    private static final EnemyStateGroup sg_CooliePain = new EnemyStateGroup(s_CooliePain[0], s_CooliePain[1], s_CooliePain[2], s_CooliePain[3], s_CooliePain[4]);
    //////////////////////
    //
    // COOLIE PAIN
    //
    //////////////////////
    private static final State[] s_CoolieDead = {new State(COOLIE_DEAD, COOLIE_DIE_RATE, DoActorDebris)};
    private static final EnemyStateGroup sg_CoolieDead = new EnemyStateGroup(s_CoolieDead);

    public static void InitCoolieStates() {
        for (EnemyStateGroup sg : new EnemyStateGroup[]{sg_CoolieCharge, sg_CoolieRun, sg_CooliePain, sg_CoolieDie, sg_CoolieStand, sg_CoolieDead}) {
            for (int rot = 0; rot < sg.getGroup().length; rot++) {
                State.InitState(sg.getGroup()[rot]);
            }
        }
    }

    public static void EnemyDefaults(final int SpriteNum, Actor_Action_Set action, Personality person) {
        USER u = getUser(SpriteNum);
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        int wpn;
        int wpn_cnt;
        int depth = 0;

        switch (u.ID) {
            case PACHINKO1:
            case PACHINKO2:
            case PACHINKO3:
            case PACHINKO4:
            case 623:
            case TOILETGIRL_R0:
            case WASHGIRL_R0:
            case CARGIRL_R0:
            case MECHANICGIRL_R0:
            case SAILORGIRL_R0:
            case PRUNEGIRL_R0:
            case TRASHCAN:
            case BUNNY_RUN_R0:
                break;
            default: {
                TotalKillable++;
            }

            break;
        }

        sp.setCstat(sp.getCstat() & ~(CSTAT_SPRITE_RESTORE));

        u.spal = (byte) sp.getPal();

        u.RotNum = 5;
        sp.setClipdist((256) >> 2);

        u.zclip = Z(48);
        u.lo_step =  Z(32);

        u.floor_dist =  (u.zclip - u.lo_step);
        u.ceiling_dist =  (SPRITEp_SIZE_Z(sp) - u.zclip);

        u.Radius = 400;

        u.MaxHealth = u.Health;

        u.PainThreshold =  (DIV16(u.Health) - 1);

        sp.setCstat(sp.getCstat() | (CSTAT_SPRITE_BLOCK | CSTAT_SPRITE_BLOCK_HITSCAN));
        sp.setExtra(sp.getExtra() | (SPRX_PLAYER_OR_ENEMY));

        sp.setPicnum(u.State.Pic);
        change_sprite_stat(SpriteNum, STAT_ENEMY);

        u.Personality = person;
        u.ActorActionSet = action;

        DoActorZrange(SpriteNum);

        // KeepActorOnFloor(SpriteNum); // for swimming actors

        // make sure we start in the water if thats where we are
        if (u.lo_sectp != -1)// && SectpUser[u.lo_sectp - sector])
        {
            int sectnum = u.lo_sectp;
            Sect_User su = getSectUser(sectnum);
            ru.m210projects.Build.Types.Sector s = boardService.getSector(sectnum);
            if (s != null && su != null && TEST(s.getExtra(), SECTFX_SINK)) {
                depth = su.depth;
            } else {

                for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = node.getNext()) {
                    Sprite np = node.get();
                    if (np.getPicnum() == ST1 && np.getHitag() == SECT_SINK) {
                        depth = np.getLotag();
                    }
                }
            }
        }

        if (depth != 0 && klabs(sp.getZ() - u.loz) < Z(8)) {
            sp.setZ(sp.getZ() + Z(depth));
            u.loz = sp.getZ();
            u.oz = sp.getZ();
        }

        if (action == null) {
            return;
        }

        NewStateGroup(SpriteNum, u.ActorActionSet.Run);

        u.ActorActionFunc = DoActorDecide;

        // find the number of int range attacks
        for (wpn = wpn_cnt = 0; u.ActorActionSet.Attack != null && wpn < u.ActorActionSet.Attack.length; wpn++) {
            if (u.ActorActionSet.Attack[wpn] != null) {
                wpn_cnt++;
            } else {
                break;
            }
        }

        // for actors this tells the number of weapons available
        // for player it tells the current weapon
        u.WeaponNum = wpn_cnt;
    }

    public static void SetupCoolie(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        if (sp == null) {
            return;
        }

        USER u;

        if (TEST(sp.getCstat(), CSTAT_SPRITE_RESTORE)) {
            u = getUser(SpriteNum);
            if (u == null) {
                return;
            }
        } else {
            u = SpawnUser(SpriteNum, COOLIE_RUN_R0, s_CoolieRun[0][0]);
            setUser(SpriteNum, u);
            u.Health = HEALTH_COOLIE;
        }

        ChangeState(SpriteNum, s_CoolieRun[0][0]);
        u.Attrib = CoolieAttrib;
        DoActorSetSpeed(SpriteNum, NORM_SPEED);
        u.StateEnd = s_CoolieDie[0];
        u.setRot(sg_CoolieRun);

        EnemyDefaults(SpriteNum, CoolieActionSet, CooliePersonality);

        sp.setXrepeat(42);
        sp.setYrepeat(42);

        u.Flags |= (SPR_XFLIP_TOGGLE);
    }

    private static final EnemyStateGroup sg_CoolieDie = new EnemyStateGroup(s_CoolieDie);
    private static void SpawnCoolg(int SpriteNum) {
        // Don't do a ghost every time
        if (RANDOM_RANGE(1000) > 700) {
            return;
        }

        NewCoolg(SpriteNum);

        PlaySpriteSound(SpriteNum, Attrib_Snds.attr_extra1, v3df_follow);
    }

    private static void CooliePain(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (!TEST(u.Flags, SPR_CLIMBING)) {
            KeepActorOnFloor(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);

        if ((u.WaitTics -= ACTORMOVETICS) <= 0) {
            InitActorDecide(SpriteNum);
        }
    }

    private static void NullCoolie(int SpriteNum) {
        USER u = getUser(SpriteNum);
        if (u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (!TEST(u.Flags, SPR_CLIMBING)) {
            KeepActorOnFloor(SpriteNum);
        }

        DoActorSectorDamage(SpriteNum);
    }

    private static void DoCoolieMove(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);
        USER u = getUser(SpriteNum);
        if (sp == null || u == null) {
            return;
        }

        if (TEST(u.Flags, SPR_SLIDING)) {
            DoActorSlide(SpriteNum);
        }

        if (u.track >= 0) {
            ActorFollowTrack(SpriteNum, ACTORMOVETICS);
        } else {
            (u.ActorActionFunc).animatorInvoke(SpriteNum);
        }

        KeepActorOnFloor(SpriteNum);

        if (DoActorSectorDamage(SpriteNum)) {
            return;
        }

        Sprite tsp = boardService.getSprite(u.tgt_sp);
        if (tsp != null && Distance(sp.getX(), sp.getY(), tsp.getX(), tsp.getY()) < 1200) {
            UpdateSinglePlayKills(SpriteNum, null);
            DoActorDie(SpriteNum, SpriteNum);
        }
    }
    private static final Actor_Action_Set CoolieActionSet = new Actor_Action_Set(sg_CoolieStand, sg_CoolieRun, null, null, null, null, null, null, null, null, null, // climb
            sg_CooliePain, // pain
            sg_CoolieDie, null, sg_CoolieDead, null, null, new StateGroup[]{sg_CoolieCharge}, new short[]{1024}, new StateGroup[]{sg_CoolieCharge}, new short[]{1024}, null, null, null);

    private static void InitCoolieCharge(int SpriteNum) {
        Sprite sp = boardService.getSprite(SpriteNum);

        if (RANDOM_P2(1024) > 950) {
            PlaySound(DIGI_COOLIESCREAM, sp, v3df_follow);
        }

        DoActorSetSpeed(SpriteNum, FAST_SPEED);

        InitActorMoveCloser.animatorInvoke(SpriteNum);

        NewStateGroup(SpriteNum, sg_CoolieCharge);
    }

    private static void DoCoolieWaitBirth(int SpriteNum) {
        USER u = getUser(SpriteNum);

        if (u != null && (u.Counter -= ACTORMOVETICS) <= 0) {
            ChangeState(SpriteNum, s_CoolieDie[9]);
        }
    }

    public static void CoolieSaveable() {
        SaveData(SpawnCoolieExp);
        SaveData(SpawnCoolg);
        SaveData(CooliePain);
        SaveData(NullCoolie);
        SaveData(DoCoolieMove);
        SaveData(InitCoolieCharge);
        SaveData(DoCoolieWaitBirth);
        SaveData(CooliePersonality);

        SaveData(CoolieAttrib);
        SaveData(s_CoolieRun);
        SaveGroup(sg_CoolieRun);
        SaveData(s_CoolieCharge);
        SaveGroup(sg_CoolieCharge);
        SaveData(s_CoolieStand);
        SaveGroup(sg_CoolieStand);
        SaveData(s_CooliePain);
        SaveGroup(sg_CooliePain);
        SaveData(s_CoolieDie);
        SaveGroup(sg_CoolieDie);
        SaveData(s_CoolieDead);
        SaveGroup(sg_CoolieDead);
        SaveData(CoolieActionSet);
    }
}
