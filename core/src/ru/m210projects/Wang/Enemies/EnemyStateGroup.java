package ru.m210projects.Wang.Enemies;

import ru.m210projects.Wang.Type.State;
import ru.m210projects.Wang.Type.StateGroup;

public class EnemyStateGroup implements StateGroup {

    private final State[][] group;
    private int index = -1;

    EnemyStateGroup(State[]... states) {
        group = states;
    }

    public State getState(int rotation, int offset) {
        return group[rotation][offset];
    }

    public State getState(int rotation) {
        return group[rotation][0];
    }

    public State[][] getGroup() {
        return group;
    }

    @Override
    public int index() {
        return index;
    }

    @Override
    public void setIndex(int index) {
        this.index = index;
    }

}